#!/bin/sh

user=trinity
pw="trinity"
ADIR=`date +%b/%W/%a/`
DIR="/home/wowserver/db_backups/${ADIR}"
DATESTRING=`date +%d%m%Y`
mkdir -p $DIR

mysqldump -R -q -u $user -p$pw trinity_realm > ${DIR}realmd.db.sql
mysqldump -R -q -u $user -p$pw trinity_char > ${DIR}char_live.db.sql
mysqldump -R -q -u $user -p$pw trinity_world > ${DIR}trinity_live.db.sql
