
function check_login(){
    if (!document.regform.r_login.value || document.regform.r_login.value.length > MAX_LOGIN_L || document.regform.r_login.value.length < MIN_LOGIN_L || !document.regform.r_login.value.match(/^[A-Za-z0-9_]+$/)) {
        $('t_login').innerHTML = Msg.cms.registerUsernameCheck+'!';
        $('t_login').show();
        SUCCESS = false;
    } else {
        $('t_login').hide();
        try
        {
            var request = new Ajax.Request(
                SITE_HREF+'index.php?n=ajax&sub=checklogin&nobody=1&ajaxon=1',
                {
                    method: 'get',
                    parameters: 'q=' + encodeURIComponent($F('r_login')),
                    onSuccess: function(reply){
                        if (reply.responseText == 'false') {
                            $('t_login').innerHTML = Msg.cms.registerUsernameExisting+'!';
                            $('t_login').show();
                            SUCCESS = false;
                        } else {
                            SUCCESS = true;
                        }
                    }
                }
            );
        }
        catch (e)
        {
            alert('Error: ' + e.toString());
        }
    }
}
function check_pass(){
    if (!document.regform.r_pass.value || document.regform.r_pass.value.length > MAX_PASS_L || document.regform.r_pass.value.length < MIN_PASS_L) {
        $('t_pass').innerHTML = Msg.cms.registerPasswordCheck+'!';
        $('t_pass').show();
        SUCCESS = false;
    } else {
        $('t_pass').hide();
        SUCCESS = true;
    }
}
function check_cpass(){
    if (!document.regform.r_cpass.value || document.regform.r_pass.value!=document.regform.r_cpass.value) {
        $('t_cpass').innerHTML = Msg.cms.registerPasswordMatch+'!';
        $('t_cpass').show();
        SUCCESS = false;
    } else {
        $('t_cpass').hide();
        SUCCESS = true;
    }
}
function check_email(){
    if (document.regform.r_email.value.length < 1 || !document.regform.r_email.value.match(/^[A-Za-z0-9_\-\.]+\@[A-Za-z0-9_\-\.]+\.\w+$/)) {
        $('t_email').innerHTML = Msg.cms.registerEmailInvalid+'!';
        $('t_email').show();
        SUCCESS = false;
    } else {
        $('t_email').hide();
        try
        {
            var request = new Ajax.Request(
                SITE_HREF+'index.php?n=ajax&sub=checkemail&nobody=1&ajaxon=1',
                {
                    method: 'get',
                    parameters: 'q=' + encodeURIComponent($F('r_email')),
                    onComplete: function(reply){
                        if (reply.responseText == 'false') {
                            $('t_email').innerHTML = Msg.cms.registerEmailExisting+'!';
                            $('t_email').show();
                            SUCCESS = false;
                        } else {
                            SUCCESS = true;
                        }
                    }
                }
            );
        }
        catch (e)
        {
            alert('Fehler: ' + e.toString());
        }
    }
}
function check_all(){
    check_login();
    check_pass();
    check_cpass();
    check_email();
    return SUCCESS;
}