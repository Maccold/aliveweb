<?php
$templategenderimage = array(
    0 => $currtmp.'images/pixel.gif',
    1 => $currtmp.'images/icons/male.gif',
    2 => $currtmp.'images/icons/female.gif'
);
/**
There are 8 menu blocks:
    1-menuNews
    2-menuAccount
    3-menuGameGuide
    4-menuInteractive
    5-menuMedia
    6-menuForums
    7-menuCommunity
    8-menuSupport

    adding custom link, for example:
    $mainnav_links['1-menuNews'][] = array(
        'lang_variable',
        'link',
        ''
    );
*/

function population_view($n) {
    global $lang;
    $maxlow = 100;
    $maxmedium = 200;
    if($n <= $maxlow){
        return '<font color="green">' . $lang['low'] . '</font>';
    }elseif($n > $maxlow && $n <= $maxmedium){
        return '<font color="orange">' . $lang['medium'] . '</font>';
    }else{
        return '<font color="red">' . $lang['high'] . '</font>';
    }
}

function build_menu_items($links_arr){
    global $user;
    global $lang;
    $r = "\n";
    foreach($links_arr as $menu_item){
        $ignore_item = 0;
        if($menu_item[2]) {


            $do_menu_excl = explode('!',$menu_item[2]);
            if(count($do_menu_excl) == 2) {
                if($user[$do_menu_excl[1]]) {
                    $ignore_item = 1;
                }
            }
            else {
                if(!$user[$do_menu_excl[0]]) {
                    $ignore_item = 1;
                }
            }
        }
        if(!$ignore_item && isset($menu_item[0]) && isset($lang[$menu_item[0]]))
            $r .='                                                <div><a class="menufiller" href="'.$menu_item[1].'">'.$lang[$menu_item[0]].'</a></div>'."\n";
    }
    return $r;
}

function build_main_menu(){

    global $mainnav_links;
    
	$output = '';
	
	foreach($mainnav_links as $menuname=>$menuitems){
        $menunamev = explode('-',strtolower($menuname));
        if(count($menuitems)>0)// && $menuitems[0][0])
        {
            static $index = 0;
            $index++;
            $output .= '
                                    <div id="'.$menunamev[1].'">
                                      <div onclick="javascript:toggleNewMenu('.$menunamev[0].'-1);" class="menu-button-off" id="'.$menunamev[1].'-button">
                                        <span class="'.$menunamev[1].'-icon-off" id="'.$menunamev[1].'-icon">&nbsp;</span><a class="'.$menunamev[1].'-header-off" id="'.$menunamev[1].'-header"><em>Menu item</em></a><a id="'.$menunamev[1].'-collapse"></a><span class="menuentry-rightborder"></span>
                                      </div>
                                      <div id="'.$menunamev[1].'-inner">
                                        <script type="text/javascript">
                                            if (menuCookie['.$menunamev[0].'-1] == 0) {
                                                document.getElementById("'.$menunamev[1].'-inner").style.display = "none";
                                                document.getElementById("'.$menunamev[1].'-button").className = "menu-button-off";
                                                document.getElementById("'.$menunamev[1].'-collapse").className = "leftmenu-pluslink";
                                                document.getElementById("'.$menunamev[1].'-icon").className = "'.$menunamev[1].'-icon-off";
                                                document.getElementById("'.$menunamev[1].'-header").className = "'.$menunamev[1].'-header-off";
                                            } else {
                                                document.getElementById("'.$menunamev[1].'-inner").style.display = "block";
                                                document.getElementById("'.$menunamev[1].'-button").className = "menu-button-on";
                                                document.getElementById("'.$menunamev[1].'-collapse").className = "leftmenu-minuslink";
                                                document.getElementById("'.$menunamev[1].'-icon").className = "'.$menunamev[1].'-icon-on";
                                                document.getElementById("'.$menunamev[1].'-header").className = "'.$menunamev[1].'-header-on";
                                            }
                                        </script>
                                        <div class="leftmenu-cont-top"></div>
                                        <div class="leftmenu-cont-mid">
                                          <div class="m-left">
                                            <div class="m-right">
                                              <div class="leftmenu-cnt" id="menucontainer'.$index.'">
                                                <ul class="mainnav">
                                                  <li style="position:relative;" id="menufiller'.$index.'">
                                                    '.build_menu_items($menuitems).'
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="leftmenu-cont-bot"></div>
                                      </div>
                                    </div>';
        }
    }
	return $output;
}

function write_subheader($subheader, $type = 0){
	if($type == 0)
		echo '<h3 class="sub-title"><span>'.$subheader.'</span></h3>';
	if($type == 1)
		echo '<h3 class="section-title"><span>'.$subheader.'</span></h3>';
}

function template_subheader($subheader){
    return '<h3 class="sub-title"><span>'.$subheader.'</span></h3>';
}

function write_metalborder_header(){
    global $MW;
	global $currtmp;
    echo '<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
    <td width="12"><img src="'.$currtmp.'/images/metalborder-top-left.gif" height="12" width="12" alt=""/></td>
    <td style="background:url(\''.$currtmp.'/images/metalborder-top.gif\');"></td>
    <td width="12"><img src="'.$currtmp.'/images/metalborder-top-right.gif" height="12" width="12" alt=""/></td>
</tr>
<tr>
    <td style="background:url(\''.$currtmp.'/images/metalborder-left.gif\');"></td>
    <td>
';
}

function write_metalborder_footer(){
    global $MW;
	global $currtmp;
    echo '        </td>
        <td style="background:url(\''.$currtmp.'/images/metalborder-right.gif\');"></td>
    </tr>
    <tr>
        <td><img src="'.$currtmp.'/images/metalborder-bot-left.gif" height="11" width="12" alt=""/></td>
        <td style="background:url(\''.$currtmp.'/images/metalborder-bot.gif\');"></td>
        <td><img src="'.$currtmp.'/images/metalborder-bot-right.gif" height="11" width="12" alt=""/></td>
    </tr>
    </tbody>
</table>
';
}

function write_form_tool(){
    global $MW;
	global $currtmp;
    $template_href = $currtmp . "/";
?>
        <div id="form_tool">
            <ul id="bbcode_tool">
                <li id="bbcode_b"><a href="#"><img src="<?php echo $template_href;?>images/button-bold.gif" alt="<?php lang('editor_bold'); ?>" title="<?php lang('editor_bold'); ?>"></a></li>
                <li id="bbcode_i"><a href="#"><img src="<?php echo $template_href;?>images/button-italic.gif" alt="<?php lang('editor_italic'); ?>" title="<?php lang('editor_italic'); ?>"></a></li>
                <li id="bbcode_u"><a href="#"><img src="<?php echo $template_href;?>images/button-underline.gif" alt="<?php lang('editor_underline'); ?>" title="<?php lang('editor_underline'); ?>"></a></li>
                <li id="bbcode_url"><a href="#"><img src="<?php echo $template_href;?>images/button-url.gif" alt="<?php lang('editor_link'); ?>" title="<?php lang('editor_link'); ?>"></a></li>
                <li id="bbcode_img"><a href="#"><img src="<?php echo $template_href;?>images/button-img.gif" alt="<?php lang('editor_image'); ?>" title="<?php lang('editor_image'); ?>"></a></li>
                <li id="bbcode_blockquote"><a href="#"><img src="<?php echo $template_href;?>images/button-quote.gif" alt="<?php lang('editor_quote'); ?>" title="<?php lang('editor_quote'); ?>"></a></li>
            </ul>
            <ul id="text_tool">
                <li id="text_size"><a href="#"><img src="<?php echo $template_href;?>images/button-size.gif" alt="<?php lang('editor_size'); ?>" title="<?php lang('editor_size'); ?>"></a>
                    <ul>
                        <li id="text_size-hugesize"><a href="#">Huge</a></li>
                        <li id="text_size-largesize"><a href="#">Large</a></li>
                        <li id="text_size-mediumsize"><a href="#">Medium</a></li>
                    </ul>
                </li>
                <li id="text_color"><a href="#"><img src="<?php echo $template_href;?>images/button-color.gif" alt="<?php lang('editor_color'); ?>" title="<?php lang('editor_color'); ?>"></a>
                    <ul>
                        <li id="text_color-red"><a href="#"><?php lang('editor_color_red'); ?></a></li>
                        <li id="text_color-green"><a href="#"><?php lang('editor_color_green'); ?></a></li>
                        <li id="text_color-blue"><a href="#"><?php lang('editor_color_blue'); ?></a></li>
                        <li id="text_color-custom"><a href="#"><?php lang('editor_color_custom'); ?></a></li>
                    </ul>
                </li>
                <li id="text_align"><a href="#"><img src="<?php echo $template_href;?>images/button-list.gif" alt="<?php lang('editor_align'); ?>" title="<?php lang('editor_align'); ?>"></a>
                    <ul>
                        <li id="text_align-left"><a href="#"><?php lang('editor_align_left'); ?></a></li>
                        <li id="text_align-right"><a href="#"><?php lang('editor_align_right'); ?></a></li>
                        <li id="text_align-center"><a href="#"><?php lang('editor_align_center'); ?></a></li>
                        <li id="text_align-justify"><a href="#"><?php lang('editor_align_justify'); ?></a></li>
                    </ul>
                </li>
                <li id="text_smile"><a href="#"><img src="<?php echo $template_href;?>images/button-emote.gif" alt="<?php lang('editor_smile'); ?>" title="<?php lang('editor_smile'); ?>"></a>
                    <ul>
<?php
$smiles = load_smiles();
$smilepath = (string)$MW->getConfig->generic->smiles_path;
foreach($smiles as $smile):
    $smilename = ucfirst(str_replace('.gif','',str_replace('.png','',$smile)));
?>
                        <li id="text_smile-<?php echo $smilepath.$smile;?>"><a href="#" title="<?php echo $smilename;?>"><img src="<?php echo $smilepath.$smile;?>" alt="<?php echo $smilename;?>"></a></li>
<?php
endforeach;
?>
                    </ul>
                </li>
            </ul>
        </div>
<?php
}

function random_screenshot(){
  $fa = array();
  if ($handle = opendir('images/screenshots/thumbs/')) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != ".." && $file != "Thumbs.db" && $file != "index.html") {
            $fa[] = $file;
        }
    }
    closedir($handle);
  }
  $fnum = count($fa);
  $fpos = rand(0, $fnum-1);
  return $fa[$fpos];
}

function load_banners($type){
    global $DB;
    $result = $DB->select("SELECT * FROM banners WHERE type=?d ORDER BY num_click DESC",$type);
    return $result;
}

function paginate($num_pages, $cur_page = -1, $link_to = array()){

	$pages = array();
	$link_to_all = false;
	
	/*echo "<!-- ";
	echo "\npages: $cur_page/$num_pages";
	*/
	$ext = (!empty($link_to[0])) ? $link_to[0] : "";
	$sub = (!empty($link_to[1])) ? $link_to[1] : "";
	$params = (!empty($link_to[2])) ? $link_to[2] : array();
		
	if ($cur_page == -1)
	{
		$cur_page = 1;
		$link_to_all = true;
	}
	
	if ($num_pages <= 1)
	{
		if(is_array($link_to))
			$pages[1] = '<li class="current"><a href="'.url_for($ext, $sub, array_merge($params, array("page" => "1")) ).'">'."1".'</a></li>';
		else
			$pages[1] = '<li class="current"><a href="'.$link_to.'&page='."1".'">'."1".'</a></li>';
	}
	else
	{
		$tens = floor($num_pages/10);
		for ($i=1;$i<=$tens;$i++)
		{
			$tp = $i*10;
			if(is_array($link_to))
				$pages[$tp] = '<li><a href="'.url_for($ext, $sub, array_merge($params, array("page" => $tp)) ).'">'.$tp.'</a></li>';
			else
				$pages[$tp] = '<li><a href="'.$link_to.'&page='.$tp.'">'.$tp.'</a></li>';
		}
		
		if ($cur_page > 3)
		{
			if(is_array($link_to))
				$pages[1] = '<li><a href="'.url_for($ext, $sub, array_merge($params, array("page" => 1)) ).'">1</a></li>';
			else
				$pages[1] = '<li><a href="'.$link_to.'&page=1">1</a></li>';
		}
		for ($current = $cur_page - 2, $stop = $cur_page + 3; $current < $stop; ++$current)
		{
			if ($current < 1 || $current > $num_pages){
				continue;
			}
			elseif ($current != $cur_page || $link_to_all){
				if(is_array($link_to))
					$pages[$current] = '<li><a href="'.url_for($ext, $sub, array_merge($params, array("page" => $current)) ).'">'.$current.'</a></li>';
				else
					$pages[$current] = '<li><a href="'.$link_to.'&page='.$current.'">'.$current.'</a></li>';
			}
			else{
				if(is_array($link_to))
					$pages[$current] = '<li class="current"><a href="'.url_for($ext, $sub, array_merge($params, array("page" => $current)) ).'">'.$current.'</a></li>';
				else
					$pages[$current] = '<li class="current"><a href="'.$link_to.'&page='.$current.'">'.$current.'</a></li>';
			}
		}
		if ($cur_page <= ($num_pages-3))
		{
			if(is_array($link_to))
				$pages[$num_pages] = '<li><a href="'.url_for($ext, $sub, array_merge($params, array("page" => $num_pages)) ).'">'.$num_pages.'</a></li>';
			else
				$pages[$num_pages] = '<li><a href="'.$link_to.'&page='.$num_pages.'">'.$num_pages.'</a></li>';
		}
	}
	$pages = array_unique($pages);
	ksort($pages);
	$pp = implode("\n", $pages);
	$pp = str_replace('//','/',$pp);

	/*echo print_r($pages,true);
	echo " -->";
*/
	return '
	<ul class="ui-pagination">
		'.$pp.'
	</ul>';
	
}

function builddiv_start($type = 0, $title = "No title set", $subtitle = "") {
	global $currtmp;
	
	if(!empty($subtitle))
		$subtitle = "<p>$subtitle</p>";
	
	if ($type == 1) {
		?>
<div class="top-banner">
	<div class="section-title">
		<span><?=$title?></span>
		<?=$subtitle?>
	</div>
	<span class="clear"><!-- --></span>		
</div>
<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
		<?
	}
	else {
		if ($title != "No title set") {
			?>
<div class="top-banner">
	<div class="section-title">
		<span><?=$title?></span>
		<?=$subtitle?>
	</div>
	<span class="clear"><!-- --></span>		
</div>
<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<?
		}
		else {
			?>
<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<?
		}
	}
}


function builddiv_end() {
	echo '</div></div></div>'; 
}

function icon_faction($faction, $tooltip = true){
	global $MANG;
	
	$label = ($faction == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	
	$output = '
	<span class="icon-frame frame-18" data-tooltip="'.$label.'">
		<img src="/images/icons/18/faction_'.$faction.'.jpg" alt="" width="18" height="18" />
	</span>';
	
	return $output;
}

function icon_race($race, $gender){
	global $MANG;
	
	$output = '
	<span class="icon-frame frame-18" data-tooltip="'.$MANG->characterInfoByID['character_race'][$race].'">
		<img src="/images/icons/18/race_'.$race.'_'.$gender.'.jpg" height="18" width="18">
	</span>';
	
	return $output;
}

function icon_class($class, $tooltip = true){
	global $MANG;
	
	if($tooltip)
		$output = '
	<span class="icon-frame frame-18" data-tooltip="'.$MANG->characterInfoByID['character_class'][$class].'">
		<img src="/images/icons/18/class_'.$class.'.jpg" height="18" width="18">
	</span>';
	else
		$output = '
	<span class="icon-frame frame-18">
		<img src="/images/icons/18/class_'.$class.'.jpg" height="18" width="18">
	</span>';
	
	
	return $output;
}

?>
