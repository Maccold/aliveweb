<?php
if(!defined("INCLUDED"))
	die();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<!-- <base href="http://portal.wow-alive.de"/> -->
<meta http-equiv="content-type" content="text/html; charset=<?php echo (string)$MW->getConfig->generic->site_encoding;?>"/>

<title><?php echo $page_title; ?></title>

<link rel="alternate" href="<?php echo $this_rss_url; ?>" type="application/rss+xml" title="<?=$page_rss_title?>"/>
<link href="http://forum.wow-alive.de/static-wow/local-common/images/favicons/wow.ico" type="image/x-icon" rel="icon" />
<link href="http://forum.wow-alive.de/static-wow/local-common/images/favicons/wow.ico" type="image/x-icon" rel="shortcut icon" />
<script type="text/javascript" src="http://static.wowhead.com/widgets/power.js"></script>

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="/<?php echo $currtmp; ?>/css/html-reset.css" />
<link rel="stylesheet" type="text/css" href="/<?php echo $currtmp; ?>/css/layout-fixed.css" />
<link rel="stylesheet" type="text/css" href="/<?php echo $currtmp; ?>/css/sideboard.css" />
<? 
foreach($css_files as $css_link){?>
<link rel="stylesheet" type="text/css" href="<?=$css_link?>" /><?	
} ?>
<!-- / CSS Stylesheet -->

<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="http://forum.wow-alive.de/js/jquery-ui-1.8.9.custom.min.js"></script>

<script type="text/javascript" src="/<?php echo $currtmp; ?>/js/core.js?v15"></script>
<script type="text/javascript" src="/<?php echo $currtmp; ?>/js/wow.js?v15"></script>
<script type="text/javascript" src="/<?php echo $currtmp; ?>/js/tooltip.js?v15"></script>

<!--[if IE 6]>
<script type="text/javascript">
  //<![CDATA[
    try { document.execCommand('BackgroundImageCache', false, true) } catch(e) {}
  //]]>
</script>
<![endif]-->

<script type="text/javascript">
//<![CDATA[
    var SITE_HREF = '<?php echo $MW->getConfig->temp->site_href;?>';
    var DOMAIN_PATH = '<?php echo $MW->getConfig->temp->site_domain;?>';
    var SITE_PATH = '<?php echo $MW->getConfig->temp->site_href;?>';
	
	var global_nav_lang = '<?php echo ""; ?>'; 
	var site_name = '<?php echo (string)$MW->getConfig->generic->site_title ?>';
	var site_link = '<?php echo "http://www.wow-alive.de/" ?>';
	var forum_link = '<?php echo "http://forum.wow-alive.de/forum.php" ?>';
	var armory_link = '<?php echo "http://arsenal.wow-alive.de/" ?>';
	var xsToken = '<?=$securityToken?>';

	Core.staticUrl = 'http://forum.wow-alive.de/static-wow';
	Core.baseUrl = 'http://portal.wow-alive.de';
	Core.cdnUrl = 'http://portal.wow-alive.de';
	Core.project = 'wow';
	Core.locale = 'de-de';
	Core.buildRegion = 'eu';
	Core.loggedIn = false;
	
//]]>
</script>

<!-- TS Viewer Sideboard -->
<script type="text/javascript" src="/js/wz_tooltip.js"></script>
<script type="text/javascript" src="http://static.tsviewer.com/short_expire/js/ts3viewer_loader.js"></script>
<script type="text/javascript" src="http://portal.wow-alive.de/js/sideboard.js"></script>
<!-- /TS Viewer Sideboard -->

<?php 
	// print something like that when use redirect('link',0,3); <meta http-equiv=refresh content="'.$wait_sec.';url='.$linkto.'">
	if(!empty($GLOBALS['redirect']))
		echo $GLOBALS['redirect'];
?>

</head>


<body id="<?=$ext."-".$sub?>">

<!-- Teamspeak -->
<div class="sb_overlay sb_slide" id="ts_overlay">
	<img class="ts_waitload_overlay" src="/images/ajax-loader.gif"> 
</div>

<div id="ts_control" class="sb_passive_container"> 
	<div id="ts_button"> 	
		<div id="ts_label"></div>     
		<!--<span id="sb_passive_count" class="sb_passive_1n" style="">4</span> -->  
		<div id="sb_passive_large"></div> 
	</div> 
	<img class="sb_waitload" src="/images/ajax-loader.gif"> 
</div>
<!-- /Teamspeak -->


<div id="wrapper">
  <div id="header">
   <a name="top"></a>
   <div id="search-bar">
   <? if( $userObject->isLoggedIn() ){ ?>
	<form action="/search/" method="get" id="search-form">
		<div>
			<input type="submit" id="search-button" value="" tabindex="41"/>
			<input type="text" name="q" id="search-field" maxlength="200" tabindex="40" alt="Durchsucht das Arsenal und mehr..." value="Durchsucht das Arsenal und mehr..." />
		</div>
	</form>
	<? } ?>
    </div>
    <h1 id="logo"><a href="<?php echo (string)$MW->getConfig->temp->site_domain;?>">World of Warcraft Alive</a></h1>
    <div class="header-plate">
	<ul id="menu">
<!-- eigentlich müsste hier die var navcustomlinks rein.. -->
		<li class="menu-home">
			<a href="http://portal.wow-alive.de<?=url_for("frontpage")?>"><span>Hauptseite</span></a>
		</li>
		<li class="menu-game">
			<a href="http://portal.wow-alive.de<?=url_for("game")?>"><span>Spiel</span></a>
		</li>
		<li class="menu-community">
			<a href="http://portal.wow-alive.de<?=url_for("server")?>"><span>Server</span></a>
		</li>
		<li class="menu-media">
			<a href="http://portal.wow-alive.de<?=url_for("account", "index")?>"><span>Account</span></a>
		</li>
		<li class="menu-forums">
			<a href="http://forum.wow-alive.de/forum.php"><span>Foren</span></a>
		</li>
		<li class="menu-services">
		<? if($userObject->can["vote"]){ ?>
			<span class="sb_passive_count sb_passive_1n" style="left:75px; top:-5px;">!!!</span>
			<a href="http://portal.wow-alive.de<?=url_for("community","vote")?>"><span>Voten</span></a>
		<? }  else { ?>
			<a href="http://portal.wow-alive.de<?=url_for("community","item")?>"><span>Vote Shop</span></a>
		<? } ?>
		</li>
	</ul>

<?	if( $userObject->isLoggedIn() ){ ?>
	<? if(count($userObject->characters) == 0){ ?>
	<!-- $chosen_char[bb] -->
	<div id="user-plate" class="user-plate plate-nochars ajax-update">
		<div class="card-overlay"></div>
		<a href="<?=url_for("account","manage");?>" rel="np" class="profile-link">
			<span class="hover"></span>
		</a>
		<div class="user-meta">
			<div class="player-name"><a href="<?=url_for("account","manage");?>"><?=$userObject->getName(); ?></a></div>
			<div class="character"></div>
			<div class="guild"></div>
		</div>
	</div>
	<? } else { 
		echo $userPlate_content;
		 ?>
	<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function() {
		Tooltip.bind('#header .user-meta .character-name', { location: 'topCenter' });
	});
	//]]>
	</script>
<? 	
	}
} 
else 
{ ?>
	<div class="user-plate ajax-update">
		<div class="user-meta meta-login">
			<a href="<?php echo url_for('account', 'login'); ?>">
			<strong>Loggt euch</strong></a> mit eurem Account ein, um Kommentare abzugeben 
			und die Seiteninhalte für euch anzupassen.
		</div>
	</div>
<? } ?>
    </div>
  </div> <!-- header -->

  <!-- content table -->
  <div id="content">
   <div class="content-top">
    <? if (!$skipbreadcrumbs && $ext != "frontpage"){ ?>
	  <!-- breadcrumb -->
	  <div class="content-trail">
		<ol class="ui-breadcrumb">
			<? 
			if(is_array($pathway_info)){
				$count = count($pathway_info);
				for($i = 0; $i < $count; $i++)
				{
					$path = $pathway_info[$i];
					$last = ($i == ($count-1) ) ? ' class="last"' : '';
					
					if(isset($path['title'])){
						if(empty($path['link'])) 
							echo '<li'.$last.'>'.$path['title']."</li>";
						else 
							echo '<li'.$last.'><a href="'.$path['link'].'">'.$path['title'].'</a></li>';
					}
				}
			}
			else
			{ ?>
			<li>
				<a href="<?php echo (string)$MW->getConfig->temp->site_domain;?>" accesskey="1" rel="np">WoW Alive</a>
			</li>
			<li><?=$pathway_str?></li>
			<?
			} ?>
		</ol>
	  </div> <!-- /content-trail -->
	<?php } ?>
	  <div class="content-bot">

		<? 
		if($show_sidebar){ 
			if($ext == "frontpage"){
				?>
		<div id="<?=$pageId?>">
			<div id="left"><?=$content?></div> 
			<div id="right"><?=$sidebar_content?></div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
				<?
			}
			else{
				?>
		<div class="bg-body">
		 <div class="bg-bottom">
		  <div class="contents-wrapper">
		 	<div class="left-col"><?=$content?></div> 
			<div class="right-col"><?=$sidebar_content?></div>
			<span class="clear"><!-- --></span>
		  </div>
		  <span class="clear"><!-- --></span>
		 </div>
		</div>
				<?
			}
		}
		else{
			echo $content; 
		}
		?>
		
	</div> <!-- /content-bot -->
   </div> <!-- /content-top -->
  </div> <!-- /content -->
  <!-- /content area table -->

  <div id="footer">
	<div id="sitemap">
		<div class="column">
			<h3 class="bnet">
				<a href="http://www.wow-alive.de/" tabindex="100">Alive</a>
			</h3>
			<ul>
				<li><a href="<?=url_for("community","vote")?>">Voten!</a></li>
				<li><a href="<?=url_for("server","rules")?>">Serverregeln</a></li>
				<li><a href="http://portal.wow-alive.de/">Portal</a></li>
				<li><a href="http://forum.wow-alive.de/">Forum</a></li>
				<li><a href="http://portal.wow-alive.de/search/">Arsenal</a></li>
				<li><a href="http://top100.wow-alive.de/raiders.php">Top 100</a></li>
				<li><a href="http://forum.wow-alive.de/showgroups.php">Das Alive Team</a></li>
			</ul>
		</div>
		<div class="column">
			<h3 class="games">
				<a href="<?=url_for("account","manage")?>" tabindex="100">Das Spiel</a>
			</h3>
			<ul>
				<li><a href="<?=url_for("account","register")?>" tabindex="100">Account erstellen</a></li>
				<li><a href="<?=url_for("server","ta")?>">Transfer</a></li>
				<li><a href="<?=url_for("server","howtoplay")?>">Installationsanleitung</a></li>
				<li><a href="http://arsenal.wow-alive.de/talent-calc.php">Talentrechner</a></li>
			</ul>
		</div>
		<div class="column">
			<h3 class="account">
				<a href="<?=url_for("account","manage")?>" tabindex="100">Accounts</a>
			</h3>
			<ul>
				<li><a href="<?=url_for("account","register")?>" tabindex="100">Spiel Account erstellen</a></li>
				<li><a href="<?=url_for("account","manage")?>">Spiel Account verwalten</a></li>
				<li><a href="http://forum.wow-alive.de/usercp.php">Forum Account verwalten</a></li>
				<li><a href="<?=url_for("community","item")?>">Item Shop</a></li>
			</ul>
		</div>
		<div class="column">
			<h3 class="support">Support</h3>
			<ul>
				<li><a href="http://forum.wow-alive.de/showgroups.php">Das Alive Team</a></li>
				<li><a href="<?=url_for("server","bugtracker")?>">Bugtracker</a></li>
				<li><a href="<?=url_for("server","realmstatus")?>">Realmstatus</a></li>
				<li><a href="https://twitter.com/#!/erd_geist" target="_blank">Twitter</a></li>
			</ul>
		</div>
		<div id="copyright">
		  &copy;2012 Blizzard Entertainment, Inc. Alle Rechte vorbehalten
		  <a href="http://forum.wow-alive.de/sendmessage.php" rel="nofollow" accesskey="9">Kontakt</a></if>
		  <? /*
		  <a href="$admincpdir/index.php">$vbphrase[admin]</a>
		  <a href="$modcpdir/index.php">$vbphrase[mod]</a> */ ?>
		  <a href="http://forum.wow-alive.de/archive/index.php">Forumarchiv</a>

		  <div class="smallfont">
			<strong>
				<a href="#top" onclick="self.scrollTo(0, 0); return false;">nach oben</a>
			</strong>
		  </div>
		</div>
		<div id="legal">
			<div class="smallfont" align="center">
				<?php echo $lang['pagegenerated'];?> <?php echo round($exec_time,4);?> sec.
				Query's: (RDB: <?php echo $DB->_statistics['count']; ?>,
				WSDB: <?php echo $WSDB->_statistics['count']?>,
				CHDB: <?php echo $CHDB->_statistics['count']?>)<br/>
				<b>&copy; <?php echo (string)$MW->getConfig->generic->copyright; ?></b>
			</div>
			<div id="blizzard" class="png-fix">&nbsp;</div>
			<span class="clear"><!-- --></span>
		</div>
	</div>
  </div> <!-- footer -->
  
  <!-- Service Bar -->
  <div id="service">
	<ul class="service-bar">
	<?php if($showTicketWarning){ ?>
		<li class="service-cell service-home service-maintenance"><a href="http://portal.wow-alive.de/admin/tickets/" tabindex="50" accesskey="1" data-tooltip="Es gibt offene Tickets">&nbsp;</a></li>
	<?php } else { ?>
		<li class="service-cell service-home"><a href="http://portal.wow-alive.de/" tabindex="50" accesskey="1" title="ALive">&nbsp;</a></li>
	<?php } ?>	
	<? if( $userObject->isLoggedIn() ){ ?>
		<li class="service-cell service-welcome">
			Willkommen, <?=(($userObject->can["g_is_admin"] || $userObject->can["g_is_supadmin"]) ? '<span class="employee"></span>' : '')?><?=$userObject->getName();?>
			 | <a href="<?=url_for("account","logout")?>">Abmelden</a>
		</li>
		<li class="service-cell service-account">
			<a href="http://forum.wow-alive.de/private.php" class="service-link" tabindex="50" accesskey="2">Nachrichten</a>
		</li>
		<?php /*?>
		<li class="service-cell service-support service-support-enhanced">
			<a href="#support" class="service-link service-link-dropdown" tabindex="50" accesskey="4" id="support-link" style="cursor: pointer; " rel="javascript">
				Support
				<span class="open-support-tickets" id="support-ticket-count">2</span>
			</a>
			<div class="support-menu" id="support-menu" style="display: none; ">
				<div class="support-primary">
				<ul class="support-nav">
					<li>
						<a href="http://eu.battle.net/support/" tabindex="55" class="support-category">
							<strong class="support-caption">Supportbeiträge</strong>
							Durchsuchen Sie unsere Wissensdatenbank.
						</a>
					</li>
					<li>
						<a href="https://eu.battle.net/support/ticket/status" tabindex="55" class="support-category">
							<strong class="support-caption">Support-Tickets</strong>
							Sehen Sie Ihre gesamte Ticket-Historie
						</a>
						<div class="ticket-summary" id="ticket-summary">
						<ul>
							<li class="first-ticket">
								<a href="https://eu.battle.net/support/ticket/thread/23297385">
									<span class="icon-ticket-status"></span>
									Der Ticketstatus zu Ticket EU23297385 hat sich zu <span class="ticket-canceled">Geschlossen</span> verändert.<br />
									<span class="ticket-datetime">10.04.2012 17:24</span>
								</a>
							</li>
							<li>
								<a href="https://eu.battle.net/support/ticket/thread/23297240">
									<span class="icon-ticket-status"></span>
									Der Ticketstatus zu Ticket EU23297240 hat sich zu <span class="ticket-canceled">Geschlossen</span> verändert.<br />
									<span class="ticket-datetime">10.04.2012 17:20</span>
								</a>
							</li>
						</ul>
						</div>
					</li>
				</ul>
				<span class="clear"><!-- --></span>
				</div>
				<div class="support-secondary"></div>
				<!--[if IE 6]> 
					<iframe id="support-shim" src="javascript:false;" frameborder="0" scrolling="no" style="display: block; position: absolute; top: 0; left: 9px; width: 297px; height: 400px; z-index: -1;"></iframe>
					<script type="text/javascript">
					//<![CDATA[
						(function(){
						var doc = document;
						var shim = doc.getElementById('support-shim');
						shim.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)';
						shim.style.display = 'block';
						})();
					//]]>
					</script>
				<![endif]-->
			</div>
		</li> */ ?>
	<? } else { ?>
		<li class="service-cell service-welcome">
			<a href="<?=url_for("account","register")?>" accesskey="1">Registrieren</a>
		</li>
	<? } ?>
	<li class="service-cell service-support">
		<a href="<?=url_for("server","bugtracker")?>" class="service-link" tabindex="50" accesskey="4">Bugtracker</a></li>
	<li class="service-cell service-explore">
		<a href="#explore" tabindex="50" accesskey="5" class="dropdown" id="explore-link" onclick="return false" style="cursor: progress" rel="javascript">Erkunden</a>
		<div class="explore-menu" id="explore-menu" style="display:none;">
			<div class="explore-primary">
				<ul class="explore-nav">
				<? if( $userObject->isGM() ){ ?>
				<li>
					<a href="<?=url_for("admin")?>" tabindex="55">
						<strong class="explore-caption">Administration</strong>
						GameMaster-Tools
					</a>
				</li>
				<? } ?>
				<li>
					<a href="http://www.wow-alive.de" tabindex="55">
						<strong class="explore-caption">ALive</strong>
						Verbinden. Spielen. Leben.
					</a>
				</li>
				<? if( $userObject->isLoggedIn() ){ ?>
				<li>
					<a href="http://forum.wow-alive.de/private.php">
						<strong class="explore-caption">Nachrichten</strong>
					</a>
				</li>
				<li>
					<a href="<?=url_for("account","manage")?>" tabindex="55">
						<strong class="explore-caption">Account</strong>
						Account verwalten
					</a>
				</li>
				<? } else { ?>
				<li>
					<a href="<?=url_for("account","register")?>" tabindex="55">
						<strong class="explore-caption">Account</strong>
						Account registrieren
					</a>
				</li>
				<? } ?>
				<li>
					<a href="<?=url_for("server","bugtracker")?>" tabindex="55">
						<strong class="explore-caption">Bugtracker</strong>
						Melde Bugs die du gefunden hast damit wir sie l&ouml;sen k&ouml;nnen.
					</a>
				</li>
				<li>
					<a href="http://forum.wow-alive.de/misc.php?do=cfrules">
						<strong class="explore-caption">Forum Regeln</strong>
					</a>
				</li>
				<li>
					<a rel="help" href="http://forum.wow-alive.de/faq.php" accesskey="5">
						<strong class="explore-caption">FAQ</strong>
						Fragen zum Forum
					</a>
				</li>
		
				</ul>
				
				<div class="explore-links">
					<h2 class="explore-caption">Community</h2>
					<ul>
						<li><a href="http://forum.wow-alive.de/showgroups.php">GameMaster</a></li>
						<!-- community link menu -->
						<li><a href="http://forum.wow-alive.de/group.php?">Interessengemeinschaften</a></li>
						<li><a href="http://forum.wow-alive.de/album.php">Bilder &amp; Alben</a></li>
						<li><a href="http://forum.wow-alive.de/profile.php?do=buddylist">Kontakte &amp; Freunde</a></li>
						<li><a href="http://forum.wow-alive.de/memberlist.php">Benutzerliste</a></li>				  
						<!-- / community link menu -->
					
					</ul>
					<br />
					
					<h2 class="explore-caption">N&uuml;tzliche Links</h2>
					<!-- user cp tools menu -->
					<ul>
					<? if( $userObject->isLoggedIn() ){ ?>
						<li><a href="http://forum.wow-alive.de/search.php?do=getnew">Neue Beiträge</a></li>
					<? } ?>
						<li><a href="http://forum.wow-alive.de/search.php?do=getdaily">Heutige Beträge</a></li>
					<? if( $userObject->isLoggedIn() ){ ?>
						<li><a href="http://forum.wow-alive.de/subscription.php" rel="nofollow">Abonnierte Themen</a></li>
					<? } ?>
						<li><a href="http://forum.wow-alive.de/online.php">Wer ist online</a></li>
					<? if( $userObject->isLoggedIn() ){ ?>
						<li><a href="http://forum.wow-alive.de/forumdisplay.php?do=markread">Alle Foren als gelesen markieren</a></li>
						<li><a href="#" onclick="window.open('http://forum.wow-alive.de/misc.php?do=buddylist&amp;focus=1','buddylist','statusbar=no,menubar=no,toolbar=no,scrollbars=yes,resizable=yes,width=250,height=300'); return false;">Kontakte</a></li>
					<? } ?>
					</ul>
					
					
					<? if( $userObject->isLoggedIn() ){ ?>
					<br />
					<h2 class="explore-caption">Benutzerkontrollzentrum</h2>
					<ul>
						<li><a href="http://forum.wow-alive.de/profile.php?do=editsignature">Signatur bearbeiten</a></li>
						<li><a href="http://forum.wow-alive.de/profile.php?do=editavatar">Benutzerbild ändern</a></li>
						<li><a href="http://forum.wow-alive.de/profile.php?do=editprofile">Profil bearbeiten</a></li>
						<li><a href="http://forum.wow-alive.de/profile.php?do=editoptions">Einstellungen ändern</a></li>
					</ul>
					<? } ?>
					<!-- / user cp tools menu -->
				</div>
				<span class="clear"><!-- --></span>
				</div>
			</div>
		</li>
  	</ul>
  </div> <!-- /service -->
  
  <div id="warnings-wrapper">
   <?php echo $GLOBALS['messages']; ?>
   
	<!--[if lt IE 8]>
	<div id="browser-warning" class="warning warning-red">
		<div class="warning-inner2">
			Sie benutzen eine veraltete Browserversion.<br />
			<a href="http://www.mozilla.org/de/firefox">Aktualisieren Sie Ihren Browser</a>.
			<a href="#close" class="warning-close" onclick="App.closeWarning('#browser-warning', 'browserWarning'); return false;"></a>
		</div>
	</div>
	<![endif]-->
	<!--[if lte IE 8]>
		<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/CFInstall.min.js?v15"></script>
		<script type="text/javascript">
		//<![CDATA[
		$(function() {
			var age = 365 * 24 * 60 * 60 * 1000;
			var src = 'https://www.google.com/chromeframe/?hl=de-DE';
			if ('http:' == document.location.protocol) {
				src = 'http://www.google.com/chromeframe/?hl=de-DE';
			}
			document.cookie = "disableGCFCheck=0;path=/;max-age="+age;
			$('#chrome-frame-link').bind({
				'click': function() {
					App.closeWarning('#browser-warning');
					CFInstall.check({
						mode: 'overlay',
						url: src
					});
					return false;
				}
			});
		});
		//]]>
		</script>
	<![endif]-->

	<noscript>
		<div id="javascript-warning" class="warning warning-red">
			<div class="warning-inner2">
				Javascript muss aktiviert sein, um diese Seite nutzen zu k&ouml;nnen!
			</div>
		</div>
	</noscript>
  </div> <!-- /warnings -->
</div> <!-- /wrapper -->

<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function() {
	<? if($ext == "frontpage"){ ?>
		$("#menu .menu-home a").addClass("active");
	<? } else if($ext == "game" ){ ?>
		$("#menu .menu-game a").addClass("active");
	<? } else if($ext == "server"
		||	($ext == "community" && $sub == "changelog")
		||	($ext == "community" && $sub == "clcore")
		||	($ext == "community" && $sub == "clpage")
			){ ?>
		$("#menu .menu-community a").addClass("active");
	<? } else if($ext == "account" 
		||	($ext == "server" && $sub == "ta")
			){ ?>
		$("#menu .menu-media a").addClass("active");
	<? } else if(
			($ext == "community" && $sub == "item")
		||	($ext == "community" && $sub == "vote")
			){ ?>
		$("#menu .menu-services a").addClass("active");
	<? } ?>
	});
	//]]>
</script>


<script type="text/javascript">
//<![CDATA[
	var Msg = {
		ui: {
			submit: 'Abschicken',
			cancel: 'Abbrechen',
			reset: 'Zurücksetzen',
			viewInGallery: 'Zur Galerie',
			loading: 'Lade…',
			unexpectedError: 'Ein Fehler ist aufgetreten',
			fansiteFind: 'Finde dies auf…',
			fansiteFindType: 'Finde {0} auf…',
			fansiteNone: 'Keine Fanseiten verfügbar.'
		},
		grammar: {
			colon: '{0}:',
			first: 'Erster',
			last: 'Letzter'
		},
		cms: {
		<? foreach($js_messageStrings as $key => $string){
		echo "\n			$key: '$string',";
		} ?>
			requestError: 'Deine Anfrage konnte nicht bearbeitet werden',
            ignoreNot: 'Dieser Benutzer wird nicht ignoriert',
            ignoreAlready: 'Dieser Benutzer wird bereits ignoriert',
            stickyRequested: 'Sticky beantragt',
            postAdded: 'Beitrag zur Beobachtung hinzugefügt',
            postRemoved: 'Beitrag von der Beobachtung gestrichen',
            userAdded: 'Benutzer zur Beobachtung hinzugefügt',
            userRemoved: 'Benutzer von der Beobachtung gestrichen',
            validationError: 'Ein benötigtes Feld ist nicht ausgefüllt',
            characterExceed: 'Der Inhalt des Textfeldes überschreitet XXXXXX Buchstaben.',
            searchFor: "Suche nach",
            searchTags: "Artikel markiert:",
            characterAjaxError: "Du bist möglicherweise nicht mehr eingeloggt. Bitte aktualisiere die Seite und versuch es erneut.",
            ilvl: "Gegenstandsstufe",
            shortQuery: "Die Suchanfrage muss mindest zwei Buchstaben lang sein"
		},
		fansite: {
			achievement: 'Erfolg',
			character: 'Charakter',
			faction: 'Fraktion',
			'class': 'Klasse',
			object: 'Objekt',
			talentcalc: 'Talente',
			skill: 'Beruf',
			quest: 'Quest',
			spell: 'Zauber',
			event: 'Event',
			title: 'Titel',
			arena: 'Arenateam',
			guild: 'Gilde',
			zone: 'Zone',
			item: 'Gegenstand',
			race: 'Volk',
			npc: 'NPC',
			pet: 'Haustier'
		},
	};
//]]>
</script>
<!--[if lt IE 8]> 
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/jquery.pngFix.pack.js?v15"></script>
<script type="text/javascript">$('.png-fix').pngFix();</script>
<![endif]-->

<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/jquery-ui-1.8.6.custom.min.js?v15"></script>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/overlay.js?v15"></script>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/search.js?v15"></script>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/jquery.mousewheel.min.js?v15"></script>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/jquery.tinyscrollbar.min.js?v15"></script>

<? if(count($refreshCacheItems)){ ?>
<script type="text/javascript">


var _urls = [
<? echo '"'.implode('", '."\n".'"', $refreshCacheItems).'"'; ?>
];

function _loadUrl(count)
{
	if (count < _urls.length) {
		$.ajax({
			'url': _urls[count],
			'success': function() {
				_loadUrl(count + 1);
			}
		});
	} 
}
$(document).ready(function() {
	_loadUrl(0);
});
</script>
<? } ?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15760448-1']);
  _gaq.push(['_setDomainName', '.wow-alive.de']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
  </body>
</html>
