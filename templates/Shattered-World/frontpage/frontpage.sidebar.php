<?php 
if(!defined("INCLUDED"))
	die();

// Wir haben ja nur einen.
$server = $servers[0];
?>
	<div id="sidebar-realm-status" class="sidebar-module">
		<div class="sidebar-title">
			<h3>Server Information:	
			<?php if ($server['realm_status']){ ?>
				<span class="status-icon up">Online</span>
			<? } else { ?>
				<span class="status-icon">Offline</span>
			<? } ?>
			</h3>
		</div>
		<span class="clear"><!-- --></span>
		<?php if ($server['realm_status']){ ?>
			<span class="up">Uptime :</span> <span class="light"><?=$server["uptime"]?></span> <br>						
		<? } ?>
		Spieler Online Rekord: <?=$server["maxplayers"]?><br />
		WoW Alive Realmlist : <span class="light">Set Realmlist <?=$server['server_ip']?>:3704</span><br />
		WoW Alive Patch Version : <span class="light"><font color='#FF0000'>3.3.5 (12340)</font></span><br />
		Server Typ:&nbsp;<b>PvE/P</b><br/>
		
		<? if (isset($server['accounts'])){ ?>
			Anzahl Accounts: <span class="dark"><?=$server['accounts']?></span><br />
		<? } ?>
		<? if (isset($server['characters'])){ ?>
			Anzahl Charaktere: <span class="dark"><?=$server['characters']?></span><br />
		<? } ?>
		
			<? if (isset($server['rates'])){ ?>
		<b>Raten:</b>&nbsp;
		Ruf-, Quest- und Kill-Erfahrung <span class="light">3x Blizzlike</span><br />
		Drop-Raten außerhalb Endgame-Raids: <span class="light">3x Blizzlike</span><br />
		Drop-Raten bei Endgame-Raids: <span class="light">1x Blizzlike</span><br />
		<? } ?> 

		<? if ($server['moreinfo']){ ?>
			<a href="<?php echo url_for('server', 'info'); ?>"><?php echo $lang['more_info']; ?></a><br/>
		<?php } ?>

		<span class="clear"><!-- --></span>
	</div>
			
	<div id="sidebar-realm-status" class="sidebar-module">
		<div class="sidebar-title">
			<h3>Realm Status 
			<?php if ($server['realm_status']){ ?>
			<span class="up">Online</span>
			<?php } else { ?>
			<span class="down">Offline</span>
			<?php } ?>
			</h3>
		</div>
		<div id="box-online">
			<table width="100%" height="37" cellspacing="0" cellpadding="0">
			<tbody>
			<tr>
				<td width="40%" height="37">
					<center><img height="25" border="0" align="absmiddle" src="/<?=$currtmp?>/images/horde.png">&nbsp;&nbsp;<?=($server["horde_online"])?></center>
				</td>
				<td width="20%" height="37">
					<center><img height="15" border="0" align="absmiddle" src="/<?=$currtmp?>/images/employee.png" style="top: -2px; position: relative;">&nbsp;&nbsp;<?=ceil($server["gm_online"])?> </center>
				</td>
				<td width="40%" height="37">
					<center><img height="25" border="0" align="absmiddle" src="/<?=$currtmp?>/images/alliance.png">&nbsp;&nbsp;<?=($server["ally_online"])?></center>
				</td>  
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3"><a href="<?=url_for("server", "playersonline")?>">Spieler online Liste</a></td>
			</tr>
			<tr>
				<td colspan="3"><a href="<?=url_for("server", "playermap")?>">Spieler online Weltkarte</a></td>
			</tr>
			</tbody></table>
		</div>
		<? if(false){ ?>
		<div id="realm">
			<div class="horde"></div>
			<div class="alliance"></div>
			<div class="status-bar">
				<div class="horde-count" rel="tooltip" title="" style="width:<?=$server["horde_percent"]?>%">
					<div class="status-separator"></div>
				</div>
				<div class="alliance-count" rel="tooltip" title="" style="width:<?=$server["ally_percent"]?>%">
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<? } ?>
		<span class="clear"><!-- --></span>
	</div>
	
<? if (!empty($quicklinks)){ ?>
	<!-- QuickLinks -->
	<div id="sidebar-quicklinks" class="sidebar-module">
		<div class="sidebar-title">
			<h3><?php echo $lang['quicklinks'];?></h3>
		</div>
		<ul>
		<? 
		foreach ($quicklinks as $key => $array) { 
			$row_class = cycle($row_class, array("a", "e"));
			?>
			<li class="<?=$row_class?>"><a href="<?=$array[1]?>"><?=$array[0]?></a></li>  
			<?
		} ?>
		</ul>
		<span class="clear"><!-- --></span>
	</div>
<? } ?>
 

<!-- VoteLinks  --> 
<?php if(count($MW->getConfig->votelinks->vote) > 0){ ?>
	<div class="sidebar-module" id="sidebar-sotd">
		<div class="sidebar-title">
			<h3 class="title-sotd">
				<a href="<?=url_for("community","vote")?>">Vote um den Tag zu retten</a>
			</h3>
		</div>
	
		<div class="sidebar-content">
			<div class="sotd" style="background-image: url(/<?=$currtmp?>/images/vote.png);">
				<a href="<?=url_for("community","vote")?>" class="image"> </a>
				<div class="caption">
				<? if($userObject->can["vote"]){ ?>
					<span class="view">
						Voten ist: <font color="green">möglich!</font>
					</span>
					<a href="<?=url_for("community","vote")?>" class="submit">Jetzt voten gehen!</a>
					<span class="clear"><!-- --></span>
				<? } else { ?> 
					<span class="view">
						Voten ist: <font color="red">erst später wieder möglich!</font>
					</span>
					<span class="clear"><!-- --></span>
				<? } ?>
				</div>
			</div>
		</div>
		<span class="clear"><!-- --></span>		
	</div>
<? } ?>


<?php if ((int)$MW->getConfig->components->right_section->media): ?>
<div class="sidebar-module">
	<h3 style="height: 20px; color: #eff0ef; font-size: 12px; letter-spacing: 1px; font-weight: bold; width: 308px; padding: 1px 0 0 8px; font-family: 'Trebuchet MS', Verdana, Arial, sans-serif;"><?php echo $lang['random_screen']; ?></h3>
	<div id="innerrightbox">
	<?php if ($screen_otd): ?>
		<a href="images/screenshots/<?php echo $screen_otd; ?>" target="_blank"><img src="show_picture.php?filename=<?php echo $screen_otd; ?>&amp;gallery=screen&amp;width=282" width="282" alt="" style="border: 1px solid #333333"/></a>
		<select onchange="window.location = options[this.selectedIndex].value" style="width: 284px;">
			<option value=""><?php echo $lang['galleries']; ?> -&gt;</option>
			<option value="index.php?n=media&sub=screen"><?php echo $lang['GallScreen']; ?></option>
			<option value="index.php?n=media&sub=wallp"><?php echo $lang['GallWalp']; ?></option>
		</select>
	<?php else: ?>
		No Screenshots in database;
	<?php endif; 
	unset($screen_otd); // Free up memory. ?>
	</div>
</div>
<?php endif; ?>

	<div class="sidebar-module">
		<div class="sidebar-title">
			<h3>Aktionen</h3>
		</div>

<?php if($MW->getConfig->werbe_spieler_aktion > 0): ?>
	<!-- Werbe Freund Section -->
		<div class="sidebar-content">
			<div class="bnet-offer">
				<div class="bnet-offer-bg">
					<a href="<?=url_for("server","fwfv")?>"><img src="/<?=$currtmp?>/images/recruit.jpg" width="300" height="100" alt=""></a>
				</div>
				<div class="desc">
					<a href="<?=url_for("server","fwfv")?>">Wirb einen Freund</a>
					<div class="subtitle">Hol deine Freunde her und spielt gemeinsam kostenlos auf Alive!</div>
				</div>
			</div>
		</div>
<?php endif; ?>
		<div class="sidebar-content">
			<div class="bnet-offer">
				<div class="bnet-offer-bg">
					<a href="<?=url_for("server","ta")?>"><img src="/<?=$currtmp?>/images/transfer.jpg" width="300" height="100" alt=""></a>
				</div>
				<div class="desc">
					<a href="<?=url_for("server","ta")?>">Transferier einen Charakter</a>
					<div class="subtitle">Hol deine Charaktere von deinem alten Servern zu uns.</div>
				</div>
			</div>
		</div>
		<span class="clear"><!-- --></span>
	</div>

	<div class="sidebar-module" id="sidebar-forums">
		<div class="sidebar-title">
			<h3 class="title-forums"><a href="forum.php">Letzte Forendiskussionen</a></h3>
		</div>

		<div class="sidebar-content poptopic-list">
		<? echo $posts_string; ?>
		</div>
	</div>
	
	<div class="sidebar-module" id="vote-links">
		<div class="sidebar-title">
			<h3 class="title-forums">Toplisten</h3>
		</div>
		<div class="sidebar-content poptopic-list" style="text-align:center">
			<a href="http://mmotoplist.de/vote/page/265/" target="_blank"><img src="http://mmotoplist.de/vote/button2/265/" alt="Vote Site down" border="0" /></a>
			<a href="http://www.wowstatus-german.de/" onmousedown="return hit('http://www.wowstatus-german.de/in.php?id=3')" target="_blank"> </a>
			<a href="http://mpogtop.com/in.php/1268428407" target="_blank"><img src="http://mpogtop.com/images/mpogtop8831.jpg" alt="Vote Site down" border="0" /></a>
			<a href="http://wowtop.de/index.php?a=in&u=Erdgeist" target="_blank"><img src="http://wowtop.de/Erdgeist_banners8.gif" alt="WoWTop.de" border="0" /></a>
			<a href="http://toplist.clan-xf.de/index.php?a=in&u=Erdgeist" target="_blank"><img src="http://toplist.clan-xf.de/images/voteus.gif" alt="Vote Site down" border="0" /></a>
			<a href="http://www.gamesitestop100.com/in.php?site=16184" target="_blank"><img src="http://www.gamesitestop100.com/images/votebutton.jpg" alt="Vote Site down" border="0" /></a>
			<a href="http://www.gamesiteguide.com/index.php?page=in&id=549" target="_blank"><img src="http://www.gamesiteguide.com/index.php?page=button&id=182" alt="Vote Site down" border="0" /></a>
			<a href="http://www.wow-status-german.de/" onmousedown="return hit('http://www.wow-status-german.de/in.php?id=24')" target="_blank"><img src="http://www.wow-status-german.de/img/banner.gif" alt="WoW Privat Server" border="0"></a>
			<a href="http://wowmania.gotop100.com/in.php?ref=600" target="_blank"><img src="http://wowmania.gotop100.com/lists/wowmania/custombanners/70634.gif" alt="Vote Site down" border="0" /></a>
			<a href="http://wow-portal.eu/toplist.php?mode=in&id=162" target="_blank"><img src="http://wow-portal.eu/image.php?mode=toplist&design=3&id=162" alt="Vote Site down" border="0"></a>
			<a href="http://www.private-wow-server.de/"; onmousedown="return hit('http://www.private-wow-server.de/in.php?id=239')" target="_blank"></a>
			<a href="http://www.private-wow-server.de/in.php?id=239" target="_blank"><img src="http://www.private-wow-server.de/img/wow_kl2.gif" alt="Vote Site down" border="0"></a>
			<a href="http://free-wow-server.com/" target="_blank"><img src="http://free-wow-server.com/button.php?u=Erdgeist123" alt="Vote Site down" border="0" /></a>
			<a href="http://www.mmorpgtoplist.com/in.php?site=36112" target="_blank"><img src="http://www.mmorpgtoplist.com/vote.jpg"  alt="Vote Site Down" border="0"></a>
			<a href="http://www.wowtopserver.de/" onmousedown="return hit('http://www.wowtopserver.de/in.php?id=794')" target="_blank"><img src="http://www.wowtopserver.de/img/banner.jpg" alt="Vote Site Down" border="0"></a>
			<a href="http://www.best-topliste.com/" target="_blank"><img src="http://www.best-topliste.com/button.php?u=erdgeist" alt="Vote Site Down" border="0" /></a>
		</div>
	
	</div>
