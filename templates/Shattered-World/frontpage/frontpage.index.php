<style media="screen" title="currentStyle" type="text/css">

img.feature {
	margin:0px;
	background-color:#fff;
	padding:2px;
	border:1px solid #ccc;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	
	-webkit-box-shadow: 0px 5px 8px 0px #4a4a4a;
    -moz-box-shadow: 0px 5px 8px 0px #4a4a4a;
    box-shadow: 0px 5px 8px 0px #4a4a4a;
	
	position: relative;
	left: -4px;
	top: -7px;
}


</style>
<script language="JavaScript">
	function hit(hitlink) {
		if(document.images) {
			(new Image()).src=''+hitlink+'';
		}
		return true;
	}
</script>

<?php 
/*$banner = (int)$MW->getConfig->generic->display_banner_flash ? 1 : 0;
if ($banner):
?>
<?php else: ?>
    <img src="<?php echo $currtmp; ?>/images/banner1.gif" alt="" width="467"/><br />
<?php endif;
*/
 ?>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/slideshow.js"></script>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/swfobject.js"></script>

	<div id="slideshow">
		<div class="container">
		<?
		$first = true;
		for($i = 0; $i < count($features); $i++){ ?>
			<div class="slide" id="slide-<?=$i?>" 
				style="background-image: url('http://portal.wow-alive.de/images/slideshow/<?=$features[$i]["id"]?>.jpg'); <?=($first ? '' : 'display: none;')?>"> 
			</div>
		<?
			$first = false;
		} ?>
		</div>
		
		<div class="paging">
		<? for($i = 0; $i < count($features); $i++){ ?>
			<a href="javascript:;" id="paging-<?=$i?>" onclick="Slideshow.jump(<?=$i?>, this);" onmouseover="Slideshow.preview(<?=$i?>);" class="current"></a>
		<? } ?>
		</div>
		
		<div class="caption">
			<h3><a href="#" class="link"><?=$features[0]["title"]?></a></h3>
			<?=$features[0]["desc"]?>
		</div>
		
		<div class="preview"></div>
		<div class="mask"></div>
	</div>

	<script type="text/javascript">
	//<![CDATA[
        $(function() {
            Slideshow.initialize('#slideshow', [
        <? for($i = 0; $i < count($features); $i++){ ?>
                {
                    image: "http://portal.wow-alive.de/images/slideshow/<?=$features[$i]["id"]?>.jpg",
                    desc: "<?=$features[$i]["desc"]?>",
                    title: "<?=$features[$i]["title"]?>",
                    url: "<?=$features[$i]["url"]?>",
					id: "<?=$features[$i]["id"]?>"
                },
        <? } ?>
            ]);

        });
	//]]>
	</script>
	
<div class="featured-news">
	<div class="featured">
		<a href="<?=url_for("server","howtoplay")?>">
		   <span class="featured-img" style="background-image: url('http://forum.wow-alive.de/images/cata/cms/3ASY9WFAIF531293443430148.jpg');"></span>
		   <span class="featured-desc"> Wie spiele ich auf Alive?</span>
		</a>
	</div>

	<div class="featured">
		<a href="<?=url_for("account","register")?>">
		   <span class="featured-img" style="background-image: url('http://forum.wow-alive.de/images/cata/cms/ZG6WXIEA9W251288953352081.jpg');"></span>
		   <span class="featured-desc"> Account erstellen</span>
		</a>
	</div>

	<div class="featured">
		<a href="<?=url_for("server","ta")?>">
		   <span class="featured-img" style="background-image: url('http://forum.wow-alive.de/images/cata/cms/W0A8FMN4RTGW1290608599461.jpg');"></span>
		   <span class="featured-desc"> Transferanleitung</span>
		</a>
	</div>

	<span class="clear"></span>
</div>

<div id="news-updates">
	<div align="center" style="font-family: Georgia;"><font size="+1">
		<marquee direction="left" loop="500" width="98%"><strong><?php echo $shortinfo['shortinfo'] ?></strong></marquee>
	</font></div>
	
	<div class="news-article first-child">
		<h3 align ="center">Herzlich Willkommen auf WoW-Alive.</h3>
		
		<h4 align="center">
			<span class="byline" style="font-size: 20px;"><a>3.3.5a Server</a></span><br>
			Set Realmlist 178.63.89.20:3704<br>
			Teamspeak: 213.239.214.203:9987 passwort: wow-alive<br>
		</h4>
              <br>
		<p>Unser Realm Norganon (Blizzlike) steht für dich bereit. Die Instanzen und Bosse sind gescriptet und die meisten der Nordend-Instanzen funktionieren. Fast alle Spells und die Dualskillung funktionieren einwandfrei. <br/>

		Wir arbeiten bei uns auf dem Server hauptsächlich mit Tickets und wenn kein GM zu sehen ist, benutz trotzdem diese Funktion. Aber selbstverständlich kannst du jederzeit einen GM ingame anwisphern. Unser Server arbeitet mit Addons wie mit Mobmap, Questhelper 1.3.10, Gatherer 3.1.14 und noch vielen mehr, problemlos zusammen.</p>

		<p>Jede Woche finden mehrere Events statt, die durch unsere Eventmanager geplant und durchgeführt werden! An Feiertagen gibt es auch Spezialevents. Diese können die verschiedensten Formen annehmen. Von Überfällen auf Städte, über kleine Quizrunden, bis zu einer kompletten Instanz.</p>

		<p>Desweiteren bieten wir euch und euren Gilden einen eigenen TS-Channel mit Subchannel nach Wahl.<br>
		Hier findet ihr sowohl Ruhe als auch Freunde in eurem gildeneigenen, passwortgeschützten Channel, sowie Räume zum Raiden und Reden ausserhalb von Gilden und eine GameMaster-Zone, in der ihr Fragen oder Probleme vorbringen könnt.(GameMaster sind im TeamSpeak an dem (S) hinter ihrem Namen zu erkennen.)</p>

		<p>Wir führen Charakter-Transfere von offiziellen oder low- bis mid-rate Servern durch und leiten auch die ein oder andere Werbeaktion mit sehr interessanten Belohnungen.</p>

		<p>In unserem Team findest du freundliche GM's, kompetente Coder und Admins. Wir sind jeden Tag mehrmals ingame anzutreffen, um bei Problemen oder Fragen weiterzuhelfen.</p>
	</div>

	<? if(!empty($news_string)){ echo $news_string; } ?>

	<div class="blog-paging">
		<a class="ui-button button1 button1-next float-right" href="http://forum.wow-alive.de/forumdisplay.php?f=16"><span><span>Mehr News</span></span></a>
		<span class="clear"> </span>
	</div>
	<span class="clear"><!-- --></span>
</div> <!-- /news-updates -->


<script type="text/javascript">
<!--
    var position = <?php echo$postnum;?>;
    var localId = postId<?php echo$postnum;?>;
    var cookieState = getcookie("news"+localId);
    var defaultOpen = "<?php echo($postnum > (int)$MW->getConfig->generic_values->news->defaultOpen?'0':'1');?>";
    if ((cookieState == 1) || (position==1 && cookieState!='0') || (defaultOpen == 1 && cookieState!='0')) {
    } else {
        document.getElementById("news"+localId).className = "news-collapse"+"<?php echo$hl;?>";       
    }
--></script>
