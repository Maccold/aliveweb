<?php 
builddiv_start(1, "GM Transfer");
?>
<style type="text/css">
textarea{
	width:500px;
	overflow:auto;
}
pre{
	border: 1px solid white;
	padding: 10px 20px;
	margin-left: 50px;
}
b, strong{ color:white;}
h2{ padding: 10px 0px; color:#F0E29A;}
</style>

<script type="text/javascript">
$(document).ready(function() {
	$("textarea").click(function(){
    	// Select field contents
    	this.select();
	});
});
</script>
<script type="text/javascript" src="http://static.wowhead.com/widgets/power.js"></script>

<? write_subheader("Transfer ID ".$transferId); ?>

<br>
<? if(!empty($message)) echo "<p><b>$message</b></p><br>"; ?>
<p><b>Aktueller Status:</b><br /><?=$basedata["gm"]?></p>
<br>

<? write_subheader("WoW-Alive Charakter-Transfer-Skript"); ?>

<b>Grunddaten:</b><br><br>

<p>
	&Uuml;berpr&uuml;ft den Alten Server genau ob er Blizzlike ist, durchforstet die HP und ggf das Forum!
		Sollte der Link zum Alten Server fehlen muss der Spieler diesen Nachreichen, da er beweisen muss das der letzte Server Blizzlike war!
		Wichtig: Wenn der Spieler von Blizzard kommt ist der Armorylink absolutes pflicht (damit ist der Link zum Armory von Blizzard gemeint.
		Ladet euch die Screenshots runter, solltet ihr Probleme mit dem Download von z.B Rapidshare haben bittet den Spieler, das Packet woanders hochzuladen! Wichtig schaut dir die Screenshots genau an, wichtig sind vor allem Played-Time (sollte bei Level 80 so bei 5 Tagen liegen) und der Login Screenshot.
		Erstellt den Charakter gem&auml;&szlig; Screens (Rasse etc), portet euch zur <b>.tele transferinsel</b> und macht den Transfer. Als letztes gibst du <b>.character customize</b> ein und logg dich aus. <br />
    Gehe nun auf das Portal und verschiebe den Charakter auf den Spieleraccount
</p><br>

<? write_subheader("Transferanzahl des Accounts: <?=$transferCount?> St&uuml;ck insgesamt"); ?><br />
<? 
if($transferCount > 0){
	?>
<div class="table">
<table width="100%">
<tr>
	<th>ID</th>
	<th>Char</th>
	<th>Server</th>
	<th>Status</th>
</tr>
<? foreach($other_transfers as $transfer){ ?>
<tr class="row1">
	<td><a href="<?=url_for("admin","transfer-detail", array("id" => $transfer["id"]))?>" target="_blank"><?=$transfer["id"]?></a></td>
	<td><?=$transfer["char"]?></td>
	<td><?=$transfer["server"]?></td>
	<td><?=$transfer["gm"]?></td>
</tr>
<? } ?>
</table>
</div>
<br /><br />
	<?	
}
?>


<?
			echo "Formular ID: ".$transferId."<br>";
			echo "ICQ: ".$basedata['icq']."<br>";
			echo "Account-ID: ".$basedata['name']."<br>";
			echo "Charactername: ".$basedata['char']."<br>";
			echo "Rasse: ".$basedata['rasse']."<br>";
			echo "Klasse: ".$basedata['klasse']."<br>";
			echo "Alter Server: ".$basedata['server']."<br>";
			echo "Server Link: <a href=\"".$basedata['serverlink']."\">".$basedata['serverlink']."</a><br>";
			echo "Armory Link: <a href=\"".$basedata['armorylink']."\">".$basedata['armorylink']."</a><br>";
			echo "Screenpaket: <a href=\"".$basedata['screen']."\">".$basedata['screen']."</a><br><br>";
     	echo "Bemerkung: ".$basedata['bemerkung']."<br><br>";
write_subheader("Basics</b>");

			$level = $basedata['level'] - 1;
	echo "<table border='2' cellspacing='1'>
	<tr><td>
	Wichtig: Der hier angegebene Levelup-Befehl setzt voraus dass der Spieler Level 1 ist.
	</td></tr>							  
	</table>";
	?>
<br>
Level, Lerngeld und Taschen:<br />
<pre rows="5">
.levelup <?php echo $level; ?>

.modify money 2000000000
.char customize
.add 21841 4
</pre><br />
<br />
Nach dem Lernen:<br />
<pre>
.modify money -2000000000
</pre><br />
<br />
<br />Geld: Jetzt erst den Orginal Betrag adden, falls dieser 100000000 uebersteigt liegt es in eurem Ermessen ob ihr dem Spieler ueberhaupt Gold addet. <br />
<?
		$gold = $basedata['gold']."0000";
		if($gold > 100000000)
			echo '<pre style="color:#C00">.modify money '.$gold.'</pre>';	
		else
			echo '<pre>.modify money '.$gold.'</pre>';	
		
?>
<br/>
<? write_subheader("Berufe & Reiten lernen:"); ?><br>

Reiten, <?=$basedata['beruf1']?>, <?=$basedata['beruf2']?>, Kochen, Angeln und Erste Hilfe.

<pre>
.learn <?=$reitenspell?> 

.learn <?=$beruf1?> 
.setskill <?=$beruf1skill?> <?=$basedata['beruf1skill']?> 450

.learn <?=$beruf2?> 
.setskill <?=$beruf2skill?> <?=$basedata['beruf2skill']?> 450

.learn 2550 
.setskill 185 <?=$basedata['kochen']?> 450

.learn 7620 
.setskill 356 <?=$basedata['angeln']?> 450

.learn 3279 
.setskill 129 <?=$basedata['erstehilfe']?> 450
</pre>
<br>
<strong>Zusatzmaterial:</strong><br>
Falls Juwelenschleifen auf 450 ist: <br>
<pre>.add 41596 50</pre>
Falls Verzauberkunst auf 450 ist: <br>
<pre>.add 34052 40</pre>
Falls Inschrifter auf 450 ist: <br>
<pre>.add 45912 10</pre>
Falls Lederverarbeitung auf 450 ist: <br>
<pre>.add 38425 30</pre>	
Falls Kochkunst auf 450 ist: <br>
<pre>.add 43016 20</pre><br>

Zusatzmaterial Sammelmakro: (wenns leer ist trifft auch nichts zu)<br />
<pre>
<? 
	if($juweMax)
		echo '.add 41596 50';
	if($vzMax)
		echo '.add 34052 40'."\n";
	if($ikMax)
		echo '.add 45912 10'."\n";
	if($lederMax)
		echo '.add 38425 30'."\n";
	if($kochMax)
		echo '.add 43016 20'."\n"; 
?>
</pre>
<br />
<br />

<?
//Items
?>
<table border='2' cellspacing='1'>
	<tr><td>
	&Uuml;berpr&uuml;ft bei den Items ob diese Itemlevel 245 &uuml;berschreiten sollte dies der Fall sein, soll der Spieler sich ein neues Item rausuchen
	</td></tr>
	</table><br>

<table border='2' cellspacing='2'>
	<tr><td><?php echo 'Kopf:<br>
                      Hals:<br>
                      Schultern:<br>
                      Ruecken:<br>
                      Brust:<br><br>
                      Wappenrock:<br>
                      Handgelenk:<br>
                      Haende:<br>
                      Taille:<br>
                      Beine:<br><br>
                      Schuhe:<br>
                      Ring1:<br> 
                      Ring2:<br>
                      Schmuck1:<br>
                      Schmuck2:<br><br>
                      Waffenhand:<br>
                      Nebenhand:<br>
                      Distanzwaffe / Siegel usw:<br>'?>
          </td>
		  <td valign="top"><?=$itemLevelOutput?></td>
          <td valign="top"><?=$itemIdList?></td>
	</tr>
</table><br/>
<b>Equip Items Makro</b>
<pre>
<?=$itemOutput?>
</pre>

<br><b><h3>MOUNTS</h3></b><br>
<pre>
.add <a href="http://de.wowhead.com/?item=<?=$items['mount_b']?>"><?=$items['mount_b']?></a><br>.add <a href="http://de.wowhead.com/?item=<?=$items['mount_f']?>"><?=$items['mount_f']?></a>
</pre>
<br>
<br><h3><strong>Random Itemsadds</strong></h3><br>
<? if( count($randomItems) > 0 ){ ?>
<pre><? foreach($randomItems as $randomItem){ ?>
.add <a href="http://de.wowhead.com/?item=<?=$randomItem?>"><?=$randomItem?></a>
<? } ?></pre>
<? } else { ?>
	<p>- Keine -</p>
<? } ?>
<br>
<br><h3><strong>Ruf</strong></h3><br>

Bitte denk an den &uuml;bergesetzten Ruf!<br><br>
<table border='2' cellspacing='2'><tr><td>
	<b>Classic</b><br>
    <b>Allianz</b><br>
     Sturmwind:<br>
	   Die Exodar<br>
		 Eisenschmiede<br>
     Gnomeregangnome<br>
     Darnassus<br>
    <br><b>Horde</b><br>
     Orgrimmar<br>
     Silbermond<br>
     Unterstadt<br>
     Donnerfels<br>
     Dunkelspeertrolle<br><br>
    <b>The Burning Crusade</b><br>
     Das Konsortium<br>
     Das Violette Auge<br>
     Die Todeshoerigen<br>
     Die Waechter der Sande<br>
     Ehrenfeste<br>
     Expedition des Cenarius<br>
     Hueter der Zeit<br>
     Kurenai<br>
     Netherschwingen<br>
     Ogrila<br>
     Sporeggar<br>
     Thrallmar<br><br>
    
     <b>Wrath of the Lich King</b><br>
     <b>Vorposten der Allianz</b><br>
     Der Silberbund<br>
     Die Frosterben<br>
     Expedition Valianz<br>
     Forscherliga<br><br>
     
     <b>Expedition der Horde</b><br>
     Die Hand der Rache<br>
     Die Sonnenhaescher<br>
     Die Taunka<br>
     Kriegshymnenoffensive<br><br>
    
     Argentumkreuzzug<br>
     Der Wyrmruhpakt<br>
     Die Kaluak<br>
     Die Orakel<br>
     Die Soehne Hodirs<br>
     Kirin Tor<br>
     Ritter der Schwarzen Klinge<br>
     Shendralar<br>
     Stamm der Wildherzen<br>
     Das &auml;scherne Verdikt
     </td><td>
     <br><b>.modify rep <a href="http://de.wowhead.com/?faction=469"> 469</a> <?=$sql_ruf_allianz1?></b><br>
     .modify rep <a href="http://de.wowhead.com/?faction=72"> 72</a> <?=$ruf['Sturmwind']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=930"> 930</a> <?=$ruf['Die_Exodar']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=47"> 47</a> <?=$ruf['Eisenschmiede']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=54"> 54</a> <?=$ruf['Gnomeregangnome']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=69"> 69</a> <?=$ruf['Darnassus']?><br> 
    <br>
    <b>.modify rep <a href="http://de.wowhead.com/?faction=67"> 67</a> <?=$sql_ruf_horde1?></b><br>
		 .modify rep <a href="http://de.wowhead.com/?faction=76"> 76</a> <?=$ruf['Orgrimmar']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=911"> 911</a> <?=$ruf['Silbermond']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=68"> 68</a> <?=$ruf['Unterstadt']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=81"> 81</a> <?=$ruf['Donnerfels']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=530"> 530</a> <?=$ruf['Dunkelspeertrolle']?><br> 
	  <br><br>  
		 .modify rep <a href="http://de.wowhead.com/?faction=933"> 933</a> <?=$ruf['Das_Konsortium']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=967"> 967</a> <?=$ruf['Das_Violette_Auge']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1012"> 1012</a> <?=$ruf['Die_Todeshoerigen']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=990"> 990</a> <?=$ruf['Die_Waechter_der_Sande']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=946"> 946</a> <?=$ruf['Ehrenfeste']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=942"> 942</a> <?=$ruf['Expedition_des_Cenarius']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=989"> 989</a> <?=$ruf['Hueter_der_Zeit']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=978"> 978</a> <?=$ruf['Kurenai']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1015"> 1015</a> <?=$ruf['Netherschwingen']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1038"> 1038</a> <?=$ruf['Ogrila']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=970"> 970</a> <?=$ruf['Sporeggar']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=947"> 947</a> <?=$ruf['Thrallmar']?><br> 
		 
	  <br><br><b>.modify rep <a href="http://de.wowhead.com/?faction=1037"> 1037</a> <?=$sql_ruf_allianz2?><br></b>
	  
     .modify rep <a href="http://de.wowhead.com/?faction=1094"> 1094</a> <?=$ruf['Der_Silberbund']?><br>
		 .modify rep <a href="http://de.wowhead.com/?faction=1126"> 1126</a> <?=$ruf['Die_Frosterben']?><br>
     .modify rep <a href="http://de.wowhead.com/?faction=1050"> 1050</a> <?=$ruf['Expedion_Valianz']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1068"> 1068</a> <?=$ruf['Forscherliga']?><br> 
		 
		<br><b>.modify rep <a href="http://de.wowhead.com/?faction=1052"> 1052</a> <?=$sql_ruf_horde2?><br></b> 
		
     .modify rep <a href="http://de.wowhead.com/?faction=1067"> 1067</a> <?=$ruf['Die_Hand_der_Rache']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1124"> 1124</a> <?=$ruf['Die_Sonnenhaescher']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1064"> 1064</a> <?=$ruf['Die_Taunka']?><br>
		 .modify rep <a href="http://de.wowhead.com/?faction=1085"> 1085</a> <?=$ruf['Kriegshymnenoffensive']?><br><br> 
          
		 .modify rep <a href="http://de.wowhead.com/?faction=1106"> 1106</a> <?=$ruf['Argentumkreuzung']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1091"> 1091</a> <?=$ruf['Der_Wyrmruhpakt']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1073"> 1073</a> <?=$ruf['Die_Kalu_ak']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1105"> 1105</a> <?=$ruf['Die_Orakel']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1119"> 1119</a> <?=$ruf['Die_Soehne_Hodir']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1090"> 1090</a> <?=$ruf['Kirin_Tor']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1098"> 1098</a> <?=$ruf['Ritter_der_schwarzen_Klinge']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=809"> 809</a> <?=$ruf['Shen_dralar']?><br> 
		 .modify rep <a href="http://de.wowhead.com/?faction=1104"> 1104</a> <?=$ruf['Stamm_der_Wildherzen']?><br> 
     .modify rep <a href="http://de.wowhead.com/?faction=1156"> 1156</a> <?=$ruf['Verdikt']?><br>
		 </td></tr>
</table>
<br><br>



<h2>Wenn das Formular noch in Bearbeitung ist:</h2>

<form action="<?=url_for("admin","transfer-detail", array("id" => $transferId))?>" method="POST">
<input type="hidden" name="transfer-status" value="inprogress">
<p>In Bearbeitung von:<br/><input type="text" name="text" size="70"><br/>
<button type="submit">In Arbeit</button>
</form>

<h2>Wenn das Formular fertig transferiert ist:</h2>

<form action="<?=url_for("admin","transfer-detail", array("id" => $transferId))?>" method="POST">
<input type="hidden" name="transfer-status" value="done">
<p>Erledigt von:<br/><input type="text" name="text" size="70"><br/>
<button type="submit">Erledigt</button>
</form>

<h2>Wenn das Formular ung&uuml;ltig ist und nicht transferiert wird:</h2>

<form action="<?=url_for("admin","transfer-detail", array("id" => $transferId))?>" method="POST">
<input type="hidden" name="transfer-status" value="delete">
<p>Als gel&ouml;scht markiert von:<br/><input type="text" name="text" size="70"><br/>
<button type="submit">Diese Anfrage als gel&ouml;scht markieren</button>
</form>

<? builddiv_end(); ?>