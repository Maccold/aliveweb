<?php 
builddiv_start(0, "GM Transfer");

?>
<style type="text/css">
.inprogress td:first-child,
.table tbody tr.inprogress:hover td:first-child{
	background-color:#F60;
	padding:0px;
}
.done td:first-child,
tbody tr.done:hover td:first-child{
	background-color:#0C0;
	padding:0px;
}
.deleted td:first-child,
tbody tr.deleted:hover td:first-child{
	background-color:#C00;
	padding:0px;
}

.table thead th {
	padding: 0;
	background: #4D1A08 url("/templates/Shattered-World/images/table-header.gif") 0 100% repeat-x;
	border-bottom: 1px solid #1A0F08;
	border-left: 0px solid #7C2804;
	border-right: 0px solid #391303;
	border-top: 0px solid #7C2804;
	white-space: nowrap;
}
.table thead th span { padding-left: 10px; }
.wiki .related { background: none; }
</style>

<? write_subheader("GM Transferbereich von WoW-Alive"); ?>

<p><font size="+1">Der Spieler sollte euch ingame mitteilen k&ouml;nnen welche ID er bekommen hat!</font></p>

<div class="wiki"><div class="related">
<span class="clear"><!-- --></span> 
<div class="related-content" id="related-loot">
	<div class="filters inline">
		<div class="keyword"> <span class="view"></span> <span class="reset" style="display: none"></span>
			<input id="filter-name-loot" type="text" class="input filter-name" data-filter="row" maxlength="25" title="Filter..." value="Filter..." />
		</div>
		<div class="filter-tabs"> 
			<a href="javascript:;" data-filter="column" data-column="0" data-value="" data-name="type" class="tab-active"> Alle </a> 
			<a href="javascript:;" data-filter="column" data-column="0" data-name="type" data-value="0"> Offen </a> 
			<a href="javascript:;" data-filter="column" data-column="0" data-name="type" data-value="1"> In Bearbeitung </a> 
			<a href="javascript:;" data-filter="column" data-column="0" data-name="type" data-value="2"> Erledigt </a> 
			<a href="javascript:;" data-filter="column" data-column="0" data-name="type" data-value="3"> Abgewiesen </a> 
		</div>
		<span class="clear"><!-- --></span> 
	</div>
	<div class="data-options-top">
		<div class="table-options data-options ">
			<div class="option">
				<ul class="ui-pagination"></ul>
			</div>
			Zeige <strong class="results-start">1</strong>–<strong class="results-end">50</strong> von <strong class="results-total"><?=$transferCount?></strong> Ergebnissen <span class="clear"><!-- --></span> 
		</div>
	</div>
	<div class="table full-width">
		<table>
			<thead>
				<tr>
					<th> <a href="javascript:;" class="sort-link"> <span class="arrow">Status</span> </a> </th>
					<th class="align-center"> <a href="javascript:;" class="sort-link numeric default"> <span class="arrow">TransferID</span> </a> </th>
					<th class="align-center"> <a href="javascript:;" class="sort-link numeric"> <span class="arrow">AccountID</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link"> <span class="arrow">Server</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link numeric"> <span class="arrow">Erstellungsdatum</span> </a> </th>
					<th> <a href="javascript:;" class="default"> <span>Bemerkung</span> </a> </th>
				</tr>
			</thead>
			<tbody>
<?php foreach($transferRows as $transfer_id => $transfer) { ?>
<tr class="<?=$transfer["classes"]?>">
	<td data-raw="<?=$transfer["status_id"]?>"> &nbsp; </td>
	<td class="align-center" data-raw="<?=$transfer_id?>"> <a href="<?=$transfer["detail_link"]?>" target="_blank"><?=$transfer_id?></a> </td>
	<td class="align-center" data-raw="<?=$transfer['name']?>"> <?=$transfer['name']?> </td>
	<td data-raw="<?=$transfer['server']?>"> <?=$transfer['server']?> </td>
	<td data-raw="<?=$transfer['datum']?>"> <?=$transfer['datum']?> </td>
	<td data-raw="<?=$transfer['gm']?>"> <?=$transfer['gm']?> </td>
</tr>
<? } ?>
			</tbody>
		</table>
	</div>
	<div class="data-options-bottom">
		<div class="table-options data-options ">
			<div class="option">
				<ul class="ui-pagination">
				</ul>
			</div>
			Zeige <strong class="results-start">1</strong>–<strong class="results-end">50</strong> von <strong class="results-total"><?=$transferCount?></strong> Ergebnissen <span class="clear"><!-- --></span> </div>
	</div>
</div>	
<script type="text/javascript">
//<![CDATA[
$(function() {
	Wiki.pageUrl = '/admin/transferlist2/';
});
//]]>
</script> 
<script type="text/javascript" src="/<?=$currtmp?>/js/wiki.js?v2"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/zone.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/table.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/filter.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/lightbox.js"></script>

<script type="text/javascript">
//<![CDATA[
$(function() {
	Wiki.related['loot'] = new WikiRelated('loot', {
		paging: true,
		totalResults: <?=$transferCount?>,
			column: 1,
			method: 'default',
			type: 'desc'
	});
});
//]]>
</script> 

</div></div>

<?php builddiv_end() ?>
