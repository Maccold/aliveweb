<style type="text/css">
.talentcalc-cell .icon .texture { background-image: url(http://eu.battle.net/wow-assets/static/images/talents/icons/<?=$char->GetClass()?>-greyscale.jpg); }
</style>		
<div id="profile-wrapper" class="profile-wrapper profile-wrapper-<?=$char->GetCssFaction()?>">
	<div class="profile-sidebar-anchor">
		<div class="profile-sidebar-outer">
			<div class="profile-sidebar-inner">
				<div class="profile-sidebar-contents">
					<?php echo $sidebar_character; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-contents">
		<div class="profile-section-header">
			<ul class="profile-tabs">
			<? foreach($talentSpecs as $n => $spec){?>
				<li class="<? if($spec["shown"]){ echo "tab-active";}?>">
					<a href="<?=$char->GetCharacterLink()?>/talent/<?=$spec["type"]?>" rel="np">
						<span class="r"><span class="m">
							<span class="icon-frame frame-14 ">
								<img src="<?=$spec["icon"]?>" alt="" width="14" height="14"/>
							</span>
							<? if($spec["active"]){?><span class="active-spec"></span><? } ?>
							<?=$spec["typeText"]?> 
						</span></span>
					</a>
				</li>
			<?php }?>
			</ul>
		</div>
		<div class="profile-section">
			<div class="character-talents-wrapper">
				<div id="talentcalc-character" class="talentcalc talentcalc-locked">
					<?php foreach ($talentTabs as $specId => $tab){ ?>
					<div class="talentcalc-tree-wrapper <? if($tab["n"] == 2){echo "tree-last";}?> <?=(($tab["active"])?"tree-specialization":"tree-nonspecialization")?>">
						<div class="talentcalc-tree-button" style="display: none; ">
							<button class="ui-button button1" type="submit">
							<span>
							<span>Heilig</span>
							</span>
							</button>
						</div>
						<div class="talentcalc-tree-header" style="visibility: visible; ">
							<span class="icon">
								<span class="icon-frame-treeheader ">
									<img src="<?=$tab["icon"]?>" alt="" width="36" height="36"/>
									<span class="frame"></span>
									<span class="roles">
									<?php if($tab["role"] == "tank-dps"){?>
										<span class="icon-dps"></span>
										<span class="icon-tank"></span>
									<?php }else{?>
										<span class="icon-<?=$tab["role"]?>"></span>
									<?php }?>
									</span>
								</span>
							</span>
							<span class="points"><?=$tab["points"]?></span>
							<span class="name"><?=$tab["name"]?></span>
							<span class="clear"><!-- --></span>
						</div>
						<div class="talentcalc-tree" style="width: 210px; height: 580px; background-image: url(/images/character/talent-calculator/backgrounds/<?=$char->GetClass()."-".$tab["n"]?>); background-position: -0px 0">
							<div class="talentcalc-cells-wrapper">
							<?php foreach ($talentCells[$tab["n"]] as $cell){?>
								<div class="talentcalc-cell" style="left: <?=($cell["Col"]*53)?>px; top: <?=($cell["Row"]*53)?>px;" data-id="<?=$cell["spell"]?>">
									<span class="icon">
										<span class="texture" style="background-position: -0px -0px;"></span>
										<span class="frame"></span>
									</span>
									<a href="javascript:;" class="interact"><span class="hover"></span></a>
									<span class="points"><span class="frame"></span><span class="value">0</span></span>
								</div>
							<?php }?>
							</div>
						</div>
						<div class="talentcalc-tree-overview" style="width: 228px; height: 387px; display: none; ">
							<div class="icon-wrapper">
								<span class="icon" style="-moz-box-shadow: 0px 0px 90px #ff7f00; -webkit-box-shadow: 0px 0px 90px #ff7f00; box-shadow: 0px 0px 90px #ff7f00;">
								<span class="texture" style="background-image: url(http://eu.media.blizzard.com/wow/icons/56/spell_holy_holybolt.jpg);"></span>
								<span class="frame"></span>
								</span>
							</div>
							<ul class="spells" style="width: 208px">
								<li class="is-ability" data-spell="20473">
								<span class="icon">
								<span class="icon-frame-ability ">
								<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_searinglight.jpg" alt="" width="36" height="36"/>
								<span class="frame"></span>
								</span>
								</span>
								<span class="name">Heiliger Schock</span>
								<span class="clear">
								<!-- -->
								</span>
								</li>
								<li data-spell="85102">
								<span class="icon">
								<span class="icon-frame frame-18 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/18/spell_holy_championsgrace.jpg&quot;);">
								</span>
								</span>
								<span class="name">Im Lichte wandeln</span>
								<span class="clear">
								<!-- -->
								</span>
								</li>
								<li data-spell="95859">
								<span class="icon">
								<span class="icon-frame frame-18 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/18/spell_nature_sleep.jpg&quot;);">
								</span>
								</span>
								<span class="name">Meditation</span>
								<span class="clear">
								<!-- -->
								</span>
								</li>
								<li data-spell="76669">
								<span class="icon">
								<span class="icon-frame frame-18 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/18/spell_holy_championsbond.jpg&quot;);">
								</span>
								</span>
								<span class="name">Meisterschaft: Erleuchtete Heilung</span>
								<span class="clear">
								<!-- -->
								</span>
								</li>
							</ul>
							<div class="description" style="width: 188px">
								Ruft zum Schutz und zur Heilung die Mächte des Lichts herbei.
							</div>
						</div>
					</div>
					<?php } ?>
					<span class="clear"><!-- --></span>
					<?php /*
					<div class="talentcalc-bottom">
						<div class="calcmode">
							<a href="javascript:;">Exportieren zum Talentrechner</a>
						</div>
						<div class="talentcalc-buttons">
							<button class="ui-button button2 " type="submit">
								<span>
									<span>übersicht anzeigen</span>
								</span>
							</button>
						</div>
						<span class="clear">
						<!-- -->
						</span>
					</div>
				</div>*/?>
				<script type="text/javascript">
        //<![CDATA[
		$(document).ready(function() {
			new TalentCalculator({
				id: "character",
				classId: <?=$char->GetClass()?>,
				calculatorMode: false,
				petMode: false,
				glyphMode: false,
				build: "000000000000000000003202300312212112123103203100000000000000",
				glyphs: [561, 704, 188, 191, 455, 186, 453, 454, 456],
				callback: null,
				nTrees: 3
		});
		});
		var MsgTalentCalculator = {
			talents: {
				tooltip: {
					rank: "Rang {0} / {1}",
					primaryTree: "Verwende zuerst {0} Punkte in deinem primären Talentbaum",
					reqTree: "Benötigt {0} Punkte in {1} Talenten",
					reqTalent: "Benötigt {0} Punkte in {1}",
					nextRank: "Nächster Rang:",
					click: "Zum Lernen klicken",
					rightClick: "Zum Verlernen rechts klicken"
				}
			},
			buttons: {
				overviewPane: {
					show: "übersicht anzeigen",
					hide: "Talentbäume anzeigen"
				}
			},
			info: {
				beastMastery: {
					tooltip: {
						title: "Tierherrschaft",
						description: "Füge 4 Bonuspunkte durch das Talent Tierherrschaft des Jägers dazu."
					}
				}
			}
		};
        //]]>
        </script>
			</div>
			<div class="character-glyphs" id="character-glyphs">
				<h3 class="category">Glyphen</h3>
				<div class="profile-box-full">
					<div class="character-glyphs-column glyphs-prime">
						<h4 class="subcategory ">Primäre</h4>
						<ul>
							<li class="filled">
							<a href="/wow/de/item/43869" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_sealofvengeance.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Siegel der Wahrheit'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
							<li class="filled">
							<a href="/wow/de/item/45744" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/ability_paladin_shieldofvengeance.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Schild der Rechtschaffenen'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
							<li class="filled">
							<a href="/wow/de/item/41098" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_crusaderstrike.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Kreuzfahrerstoß'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
						</ul>
					</div>
					<div class="character-glyphs-column glyphs-major">
						<h4 class="subcategory ">Erhebliche</h4>
						<ul>
							<li class="filled">
							<a href="/wow/de/item/41101" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_avengersshield.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Fokussierter Schild'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
							<li class="filled">
							<a href="/wow/de/item/43367" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_layonhands.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Handauflegung'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
							<li class="filled">
							<a href="/wow/de/item/41096" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_divineprotection.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Göttlicher Schutz'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
						</ul>
					</div>
					<div class="character-glyphs-column glyphs-minor">
						<h4 class="subcategory ">Geringe</h4>
						<ul>
							<li class="filled">
							<a href="/wow/de/item/43340" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_greaterblessingofkings.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Segen der Macht'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
							<li class="filled">
							<a href="/wow/de/item/43366" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_healingaura.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Einsicht'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
							<li class="filled">
							<a href="/wow/de/item/43368" class="color-q1">
							<span class="icon">
							<span class="icon-frame frame-27 ">
							<img src="http://eu.media.blizzard.com/wow/icons/36/spell_holy_sealofvengeance.jpg" alt="" width="27" height="27"/>
							</span>
							</span>
							<span class="name">Glyphe 'Wahrheit'</span>
							</a>
							<span class="clear"><!-- --></span>
							</li>
						</ul>
					</div>
					<span class="clear"><!-- --></span>
				</div>
			</div>
		</div>
	</div>
	<span class="clear"><!-- --></span>
</div>
<script type="text/javascript" src="/<?=$currtmp?>/js/profile.js?v20"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/talent-calculator.js?v20"></script>
