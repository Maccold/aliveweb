<div id="wiki" class="wiki wiki-item">
	<div class="sidebar">
		<div class="snippet">
			<div class="model-viewer">
				<div class="model can-drag" id="model-<?=$item->entry?>">
					<div class="loading">
						<div class="viewer" style="background-image: url(http://portal.wow-alive.de/templates/Shattered-World/images/items/item<?=$item->entry?>.jpg); background-position: 0px 0px; "></div>
					</div>
					<a href="javascript:;" class="rotate"></a> </div>
			</div>
			<script type="text/javascript">
			//<![CDATA[
			$(function() {
				Item.model = new ModelRotator("#model-<?=$item->entry?>", {
					zoom: false
				});
			});
			//]]>
			</script> 
		</div>
		<div class="snippet">
			<h3>Schnellinfos</h3>
			<ul class="fact-list">
				<!--<li> <span class="term">Entzaubern:</span> Erfordert Verzauberkunst (375) </li>-->
			</ul>
		</div>
		<div class="snippet">
			<h3>Erfahrt mehr</h3>
			<span id="fansite-links" class="fansite-group">
				<a href="http://de.wowhead.com/item=<?=$item->entry?>" target="_blank">Wowhead</a> 
				<a href="http://de.wow.wikia.com/wiki/<?=$item->name?>" target="_blank">Wowpedia</a> 
				<a href="http://wowdata.buffed.de/?i=<?=$item->entry?>" target="_blank">Buffed.de</a>
			</span>  
		</div>
	</div>
	<div class="info">
		<div class="title">
			<h2 class="color-q<?=$item->overallQualityId?>"><?=$item->name?></h2>
		</div>
		<div class="item-detail"> 
			<span class="icon-frame frame-56 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/56/<?=$item->icon?>.jpg&quot;);">
			<? if($item->stackable > 1){ ?><span class="stack"><?=$item->stackable?></span><? } ?>
			</span>
			<ul class="item-specs" style="margin: 0">
				<?=$item->getData("glyphType")?>
				<?=$item->getData("zoneBound")?>
				<!-- todo -->
				<?=$item->getData("heroic")?>
				<?=$item->getData("instanceBound")?>
				<?=$item->getData("conjured")?>
				<?=$item->getData("bonding")?>
				<?=$item->getData("maxCount")?>
				<?=$item->getData("requiredSkill")?>
				<?=$item->getData("requiredAbility")?>
				<?=$item->getData("startQuestId")?>
				<?=$item->getData("inventoryType")?>
				<?=$item->getData("damageData")?>
				<?=$item->getData("armor")?>
				<?=$item->getData("blockValue")?>
				<?=$item->getData("bonusStrength")?>
				<?=$item->getData("bonusAgility")?>
				<?=$item->getData("bonusStamina")?>
				<?=$item->getData("bonusIntellect")?>
				<?=$item->getData("bonusSpirit")?>
				<?=$item->getData("heirloomInfo")?>
				<!-- todo -->
				<?=$item->getData("fireResist")?>
				<?=$item->getData("natureResist")?>
				<?=$item->getData("frostResist")?>
				<?=$item->getData("shadowResist")?>
				<?=$item->getData("arcaneResist")?>
				<?=$item->getData("enchant")?>
				<!-- todo -->
				<?=$item->getData("randomEnchantData")?>
				<?=$item->getData("socketData")?>
				<?=$item->getData("gemProperties")?>
				<?=$item->getData("durabilityCurrent")?>
				<?=$item->getData("allowableRaces")?>
				<?=$item->getData("allowableClasses")?>
				<?=$item->getData("requiredLevel")?>
				<?=$item->getData("requiredFaction")?>
				<?=$item->getData("itemLevel")?>
				<?=$item->getData("requiredPersonalRating")?>
				<?=$item->getData("bonusDefenseSkillRating")?>
				<?=$item->getData("bonusDodgeRating")?>
				<?=$item->getData("bonusParryRating")?>
				<?=$item->getData("bonusBlockRating")?>
				<?=$item->getData("bonusHitMeleeRating")?>
				<?=$item->getData("bonusHitRangedRating")?>
				<?=$item->getData("bonusHitSpellRating")?>
				<?=$item->getData("bonusCritMeleeRating")?>
				<?=$item->getData("bonusCritRangedRating")?>
				<?=$item->getData("bonusCritSpellRating")?>
				<?=$item->getData("bonusHitTakenMeleeRating")?>
				<?=$item->getData("bonusHitTakenRangedRating")?>
				<?=$item->getData("bonusHitTakenSpellRating")?>
				<?=$item->getData("bonusCritTakenMeleeRating")?>
				<?=$item->getData("bonusCritTakenRangedRating")?>
				<?=$item->getData("bonusCritTakenSpellRating")?>
				<?=$item->getData("bonusHasteMeleeRating")?>
				<?=$item->getData("bonusHasteRangedRating")?>
				<?=$item->getData("bonusHasteSpellRating")?>
				<?=$item->getData("bonusHitRating")?>
				<?=$item->getData("bonusCritRating")?>
				<?=$item->getData("bonusHitTakenRating")?>
				<?=$item->getData("bonusCritTakenRating")?>
				<?=$item->getData("bonusResilienceRating")?>
				<?=$item->getData("bonusHasteRating")?>
				<?=$item->getData("bonusSpellPower")?>
				<?=$item->getData("bonusAttackPower")?>
				<?=$item->getData("bonusFeralAttackPower")?>
				<?=$item->getData("bonusFeralAttackPower")?>
				<?=$item->getData("bonusManaRegen")?>
				<?=$item->getData("bonusArmorPenetration")?>
				<?=$item->getData("bonusBlockValue")?>
				<?=$item->getData("bonusHealthRegen")?>
				<?=$item->getData("bonusSpellPenetration")?>
				<?=$item->getData("bonusExpertiseRating")?>
				<?=$item->getData("spellData")?>
				<?=$item->getData("sellPrice")?>
				<? if(isset($item->setData) && count($item->itemsetItems) > 0 ){ ?>
				<ul class="item-specs">
					<li class="color-tooltip-yellow"><? echo  $item->itemsetName." (".$item->numSetPieces."/".$item->itemsetCount.")"; ?></li>
					<? if($item->requiredSkillId > 0 && $item->meetsSkillReq) { ?>
					<li><span class="color-d4"><? echo $lang_strings['armory.item-tooltip.requires']." ".$item->requiredSkill; ?></span></li>
					<? } elseif($item->requiredSkillId > 0) { ?>
					<li><span class="color-tooltip-red"><? echo $lang_strings['armory.item-tooltip.requires']." ".$item->requiredSkill; ?></span></li>
					<? } ?>
					<? foreach($item->itemsetItems as $setItem){ ?>
					<li class="indent">
						<? if($setItem["equipped"]){ ?>
						<span class="color-q1">
						<?=$setItem["name"]?>
						</span>
						<? } else { ?>
						<span class="color-d4">
						<?=$setItem["name"]?>
						</span>
						<? } ?>
					</li>
					<? } ?>
					<li class="indent-top"> </li>
					<? 
			foreach($item->itemsetBonus as $order => $bonus){
				if($item->numSetPieces >= $bonus["threshold"])
					echo '<li class="color-tooltip-green">('.$bonus["threshold"].') '.$lang_strings['armory.item-tooltip.set'].': '.$bonus["desc"].'</li>';
				else
					echo '<li class="color-d4">('.$bonus["threshold"].') '.$lang_strings['armory.item-tooltip.set'].': '.$bonus["desc"].'</li>';	
				
			} ?>
				</ul>
				<? } ?>
				<?=$item->getData("desc")?>
			</ul>
			<? if($item->counterpartId > 0){ ?>
			<div class="faction-related">
				<a href="/item/<?=$item->counterpartId?>/" data-tooltip="#faction-tooltip"> 
					<span class="icon-frame frame-14 "> <img src="/images/icons/18/faction_<?=$item->counterpartFaction?>.jpg" alt="" width="14" height="14" /> </span> 
					<span class="icon-frame frame-14 "> <img src="/images/icons/18/<?=$item->counterpartIcon?>.jpg" alt="" width="14" height="14" /> </span> </a>
					<div id="faction-tooltip" style="display: none"> Dieser Gegenstand wird zu <span class="color-q<?=$item->overallQualityId?>"><?=$item->counterpartName?></span> verwandelt, falls ihr zu der <?=$lang_strings["armory.faction.".$item->counterpartFaction]?> transferiert. </div>
			</div>
			<? } ?>
		</div>
	</div>
	<span class="clear"><!-- --></span>
	<div class="related">
		<div class="tabs ">
			<ul id="related-tabs">
			<? if($item->sourceTypeCount["creature"] > 0){ ?>
				<li> <a href="/item/<?=$item->entry?>/#dropCreatures" data-key="dropCreatures" id="tab-dropCreatures" class="tab-active"> <span><span> Erhältlich von (<?=$item->sourceTypeCount["creature"]?>)</span></span> </a> </li>
			<? } ?>
			<? if($item->sourceTypeCount["vendor"] > 0){ ?>
				<li> <a href="/item/<?=$item->entry?>/#vendors" data-key="vendors" id="tab-vendors" class="tab-active"> <span><span> H&auml;ndler (<?=$item->sourceTypeCount["vendor"]?>)</span></span> </a> </li>
			<? } ?>
			</ul>
			<span class="clear"><!-- --></span>
		</div>
		<div id="related-content" class="">
			
		</div>
	</div>
</div>
<script type="text/javascript">
//<![CDATA[

$(function() {
	Wiki.pageUrl = '/item/<?=$item->entry?>/';
});

//]]>
</script> 
<script type="text/javascript" src="/<?=$currtmp?>/js/wiki.js?v2"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/item.js?v1"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/model-rotator.js?v1"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/table.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/filter.js"></script>