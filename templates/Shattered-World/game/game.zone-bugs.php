
<div class="related-content" id="related-bugs"> 
	<span id="comments"></span>
	<div id="page-comments">
		<div class="page-comment-interior">
			<h3> Bekannte Bugs (<?=count($bugs)?>) </h3>
			<div class="comments-container"> 
				<script type="text/javascript">
				//<![CDATA[
					var textAreaFocused = false;
				//]]>
				</script>
				
				<? foreach($bugs as $n => $bug){ ?>
				<div style="z-index: 3;" class="comment" id="c-<?=$n?>">
					<div class="avatar portrait-b">
						<div class="avatar-interior"> <img height="64" src="http://portal.wow-alive.de/templates/Shattered-World/images/wiki/Trigmas.gif" alt="" /> </div>
					</div>
					<div class="comment-interior">
						<div class="character-info user">
							<div class="user-name"> 
								<a href="<?=url_for("community", "bugtracker")?>" class="wow-class-8" rel="np"> GameMaster </a> 
							</div>
							<span class="time"> <?=$bug["date"]?></span> 
						</div>
						<div class="content">
							<?=$bug["text"]?>
						</div>
					</div>
				</div>
				<? } ?>
				<span class="clear"><!-- --></span> 
			</div>
		</div>
	</div>
	<script type="text/javascript">
    //<![CDATA[
		Wiki.related['bugs'] = new WikiRelated('bugs');
    //]]>
	</script> 
</div>
