<div class="profile-sidebar-contents">
	<?php if($char_mode != "simple" && $char_mode != "advanced"){ ?>
	<div class="profile-sidebar-crest">
		<a href="<?=$char->GetCharacterLink()?>" rel="np"
			class="profile-sidebar-character-model"
			style="background-image: url(/images/2d/inset/<?=$char->GetRace()?>-<?=$char->GetGender()?>.jpg);">
			<span class="hover"></span> <span class="fade"></span> </a>
		<div class="profile-sidebar-info">
			<div class="name">
				<a href="<?=$char->GetCharacterLink()?>" rel="np"><?=$char->GetName()?></a>
			</div>
			<div class="under-name <?=$char->GetCssClass()?>">
				<span class="level"><strong><?=$char->GetRaw("level")?></strong>
				</span>, <span class="race"><?=$char->GetRaceText()?></span>,
				<span class="class"><?=$char->GetClassText()?></span>
			</div>
			<div class="guild">
				<a href="<?=$char->GetGuildLink()?>"><?=$char->GetGuildName?></a>
			</div>
			<div class="realm">
				<span id="profile-info-realm" class="tip" data-battlegroup="Alive">Norgannon</span>
			</div>
		</div>
	</div>
	<?php } else { ?>
	<div class="profile-info-anchor">
		<div class="profile-info">
			<div class="name">
				<a href="<?=$char->GetCharacterLink()?>/" rel="np"><?=$char->GetName()?></a>
			</div>
			<div class="title-guild">
				<div class="title">	<?=$char->GetTitle()?> </div>
				<div class="guild">
					<a href="<?=$char->GetGuildLink()?>"><?=$char->GetGuildName()?> </a>
				</div>
			</div>
			<span class="clear"> <!-- --> </span>
			<div class="under-name <?=$char->GetCssClass()?>">
				<span class="level"><strong><?=$char->GetRaw("level")?> </strong> </span>,
				<span class="race"><?=$char->GetRaceText()?> </span>, <a
					id="profile-info-spec"
					href="<?=$char->GetCharacterLink()?>/talent/" class="spec tip"><?=$talent_spec[$char->GetActiveSpec()]["prim"]?>
				</a>, <span class="class"><?=$char->GetClassText()?> </span><span
					class="comma">,</span> <span class="realm tip"
					id="profile-info-realm" data-battlegroup="Alive"> Norgannon </span>
			</div>
			<div class="achievements">
				<a href="<?=$char->GetCharacterLink()?>/achievement"><?=$char->GetAchievementPoints()?></a>
			</div>
		</div>
	</div>
	<?php } ?>
	<ul class="profile-sidebar-menu" id="profile-sidebar-menu">
	<?php if($char_mode == "achievement"){ ?>
		<li> <a href="<?=$char->GetCharacterLink()?>/" class="back-to" rel="np"><span class="arrow"><span class="icon">Charakterübersicht</span></span></a> </li>
		<li class="root-menu"> <a href="<?=$char->GetCharacterLink()?>/achievement" class="back-to" rel="np"><span class="arrow"><span class="icon">Erfolge</span></span></a> </li>
		<li class=" active"> <a href="<?=$char->GetCharacterLink()?>/achievement#summary" class="" rel="np"> <span class="arrow"><span class="icon"> Erfolge </span></span> </a> </li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#92" class="" rel="np"> <span class="arrow"><span class="icon"> Allgemein </span></span> </a> </li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#96" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Quests </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#96:14861" class="" rel="np"> <span class="arrow"><span class="icon"> Östliche Königreiche </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#96:15081" class="" rel="np"> <span class="arrow"><span class="icon"> Kalimdor </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#96:14862" class="" rel="np"> <span class="arrow"><span class="icon"> Scherbenwelt </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#96:14863" class="" rel="np"> <span class="arrow"><span class="icon"> Nordend </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#96:15070" class="" rel="np"> <span class="arrow"><span class="icon"> Cataclysm </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#97" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Erkundung </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#97:14777" class="" rel="np"> <span class="arrow"><span class="icon"> Östliche Königreiche </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#97:14778" class="" rel="np"> <span class="arrow"><span class="icon"> Kalimdor </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#97:14779" class="" rel="np"> <span class="arrow"><span class="icon"> Scherbenwelt </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#97:14780" class="" rel="np"> <span class="arrow"><span class="icon"> Nordend </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#97:15069" class="" rel="np"> <span class="arrow"><span class="icon"> Cataclysm </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Spieler gegen Spieler </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:165" class="" rel="np"> <span class="arrow"><span class="icon"> Arena </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:14801" class="" rel="np"> <span class="arrow"><span class="icon"> Alteractal </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:14802" class="" rel="np"> <span class="arrow"><span class="icon"> Arathibecken </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:14803" class="" rel="np"> <span class="arrow"><span class="icon"> Auge des Sturms </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:14804" class="" rel="np"> <span class="arrow"><span class="icon"> Kriegshymnenschlucht </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:14881" class="" rel="np"> <span class="arrow"><span class="icon"> Strand der Uralten </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:14901" class="" rel="np"> <span class="arrow"><span class="icon"> Tausendwinter </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:15003" class="" rel="np"> <span class="arrow"><span class="icon"> Insel der Eroberung </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:15073" class="" rel="np"> <span class="arrow"><span class="icon"> Schlacht um Gilneas </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:15074" class="" rel="np"> <span class="arrow"><span class="icon"> Zwillingsgipfel </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:15075" class="" rel="np"> <span class="arrow"><span class="icon"> Tol Barad </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#95:15092" class="" rel="np"> <span class="arrow"><span class="icon"> Gewertete Schlachtfelder </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Dungeons &amp; Schlachtzüge </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168:14808" class="" rel="np"> <span class="arrow"><span class="icon"> Classic </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168:14805" class="" rel="np"> <span class="arrow"><span class="icon"> The Burning Crusade </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168:14806" class="" rel="np"> <span class="arrow"><span class="icon"> Wrath of the Lich King - Dungeons </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168:14922" class="" rel="np"> <span class="arrow"><span class="icon"> Wrath of the Lich King - Schlachtzüge </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168:15067" class="" rel="np"> <span class="arrow"><span class="icon"> Cataclysm - Dungeons </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#168:15068" class="" rel="np"> <span class="arrow"><span class="icon"> Cataclysm - Schlachtzüge </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#169" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Berufe </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#169:170" class="" rel="np"> <span class="arrow"><span class="icon"> Kochkunst </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#169:171" class="" rel="np"> <span class="arrow"><span class="icon"> Angeln </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#169:172" class="" rel="np"> <span class="arrow"><span class="icon"> Erste Hilfe </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#169:15071" class="" rel="np"> <span class="arrow"><span class="icon"> Archäologie </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#201" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Ruf </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#201:14864" class="" rel="np"> <span class="arrow"><span class="icon"> Classic </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#201:14865" class="" rel="np"> <span class="arrow"><span class="icon"> The Burning Crusade </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#201:14866" class="" rel="np"> <span class="arrow"><span class="icon"> Wrath of the Lich King </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#201:15072" class="" rel="np"> <span class="arrow"><span class="icon"> Cataclysm </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Weltereignisse </span></span> </a>
			<ul>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:160" class="" rel="np"> <span class="arrow"><span class="icon"> Mondfest </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:187" class="" rel="np"> <span class="arrow"><span class="icon"> Liebe liegt in der Luft </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:159" class="" rel="np"> <span class="arrow"><span class="icon"> Nobelgarten </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:163" class="" rel="np"> <span class="arrow"><span class="icon"> Kinderwoche </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:161" class="" rel="np"> <span class="arrow"><span class="icon"> Sonnenwende </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:162" class="" rel="np"> <span class="arrow"><span class="icon"> Braufest </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:158" class="" rel="np"> <span class="arrow"><span class="icon"> Schlotternächte </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:14981" class="" rel="np"> <span class="arrow"><span class="icon"> Die Pilgerfreuden </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:156" class="" rel="np"> <span class="arrow"><span class="icon"> Winterhauch </span></span> </a> </li>
				<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:14941" class="" rel="np"> <span class="arrow"><span class="icon"> Argentumturnier </span></span> </a> </li>
				<? /*	<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#155:15101" class="" rel="np"> <span class="arrow"><span class="icon"> Dunkelmond-Jahrmarkt </span></span> </a> </li> */ ?>
			</ul>
		</li>
		<li class=""> <a href="<?=$char->GetCharacterLink()?>/achievement#81" class="" rel="np"> <span class="arrow"><span class="icon"> Heldentaten </span></span> </a> </li>
	<?php } else { ?>
		<li <?php if($char_mode == "simple" || $char_mode == "advanced"){ echo ' class="active"'; }?>>
			<a href="<?=$char->GetCharacterLink()?>/" class="" rel="np"> 
				<span class="arrow"><span class="icon"> Übersicht </span> </span></a>
		</li>
		<?php if($char->GetGuildID() > 0){?>
		<li class="">
			<a href="<?=$char->GetGuildLink()?>" class="has-submenu" rel="np">
				<span class="arrow"><span class="icon"> Gilde </span> </span> </a>
		</li>
		<?php } else { ?>
		<li class="disabled">
			<a href="javascript:;" rel="np">
				<span class="arrow"><span class="icon"> Gilde </span> </span> </a>
		</li>
		<?php } ?>
		<li class="<?=($char_mode == "talent" ? "active" : "")?>">
			<a href="<?=$char->GetCharacterLink()?>/talent/primary" class="" rel="np"> 
			<span class="arrow"><span class="icon"> Talente &amp; Glyphen </span> </span> </a>
		</li>
		<li class="<?=($char_mode == "pvp" ? "active" : "")?>">
			<a href="<?=$char->GetCharacterLink()?>/pvp" class="" rel="np"> 
				<span class="arrow"><span class="icon"> PvP </span></span> </a>
		</li>
		<li class="<?=($char_mode == "achievements" ? "active" : "")?> disabled">
			<a href="javascript:;" class="" rel="np"> 
				<span class="arrow"><span class="icon"> Erfolge (in arbeit) </span></span> </a>
		</li>
		<?php /*
		<li class=" disabled">
			<a href="javascript:;" class=" has-submenu vault" rel="np"> 
			<span class="arrow"><span
					class="icon"> Auktionen </span> </span> </a>
		</li>
		<li class=" disabled">
			<a href="javascript:;" class=" vault" rel="np">
				<span class="arrow"><span class="icon"> Ereignisse </span> </span> </a>
		</li>
		<li class=" disabled">
			<a href="<?=$char->GetCharacterLink()?>/achievement" class=" has-submenu" rel="np"> 
				<span class="arrow"><span class="icon"> Erfolge </span> </span> </a>
		</li>
		<li class=" disabled">
			<a href="<?=$char->GetCharacterLink()?>/companion" class="" rel="np"> 
				<span class="arrow"><span class="icon"> Haus- und Reittiere </span> </span>
		</a>
		</li>
		<li class=" disabled">
			<a href="<?=$char->GetCharacterLink()?>/profession/" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon">
						Berufe </span> </span> </a>
		</li>
		<li class=" disabled">
			<a href="<?=$char->GetCharacterLink()?>/reputation/" class="" rel="np">
				<span class="arrow"><span class="icon"> Ruf </span> </span> </a>
		</li>
		<li class="disabled">
			<a href="<?=$char->GetCharacterLink()?>/feed" class="" rel="np"> 
				<span class="arrow"><span class="icon"> Aktivitäten-Feed </span> </span> </a>
		</li>
		*/ ?>
	<?php }?>
	</ul>
	<span class="clear"><!--  --></span>
	<?php if($char_mode == "simple" || $char_mode == "advanced"){ ?>
	<div class="summary-sidebar-links">
		<!--<span class="summary-sidebar-button"> <a href="javascript:;" id="summary-link-tools" class="summary-link-tools"></a> </span> -->
		<span class="summary-sidebar-button"> 
			<a href="javascript:;" data-fansite="character|EU|<?=$char->GetName();?>|<?=$char->realmName?>" class="fansite-link "> </a> 
		</span>
	</div>
	<?php }?>
</div>