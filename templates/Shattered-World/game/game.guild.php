<div id="profile-wrapper" class="profile-wrapper profile-wrapper-<?=$guildRow["factionStyle"]?>">
	<div class="profile-sidebar-anchor">
		<div class="profile-sidebar-outer">
			<div class="profile-sidebar-inner">
				<div class="profile-sidebar-contents">
					<div class="profile-sidebar-tabard">
						<div class="guild-tabard">
							<canvas id="guild-tabard" width="240" height="240" style="display: inline; ">
								<div class="guild-tabard-default "></div>
							</canvas>
							<script type="text/javascript">
					        //<![CDATA[
								$(document).ready(function() {
									var tabard = new GuildTabard('guild-tabard', {
										'ring': 'alliance',
										'bg': [ 0, <?=$guildRow["BackgroundColor"]?> ],
										'border': [ <?=$guildRow["BorderStyle"]?>, <?=$guildRow["BorderColor"]?> ],
										'emblem': [ <?=$guildRow["EmblemStyle"]?>, <?=$guildRow["EmblemColor"]?> ]
									});
								});
					        //]]>
					        </script>
							<div class="tabard-overlay"></div>
							<div class="crest"></div>
							<a class="tabard-link" href="<?=$guildRow["url"]?>"></a>
						</div>
						<div class="profile-sidebar-info">
							<div class="name">
								<a href="<?=$guildRow["url"]?>"><?=$guildRow["name"]?></a>
							</div>
							<div class="under-name">
								 <span class="faction"><?=$guildRow["factionText"]?></span>-Gilde
							</div>
							<div class="realm">
								<span id="profile-info-realm"><?=$guildRow["realm"]?></span>
							</div>
						</div>
					</div>
					<ul class="profile-sidebar-menu" id="profile-sidebar-menu">
						<?/*<li class="">
							<a href="/wow/de/guild/arygos/Accept%20your%20Fate/" class="" rel="np">
								<span class="arrow"><span class="icon">Übersicht </span></span>
							</a>
						</li>*/?>
						<?php if($returnTo == "character"){?>
						<li class="">
							<a href="<?=$returnToUrl?>" class="back-to" rel="np">
								<span class="arrow"><span class="icon"><?=$returnToText?></span></span></a>
						</li>
						<?php } ?>
						<li<?php if($view == "list"){ echo' class="active"'; }?>>
							<a href="<?=$guildRow["url"]?>" class="" rel="np">
								<span class="arrow"><span class="icon">Mitgliederverzeichnis </span></span>
							</a>
						</li>
						<li<?php if($view == "professions"){ echo' class="active"'; }else{echo ' class="disabled inactive"';}?>>
							<a href="javascript:;" class="disabled" rel="np">
								<span class="arrow"><span class="icon">Gildenberufe </span></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-contents">
		<div class="profile-section-header">
			<h3 class="category"> Mitgliederverzeichnis </h3>
		</div>
		<div id="roster" class="profile-section">
			<form id="roster-form" action="">
				<div class="roster-filters clear-after">
					<input type="hidden" name="view" id="filter-view"/>
					<div id="roster-buttons">
						<button class="ui-button button2 " type="submit"><span><span> Filter </span></span></button>
						<a href="javascript:;" id="filter-reset">Zurücksetzen</a>
					</div>
					<div class="selection">
						<label for="filter-name">Name:</label>
						<input type="text" name="name" class="input character" id="filter-name" data-column="0" value="" data-filter="column" alt="Name eintragen"/>
					</div>
					<div class="selection">
						<label for="filter-minLvl">Stufe:</label>
						<input type="text" name="minLvl" id="filter-minLvl" class="input level" value="1" maxlength="2" data-min="1" data-filter="range" data-column="3"/> - <input type="text" name="maxLvl" id="filter-maxLvl" class="input level" value="80" maxlength="2" data-max="85" data-filter="range" data-column="3"/>
					</div>
					<div class="selection">
						<label for="filter-race">Volk:</label>
						<select name="race" class="input class" id="filter-race" data-column="1" data-filter="column">
							<option value="">Alle</option>
						<?php if($guildRow["faction"] == 0){?>
							<option value="1">Menschen</option>
							<option value="3">Zwerge</option>
							<option value="4">Nachtelfen</option>
							<option value="7">Gnome</option>
							<option value="11">Draenei</option>
						<?php } else { ?>
							<option value="2">Orcs</option>
							<option value="5">Untote</option>
							<option value="6">Tauren</option>
							<option value="8">Trolle</option>
							<option value="10">Blutelfen</option>
						<?php } ?>
						</select>
					</div>
					<div class="selection">
						<label for="filter-class">Klasse:</label>
						<select name="class" class="input class" id="filter-class" data-column="2" data-filter="column">
							<option value="">Alle</option>
							<option value="6">Todesritter</option>
							<option value="11">Druide</option>
							<option value="3">Jäger</option>
							<option value="8">Magier</option>
							<option value="2">Paladin</option>
							<option value="5">Priester</option>
							<option value="4">Schurke</option>
							<option value="7">Schamane</option>
							<option value="1">Krieger</option>
							<option value="9">Hexenmeister</option>
						</select>
					</div>
					<div class="selection inputs-rank">
						<label for="filter-rank">Gildenrang:</label>
						<select name="rank" class="input guildrank" id="filter-rank" data-column="4" data-filter="column">
							<option value="">Alle</option>
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
						</select>
					</div>
					<span class="clear"><!-- --></span>
				</div>
			</form>
			<div class="table">
				<div class="table-options data-options ">
					<div class="option">
						<ul class="ui-pagination">
						</ul>
					</div>
					 Zeige <strong class="results-start">1</strong>–<strong class="results-end">50</strong> 
					 von <strong class="results-total"><?=$totalMemberCount?></strong> Ergebnissen 
					 <span class="clear"><!-- --></span>
				</div>
				<table id="guild-roster">
				<thead>
				<tr>
					<th class="name">
						<a href="<?=$guildRow["url"]?>/?view=achievementPoints&amp;sort=name&amp;dir=a" class="sort-link">
						<span class="arrow"> Name </span>
						</a>
					</th>
					<th class="race align-center">
						<a href="<?=$guildRow["url"]?>/?view=achievementPoints&amp;sort=race&amp;dir=a" class="sort-link numeric">
						<span class="arrow"> Volk </span>
						</a>
					</th>
					<th class="cls align-center">
						<a href="<?=$guildRow["url"]?>/?view=achievementPoints&amp;sort=class&amp;dir=a" class="sort-link numeric">
						<span class="arrow"> Klasse </span>
						</a>
					</th>
					<th class="lvl align-center">
						<a href="<?=$guildRow["url"]?>/?view=achievementPoints&amp;sort=lvl&amp;dir=a" class="sort-link numeric">
						<span class="arrow"> Stufe </span>
						</a>
					</th>
					<th class="rank align-center">
						<a href="<?=$guildRow["url"]?>/?view=achievementPoints&amp;sort=rank&amp;dir=a" class="sort-link numeric">
						<span class="arrow"> Gildenrang </span>
						</a>
					</th>
					<th class="ach-points align-center">
						<a href="<?=$guildRow["url"]?>/?view=achievementPoints&amp;sort=achievements&amp;dir=d" class="sort-link numeric">
						<span class="arrow"> Erfolgspunkte </span>
						</a>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($memberRows as $row){?>
					<tr class="<?=$row["css"]?> class-<?=$row["class"]?> race-<?=$row["race"]?>" data-level="<?=$row["level"]?>">
						<td class="name" data-raw="<?=utf8_decode($row["name"])?>">
							<strong><a href="<?=$row["url"]?>" class="color-c<?=$row["class"]?>"><?=htmlentities($row["name"])?></a></strong>
						</td>
						<td class="race" data-raw="<?=$row["race"]?>">
							<?=icon_race($row["race"],$row["gender"])?>
						</td>
						<td class="cls" data-raw="<?=$row["class"]?>">
							<?=icon_class($row["class"])?>
						</td>
						<td class="lvl" data-raw="<?=$row["level"]?>"><?=$row["level"]?></td>
						<td class="rank" data-raw="<?=$row["rank"]?>">
						<?php if($row["rank"] == 0){?>
							<span class="guild-master"> Gildenmeister <span class="symbol"></span></span>
						<?php } else { ?>
							<span>Rang <?=$row["rank"]?> </span>
						<?php } ?>
						</td>
						<td class="ach-points" data-raw="<?=$row["achievementPoints"]?>"><span class="ach-icon"><?=$row["achievementPoints"]?></span></td>
					</tr>
				<?php }?>
				</tbody>
				</table>
				<div class="table-options data-options ">
					<div class="option">
						<ul class="ui-pagination"></ul>
					</div>
					 Zeige <strong class="results-start">1</strong>–<strong class="results-end">50</strong> 
					 von <strong class="results-total"><?=$totalMemberCount?></strong> Ergebnissen <span class="clear">
					<!-- -->
					</span>
				</div>
			</div>
		</div>
	</div>
	<span class="clear"><!-- --></span>
</div>
<script type="text/javascript">
//<![CDATA[
$(function() {
	Profile.url = '<?=$guildRow["url"]?>/';
});

$(function() {
	var table = new Table($('#roster'), {
		paging: true,
		totalResults: <?=$totalMemberCount?>,
		results: 50,
		column: 4,
		method: 'numeric',
		type: 'asc'
	});

	// Setup filters
	function filterRows(){
		//table.filter("column",2,$("#filter-class").val(),"exact");
		table.filterBatch({
			columns: [
				[0,$("#filter-name").val(),"default"],
				[1,$("#filter-race").val(),"exact"],
				[2,$("#filter-class").val(),"exact"],
				[3,[$("#filter-minLvl").val(),$("#filter-maxLvl").val()],"range"],
				[4,$("#filter-rank").val(),"exact"]
			]
		});
		
	}
	
	$("#filter-minLvl, #filter-maxLvl, #filter-name").keyup(filterRows);
	$("#filter-class, #filter-race, #filter-rank").change(filterRows);
	$("#filter-reset").click(function(){ 
		table.reset(); 
		$("#filter-class, #filter-race, #filter-rank, #filter-minLvl, #filter-maxLvl").val("");
	});
});


var MsgProfile = {
	tooltip: {
		feature: {
			notYetAvailable: "Diese Funktion ist derzeit noch nicht verfügbar."
		},
		vault: {
			character: "Diese Sektion ist nur verfügbar, wenn du mit diesem Charakter eingeloggt bist.",
			guild: "Diese Sektion ist nur verfügbar, wenn du mit einem Charakter aus dieser Gilde eingeloggt bist."
		}
	}
};
//]]>
        </script>
<script type="text/javascript" src="/<?=$currtmp?>/js/table.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/filter.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/dropdown.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/profile.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/guild-tabard.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/guild.js"></script>