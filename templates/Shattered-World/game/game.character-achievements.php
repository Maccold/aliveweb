
<div id="profile-wrapper" class="profile-wrapper profile-wrapper-<?=$char->GetCssFaction()?>">
	<div class="profile-sidebar-anchor">
		<div class="profile-sidebar-outer">
			<div class="profile-sidebar-inner">
				<div class="profile-sidebar-contents">
					<?php echo $sidebar_character; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-contents">
		<div class="profile-section-header">
			<div class="achievement-points-anchor">
				<div class="achievement-points"> <?=$summary["points"]?> </div>
			</div>
			<ul class="profile-tabs">
				<li class="tab-active"> <a href="achievement" rel="np"> <span class="r"><span class="m"> Erfolge </span></span> </a> </li>
				<li> <a href="statistic" rel="np"> <span class="r"><span class="m"> Statistiken </span></span> </a> </li>
			</ul>
		</div>
		<div class="profile-section">
			<div class="search-container keyword" id="search-container" style="display: none"> <span class="view"></span> <span class="reset" style="display: none"></span>
				<input type="text" id="achievement-search" alt="Suchen…" value="Suchen…" onKeyUp="AchievementsHandler.doSearch(this.value)" class="input" autocomplete="off"  />
			</div>
			<div id="cat-summary" class="container" style="display: none">
				<h3 class="category">Fortschrittsübersicht</h3>
				<div class="achievements-total">
					<div class="profile-box-full">
						<div class="achievements-total-completed">
							<div class="desc"> Vollständig abgeschlossen </div>
							<div class="profile-progress border-4" onMouseOver="Tooltip.show(this, &#39;<?=$summary["points"]." / ".$summary["totalPoints"]?> Punkte&#39;, { location: &#39;middleRight&#39; });">
								<div class="bar border-4 hover" style="width: <?=round($summary["earned"]/$summary["total"]*100)?>%"></div>
								<div class="bar-contents"> <strong> <?=$summary["earned"]." / ".$summary["total"]." (".round($summary["earned"]/$summary["total"]*100)."%)"?></strong> </div>
							</div>
						</div>
						<div class="achievements-categories-total">
						<?php for($i = 0; $i < (count($info_categories)-1); $i++){?>
							<div class="entry">
								<div class="entry-inner <? if(($i+1) % 3 == 0){ echo "entry-inner-right"; }?>"> <strong class="desc"><?=$summaryCats[$info_categories[$i]]["label"]?></strong>
									<div class="active-category" onClick="window.location.hash = 'achievement#<?=$info_categories[$i]?>'; dm.openEntry(false)">
										<div class="profile-progress border-4" onMouseOver="Tooltip.show(this, &#39;<?=$summaryCats[$info_categories[$i]]["points"]." / ".$summaryCats[$info_categories[$i]]["totalPoints"]?> Punkte&#39;, { location: &#39;middleRight&#39; });">
											<div class="bar border-4 hover" style="width: <?=$summaryCats[$info_categories[$i]]["percent"]?>%"></div>
											<div class="bar-contents"> <?=$summaryCats[$info_categories[$i]]["earned"]." / ".$summaryCats[$info_categories[$i]]["total"]." (".$summaryCats[$info_categories[$i]]["percent"]."%)"?> </div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
							<div class="entry">
								<div class="entry-inner entry-inner-right"> <strong class="desc"><?=$summaryCats[81]["label"]?></strong>
									<div class="active-category" onClick="window.location.hash = 'achievement#81'; dm.openEntry(false)">
										<div class="profile-progress border-4 completed" onMouseOver="">
											<div class="bar border-4 hover" style="width: 0%"></div>
											<div class="bar-contents"> <?=$summaryCats[81]["earned"]?> </div>
										</div>
									</div>
								</div>
							</div>
							<span class="clear"><!-- --></span>
						</div>
					</div>
				</div>
				<h3 class="category">Kürzlich erhalten</h3>
				<div class="achievements-recent profile-box-full">
					<ul>
					<?php foreach($lastAchievements as $i => $achievement){ ?>
						<li<?php if($i == 5){ echo 'class="last"'; }?>> <a href="achievement<?=$achievement["link"]?>" onClick="window.location.hash = '<?=$achievement["link"]?>'; dm.openEntry(false)" class="clear-after"> 
							<span class="float-right"> 
								<span class="points"><?=$achievement["points"]?></span> 
								<span class="date"><?=$achievement["dateCompleted"]?></span> 
							</span> 
							<span class="icon"> 
								<span  class="icon-frame frame-18 " style='background-image: url("/images/icons/18/<?=$achievement["icon"]?>.jpg");'> </span> 
							</span> 
							<span class="info"> 
								<strong class="title"><?=htmlentities($achievement["title"])?></strong> 
								<span class="description"><?=htmlentities($achievement["desc"])?></span> 
							</span> </a> </li>
					<?php } ?>
					</ul>
				</div>
			</div>
			<div id="achievement-list" class="achievements-list"> </div>
		</div>
	</div>
	<span class="clear"><!-- --></span>
</div>

<script type="text/javascript">
//<![CDATA[
$(function() {
	Profile.url = '<?=$char->GetUrl()?>/achievement';
});

	var MsgProfile = {
		tooltip: {
			feature: {
				notYetAvailable: "Diese Funktion ist derzeit noch nicht verfügbar."
			},
			vault: {
				character: "Diese Sektion ist nur verfügbar, wenn du mit diesem Charakter eingeloggt bist.",
				guild: "Diese Sektion ist nur verfügbar, wenn du mit einem Charakter aus dieser Gilde eingeloggt bist."
			}
		}
	};
//]]>
</script> 
<script type="text/javascript">
//<![CDATA[
$(document).ready(function () {
	DynamicMenu.init({ "section": "achievement" });
	AchievementsHandler.init();
})
//]]>
</script> 
<script type="text/javascript" src="/<?=$currtmp?>/js/profile.js?v20"></script> 
<script type="text/javascript" src="/<?=$currtmp?>/js/achievement.js?v20"></script> 