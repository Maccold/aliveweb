<script type="text/javascript" src="/<?=$currtmp?>/js/guild-tabard.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/arena-flags.js"></script>

<div class="search">
	<div class="search-right">
		<div class="search-header">
			<form action="/search/" method="get" class="search-form">
				<div>
					<input id="search-page-field" type="text" name="q" maxlength="200" tabindex="2" value="<?php echo $search_query;?>"/>
					<button class="ui-button button1 " type="submit"><span><span>Suchen</span></span></button>
				</div>
			</form>
		</div>
		<?php if($no_results){ ?>
		<div class="no-results">
			<h3 class="subheader "> Erneut suchen </h3>
			<h3 class="category "> Suchvorschläge: </h3>
			<ul>
				<li>Stelle bitte sicher, dass die Begriffe richtig geschrieben wurden</li>
				<li>Benutze allgemeinere Suchbegriffe</li>
				<li>Falls du den Name und Realm eines Charakters kennst, nutze das Format Charakter@Realm</li>
			</ul>
		</div>
		<?php } elseif($search_filter == "none") { ?>
		
		<div class="helpers">
			<h3 class="subheader">Alle Ergebnisse für <span><?php echo $search_query;?></span></h3>
		</div>
		<div class="summary">
		<?php if($charCount > 0){?>
			<div class="results results-grid wow-results">
				<h3 class="category "><a href="/search/?q=<?=urlencode($search_query)?>&amp;f=character">Charakter</a> (<?=$charCount?>) </h3>
				<?php foreach($charRows as $row){ ?>
				<div class="grid">
					<div class="wowcharacter">
						<a href="/character/<?=strtolower($row["realm"])?>/<?=urlencode($row["name"])?>/" class="icon-frame frame-56 thumbnail">
							<img src="<?=$row["icon"]?>" alt="" width="56" height="56"/>
						</a>
						<a href="/character/<?=$row["realm"]?>/<?=urlencode($row["name"])?>/" class="color-c<?=$row["class"]?>">
						<strong><?=$row["name"]?></strong>
						</a><br/>
						<?=$row["level"]?>, <?=$row["raceLabel"]?>, <?=$row["classLabel"]?><br/>
						<?=$row["realm"]?> <span class="clear"><!-- --></span>
					</div>
				</div>
				<?php } ?>
				<span class="clear">
				<!-- -->
				</span>
			</div>
		<?php }?>
		<?php if($itemCount > 0){ ?>
			<div class="results results-grid wow-results">
				<h3 class="category "><a href="/search/?q=<?=urlencode($search_query)?>&amp;f=item">Gegenstand</a> (<?=$itemCount?>) </h3>
				<?php foreach($itemRows as $row){ ?>
				<div class="grid">
					<div class="wowitem">
						<a href="/item/<?=$row["entry"]?>" class="thumbnail">
							<span class="icon-frame frame-32 ">
								<img src="<?=$displayIds[$row["displayid"]]["icon"]?>" alt="" width="32" height="32"/>
							</span>
						</a>
						<a href="/item/<?=$row["entry"]?>" class="color-q<?=$row["quality"]?>">
							<strong><?=htmlentities($row["name"])?></strong>
						</a><br/>
						Gegenstandsstufe <?=$row["ItemLevel"]?>
						<span class="clear"><!-- --></span>
					</div>
				</div>
				<?php } ?>
				<span class="clear"><!-- --></span>
			</div>
		<?php } ?>
		<?php if($arenaCount > 0){ ?>
			<div class="results results-grid wow-results">
				<h3 class="category "><a href="/search/?q=<?=urlencode($search_query)?>&amp;f=arena">Arenateams</a> (<?=$arenaCount?>) </h3>
				<?php foreach($arenaRows as $row){ ?>
				<div class="grid">
					<div class="wowguild">
						<canvas id="flag-<?=$row["arenaTeamId"]?>" class="thumbnail" width="32" height="32" style="display: block; "></canvas>
						<script type="text/javascript">
				        //<![CDATA[
							$(function(){
								var flag<?=$row["arenaTeamId"]?> = new ArenaFlag('flag-<?=$row["arenaTeamId"]?>', {
									bg: [ '2', '<?=$row["backgroundColor"]?>' ],
									border: [ '22', '<?=$row["borderColor"]?>' ],
									emblem: [ '<?=$row["emblemStyle"]?>', '<?=$row["emblemColor"]?>' ]
								});
							});
						//]]>
        				</script>
						<a href="<?=$row["url"]?>" class="sublink">
							<strong><?=htmlentities($row["name"])?></strong>
						</a><br />
						<span class="color-tooltip-<?=$row["faction_style"]?>"><?=$row["realm"]?></span>
				        <span class="clear"><!-- --></span>
					</div>
				</div>
				<?php } ?>
				<span class="clear"><!-- --></span>
			</div>
		<?php } ?>
		<?php if($guildCount > 0){ ?>
			<div class="results results-grid wow-results">
				<h3 class="category "><a href="/search/?q=<?=urlencode($search_query)?>&amp;f=guild">Gilden</a> (<?=$guildCount?>) </h3>
				<?php foreach($guildRows as $row){ ?>
				<div class="grid">
					<div class="wowguild">
						<canvas id="tabard-<?=$row["guildid"]?>" class="thumbnail" width="32" height="32" style="display: block; "></canvas>
						<a href="/guild/<?=$row["realm"]?>/<?=urlencode($row["name"])?>/" class="sublink">
							<strong><?=htmlentities($row["name"])?></strong>
						</a><br />
						<span class="color-tooltip-<?=$row["faction_style"]?>"><?=$row["realm"]?></span>
				        <script type="text/javascript">
				        //<![CDATA[
						$(function(){
							var tabard<?=$row["guildid"]?> = new GuildTabard('tabard-<?=$row["guildid"]?>', {
								ring: '<?=$row["faction_style"]?>',
								bg: [ 0, '-1' ],
								border: [ '-1', '-1' ],
								emblem: [ '-1', '-1' ]
							});
						});
        				//]]>
        				</script>
						<span class="clear"><!-- --></span>
					</div>
				</div>
				<?php } ?>
				<span class="clear"><!-- --></span>
			</div>
		<?php } ?>
			<?php /*
			<div class="results post-results">
				<h3 class="category "><a href="?q=G%C3%BCrtelschnalle&amp;f=post">Forenergebnisse für Gürtelschnalle</a>
				(1036) </h3>
				<div class="result ">
					<h4 class="subcategory ">
					<a href="/wow/de-de/forum/topic/2313682107">Gürtelschnalle für Ringe?</a>
					<span class="small">(2 Antworten)</span>
					</h4>
					<div class="meta">
						<a href="/wow/de-de/forum/878732/" class="sublink">General</a> - Gepostet von <a href="/wow/de-de/search?sort=time&amp;a=Arthlas%40Aman'Thul" class="author">
						Arthlas </a> am 29.06.11 17:01
					</div>
					<div class="content">
						 Hallo, ich meine irgendwo gelesen zu haben das Juweliere sowas ähnliches wie die lederer herstellen können wie die Gürtelschnalle jedoch für Ringe, oder hab ich mich verlesen? wär froh über infos bzw …
					</div>
					<span class="clear">
					<!-- -->
					</span>
				</div>
				<div class="result ">
					<h4 class="subcategory ">
					<a href="/wow/de-de/forum/topic/1267110018">Ewige Gürtelschnalle auf 300+ Items?</a>
					<span class="small">(2 Antworten)</span>
					</h4>
					<div class="meta">
						<a href="/wow/de-de/forum/878727/" class="sublink">Profession</a> - Gepostet von <a href="/wow/de-de/search?sort=time&amp;a=Dewingher%40Area%2052" class="author">
						Dewingher </a> am 17.12.10 10:40
					</div>
					<div class="content">
						 Hallo, kann es sein das man die ewige Gürtelschnalle doch auf 300+ Items bringen kann, obwohl der Tooltip etwas anderes sagt? Da wäre wohl ein Fix mal erforderlich, sonst wird man als Schmied langsa…
					</div>
					<span class="clear">
					<!-- -->
					</span>
				</div>
				<div class="result ">
					<h4 class="subcategory "><span class="small status">Antworten:</span>
					<a href="/wow/de-de/forum/topic/927081454?page=19#372">Wie bekannt bist du...?</a>
					<span class="small">(497 Antworten)</span>
					</h4>
					<div class="meta">
						<a href="/wow/de-de/forum/940416/" class="sublink">Shattrath</a> - Gepostet von <a href="/wow/de-de/search?sort=time&amp;a=Darkota%40Shattrath" class="author">
						Darkota </a> am 19.05.11 05:55
					</div>
					<div class="content">
						 10/10 Giev Gürtelschnalle!
					</div>
					<span class="clear">
					<!-- -->
					</span>
				</div>
				<div class="result ">
					<h4 class="subcategory "><span class="small status">Antworten:</span>
					<a href="/wow/de-de/forum/topic/2601191797?page=16#309">Verbessere den Druiden über dir (die Dritte)</a>
					<span class="small">(309 Antworten)</span>
					</h4>
					<div class="meta">
						<a href="/wow/de-de/forum/878775/" class="sublink">Druid</a> - Gepostet von <a href="/wow/de-de/search?sort=time&amp;a=R%C3%A2ve%40Aman'Thul" class="author">
						Râve </a> am 19.02.12 20:31
					</div>
					<div class="content">
						 Gürtelschnalle fehlt :D
					</div>
					<span class="clear">
					<!-- -->
					</span>
				</div>
				<div class="result ">
					<h4 class="subcategory "><span class="small status">Antworten:</span>
					<a href="/wow/de-de/forum/topic/1302973478?page=6#103">Bewertet / Verbessert den Krieger über euch</a>
					<span class="small">(492 Antworten)</span>
					</h4>
					<div class="meta">
						<a href="/wow/de-de/forum/878882/" class="sublink">Warrior</a> - Gepostet von <a href="/wow/de-de/search?sort=time&amp;a=Naq%40Nera'thor" class="author">
						Naq </a> am 25.01.11 10:11
					</div>
					<div class="content">
						 Netter Tank xD Gürtelschnalle fehlt.
					</div>
					<span class="clear">
					<!-- -->
					</span>
				</div>
				<div class="view-all">
					<a href="?q=G%C3%BCrtelschnalle&amp;f=post" class="sublink">Alle Forenergebnisse einsehen</a>
				</div>
			</div>*/ ?>
		</div>
		<?php } elseif($search_filter == "character"){ 
		/* 
		 * 
		 *  C H A R A C T E R S
		 * 
		 */
		?>
		<div class="helpers">
			<h3 class="subheader ">Charakterergebnisse für <span><?php echo $search_query;?></span></h3>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "character")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$charCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<div class="view-table">
			<div class="table ">
				<table>
				<thead>
				<tr>
					<th class=" first-child">
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "character",
							"sort" => "subject",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "subject"){ echo ($dir == "a") ? "up" : "down"; }?>"> Name </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "character",
							"sort" => "level",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "level"){ echo ($dir == "a") ? "up" : "down"; }?>"> Stufe </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "character",
							"sort" => "race",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "race"){ echo ($dir == "a") ? "up" : "down"; }?>"> Volk </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "character",
							"sort" => "class",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "class"){ echo ($dir == "a") ? "up" : "down"; }?>"> Klasse </span>
						</a>
					</th>
					<th>
						<span class="sort-tab"><span> Fraktion </span></span>
					</th>
					<th>
						<span class="sort-tab"><span> Gilde </span></span>
					</th>
					<th>
						<span class="sort-tab"><span> Realm </span></span>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($charRows as $n => $row){?>
				<tr class="<?=$row["rowClass"]?>">
					<td>
						<a href="/character/<?=$row["realm"]?>/<?=urlencode($row["name"])?>/" class="item-link color-c<?=$row["class"]?>">
							<span class="icon-frame frame-18">
								<img src="<?=$row["icon"]?>" alt="" width="18" height="18"/>
							</span>
							<strong><?=$row["name"]?></strong>
						</a>
					</td>
					<td class="align-center"><?=$row["level"]?></td>
					<td class="align-center">
						<?=icon_race($row["race"],$row["gender"])?>
					</td>
					<td class="align-center">
						<?=icon_class($row["class"])?>
					</td>
					<td class="align-center">
						<?=icon_faction($row["faction"])?>
					</td>
					<td>
					<?php if(!empty($row["guild"])){?>
						<a href="/guild/<?=$row["realm"]?>/<?=urlencode($row["guild"])?>/" class="sublink"><?=$row["guild"]?></a>
					<?php } ?>
					</td>
					<td><?=$row["realm"]?></td>
				</tr>
				<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "character")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$charCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<?php } elseif($search_filter == "item"){
		/* 
		 * 
		 * 	I T E M S
		 * 
		 */
		?>
		<div class="helpers">
			<h3 class="subheader ">Gegenstandsergebnisse für <span><?php echo $search_query;?></span></h3>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "item")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$itemCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<div class="view-table">
			<div class="table ">
				<table>
				<thead>
				<tr>
					<th class=" first-child">
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "item",
							"sort" => "subject",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "subject"){ echo ($dir == "a") ? "up" : "down"; }?>"> Name </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "item",
							"sort" => "level",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "level"){ echo ($dir == "a") ? "up" : "down"; }?>"> Stufe </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "item",
							"sort" => "req",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "req"){ echo ($dir == "a") ? "up" : "down"; }?>"> Ben&ouml;tigt </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "item",
							"sort" => "type",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "type"){ echo ($dir == "a") ? "up" : "down"; }?>"> Art </span>
						</a>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($itemRows as $n => $row){?>
					<tr class="<?=$row["rowClass"]?>">
					<td>
						<a href="/item/<?=$row["entry"]?>/" class="item-link color-q<?=$row["quality"]?>">
							<span class="icon-frame frame-18">
								<img src="<?=$displayIds[$row["displayid"]]["icon"]?>" alt="" width="18" height="18"/>
							</span>
							<strong><?=htmlentities($row["name"])?></strong>
						</a>
					</td>
					<td class="align-center"><?=$row["ItemLevel"]?></td>
					<td class="align-center"><?=$row["RequiredLevel"]?></td>
					<td><?=$row["typeText"]?></td>
				</tr>
				<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "item")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$itemCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<?php } elseif($search_filter == "guild"){
		/* 
		 * 
		 * 	G U I L D S
		 * 
		 */
		?>
		<div class="helpers">
			<h3 class="subheader ">Gildenergebnisse für <span><?php echo $search_query;?></span></h3>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "guild")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$guildCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<div class="view-table">
			<div class="table ">
				<table>
				<thead>
				<tr>
					<th class=" first-child" colspan="2">
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "guild",
							"sort" => "subject",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "subject"){ echo ($dir == "a") ? "up" : "down"; }?>"> Name </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "guild",
							"sort" => "faction",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "faction"){ echo ($dir == "a") ? "up" : "down"; }?>"> Fraktion </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "guild",
							"sort" => "leader",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "leader"){ echo ($dir == "a") ? "up" : "down"; }?>"> Anf&uuml;hrer </span>
						</a>
					</th>
					<th>
						<span class="sort-tab"><span> Mitglieder </span></span></th>
					</th>
					<th>
						<span class="sort-tab"><span> Realm </span></span>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($guildRows as $n => $row){?>
					<tr class="<?=$row["rowClass"]?>">
					<td>
						<canvas id="tabard-<?=$row["guildid"]?>" class="thumbnail" width="32" height="32" style="display: block; "></canvas>
						<script type="text/javascript">
				        //<![CDATA[
						$(function(){
							var tabard<?=$row["guildid"]?> = new GuildTabard('tabard-<?=$row["guildid"]?>', {
								ring: '<?=$row["faction_style"]?>',
								bg: [ 0, <?=$row["BackgroundColor"]?> ],
								border: [ <?=$row["BorderStyle"]?>, <?=$row["BorderColor"]?> ],
								emblem: [ <?=$row["EmblemStyle"]?>, <?=$row["EmblemColor"]?> ]
							});
						});
        				//]]>
        				</script>
					</td>
					<td>	
						<a href="/guild/<?=$row["realm"]?>/<?=urlencode($row["name"])?>/">
							<strong><?=htmlentities($row["name"])?></strong>
						</a>
					</td>
					<td class="align-center"><?=icon_faction($row["faction"])?></td>
					<td>
						<a href="<?=($row["leader_url"])?>/" class="item-link color-c<?=$row["leader_class"]?>">
							<strong><?=htmlentities($row["leader_name"])?></strong>
						</a>
					</td>
					<td class="align-center"><?=$row["memberCount"]?></td>
					<td><?=$row["realm"]?></td>
				</tr>
				<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "item")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$guildCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<?php } elseif($search_filter == "arena"){
		/* 
		 * 
		 * 	A R E N A T E A M S
		 * 
		 */
		?>
		<div class="helpers">
			<h3 class="subheader ">Arenateamergebnisse für <span><?php echo $search_query;?></span></h3>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "arena")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$arenaCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<div class="view-table">
			<div class="table ">
				<table>
				<thead>
				<tr>
					<th class=" first-child" colspan="2">
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "arena",
							"sort" => "subject",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "subject"){ echo ($dir == "a") ? "up" : "down"; }?>"> Name </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "arena",
							"sort" => "type",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "type"){ echo ($dir == "a") ? "up" : "down"; }?>"> Modus </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "arena",
							"sort" => "faction",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "faction"){ echo ($dir == "a") ? "up" : "down"; }?>"> Fraktion </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "arena",
							"sort" => "leader",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "leader"){ echo ($dir == "a") ? "up" : "down"; }?>"> Kapit&auml;n </span>
						</a>
					</th>
					<th>
						<a href="<?=url_for("search","",array(
							"q" => ($search_query), 
							"f" => "arena",
							"sort" => "rating",
							"dir" => ($dir == "a") ? "d" : "a"))?>" class="sort-link">
							<span class="arrow <?php if($sort == "rating"){ echo ($dir == "a") ? "up" : "down"; }?>"> Wertung </span>
						</a>
					</th>
					<th>
						<span class="sort-tab"><span> Realm </span></span>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($arenaRows as $n => $row){?>
					<tr class="<?=$row["rowClass"]?>">
					<td>
						<canvas id="flag-<?=$row["arenaTeamId"]?>" class="thumbnail" width="32" height="32" style="display: block; "></canvas>
						<script type="text/javascript">
				        //<![CDATA[
							$(function(){
								var flag<?=$row["arenaTeamId"]?> = new ArenaFlag('flag-<?=$row["arenaTeamId"]?>', {
									bg: [ '2', '<?=$row["backgroundColor"]?>' ],
									border: [ '22', '<?=$row["borderColor"]?>' ],
									emblem: [ '<?=$row["emblemStyle"]?>', '<?=$row["emblemColor"]?>' ]
								});
							});
						//]]>
        				</script>
					</td>
					<td>	
						<a href="<?=$row["url"]?>">
							<strong><?=htmlentities($row["name"])?></strong>
						</a>
					</td>
					<td class="align-center"><?=$row["typeText"]?></td>
					<td class="align-center"><?=icon_faction($row["faction"])?></td>
					<td>
						<a href="/character/<?=$row["realm"]?>/<?=urlencode($row["leader_name"])?>/" class="item-link color-c<?=$row["leader_class"]?>">
							<strong><?=htmlentities($row["leader_name"])?></strong>
						</a>
					</td>
					<td class="align-center"><?=$row["rating"]?></td>
					<td><?=$row["realm"]?></td>
				</tr>
				<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
		<div class="data-options">
			<div class="option">
			<?php echo paginate($numPages, $page, array("search", "", array("q" => ($search_query), "f" => "arena")));?>
			</div>
			Zeige <strong class="results-start"><?=$firstResultNumber?></strong>–<strong class="results-end"><?=$lastResultNumber?></strong> 
			von <strong class="results-total"><?=$arenaCount?></strong> Ergebnissen
			<span class="clear"><!-- --></span>
		</div>
		<?php } ?>
	</div>
	<div class="search-left">
		<div class="search-header">
			<h2 class="header "> Suchen </h2>
		</div>
		<?php if($no_results == false){?>
		<ul class="dynamic-menu" id="menu-search">
			<li <? if($search_filter == "none"){ echo 'class="item-active"';}?>>
				<a href="/search/?q=<?php echo $search_query; ?>"> <span class="arrow">Alle</span> </a>
			</li>
		<?php if($arenaCount > 0){?>
			<li <? if($search_filter == "arena"){ echo 'class="item-active"';}?>>
				<a href="/search/?q=<?php echo $search_query; ?>&amp;f=arena">
					<span class="arrow">Arenateams <span>(<?=$arenaCount?>)</span></span> 
				</a>
			</li>
		<?php } ?>
		<?php if($charCount > 0){?>
			<li <? if($search_filter == "character"){ echo 'class="item-active"';}?>>
				<a href="/search/?q=<?php echo $search_query; ?>&amp;f=character"> 
					<span class="arrow">Charakter <span>(<?=$charCount?>)</span></span>
				</a>
			</li>
		<?php } ?>
		<?php if($itemCount > 0){?>
			<li <? if($search_filter == "item"){ echo 'class="item-active"';}?>>
				<a href="/search/?q=<?php echo $search_query; ?>&amp;f=item">
					<span class="arrow">Gegenstand <span>(<?=$itemCount?>)</span> </span>
				</a>
			</li>
		<?php } ?>
		<?php if($guildCount > 0){?>
			<li <? if($search_filter == "guild"){ echo 'class="item-active"';}?>>
				<a href="/search/?q=<?php echo $search_query; ?>&amp;f=guild">
					<span class="arrow">Gilden <span>(<?=$guildCount?>)</span> </span>
				</a>
			</li>
		<?php } ?>
		</ul>
		<?php } ?>
	</div>
	<span class="clear">
	<!-- -->
	</span>
</div>