<div lang="en" style="display: none;">
	<div id="cat-<?=$achievementCategory?>" class="container<? if($achievementCategory == 81){ echo " feats"; } ?>">
		<h3 class="category"><?=$catLabel?></h3>
		<div class="profile-progress border-4" onmouseover="Tooltip.show(this, &#39;<?$pointsEarned." / ".$pointsTotal?> Punkte&#39;, { location: &#39;middleRight&#39; });">
			<div class="bar border-4 hover" style="width: <?=$percentageComplete?>%">
			</div>
			<div class="bar-contents">
				 <?=$countComplete." / ".$countTotal." (".$percentageComplete."%)"?>
			</div>
		</div>
		<ul>
			
		<?php foreach($allAchievements as $achievement){ ?>
		<li class="achievement <?=$achievement["cssClass"]?>" data-id="<?=$achievement["data"]["id"]?>" data-href="#<?=$achievement["data"]["link"]?>">
			<p>
				<strong><?=htmlentities($achievement["data"]["title"])?></strong>
				<span><?=htmlentities($achievement["data"]["desc"])?></span>
			</p>
			<a href="javascript:;" data-fansite="achievement|<?=$achievement["data"]["id"]?>" class="fansite-link "></a>
			<?php if($achievement["expandable"]){?><div class="icon-expandable"> </div><?php } ?>
			<div class="meta-achievements">
			<?php if(isset($achievement["achievement_tree"])){ ?>
				<ul class="sub-achievements">
					<?php foreach($achievement["achievement_tree"] as $sub){ ?>
					<li data-achievement="<?=$sub["id"]?>">
						<span class="icon-frame frame-36 " style='background-image: url("/images/icons/36/<?=$sub["icon"]?>.jpg");'></span>
						<span class="points border-3"><?=$sub["points"]?></span>
					</li>
					<?php } ?>
				</ul>
			<?php } ?>
			<?php if(isset($achievement["criteria"]) && isset($criteria[0]["maxQuantity"],$criteria[0]["quantity"])){ ?>
				<?php foreach($achievement["criteria"] as $criteria){ ?>
				<div class="profile-progress border-4 completed">
					<div class="bar border-4 hover" style="width: 100%">
					</div>
					<div class="bar-contents">
						 <?=$criteria["quantity"]." / ".$criteria["maxQuantity"]." (".round($criteria["quantity"]/$criteria["maxQuantity"]*100)."%)"?>
					</div>
				</div>
				<?php } ?>
			<?php } elseif(isset($achievement["criteria"])) { ?>
				<ul>
				<?php foreach($achievement["criteria"] as $criteria){ ?>
					<li<? if(!empty($criteria["date"])){ echo ' class="unlocked"'; } ?>><?=htmlentities($criteria["name"])?></li>
				<?php } ?>
				</ul>
			<?php } ?>
			</div>
			<span class="icon-frame frame-50 ">
				<img src="/images/icons/56/<?=$achievement["data"]["icon"]?>.jpg" alt="" width="50" height="50"/>
			</span>
			<div class="points-big border-8">
				<strong><?=$achievement["data"]["points"]?></strong>
				<span class="date">01.08.2011 </span>
			</div>
		</li>
		<?php } ?>
		
		<?php if(false){ ?>
			
		
			<li class="achievement locked has-sub" data-id="5455" data-href="#92:a5455">
			<p>
				<strong>Ich wär&#39; so gerne Millionär</strong>
				<span>Plündert 50.000 Gold.</span>
			</p>
			<a href="javascript:;" data-fansite="achievement|5455" class="fansite-link "></a>
			<div class="icon-expandable">
			</div>
			<div class="meta-achievements">
				<div class="profile-progress border-4">
					<div class="bar border-4 hover" style="width: 69%">
					</div>
					<div class="bar-contents">
						<span class="icon-gold">34.782</span>
						<span class="icon-silver">72</span>
						<span class="icon-copper">37</span>
					</div>
				</div>
			</div>
			<span class="icon-frame frame-50 ">
			<img src="http://eu.media.blizzard.com/wow/icons/56/inv_misc_coin_01.jpg" alt="" width="50" height="50"/>
			</span>
			<div class="points-big border-8">
				<strong>60</strong>
			</div>
			</li>
			<li class="achievement locked has-sub" data-id="1250" data-href="#92:a1250">
			<p>
				<strong>Haustierepidemie</strong>
				<span>Sammelt 50 einzigartige Haustiere.</span>
			</p>
			<a href="javascript:;" data-fansite="achievement|1250" class="fansite-link "></a>
			<div class="icon-expandable">
			</div>
			<div class="meta-achievements">
				<div class="profile-progress border-4">
					<div class="bar border-4 hover" style="width: 74%">
					</div>
					<div class="bar-contents">
						 37 / 50 (74%)
					</div>
				</div>
			</div>
			<span class="icon-frame frame-50 ">
			<img src="http://eu.media.blizzard.com/wow/icons/56/inv_box_petcarrier_01.jpg" alt="" width="50" height="50"/>
			</span>
			<div class="points-big border-8">
				<strong>40</strong>
			</div>
			<div class="reward">
				 Belohnung: <a href="/wow/de/item/40653" class="color-q3">Miefende Haustiertragevorrichtung</a>
			</div>
			</li>
				<li class="achievement locked has-sub" data-id="2142" data-href="#92:a2142">
				<p>
					<strong>Die Scheune wird voll</strong>
					<span>Erhaltet 25 Reittiere.</span>
				</p>
				<a href="javascript:;" data-fansite="achievement|2142" class="fansite-link "></a>
				<div class="icon-expandable">
				</div>
				<div class="meta-achievements">
					<div class="profile-progress border-4">
						<div class="bar border-4 hover" style="width: 76%">
						</div>
						<div class="bar-contents">
							 19 / 25 (76%)
						</div>
					</div>
				</div>
				<span class="icon-frame frame-50 ">
				<img src="http://eu.media.blizzard.com/wow/icons/56/ability_mount_ridingelekkelite_green.jpg" alt="" width="50" height="50"/>
				</span>
				<div class="points-big border-8">
					<strong>20</strong>
				</div>
				</li>
				<?php }?>
			</ul>
		</div>
	</div>