

<div id="profile-wrapper" class="profile-wrapper profile-wrapper-<?=$char->GetCssFaction()?>">
	<div class="profile-sidebar-anchor">
		<div class="profile-sidebar-outer">
			<div class="profile-sidebar-inner">
				<div class="profile-sidebar-contents">
					<?php echo $sidebar_character; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-contents">
		<div class="profile-section-header">
			<h3 class="category ">PvP</h3>
		</div>
		<div class="profile-section">
			<?php if($char->GetTotalKills() > 0){?>
			<div class="pvp-weekly-best">
				 Ehrenwerte Kills insgesamt <span class="rating"><?=$char->GetTotalKills()?></span>
			</div>
			<?php } ?>
			<?php if($bestWeeklyRating > 0){?>
			<div class="pvp-weekly-best">
				 Beste eigene Wertung der Woche <span class="rating"><?=$bestWeeklyRating?></span> (<?=$bestWeeklyType?>)
			</div>
			<?php } ?>
			<div id="pvp-tabs" class="pvp-tabs">
				<?php foreach($arenaTeams as $type => $team){ ?>
				<div class="tab <?=(($team->selected)? 'tab-selected' : '')?>" id="pvp-tab-<?=$type?>" data-id="<?=$type?>">
					<span class="type"><?=$type?></span>
					<div class="arenateam-flag-simple">
						<canvas id="arenateam-flag-simple-<?=$type?>" width="128" height="128" style="display: inline; ">
							<div class="arenateam-flag-simple-default"></div>
						</canvas>
						<script type="text/javascript">
						//<![CDATA[
							$(document).ready(function() {
								var flag = new ArenaFlag('arenateam-flag-simple-<?=$type?>', {
									bg: [ '<?=$team->backgroundStyle?>', '<?=$team->backgroundColor?>' ],
									border: [ '<?=$team->borderStyle?>', '<?=$team->borderColor?>' ],
									emblem: [ '<?=$team->emblemStyle?>', '<?=$team->emblemColor?>' ]
								}, true);
							});
        				//]]>
        				</script>
					</div>
					<ul class="ratings">
						<li><span class="rank">#<?=$team->rank?></span></li>
						<li>
							<span class="value"><?=$team->rating?></span>
							<span class="name">Team</span>
						</li>
						<li>
							<span class="value"><?=$team->personalRating?></span>
							<span class="name">Eigene</span>
						</li>
					</ul>
				</div>
				<?php } ?>
				<span class="clear"><!-- --></span>
			</div>
			<div id="pvp-tabs-content" class="pvp-tabs-content">
				<?php foreach($arenaTeams as $type => $team){ ?>
				<div class="tab-content" id="pvp-tab-content-<?=$type?>" style="">
					<div class="arenateam-stats">
						<table>
						<thead>
						<tr>
							<th class="align-left">
								<span class="sort-tab"><a class="team-name" href="<?=$team->GetUrl()?>"><?=$team->GetName()?></a></span>
							</th>
							<th width="23%" class="align-center">
								<span class="sort-tab">Spiele</span>
							</th>
							<th width="23%" class="align-center">
								<span class="sort-tab">Siege - Niederlagen</span>
							</th>
							<th width="23%" class="align-center">
								<span class="sort-tab">Teamwertung</span>
							</th>
						</tr>
						</thead>
						<tbody>
						<tr class="row2">
							<td class="align-left">
								<strong class="week">Diese Woche</strong>
							</td>
							<td class="align-center"><?=$team->weekGames?></td>
							<td class="align-center arenateam-gameswonlost">
								<span class="arenateam-gameswon"><?=$team->weekWins?></span> - <span class="arenateam-gameslost"><?=$team->weekLosses?></span>
								<span class="arenateam-percent">(<?=$team->weekPercentage?>%)</span>
							</td>
							<td class="align-center">
								<span class="arenateam-rating"><?=$team->rating?></span>
							</td>
						</tr>
						<tr class="row1">
							<td class="align-left">
								<strong class="season">Saison</strong>
							</td>
							<td class="align-center">
								<?=$team->seasonGames?>
							</td>
							<td class="align-center arenateam-gameswonlost">
								<span class="arenateam-gameswon"><?=$team->seasonGames?></span> - <span class="arenateam-gameslost"><?=$team->seasonLosses?></span>
								<span class="arenateam-percent">(<?=$team->seasonPercentage?>%)</span>
							</td>
							<td class="align-center">
								<span class="arenateam-rating"><?=$team->rating?></span>
							</td>
						</tr>
						</tbody>
						</table>
					</div>
					<span class="clear"><!-- --></span>
					<div class="pvp-roster">
						<div class="ui-dropdown" id="filter-timeframe-<?=$type?>">
							<select style="display: none; ">
								<option value="season">Nach Saison</option>
								<option value="weekly">Nach Wochen</option>
							</select>
						</div>
						<h3 class="category ">Verzeichnis</h3>
						<div class="arenateam-roster table" id="arena-roster-<?=$type?>">
							<table>
							<thead>
							<tr>
								<th>
									<a href="javascript:;" class="sort-link">
									<span class="arrow">Name</span>
									</a>
								</th>
								<th style="display: none" class="align-center season">
									<a href="javascript:;" class="sort-link numeric">
									<span class="arrow">Saison gespielt</span>
									</a>
								</th>
								<th style="display: none" class="align-center season">
									<a href="javascript:;" class="sort-link numeric">
									<span class="arrow">Saison: Siege - Niederlagen</span>
									</a>
								</th>
								<th class="align-center weekly">
									<a href="javascript:;" class="sort-link numeric">
									<span class="arrow down">Gespielt</span>
									</a>
								</th>
								<th class="align-center weekly">
									<a href="javascript:;" class="sort-link numeric">
									<span class="arrow">Siege - Niederlagen</span>
									</a>
								</th>
								<th class="align-center">
									<a href="javascript:;" class="sort-link numeric">
									<span class="arrow">Wertung</span>
									</a>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach($team->members as $n => $member){?>
							<tr class="<?=(($n % 2 == 0) ? "row1" : "row2")?>" style="display: table-row; ">
								<td data-raw="<?=urlencode($member->GetName())?>" style="width: 40%">
									<a href="<?=$member->GetUrl()?>/talent" rel="np">
										<span class="character-talents">
											<span class="icon">
												<span class="icon-frame frame-12 ">
													<img src="<?=$member->GetTalentIcon(18)?>" alt="" width="12" height="12"/>
												</span>
											</span>
											<span class="points"><?=$member->GetTalentDataString()?></span>
											<span class="clear"><!-- --></span>
										</span>
									</a>
									<a href="<?=$member->GetUrl()?>" class="color-c<?=$member->GetClass()?>" rel="allow">
										<span class="icon-frame frame-14 ">
											<img src="/images/icons/race/<?=$member->GetRace()."-".$member->GetGender()?>.gif" alt="" width="14" height="14"/>
										</span>
										<span class="icon-frame frame-14 ">
											<img src="/images/icons/class/<?=$member->GetClass()?>.gif" alt="" width="14" height="14"/>
										</span>
										<?=$member->GetName()?>
									</a>
									<?php if($member->GetGUID() == $team->GetCaptainGUID()){?>
									<span class="leader" data-tooltip="Teamkapitän"></span>
									<?php }?>
								</td>
								<td class="align-center season" data-raw="<?=$member->seasonGames?>">
									 <?=$member->seasonGames?> <span class="arenateam-percent">(<?=$member->seasonAttendance?>%)</span>
								</td>
								<td class="align-center season arenateam-gameswonlost" data-raw="<?=$member->seasonWins?>">
									<span class="arenateam-gameswon"> <?=$member->seasonWins?></span> – <span class="arenateam-gameslost"> <?=$member->seasonLosses?></span>
									<span class="arenateam-percent">(<?=$member->seasonPercentage?>%)</span>
								</td>
								<td class="align-center weekly" style="display: none" data-raw="<?=$member->weekGames?>">
									 <?=$member->weekGames?> <span class="arenateam-percent">(<?=$member->weekAttendance?>%)</span>
								</td>
								<td class="align-center weekly arenateam-gameswonlost" data-raw="<?=$member->weekWins?>" style="display: none">
									<span class="arenateam-gameswon"><?=$member->weekWins?></span> – <span class="arenateam-gameslost"><?=$member->weekLosses?></span>
									<span class="arenateam-percent">(<?=$member->weekPercentage?>%)</span>
								</td>
								<td class="align-center">
									<span class="arenateam-rating"><?=$member->personalRating?></span>
								</td>
							</tr>
							<?php } ?>
							</tbody>
							</table>
						</div>
						<script type="text/javascript">
			        //<![CDATA[
					$(document).ready(function() {
						new Table('#arena-roster-<? echo $type;?>', { column: 3, method: 'numeric', type: 'desc' });
					});
			        //]]>
			        </script>
					</div>
					<script type="text/javascript">
			        //<![CDATA[
						$(document).ready(function() {
							$('#filter-timeframe-<?=$type?>').dropdown({
								callback: function(dropdown, value) {
									Arena.swapStats('#arena-roster-<?=$type?>', value, dropdown);
								}
							});
						});
			        //]]>
			        </script>
				</div>
				<?php } ?>
				<span class="clear"><!-- --></span>
			</div>
			
		</div>
		<script type="text/javascript">
        //<![CDATA[
			$(document).ready(function() {
				Pvp.initialize();
			});
        //]]>
        </script>
	</div>
	<span class="clear"><!-- --></span>
</div>
<script type="text/javascript">
//<![CDATA[
$(function() {
	Profile.url = '<?=$char->GetUrl()?>/pvp';
});
var MsgProfile = {
	tooltip: {
		feature: { notYetAvailable: "Diese Funktion ist derzeit noch nicht verfügbar."},
		vault: {
			character: "Diese Sektion ist nur verfügbar, wenn du mit diesem Charakter eingeloggt bist.",
			guild: "Diese Sektion ist nur verfügbar, wenn du mit einem Charakter aus dieser Gilde eingeloggt bist."
		}
	}
};
//]]>
</script>
<script type="text/javascript" src="/<?=$currtmp?>/js/table.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/dropdown.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/profile.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/pvp.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/arena-flags.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/arena.js"></script>
