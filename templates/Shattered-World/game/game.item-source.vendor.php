<div class="related-content" id="related-vendors" style="display: block; ">
	<div class="filters inline">
		<div class="keyword"> 
			<span class="view"></span> 
			<span class="reset" style="display: none"></span>
			<input id="filter-name-dropCreatures" type="text" class="input filter-name" data-filter="row" maxlength="25" title="Filter..." value="Filter..." />
		</div>
		<span class="clear"><!-- --></span>
	</div>
	<div class="data-options-top">
		<div class="table-options data-options ">
			<div class="option">
				<ul class="ui-pagination"></ul>
			</div> 
			Zeige <strong class="results-start">1</strong>–<strong class="results-end"><?=min(array(count($sources),50))?></strong> von <strong class="results-total"><?=count($sources)?></strong> Ergebnissen 
			<span class="clear"><!-- --></span>
		</div>
	</div>
	<div class="table full-width">
		<table>
			<thead>
				<tr>
					<th> <a href="javascript:;" class="sort-link"> <span class="arrow up">Name</span> </a> </th>
					<th class="align-center" width="60"> <a href="javascript:;" class="sort-link numeric"> <span class="arrow">Stufe</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link"> <span class="arrow">Ort</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link"> <span class="arrow">Kosten</span> </a> </th>
				</tr>
			</thead>
			<tbody>
			<? foreach($sources as $source){ ?>
				<tr class="row<?=(($source["n"] % 2 == 0) ? 1 : 2)?>" style="display: table-row;">
					<td data-raw="<?=$source["raw_name"]?>">
						<?=$source["fansite"]?>
						<?=$source["name"]?>
					</td>
					<td class="align-center" data-raw="<?=$source["level"]?>"> 
						<?=$source["level"]?> 
						<? if(!empty($source["rank"])){ ?><em>(<?=$source["rank"]?>)</em><? } ?></td>
					<td data-raw="<?=$source["locationName"]?>">
						<?=$source["locations"]?>
					</td>
					<td>
						<?=$source["cost"]?>
					</td>
				</tr>
			<? } ?>
			<? if(count($sources) <= 0){ ?>
				<tr class="no-results">
					<td colspan="7" class="align-center"> Keine Ergebnisse gefunden. </td>
				</tr>
			<? } ?>
			</tbody>
		</table>
	</div>
	<div class="data-options-bottom">
		<div class="table-options data-options ">
			<div class="option">
				<ul class="ui-pagination"></ul>
			</div> 
			Zeige <strong class="results-start">1</strong>–<strong class="results-end"><?=min(array(count($sources),50))?></strong> von <strong class="results-total"><?=count($sources)?></strong> Ergebnissen 
			<span class="clear"><!-- --></span>
		</div>
	</div>
</div>
<script type="text/javascript">
//<![CDATA[
	Wiki.related['vendors'] = new WikiRelated('vendors', {
		paging: true,
		totalResults: <?=count($sources)?>,
		column: 0,
		method: 'default',
		type: 'asc'
	});
//]]>
</script> 
