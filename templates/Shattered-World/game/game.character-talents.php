<style type="text/css">
.talentcalc-cell .icon .texture { background-image: url(http://eu.battle.net/wow-assets/static/images/talents/icons/<?=$char->GetClass()?>-greyscale.jpg); }
</style>		
<div id="profile-wrapper" class="profile-wrapper profile-wrapper-<?=$char->GetCssFaction()?>">
	<div class="profile-sidebar-anchor">
		<div class="profile-sidebar-outer">
			<div class="profile-sidebar-inner">
				<div class="profile-sidebar-contents">
					<?php echo $sidebar_character; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-contents">
		<div class="profile-section-header">
			<ul class="profile-tabs">
			<? foreach($talentSpecs as $n => $spec){?>
				<li class="<? if($spec["shown"]){ echo "tab-active";}?>">
					<a href="<?=$char->GetCharacterLink()?>/talent/<?=$spec["type"]?>" rel="np">
						<span class="r"><span class="m">
							<span class="icon-frame frame-14 ">
								<img src="<?=$spec["icon"]?>" alt="" width="14" height="14"/>
							</span>
							<? if($spec["active"]){?><span class="active-spec"></span><? } ?>
							<?=$spec["typeText"]?> 
						</span></span>
					</a>
				</li>
			<?php }?>
			</ul>
		</div>
		<div class="profile-section">
			<div class="character-talents-wrapper">
				<div id="talentcalc-character" class="talentcalc talentcalc-locked">
					<?php foreach ($talentTabs as $specId => $tab){ ?>
					<div class="talentcalc-tree-wrapper <? if($tab["n"] == 2){echo "tree-last";}?> <?=(($tab["active"])?"tree-specialization":"tree-nonspecialization")?>">
						<div class="talentcalc-tree-button" style="display: none; ">
							<button class="ui-button button1" type="submit">
								<span><span>Heilig</span></span>
							</button>
						</div>
						<div class="talentcalc-tree-header" style="visibility: visible; ">
							<span class="icon">
								<span class="icon-frame-treeheader ">
									<img src="<?=$tab["icon"]?>" alt="" width="36" height="36"/>
									<span class="frame"></span>
									<span class="roles">
									<?php if($tab["role"] == "tank-dps"){?>
										<span class="icon-dps"></span>
										<span class="icon-tank"></span>
									<?php }else{?>
										<span class="icon-<?=$tab["role"]?>"></span>
									<?php }?>
									</span>
								</span>
							</span>
							<span class="points"><?=$tab["points"]?></span>
							<span class="name"><?=$tab["name"]?></span>
							<span class="clear"><!-- --></span>
						</div>
						<div class="talentcalc-tree" style="width: 220px; height: 600px; background-image: url(/images/character/talent-calculator/backgrounds/<?=$char->GetClass()."-".$tab["n"]?>.jpg); background-position: -0px 0">
							<div class="talentcalc-cells-wrapper">
							<?php foreach ($talentCells[$tab["n"]] as $cell){?>
								<div class="talentcalc-cell" style="left: <?=($cell["Col"]*53)?>px; top: <?=($cell["Row"]*53)?>px;" data-id="<?=$cell["TalentID"]?>">
									<span class="icon">
										<span class="texture" style="background-image:url(/images/icons/36/<?=$cell["icon"]?>.grey.jpg);"></span>
										<span class="texture-color" style="background-image:url(/images/icons/36/<?=$cell["icon"]?>.jpg);"></span>
										<? if($cell["ability"]){?><span class="ability"></span><?}?>
										<span class="frame"></span>
									</span>
									<a href="javascript:;" class="interact"><span class="hover"></span></a>
									<span class="points"><span class="frame"></span><span class="value">0</span></span>
									<? if(!empty($cell["arrow"])){ echo $cell["arrow"]; } ?>
								</div>
							<?php }?>
							</div>
						</div>
						<div class="talentcalc-tree-overview" style="width: 228px; height: 387px; display: none; "></div>
					</div>
					<?php } ?>
					<span class="clear"><!-- --></span>
					<?php /*
					<div class="talentcalc-bottom">
						<div class="calcmode">
							<a href="javascript:;">Exportieren zum Talentrechner</a>
						</div>
						<div class="talentcalc-buttons">
							<button class="ui-button button2 " type="submit">
								<span>
									<span>übersicht anzeigen</span>
								</span>
							</button>
						</div>
						<span class="clear">
						<!-- -->
						</span>
					</div>
				</div>*/?>
				<script type="text/javascript">
        //<![CDATA[
		$(document).ready(function() {
			new TalentCalculator({
				id: "character",
				classId: <?=$char->GetClass()?>,
				calculatorMode: false,
				petMode: false,
				glyphMode: false,
				build: "<?=$shownBuild?>",
				glyphs: [<?=$charGlyphString?>],
				callback: null,
				nTrees: 3
		});
		});
		var MsgTalentCalculator = {
			talents: {
				tooltip: {
					rank: "Rang {0} / {1}",
					primaryTree: "Verwende zuerst {0} Punkte in deinem primären Talentbaum",
					reqTree: "Benötigt {0} Punkte in {1} Talenten",
					reqTalent: "Benötigt {0} Punkte in {1}",
					nextRank: "Nächster Rang:",
					click: "Zum Lernen klicken",
					rightClick: "Zum Verlernen rechts klicken"
				}
			},
			buttons: {
				overviewPane: {
					show: "übersicht anzeigen",
					hide: "Talentbäume anzeigen"
				}
			},
			info: {
				beastMastery: {
					tooltip: {
						title: "Tierherrschaft",
						description: "Füge 4 Bonuspunkte durch das Talent Tierherrschaft des Jägers dazu."
					}
				}
			}
		};
        //]]>
        </script>
			</div>
			<div class="character-glyphs" id="character-glyphs">
				<h3 class="category">Glyphen</h3>
				<div class="profile-box-full">
					<div class="character-glyphs-column glyphs-prime">
						<h4 class="subcategory ">Erhebliche</h4>
						<ul>
						<?php foreach($majorGlyphs as $glyph){?>
							<li class="filled">
								<a href="/item/<?=$glyph["item"]?>" class="color-q1">
									<span class="icon">
										<span class="icon-frame frame-27 ">
											<img src="/images/icons/36/<?=$glyph["icon"]?>.jpg" alt="" width="27" height="27"/>
										</span>
									</span>
									<span class="name"><?=$glyph["name"]?></span>
								</a>
								<span class="clear"><!-- --></span>
							</li>
						<?php } ?>
						</ul>
					</div>
					<div class="character-glyphs-column glyphs-major">
						<h4 class="subcategory ">Geringe</h4>
						<ul>
						<?php foreach($minorGlyphs as $glyph){?>
							<li class="filled">
								<a href="/item/<?=$glyph["item"]?>" class="color-q1">
									<span class="icon">
										<span class="icon-frame frame-27 ">
											<img src="/images/icons/36/<?=$glyph["icon"]?>.jpg" alt="" width="27" height="27"/>
										</span>
									</span>
									<span class="name"><?=$glyph["name"]?></span>
								</a>
								<span class="clear"><!-- --></span>
							</li>
						<?php } ?>
						</ul>
					</div>
					<span class="clear"><!-- --></span>
				</div>
			</div>
		</div>
	</div>
	<span class="clear"><!-- --></span>
</div>
<script type="text/javascript" src="/<?=$currtmp?>/js/profile.js?v20"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/talent-calculator.js?v=<?=time()?>"></script>
