<style type="text/css">
#content .content-top { background: url("/<?=$currtmp?>/images/zone/bgs/<?=$zone->label?>.jpg") 0 0 no-repeat; }

.table thead th {
padding: 0;
background: #4D1A08 url("/templates/Shattered-World/images/table-header.gif") 0 100% repeat-x;
border-bottom: 1px solid #1A0F08;
border-left: 0px solid #7C2804;
border-right: 0px solid #391303;
border-top: 0px solid #7C2804;
white-space: nowrap;
}

</style>

<div id="wiki" class="wiki wiki-zone">
	<div class="sidebar">
		<table class="media-frame">
			<tr>
				<td class="tl"></td>
				<td class="tm"></td>
				<td class="tr"></td>
			</tr>
			<tr>
				<td class="ml"></td>
				<td class="mm"><a href="javascript:;" class="thumbnail" onClick="Lightbox.loadImage([{ src: '/templates/Shattered-World/images/zone/screenshots/<?=$zone->label?>.jpg' }]);"> <span class="view"></span> <img src="/templates/Shattered-World/images/zone/thumbnails/<?=$zone->label?>.jpg" width="265" alt="" /> </a></td>
				<td class="mr"></td>
			</tr>
			<tr>
				<td class="bl"></td>
				<td class="bm"></td>
				<td class="br"></td>
			</tr>
		</table>
		<div class="snippet">
			<h3>Schnellinfos</h3>
			<ul class="fact-list">
				<li> <span class="term">Typ:</span> <?=$zone->getType()?></li>
				<li> <span class="term">Spieler:</span> <?=$zone->getSize()?></li>
				<li> <span class="term">Stufe:</span> <?=$zone->getLevel()?></li>
				<? if(!empty($zone->location)){ ?><li> <span class="term">Ort:</span> <?=$zone->location?></li><? } ?>
				<? if(!empty($zone->patch)){ ?><li> <span class="term">Eingeführt mit Patch:</span> <?=$zone->patch?></li><? } ?>
				<? 
				if( $zone->is_heroic && $zone->heroic_closed ){ ?><li class="color-tooltip-red">Heroischer Modus geschlossen <span class="icon-heroic-skull"></span></li><? 
				} else if( $zone->is_heroic ){ ?><li>Heroischer Modus verfügbar <span class="icon-heroic-skull"></span></li><? } ?>
			</ul>
		</div>
		<div class="snippet">
			<h3>Karte</h3>
			<table class="media-frame">
				<tr>
					<td class="tl"></td>
					<td class="tm"></td>
					<td class="tr"></td>
				</tr>
				<tr>
					<td class="ml"></td>
					<td class="mm"><a href="javascript:;" id="map-floors" class="thumbnail" style="background: url(/templates/Shattered-World/images/zone/maps/<?=$zone->label?>.jpg) 0 0 no-repeat;"> <span class="view"></span> </a></td>
					<td class="mr"></td>
				</tr>
				<tr>
					<td class="bl"></td>
					<td class="bm"></td>
					<td class="br"></td>
				</tr>
			</table>
			<script type="text/javascript">
			//<![CDATA[
			$(function() {
				Zone.floors = [
				<?
				foreach($zone->floors as $key => $title){
					
					echo "\n\t\t".'{ title: "'.$title.'", src: "/templates/Shattered-World/images/zone/maps-large/'.$zone->label.$key.'-large.jpg"},';
				} ?>
				];
			});
			//]]>
			</script>
			<div class="radio-buttons" id="map-radios"> 
				<? 
				foreach($zone->floors as $key => $title){ 
					echo "\n\t\t".'<a href="javascript:;" id="map-radio-'.$key.'" data-id="'.$key.'" data-tooltip="'.$title.'"> </a>';
				} 
				?>
			</div>
		</div>
		<div class="snippet">
			<h3>Erfahrt mehr</h3>
			<span id="fansite-links" class="fansite-group">
				<a href="http://de.wowhead.com/zone=<?=$zone_id?>" target="_blank">Wowhead</a> 
				<a href="http://de.wow.wikia.com/wiki/<?=str_replace(" ","_",html_entity_decode($zone->name))?>" target="_blank">Wowpedia</a> 
				<a href="http://wowdata.buffed.de/?zone=<?=$zone_id?>" target="_blank">Buffed.de</a>
			</span> 
		</div>
	</div>
	<div class="info">
		<div class="title">
			<h2><?=$zone->name?></h2>
			<? if($zone->expansion == 2) { ?>
			<span class="expansion-name color-ex2"> Benötigt Wrath of the Lich King </span>
			<? } else if($zone->expansion == 1) { ?>
			<span class="expansion-name color-ex1"> Benötigt The Burning Crusade </span>
			<? } ?>
		</div>
		<p class="intro"> <?=$zone->intro?> </p>
		<div class="lore">
			<p><?=$zone->lore?> </p>
		</div>
		<div class="panel">
			<div class="panel-title">Bosse</div>
			<div class="zone-bosses">
				<div class="boss-column-portrait">
			<?
			if(count($zone->wings) > 0){
				foreach($zone->wings as $wing_data){
					echo '<span class="wing-name">'.$wing_data["name"].'</span>';
					$n++;
					
					foreach($wing_data["bosses"] as $boss){
						?>
						<div class="boss-avatar"> 
							<a href="/game/zone/<?=$zone->label?>/<?=$boss["label"]?>" data-npc="<?=$boss["id"]?>"> 
								<span class="boss-portrait" style="background-image: url('/templates/Shattered-World/images/npcs/creature<?=$boss["id"]?>.jpg');"> </span> 
								<span class="boss-details">	
									<div class="boss-name"> <?=$boss["name"]?> </div> 
									<? if($boss["closed"]){ ?><div class="color-tooltip-red">Dieser Kampf ist geschlossen.</div><? }?>
								</span> 
							</a> 
						</div>
						<?
					}
						
					if($n < count($zone->wings)){
						echo "\n\t\t\t\t".'</div>';
						if($n % 2 == 0)
							echo '<span class="clear"><!-- --></span>';
						echo "\n\t\t\t\t".'<div class="boss-column-portrait">';
					}
					
					
				} 
			}
			else{ 
				$half = ceil(count($zone->bosses) / 2);
				$n = 0;
				foreach($zone->bosses as $boss){
					$n++;
					?>
						<div class="boss-avatar"> 
							<a href="/game/zone/<?=$zone->label?>/<?=$boss["label"]?>" data-npc="<?=$boss["id"]?>"> 
								<span class="boss-portrait" style="background-image: url('/templates/Shattered-World/images/npcs/creature<?=$boss["id"]?>.jpg');"> </span> 
								<span class="boss-details">	
									<div class="boss-name"> <?=$boss["name"]?> </div> 
									<? if($boss["closed"]){ ?><div class="color-tooltip-red">Dieser Kampf ist geschlossen.</div><? }?>
								</span> 
							</a> 
						</div>
					<?
					if($n == $half){
						echo "\n\t\t\t\t".'</div>';
						echo "\n\t\t\t\t".'<div class="boss-column-portrait">';
					}
					
				} 
			}
			?>
				</div>
				<span class="clear"><!-- --></span> 
			</div> <!-- /zone-bosses -->
			<span class="clear"><!-- --></span> 
		</div>
	</div> <!-- /info -->
	<span class="clear"><!-- --></span>
	
	<div class="related">
		<div class="tabs ">
			<ul id="related-tabs">
				<li> 
					<a href="/game/zone/<?=$zone->label?>/#loot" data-key="loot" id="tab-loot" class="tab-active"> 
						<span><span> Beute </span></span> 
					</a> 
				</li>
				<!--
				<li>
					<a href="#achievements" data-key="achievements" id="tab-achievements"> 
						<span><span> Erfolge (<em>7</em>) </span></span>
					</a> 
				</li>-->
				<li> 
					<a href="/game/zone/<?=$zone->label?>/#bugs" data-key="bugs" id="tab-bugs"> 
						<span><span> Bekannte Bugs </span></span> 
					</a> 
				</li>
			</ul>
			<span class="clear"><!-- --></span> 
		</div>
		<div id="related-content" class="loading">
			
		</div>
	</div>
</div>

<script type="text/javascript">
//<![CDATA[
$(function() {
	Wiki.pageUrl = '/game/zone/<?=$zone->label?>/';
});
//]]>
</script> 
<script type="text/javascript" src="/<?=$currtmp?>/js/wiki.js?v2"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/zone.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/table.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/filter.js"></script>
<script type="text/javascript" src="/<?=$currtmp?>/js/lightbox.js"></script>
