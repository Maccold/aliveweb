
<div class="related-content" id="related-loot">
	<div class="filters inline">
		<div class="keyword"> <span class="view"></span> <span class="reset" style="display: none"></span>
			<input id="filter-name-loot" type="text" class="input filter-name" data-filter="row" maxlength="25" title="Filter..." value="Filter..." />
		</div>
		<div class="filter-tabs"> 
			<a href="javascript:;" data-filter="column" data-column="5" data-value="" data-name="type" class="tab-active"> Alle </a> 
			<a href="javascript:;" data-filter="column" data-column="5" data-name="type" data-value="0"> Normal 10 </a> 
			<a href="javascript:;" data-filter="column" data-column="5" data-name="type" data-value="2"> Heroisch 10 </a> 
			<a href="javascript:;" data-filter="column" data-column="5" data-name="type" data-value="1"> Normal 25 </a> 
			<a href="javascript:;" data-filter="column" data-column="5" data-name="type" data-value="3"> Heroisch 25 </a> 
		</div>
		<span class="clear"><!-- --></span>
		<div class="filter">
			<label for="filter-class">Zeige Gegenstände für</label>
			<select class="input select filter-class" data-filter="class" data-name="class">
				<option value="">Alle Klassen</option>
				<option value="class-11">Druide</option>
				<option value="class-9">Hexenmeister</option>
				<option value="class-3">Jäger</option>
				<option value="class-1">Krieger</option>
				<option value="class-8">Magier</option>
				<option value="class-2">Paladin</option>
				<option value="class-5">Priester</option>
				<option value="class-7">Schamane</option>
				<option value="class-4">Schurke</option>
				<option value="class-6">Todesritter</option>
			</select>
		</div>
		<div class="filter" style="padding-top: 3px;">
			<label for="filter-isEquippable-loot">
				<input id="filter-isEquippable-loot" type="checkbox" class="input checkbox filter-isEquippable" data-name="isEquippable" data-filter="class" data-value="is-equipment" />
				nur Ausrüstung </label>
		</div>
		<span class="clear"><!-- --></span> 
	</div>
	<div class="data-options-top">
		<div class="table-options data-options ">
			<div class="option">
				<ul class="ui-pagination">
				</ul>
			</div>
			Zeige <strong class="results-start">1</strong>–<strong class="results-end">50</strong> von <strong class="results-total"><?=$loot_count?></strong> Ergebnissen <span class="clear"><!-- --></span> </div>
	</div>
	<div class="table full-width">
		<table>
			<thead>
				<tr>
					<th> <a href="javascript:;" class="sort-link default"> <span class="arrow">Name</span> </a> </th>
					<th class="align-center"> <a href="javascript:;" class="sort-link numeric"> <span class="arrow">Stufe</span> </a> </th>
					<th class="align-center"> <a href="javascript:;" class="sort-link numeric"> <span class="arrow">Benötigt</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link default"> <span class="arrow">Typ</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link numeric"> <span class="arrow">Droprate</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link default"> <span class="arrow">Modus</span> </a> </th>
					<th> <a href="javascript:;" class="sort-link default"> <span class="arrow">Quelle</span> </a> </th>
				</tr>
			</thead>
			<tbody>
				<?=$loot_content?>
				<tr class="no-results">
					<td colspan="7" class="align-center"> Keine Ergebnisse gefunden. </td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="data-options-bottom">
		<div class="table-options data-options ">
			<div class="option">
				<ul class="ui-pagination">
				</ul>
			</div>
			Zeige <strong class="results-start">1</strong>–<strong class="results-end">50</strong> von <strong class="results-total"><?=$loot_count?></strong> Ergebnissen <span class="clear"><!-- --></span> </div>
	</div>
	<script type="text/javascript">
        //<![CDATA[
		Wiki.related['loot'] = new WikiRelated('loot', {
			paging: true,
			totalResults: <?=$loot_count?>,
				column: 0,
				method: 'default',
				type: 'asc'
		});
        //]]>
        </script> 
</div>
