
<div class="content-header">
	<h2 class="header ">Server Ranking</h2>
	<span class="clear"><!-- --></span>
</div>

<div class="pvp pvp-ladder">
	<div class="pvp-right">
		<div class="ladder-title">
			<h3 class="category">nach <?=$title?></h3>
		</div>

		<div id="ladders">
			<div class="table-options data-options ">
				<div class="option">
				<? if($numPages > 1) echo paginate($numPages, $page, array("server", "ranking", array("mode" => $mode))); ?>
				</div>
				Zeige <strong class="results-start"><?=$firstCharNumber?></strong>–<strong class="results-end"><?=$lastCharNumber?></strong> 
				von <strong class="results-total"><?=$sumChars?></strong> Ergebnissen
				<span class="clear"><!-- --></span>
			</div>

			<div class="table ">
		<table>
			<thead>
			<tr>
				<th><span class="sort-tab">#</span></th>
				<th><span class="sort-tab">Charakter</span></th>
				<th><span class="sort-tab">Fraktion</span></th>
				<th><span class="sort-tab">Erfolgspunkte</span></th>
				<th><span class="sort-tab">Itemlevel</span></th>
			</tr>
			</thead>
			<tbody>
			<? foreach($characterRows as $n => $team){ ?>
			<tr class="<?=$team["css"]?>" id="rank-<?=$team["rank"]?>">
				<td class="ranking">
					<?=$team["rank"]?>
				</td>
				<td>
					<a href="/character/Norgannon/<?=$team["name"]?>">
						<?=$team["name"]?>
						<div class="player-icons">
							<?=icon_race($team["race"],$team["gender"])?>
							<?=icon_class($team["class"])?>
						</div>
					</a>
				</td>
				<td class="align-center">
					<?=icon_faction($team["faction"])?>
				</td>
				<td class="align-center"><span class="achievements">
					<a href="http://arsenal.wow-alive.de/character-achievements.php?r=Norgannon&cn=<?=urlencode($team["name"])?>" target="_blank"><?=$team["achievementPoints"]?></a>
				</span></td>
				<td class="align-center">
						<span id="tt-equipped-<?=$n?>" style="display:none"><span class="color-d2"><?=$team["itemLevelEquipped"]?></span> ausgerüstet</span>
						<div id="summary-averageilvl-best" class="best" data-tooltip="#tt-equipped-<?=$n?>"><?=$team["itemLevel"]?></div>
				</td>
			</tr>
			<? } ?>
			</tbody>
		</table>
			</div>

			<div class="table-options data-options ">
				<div class="option">
					<? if($numPages > 1) echo paginate($numPages, $page, array("server", "ranking", array("mode" => $mode)) ); ?>
				</div>
				Zeige <strong class="results-start"><?=$firstCharNumber?></strong>–<strong class="results-end"><?=$lastCharNumber?></strong> 
				von <strong class="results-total"><?=$sumChars?></strong> Ergebnissen
				<span class="clear"><!-- --></span>
			</div>
		</div> <!-- /.ladders -->
	</div> 

	<div class="pvp-left">
		<ul class="dynamic-menu" id="menu-pvp">
			<li class="root-item back-to">
				<a href="<?=url_for("server")?>"><span class="arrow">Übersicht</span></a>
			</li>
			<li class="has-submenu<? if($mode == "achievement") echo " item-active";?>">
				<a href="<?=url_for("server","ranking", array("mode" => "achievement"))?>">
					<span class="arrow">Erfolgspunkte</span>
				</a>
			</li>
			<li class="has-submenu<? if($mode == "itemlevel") echo " item-active";?>">
				<a href="<?=url_for("server","ranking", array("mode" => "itemlevel"))?>">
					<span class="arrow">Itemlevel</span>
				</a>
			</li>
		</ul>
	</div>

	<span class="clear"><!-- --></span>
</div>
	