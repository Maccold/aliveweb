<?php 
builddiv_start(1, "Bugtracker");
?>
<style type="text/css">
textarea{
	width:500px;
	overflow:auto;
}
pre{
	border: 1px solid white;
	padding: 10px 20px;
	margin-left: 50px;
}
b, strong{ color:white;}
h2{ padding: 10px 0px; color:#F0E29A;}
</style>

<script type="text/javascript">
$(document).ready(function() {
	
	$("#class").change(function(){
		$(".form-details").hide();
		/*if(this.value == "[Charakter]"){
			$("#char-detail").show();
		}*/
		if(this.value == "[Quest]"){
			$("#quest-detail").show();
		}
		if(this.value == "[Instanz]"){
			$("#instance-detail").show();
		}
	});

	$("#zone-name").autocomplete({
		minLength: 2,
		source: instanceData,
		select: function(event, ui) {
			$("#zone-id").val(ui.item.value);
			$("#form-link").val("http://de.wowhead.com/zone="+ui.item.value);
			$("#link-tt").html('<a href="http://de.wowhead.com/zone='+ui.item.value+'" target="_blank">WoWhead Tooltip</a>');
		},
		change: function(event, ui) {
			$("#zone-name").val(ui.item.label);
		} 
	});
	
	$("#detail-search").autocomplete({ 
		minLength: 3,
		source: "http://portal.wow-alive.de/ajax/search-quest/",
		select: function(event, ui) {
			$("#form-link").val("http://de.wowhead.com/quest="+ui.item.value);
			$("#form-title").val(ui.item.label);
			$("#link-tt").html('<a href="http://de.wowhead.com/quest='+ui.item.value+'" target="_blank">WoWhead Tooltip</a>');
		},
		change: function(event, ui) {
			$("#detail-search").val("");
		} 
	});
	
	$("#form-submit").click(function(){
		if($("#class").val() == "-"){
			Toast.show("Bitte wähle zuerst aus welche Kategorie der Bug hat.");
			return false;
		}
		if($("#form-title").val() == ""){
			Toast.show("Bitte trage vor dem Abschicken einen Titel ein.");
			return false;
		}
		
	});
});
</script>

<form action="/server/bugtracker/bug/<?=$bugId?>/action/change" method="post">
	

<div class="table">
<table border="0" cellpadding="5" cellspacing="0" width="800">
	<thead>
		<tr>
			<th colspan="4"><span class="sort-tab">Bug #<?=$bugId?> <?=$title?></span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td width="120"><strong>Erstellt am:</strong></td>
			<td>
				<?php if(empty($createdDetail)){ ?>
					<?=$bugRow["date"]?>
				<?php } else { ?>
				 <span data-tooltip="<?=$bugRow["date"]?>"><?=$createdDetail?></span>
				<?php } ?>
			</td>
			<td width="120"><strong><? if(!empty($bugRow["date2"])){?>Letzte Bearbeitung
					am:<? }?> </strong></td>
			<td><?=$bugRow["date2"]?></td>
		</tr>
		<tr>
			<td valign="top" width="120"><strong>Status:</strong></td>
			<td>
				<?=select_field("state", array("Offen", "Bearbeitung", "Erledigt", "Abgewiesen", "Nicht umsetzbar"))?>
			</td>
			<td width="120"><strong>Prozent erledigt:</strong></td>
			<td><input type="text" name="complete" size="40" value="<?=$post_data["complete"]?>"/></td>
		</tr>
		<tr>
			<td valign="top" width="120"><strong>Kategorie:</strong></td>
			<td>
				<?=select_field("class", array("[Quest]", "[Instanz]", "[Charakter]", "[Charakter/Druide]", "[Charakter/Hexenmeister]", "[Charakter/Jäger]", "[Charakter/Krieger]", "[Charakter/Magier]", "[Charakter/Paladin]", "[Charakter/Priester]", "[Charakter/Schamane]", "[Charakter/Schurke]", "[Charakter/Todesritter]", "[Erfolg]", "[Item]", "[NPC]", "[Homepage]"))?>
				<div id="instance-detail" class="form-details" style="<? if($bugRow["class"] != "[Instanz]"){ echo "display:none";}?>">
					<br>
					<label>Um welche Instanz/welchen Raid geht es:</label><br/>
					<input type="text" id="zone-name" name="auto-instance" size="40" value=""/>
					<input type="hidden" name="zone-id" name="zone-id" value="0"/>
				</div>
				<div id="quest-detail" class="form-details" style="<? if($bugRow["class"] != "[Quest]"){ echo "display:none";}?>">
					<br>
					<label>Suche in diesem Feld nach dem Questnamen:</label><br/>
					<input type="text" id="detail-search" name="quest-detail" size="40" value=""/>
				</div>
			</td>
			<td width="120"><strong>Link:</strong></td>
			<td><input type="text" id="form-link" name="link" size="40" value="<?=$post_data["link"]?>"/></td>
		</tr>
		<tr>
			<td colspan="4"><hr /></td>
		</tr>
		<tr>
			<td><strong>Titel:</strong></td>
			<td colspan="3"><input type="text" name="title" size="80" value="<?=$post_data["title"]?>" style="width:650px"/></td>
		</tr>
		<tr>
			<td><strong>Beschreibung:</strong></td>
			<td colspan="3"><textarea rows="8" name="desc" cols="95" style="width:650px"><?=$post_data["desc"]?></textarea></td>
		</tr>
		<tr>
			<td>
				<div class="submit">
					<button class="ui-button button1 comment-submit " type="submit" id="form-submit">
						<span><span>Speichern</span></span>
					</button>
				</div>
			</td>
		</tr>
	</tbody>
</table>

</div>

<script type="text/javascript" src="/js/data.instances.js"></script>
</form>				
<? builddiv_end(); ?>