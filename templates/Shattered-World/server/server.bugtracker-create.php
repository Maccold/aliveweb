<?php 
builddiv_start(1, "Bugtracker");
?>
<style type="text/css">
textarea{
	width:500px;
	overflow:auto;
}
pre{
	border: 1px solid white;
	padding: 10px 20px;
	margin-left: 50px;
}
b, strong{ color:white;}
h2{ padding: 10px 0px; color:#F0E29A;}
</style>

<script type="text/javascript">
$(document).ready(function() {

	function getBugs(term){
		$.getJSON('/ajax/search/bugs/?term='+term, function(data) {
			var items = [];
			
			$.each(data, function(key, val) {
				items.push('<a href="/server/bugtracker/bug/' + key + '" target="_blank"> Bug #'+key+' - ' + val + '</a>');
			});
			if(items.length > 0){
				$('#other-bugs').html('<span class="color-tooltip-red">Achtung!</span> Es gibt dazu schon Bug Reports:<br/>'+items.join('<br/>'));
				$("#label-other-bugs").html("");
			}
			else{
				$('#other-bugs').html('Keine anderen Bug Reports gefunden - <span class="color-tooltip-green">Alles in Ordnung</span>');
				$("#label-other-bugs").html("");
			}
		});	
	}
	
	$("#class").change(function(){
		$(".form-details").hide();
		/*if(this.value == "[Charakter]"){
			$("#char-detail").show();
		}*/
		if(this.value == "[Quest]"){
			$("#quest-detail").show();
		}
		if(this.value == "[Instanz]"){
			$("#instance-detail").show();
			$("#npc-detail").show();
			$("#link2-wrapper").show();
			
		}
		if(this.value == "[NPC]"){
			$("#npc-detail").show();
		}
	});

	$("#zone-name").autocomplete({
		minLength: 2,
		source: instanceData,
		select: function(event, ui) {
			$("#zone-id").val(ui.item.value);
			if($("#form-title").val().length == 0)
				$("#form-title").val(ui.item.label);
			$("#form-link").val("http://de.wowhead.com/zone="+ui.item.value);
			$("#link-tt").html('<a href="http://de.wowhead.com/zone='+ui.item.value+'" target="_blank">WoWhead Tooltip</a>');
			getBugs($("#form-link").val());
		},
		change: function(event, ui) {
			$("#zone-name").val("");
		} 
	});
	$("#zone-name").blur(function(event, ui) {
		$("#zone-name").val("");
	});

	$("#auto-npc").autocomplete({
		minLength: 3,
		source: "http://portal.wow-alive.de/ajax/search/npc/",
		select: function(event, ui) {
			$("#npc-id").val(ui.item.value);
			if($("#class").val() == "[NPC]"){
				$("#form-link").val("http://de.wowhead.com/npc="+ui.item.value);
				$("#link-tt").html('<a href="http://de.wowhead.com/npc='+ui.item.value+'" target="_blank">WoWhead Tooltip</a>');
				if($("#form-title").val().length == 0)
					$("#form-title").val(ui.item.label);
			}
			else{
				$("#link2-wrapper").show();
				$("#form-link2").val("http://de.wowhead.com/npc="+ui.item.value);
				$("#link-tt2").html('<a href="http://de.wowhead.com/npc='+ui.item.value+'" target="_blank">WoWhead Tooltip</a>');
				$("#form-title").val($("#form-title").val()+": "+ui.item.label);
			}
			getBugs($("#form-link").val());
		},
		change: function(event, ui) {
			$("#auto-npc").val("");
		} 
	});
	
	$("#detail-search").autocomplete({ 
		minLength: 3,
		source: "http://portal.wow-alive.de/ajax/search-quest/",
		select: function(event, ui) {
			$("#form-link").val("http://de.wowhead.com/quest="+ui.item.value);
			$("#form-title").val(ui.item.label);
			$("#link-tt").html('<a href="http://de.wowhead.com/quest='+ui.item.value+'" target="_blank">WoWhead Tooltip</a>');
			getBugs($("#form-link").val());
		},
		change: function(event, ui) {
			$("#detail-search").val("");
		} 
	});
	
	$("#form-submit").click(function(){
		if($("#class").val() == "-"){
			Toast.show("Bitte wähle zuerst aus welche Kategorie der Bug hat.");
			return false;
		}
		if($("#form-title").val() == ""){
			Toast.show("Bitte trage vor dem Abschicken einen Titel ein.");
			return false;
		}
		
	});

	$("#form-link").change(function(){ getBugs($("#form-link").val()); });
});
</script>

<form action="/server/bugtracker/action/create/" method="post">
			

<div class="table">
<table border="0" cellpadding="5" cellspacing="0" width="800">
	<thead>
		<tr>
			<th colspan="3"><span class="sort-tab">Neuen Bug eintragen</span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td valign="top" width="120"><strong>Kategorie:</strong></td>
			<td valign="top">
				<?=select_field("class", array("-","[Quest]", "[Instanz]", "[NPC]", "[Charakter]", "[Charakter/Druide]", "[Charakter/Hexenmeister]", "[Charakter/Jäger]", "[Charakter/Krieger]", "[Charakter/Magier]", "[Charakter/Paladin]", "[Charakter/Priester]", "[Charakter/Schamane]", "[Charakter/Schurke]", "[Charakter/Todesritter]", "[Erfolg]", "[Item]", "[Homepage]"))?>
				<div id="char-detail" class="form-details" style="display:none">
					<br>
					<label>Ist ein bestimmter Charakter betroffen dann gib hier den Charakternamen ein:</label><br/>
					<input type="text" id="detail-char" name="char-detail" size="50" value=""/>
				</div>
				<div id="instance-detail" class="form-details" style="display:none">
					<br>
					<label>Um welche Instanz/welchen Raid geht es:</label><br/>
					<input type="text" id="zone-name" name="auto-instance" size="50" value=""/>
					<input type="hidden" id="zone-id" name="zone-id" value="0"/>
				</div>
				<div id="npc-detail" class="form-details" style="display:none">
					<label>Ist ein bestimmter Boss/NPC betroffen:</label><br/>
					<input type="text" id="auto-npc" name="auto-npc" size="50" value=""/>
					<input type="hidden" id="npc-id" name="npc-id" value="0"/>
				</div>
				<div id="quest-detail" class="form-details" style="display:none">
					<br>
					<label>Suche in diesem Feld nach dem Questnamen:</label><br/>
					<input type="text" id="detail-search" name="quest-detail" size="50" value=""/>
				</div>
				<br/><strong>Kategorienhilfe:</strong>
				<ul>
					<li>Charakter: Fehler bei Talenten, Fähigkeiten. Fehlendes Equip oder anderes bitte einen GM direkt anschreiben.</li>
					<li>Quest: Wenn ein Quest im Spiel ist wähle diese Kategorie.</li>
					<li>Instanz: Alles rund um Instanzen und Raids, fehlerhafte Beute oder Bosse und ähnliches.</li>
					<li>NPC: Fehler mit Kreaturen/Bossen außerhalb von Raids und ohne direkten Zusammenhang mit einer Quest</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td valign="top" width="120"><strong>Link:</strong></td>
			<td valign="top">
				<input type="text" id="form-link" name="link" size="50" value=""/> <span id="link-tt"></span><br>
				<span id="link2-wrapper" style="display:none"><input type="text" id="form-link2" name="link2" size="50" value=""/> <span id="link-tt2"></span><br></span>
				Hier den Link von <a href="http://de.wowhead.com" target="_blank">http://de.wowhead.com</a> eintragen.</td>
		</tr>
		<tr>
			<td valign="top" id="label-other-bugs">&nbsp;</td>
			<td id="other-bugs">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3"><hr /></td>
		</tr>
		<tr>
			<td><strong>Titel:</strong></td>
			<td colspan="2"><input type="text" id="form-title" name="title" size="50" value=""/></td>
		</tr>
		<tr>
			<td><strong>Beschreibung:</strong></td>
			<td colspan="2"><textarea rows="8" id="form-desc" name="desc" cols="95"></textarea></td>
		</tr>
		<tr>
			<td>
				<div class="submit">
					<button class="ui-button button1 comment-submit " type="submit" id="form-submit">
						<span><span>Eintragen</span></span>
					</button>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<hr />

</div>

</form>				
<script type="text/javascript" src="/js/data.instances.js"></script>
<? builddiv_end(); ?>