<style type="text/css">
.important{color:white;}

.services-content ul,
.services-content ol{ margin-left:20px }
.services-content ul li{ margin-left:20px; list-style-type:disc; }
.services-content ol li{ margin-left:20px; list-style:decimal; }

</style>

<div class="top-banner">
	<div class="section-title">
		<span>Transferanleitung</span>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">
					<?php echo $content; ?>
					<span class="clear"></span>
					
					<a href="<?=url_for("account","ct")?>" class="ui-button button1 button1-next float-right"><span><span>Weiter zum Formular</span></span></a>
					
				</div> <!-- /sub-services-section -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<? echo $account_sidebar; ?>
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>