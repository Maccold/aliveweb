<?php
if( INCLUDED !== true)
	exit;
?>

<?php builddiv_start(1, "WoW Alive Teammitglieder"); ?>

<div class="left-col">
	<div class="services-content">
<br> 
<br>
<!-- Serverleitung -->
<? write_subheader("Serverleitung"); ?>

<table width="600" border="0" cellspacing="0" cellpadding="0" class="postContainerPlain"> 
<tr> 
	<td width="40%"><div><img src="http://portal.wow-alive.de/templates/WotLK/images/SL_avatar.png" width="200" height="200" /></div></td> 
	<td width="60%">
		<table width="500" cellspacing="20">
		<tr>
			<td>
			<table>
			<? foreach($gm_groups[8] as $basis){ ?>
			<tr>
				<td><?=$basis?><br></td>
			</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
	</td> 
</tr>
</table>

<table width="600" cellspacing="20">
<tr>
	<td>Zust&auml;ndig f&uuml;r die Wartung und technische Lauff&auml;higkeit des Root-Servers sowie das &Uuml;berarbeiten der Scripte und das Beheben von Bugs. Diese GMs sind, wenn sie denn mal online sind, meist mit dem Testen wichtiger Funktionen besch&auml;tigt und sollten ingame nicht angesprochen werden. Diese GMs machen KEINEN Transfer oder direkten Support ingame. Diese Jungs k&ouml;nnen aber bei allen technischen Problemen betreffend Server, Homepage und &auml;hnlichem im Forum oder Teamspeak angesprochen werden.<br> </td>
</tr>
</table> 
<br>

<!-- Teamleiter -->
<? write_subheader("Game Master Teamleiter"); ?>		

<table width="600" border="0" cellspacing="0" cellpadding="0" class="postContainerPlain"> 
<tr> 
	<td width="40%"><div><img src="http://portal.wow-alive.de/templates/WotLK/images/SL_avatar.png" width="200" height="200" /></div></td> 
	<td width="60%">
		<table width="500" cellspacing="20">
		<tr>
			<td>
			<table>
			<? foreach($gm_groups[4] as $basis){ ?>
			<tr>
				<td><?=$basis?><br></td>
			</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
	</td> 
</tr>
</table>

<table width="600" cellspacing="20">
<tr>
	<td>Zust&auml;ndig f&uuml;r die Kommunikation und Planung innerhalb der GameMaster auf dem Server. Diese Position f&uuml;hrt das Team direkt. Wenn Fragen zu gr&ouml;sseren Transfer (Gildentransfer) oder auch Lob und Tadel zu der Arbeit der Gamemaster gibt dann ist sie eure Ansprechspersonen. <br> </td>
</tr>
</table>
<br>

<!-- TransferGM -->  
<? write_subheader("Transfer Game Master"); ?>
<table width="600" border="0" cellspacing="0" cellpadding="0" class="postContainerPlain"> 
<tr> 
	<td width="40%"><div><img src="http://portal.wow-alive.de/templates/WotLK/images/SL_avatar.png" width="200" height="200" /></div></td> 
	<td width="60%">
		<table width="500" cellspacing="20">
		<tr>
			<td>
			<table>
			<? foreach($gm_groups[3] as $basis){ ?>
			<tr>
				<td><?=$basis?><br></td>
			</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
	</td> 
</tr>
</table>

<table width="600" cellspacing="20">
<tr>
  <td>Zust&auml;ndig f&uuml;r das Durchf&uuml;hren der Transfers von anderen Servern zu Alive UND auch f&uuml;r den Transfer von einer Fraktion zur anderen. Diese GMs K&Ouml;NNEN auch den Support eines LvL2 und LvL1 GMs &uuml;bernehmen aber vorrangig sind sie nur f&uuml;r Transfers zust&auml;ndig, sollte kein Support GM anwesend sein so kann man sich mit seinem Anliegen auch an einen Transfer GM richten.<br> </td>
</tr>  
</table> 
<br>

<!-- Game Master --> 
<? write_subheader("Game Master"); ?>
<table width="600" border="0" cellspacing="0" cellpadding="0" class="postContainerPlain"> 
<tr> 
	<td width="40%"><div><img src="http://portal.wow-alive.de/templates/WotLK/images/SL_avatar.png" width="200" height="200" /></div></td> 
	<td width="60%">
		<table width="500" cellspacing="20">
		<tr>
			<td>
			<table>
			<? foreach($gm_groups[2] as $basis){ ?>
			<tr>
				<td><?=$basis?><br></td>
			</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
	</td> 
</tr>
</table>

<table width="600" cellspacing="20">
<tr>
	<td>Zust&auml;ndig f&uuml;r den Questsupport, Instanzsupport und Communitysupport. Beantworten Fragen aller Art und sind in der Lage Quests zu adden, zu completen und zu &uuml;berpr&uuml;fen. K&ouml;nnen auch wichtige NPCs adden oder entfernen. Diese GMs &uuml;berwachen die Einhaltung der Regeln genau und k&ouml;nnen jeden Verstoss sofort in Form von Bann, Mute oder Freeze ahnden. Wer z.b. einen Hacker findet kann dies jederzeit einem LvL2 GM melden.<br> </td>
</tr>   
</table> 
<br>

<!-- Coder -->
<? write_subheader("DB/Core Scripter"); ?>

<table width="600" border="0" cellspacing="0" cellpadding="0" class="postContainerPlain"> 
<tr> 
	<td width="40%"><div><img src="http://portal.wow-alive.de/templates/WotLK/images/SL_avatar.png" width="200" height="200" /></div></td> 
	<td width="60%">
		<table width="500" cellspacing="20">
		<tr>
			<td>
			<table>
			<? foreach($gm_groups[5] as $basis){ ?>
			<tr>
				<td><?=$basis?><br></td>
			</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
	</td> 
</tr>
</table>

<table width="600" cellspacing="20">
<tr>
	<td>Zust&auml;ndig f&uuml;r die Wartung und technische Lauff&auml;higkeit des Root-Servers sowie das &Uuml;berarbeiten der Scripte und das Beheben von Bugs. Diese GMs sind, wenn sie denn mal online sind, meist mit dem Testen wichtiger Funktionen besch&auml;tigt und sollten ingame nicht angesprochen werden. Diese GMs machen KEINEN Transfer oder direkten Support ingame. Diese Jungs k&ouml;nnen aber bei allen technischen Problemen betreffend Server, Homepage und &auml;hnlichem im Forum oder Teamspeak angesprochen werden.<br> </td>
</tr>
</table> 
<br>

<!-- Supporter --> 
<? write_subheader("Game Master auf Probe"); ?>
<table width="600" border="0" cellspacing="0" cellpadding="0" class="postContainerPlain"> 
<tr> 
	<td width="40%"><div><img src="http://portal.wow-alive.de/templates/WotLK/images/SL_avatar.png" width="200" height="200" /></div></td> 
	<td width="60%">
		<table width="500" cellspacing="20">
		<tr>
			<td>
			<table>
			<? foreach($gm_groups[1] as $basis){ ?>
			<tr>
				<td><?=$basis?><br></td>
			</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
	</td> 
</tr>
</table>

<table width="600" cellspacing="20">
<tr>
	<td>Zust&auml;ndig f&uuml;r den Questsupport, Instanzsupport und Communitysupport. Beantworten Fragen aller Art und sind in der Lage Quests zu &uuml;berpr&uuml;fen. Diese GMs &uuml;berwachen die Einhaltung der Regeln genau und k&ouml;nnen jeden Verstoss sofort in Form von Bann, Mute oder Freeze ahnden. Wer z.b. einen Hacker findet kann dies jederzeit einem LvL1 GM melden.<br> </td>
</tr>  
</table> 
<br>
	</div>
</div>

<div class="right-col">
	<? echo $server_sidebar; ?>
</div>
<span class="clear"><!-- --></span>

<?php builddiv_end(); ?>

