<br />
<?php builddiv_start(0, $lang['realm_status'], $desc) ?>
<style type = "text/css">
</style>


<div class="table">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <th align="center" nowrap="nowrap" width="53"><?php echo $lang['status'];?></th>
            <th align="center" nowrap="nowrap"><?php echo $lang['uptime'];?></th>
            <th align="center" nowrap="nowrap"><?php echo $lang['realm_name'];?></th>
            <th align="center" nowrap="nowrap" width="120"><?php echo $lang['si_type'];?></th>
            <th align="center" nowrap="nowrap" width="120"><?php echo $lang['si_pop'];?></th>
        </tr>
<?php foreach($realms as $realm_id => $realm): ?>
        <tr class="row<?php echo $realm['res_color'] ?>">
            <td align="center">
				<img src="<?php echo $realm['img']; ?>" height='18' width='18' alt=""/>
			</td>
            <td width="168" align="center" nowrap="nowrap">
				<?php if($realm['uptime'] != 0) { echo sec_to_dhms($realm['uptime'],true); } ?>
			</td>
            <td width="802">
				<?php echo $realm['name']; ?>
			</td>
            <td align="center">
				<?php echo $realm['type']; ?>
			</td>
            <td align="center">
				<?php echo $realm['pop']." (".population_view($realm['pop']).")"; ?>
			</td>
        </tr>
<?php endforeach; ?>
        </tbody>
    </table>
	
<?php builddiv_end() ?> <br>
