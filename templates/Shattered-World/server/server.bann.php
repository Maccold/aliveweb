<br>
<?php builddiv_start(0, $lang['bann']) ?>

<style type="text/css">
  a.server { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; font-weight: bold; }
  td.serverStatus1 { font-size: 0.8em; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus2 { font-size: 0.8em; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
  td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
</style>

<div class="table">
    <table cellpadding='3' cellspacing='0' width='100%'>
     <tr> 
      <th align="center" colspan='6' nowrap="nowrap">Realm: <?php echo $realm_info_new['name']; ?></th>          
    </tr>
    <tr>
      <th align="center" nowrap="nowrap"><?php echo $lang['id'];?>&nbsp;</th>
      <th align="center" nowrap="nowrap"><?php echo $lang['bannedby'];?>&nbsp;</th>
      <th align="center" nowrap="nowrap"><?php echo $lang['banreason'];?>&nbsp;</th>
      <th align="center" nowrap="nowrap"><?php echo $lang['active'];?>&nbsp;</th>
    </tr>
<?php foreach($item_res as $item): ?>
    <tr class="row<?php echo $item['res_color'] ?><? if($item['active'] == 0) echo " disabled"; ?>">
            <td align="center"><?php echo $item['id']; ?></td>
            <td align="center"><?php echo $item['bannedby']; ?></td>
            <td align="center"><?php echo $item['banreason']; ?></td>
            <td align="center"><?php echo $item['active']; ?></td>
    </tr>
<?php endforeach; unset($item_res, $item); ?>
<?php unset($realm_info_new); ?>
    </tbody>
    </table>
</div>
<?php builddiv_end() ?>