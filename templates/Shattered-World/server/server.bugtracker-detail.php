<?php 
builddiv_start(1, "Bugtracker");
?>
<style type="text/css">
textarea{
	width:500px;
	overflow:auto;
}
pre{
	border: 1px solid white;
	padding: 10px 20px;
	margin-left: 50px;
}
b, strong{ color:white;}
h2{ padding: 10px 0px; color:#F0E29A;}
</style>
<script type="text/javascript">

function showCommentEdit(id,content){
	
	var bug = $("#bug-id").val();
	
	content = '<form action="/server/bugtracker/bug/'+bug+'/action/change-comment" method="post">\
	<input type="hidden" id="comment-id" name="comment-id" value="'+id+'">\
	<textarea name="new-content" rows="4">'+content+'</textarea>';
	content += '<button type="submit" class="ui-button button2"><span><span>Speichern</span></span></button></form>';
	$("#comment-content-"+id).html(content);
	
}

function editComment(id){

	$.ajax({
		url: "/ajax/search/bug-comment/?term="+id,
		success: function(data){
			showCommentEdit(id,data);
		}
	});
}

</script>

<input type="hidden" id="bug-id" name="bug-id" value="<?=$bugId?>">
<?php if($userObject->isGM()){ ?>
	<a href="/server/bugtracker/bug/<?=$bugId?>/action/edit" class="ui-button button2"><span><span>Bug-Report bearbeiten</span></span></a>&nbsp;
	<span class="clear"></span>
	<br/>
<?php } ?>
<div class="table">
<table border="0" cellpadding="5" cellspacing="0" width="800">
	<thead>
		<tr>
			<th colspan="4"><span class="sort-tab">Bug #<?=$bugId?> <?=$class?> <?=$title?><span class="<?=$cssState?>" style="float: right;"><?=strtoupper($state)?></span></span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td width="120"><strong>Erstellt am:</strong></td>
			<td>
				<?php if(empty($createdDetail)){ ?>
					<?=$date?>
				<?php } else { ?>
				 <span data-tooltip="<?=$date?>"><?=$createdDetail?></span>
				<?php } ?>
			</td>
			<td width="160"><strong><? if(!empty($date2)){?>Letzte Bearbeitung am:<? }?> </strong></td>
			<td>
				<?php if(empty($changedDetail)){ ?>
					<?=$date2?>
				<?php } else { ?>
				 <span data-tooltip="<?=$date2?>"><?=$changedDetail?></span>
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td><strong>Status:</strong></td>
			<td><?=$state?></td>
			<td ><strong>Prozent erledigt:</strong></td>
			<td><?=$complete?></td>
		</tr>
		<tr>
			<td width="120"><strong>Kategorie:</strong></td>
			<td><?=$class?></td>
			<td><strong>Link:</strong></td>
			<td><?php foreach($links as $link){ echo $link."<br/>"; }?></td>
		</tr>
		<tr>
			<td colspan="4"><hr /></td>
		</tr>
		<tr>
			<td valign="top"><strong>Beschreibung:</strong></td>
			<td colspan="3">
			<?php if($bugPoster["details"]){ ?>
				eingereicht von 
				<strong><a href="<?=$bugPoster["url"]?>" class="wow-class-<?=$bugPoster["class"]?>" rel="np" target="_blank"> <?=$bugPoster["name"]?></a></strong>:<br/>
			<?php } ?>
				<?=nl2br($desc)?>
			</td>
		</tr>
	</tbody>
</table>
<hr />
<?php if(count($otherBugs) > 0){?>
<p>Verwandte Bug Reports:</p>
<ul>
	<?php foreach($otherBugs as $row){?>
	<li><?=$row?></li>
	<?php }?>
</ul>
<hr/>
<?php }?>

<div id="page-comments">

<?php 
foreach($bugLog as $timestamp => $comment){ 
	if(is_array($comment)){
		?>
<div class="comment">
	<div class="avatar portrait-b">
		<div class="avatar-interior">
		<?php if(isset($comment["avatar"])){ ?>
			<a href="<?=$comment["char_url"]?>">
				<img height="64" src="<?=$comment["avatar"]?>" alt=""/>
			</a>
		<?php } ?>
		</div>
	</div>
	<div class="comment-interior">
		<div class="character-info user">
			<div class="user-name">
			<?php if($comment["gm"]){ echo '<span class="employee"></span>'; }?>
			<?php if($comment["details"]){ ?>
				<a href="<?=$comment["char_url"]?>" class="wow-class-<?=$comment["char_class"]?>" rel="np" target="_blank"> <?=$comment["name"]?> </a>
			<?php } else { ?>
				<?=$comment["name"]?>
			<?php } ?>
			</div>
			<span class="time"><?=$comment["date"]?></span>
		</div>
		<div class="content">
			 <span id="comment-content-<?=$comment["id"]?>"><?php echo nl2br($comment["text"]);?></span>
			 <?php if(!empty($comment["action"])){?>
			 <br/><span class="action-log"><?=$comment["action"]?></span>
			 <?php }?>
		</div>
		<div class="comment-actions">
		<?php if(($userObject->id == $comment["posterAccountId"]) || $userObject->isBugtrackerDev()){ ?>
			<button type="button" onclick="editComment(<?=$comment["id"]?>);" class="reply-link ui-button button2"><span><span>Bearbeiten</span></span></button>
		<?php } ?>
		</div>
	</div>
</div>
<?php 
	}
	else{
		?>
<div class="comment">
		<?=$comment?>
</div>
		<?php 
	} 
} ?>

<?php if($charSelected){ ?>
<div class="new-post">
	<div class="comment">
		<div class="portrait-b">
			<div class="avatar-interior">
				<a href="<?=$char->GetUrl()?>">
					<img height="64" src="<?=$char->GetCharacterAvatar()?>" alt=""/>
				</a>
			</div>
		</div>
		<div class="comment-interior">
			<div class="character-info user">
				<div class="user-name">
					<a href="<?=$userObject->GetActiveCharUrl()?>" class="wow-class-2" rel="np" target="_blank"> <?=$userObject->GetActiveCharName()?> </a>
				</div>
			</div>
			<div class="content">
			<form action="/server/bugtracker/bug/<?=$bugId?>" method="post">
				<input type="hidden" name="action" value="new"/>
				<input type="hidden" name="bug" value="<?=$bugId?>"/>
				<?php if($userObject->isGM()){?>
					Status ändern: 
						<?=select_field("change-state",array("Offen", "Bearbeitung", "Erledigt", "Abgewiesen", "Nicht umsetzbar"), $state)?>
				<?php } ?>
				<div class="comment-ta">
					<textarea id="comment-ta" cols="78" rows="3" name="detail"></textarea>
				</div>
				<div class="action">
					<div class="submit">
						<button class="ui-button button1 comment-submit " type="submit" onclick="Cms.Comments.ajaxComment(this, Wiki.postComment);">
							<span><span>Kommentieren</span></span>
						</button>
					</div>
					<span class="clear"><!-- --></span>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
<?php } ?>


</div> <!-- /page-comments -->


</div>
<? builddiv_end(); ?>