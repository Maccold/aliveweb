<? 
//return; 
?>
<style type="text/css">
#content .content-top { background: url("http://forum.wow-alive.de/static-wow/images/character/summary/backgrounds/race/<?=$char->raw["race"]?>.jpg") left top no-repeat; }
.profile-wrapper { background-image: url("http://forum.wow-alive.de/static-wow/images/2d/profilemain/race/<?=$char->raw["race"]?>-<?=$char->raw["gender"]?>.jpg"); }
</style>

<div id="profile-wrapper" class="profile-wrapper profile-wrapper-<?=$char->GetCssFaction()?>">
	<div class="profile-sidebar-anchor">
		<div class="profile-sidebar-outer">
			<div class="profile-sidebar-inner">
				<div class="profile-sidebar-contents">
					<div class="profile-info-anchor">
						<div class="profile-info">
							<div class="name"><a href="<?=$char->GetCharacterLink()?>/" rel="np"><?=$char->GetName()?></a></div>
							<div class="title-guild">
								<div class="title"><?=$char->GetTitle()?> </div>
								<div class="guild"> <a href="<?=$char->GetGuildLink()?>"><?=$char->GetGuildName()?></a> </div>
							</div>
							<span class="clear"><!-- --></span>
							<div class="under-name <?=$char->GetCssClass()?>"> <span class="level"><strong><?=$char->GetRaw("level")?></strong></span>, <span class="race"><?=$char->GetRace()?></span>, <a id="profile-info-spec" href="<?=$char->GetCharacterLink()?>/talent/" class="spec tip">Kampf</a>, <span class="class"><?=$char->GetClass()?></span><span class="comma">,</span> <span class="realm tip" id="profile-info-realm" data-battlegroup="Norgannon"> Norgannon </span> </div>
							<div class="achievements"><a href="<?=$char->GetCharacterLink()?>/achievement"><?=$char->GetAchievementPoints()?></a></div>
						</div>
					</div>
					<ul class="profile-sidebar-menu" id="profile-sidebar-menu">
						<li class=" active"> <a href="<?=$char->GetCharacterLink()?>/" class="" rel="np"> <span class="arrow"><span class="icon"> Übersicht </span></span> </a> </li>
						<li class=" disabled"> <a href="<?=$char->GetCharacterLink()?>/talent/" class="" rel="np"> <span class="arrow"><span class="icon"> Talente &amp; Glyphen </span></span> </a> </li>
						<li class=" disabled"> <a href="javascript:;" class=" has-submenu vault" rel="np"> <span class="arrow"><span class="icon"> Auktionen </span></span> </a> </li>
						<li class=" disabled"> <a href="javascript:;" class=" vault" rel="np"> <span class="arrow"><span class="icon"> Ereignisse </span></span> </a> </li>
						<li class=" disabled"> <a href="<?=$char->GetCharacterLink()?>/achievement" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Erfolge </span></span> </a> </li>
						<li class=" disabled"> <a href="<?=$char->GetCharacterLink()?>/companion" class="" rel="np"> <span class="arrow"><span class="icon"> Haus- und Reittiere </span></span> </a> </li>
						<li class=" disabled"> <a href="<?=$char->GetCharacterLink()?>/profession/" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Berufe </span></span> </a> </li>
						<li class=" disabled"> <a href="<?=$char->GetCharacterLink()?>/reputation/" class="" rel="np"> <span class="arrow"><span class="icon"> Ruf </span></span> </a> </li>
						<li class=" disabled"> <a href="<?=$char->GetCharacterLink()?>/pvp" class="" rel="np"> <span class="arrow"><span class="icon"> PvP </span></span> </a> </li>
						<li class="disabled"> <a href="<?=$char->GetCharacterLink()?>/feed" class="" rel="np"> <span class="arrow"><span class="icon"> Aktivitäten-Feed </span></span> </a> </li>
						<li class="disabled"> <a href="<?=$char->GetGuildLink()?>" class=" has-submenu" rel="np"> <span class="arrow"><span class="icon"> Gilde </span></span> </a> </li>
					</ul>
					<div class="summary-sidebar-links"> <span class="summary-sidebar-button"> <a href="javascript:;" id="summary-link-tools" class="summary-link-tools"></a> </span> <span class="summary-sidebar-button"> <a href="javascript:;" data-fansite="character|EU|<?=$char->GetName();?>|<?=$char->realmName?>" class="fansite-link "> </a> </span> </div>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-contents">
		<div class="summary-top">
			<div class="summary-top-right">
				<ul class="profile-view-options" id="profile-view-options-summary">
					<li> <a href="<?=$char->GetCharacterLink()?>/advanced" rel="np" class="advanced"> Erweitert </a> </li>
					<li class="current"> <a href="<?=$char->GetCharacterLink()?>/simple" rel="np" class="simple"> Einfach </a> </li>
				</ul>
				<div class="summary-averageilvl">
					<div class="rest"> Durchschnittliche Gegenstandsstufe<br />
						(<span class="equipped">389</span> ausgerüstet) </div>
					<div id="summary-averageilvl-best" class="best tip" data-id="averageilvl"> 389 </div>
				</div>
			</div>
			<div class="summary-top-inventory">
				<div id="summary-inventory" class="summary-inventory summary-inventory-simple">
					<div data-id="0" data-type="1" class="slot slot-1 item-quality-4" style=" left: 0px; top: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/71047" class="item" data-item="e=4209&amp;g0=68778&amp;g1=52211&amp;re=140&amp;set=71047,71045&amp;d=87"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_helmet_leather_raidrogue_j_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="1" data-type="2" class="slot slot-2 item-quality-4" style=" left: 0px; top: 58px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77091" class="item" data-item=""><img src="http://eu.media.blizzard.com/wow/icons/56/inv_misc_necklace14.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="2" data-type="3" class="slot slot-3 item-quality-4" style=" left: 0px; top: 116px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/78833" class="item" data-item="e=4204&amp;g0=52212&amp;g1=52211&amp;re=147&amp;set=78833,77024,77026&amp;d=90"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_shoulder_leather_raidrogue_k_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="14" data-type="16" class="slot slot-16 item-quality-4" style=" left: 0px; top: 174px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/70992" class="item" data-item="e=1099&amp;g0=52212&amp;g1=52212&amp;re=139&amp;s=1219380864"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_cape_firelands_fireset_d_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="4" data-type="5" class="slot slot-5 item-quality-4" style=" left: 0px; top: 232px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/71045" class="item" data-item="e=4102&amp;g0=52212&amp;g1=52220&amp;re=147&amp;set=71047,71045&amp;d=147"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_chest_leather_raidrogue_j_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="3" data-type="4" class="slot slot-4" style=" left: 0px; top: 290px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="javascript:;" class="empty"><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="18" data-type="19" class="slot slot-19 item-quality-4" style=" left: 0px; top: 348px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/69210" class="item" data-item=""><img src="http://eu.media.blizzard.com/wow/icons/56/inv_epicguildtabard.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="8" data-type="9" class="slot slot-9 item-quality-4" style=" left: 0px; top: 406px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77322" class="item" data-item="e=4190&amp;re=145&amp;d=50"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_bracer_leather_raidrogue_k_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="9" data-type="10" class="slot slot-10 slot-align-right item-quality-4" style=" top: 0px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77024" class="item" data-item="e=4107&amp;g0=52212&amp;re=161&amp;set=78833,77024,77026&amp;d=50"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_gauntlets_leather_raidrogue_k_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="5" data-type="6" class="slot slot-6 slot-align-right item-quality-4" style=" top: 58px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/71131" class="item" data-item="es=3729&amp;g0=52212&amp;g1=52258&amp;d=50"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_belt_leather_raidrogue_j_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="6" data-type="7" class="slot slot-7 slot-align-right item-quality-4" style=" top: 116px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77026" class="item" data-item="e=4440&amp;g0=52258&amp;g1=52258&amp;g2=52211&amp;re=146&amp;set=78833,77024,77026&amp;d=114"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_pants_leather_raidrogue_k_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="7" data-type="8" class="slot slot-8 slot-align-right item-quality-4" style=" top: 174px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77254" class="item" data-item="g0=52212&amp;g1=52212&amp;re=140&amp;s=792768544&amp;d=79"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_boots_leather_raidrogue_k_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="10" data-type="11" class="slot slot-11 slot-align-right item-quality-4" style=" top: 232px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77111" class="item" data-item="g0=52212"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_jewelry_ring_89.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="11" data-type="11" class="slot slot-11 slot-align-right item-quality-4" style=" top: 290px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/71209" class="item" data-item="re=168"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_jewelry_ring_firelandsraid_03b.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="12" data-type="12" class="slot slot-12 slot-align-right item-quality-4" style=" top: 348px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77973" class="item" data-item=""><img src="http://eu.media.blizzard.com/wow/icons/56/inv_relics_sundial.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="13" data-type="12" class="slot slot-12 slot-align-right item-quality-4" style=" top: 406px; right: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/69112" class="item" data-item="s=1191321536"><img src="http://eu.media.blizzard.com/wow/icons/56/spell_deathknight_gnaw_ghoul.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="15" data-type="21" class="slot slot-21 slot-align-right item-quality-4" style=" left: 241px; bottom: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77945" class="item" data-item="e=4099&amp;g0=52212&amp;re=147&amp;s=318230304&amp;set=77945,77946&amp;d=71"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_knife_1h_deathwingraid_e_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="16" data-type="22" class="slot slot-22 item-quality-4" style=" left: 306px; bottom: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77946" class="item" data-item="e=4099&amp;g0=52212&amp;s=681327680&amp;set=77945,77946&amp;d=66"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_knife_1h_deathwingraid_e_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
					<div data-id="17" data-type="15" class="slot slot-15 item-quality-4" style=" left: 371px; bottom: 0px;">
						<div class="slot-inner">
							<div class="slot-contents"> <a href="/wow/de/item/77086" class="item" data-item="re=168"><img src="http://eu.media.blizzard.com/wow/icons/56/inv_thrown_1h_deathwingraid_d_01.jpg" alt="" /><span class="frame"></span></a> </div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
        //<![CDATA[

		$(document).ready(function() {

			var summaryInventory = new Summary.Inventory({ view: "simple" }, {

			

			0: {

				name: "Helm des dunklen Phönix",

				quality: 4,

				icon: "inv_helmet_leather_raidrogue_j_01"

			}

			,

			1: {

				name: "Gemme der schrecklichen Erinnerungen",

				quality: 4,

				icon: "inv_misc_necklace14"

			}

			,

			2: {

				name: "Netzrüstungschiftung des Schwarzfangs",

				quality: 4,

				icon: "inv_shoulder_leather_raidrogue_k_01"

			}

			,

			14: {

				name: "Schreckensfeuertuch",

				quality: 4,

				icon: "inv_cape_firelands_fireset_d_01"

			}

			,

			4: {

				name: "Tunika des dunklen Phönix",

				quality: 4,

				icon: "inv_chest_leather_raidrogue_j_01"

			}

			,

			18: {

				name: "Wohlbekannter Gildenwappenrock",

				quality: 4,

				icon: "inv_epicguildtabard"

			}

			,

			8: {

				name: "Armschienen der mannigfaltigen Taschen",

				quality: 4,

				icon: "inv_bracer_leather_raidrogue_k_01"

			}

			,

			9: {

				name: "Netzrüstunghandschuhe des Schwarzfangs",

				quality: 4,

				icon: "inv_gauntlets_leather_raidrogue_k_01"

			}

			,

			5: {

				name: "Flammenbindender Gurt",

				quality: 4,

				icon: "inv_belt_leather_raidrogue_j_01"

			}

			,

			6: {

				name: "Netzrüstungbeinschützer des Schwarzfangs",

				quality: 4,

				icon: "inv_pants_leather_raidrogue_k_01"

			}

			,

			7: {

				name: "Blutige Fußpolster des Vernehmers",

				quality: 4,

				icon: "inv_boots_leather_raidrogue_k_01"

			}

			,

			10: {

				name: "Notlandungsschleife",

				quality: 4,

				icon: "inv_jewelry_ring_89"

			}

			,

			11: {

				name: "Gesplittertes Schwefelsiegel",

				quality: 4,

				icon: "inv_jewelry_ring_firelandsraid_03b"

			}

			,

			12: {

				name: "Kompass des Sternenfängers",

				quality: 4,

				icon: "inv_relics_sundial"

			}

			,

			13: {

				name: "Der Hungerleider",

				quality: 4,

				icon: "spell_deathknight_gnaw_ghoul"

			}

			,

			15: {

				name: "Furcht",

				quality: 4,

				icon: "inv_knife_1h_deathwingraid_e_01"

			}

			,

			16: {

				name: "Rache",

				quality: 4,

				icon: "inv_knife_1h_deathwingraid_e_01"

			}

			,

			17: {

				name: "Windschneiderbumerang",

				quality: 4,

				icon: "inv_thrown_1h_deathwingraid_d_01"

			}

			});

		});

        //]]>
        </script> 
			</div>
		</div>
		<div class="summary-bottom">
			<div class="profile-recentactivity">
				<h3 class="category "> Letzte Aktivitäten </h3>
				<div class="profile-box-simple">
					<ul class="activity-feed">
						<li class="bosskill ">
							<dl>
								<dd> <span class="icon"></span> 3x <a href="/wow/de/zone/the-slave-pens/quagmirran" data-npc="17942">Quagmirran</a> bezwungen (<a href="/wow/de/zone/the-slave-pens/" data-zone="3717">Die Sklavenunterkünfte</a>) </dd>
								<dt>vor 7 Stunden</dt>
							</dl>
						</li>
						<li class="bosskill ">
							<dl>
								<dd> <span class="icon"></span> 3x <a href="/wow/de/zone/the-underbog/the-black-stalker" data-npc="17882">Die Schattenmutter</a> bezwungen (<a href="/wow/de/zone/the-underbog/" data-zone="3716">Der Tiefensumpf</a>) </dd>
								<dt>vor 7 Stunden</dt>
							</dl>
						</li>
						<li>
							<dl>
								<dd> <a href="/wow/de/item/77026" class="color-q4" data-item="e=4440&amp;g0=52258&amp;g1=52258&amp;g2=52211&amp;re=146&amp;set=78833,77024,77026&amp;d=114"> <span class="icon-frame frame-18 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/18/inv_pants_leather_raidrogue_k_01.jpg&quot;);"> </span> </a> Erhalten <a href="/wow/de/item/77026" class="color-q4" data-item="e=4440&amp;g0=52258&amp;g1=52258&amp;g2=52211&amp;re=146&amp;set=78833,77024,77026&amp;d=114">Netzrüstungbeinschützer des Schwarzfangs</a>. </dd>
								<dt>vor 1 Tag</dt>
							</dl>
						</li>
						<li>
							<dl>
								<dd> <a href="/wow/de/item/77254" class="color-q4" data-item="g0=52212&amp;g1=52212&amp;re=140&amp;s=792768544&amp;d=79"> <span class="icon-frame frame-18 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/18/inv_boots_leather_raidrogue_k_01.jpg&quot;);"> </span> </a> Erhalten <a href="/wow/de/item/77254" class="color-q4" data-item="g0=52212&amp;g1=52212&amp;re=140&amp;s=792768544&amp;d=79">Blutige Fußpolster des Vernehmers</a>. </dd>
								<dt>vor 1 Tag</dt>
							</dl>
						</li>
						<li>
							<dl>
								<dd> <a href="/wow/de/item/77091" class="color-q4" data-item=""> <span class="icon-frame frame-18 " style="background-image: url(&quot;http://eu.media.blizzard.com/wow/icons/18/inv_misc_necklace14.jpg&quot;);"> </span> </a> Erhalten <a href="/wow/de/item/77091" class="color-q4" data-item="">Gemme der schrecklichen Erinnerungen</a>. </dd>
								<dt>vor 3 Tagen</dt>
							</dl>
						</li>
					</ul>
					<div class="profile-linktomore"> <a href="<?=$char->GetCharacterLink()?>/feed" rel="np">Frühere Aktivitäten anzeigen</a> </div>
					<span class="clear"><!-- --></span> </div>
			</div>
			<div class="summary-bottom-left">
				<div class="summary-talents" id="summary-talents">
					<ul>
						<li class="summary-talents-1"> <a href="<?=$char->GetCharacterLink()?>/talent/secondary"><span class="inner"> <span class="icon"><img src="http://eu.media.blizzard.com/wow/icons/36/ability_rogue_eviscerate.jpg" alt="" /><span class="frame"></span></span> <span class="roles"> <span class="icon-dps"></span> </span> <span class="name-build"> <span class="name">Meucheln</span> <span class="build">31<ins>/</ins>2<ins>/</ins>8</span> </span> </span></a> </li>
						<li class="summary-talents-1"> <a href="<?=$char->GetCharacterLink()?>/talent/primary" class="active"><span class="inner"> <span class="checkmark"></span> <span class="icon"><img src="http://eu.media.blizzard.com/wow/icons/36/ability_backstab.jpg" alt="" /><span class="frame"></span></span> <span class="roles"> <span class="icon-dps"></span> </span> <span class="name-build"> <span class="name">Kampf</span> <span class="build">7<ins>/</ins>31<ins>/</ins>3</span> </span> </span></a> </li>
					</ul>
				</div>
				<div class="summary-health-resource">
					<ul>
						<li class="health" id="summary-health" data-id="health"><span class="name">Gesundheit</span><span class="value">136281</span></li>
						<li class="resource-3" id="summary-power" data-id="power-3"><span class="name">Energie</span><span class="value">100</span></li>
					</ul>
				</div>
				<div class="summary-stats-profs-bgs">
					<div class="summary-stats" id="summary-stats">
						<div id="summary-stats-advanced" class="summary-stats-advanced">
							<div class="summary-stats-advanced-base">
								<div class="summary-stats-column">
									<h4>Basis</h4>
									<ul>
										<li data-id="agility" class=""> <span class="name">Beweglichkeit</span> <span class="value color-q2">6720</span> <span class="clear"><!-- --></span> </li>
										<li data-id="stamina" class=""> <span class="name">Ausdauer</span> <span class="value color-q2">6858</span> <span class="clear"><!-- --></span> </li>
										<li data-id="mastery" class=""> <span class="name">Meisterschaft</span> <span class="value">14,93</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
							</div>
							<div class="summary-stats-advanced-role">
								<div class="summary-stats-column">
									<h4>Andere</h4>
									<ul>
										<li data-id="meleeattackpower" class=""> <span class="name">Angriffskraft</span> <span class="value color-q2">19175</span> <span class="clear"><!-- --></span> </li>
										<li data-id="expertise" class=""> <span class="name">Waffenkunde</span> <span class="value">26/26</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleehaste" class=""> <span class="name">Tempowertung</span> <span class="value">13,63%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleehit" class=""> <span class="name">Trefferwertung</span> <span class="value">+16,86%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleecrit" class=""> <span class="name">Kritische Trefferwertung</span> <span class="value">25,86%</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
							</div>
							<div class="summary-stats-end"></div>
						</div>
						<div id="summary-stats-simple" class="summary-stats-simple" style=" display: none">
							<div class="summary-stats-simple-base">
								<div class="summary-stats-column">
									<h4>Basis</h4>
									<ul>
										<li data-id="strength" class=""> <span class="name">Stärke</span> <span class="value color-q2">145</span> <span class="clear"><!-- --></span> </li>
										<li data-id="agility" class=""> <span class="name">Beweglichkeit</span> <span class="value color-q2">6720</span> <span class="clear"><!-- --></span> </li>
										<li data-id="stamina" class=""> <span class="name">Ausdauer</span> <span class="value color-q2">6858</span> <span class="clear"><!-- --></span> </li>
										<li data-id="intellect" class=""> <span class="name">Intelligenz</span> <span class="value color-q2">62</span> <span class="clear"><!-- --></span> </li>
										<li data-id="spirit" class=""> <span class="name">Willenskraft</span> <span class="value color-q2">90</span> <span class="clear"><!-- --></span> </li>
										<li data-id="mastery" class=""> <span class="name">Meisterschaft</span> <span class="value">14,93</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
							</div>
							<div class="summary-stats-simple-other"> <a id="summary-stats-simple-arrow" class="summary-stats-simple-arrow" href="javascript:;"></a>
								<div class="summary-stats-column">
									<h4>Nahkampf</h4>
									<ul>
										<li data-id="meleedamage" class=""> <span class="name">Schaden</span> <span class="value">3295–4007</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleedps" class=""> <span class="name">DPS</span> <span class="value">2443,7/2138,3</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleeattackpower" class=""> <span class="name">Angriffskraft</span> <span class="value color-q2">19175</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleespeed" class=""> <span class="name">Geschwindigkeit</span> <span class="value">1,49/1,16</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleehaste" class=""> <span class="name">Tempowertung</span> <span class="value">13,63%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleehit" class=""> <span class="name">Trefferwertung</span> <span class="value">+16,86%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="meleecrit" class=""> <span class="name">Kritische Trefferwertung</span> <span class="value">25,86%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="expertise" class=""> <span class="name">Waffenkunde</span> <span class="value">26/26</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
								<div class="summary-stats-column" style="display: none">
									<h4>Fernkampf</h4>
									<ul>
										<li data-id="rangeddamage" class=""> <span class="name">Schaden</span> <span class="value">2418–3130</span> <span class="clear"><!-- --></span> </li>
										<li data-id="rangeddps" class=""> <span class="name">DPS</span> <span class="value">1576,4</span> <span class="clear"><!-- --></span> </li>
										<li data-id="rangedattackpower" class=""> <span class="name">Angriffskraft</span> <span class="value color-q2">6985</span> <span class="clear"><!-- --></span> </li>
										<li data-id="rangedspeed" class=""> <span class="name">Geschwindigkeit</span> <span class="value">1,76</span> <span class="clear"><!-- --></span> </li>
										<li data-id="rangedhaste" class=""> <span class="name">Tempo</span> <span class="value">13,63%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="rangedhit" class=""> <span class="name">Trefferchance</span> <span class="value">+16,86%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="rangedcrit" class=""> <span class="name">Kritische Fernkampfftrefferchance</span> <span class="value">25,86%</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
								<div class="summary-stats-column" style="display: none">
									<h4>Zauber</h4>
									<ul>
										<li data-id="spellpower" class=""> <span class="name">Zaubermacht</span> <span class="value">52</span> <span class="clear"><!-- --></span> </li>
										<li data-id="spellhaste" class=""> <span class="name">Tempo</span> <span class="value">13,63%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="spellhit" class=""> <span class="name">Trefferchance</span> <span class="value">+18,73%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="spellcrit" class=""> <span class="name">Kritische Trefferchance</span> <span class="value">5,46%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="spellpenetration" class=""> <span class="name">Zauberdurchschlagskraft</span> <span class="value">0</span> <span class="clear"><!-- --></span> </li>
										<li data-id="manaregen" class=" no-tooltip"> <span class="name">Manaregeneration</span> <span class="value color-q0">--</span> <span class="clear"><!-- --></span> </li>
										<li data-id="combatregen" class=" no-tooltip"> <span class="name">Kampfregeneration</span> <span class="value color-q0">--</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
								<div class="summary-stats-column" style="display: none">
									<h4>Verteidigung</h4>
									<ul>
										<li data-id="armor" class=""> <span class="name">Rüstung</span> <span class="value">17420</span> <span class="clear"><!-- --></span> </li>
										<li data-id="dodge" class=""> <span class="name">Ausweichen</span> <span class="value">32,68%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="parry" class=""> <span class="name">Parieren</span> <span class="value">5,00%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="block" class=""> <span class="name">Blocken</span> <span class="value">0,00%</span> <span class="clear"><!-- --></span> </li>
										<li data-id="resilience" class=""> <span class="name">Abhärtung</span> <span class="value">0</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
								<div class="summary-stats-column" style="display: none">
									<h4>Widerstand</h4>
									<ul>
										<li data-id="arcaneres" class=" has-icon"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/resist_arcane.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Arkan</span> <span class="value">0</span> <span class="clear"><!-- --></span> </li>
										<li data-id="fireres" class=" has-icon"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/resist_fire.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Feuer</span> <span class="value">0</span> <span class="clear"><!-- --></span> </li>
										<li data-id="frostres" class=" has-icon"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/resist_frost.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Frost</span> <span class="value">0</span> <span class="clear"><!-- --></span> </li>
										<li data-id="natureres" class=" has-icon"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/resist_nature.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Natur</span> <span class="value">64</span> <span class="clear"><!-- --></span> </li>
										<li data-id="shadowres" class=" has-icon"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/resist_shadow.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Schatten</span> <span class="value">64</span> <span class="clear"><!-- --></span> </li>
									</ul>
								</div>
							</div>
							<div class="summary-stats-end"></div>
						</div>
						<a href="javascript:;" id="summary-stats-toggler" class="summary-stats-toggler"><span class="inner"><span class="arrow">Alle Statistiken anzeigen</span></span></a> </div>
					<script type="text/javascript">
        //<![CDATA[
$(document).ready(function() {
	new Summary.Stats( {
		"health": 136281,
					"power": 100,
					"powerTypeId": 3,
					"hasOffhandWeapon": true,
					"masteryName": "Parierdolch",
					"masteryDescription": "Eure Angriffe mit der Waffenhand gewähren Euch eine Chance von 16%, einen zusätzlichen Angriff auszulösen, der Schaden in Höhe von 100% eines Angriffs mit der Waffenhand verursacht. Jeder Punkt Meisterschaft erhöht diese Chance um zusätzlich 2.00%.",
					"averageItemLevelEquipped": 389,
					"averageItemLevelBest": 389,
					"spellHitRating": 1304,
					"agiBase": 218,
					"energy": 100,
					"expertiseOffPercent": 6.5,
					"critPercent": 25.856426239013672,
					"rangeCritPercent": 25.856426239013672,
					"dodgeRatingPercent": 0,
					"parry": 5,
					"parryRating": 0,
					"rangeBonusWeaponRating": 0,
					"atkPowerBase": 18913,
					"runicPower": 0,
					"rangeHitPercent": 16.856822967529297,
					"bonusOffWeaponRating": 0,
					"resilience_damage": 0,
					"mana": 0,
					"masteryRatingBonus": 6.933287143707275,
					"dmgMainSpeed": 1.49399995803833,
					"rangeAtkPowerBonus": 190,
					"expertiseMain": 26,
					"shadowDamage": 52,
					"rangeHitRating": 1304,
					"spellDmg_petSpellDmg": -1,
					"shadowResist": 64,
					"resistNature_pet": -1,
					"armorPercent": 40.0551872253418,
					"spellHitRatingPercent": 12.728689193725586,
					"manaRegenPerFive": 0,
					"dmgRangeDps": 1576.3564453125,
					"frostCrit": 5.456714153289795,
					"armorPenetrationPercent": 0,
					"resistShadow_pet": -1,
					"focus": 0,
					"rangeHitRatingPercent": 10.856822967529297,
					"natureResist": 64,
					"intTotal": 62,
					"expertiseRating": 788,
					"bonusOffMainWeaponSkill": 0,
					"frostResist": 0,
					"int_mp": -1,
					"arcaneCrit": 5.456714153289795,
					"holyCrit": 5.456714153289795,
					"bonusMainWeaponSkill": 0,
					"natureCrit": 5.456714153289795,
					"sprBase": 70,
					"agi_ap": 13420,
					"dodge": 32.67673110961914,
					"atkPowerBonus": 262,
					"spr_regen": -1,
					"expertiseRatingPercent": 26.242876052856445,
					"mastery": 14.933286666870117,
					"health": 136281,
					"manaRegenCombat": 0,
					"sprTotal": 90,
					"rangeCritRatingPercent": 4.456715106964111,
					"dodgeRating": 0,
					"bonusMainWeaponRating": 0,
					"intBase": 42,
					"strBase": 125,
					"critRatingPercent": 4.456715106964111,
					"rangeHasteRatingPercent": 13.634536743164062,
					"rangeBonusWeaponSkill": 0,
					"dmgRangeMin": 2418,
					"rangeHasteRating": 1746,
					"dmgOffSpeed": 1.1619999408721924,
					"resistFire_pet": -1,
					"defense": 0,
					"strTotal": 145,
					"fireCrit": 5.456714153289795,
					"natureDamage": 52,
					"dmgMainMax": 4007,
					"dmgMainMin": 3295,
					"resilience_crit": 0,
					"holyResist": 0,
					"rangeAtkPowerBase": 6795,
					"dmgOffMin": 2242,
					"spellHitPercent": 18.728689193725586,
					"spellDmg_petAp": -1,
					"agi_armor": 13440,
					"resistHoly_pet": -1,
					"hitRating": 1304,
					"str_ap": 135,
					"block_damage": 30,
					"dmgOffMax": 2727,
					"masteryRating": 1243,
					"spellCritRating": 799,
					"resilience": 0,
					"expertiseOff": 26,
					"defensePercent": 0,
					"blockRating": 0,
					"armor_petArmor": -1,
					"block": 0,
					"dmgOffDps": 2138.306640625,
					"dmgRangeMax": 3130,
					"power": 100,
					"resistArcane_pet": -1,
					"dmgMainDps": 2443.674072265625,
					"healing": 52,
					"str_block": -1,
					"rangeAtkPowerLoss": 0,
					"rangeCritRating": 799,
					"fireDamage": 52,
					"shadowCrit": 5.456714153289795,
					"hasteRating": 1746,
					"arcaneDamage": 52,
					"rangeAtkPowerTotal": 6985,
					"expertiseMainPercent": 6.5,
					"atkPowerTotal": 19175,
					"agiTotal": 6720,
					"ap_dps": 1369.642822265625,
					"atkPowerLoss": 0,
					"staBase": 114,
					"fireResist": 0,
					"blockRatingPercent": 0,
					"hitRatingPercent": 10.856822967529297,
					"hitPercent": 16.856822967529297,
					"int_crit": -1,
					"rap_petSpellDmg": -1,
					"arcaneResist": 0,
					"resistFrost_pet": -1,
					"dmgRangeSpeed": 1.7599999904632568,
					"hasteRatingPercent": 13.634536743164062,
					"frostDamage": 52,
					"sta_hp": 68400,
					"agi_crit": 20.39971160888672,
					"rage": 0,
					"armorTotal": 17420,
					"sta_petSta": -1,
					"spellCritRatingPercent": 4.456715106964111,
					"armorBase": 17420,
					"spellCritPercent": 5.456714153289795,
					"critRating": 799,
					"armorPenetration": 0,
					"spellPenetration": 0,
					"parryRatingPercent": 0,
					"spellDamage": 52,
					"staTotal": 6858,
					"rap_petAp": -1,
					"holyDamage": 52,
			"foo": true
	}
	);
});
        //]]>
        </script>
					<div class="summary-stats-bottom">
						<div class="summary-battlegrounds">
							<ul>
								<li class="rating"><span class="name">Schlachtfeldwertung</span><span class="value">0</span> <span class="clear"><!-- --></span> </li>
								<li class="kills"><span class="name">Ehrenhafte Siege</span><span class="value">5270</span> <span class="clear"><!-- --></span> </li>
							</ul>
						</div>
						<div class="summary-professions">
							<ul>
								<li>
									<div class="profile-progress border-3 completed">
										<div class="bar border-3 hover" style="width: 100%"></div>
										<div class="bar-contents"> <a class="profession-details" href="<?=$char->GetCharacterLink()?>/profession/jewelcrafting"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/inv_misc_gem_01.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Juwelenschleifen</span> <span class="value">525</span> </a> </div>
									</div>
								</li>
								<li>
									<div class="profile-progress border-3 completed">
										<div class="bar border-3 hover" style="width: 100%"></div>
										<div class="bar-contents"> <a class="profession-details" href="<?=$char->GetCharacterLink()?>/profession/leatherworking"> <span class="icon"> <span class="icon-frame frame-12 "> <img src="http://eu.media.blizzard.com/wow/icons/18/trade_leatherworking.jpg" alt="" width="12" height="12" /> </span> </span> <span class="name">Lederverarbeitung</span> <span class="value">525</span> </a> </div>
									</div>
								</li>
							</ul>
						</div>
						<span class="clear"><!-- --></span> </div>
				</div>
			</div>
			<span class="clear"><!-- --></span>
			<div id="summary-raid" class="summary-raid">
				<h3 class="category">Schlachtszugsfortschritt</h3>
				<div class="profile-box-full">
					<div class="prestige">
						<div>Höchster Schlachtzugstitel: <strong> <a href="<?=$char->GetCharacterLink()?>/achievement#168:15068:a6177" data-achievement="6177"> der Tod des Zerstörers</a> </strong> </div>
					</div>
					<div class="summary-raid-wrapper">
						<div class="summary-raid-wrapper-left"> <a id="summary-raid-arrow-left" class="arrow-left" href="javascript:;"></a> <a id="summary-raid-arrow-right" class="arrow-right" href="javascript:;"></a>
							<div class="lfr"><span>LFR</span></div>
							<div class="normal"><span>Normal</span></div>
							<div class="heroic"><span>Heroisch</span></div>
						</div>
						<div id="summary-raid-wrapper-table" class="summary-raid-wrapper-table">
							<table cellspacing="0">
								<thead>
									<tr>
										<th class="spacer-left"><div></div></th>
										<th class="trivial" colspan="43"> <div class="name-anchor">
												<div class="name" id="summary-raid-head-trivial" style="left: 1567px; ">Gewöhnlich</div>
											</div>
											<div class="marker"></div>
										</th>
										<th></th>
										<th class="optimal" colspan="9"> <div class="name-anchor">
												<div class="name">Optimal</div>
											</div>
											<div class="marker"></div>
										</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr class="icons">
										<td></td>
										<td class="mc expansion-0" data-raid="mc"><div class="icon"> <a href="/wow/de/zone/molten-core/" data-zone="2717">GK</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="bwl expansion-0" data-raid="bwl"><div class="icon"> <a href="/wow/de/zone/blackwing-lair/" data-zone="2677">PSH</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="aq10 expansion-0" data-raid="aq10"><div class="icon"> <a href="/wow/de/zone/ruins-of-ahnqiraj/" data-zone="3429">AQ10</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="aq40 expansion-0" data-raid="aq40"><div class="icon"> <a href="/wow/de/zone/ahnqiraj-temple/" data-zone="3428">AQ40</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="kar expansion-1" data-raid="kar"><div class="icon"> <a href="/wow/de/zone/karazhan/" data-zone="3457">Kar</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="mag expansion-1" data-raid="mag"><div class="icon"> <a href="/wow/de/zone/magtheridons-lair/" data-zone="3836">Mag</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="gru expansion-1" data-raid="gru"><div class="icon"> <a href="/wow/de/zone/gruuls-lair/" data-zone="3923">Gru</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="ssc expansion-1" data-raid="ssc"><div class="icon"> <a href="/wow/de/zone/serpentshrine-cavern/" data-zone="3607">SSC</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="tk expansion-1" data-raid="tk"><div class="icon"> <a href="/wow/de/zone/tempest-keep/" data-zone="3845">FdS</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="mh expansion-1" data-raid="mh"><div class="icon"> <a href="/wow/de/zone/the-battle-for-mount-hyjal/" data-zone="3606">MH</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="bt expansion-1" data-raid="bt"><div class="icon"> <a href="/wow/de/zone/black-temple/" data-zone="3959">ST</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="sp expansion-1" data-raid="sp"><div class="icon"> <a href="/wow/de/zone/the-sunwell/" data-zone="4075">SP</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="voa expansion-2" data-raid="voa"><div class="icon"> <a href="/wow/de/zone/vault-of-archavon/" data-zone="4603">VoA</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="nax expansion-2" data-raid="nax"><div class="icon"> <a href="/wow/de/zone/naxxramas/" data-zone="3456">Nax</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="os expansion-2" data-raid="os"><div class="icon"> <a href="/wow/de/zone/the-obsidian-sanctum/" data-zone="4493">OS</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="eoe expansion-2" data-raid="eoe"><div class="icon"> <a href="/wow/de/zone/the-eye-of-eternity/" data-zone="4500">EoE</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="uld expansion-2" data-raid="uld"><div class="icon"> <a href="/wow/de/zone/ulduar/" data-zone="4273">Uld</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="ony expansion-2" data-raid="ony"><div class="icon"> <a href="/wow/de/zone/onyxias-lair/" data-zone="2159">Ony</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="toc expansion-2" data-raid="toc"><div class="icon"> <a href="/wow/de/zone/trial-of-the-crusader/" data-zone="4722">ToC</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="icc expansion-2" data-raid="icc"><div class="icon"> <a href="/wow/de/zone/icecrown-citadel/" data-zone="4812">ICC</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="rs expansion-2" data-raid="rs"><div class="icon"> <a href="/wow/de/zone/the-ruby-sanctum/" data-zone="4987">RS</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="bh expansion-3" data-raid="bh"><div class="icon"> <a href="/wow/de/zone/baradin-hold/" data-zone="5600">BH</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="bd expansion-3" data-raid="bd"><div class="icon"> <a href="/wow/de/zone/blackwing-descent/" data-zone="5094">BD</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="bot expansion-3" data-raid="bot"><div class="icon"> <a href="/wow/de/zone/the-bastion-of-twilight/" data-zone="5334">BoT</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="tfw expansion-3" data-raid="tfw"><div class="icon"> <a href="/wow/de/zone/throne-of-the-four-winds/" data-zone="5638">TFW</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="fl expansion-3" data-raid="fl"><div class="icon"> <a href="/wow/de/zone/firelands/" data-zone="5723">FL</a> </div></td>
										<td class="spacer"><div></div></td>
										<td class="ds expansion-3" data-raid="ds"><div class="icon"> <a href="/wow/de/zone/dragon-soul/" data-zone="5892">DS</a> </div></td>
										<td class="spacer-edge"><div></div></td>
									</tr>
									<tr class="lfr">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td data-raid="ds" class="status status-completed"><div></div></td>
									</tr>
									<tr class="normal">
										<td></td>
										<td data-raid="mc" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="bwl" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="aq10" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="aq40" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="kar" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="mag" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="gru" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="ssc" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="tk" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="mh" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="bt" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="sp" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="voa" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="nax" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="os" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="eoe" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="uld" class="status status-in-progress"><div></div></td>
										<td></td>
										<td data-raid="ony" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="toc" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="icc" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="rs" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="bh" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="bd" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="bot" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="tfw" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="fl" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="ds" class="status status-completed"><div></div></td>
									</tr>
									<tr class="heroic">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td data-raid="toc" class="status status-completed"><div></div></td>
										<td></td>
										<td data-raid="icc" class="status status-in-progress"><div></div></td>
										<td></td>
										<td data-raid="rs" class="status status-incomplete"><div></div></td>
										<td></td>
										<td></td>
										<td></td>
										<td data-raid="bd" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="bot" class="status status-in-progress"><div></div></td>
										<td></td>
										<td data-raid="tfw" class="status status-incomplete"><div></div></td>
										<td></td>
										<td data-raid="fl" class="status status-in-progress"><div></div></td>
										<td></td>
										<td data-raid="ds" class="status status-incomplete"><div></div></td>
									</tr>
								</tbody>
							</table>
						</div>
						<span class="clear"><!-- --></span> </div>
					<div class="summary-raid-legend"> <span class="completed">Abgeschlossen</span> <span class="in-progress">Im Gange</span> </div>
					<script type="text/javascript">
        //<![CDATA[
$(document).ready(function() {
	new Summary.RaidProgression( {
		nTrivialRaids: 22, nOptimalRaids: 5, nChallengingRaids: 0
	}
	, {
		mc: {
			name: "Geschmolzener Kern",
						playerLevel: 60,
						nPlayers: 40,
						location: "Der Schwarzfels",
						expansion: 0,
						heroicMode: false,
						bosses: 	[ {
				name: "Ragnaros", nKills: 0
			}
			]
		}
		,
				bwl: {
			name: "Pechschwingenhort",
						playerLevel: 60,
						nPlayers: 40,
						location: "Der Schwarzfels",
						expansion: 0,
						heroicMode: false,
						bosses: 	[ {
				name: "Nefarian", nKills: 1
			}
			]
		}
		,
				aq10: {
			name: "Ruinen von Ahn\'Qiraj",
						playerLevel: 60,
						nPlayers: 10,
						location: "Silithus",
						expansion: 0,
						heroicMode: false,
						bosses: 	[ {
				name: "Ossirian der Narbenlose", nKills: 0
			}
			]
		}
		,
				aq40: {
			name: "Tempel von Ahn\'Qiraj",
						playerLevel: 60,
						nPlayers: 40,
						location: "Silithus",
						expansion: 0,
						heroicMode: false,
						bosses: 	[ {
				name: "C\'Thun", nKills: 0
			}
			]
		}
		,
				kar: {
			name: "Karazhan",
						playerLevel: 70,
						nPlayers: 10,
						location: "Gebirgspass der Totenwinde",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Prinz Malchezaar", nKills: 4
			}
			]
		}
		,
				mag: {
			name: "Magtheridons Kammer",
						playerLevel: 70,
						nPlayers: 25,
						location: "Höllenfeuerhalbinsel",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Magtheridon", nKills: 0
			}
			]
		}
		,
				gru: {
			name: "Gruuls Unterschlupf",
						playerLevel: 70,
						nPlayers: 25,
						location: "Schergrat",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Gruul der Drachenschlächter", nKills: 0
			}
			]
		}
		,
				ssc: {
			name: "Höhle des Schlangenschreins",
						playerLevel: 70,
						nPlayers: 25,
						location: "Zangarmarschen",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Lady Vashj", nKills: 0
			}
			]
		}
		,
				tk: {
			name: "Festung der Stürme",
						playerLevel: 70,
						nPlayers: 25,
						location: "Nethersturm",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Kael\'thas Sonnenwanderer", nKills: 2
			}
			]
		}
		,
				mh: {
			name: "Die Schlacht um den Hyjal",
						playerLevel: 70,
						nPlayers: 25,
						location: "Höhlen der Zeit",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Archimonde", nKills: 0
			}
			]
		}
		,
				bt: {
			name: "Der Schwarze Tempel",
						playerLevel: 70,
						nPlayers: 25,
						location: "Schattenmondtal",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Illidan Sturmgrimm", nKills: 1
			}
			]
		}
		,
				sp: {
			name: "Der Sonnenbrunnen",
						playerLevel: 70,
						nPlayers: 25,
						location: "Insel von Quel\'Danas",
						expansion: 1,
						heroicMode: false,
						bosses: 	[ {
				name: "Kil\'jaeden", nKills: 1
			}
			]
		}
		,
				voa: {
			name: "Archavons Kammer",
						playerLevel: 80,
						nPlayers: -10,
						location: "Tausendwintersee",
						expansion: 2,
						heroicMode: false,
						bosses: 	[ {
				name: "Archavon der Steinwächter", nKills: 45
			}
			, {
				name: "Emalon der Sturmwächter", nKills: 27
			}
			, {
				name: "Koralon der Flammenwächter", nKills: 19
			}
			, {
				name: "Toravon der Eiswächter", nKills: 20
			}
			]
		}
		,
				nax: {
			name: "Naxxramas",
						playerLevel: 80,
						nPlayers: -10,
						location: "Drachenöde",
						expansion: 2,
						heroicMode: false,
						bosses: 	[ {
				name: "Anub\'Rekhan", nKills: 31
			}
			, {
				name: "Großwitwe Faerlina", nKills: 23
			}
			, {
				name: "Maexxna", nKills: 24
			}
			, {
				name: "Flickwerk", nKills: 20
			}
			, {
				name: "Grobbulus", nKills: 16
			}
			, {
				name: "Gluth", nKills: 13
			}
			, {
				name: "Thaddius", nKills: 11
			}
			, {
				name: "Noth der Seuchenfürst", nKills: 25
			}
			, {
				name: "Heigan der Unreine", nKills: 19
			}
			, {
				name: "Loatheb", nKills: 19
			}
			, {
				name: "Instrukteur Razuvious", nKills: 24
			}
			, {
				name: "Gothik der Ernter", nKills: 21
			}
			, {
				name: "Die vier Reiter", nKills: 18
			}
			, {
				name: "Saphiron", nKills: 12
			}
			, {
				name: "Kel\'Thuzad", nKills: 12
			}
			]
		}
		,
				os: {
			name: "Das Obsidiansanktum",
						playerLevel: 80,
						nPlayers: -10,
						location: "Drachenöde",
						expansion: 2,
						heroicMode: false,
						bosses: 	[ {
				name: "Sartharion", nKills: 23
			}
			]
		}
		,
				eoe: {
			name: "Das Auge der Ewigkeit",
						playerLevel: 80,
						nPlayers: -10,
						location: "Boreanische Tundra",
						expansion: 2,
						heroicMode: false,
						bosses: 	[ {
				name: "Malygos", nKills: 8
			}
			]
		}
		,
				uld: {
			name: "Ulduar",
						playerLevel: 80,
						nPlayers: -10,
						location: "Die Sturmgipfel",
						expansion: 2,
						heroicMode: false,
						bosses: 	[ {
				name: "Flammenleviathan", nKills: 32
			}
			, {
				name: "Klingenschuppe", nKills: 18, optional: true
			}
			, {
				name: "XT-002 Dekonstruktor", nKills: 21
			}
			, {
				name: "Ignis, Meister des Eisenwerks", nKills: 10, optional: true
			}
			, {
				name: "Versammlung des Eisens", nKills: 10
			}
			, {
				name: "Kologarn", nKills: 15
			}
			, {
				name: "Auriaya", nKills: 10
			}
			, {
				name: "Hodir", nKills: 0
			}
			, {
				name: "Thorim", nKills: 5
			}
			, {
				name: "Freya", nKills: 4
			}
			, {
				name: "Leviathan Mk II", nKills: 3
			}
			, {
				name: "General Vezax", nKills: 2
			}
			, {
				name: "Yogg-Saron", nKills: 0
			}
			, {
				name: "Algalon der Beobachter", nKills: 0, optional: true
			}
			]
		}
		,
				ony: {
			name: "Onyxias Hort",
						playerLevel: 80,
						nPlayers: -10,
						location: "Düstermarschen",
						expansion: 2,
						heroicMode: false,
						bosses: 	[ {
				name: "Onyxia", nKills: 9
			}
			]
		}
		,
				toc: {
			name: "Prüfung des Kreuzfahrers",
						playerLevel: 80,
						nPlayers: -10,
						location: "Eiskrone",
						expansion: 2,
						heroicMode: true,
						bosses: 	[ {
				name: "Eisheuler", nKills: 36, nHeroicKills: 2
			}
			, {
				name: "Lord Jaraxxus", nKills: 34, nHeroicKills: 3
			}
			, {
				name: "Bezwingt die Fraktionschampions", nKills: 23, nHeroicKills: 3
			}
			, {
				name: "Eydis Nachtbann", nKills: 22, nHeroicKills: 3
			}
			, {
				name: "Anub\'arak", nKills: 19, nHeroicKills: 13
			}
			]
		}
		,
				icc: {
			name: "Eiskronenzitadelle",
						playerLevel: 80,
						nPlayers: -10,
						location: "Eiskrone",
						expansion: 2,
						heroicMode: true,
						bosses: 	[ {
				name: "Lord Mark\'gar", nKills: 39, nHeroicKills: 16
			}
			, {
				name: "Lady Todeswisper", nKills: 52, nHeroicKills: 5
			}
			, {
				name: "Erringt den Sieg im Luftschiffkampf", nKills: 37, nHeroicKills: 19
			}
			, {
				name: "Der Todesbringer", nKills: 42, nHeroicKills: 11
			}
			, {
				name: "Fauldarm", nKills: 27, nHeroicKills: 10
			}
			, {
				name: "Modermiene", nKills: 26, nHeroicKills: 11
			}
			, {
				name: "Professor Seuchenmord", nKills: 17, nHeroicKills: 2
			}
			, {
				name: "Prinz Valanar", nKills: 18, nHeroicKills: 6
			}
			, {
				name: "Blutkönigin Lana\'thel", nKills: 7, nHeroicKills: 10
			}
			, {
				name: "Rettet Valithria Traumwandler", nKills: 15, nHeroicKills: 6
			}
			, {
				name: "Sindragosa", nKills: 6, nHeroicKills: 1
			}
			, {
				name: "Der Lichkönig", nKills: 5, nHeroicKills: 0
			}
			]
		}
		,
				rs: {
			name: "Das Rubinsanktum",
						playerLevel: 80,
						nPlayers: -10,
						location: "Drachenöde",
						expansion: 2,
						heroicMode: true,
						bosses: 	[ {
				name: "Halion", nKills: 7, nHeroicKills: 0
			}
			]
		}
		,
				bh: {
			name: "Baradinfestung",
						playerLevel: 85,
						nPlayers: -10,
						location: "Tol Barad",
						expansion: 3,
						heroicMode: false,
						bosses: 	[ {
				name: "Argaloth", nKills: 23
			}
			, {
				name: "Occu\'thar", nKills: 12
			}
			, {
				name: "Alizabal", nKills: 2
			}
			]
		}
		,
				bd: {
			name: "Pechschwingenabstieg",
						playerLevel: 85,
						nPlayers: -10,
						location: "Der Schwarzfels",
						expansion: 3,
						heroicMode: true,
						bosses: 	[ {
				name: "Magmaul", nKills: 12, nHeroicKills: 0
			}
			, {
				name: "Toxitron", nKills: 12, nHeroicKills: 0
			}
			, {
				name: "Maloriak", nKills: 11, nHeroicKills: 0
			}
			, {
				name: "Atramedes", nKills: 10, nHeroicKills: 0
			}
			, {
				name: "Schimaeron", nKills: 6, nHeroicKills: 0
			}
			, {
				name: "Nefarian", nKills: 5, nHeroicKills: 0
			}
			]
		}
		,
				bot: {
			name: "Die Bastion des Zwielichts",
						playerLevel: 85,
						nPlayers: -10,
						location: "Schattenhochland",
						expansion: 3,
						heroicMode: true,
						bosses: 	[ {
				name: "Halfus Wyrmbrecher", nKills: 5, nHeroicKills: 5
			}
			, {
				name: "Valiona", nKills: 14, nHeroicKills: 0
			}
			, {
				name: "Elementiumungeheuer", nKills: 11, nHeroicKills: 0
			}
			, {
				name: "Cho\'gall", nKills: 7, nHeroicKills: 0
			}
			, {
				name: "Sinestra", nHeroicKills: 0, optional: true
			}
			]
		}
		,
				tfw: {
			name: "Thron der Vier Winde",
						playerLevel: 85,
						nPlayers: -10,
						location: "Uldum",
						expansion: 3,
						heroicMode: true,
						bosses: 	[ {
				name: "Konklave des Windes", nKills: 11, nHeroicKills: 0
			}
			, {
				name: "Al\'Akir", nKills: 7, nHeroicKills: 0
			}
			]
		}
		,
				fl: {
			name: "Feuerlande",
						playerLevel: 85,
						nPlayers: -10,
						location: "Hyjal",
						expansion: 3,
						heroicMode: true,
						bosses: 	[ {
				name: "Beth\'tilac", nKills: 10, nHeroicKills: 0
			}
			, {
				name: "Lord Rhyolith", nKills: 7, nHeroicKills: 3
			}
			, {
				name: "Alysrazar", nKills: 4, nHeroicKills: 0
			}
			, {
				name: "Shannox", nKills: 7, nHeroicKills: 3
			}
			, {
				name: "Baloroc", nKills: 6, nHeroicKills: 0
			}
			, {
				name: "Majordomus Hirschhaupt", nKills: 4, nHeroicKills: 2
			}
			, {
				name: "Ragnaros", nKills: 3
			}
			, {
				name: "Ragnaros", nHeroicKills: 0
			}
			]
		}
		,
				ds: {
			name: "Drachenseele",
						playerLevel: 85,
						nPlayers: -10,
						location: "Drachenseele",
						expansion: 3,
						heroicMode: true,
						bosses: 	[ {
				name: "Morchok", nLfrKills: 4, nKills: 2, nHeroicKills: 0
			}
			, {
				name: "Kriegsherr Zon\'ozz", nLfrKills: 4, nKills: 3, nHeroicKills: 0
			}
			, {
				name: "Yor\'sahj der Unermüdliche", nLfrKills: 4, nKills: 2, nHeroicKills: 0
			}
			, {
				name: "Hagara die Sturmbinderin", nLfrKills: 4, nKills: 3, nHeroicKills: 0
			}
			, {
				name: "Ultraxion", nLfrKills: 3, nKills: 3, nHeroicKills: 0
			}
			, {
				name: "Kriegsmeister Schwarzhorn", nLfrKills: 3, nKills: 3, nHeroicKills: 0
			}
			, {
				name: "Todesschwinges Rückgrat", nLfrKills: 3, nKills: 2, nHeroicKills: 0
			}
			, {
				name: "Todesschwinges Wahnsinn", nLfrKills: 3, nKills: 2, nHeroicKills: 0
			}
			]
		}
	}
	);
}
);
        //]]>
        </script> 
				</div>
			</div>
			<span class="clear"><!-- --></span>
			<div class="summary-lastupdate"> Letzte Aktualisierung am <?=$char->GetCachedDate();?> </div>
		</div>
	</div>
	<span class="clear"><!-- --></span>
</div>
<script type="text/javascript">
//<![CDATA[

	$(function() {
		Profile.url = '<?=$char->GetCharacterLink()?>/summary';
	});
	
	var MsgProfile = {
		tooltip: {
			feature: {
				notYetAvailable: "Diese Funktion ist derzeit noch nicht verfügbar."
			},
			vault: {
				character: "Diese Sektion ist nur verfügbar, wenn du mit diesem Charakter eingeloggt bist.",
				guild: "Diese Sektion ist nur verfügbar, wenn du mit einem Charakter aus dieser Gilde eingeloggt bist."
			}
		}	
	};
	
//]]>
</script> 
<script type="text/javascript">
//<![CDATA[
var MsgSummary = {
	inventory: {
		slots: {
			1: "Kopf",
			2: "Hals",
			3: "Schultern",
			4: "Hemd",
			5: "Brust",
			6: "Gürtel",
			7: "Füße",
			8: "Beine",
			9: "Handgelenke",
			10: "Hände",
			11: "Finger",
			12: "Schmuck",
			15: "Distanzwaffe",
			16: "Rücken",
			19: "Wappenrock",
			21: "Waffenhand",
			22: "Schildhand",
			28: "Relikt",
			empty: "Dieser Platz ist leer."
		}
	}
	,
	audit: {
		whatIsThis: "Diese Übersicht macht Empfehlungen, wie dieser Charakter verbessert werden könnte. Dazu wird Folgendes überprüft:<br /\><br /\>- Leere Glyphensockel<br /\>- Unverbrauchte Talentpunkte<br /\>- Unverzauberte Gegenstände<br /\>- Leere Sockel<br /\>- Nicht optimale Rüstungsteile<br /\>- Fehlende Gürtelschnalle<br /\>- Unbenutzte Berufsboni",
		missing: "Es fehlt {0}",
		enchants: {
			tooltip: "Unverzaubert"
		}
		,
		sockets: {
			singular: "Leerer Sockel",
			plural: "Leere Sockel"
		}
		,
		armor: {
			tooltip: "Keine {0}",
			1: "Stoff",
			2: "Leder",
			3: "Kettenrüstung",
			4: "Platte"
		}
		,
		lowLevel: {
			tooltip: "Niedrigstufig"
		}
		,
		blacksmithing: {
			name: "Schmieden",
			tooltip: "Fehlende Sockel"
		}
		,
		enchanting: {
			name: "Verzauberkunst",
			tooltip: "Unverzaubert"
		}
		,
		engineering: {
			name: "Ingenieurskunst",
			tooltip: "Fehlende Verbesserung"
		}
		,
		inscription: {
			name: "Inschriftenkunde",
			tooltip: "Fehlende Verzauberung"
		}
		,
		leatherworking: {
			name: "Lederverarbeitung",
			tooltip: "Fehlende Verzauberung"
		}
	}
	,
	talents: {
		specTooltip: {
			title: "Talentspezialisierungen",
			primary: "Primär:",
			secondary: "Sekundär:",
			active: "Aktiv"
		}
	}
	,
	stats: {
		toggle: {
			all: "Alle Statistiken anzeigen",
			core: "Nur Hauptstatistiken anzeigen"
		}
		,
		increases: {
			attackPower: "Erhöht Angriffskraft um {0}.",
			critChance: "Erhöht kritische Trefferchance um {0}%.",
			spellCritChance: "Erhöht kritische Zauberchance um {0}%.",
			health: "Erhöht Gesundheit um {0}.",
			mana: "Erhöht Mana um {0}.",
			manaRegen: "Erhöht Manaregeneration um {0} alle 5 Sekunden, solange nicht gezaubert wird.",
			meleeDps: "Erhöht den Schaden mit Nahkampfwaffen um {0} Schaden pro Sekunde.",
			rangedDps: "Erhöht den Schaden mit Fernkampfwaffen um {0} Schaden pro Sekunde.",
			petArmor: "Erhöht die Rüstunf deines Begleiters um {0}.",
			petAttackPower: "Erhöht die Angriffskraft deines Begleiters um {0}.",
			petSpellDamage: "Erhöht des Zauberschaden deines Begleiters um {0}.",
			petAttackPowerSpellDamage: "Erhöht die Angriffskraft deines Begleiters um {0} und dessen Zauberschaden um {1}."
		}
		,
		decreases: {
			damageTaken: "Reduziert erhaltenen körperlichen Schaden um {0}%.",
			enemyRes: "Reduziert gegnerischen Widerstände um {0}.",
			dodgeParry: "Reduziert die Chance, dass eigene Angriffe pariert oder ihnen ausgewichen wird um {0}%."
		}
		,
		noBenefits: "Beinhaltet keine Vorteile für deine Klasse.",
		beforeReturns: "(Bevor der Nutzen sinkt)",
		damage: {
			speed: "Angriffsgeschwindigkeit (Sekunden):",
			damage: "Schaden:",
			dps: "Schaden pro Sekunde:"
		}
		,
		averageItemLevel: {
			title: "Gegenstandsstufe {0}",
			description: "Die durschschnittliche Gegenstandsstufe deiner besten Ausrüstungsgegenstände. Durch das Erhöhen dieses Wertes erhälst du Zugang zu schwierigeren Dungeons über den Dungeonfinder."
		}
		,
		health: {
			title: "Gesundheit {0}",
			description: "Dein maximaler Gesundheitswert. Wenn deine Gesundheit null erreicht, stirbst du."
		}
		,
		mana: {
			title: "Mana {0}",
			description: "Dein maximaler Manawert. Mana erlaubt es dir, Zauber zu wirken."
		}
		,
		rage: {
			title: "Wut {0}",
			description: "Dein maximaler Wutwert. Wut wird mit dem Verwenden von Fähigkeiten verbraucht und wird wiederhergestellt, indem man Feinde angreift oder im Kampf Schaden erleidet."
		}
		,
		focus: {
			title: "Fokus {0}",
			description: "Dein maximaler Fokuswert. Fokus wird durch das Verwenden von Fähigkeiten verbraucht und regeneriert sich automatisch im Verlauf der Zeit."
		}
		,
		energy: {
			title: "Energie {0}",
			description: "Energie wird durch das Verwenden von Fähigkeiten verbraucht und regeneriert sich automatisch im Verlauf der Zeit."
		}
		,
		runic: {
			title: "Runenmacht {0}",
			description: "Dein maximaler Runenmachtwert."
		}
		,
		strength: {
			title: "Stärke {0}"
		}
		,
		agility: {
			title: "Beweglichkeit {0}"
		}
		,
		stamina: {
			title: "Ausdauer {0}"
		}
		,
		intellect: {
			title: "Intelligenz {0}"
		}
		,
		spirit: {
			title: "Willenskraft {0}"
		}
		,
		mastery: {
			title: "Meisterschaft {0}",
			description: "Meisterschaftswertung von {0} erhöht Meisterschaft um {1}.",
			unknown: "Du musst Meisterschaft bei deinem Lehrer erlernen, bevor dieser Wert einen Effekt hat.",
			unspecced: "Du musst eine Talentspezialisierung auswählen, um Meisterschaft zu aktivieren. "
		}
		,
		meleeDps: {
			title: "Schaden pro Sekunde"
		}
		,
		meleeAttackPower: {
			title: "Nahkampfangriffskraft {0}"
		}
		,
		meleeSpeed: {
			title: "Nahkampfangriffsgeschwindigkeit {0}"
		}
		,
		meleeHaste: {
			title: "Nahkampftempowertung {0}%",
			description: "Nahkampftempowertung von {0} erhöht das Nahkampftempo um {1}%.",
			description2: "Erhöht Nahkampfangriffsgeschwindigkeit."
		}
		,
		meleeHit: {
			title: "Nahkampftrefferchance {0}%",
			description: "Nahkampftrefferwertung von {0} erhöht die Nahkampftrefferchance um {1}%."
		}
		,
		meleeCrit: {
			title: "Kritische Nahkampftrefferchance {0}%",
			description: "Kritische Nahkampftrefferwertung {0} erhöht die kritische Nahkampftrefferchance um {1}%.",
			description2: "Es besteht die Chance, dass Nahkampfangriffe Zusatzschaden verursachen."
		}
		,
		expertise: {
			title: "Waffenkunde {0}",
			description: "Waffenkundewert von {0} erhöht die Waffenkunde um {1}."
		}
		,
		rangedDps: {
			title: "Schaden pro Sekunde"
		}
		,
		rangedAttackPower: {
			title: "Fernkampfangriffskraft {0}"
		}
		,
		rangedSpeed: {
			title: "Fernkampfangriffsgeschwindigkeit {0}"
		}
		,
		rangedHaste: {
			title: "Fernkampftempo {0}%",
			description: "Tempowertung von {0} erhöht Tempo um {1}%.",
			description2: "Erhöht die Fernkampfgeschwindigkeit."
		}
		,
		rangedHit: {
			title: "Fernkampftrefferchance {0}%",
			description: "Trefferchancewertung von {0} erhöht Trefferchance um {1}%."
		}
		,
		rangedCrit: {
			title: "Kritische Fernkampfftrefferchance {0}%",
			description: "Kritische Fernkampfftrefferwertung von {0} erhöht Fernkampfftrefferchance um {1}%.",
			description2: "Es besteht die Chance, dass Fernkampfangriffe Zusatzschaden verursachen."
		}
		,
		spellPower: {
			title: "Zaubermacht {0}",
			description: "Erhöht den Schaden und die Heilung von Zaubern."
		}
		,
		spellHaste: {
			title: "Zaubertempo {0}%",
			description: "Zaubertempowertung von {0} erhöht das Zaubertempo um {1}%.",
			description2: "Erhöht die Wirkungsgeschwindigkeit von Zaubern."
		}
		,
		spellHit: {
			title: "Zaubertrefferchance {0}%",
			description: "Zaubertrefferwertung von {0} erhöht die Zaubertrefferchance um {1}%."
		}
		,
		spellCrit: {
			title: "Kritische Zaubertrefferchance {0}%",
			description: "Kritische Zaubertrefferwertung von {0} erhöht kritische Zaubertrefferchance um {1}%.",
			description2: "Es besteht die Chance, dass Zauber zusätzlichen Schaden oder Heilung verursachen."
		}
		,
		spellPenetration: {
			title: "Zauberdurchschlagskraft {0}"
		}
		,
		manaRegen: {
			title: "Manaregeneration",
			description: "{0} Mana alle 5 Sekunden regeneriert, während ihr euch außerhalb eines Kampfes befindet."
		}
		,
		combatRegen: {
			title: "Kampfregeneration",
			description: "{0} Mana alle 5 Sekunden regeneriert, während ihr euch im Kampf befindet."
		}
		,
		armor: {
			title: "Rüstung {0}"
		}
		,
		dodge: {
			title: "Ausweichchance {0}%",
			description: "Ausweichwertung von {0} erhöht die Ausweichchance um {1}%."
		}
		,
		parry: {
			title: "Parierchance {0}%",
			description: "Parierwertung von {0} erhöht die Parierchance um {1}%."
		}
		,
		block: {
			title: "Blockchance {0}%",
			description: "Blockwertung von {0} erhöht Blockchance um {1}%.",
			description2: "Dein Blocken verhindert {0}% des erlittenen Schadens."
		}
		,
		resilience: {
			title: "Abhärtung {0}",
			description: "Gewährt {0}% Schadensreduzierung, die einem durch andere Spieler oder deren Tiere/Diener zugefügt wird."
		}
		,
		arcaneRes: {
			title: "Arkanwiderstand {0}",
			description: "Reduziert erlittenen Arkanschaden um durchnittlich {0}%."
		}
		,
		fireRes: {
			title: "Feuerwiderstand {0}",
			description: "Reduziert erlittenen Feuerschaden um durchnittlich {0}%."
		}
		,
		frostRes: {
			title: "Frostwiderstand {0}",
			description: "Reduziert erlittenen Frostschaden um durchnittlich {0}%."
		}
		,
		natureRes: {
			title: "Naturwiderstand {0}",
			description: "Reduziert erlittenen Naturschaden um durchnittlich {0}%."
		}
		,
		shadowRes: {
			title: "Schattenwiderstand {0}",
			description: "Reduziert erlittenen Schattenschaden um durchnittlich {0}%."
		}
	}
	,
	recentActivity: {
		subscribe: "Diesen Feed abonnieren"
	}
	,
	raid: {
		tooltip: {
			lfr: "(LFR)",
			normal: "(Normal)",
			heroic: "(Heroisch)",
			complete: "{0}% abgeschlossen ({1}/{2})",
			optional: "(optional)"
		}
	}
	,
	links: {
		tools: "Einstellungen",
		saveImage: "Charakterbild speichern",
		saveimageTitle: "Export your character image for use with the World of Warcraft Rewards Visa credit card."
	}
};
//]]>
</script> 
