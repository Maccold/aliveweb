<div class="content-header">
	<h2 class="header ">Spieler gegen Spieler</h2>
	<span class="clear"><!-- --></span>
</div>

<div class="pvp pvp-summary">
	<div class="pvp-right">
		<div class="top-title">
			<h3 class="category ">Top Arenateams</h3>
			<span class="clear"><!-- --></span>
		</div>

		<div class="top-teams">
	
	<div class="column top-2v2 first-child">
		<h2><a href="<?=url_for("server","pvp-list", array("mode" => "2v2"))?>">2v2</a></h2>
		<ul>
		<? foreach($teams2v2 as $rank => $team_id) {?>
			<li class="<?=$css_classes[$rank]?>">
				<span class="ranking"><?=$rank?></span>
				<div class="name">
					<a href="<?=url_for("server","arena", array("mode" => "2v2", "teamname" => $arenaTeams[$team_id]["name"]))?>"><?=$arenaTeams[$team_id]["name"]?></a>
				</div>
				<div class="rating-realm">
					<span class="rating"><?=$arenaTeams[$team_id]["rating"]?></span>
					<span class="realm"><?=$arenaTeams[$team_id]["faction"]?></span>
				</div>
				<div class="members">
				<? foreach($arenaTeams[$team_id]["members"] as $player) { ?>
					<a href="/character/norgannon/<?=$player["name"]?>/">
						<?=icon_class($player["class"], false)?>
					</a>
				<? } ?>
				</div>
			</li>
		<? } ?>
		</ul>

		<a href="<?=url_for("server","pvp-list", array("mode" => "2v2"))?>" class="all">2v2-Ladder einsehen </a>
	</div>

	<div class="column top-3v3">
		<h2><a href="<?=url_for("server","pvp-list", array("mode" => "3v3"))?>">3v3</a></h2>
		<ul>
		<? foreach($teams3v3 as $rank => $team_id) {?>
			<li class="<?=$css_classes[$rank]?>">
				<span class="ranking"><?=$rank?></span>
				<div class="name">
					<a href="<?=url_for("server","arena", array("mode" => "3v3", "teamname" => $arenaTeams[$team_id]["name"]))?>"><?=$arenaTeams[$team_id]["name"]?></a>
				</div>
				<div class="rating-realm">
					<span class="rating"><?=$arenaTeams[$team_id]["rating"]?></span>
					<span class="realm"><?=$arenaTeams[$team_id]["faction"]?></span>
				</div>
				<div class="members">
				<? foreach($arenaTeams[$team_id]["members"] as $player) { ?>
					<a href="/character/norgannon/<?=$player["name"]?>/">
						<?=icon_class($player["class"], false)?>
					</a>
				<? } ?>
				</div>
			</li>
		<? } ?>
		</ul>

		<a href="<?=url_for("server","pvp-list", array("mode" => "3v3"))?>" class="all">3v3-Ladder einsehen </a>
	</div>

	<div class="column top-5v5">
		<h2><a href="<?=url_for("server","pvp-list", array("mode" => "5v5"))?>">5v5</a></h2>
		<ul>
		<? foreach($teams5v5 as $rank => $team_id) {?>
			<li class="<?=$css_classes[$rank]?>">
				<span class="ranking"><?=$rank?></span>
				<div class="name">
					<a href="<?=url_for("server","arena", array("mode" => "5v5", "teamname" => $arenaTeams[$team_id]["name"]))?>"><?=$arenaTeams[$team_id]["name"]?></a>
				</div>
				<div class="rating-realm">
					<span class="rating"><?=$arenaTeams[$team_id]["rating"]?></span>
					<span class="realm"><?=$arenaTeams[$team_id]["faction"]?></span>
				</div>
				<div class="members">
				<? foreach($arenaTeams[$team_id]["members"] as $player) { ?>
					<a href="/character/norgannon/<?=$player["name"]?>/">
						<?=icon_class($player["class"], false)?>
					</a>
				<? } ?>
				</div>
			</li>
		<? } ?>
		</ul>

		<a href="<?=url_for("server","pvp-list", array("mode" => "5v5"))?>" class="all">5v5-Ladder einsehen </a>
	</div>

	<span class="clear"><!-- --></span>
</div>

<div class="popular">
	<div class="column-right">
		<h3 class="category ">Ehrenhafte Kills - Horde</h3>

		<div class="top-bgs">
			<div class="table ">
		<table>
			<tbody>
			<? foreach($hordeKillers as $rank => $player){ ?>
			<tr class="<?=$player["css"]?>">
				<td class="align-center"><?=$rank?></td>
				<td>
					<a href="/character/norgannon/<?=$player["name"]?>" class="color-c<?=$player["class"]?>">
						<?=icon_class($player["class"])?> <?=$player["name"]?>
					</a>
				</td>
				<td>Norgannon</td>
				<td><span class="rating"><?=$player["totalKills"]?></span></td>
			</tr>
			<? } ?>
			</tbody>
		</table>
			</div>
			<div class="view-all">
				<a href="<?=url_for("server","honor")?>">Zeige volle Liste</a>
			</div>
		</div>
	</div>

	<div class="column-left">
		<h3 class="category ">Ehrenhafte Kills - Allianz</h3>

		<div class="top-bgs">
			<div class="table ">
		<table>
			<tbody>
			<? foreach($allianceKillers as $rank => $player){ ?>
			<tr class="<?=$player["css"]?>">
				<td class="align-center"><?=$rank?></td>
				<td>
					<a href="/character/norgannon/<?=$player["name"]?>" class="color-c<?=$player["class"]?>">
						<?=icon_class($player["class"])?> <?=$player["name"]?>
					</a>
				</td>
				<td>Norgannon</td>
				<td><span class="rating"><?=$player["totalKills"]?></span></td>
			</tr>
			<? } ?>
			</tbody>
		</table>
			</div>
			<div class="view-all">
				<a href="<?=url_for("server","honor")?>">Zeige volle Liste</a>
			</div>
		</div>
	</div>
	<span class="clear"><!-- --></span>

	<div class="column-right">
		<h3 class="category ">Beliebte PvP-Talentverteilungen</h3>
		<div class="class-specs">
		<ul>
			<li>
				<span class="percent">54%</span>
				<span class="class color-c6">
					<?=icon_class(6)?>
					Todesritter </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="/images/icons/18/spell_deathknight_frostpresence.jpg" alt="" width="14" height="14"/>
				</span>
				Frost </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
					59% </span>
				<span class="class color-c11">
					<?=icon_class(11)?>
					Druide </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/ability_racial_bearform.jpg" alt="" width="14" height="14"/>
				</span>
				Wilder Kampf </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				69% </span>
				<span class="class color-c3">
				<?=icon_class(3)?>
				Jäger </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/ability_hunter_focusedaim.jpg" alt="" width="14" height="14"/>
				</span>
				Treffsicherheit </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				69% </span>
				<span class="class color-c8">
				<?=icon_class(8)?>
				Magier </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/spell_frost_frostbolt02.jpg" alt="" width="14" height="14"/>
				</span>
				Frost </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				57% </span>
				<span class="class color-c2">
				<?=icon_class(2)?>
				Paladin </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/spell_holy_holybolt.jpg" alt="" width="14" height="14"/>
				</span>
				Heilig </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				71% </span>
				<span class="class color-c5">
				<?=icon_class(5)?>
				Priester </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/spell_holy_powerwordshield.jpg" alt="" width="14" height="14"/>
				</span>
				Disziplin </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				85% </span>
				<span class="class color-c4">
				<?=icon_class(4)?>
				Schurke </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/ability_stealth.jpg" alt="" width="14" height="14"/>
				</span>
				Täuschung </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				65% </span>
				<span class="class color-c7">
				<?=icon_class(7)?>
				Schamane </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/spell_nature_magicimmunity.jpg" alt="" width="14" height="14"/>
				</span>
				Wiederherstellung </span>
				<span class="clear"><!-- -->	</span>
				</li>
				<li>
				<span class="percent">
				83% </span>
				<span class="class color-c9">
				<?=icon_class(9)?>
				Hexenmeister </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/spell_shadow_deathcoil.jpg" alt="" width="14" height="14"/>
				</span>
				Gebrechen </span>
				<span class="clear"><!-- -->	</span>
			</li>
			<li>
				<span class="percent">
				86% </span>
				<span class="class color-c1">
				<?=icon_class(1)?>
				Krieger </span>
				<span class="tree">
				<span class="icon-frame frame-14 ">
					<img src="http://eu.media.blizzard.com/wow/icons/18/ability_warrior_savageblow.jpg" alt="" width="14" height="14"/>
				</span>
				Waffen </span>
				<span class="clear"><!-- -->	</span>
				</li>
			</ul>
			</div>
		</div>

		<div class="column-left">
			<h3 class="category ">Beliebte Teamzusammenstellungen</h3>
			<div class="team-comps">
				<div class="comp comp-2v2 first-child">
					<ul>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(6)?>
						<span class="percent">7%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(8)?>
						<span class="percent">6%</span>
					</li>
					<li>
						<?=icon_class(4)?>
						<?=icon_class(8)?>
						<span class="percent">5%</span>
					</li>
					<li>
						<?=icon_class(1)?>
						<?=icon_class(2)?>
						<span class="percent">4%</span>
					</li>
					<li>
						<?=icon_class(4)?>
						<?=icon_class(5)?>
						<span class="percent">4%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(11)?>
						<span class="percent">4%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(6)?>
						<span class="percent">3%</span>
					</li>
					<li>
						<?=icon_class(7)?>
						<?=icon_class(9)?>
						<span class="percent">3%</span>
					</li>
					<li>
						<?=icon_class(1)?>
						<?=icon_class(7)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(11)?>
						<span class="percent">2%</span>
					</li>
					</ul>
					<h3>2v2</h3>
				</div>

				<div class="comp comp-3v3">
					<ul>
					<li>
						<?=icon_class(4)?>
						<?=icon_class(5)?>
						<?=icon_class(8)?>
						<span class="percent">4%</span>
					</li>
					<li>
						<?=icon_class(1)?>
						<?=icon_class(2)?>
						<?=icon_class(6)?>
						<span class="percent">3%</span>
					</li>
					<li>
						<?=icon_class(4)?>
						<?=icon_class(7)?>
						<?=icon_class(9)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(1)?>
						<?=icon_class(2)?>
						<?=icon_class(11)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(4)?>
						<?=icon_class(6)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(8)?>
						<?=icon_class(11)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(5)?>
						<?=icon_class(6)?>
						<span class="percent">1%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(7)?>
						<?=icon_class(9)?>
						<span class="percent">1%</span>
					</li>
					<li>
						<?=icon_class(7)?>
						<?=icon_class(8)?>
						<?=icon_class(9)?>
						<span class="percent">1%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(3)?>
						<?=icon_class(6)?>
						<span class="percent">1%</span>
					</li>
					</ul>
					<h3>3v3</h3>
				</div>


				<div class="comp comp-3v3">
					<ul>
					<li>
						<?=icon_class(4)?>
						<?=icon_class(5)?>
						<?=icon_class(8)?>
						<span class="percent">4%</span>
					</li>
					<li>
						<?=icon_class(1)?>
						<?=icon_class(2)?>
						<?=icon_class(6)?>
						<span class="percent">3%</span>
					</li>
					<li>
						<?=icon_class(4)?>
						<?=icon_class(7)?>
						<?=icon_class(9)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(1)?>
						<?=icon_class(2)?>
						<?=icon_class(11)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(4)?>
						<?=icon_class(6)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(8)?>
						<?=icon_class(11)?>
						<span class="percent">2%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(5)?>
						<?=icon_class(6)?>
						<span class="percent">1%</span>
					</li>
					<li>
						<?=icon_class(5)?>
						<?=icon_class(7)?>
						<?=icon_class(9)?>
						<span class="percent">1%</span>
					</li>
					<li>
						<?=icon_class(7)?>
						<?=icon_class(8)?>
						<?=icon_class(9)?>
						<span class="percent">1%</span>
					</li>
					<li>
						<?=icon_class(2)?>
						<?=icon_class(3)?>
						<?=icon_class(6)?>
						<span class="percent">1%</span>
					</li>
					</ul>
					<h3>3v3</h3>
				</div>
			
				<span class="clear"><!-- --></span>
			</div>
		</div>
	</div>
</div> <!-- /.pvp-right -->
	
	<div class="pvp-left">
		<ul class="dynamic-menu" id="menu-pvp">
			<li class="root-item     item-active">
				<a href="<?=url_for("server","pvp")?>"><span class="arrow">Übersicht</span></a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","pvp-list", array("mode" => "2v2"))?>">
					<span class="arrow">2v2</span>
				</a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","pvp-list", array("mode" => "3v3"))?>">
					<span class="arrow">3v3</span>
				</a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","pvp-list", array("mode" => "5v5"))?>">
					<span class="arrow">5v5</span>
				</a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","honor")?>">
					<span class="arrow">Ehrenhafte Kills</span>
				</a>
			</li>
		</ul>
	</div>
	<span class="clear"><!-- --></span>
</div>
	