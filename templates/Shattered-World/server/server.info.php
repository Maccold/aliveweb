
<?php builddiv_start(0, "Server Info") ?>

<div class="left-col">
	<div class="services-content">
	
<div class="table">
<table class="serverstatus1" align="center" cellpadding="0">
    <tr>
		<th colspan="2">Server Info</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_straight as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? htmlspecialchars($config_details[$key]) : ''; ?></td>
	</tr>
<?php endforeach; ?>
    <tr>
		<th colspan="2">Horde und Alliance Zusammenarbeit</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_true_false as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? (($config_details[$key] == 1) ? 'Ja' : 'Nein') : ''; ?></td>
	</tr>
<?php endforeach; ?>
    <tr>
		<th colspan="2">Server Raten</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_blizzlike as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? htmlspecialchars($config_details[$key]) . ' x BlizzLike' : ''; ?></td>
	</tr>
<?php endforeach; ?>
    <tr>
		<th colspan="2">Berufsskill</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_skillchances as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? htmlspecialchars($config_details[$key]) . '%' : ''; ?></td>
	</tr>
<?php endforeach; ?>
</table>
</div>

	</div>
</div>

<div class="right-col">
	<? echo $server_sidebar; ?>
</div>
<span class="clear"><!-- --></span>
<?php builddiv_end() ?><br>
