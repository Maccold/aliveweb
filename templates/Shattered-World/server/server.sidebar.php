	<div class="sub-services">					
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Spieler Online</span>
			</div>
			<ul>
				<li><a href="<?=url_for("server","playersonline")?>" class="account-security" ><span>Liste der Spieler Online</span></a></li>
				<li><a href="<?=url_for("server","playermap")?>" class="account-security" ><span>Weltkarte</span></a></li>
			</ul>					
		</div>
		
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Server Informationen</span>
			</div>
			<ul>
				<li><a href="<?=url_for("server","howtoplay")?>" class="account-security" ><span>Installationsanleitung</span></a></li>
				<li><a href="<?=url_for("server","gms")?>" class="account-security" ><span>Das Alive Team</span></a></li>
				<li><a href="<?=url_for("server","changelog")?>" class="account-security" ><span>Server Changelog</span></a></li>
				<li><a href="<?=url_for("community","clpage")?>" class="account-security" ><span>Page Changelog</span></a></li>
			</ul>					
		</div>
	
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Media</span>
			</div>
			<ul>
				<li><a href="<?=url_for("community","player")?>" class="account-security" ><span>Spielerbilder</span></a></li>
				<li><a href="<?=url_for("community","tc001")?>" class="account-security" ><span>Videos</span></a></li>
			</ul>					
		</div>
		
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Server Seiten</span>
			</div>
			<ul>
				<li><a href="<?=url_for("community","bugtracker")?>" class="account-security" ><span>Bugtracker</span></a></li>
				<li><a href="<?=url_for("server","ranking")?>" class="account-security" ><span>Erfolgs-Ladder</span></a></li>
				<li><a href="<?=url_for("server","ranking")?>itemlevel" class="account-security" ><span>Gear-Ladder</span></a></li>
				<li><a href="<?=url_for("community","poll")?>" class="account-security" ><span>Umfragen</span></a></li>
			</ul>					
		</div>
		
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Mehr Information</span>
			</div>
			<div class="content-block">
			<ul>
				<li>
					<span class="block content-2">
						<span class="content-title"><a href="<?=url_for("server", "pvp")?>">Arena-Ladders der Season</a></span>
						<span class="content-desc">
							Die gewerteten Arena-Ladders aktuellen Season sind da. Werft jetzt einen Blick darauf!
							<span class="content-block-arenalinks">
								<a href="<?=url_for("server", "pvp-list", array("mode" => "2v2"))?>" target="_blank">2v2</a> - 
								<a href="<?=url_for("server", "pvp-list", array("mode" => "3v3"))?>" target="_blank">3v3</a> -
								<a href="<?=url_for("server", "pvp-list", array("mode" => "5v5"))?>" target="_blank">5v5</a>
							</span>
						</span>
					</span>
				</li>
				<li>
					<span class="block content-1">
						<a href="<?=url_for("server", "realmstatus")?>">
							<span class="content-title">Realmstatus</span>
							<span class="content-desc">Informiert euch über den aktuellen Status aller Realms von WoW Alive.</span>
						</a>
					</span>
				</li>
				<li>
					<span class="block content-3">
						<a href="http://portal.wow-alive.de/search/" target="_blank">
							<span class="content-title">Du suchst nach dem Arsenal?</span>
							<span class="content-desc">Findet heraus, wie eure Charaktere oder die von anderen Spielern ausgerüstet sind.</span>
						</a>
					</span>
				</li>
			</ul>
			</div>
		</div>
	</div>