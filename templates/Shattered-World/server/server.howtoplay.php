<?php
if( INCLUDED !== true)
	exit;
?>

<?php builddiv_start(1, "WoW Alive Installationsanleitung"); ?>
<div class="left-col">
	<div class="services-content">
		<?php echo $content; ?>
	</div>
</div>

<div class="right-col">
	<? echo $server_sidebar; ?>
</div>
<span class="clear"><!-- --></span>

<?php builddiv_end(); ?>