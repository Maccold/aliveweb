<div class="content-header">
	<h2 class="header ">Spieler gegen Spieler</h2>
	<span class="clear"><!-- --></span>
</div>

<div class="pvp pvp-ladder">
	<div class="pvp-right">
		<div class="ladder-title">
			<h3 class="category">Arena Teams <?=$mode?></h3>
		</div>

		<div id="ladders">
			<div class="table-options data-options ">
				<div class="option">
				<? if($numPages > 1) echo paginate($numPages, $page, array("server", "pvp-list", array("mode" => $mode))); ?>
				</div>
				Zeige <strong class="results-start"><?=$firstTeamNumber?></strong>–<strong class="results-end"><?=$lastTeamNumber?></strong> 
				von <strong class="results-total"><?=$sumTeams?></strong> Ergebnissen
				<span class="clear"><!-- --></span>
			</div>

			<div class="table ">
		<table>
			<thead>
			<tr>
				<th><span class="sort-tab"><span> # </span></span></th>
				<th><span class="sort-tab"><span> Team </span></span></th>
				<th><span class="sort-tab"><span> Realm </span></span></th>
				<th><span class="sort-tab"><span> Fraktion </span></span></th>
				<th><span class="sort-tab"><span> Siege </span></span></th>
				<th><span class="sort-tab"><span> Niederlagen </span></span></th>
				<th><span class="sort-tab"><span> Wertung </span></span></th>
			</tr>
			</thead>
			<tbody>
			<? foreach($arenaTeams as $n => $team){ ?>
			<tr class="<?=$team["css"]?>" id="rank-<?=$team["rank"]?>">
				<td class="ranking">
					<?=$team["rank"]?>
				<? if($team["lastweek_rank"] == $team["rank"]){ ?>
					<span id="rank-tooltip-<?=$team["rank"]?>" style="display: none">
						Aktuelle Platzierung in der Ladder
					</span>
					<span class="arrow-new" data-tooltip="#rank-tooltip-<?=$team["rank"]?>"></span>
				<? } else if($team["lastweek_rank"] == 0){ ?>
					<span id="rank-tooltip-<?=$team["rank"]?>" style="display: none">
						Neuplatzierung in der Ladder
					</span>
					<span class="arrow-up" data-tooltip="#rank-tooltip-<?=$team["rank"]?>"></span>
				<? } else if($team["rank"] < $team["lastweek_rank"]){ ?>
					<span id="rank-tooltip-<?=$team["rank"]?>" style="display: none">
						Letzter Rang: <strong><?=$team["lastweek_rank"]?></strong><br />
						Aufgestiegen um <?=$team["lastweek_rank"]-$team["rank"]?> Ränge
					</span>
					<span class="arrow-up" data-tooltip="#rank-tooltip-<?=$team["rank"]?>"></span>
				<? } else if($team["rank"] > $team["lastweek_rank"]){ ?>
					<span id="rank-tooltip-<?=$team["rank"]?>" style="display: none">
						Letzter Rang: <strong><?=$team["lastweek_rank"]?></strong><br />
						Abgestiegen um <?=$team["rank"]-$team["lastweek_rank"]?> Ränge
					</span>
					<span class="arrow-down" data-tooltip="#rank-tooltip-<?=$team["rank"]?>"></span>
				<? } ?>
				</td>
				<td>
					<div class="player-icons">
					<? foreach($team["members"] as $member){ ?>
						<a href="/character/norgannon/<?=$member["name"]?>">
							<?=icon_class($member["class"], false)?>
						</a>
					<? } ?>
					</div>
					<a href="<?=url_for("server","arena", array(
						"mode" => $mode,
						"teamname" => $team["name"]
						))?>"><?=$team["name"]?></a>
				</td>
				<td>Norgannon</td>
				<td class="align-center">
					<?=icon_faction($team["faction"])?>
				</td>
				<td class="align-center"><span class="win"><?=$team["seasonWins"]?></span></td>
				<td class="align-center"><span class="loss"><?=($team["seasonGames"]-$team["seasonWins"])?></span></td>
				<td class="align-center"><span class="rating"><?=$team["rating"]?></span></td>
			</tr>
			<? } ?>
			</tbody>
		</table>
			</div>

			<div class="table-options data-options ">
				<div class="option">
					<? if($numPages > 1) echo paginate($numPages, $page, array("server", "pvp-list", array("mode" => $mode)) ); ?>
				</div>
				Zeige <strong class="results-start"><?=$firstTeamNumber?></strong>–<strong class="results-end"><?=$lastTeamNumber?></strong> 
				von <strong class="results-total"><?=$sumTeams?></strong> Ergebnissen
				<span class="clear"><!-- --></span>
			</div>
		</div> <!-- /.ladders -->
	</div> 

	<div class="pvp-left">
		<ul class="dynamic-menu" id="menu-pvp">
			<li class="root-item back-to">
				<a href="<?=url_for("server","pvp")?>"><span class="arrow">Übersicht</span></a>
			</li>
			<li class="has-submenu<? if($type == 2) echo " item-active";?>">
				<a href="<?=url_for("server","pvp-list", array("mode" => "2v2"))?>">
					<span class="arrow">2v2</span>
				</a>
			</li>
			<li class="has-submenu<? if($type == 3) echo " item-active";?>">
				<a href="<?=url_for("server","pvp-list", array("mode" => "3v3"))?>">
					<span class="arrow">3v3</span>
				</a>
			</li>
			<li class="has-submenu<? if($type == 5) echo " item-active";?>">
				<a href="<?=url_for("server","pvp-list", array("mode" => "5v5"))?>">
					<span class="arrow">5v5</span>
				</a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","honor")?>">
					<span class="arrow">Ehrenhafte Kills</span>
				</a>
			</li>
		</ul>
	</div>

	<span class="clear"><!-- --></span>
</div>
	