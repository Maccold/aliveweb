<div class="content-header">
	<h2 class="header ">Spieler gegen Spieler</h2>
	<span class="clear"><!-- --></span>
</div>

<div class="pvp pvp-summary">
	<div class="pvp-right">
		<div class="top-title">
			<h3 class="category ">Top Arenateams</h3>
			<span class="clear"><!-- --></span>
		</div>

<div class="popular">
	<div class="column-right">
		<h3 class="category ">Ehrenhafte Kills - Horde</h3>

		<div class="top-bgs">
			<div class="table ">
		<table>
			<tbody>
			<? foreach($hordeKillers as $rank => $player){ ?>
			<tr class="<?=$player["css"]?>">
				<td class="align-center"><?=$rank?></td>
				<td>
					<a href="/character/Norgannon/<?=$player["name"]?>" class="color-c<?=$player["class"]?>">
						<?=icon_class($player["class"])?> <?=$player["name"]?>
					</a>
				</td>
				<td>Norgannon</td>
				<td><span class="rating"><?=$player["totalKills"]?></span></td>
			</tr>
			<? } ?>
			</tbody>
		</table>
			</div>
			<div class="view-all">
				<a href="<?=url_for("server","honor")?>">Zeige volle Liste</a>
			</div>
		</div>
	</div>

	<div class="column-left">
		<h3 class="category ">Ehrenhafte Kills - Allianz</h3>

		<div class="top-bgs">
			<div class="table ">
		<table>
			<tbody>
			<? foreach($allianceKillers as $rank => $player){ ?>
			<tr class="<?=$player["css"]?>">
				<td class="align-center"><?=$rank?></td>
				<td>
					<a href="/character/Norgannon/<?=$player["name"]?>" class="color-c<?=$player["class"]?>">
						<?=icon_class($player["class"])?> <?=$player["name"]?>
					</a>
				</td>
				<td>Norgannon</td>
				<td><span class="rating"><?=$player["totalKills"]?></span></td>
			</tr>
			<? } ?>
			</tbody>
		</table>
			</div>
			<div class="view-all">
				<a href="<?=url_for("server","honor")?>">Zeige volle Liste</a>
			</div>
		</div>
	</div>
	<span class="clear"><!-- --></span>

	
	</div>
</div> <!-- /.pvp-right -->
	
	<div class="pvp-left">
		<ul class="dynamic-menu" id="menu-pvp">
			<li class="root-item back-to">
				<a href="<?=url_for("server","pvp")?>"><span class="arrow">Übersicht</span></a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","pvp-list", array("mode" => "2v2"))?>">
					<span class="arrow">2v2</span>
				</a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","pvp-list", array("mode" => "3v3"))?>">
					<span class="arrow">3v3</span>
				</a>
			</li>
			<li class="has-submenu">
				<a href="<?=url_for("server","pvp-list", array("mode" => "5v5"))?>">
					<span class="arrow">5v5</span>
				</a>
			</li>
			<li class="has-submenu item-active">
				<a href="<?=url_for("server","honor")?>">
					<span class="arrow">Ehrenhafte Kills</span>
				</a>
			</li>
		</ul>
	</div>
	<span class="clear"><!-- --></span>
</div>
	