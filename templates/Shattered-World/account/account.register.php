<script type="text/javascript">
<!--
var MIN_LOGIN_L = <?php echo $regparams['MIN_LOGIN_L']; ?>;
var MAX_LOGIN_L = <?php echo $regparams['MAX_LOGIN_L']; ?>;
var MIN_PASS_L  = <?php echo $regparams['MIN_PASS_L']; ?>;
var MAX_PASS_L  = <?php echo $regparams['MAX_PASS_L']; ?>;
var SUCCESS = false;

// -->
</script>
<style media="screen" title="currentStyle" type="text/css">
p.nm, p.wm { 
	margin: 0.5em 0 0.5em 0; 
	padding: 3px; 
}
	
p.nm { 
	background-color: #FEF5DA; 
	border-right: 1px solid #D0CBAF;
	border-bottom: 1px solid #D0CBAF; 
	color: #605033; 
}
	
p.wm { 
	background-color: #FBD8D7; 
	border-right: 1px solid #DCBFB4;
	border-bottom: 1px solid #DCBFB4; 
	color: #6A0D0B; 
}
#regform label {
	display: block;
	margin-top: 1em;
	font-weight: bold;
}
p.nm, p.wm { 
	margin: 0px;
	margin-top: 3px;
}
</style>

<div class="top-banner">
	<div class="section-title">
		<span>Registration</span>
		<p>&nbsp;</p>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">
<?php
echo "<!-- Step: ".$step ." -->";

if(count($err_array)){
	?>
	<div class="talkback-code">
		<div class="talkback-code-interior">
			<div class="talkback-icon">
        		<h4 class="code-header">WoW Alive Meldung</h4>
				<div align="left">
					<!-- main error message -->
					<div style="margin: 10px">
					<? foreach($err_array as $line ){ echo "\n$line <br/>"; }?>
					</div>
					<!-- / main error message -->
				</div>
			</div> <!-- /talkback-icon -->
		</div> <!-- /talkback-code-interior -->
	</div>
	<?	
}

	
if( $step == STEP_START && (int)$MW->getConfig->generic->req_reg_key == 1)
{
?>
<form method="post" action="<?=url_for("account", "register")?>">
        <input type="hidden" name="step" value="<?=STEP_FIRST?>"/>
        <div style="margin:4px;padding:6px 9px 6px 9px;text-align:left;">
        <b><?php echo $lang['reg_key'];?>:</b> <input type="text" class="input" name="r_key" value="<?=$form_data["r_key"]?>" size="45" maxlength="50"/>
        </div>
        <div style="background:none;margin:4px;padding:6px 9px 0px 9px;text-align:left;">
        <button type="submit" class="ui-button button1 button1-next"><span><span><?php echo $lang['next'];?></span></span></button>
        </div>
</form>
<?php
}
elseif( $step == STEP_FIRST || ( $step == STEP_START && (int)$MW->getConfig->generic->req_reg_key == 0 ) )
{
	?>
<form method="post" action="<?=url_for("account", "register")?>">
	<input type="hidden" name="step" value="<?=STEP_FORM?>"/>
	<input type="hidden" name="r_key" value="<?php echo $_POST['r_key'];?>"/>
	<div style="margin:4px;padding:6px 9px 6px 9px;text-align:left;">
		<? write_subheader($lang['rules_agreement']) ?>
	</div>
	<div style="color: red"><?php echo $lang['warn_email'] ?></div>
	<br/>
	
	<?php echo lang_resource('acc_create_rules.html'); ?>
	
	<div style="margin:4px;padding:6px 9px 0px 9px;text-align:left;">
		<a href="<?=url_for("home", "index")?>" class="ui-button button1"><span><span><?php echo $lang['disagree']; ?></span></span></a>&nbsp;&nbsp;
		<button type="submit" class="ui-button button1 button1-next"><span><span><?php echo $lang['agree']; ?></span></span></button>
	</div>
</form>
	<?php
}
elseif( $step == STEP_FORM )
{
	?>
<form method="post" action="<?=url_for("account", "register")?>" name="regform" id="regform" onsubmit="return check_all();">
        <input type="hidden" name="r_key" value="<?php echo $_POST['r_key'];?>"/>
        <input type="hidden" name="step" value="<?=STEP_LAST?>"/>

        <label for="r_login"><?php echo $lang['username'];?>:</label>
        <input type="text" class="input" id="r_login" name="r_login" value="<?=$form_data["r_login"]?>" size="40" maxlength="16" onblur="check_login();"/>
        <p id="t_login" style="display:none;" class="wm"></p>

        <label for="r_pass"><?php echo $lang['pass'];?>:</label>
        <input type="password" id="r_pass" name="r_pass" value="<?=$form_data["r_pass"]?>" size="40" maxlength="16" onblur="check_pass();"/>
        <p id="t_pass" style="display:none;" class="wm"></p>

        <label for="r_cpass"><?php echo $lang['cpass'];?>:</label>
        <input type="password" id="r_cpass" name="r_cpass" value="<?=$form_data["r_cpass"]?>" size="40" maxlength="16" onblur="check_cpass();"/>
        <p id="t_cpass" style="display:none;" class="wm"></p>

        <label for="r_email"><?php echo $lang['email'];?>:</label>
        <input type="text" class="input" id="r_email" name="r_email" value="<?=$form_data["r_email"]?>" size="40" maxlength="50" onblur="check_email();"/>
        <p id="t_email" style="display:none;" class="wm"></p>

		<?php if ((int)$MW->getConfig->generic_values->account_registrer->secret_questions_input == 0 && false): ?>

		<label for="secretq1"><?php echo $lang['secretq']; ?> 1:</label>
        Q: <select class="input select" id="secretq1" name="secretq1">
            <option value="Disabled">Disabled</option>
            <?php foreach ($MW->getConfig->secret_questions->question as $question): ?>
            <option value="<?php echo htmlspecialchars($question); ?>"><?php echo $question; ?></option>
            <?php endforeach; ?>
        </select><br />
        A: <input type="hidden" id="secreta1" name="secreta1" value="<?=$form_data["secreta1"]?>" size="40" maxlength="50"/>

        <label for="secretq2"><?php echo $lang['secretq']; ?> 2:</label>
        Q: <select class="input select" id="secretq2" name="secretq2">
            <option value="Disabled">Disable</option>
            <?php foreach ($MW->getConfig->secret_questions->question as $question): ?>
            <option value="<?php echo htmlspecialchars($question); ?>"><?php echo $question; ?></option>
            <?php endforeach; ?>
        </select><br />
        A: <input type="hidden" id="secreta2" name="secreta2" value="<?=$form_data["secreta2"]?>" size="40" maxlength="50"/>
        
        <?php endif; ?>

        <?php if ((int)$MW->getConfig->generic_values->account_registrer->secret_questions_input): ?>
        <label for="secretq1"><?php echo $lang['secretq']; ?> 1:</label>
        Q: <select class="input select" id="secretq1" name="secretq1">
            <option value="0">None</option>
            <?php foreach ($MW->getConfig->secret_questions->question as $question): ?>
            <option value="<?php echo htmlspecialchars($question); ?>"><?php echo $question; ?></option>
            <?php endforeach; ?>
        </select><br />
        A: <input type="text" class="input" id="secreta1" name="secreta1" value="<?=$form_data["secreta1"]?>" size="40" maxlength="50"/>

        <label for="secretq2"><?php echo $lang['secretq']; ?> 2:</label>
        Q: <select class="input select" id="secretq2" name="secretq2">
            <option value="0">None</option>
            <?php foreach ($MW->getConfig->secret_questions->question as $question): ?>
            <option value="<?php echo htmlspecialchars($question); ?>"><?php echo $question; ?></option>
            <?php endforeach; ?>
        </select><br />
        A: <input type="text" class="input" id="secreta2" name="secreta2" value="<?=$form_data["secreta2"]?>" class="input" size="40" maxlength="50"/>
        <?php endif; ?>

        <label for="r_account_type"><?php echo $lang['exp_select']; ?>:</label>
        <select class="input select" id="r_account_type" name="r_account_type">
        <option selected="selected" value="2"><?php echo $lang['wotlk'];?></option>
		<option value="1"><?php echo $lang['tbc'];?></option>
        <option value="0"><?php echo $lang['classic'];?></option>
        </select><br /><br />

	<?php
	if ((int)$MW->getConfig->generic_values->account_registrer->enable_image_verfication):
		
		// Initialize random image:
		$captcha=new Captcha;
		$captcha->load_ttf();
		$captcha->make_captcha();
		$captcha->delold();
		$filename=$captcha->filename;
		$privkey=$captcha->privkey;
		$DB->query("INSERT INTO `acc_creation_captcha`(filename, acc_creation_captcha.key) VALUES('$filename','$privkey')");
		?>
		<img src="/<?php echo $filename; ?>" alt=""/><br />
		<input type="hidden" name="filename_image" value="<?php echo $filename; ?>"/>
		<b>Type letters above (6 characters)</b>
		<br />
		<input type="text" class="input" name="image_key"/><br />
	<?php endif; ?>
        
        <br />
        <button type="submit" class="ui-button button1 button1-next"><span><span><?php lang('register');?></span></span></button>
</form>
<?php
}
elseif( $step == STEP_LAST )
{
	if($reg_succ === true){
		if((int)$MW->getConfig->generic->req_reg_act){
			echo $lang['email_sent_act'];
		}
		else{
			echo $lang['reg_succ'].'<meta http-equiv=refresh content="3;url='.$MW->getConfig->temp->site_href.'">';
		}
	}
}

?>
				</div> <!-- /sub-services-section -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>

