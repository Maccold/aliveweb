<?php
if(INCLUDED!==true)
	exit; 
?>
<style type="text/css">
td{ padding: 2px; }
td.label{
    font-weight: bold;
    font-size: 12px;
}
</style>

<div class="top-banner">
	<div class="section-title">
		<span><?php echo $lang['change_your_info'];?></span>
		<p><?=$lang[accountmanange_intro]?></p>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">
				
<?php write_subheader($lang['general_info']); ?>

<form name="mainform" method="post" action="<?=url_for("account","manage"); ?>" enctype="multipart/form-data" onsubmit="return validateforms(this)">
<input type="hidden" name="action" value="change" />

<table border="0" cellspacing="2" cellpadding="4">
<tr>
	<td align="right" valign="top" width="180"><span><b><?php echo $lang['username'];?></b></span></td>
    <td align="left"><?=$profile['username']?></td>
</tr>
<tr>
      <td align="right" valign="top"><span><b>Account-ID</b></span></td>
      <td align="left"><?=$userObject->id?></td>
</tr>
<tr>
      <td width="200" align="right"><span><b><?php echo $lang['hideemail'];?>&#058;</b></span></td>
   	  <td>
	  	<select name="profile[hideemail]" style="margin:1px;">
            <option value="0"<?php if($profile['hideemail']=="0")echo' selected';?>><?php echo $lang['no'];?> </option>
            <option value="1"<?php if($profile['hideemail']==1)echo' selected';?>><?php echo $lang['yes'];?> </option>
        </select>
	  </td>
</tr>
<tr>
      <td align="right"><span><b><?php echo $lang['hideprofile'];?>&#058;</b></span></td>
      <td align="left">
	  	<select name="profile[hideprofile]" style="margin:1px;">
            <option value="0"<?php if($profile['hideprofile']=="0")echo' selected';?>><?php echo $lang['no'];?> </option>
            <option value="1"<?php if($profile['hideprofile']==1)echo' selected';?>><?php echo $lang['yes'];?> </option>
      </select>
	  </td>
</tr>

<tr>
      <td align="right"><span><b><?php echo $lang['gender'];?>&#058;</b></span></td>
    <td align="left">
	  	<select name="profile[gender]">
            <option value="0"<?php if($profile['gender']=="0")echo' selected';?>><?php echo $lang['notselected'];?> </option>
            <option value="1"<?php if($profile['gender']==1)echo' selected';?>><?php echo $lang['male'];?> </option>
            <option value="2"<?php if($profile['gender']==2)echo' selected';?>><?php echo $lang['female'];?> </option>
    	</select>
	</td>
</tr>

<tr>
      <td align="right"><span><b><?php echo $lang['homepage'];?>&#058;</b></span></td>
      <td align="left">
	  	<input name="profile[homepage]" type="text" size="36" style="margin:1px;" value="<?php echo$profile['homepage'];?>" />
	  </td>
</tr>
<tr>
    <td align="right" nowrap="nowrap"><span><b><?php echo $lang['icq'];?></b></span></td>
    <td align="left"><input name="profile[icq]" type="text" size="36" style="margin:1px;" value="<?php echo$profile['icq'];?>" /></td>
</tr>

<tr>
      <td align="right" valign="top"><span><b><?php echo $lang['msn'];?></b></span></td>
      <td align="left">
	  	<input name="profile[msn]" type="text" size="36" style="margin:1px;" value="<?php echo$profile['msn'];?>" /></td>
</tr>

<tr>
      <td align="right"><span><b><?php echo $lang['wherefrom'];?></b></span></td>
      <td align="left">
	  	<input name="profile[location]" type="text" size="36" style="margin:1px;" value="<?php echo$profile['location'];?>" />
      </td>
</tr>

<?php if((int)$MW->getConfig->generic->change_template) { ?>
<tr>
      <td align="right">
      <span><b>
     	<?php echo$lang['theme'];?>
	  </b></span>
      </td>
      <td align="left">
	  	<select name="profile[theme]" style="margin:1px;">
            <?php
            $i = 0;
            foreach($MW->getConfig->templates->template as $template){ ?>
            <option value="<?php echo$i;?>"<?php if($profile['theme']==$i)echo' selected';?>><?php echo (string)$template;?>
            <?php $i++; } ?>
            </option>
        </select>
      </td>
</tr>
<?php } ?>

<?php /*if($profile['avatar']) { ?>
<tr>
      <td align="right">
      <span><b>
     	<?php echo $lang['avatar'];?>
			<br />
      </b></span>
      </td>
      <td align="left">
	  	<img src="<?php echo (string)$MW->getConfig->generic->avatar_path;?><?php echo$profile['avatar'];?>" style="margin:1px;"> <br/>
          <input type="hidden" name="avatarfile" value="<?php echo$profile['avatar'];?>">
          <b><?php echo $lang['delavatar'];?></b>
          <input type="checkbox" size="36" name="deleteavatar" style="margin:1px;" value="1">
      </td>
</tr>
<?php }else{ ?>
<tr>
      <td align="right">
      <span><b>
			<?php echo $lang['avatar'];?>
			<img src="<?php echo $currtmp; ?>/images/icons2/warning.gif" width="15" height="15"
			 onmouseover="ddrivetip('<?php echo $lang['maxavatarsize'];?>: <?php echo (int)$MW->getConfig->generic->max_avatar_file;?> bytes, <?php echo $lang['maxavatarres'];?> <?php echo (string)$MW->getConfig->generic->max_avatar_size;?> px.<br/>','#ffffff')";
			 onmouseout="hideddrivetip()">
			<br />
      </b></span>
      </td>
      <td align="left">
		<input type="file" size="36" name="avatar" style="margin:1px;">
      </td>
</tr>
<?php }*/ ?>
<? /*
<tr>
      <td align="right">
      <span><b>
			<?php echo $lang['signature'];?>
			<img src="<?php echo $currtmp; ?>/images/icons2/info.gif" width="16" height="16"
			 onmouseover="ddrivetip('You may use normal BBcode tags in signature.','#ffffff')";
			 onmouseout="hideddrivetip()">
			<br />
      </b></span>
      </td>
      <td align="left">
	  	<textarea name="profile[signature]" maxlength="255" rows="3" cols="40"><?php echo my_previewreverse($profile['signature']);?></textarea>
	  </td>
</tr> */ ?>

<tr>
	<td> </td>
	<td align="left" colspan="2">
	*<input type="checkbox" name="legal" value="yes">&nbsp;<span><b><?php echo $lang['agreeement_detailschange']; ?> </b></span>
	</td>
</tr>

<tr>
    <td align="left">&nbsp;</td>
	<td colspan="2">
	<button class="ui-button button2" type="submit"><span><span><?php echo $lang['dochange'];?></span></span></button>
	</td>
</tr>

</table>

</form>


<?php echo write_subheader($lang['accountmanage_important']); ?>

<table border="0" cellspacing="2" cellpadding="4">

<?php if((int)$MW->getConfig->generic->change_pass) { ?>
<form method="post" action="<?=url_for("account","manage", array("action" => "changepass")); ?>">
<tr>
	<td align="right" valign="top" width="180"><span><b>Passwort ändern</b></span></td>
	<td align="left">
			<input type="password" size="25" name="new_pass">
	</td>
	<td>
			<button class="ui-button button2" type="submit"><span><span>Passwort ändern</span></span></button>
	</td>
</tr>
</form>
<?php } ?>

<?php if((int)$MW->getConfig->generic->change_email) { ?>
<form method="post" action="<?=url_for("account","manage", array("action" => "changeemail")); ?>">
<tr>
	<td align="right" valign="top"><span><b>E-Mail ändern</b></span></td>
	<td align="left">
		<input type="text" name="new_email" size="25" value="<?php echo $profile['email'];?>">
	</td>
	<td>
		<button class="ui-button button2" type="submit"><span><span><?php echo $lang['change_email_button'] ?></span></span></button>
	</td>
</tr>
</form>
<?php } else { ?>
<tr>
	<td align="right" valign="top">
		<span><b>
		<?php echo $lang['email'];?>
		</b></span>
	</td>
	<td align="left" colspan="2">
		<?php echo $profile['email'];?>
	</td>
</tr>
<?php } ?>

<!--Secret QUESTION-->
<tr>
	<td align="right"> </td>
	<td align="left" colspan="2">
	<?php
		if ($profile['secretq1'] == '0'){
			echo '<span style="color: red">'.$lang['not_have_secretq'].'</span>';
		}
		else{
			echo '<span style="color: green">'.$lang['have_secretq'].'</span>';
		}
	?>
	</td>
</tr>
	
<form method="post" action="<?=url_for("account","manage", array("action" => "changesecretq")); ?>">

<tr>
	<td align="right" class="label">
		<?php echo $lang['secretq'];?> 1
		<span class="note" onmouseover="Tooltip.show(this, '<?php echo $lang['secretq_info']; ?>: <ul><li><?php echo $lang['secretq_info_mincharacters']; ?>.<li></li><?php echo $lang['secretq_info_nosymbols']; ?>.<li></li><?php echo $lang['secretq_info_bothfields']; ?>.</li></ul>');"></span>
	</td>
	<td align="left" colspan="2">
		<select name="secretq1">
			<option <?php if($profile['secretq1'] == 0)echo "selected"; ?> value="0">None</option>
		<?php
			foreach ($MW->getConfig->secret_questions->question as $question){
				?>
			<option value="<?php echo htmlspecialchars($question); ?>" <?php if ($profile['secretq1'] == htmlspecialchars($question)){ echo "selected"; } ?>><?php echo $question; ?></option>
				<?php
			}
		?>
		</select>
		<input type="name" name="secreta1" style="margin:1px;">
	</td>
</tr>

<tr>
	<td align="right" class="label">
		<?php echo $lang['secretq'];?> 2
		<span class="note" onmouseover="Tooltip.show(this, '<?php echo $lang['secretq_info']; ?>: <ul><li><?php echo $lang['secretq_info_mincharacters']; ?>.<li></li><?php echo $lang['secretq_info_nosymbols']; ?>.<li></li><?php echo $lang['secretq_info_bothfields']; ?>.</li></ul>');"></span>
	</td>
	<td align="left" colspan="2">
		<select name="secretq2">
			<option <?php if($profile['secretq2'] == 0)echo "selected"; ?> value="0">None</option>
			<?php
				foreach ($MW->getConfig->secret_questions->question as $question){
				?>
			<option value="<?php echo htmlspecialchars($question); ?>" <?php if ($profile['secretq2'] == htmlspecialchars($question)){ echo "selected"; } ?>><?php echo $question; ?></option>
				<?php
			}
			?>
		</select>
		<input type="name" name="secreta2" style="margin:1px;">
	</td>
</tr>

<tr>
	<td></td>
	<td align="left" colspan="2">
		<button class="ui-button button2" type="submit"><span><span>Fragen speichern</span></span></button>
	</td>
</tr>
</form>
<!--Secret QUESTION END-->

<!-- Gameplay option start -->
<tr>
	<td>
		<form method="POST" action="<?=url_for("account","manage", array("action" => "change_gameplay"))?>">
			<input type="hidden" name="switch_wow_type" value="wotlk" />
			<button class="ui-button button2" type="submit"><span><span>WotLK</span></span></button>
	    </form>
	</td>
	<td colspan="2">
    	<b><?php echo $lang['make_acct_wotlk'];?></b>
	</td>
</tr>
<tr>
	<td>
		<form method="POST" action="<?=url_for("account","manage", array("action" => "change_gameplay"))?>">
			<input type="hidden" name="switch_wow_type" value="tbc" />
			<button class="ui-button button2" type="submit"><span><span>TBC</span></span></button>
		</form>
	</td>
	<td colspan="2">
		<b><?php echo $lang['make_acct_tbc'];?></b>
	</td>
</tr>
<tr>
	<td>
		<form method="POST" action="<?=url_for("account","manage", array("action" => "change_gameplay"))?>">
			<input type="hidden" name="switch_wow_type" value="classic" />
			<button class="ui-button button2" type="submit"><span><span>Classic</span></span></button>
		</form>
	</td>
	<td colspan="2">
		<b><?php echo $lang['make_acct_classic'];?></b>
	</td>
</tr> 
<!-- Gameplay option STOP -->

</table>

				</div> <!-- /sub-services-section -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<? echo $account_sidebar; ?>
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>


