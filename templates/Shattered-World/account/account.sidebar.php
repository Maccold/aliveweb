	<div class="sub-services">					
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Account Dienste</span>
			</div>
			<ul>
				<li><a href="<?=url_for("account","manage")?>" class="account-security" ><span>Account verwalten</span></a></li>
				<li><a href="<?=url_for("account","connect")?>" class="account-recovery" ><span>Forum Account verbinden</span></a></li>
			</ul>					
		</div>
		
		<div class="sub-services-section">
			<div class="sub-title">
				<span>Charakter Dienste</span>
			</div>
			<ul>
				<li><a href="<?=url_for("account","chartools")?>" class="character-name-change"><span>Charakterumbenennung</span></a></li>
				<li><a href="<?=url_for("account","chartools")?>" class="character-race-change"><span>Volkswechsel</span></a></li>
				<li><a href="<?=url_for("account","chartools")?>" class="character-faction-change"><span>Fraktionswechsel</span></a></li>
				<li><a href="<?=url_for("account","chartools")?>" class="character-customization"><span>Rundum-Charakteranpassung</span></a></li>
				<!--<li><a href="" class="character-ptr" ><span>Charakter auf PTR kopieren</span></a></li>-->
			</ul>					
		</div>

		<div class="sub-services-section">
			<div class="sub-title">
				<span>ANDERE DIENSTE UND AKTIONEN</span>
			</div>
			<ul>
        <? if($MW->getConfig->werbe_spieler_aktion > 0){ ?>
				<li><a href="<?=url_for("server","fwfv")?>" class="etc-raf" ><span>Werbt einen Freund</span></a></li>
		<? } ?>
				<li><a href="<?=url_for("server","ta")?>" class="character-free-move"><span>Transferier einen Charakter</span></a></li>
				<li><a href="<?=url_for("community","vote")?>" class="etc-sor"><span>Vote um den Tag zu retten</span></a></li>
				<li><a href="<?=url_for("community","item")?>" class="get-wow-buy"><span>Vote Shop</span></a></li>
			</ul>		
        </div>
	</div>