<?php
if(INCLUDED!==true)
	exit; 
?>
<style type="text/css">
div.errorMsg {
	width: 60%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;
	color: #700;
	margin: 10px 20px;
	padding: 0px 20px;
}
.services-content p{ text-align: left; margin: 0px 20px; }
	ul.errorMsg { line-height: 20px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090; padding-top: 5px; padding-bottom:5px;}
	div.noErrorMsg, p.noErrorMsg { width: 80%; line-height: 30px; font-size: 10pt; border: 2px solid #00ff24; background: #afffa9;}
	div.errorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
	td.serverStatus1 { font-weight: bold; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
	td.serverStatus2 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
	td.serverStatus3 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
	td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
</style>

<div class="top-banner">
	<div class="section-title">
		<span>Charakter Tools</span>
		<p>Hier kannst du deinen Charakter bearbeiten.</p>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">


<?php write_subheader($lang['vote_acct']); ?>

<div class="table">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<th class="" align="center" nowrap="nowrap"><?php echo $lang['vote_curacct']; ?></th>
	<th class="" align="center" nowrap="nowrap"><?php echo $lang['vote_curchar']; ?></th> 
	<th class="" align="center" nowrap="nowrap"><?php echo $lang['vote_points']; ?></th> 
</tr>
	
<tr class="row1">
	<td align="center" nowrap="nowrap"><?php echo $user['username']; ?></td>
	<td align="center" nowrap="nowrap"><?php echo $user['character_name']; ?></td>
	<td align="left" nowrap="nowrap">
		<?php echo $lang['vote_balance'] ?> <?php echo $_SESSION["points"]; ?><br />
		<?php echo $lang['vote_apt'] ?> <?php echo $_SESSION["date_points"]; ?> </td>
</tr>
</table>          
</div>

<b><center><?php echo $lang['vote_keep'] ?></center></b>
<ul>
	<li><?php echo $lang['chartool_desc1'] ?></li>
	<li><?php echo $lang['chartool_desc2'] ?></li>
</ul>

<br />

<!-- CHARACTER RENAME -->
<?php write_subheader("Charakter umbenennen"); ?>

		<? if(isset($renameSuccess) && $renameSuccess){ ?>
			<p class="noErrorMsg">Du kannst nun beim n&auml;chsten Login den Namen deines Charakters &auml;ndern.</p>
		<? } ?>
		<? if(isset($renameErrors) && count($renameErrors) > 0){ ?>
			<ul class="errorMsg">
			<? foreach($renameErrors as $error) { ?>
				<li><?=$error?></li>
			<? } ?>
			</ul>
		<? } ?>
		<? if(show_rename == true) { ?>
			<form action="<?=url_for("account","chartools")?>" method="post">
			<p align="left"><?php echo $lang['customize_desc1']; ?></p>
			<p><?php echo $lang['charname']; ?> <select name="name">
			<?php
			foreach($characters as $char){
				?><option value="<?=$char['guid']?>"><?=$char['name']?></option><?
			}
			?>
			</select>
			<br />
			Preis: <u class="price"><?=$char_rename_points?></u> AliveCash<br><br />
			<?php if ($your_points >= $char_rename_points){ ?>
				<input type="submit" name="rename" value="Best&auml;tigen">
			<?php } else{ ?> 
				<font color="red"><center>Du hast nicht genug Alive-Cash um dir einen Rename zu kaufen.</center></font><br />
			<?php } ?>
			</p>
		</form>
		<?} else{ ?>
			<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
		<? } ?>
<br />

<!-- CHARACTER RECUSOMTIZATION -->
<?php write_subheader("Charakter Anpassung"); ?>

		<? if(isset($customSuccess) && $customSuccess){ ?>
			<p class="noErrorMsg">Du kannst nun beim n&auml;chsten Login deinen Charakter neu gestalten.</p>
		<? } ?>
		<? if(isset($customErrors) && count($customErrors) > 0){ ?>
			<ul class="errorMsg">
			<? foreach($customErrors as $error) { ?>
				<li><?=$error?></li>
			<? } ?>
			</ul>
		<? } ?>
		<? if($show_custom == true) { ?>
			<form action="<?=url_for("account","chartools")?>" method="post">
			<p align="left"><?php echo $lang['customize_desc2']; ?></p>
			<p><?php echo $lang['charname']; ?> <select name="char_c_name">
			<?php
			foreach($characters as $char){
				?><option value="<?=$char['guid']?>"><?=$char['name']?></option><?
			}
			?>
			</select>
			<br />
			Preis: <u class="price"><?=$char_custom_points?></u> AliveCash<br><br />
			<?php if ($your_points >= $char_custom_points){ ?>
				<input type="submit" name="customize" value="Best&auml;tigen">
			<?php } else{ ?> 
				<font color="red"><center>Du hast nicht genug Alive-Cash um dir eine Charakter Anpassung zu kaufen.</center></font><br />
			<?php } ?>
			</p>
		</form>
		<?} else{ ?>
			<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
		<? } ?>
<br />

<!-- CHARACTER FACTION CHANGER -->
<?php write_subheader("Fraktion ändern"); ?>
		
		<? if(isset($factionChangeSuccess) && $factionChangeSuccess){ ?>
			<p class="noErrorMsg">Du kannst nun beim n&auml;chsten Login die Fraktion deines Charakters &auml;ndern.</p>
		<? } ?>
		<? if(isset($factionChangeErrors) && count($factionChangeErrors) > 0){ ?>
			<ul class="errorMsg">
			<? foreach($factionChangeErrors as $error) { ?>
				<li><?=$error?></li>
			<? } ?>
			</ul>
		<? } ?>
		
		<? if($allow_faction_change == true) { ?>
		
		<p style="color: red; font-size: 20px; font-weight: bold;">Beachte:</p>
		<ul style="list-style: disc; margin: 0px 0px 10px 60px; width: 500px; color: white;">
			<li>Vorher alle Quests beenden.</li>
			<li>Vorher alle Briefe aus der Post nehmen.</li>
			<li>Wenn der Fraktionswechsel abgebrochen wird, gehen alle fraktionsspezifischen Mounts/Ruf/Items/.. verloren.</li>
			<li>Gleiches gilt wenn man einen Fraktionswechsel beantragt aber nur eine andere Rasse der gleichen Fraktion auswählt.</li>
			<li>Verluste dieser Art werden nicht erstattet.</li>
		</ul>
		
		<form action="<?=url_for("account","chartools")?>" method="post">
			<p>Hier kannst du deine Fraktion deines Charakters veränern. Wähle deinen Charakter den du bearbeiten möchtest aus und klick unten auf den Button .</p>
			<p><b>Wichtig:</b> Wenn du einen Fraktionstransfer kaufst sei dir da wirklich sicher, einen Fraktionswechsel abschließen ohne die Fraktion geändert zu haben kann zu Fehlern führen.</p>
			<p>Bedenke dass es manche Mounts/Items gibt die kein Gegenstück auf der neuen Fraktion haben. Sollte es Probleme geben melde dich bitte sofort bei einem GM3. Ausnutzen von Fehlern des Wechsels (Heldentum + Kampfrausch auf einem Charakter etc) kann zu einem Bann führen, wenn dir so etwas auffällt melde dich umgehend bei einem GM.</p>
			<p><?php echo $lang['charname']; ?> <select name="char_f_name">
			<?php
			foreach($characters as $char){
				?><option value="<?=$char['guid']?>"><?=$char['name']?></option><?
			}
			?>
			</select>
			<br />
			Preis: <u class="price"><?=$char_faction_points?></u> AliveCash<br><br />
			<?php if ($your_points >= $char_faction_points){ ?>
				<input type="submit" name="faction_change" value="Best&auml;tigen">
			<?php } else{ ?> 
				<font color="red"><center>Du hast nicht genug Alive-Cash um dir einen Fraktionswechsel zu kaufen.</center></font><br />
			<?php } ?>
			</p>
		</form>
		<? } else{ ?>
			<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
		<? } ?>
<br />

<!-- CHARACTER Race CHANGER -->
<?php write_subheader("Rasse ändern"); ?>

		<? if(isset($raceChangeSuccess) && $raceChangeSuccess){ ?>
			<p class="noErrorMsg">Du kannst nun beim n&auml;chsten Login die Rasse deines Charakters &auml;ndern.</p>
		<? } ?>
		<? if(isset($raceChangeErrors) && count($raceChangeErrors) > 0){ ?>
			<ul class="errorMsg">
			<? foreach($raceChangeErrors as $error) { ?>
				<li><?=$error?></li>
			<? } ?>
			</ul>
		<? } ?>
		<? if($show_changer == true) { ?>
			<form action="<?=url_for("account","chartools")?>" method="post">
			Hier kannst du deine Rasse deines Charakters bearbeiten. Wähle deinen Charakter den du bearbeiten möchtest aus und klick unten auf den Button .<br/>
			<br />
			<p><?php echo $lang['charname']; ?> <select name="char_r_name">
			<?php
			foreach($characters as $char){
				?><option value="<?=$char['guid']?>"><?=$char['name']?></option><?
			}
			?>
			</select>
			<br />
			Preis: <u class="price"><?=$char_race_points?></u> AliveCash<br><br />
			<?php if ($your_points >= $char_race_points){ ?>
				<input type="submit" name="race_change" value="Best&auml;tigen">
			<?php } else{ ?> 
				<font color="red"><center>Du hast nicht genug Alive-Cash um die Rasse deines Charakters ändern zu können.</center></font><br />
			<?php } ?>
			</p>
		</form>
		<?} else{ ?>
			<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
		<? } ?>

				</div> <!-- /sub-services-section -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<? echo $account_sidebar; ?>
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>

