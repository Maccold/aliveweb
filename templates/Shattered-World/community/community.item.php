<? /* Shattered-World/community/item */ ?>
<br>
<style>
	div.noErrorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #00ff24; background: #afffa9;}
	div.errorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
	td.serverStatus1 { font-weight: bold; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
	td.serverStatus2 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
	td.serverStatus3 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
	td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
	
	#content .services-content .sub-title { margin: 2px 0px; text-align:center; }
</style>
<script type="text/javascript" src="http://portal.wow-alive.de/wowhead/power.js"></script>

<? if ($showvote == false){ ?>
<div class="talkback-code">
	<div class="talkback-code-interior">
		<div class="talkback-icon">
			<h4 class="code-header">WoW Alive Meldung</h4>
			<div align="left">
				<!-- main error message -->
				<div style="margin: 10px"><?php echo $lang['chat_disable'] ?></div>
				<!-- / main error message -->
			</div>
		</div> <!-- /talkback-icon -->
	</div> <!-- /talkback-code-interior -->
</div>
<? } else if(!isset($_SESSION["char_name"])) { ?>
<div class="talkback-code">
	<div class="talkback-code-interior">
		<div class="talkback-icon">
			<h4 class="code-header">WoW Alive Meldung</h4>
			<div align="left">
				<!-- main error message -->
				<div style="margin: 10px">Du musst zuerst einen Charakter wählen bevor du im Vote-Shop etwas kaufen kannst!</div>
				<!-- / main error message -->
			</div>
		</div> <!-- /talkback-icon -->
	</div> <!-- /talkback-code-interior -->
</div>
<? } else { ?>

<div class="top-banner">
	<div class="section-title">
		<span><?=$lang['item']?></span>
		<p>Hier kannst du deine Votepunkte gegen Items eintauschen.</p>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">
				
<span class="clear"><!-- --></span>	
<? write_subheader($lang['vote_acct']); ?>

<div class="table">
    <table width="100%" cellpadding="3" cellspacing="0">
		<tr>
            <th align="center" nowrap="nowrap"><?php echo $lang['vote_curacct']; ?></th>
            <th align="center" nowrap="nowrap"><?php echo $lang['vote_curchar']; ?></th> 
            <th align="center" nowrap="nowrap"><?php echo $lang['vote_points']; ?></th> 
        </tr>

        <tr class="row1">
            <td align="center" nowrap="nowrap"><?php echo $user['username']; ?></td>
            <td align="center" nowrap="nowrap"><?php echo $user['character_name']; ?></td>
            <td align="left" nowrap="nowrap">
				<?php echo $lang['vote_balance'] ?> <?php echo $_SESSION["points"]; ?><br />
				<?php echo $lang['vote_apt'] ?> <?php echo $_SESSION["date_points"]; ?> 
			</td>
        </tr>
          
        <tr class="row2">
            <td colspan="3" align="left">
				<br>
				<b><center><?php echo $lang['vote_keep'] ?></center></b>
                <ul>
					<li><?php echo $lang['vote_change'] ?></li>
					<li><?php echo $lang['reward_instant'] ?><br><br></li>
					<li><span style="color:red;"><?php echo $lang['vote_reward'] ?></span></li>
					<li><span style="color: red; font-weight: bold;"><?php echo $lang['vote_hack'] ?></span><br><br></li>
				</ul>
            </td>
        </tr>
    </table>
</div>

<br />
<? write_subheader($lang["vote_curchar"].": ".$_SESSION["char_name"]."<br />")?>
<? if(!empty($reward_message)) echo "<p>$reward_message</p>"; ?>
<div class="table">
<?php
	echo '<table border="1" cellspacing="1" width="550" cellpadding="3" align="center">';
	show_rewards1();
	echo '<span class="clear"><!-- --></span>';
	show_rewards2();
	echo '<span class="clear"><!-- --></span>';
	show_rewards3();
	echo '<span class="clear"><!-- --></span>';
	show_rewards4();
	echo '<span class="clear"><!-- --></span>';
	show_rewards5();
	echo '<span class="clear"><!-- --></span>';
	show_rewards6();
	echo '<span class="clear"><!-- --></span>';
	show_rewards7();
	echo '<span class="clear"><!-- --></span>';
	show_rewards8();
	echo '</table>';
?>
</div>
<br />


				</div> <!-- /sub-services-section -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<? echo $account_sidebar; ?>
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>

<?php
}
?>
