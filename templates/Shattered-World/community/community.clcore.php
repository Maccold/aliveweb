<?php
if(INCLUDED!==true)
	exit; 
?>

<div class="top-banner">
	<div class="section-title">
		<span><?php echo $lang['clcore'];?></span>
		<p>Hier findest du die aktuellen behobenen Bugs sch&ouml;n aufgelistet.</p>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">

	<p>Die komplette Liste der Core Arbeiten. Die <b>Aktuelle Alive Core Revision</b> zeigt dir an ob schon alle &Auml;nderungen aktiv sind, oder mit dem n&auml;chsten Update (Serverneustart) kommen.</p>
					
	<br />
	<p>
		<b>Aktuelle Alive Core Revision <?=$currentServerRevision?></b><br/>
	</p>
	<br />
	<br />
	
	<?php write_subheader("Alive Server Änderungen");?>
	
	<div class="table">
	<table>
	    <tr>
			<th>Alive-Rev</th>
			<th>&Auml;nderungen</th>
			<th>Datum</th>
			<th>Author</th>
		</tr>
	<? foreach($serverRevisions as $basis){ ?>
		<tr class="<?=$basis["class"]?>">
			<td><?=$basis['Rev']?></td>
			<td><?=$basis['Message']?></td>
			<td nowrap="nowrap"><?=$basis['Date']?></td>
			<td nowrap="nowrap"><?=$basis['Author']?></td>
		</tr>
	<? } ?>
	</table>
	</div>
	<span class="clear"><!-- --></span>	


				</div> <!-- /services-content -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<? echo $server_sidebar; ?>
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>