<?php
if(INCLUDED!==true)
	exit; 
?>

<div class="top-banner">
	<div class="section-title">
		<span><?php echo $lang['changelog'];?></span>
		<p>Hier findest du die aktuellen behobenen Bugs sch&ouml;n aufgelistet.</p>
	</div>
	<span class="clear"><!-- --></span>		
</div>

<div class="bg-body">
	<div class="body-wrapper">
		<div class="contents-wrapper">
			<div class="left-col">
				<div class="services-content">


	<p>Nicht jeder Bug ist es wert gross publiziert zu werden, aber f&uuml;r Spieler die Interesse an Serverseitig behoben Bugs und oder einfach auch den Kleinkram interessieren k&ouml;nnen hier die letzten Arbeiten der Teams nachlesen ;) Die <b>Aktuelle Alive Core/Page Revision</b> zeigt dir an ob schon alle &Auml;nderungen aktiv sind, oder mit dem n&auml;chsten Update (Serverneustart) kommen.</p>
	
	<br />
	<p>
		<b>Aktuelle Alive Core Revision <?=$currentServerRevision?></b><br/>
		<b>Aktuelle Alive Portal Revision <?=$currentPageRevision?></b>
	</p>
	<br />
	<br />
	
	<?php write_subheader("Alive Server Änderungen");?>
	
	<div class="table">
	<table>
	    <tr>
			<th>Alive-Rev</th>
			<th>&Auml;nderungen</th>
			<th>Datum</th>
			<th>Author</th>
		</tr>
	<? foreach($serverRevisions as $basis){ ?>
		<tr class="<?=$basis["class"]?>">
			<td><?=$basis['Rev']?></td>
			<td><?=$basis['Message']?></td>
			<td nowrap="nowrap"><?=$basis['Date']?></td>
			<td nowrap="nowrap"><?=$basis['Author']?></td>
		</tr>
	<? } ?>
	</table>
	</div>
	<span class="clear"><!-- --></span>	
	<br />
	<br />
	<p><a href="<?=url_for("community", "clcore")?>">Zeig mir alle Änderungen am Server</a></p>
	
	<span class="clear"><!-- --></span>	
	<br />
	<br />
	<?php write_subheader("Alive Page Änderungen");?>
	
	<div class="table">
	<table>
		<tr>
			<th>Alive-Rev</th>
			<th>&Auml;nderungen</th>
			<th>Datum</th>
			<th>Author</th>
		</tr>
	<? foreach($pageRevisions as $basis){ ?>
		<tr class="<?=$basis["class"]?>">
			<td><?=$basis['Rev']?></td>
			<td><?=$basis['Message']?></td>
			<td nowrap="nowrap"><?=$basis['Date']?></td>
			<td nowrap="nowrap"><?=$basis['Author']?></td>
		</tr>
	<? } ?>
	</table>
	</div>
	<span class="clear"><!-- --></span>	
	<br />
	<br />
	
	<p><a href="<?=url_for("community", "clpage")?>">Zeig mir alle Änderungen an der Page</a></p>
	<span class="clear"><!-- --></span>	
	
				</div> <!-- /services-content -->
				<span class="clear"><!-- --></span>
			</div>
			<div class="right-col">
				<? echo $server_sidebar; ?>
				<span class="clear"><!-- --></span>
			</div>
			<span class="clear"><!-- --></span>
		</div>
		<span class="clear"><!-- --></span>
	</div>
</div>
<span class="clear"><!-- --></span>