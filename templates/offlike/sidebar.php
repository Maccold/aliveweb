<?php 

if(!defined("INCLUDED"))
	die();


if (!empty($quicklinks)):
	?>
	<!-- QuickLinks -->
	<div id="q-links">
		<h3><?php echo $lang['quicklinks'];?></h3>
		<ul>
		<? 
		foreach ($quicklinks as $key => $array) { 
			$row_class = cycle($row_class, array("a", "e"));
			?>
			<li class="<?=$row_class?>"><a href="<?=$array[1]?>"><?=$array[0]?></a></li>  
			<?
		} ?>
		</ul></div>
		<span class="gfxhook"></span>
<?php endif; ?>
 
<div id="online"> 
	<a href="<?=url_for("server","playersonline")?>">
		<iframe src="http://portal.wow-alive.de/tools/online.php" frameborder="0" border="0" scrolling="no" scroll="no" noresize width="312px"></iframe>
	</a> 
</div><br>

<!-- VoteLinks  --> 
<?php if(count($MW->getConfig->votelinks->vote) > 0): ?>

<div id="votebox">      
  <a href="<?=url_for("community","vote")?>"><img src="http://portal.wow-alive.de/templates/WoW-Alive_WOTLK/images/pixel000.gif" width="305" height="104" /></a>
</div>
<hr> 

<?php endif; ?>


<?php if ((int)$MW->getConfig->components->right_section->media): ?>
<div id="rightbox">
	<h3 style="height: 20px; color: #eff0ef; font-size: 12px; letter-spacing: 1px; font-weight: bold; width: 308px; padding: 1px 0 0 8px; font-family: 'Trebuchet MS', Verdana, Arial, sans-serif;"><?php echo $lang['random_screen']; ?></h3>
	<div id="innerrightbox">
	<?php if ($screen_otd): ?>
		<a href="images/screenshots/<?php echo $screen_otd; ?>" target="_blank"><img src="show_picture.php?filename=<?php echo $screen_otd; ?>&amp;gallery=screen&amp;width=282" width="282" alt="" style="border: 1px solid #333333"/></a>
		<select onchange="window.location = options[this.selectedIndex].value" style="width: 284px;">
			<option value=""><?php echo $lang['galleries']; ?> -&gt;</option>
			<option value="index.php?n=media&sub=screen"><?php echo $lang['GallScreen']; ?></option>
			<option value="index.php?n=media&sub=wallp"><?php echo $lang['GallWalp']; ?></option>
		</select>
	<?php else: ?>
		No Screenshots in database;
	<?php endif; 
	unset($screen_otd); // Free up memory. ?>
	</div>
</div>
<?php endif; ?>

<?php if ((int)$MW->getConfig->components->right_section->newbguide): ?>
<!-- Newcomers section -->
<div id="rightbox">
	<div class="newcommer">
		<h4><?php echo $lang['newcomers']; ?></h4>
		<p style="margin-bottom: -1px;">
			<?php echo $lang['newcomers2']; ?>
		</p>
		<ul>
			<li>&nbsp;&nbsp;<a href="<?=url_for("server","howtoplay")?>"><?php echo $lang['byj_1']; ?></a></li>
			<li>&nbsp;&nbsp;<a href="<?=url_for('account', 'register')?>"><?php echo $lang['byj_2']; ?></a></li>
			<li>&nbsp;&nbsp;<a href="<?=url_for('server', 'ta')?>"><?php echo $lang['ta']; ?></a></li>
		</ul>
	</div>
</div>
<?php endif; ?>


<?php if($MW->getConfig->werbe_spieler_aktion > 0): ?>
<!-- Werbe Freund Section -->
<div id="friendbox">       
  <a href="<?=url_for("server","fwfv")?>"><img src="http://portal.wow-alive.de/templates/WoW-Alive_BC/images/pixel000.gif" width="305" height="145" /></a>
</div>
<hr> 
<?php endif; ?>

<?php if (isset($usersonhomepage) || isset($hits)): ?>
	<!-- Server Information -->
	<?php if (count($servers) > 0): ?>
	<?php foreach($servers as $server): ?>
	<div id="box3">
		<h3><?php echo $lang['serverinfo'];?></h3>
		<ul>
		<li><div>&nbsp;</div></li>
		<li><div><?php echo $lang['si_name']; ?>:&nbsp;<b><?php echo $server['name'];?></b></div></li>
	<?php if (isset($server['realm_status'])): ?>
		<li><div><?php echo $lang['si_status']; ?>:&nbsp;
		<?php if ($server['realm_status']): ?>
			<img src="images/uparrow2.gif" height="12" alt="Online" /> <b style="color: rgb(35, 67, 3);">Online</b>
		<?php else: ?>
			<img src="images/downarrow2.gif" height="12" alt="Offline" /> <b style="color: rgb(102, 13, 2);">Offline</b>
		<?php endif; ?></div></li>
	<?php endif; 
	
	if (isset($server['onlineurl'])): ?>
		<li><div>
			<?php echo $lang['si_on']; ?>:&nbsp;
			<a href="<?php echo $server['onlineurl'] ?>"><?php echo $server['playersonline']; ?></a>
			<?php if (isset($server['playermapurl'])): ?>
				(<a href="<?=url_for("server", "playermap")?>"><?php echo $lang['playermap'] ?></a>)
			<?php endif; ?>
		</div></li>
	<?php endif; ?> 
	<? if (isset($server['server_ip'])): ?>
		<li><div><?php echo $lang['si_ip']; ?>:&nbsp;<b><?php echo $server['server_ip']; ?><?php echo ":3704" ?></b></div></li>

	<?php endif;  ?> 

	<? if (isset($server['type'])): ?>
		<li><div><?php echo $lang['si_type']; ?>:&nbsp;<b><?php echo $server['type'];?></b></div></li>
	<?php endif;  ?> 

	<? if (isset($server['language'])): ?>
		<li><div><?php echo $lang['si_lang']; ?>:&nbsp;<b><?php echo $server['language']; ?></b></div></li>
	<?php endif;  ?> 

	<? if (isset($server['population'])): ?>
		<li><div><?php echo $lang['si_pop']; ?>:&nbsp;<b><?php echo population_view($server['population']);?></b></div></li>
	<?php endif;  ?> 

	<? if (isset($server['accounts'])): ?>
		<li><div><?php echo $lang['si_acc']; ?>:&nbsp;<b><?php echo $server['accounts']; ?><?php if (isset($server['active_accounts'])): ?> <?php echo sprintf($lang['si_active_acc'], $server['active_accounts']); ?><?php endif; ?></b></div></li>
	<?php endif;  ?> 

	<? if (isset($server['characters'])): ?>
		<li><div><?php echo $lang['si_chars']; ?>:&nbsp;<b><?php echo $server['characters'];?></b></div></li>
	<?php endif;  ?> 

	<? if (isset($server['rates'])): ?>
		<li><div>
			<dl>
<dt><?php echo "Rates"; ?>:&nbsp;</dt>
<dd><?php echo "Startlevel"; ?>                           = <?php echo $server['rates']['StartPlayerLevel'];?> </dd>
<dd><?php echo "Items"; ?>                                = <?php echo $server['rates']['Rate.Drop.Item.Artifact'];?> x Blizzlike</dd>
<dd><?php echo $lang['si_droprate_money']; ?>             = <?php echo $server['rates']['Rate.Drop.Money'];?> x Blizzlike</dd>
<dd><?php echo $lang['si_exprate_kill']; ?> Level  0 - 69  = <?php echo $server['rates']['Rate.XP.Kill'];?> x Blizzlike</dd>
<dd><?php echo $lang['si_exprate_kill']; ?> Level 70 - 80 = <?php echo $server['rates']['Rate.XP.PastLevel70'];?> x Blizzlike</dd>  
<dd><?php echo $lang['si_exprate_quest']; ?>              = <?php echo $server['rates']['Rate.XP.Quest'];?> x Blizzlike</dd>
<dd><?php echo "Ruf"; ?>                                  = <?php echo $server['rates']['Rate.XP.Explore'];?> x Blizzlike</dd>
			</dl>
		</div></li>
	<?php endif;  ?> 
	
	<? if ($server['moreinfo']): ?>
		<li><div><a href="<?php echo url_for('server', 'info'); ?>"><?php echo $lang['more_info']; ?></a></div></li>
	<?php endif; ?>

		<li><div>&nbsp;</div></li>
	</ul></div>
	<span class="gfxhook"></span>
	<hr>
	<?php break; 
	endforeach; 
	
	endif; // END if (count($servers) > 0)
	?>   
       
	<!-- User Online -->
	<div id="box2">
		<h3><?php echo $lang['useronhp'];?></h3>
		<ul>
			<li>&nbsp;</li>
			<?php if (isset($usersonhomepage)): ?>
			<li>
				<a href="<?=url_for("whoisonline")?>">&nbsp;<?php echo $usersonhomepage;?>&nbsp;</a>
				<?php echo ($usersonhomepage == 1) ? $lang['isonline'] : $lang['areonline']; ?>
			</li>
			<?php endif; ?>
	
			<li>&nbsp;</li>
			<?php if (isset($hits)): ?>
			<li><p  style="padding-left:19px; margin-top:-8px"><?php echo $lang['hits']; ?>: <?php echo $hits; ?></p></li>
			<?php endif; ?>
		</ul>
	</div>
	<span class="gfxhook"></span>
	<hr>
<?php endif; ?>

<div id="box4">
	<h3><a href="<? echo $forum_link; ?>" style="text-decoration: none">&nbsp;<?php echo Forendiskussion;?>&nbsp;</a></h3>
	&nbsp;
	<iframe src="http://portal.wow-alive.de/tools/rss2html/rss2html.php" frameborder="0" border="0" scrolling="no" scroll="no" noresize height="1200px" width="295px"></iframe>
</div>