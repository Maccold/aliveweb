<br />

<style>
div.errorMsg { width: 60%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
</style>
<style type="text/css">
  div.noErrorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #00ff24; background: #afffa9;}
  div.errorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
  td.serverStatus1 { font-weight: bold; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus2 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus3 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
  td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
</style>

<table align="center" width="100%" style="font-size:0.8em;"><tr><td align="left">

<div id="container-community">
<div class="phatlootbox-top">
<h2 class="community">
<span class="hide">Registration</span>
</h2>
<span class="phatlootbox-visual comm"></span>
</div>
<div class="phatlootbox-wrapper">
<div style="background: url(<?php echo $currtmp; ?>/images/phatlootbox-top-parchment.jpg) repeat-y top right; height: 7px; width: 456px; margin-left: 6px; font-size: 1px;"></div>
<div class="community-cnt">
      <form method="post" action="index.php?n=account&amp;sub=reg">
        <div style="margin:4px;padding:6px 9px 6px 9px;text-align:left;">
        <h2 style="margin:2px;"> <?php echo $lang['rules_agreement'] ?> </h2>
        <div style="color: red"><?php echo $lang['warn_email'] ?></div>
        <br/>
        <?php echo lang_resource('acc_create_rules.html'); ?>
        </div>
        <div style="margin:4px;padding:6px 9px 0px 9px;text-align:left;">
        <input type="button" class="button" value="<?php echo $lang['disagree']; ?>" onclick="location.href='index.php'"/> &nbsp;&nbsp;
        <input type="submit" class="button" value="<?php echo $lang['agree']; ?>"/>
</div></div>

<div class="phatlootbox-bottom"></div>
      </form>
    </tr>
</table>
<br>
<br />

