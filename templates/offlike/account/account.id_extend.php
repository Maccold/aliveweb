<Br />
<?php builddiv_start(1, "Raid ID Verlängerung") ?>
<style>
div.errorMsg { width: 60%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
</style>
<style type="text/css">
  div.noErrorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #00ff24; background: #afffa9;}
  div.errorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
  td.serverStatus1 { font-weight: bold; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus2 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus3 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
  td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
</style>
<!-- CHARACTER RAID ID EXTEND -->
<?php echo "Hier kannst du die Raid IDs von deinem Charakter verlängern so dass diese am Mittwoch nach dem allgemeinen ID Reset wiederhergestellt werden.<br>
			Diese Verlängerung kostet dich ".$extend_points." AliveCash.<br><br>"; ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>

    <td width="12"><img src="templates/WotLK/images/metalborder-top-left.gif" alt="" width="12" height="12"></td>
    <td style="background: transparent url(templates/WotLK/images/metalborder-top.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td width="12"><img src="templates/WotLK/images/metalborder-top-right.gif" alt="" width="12" height="12"></td>
</tr>
<tr>
    <td style="background: transparent url(templates/WotLK/images/metalborder-left.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td>
    <table width="100%" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="rankingHeader" colspan="3" align="center" nowrap="nowrap"><?php echo $lang['vote_acct']; ?></td>
        </tr>

		<tr>
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_curacct']; ?></td>
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_curchar']; ?></td> 
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_points']; ?></td> 
        </tr>

        <tr>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $user['username']; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $user['character_name']; ?></td>
            <td class="serverStatus1" align="left" nowrap="nowrap"><?php echo $lang['vote_balance'] ?> <?php echo $_SESSION["points"]; ?><br /><?php echo $lang['vote_apt'] ?> <?php echo $_SESSION["date_points"]; ?> </td>
        </tr>
          
                <tr>
            <td colspan="3" align="left"><br><b><center><?php echo $lang['vote_keep'] ?></center></b>
                <ul>
                    
                    <li><?php echo $lang['chartool_desc1'] ?></li>
			  
              
                    <li><?php echo $lang['chartool_desc2'] ?><br><br>

                    </li>
                    </li></ul>
            </td>
        </tr>
        
    
    </table>
        </td>

        <td style="background: transparent url(templates/WotLK/images/metalborder-right.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    </tr>

    <tr>
        <td><img src="templates/WotLK/images/metalborder-bot-left.gif" alt="" width="12" height="11"></td>
        <td style="background: transparent url(templates/WotLK/images/metalborder-bot.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
        <td><img src="templates/WotLK/images/metalborder-bot-right.gif" alt="" width="12" height="11"></td>

    </tr>
    </tbody>

</table><br>

<br />

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>

    <td width="12"><img src="templates/WotLK/images/metalborder-top-left.gif" alt="" width="12" height="12"></td>
    <td style="background: transparent url(templates/WotLK/images/metalborder-top.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td width="12"><img src="templates/WotLK/images/metalborder-top-right.gif" alt="" width="12" height="12"></td>
</tr>
<tr>
    <td style="background: transparent url(templates/WotLK/images/metalborder-left.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td>
    <table width="100%" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="rankingHeader" colspan="4" align="center" nowrap="nowrap">Bestehende Raid IDs von <?php echo $user['character_name']; ?></td>
        </tr>
		
		<tr>
            <td class="rankingHeader" align="center" nowrap="nowrap">ID</td> 
            <td class="rankingHeader" align="center" nowrap="nowrap">Raid Instanz</td>
            <td class="rankingHeader" align="center" nowrap="nowrap">Schwierigkeit</td> 
            <td class="rankingHeader" align="center" nowrap="nowrap">&nbsp;</td> 
        </tr>
		<? if(count($instance_ids) == 0){ ?>
		<tr>
            <td class="serverStatus1" align="center" nowrap="nowrap" colspan="4">Dieser Charakter hat keine aktiven Raid IDs</td>
        </tr>
		<? } ?>
		<? foreach($instance_ids as $instance_id => $instance_data){ ?>
        <tr>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $instance_data['id']; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $instance_data["name"]; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $instance_data["difficulty"]; ?></td>
            <td class="serverStatus1" align="left" nowrap="nowrap"> 
			<? 
			if($instance_data["extended"] == true)
			{ 
				?>
				<strong>Verl&auml;ngert</strong>
				<? 
			}
			else
			{ 
				?>
			
				<form action="index.php?n=account&sub=id_extend" method="post">
					<input type="hidden" name="instance_id" value="<?php echo $instance_data['id']; ?>" />
					<input type='submit' name='extend' value='Verl&auml;ngern' />
				</form>
				<? 
			} 
			?>
			</td>
        </tr>
		<? } ?>
    </table>
        </td>

        <td style="background: transparent url(templates/WotLK/images/metalborder-right.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    </tr>

    <tr>
        <td><img src="templates/WotLK/images/metalborder-bot-left.gif" alt="" width="12" height="11"></td>
        <td style="background: transparent url(templates/WotLK/images/metalborder-bot.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
        <td><img src="templates/WotLK/images/metalborder-bot-right.gif" alt="" width="12" height="11"></td>

    </tr>
    </tbody>

</table><br>

<?php builddiv_end() ?>
