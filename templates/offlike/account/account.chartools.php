<Br />
<?php builddiv_start(1, "Charakter Tools") ?>
<style>
div.errorMsg { width: 60%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
</style>
<style type="text/css">
	ul.errorMsg { line-height: 20px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090; padding-top: 5px; padding-bottom:5px;}
  div.noErrorMsg, p.noErrorMsg { width: 80%; line-height: 30px; font-size: 10pt; border: 2px solid #00ff24; background: #afffa9;}
  div.errorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090;}
  td.serverStatus1 { font-weight: bold; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus2 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus3 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
  td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
</style>
<!-- CHARACTER RENAME -->
<?php echo add_pictureletter("Hier kannst du deinen Charakter bearbeiten.<br><br><br>"); ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>

    <td width="12"><img src="templates/WotLK/images/metalborder-top-left.gif" alt="" width="12" height="12"></td>
    <td style="background: transparent url(templates/WotLK/images/metalborder-top.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td width="12"><img src="templates/WotLK/images/metalborder-top-right.gif" alt="" width="12" height="12"></td>
</tr>
<tr>
    <td style="background: transparent url(templates/WotLK/images/metalborder-left.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td>
    <table width="100%" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>

            <td class="rankingHeader" colspan="3" align="center" nowrap="nowrap"><?php echo $lang['vote_acct'] ?></td>
        </tr>

		<tr>
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_curacct']; ?></td>
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_curchar']; ?></td> 
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_points']; ?></td> 
        </tr>

        <tr>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $user['username']; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $user['character_name']; ?></td>
            <td class="serverStatus1" align="left" nowrap="nowrap"><?php echo $lang['vote_balance'] ?> <?php echo $_SESSION["points"]; ?><br /><?php echo $lang['vote_apt'] ?> <?php echo $_SESSION["date_points"]; ?> </td>
        </tr>
          
                <tr>
            <td colspan="3" align="left"><br><b><center><?php echo $lang['vote_keep'] ?></center></b>
                <ul>
                    
                    <li><?php echo $lang['chartool_desc1'] ?></li>
			  
              
                    <li><?php echo $lang['chartool_desc2'] ?><br><br>

                    </li>
                    </li></ul>
            </td>
        </tr>
        
    
    </table>
        </td>

        <td style="background: transparent url(templates/WotLK/images/metalborder-right.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    </tr>

    <tr>
        <td><img src="templates/WotLK/images/metalborder-bot-left.gif" alt="" width="12" height="11"></td>
        <td style="background: transparent url(templates/WotLK/images/metalborder-bot.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
        <td><img src="templates/WotLK/images/metalborder-bot-right.gif" alt="" width="12" height="11"></td>

    </tr>
    </tbody>

</table><br>

<br />

	
<?php write_subheader("Charakter umbenennen"); ?>
<center>
<table width = "580" style = "border-width: 1px; border-style: dotted; border-color: #928058;"><tr><td><table style = "border-width: 1px; border-style: solid; border-color: black; background-image: url('<?php echo $currtmp; ?>/images/light3.jpg');"><tr><td>

<table border=0 cellspacing=0 cellpadding=4 width="580px">
<tr>
<center><br><?php echo $lang['customize_desc1']; ?> <br></center>
<td>
<form action="<?=url_for("account","chartools")?>" method="post">
<center>
<?php
if ($show_rename == false){
	?>
	<center>
		<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
	</center>
	<?php
}
elseif($char_rename_points > $your_points){
	echo "<center><font color=\"red\"><center>Du hast nicht genug Alive-Cash um dir einen Namenswechsel zu kaufen, du ben&ouml;tigst $char_rename_points AC.</center></font></center>";
}
else{
	?>
<table width="550" border="0" cellpadding="2px">
  <tr>
	<td><center><?php echo $lang['rename_desc1']; ?></center>
	</td>
  </tr>
</table>
<br />
	<?php
	if($char_rename_points > $your_points){
		$disabledr = 1;
		echo "<font color=\"red\"><center>Du hast nicht genug Alive-Cash um dir ein Namenswechsel zu kaufen.</center></font>";
	}else{
		$disabledr = 0;
	}
	?>
<table width="300" border="0" cellpadding="2px">
  <tr>
  <td><?php echo $lang['charname']; ?></td>
  <td>
    <select name="name">
	<?php

	$qray = $CHDB->select("SELECT * FROM `characters` WHERE account='$user[id]'");
	foreach($qray as $c){
        echo "<option value='".$c['name']."'>".$c['name']."</option>";
	}

	?>
</select>
</td>
  </tr>
  <tr>
    <td><?php echo $lang['desired_name']; ?></td>
    <td><input type='text' name='newname' maxlength='20' size='20'/></td>
  </tr>
  <tr>
	<td colspan='2' align='center'>Preis: <font color=blue><u><?php echo $char_rename_points; ?></u></font> <?php echo $lang['vote_points']; ?>
	</td>
  </tr>
	<?php if ($disabledr == 0){ ?>
  <td colspan='2' align='center'>
                        <input type='submit' name='rename' value='Umbenennen'  />
  </td>
  	<?php }else{ ?>
    <td colspan='2' align='center'>
                        <input type='submit' name='rename' value='Zuwenig Alive-Cash' disabled='disabled' />
  </td>
  	<?php } ?>
</table>
</center>
<?php 
} 

?>
</form>
<?php
if (isset($_POST['rename']) && ($char_rename_points <= $your_points)) {
    if (($_POST['name']) == '' or ($_POST['newname']) == '') {
        echo "<p align='center'><font color='red'>Gib einen neuen Namen ein.</font></p>";
            exit();
        }
        $name = $_POST['name'];
        $newname = ucfirst(strtolower(trim($_POST['newname'])));
        $status = check_if_online($name, $CHDB);
        $newname_exist = check_if_name_exist($newname, $CHDB);
        if ($status == -1) {
            echo "<p align='center'><font color='red'>Der Charakter existiert nicht.
                </font></p>";
            exit();
        }
        if ($newname_exist == 1) {
            echo "<p align='center'><font color='red'>Der Name ist schon vorhanden, bitte wähle einen anderen Namen.</font></p>";
            exit();
        }
       if ($status == 1)
        echo "<p align='center'><font color='red'>Du bist online.</font></p>";
    else 
	{
    	change_name($name, $newname, $CHDB, $DB);
		$userObject->changeVotePoints(-$char_rename_points);
		echo "<p align='center'><font color='blue'>Dein Charakter " .$name." wurde erfolgreich in " .$newname. " umbenannt.</font></p>";
    }
}
?>
</td>
</tr>
</table>
</td></tr></table>
</td></tr></table>
</center>
<br />
<center>
<!-- CHARACTER RECUSOMTIZATION -->
<br />
<?php write_subheader("Character Re-Customization"); ?>
<center>
<table width = "580" style = "border-width: 1px; border-style: dotted; border-color: #928058;"><tr><td><table style = "border-width: 1px; border-style: solid; border-color: black; background-image: url('<?php echo $currtmp; ?>/images/light3.jpg');"><tr><td>
<table border=0 cellspacing=0 cellpadding=4 width="580px">
<tr>
<td>
<form action="<?=url_for("account","chartools")?>" method="post">
<center>
<table width="540" border="0" cellpadding="2px">
<tr><td><center>
<?php
if ($show_custom == false){
	?>
	<center>
	<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
	</center>
	<?php
}
elseif($char_custom_points > $your_points){
	echo "<center><font color=\"red\"><center>Du hast nicht genug Alive-Cash um dir eine Customization zu kaufen, du ben&ouml;tigst <?=$char_custom_points?> AC.</center></font></center>";
}
else{
?>
<center><?php echo $lang['customize_desc2']; ?></center>
</table>
<br />
<?php
if($char_custom_points > $your_points){
			$disabledc = 1;
			echo "<font color=\"red\"><center>Du hast nicht genug Alive-Cash um dir ein Customizion zu kaufen.</center></font>";
		}else{
			$disabledc = 0;
			
		}
?>
<table width="250" border="0" cellpadding="2px">
   <tr>
  <td><?php echo $lang['charname']; ?></td>
  <td>
    <select name="char_c_name">
<?php

foreach($characters as $c){
        echo "<option value='".$c['name']."'>".$c['name']."</option>";
}

?>
</select>
</td></tr>
  <tr>
	<td colspan='2' align='center'>Preis: <font color=blue><u><?php echo $char_custom_points; ?></u></font> <?php echo $lang['vote_points']; ?>
	</td>
  </tr>
<tr>
<?php if ($disabledc == 0){ ?>
  <td colspan='2' align='center'>
                        <input type='submit' name='customize' value='Weiter'/>
  </td>
  <?php }else{ ?>
  <td colspan='2' align='center'>
                        <input type='submit' name='customize' value='Zuwenig Alive-Cash' disabled='disabled' />
  </td>
  <?php } ?>
</tr>
</center></td></tr>
</table>
</form>
<table width="300" border="0" cellpadding="2px">
<?php } ?>
<?php
if (isset($_POST['customize']) && ($char_custom_points <= $your_points)) {

        $name = $_POST['char_c_name'];
        $status = check_if_online($name, $CHDB);
        if ($status == -1) {
            echo "<p align='center'><font color='red'>Der Charakter existiert nicht.
                </font></p>";
            exit();
        }
		if ($status == 1)
        echo "<p align='center'><font color='red'>Du bist online.</font></p>";
		else 
	{
            customize($name, $CHDB, $DB);
			$userObject->changeVotePoints(-$char_custom_points);
			//$DB->query("UPDATE `voting_points` SET `points`=(`points` - {$char_custom_points}) WHERE id=?d AND points > {$char_custom_points}",$account_id);
        echo "<p align='center'><font color='blue'>Erfolgreich bearbeitet. Logge dich in WoW ein und bearbeite dein Charakter.</font></p>";
    }
}
?>
</table>
</center>
</td>
</tr>
</table>
</td></tr></table>
</td></tr></table>
</center>
<br />

<!-- CHARACTER FACTION CHANGER -->
<br />
<?php write_subheader("Fraktion ändern"); ?>
<div style="width:580px; border-width: 1px; border-style: dotted; border-color: #928058;">
	<div style="border-width: 1px; border-style: solid; border-color: black; background-image: url('<?php echo $currtmp; ?>/images/light3.jpg');">
		<div style="width:580px; padding:4px;">
		
		<? if(isset($factionChangeSuccess) && $factionChangeSuccess){ ?>
			<p class="noErrorMsg">Du kannst nun beim n&auml;chsten Login die Fraktion deines Charakters &auml;ndern.</p>
		<? } ?>
		<? if(isset($factionChangeErrors) && count($factionChangeErrors) > 0){ ?>
			<ul class="errorMsg">
			<? foreach($factionChangeErrors as $error) { ?>
				<li><?=$error?></li>
			<? } ?>
			</ul>
		<? } ?>
		
		<? if($allow_faction_change == true) { 
		echo '<!-- !'.(int)$MW->getConfig->character_tools->faction_change.' -->';
		?>
		<form action="<?=url_for("account","chartools")?>" method="post">
			Hier kannst du deine Fraktion deines Charakters veränern. Wähle deinen Charakter den du bearbeiten möchtest aus und klick unten auf den Button .<br/>
			<b>Wichtig:</b> Wenn du einen Fraktionstransfer kaufst sei dir da wirklich sicher, einen Fraktionswechsel abschließen ohne die Fraktion geändert zu haben führt zum Verlust von Ruf/Mounts/Equip deiner alten Fraktion.<br />
			<?php echo $lang['charname']; ?> <select name="char_f_name">
			<?php
			foreach($characters as $char){
				?><option value="<?=$char['guid']?>"><?=$char['name']?></option><?
			}
			?>
			</select>
			<br />
			Preis: <font color="blue"><u><?=$char_faction_points?></u></font> AliveCash<br><br />
			<?php if ($your_points >= $char_faction_points){ ?>
				<input type="submit" name="faction_change" value="Best&auml;tigen">
			<?php } else{ ?> 
				<font color="red"><center>Du hast nicht genug Alive-Cash um dir einen Fraktionswechsel zu kaufen.</center></font><br />
			<?php } ?>
		</form>
		<? } else{ ?>
		<center>
			<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
		</center>
		<? } ?>
		</div>
	</div>
</div>
<br />

<!-- CHARACTER Race CHANGER -->
<br />
<?php write_subheader("Rasse ändern"); ?>
<div style="width:580px; border-width: 1px; border-style: dotted; border-color: #928058;">
	<div style="border-width: 1px; border-style: solid; border-color: black; background-image: url('<?php echo $currtmp; ?>/images/light3.jpg');">
		<div style="width:580px; padding:4px;">
		
		<? if(isset($raceChangeSuccess) && $raceChangeSuccess){ ?>
			<p class="noErrorMsg">Du kannst nun beim n&auml;chsten Login die Rasse deines Charakters &auml;ndern.</p>
		<? } ?>
		<? if(isset($raceChangeErrors) && count($raceChangeErrors) > 0){ ?>
			<ul class="errorMsg">
			<? foreach($raceChangeErrors as $error) { ?>
				<li><?=$error?></li>
			<? } ?>
			</ul>
		<? } ?>
		
		<? if($show_changer == true) { ?>
		<form action="<?=url_for("account","chartools")?>" method="post">
			Hier kannst du deine Rasse deines Charakters bearbeiten. Wähle deinen Charakter den du bearbeiten möchtest aus und klick unten auf den Button .<br/>
			<?php echo $lang['charname']; ?> <select name="char_r_name">
			<?php
			foreach($characters as $char){
				?><option value="<?=$char['guid']?>"><?=$char['name']?></option><?
			}
			?>
			</select>
			<br />
			Preis: <font color="blue"><u><?=$char_race_points?></u></font> AliveCash<br><br />
			<?php if ($your_points >= $char_race_points){ ?>
				<input type="submit" name="race_change" value="Best&auml;tigen">
			<?php } else{ ?> 
				<font color="red"><center>Du hast nicht genug Alive-Cash um dir einen Rassenwechsel zu kaufen.</center></font><br />
			<?php } ?>
		</form>
		<? } else{ ?>
		<center>
			<div class="errorMsg"><b><?php echo $lang['chat_disable'] ?></b></div>
		</center>
		<? } ?>
		</div>
	</div>
</div>
<br />
<?php builddiv_end() ?>
