<style type="text/css">
#content .content-top {
	background: url("http://portal.wow-alive.de/templates/Shattered-World/images/wiki/zone/index-bg.jpg") 0 0 no-repeat;
}
#content .body-wrapper,
#content .contents-wrapper,
#content .bg-body { background:none;}
h3.sub-title{margin-left: 100px;}
</style>
<br>
<?php builddiv_start(1, $lang['login']) ?>

<form method="post" action="/account/login">
	<input type="hidden" name="action" value="login">
		
<?php if(isset($requiresLogin) && $requiresLogin == true){ ?>
	<input type="hidden" name="previous-page" value="<?=$previousUrl?>">
	
	<?=write_subheader("Bitte logge dich zuerst ein.");?>
<?php } ?>

<?php
if($user['id']<=0){
?>
<table align="center" width="100%">
	<tr>
		<td align="center" width="100%">
			<div style="border: background : none; margin: 1px; padding: 6px 9px 6px 9px; text-align: center; width: 70%;">
					<b><?php echo $lang['username'] ?> </b> <input type="text"
						size="26" style="font-size: 11px;" name="login">
				</div>
				<div
					style="border: background : none; margin: 1px; padding: 6px 9px 6px 9px; text-align: center; width: 70%;">
					<b><?php echo $lang['pass'] ?> </b> <input type="password"
						size="26" style="font-size: 11px;" name="pass">
				</div>
				<div
					style="background: none; margin: 1px; padding: 6px 9px 0px 9px; text-align: center; width: 70%;">
					<a href="/account/restore/" class="ui-button button2"><span><span>Passwort
								vergessen</span> </span> </a>
					<button type="submit" size="16" class="ui-button button2"
						style="font-size: 12px;">
						<span><span><?php echo $lang['login'] ?> </span> </span>
					</button>
				</div>
		</td>
	</tr>
</table>
<br />
		
<?php
}else{
    echo "<br /><br /><center><b>Welcome ".$user['username']."</b><br />".$lang['now_logged_in']."</center><br /><br />";
}
?>
</form>
		
<?php builddiv_end() ?>