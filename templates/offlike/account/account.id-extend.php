<Br />
<?php builddiv_start(1, "Raid ID Verlängerung") ?>
<style type="text/css">
  .noErrorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #00ff24; background: #afffa9; padding-left:8px;}
  .errorMsg { width: 80%; height: 30px; line-height: 30px; font-size: 10pt; border: 2px solid #e03131; background: #ff9090; padding-left:8px;}
  td.serverStatus1 { font-weight: bold; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus2 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus3 { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
  td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;
 }
</style>
<!-- CHARACTER RAID ID EXTEND -->

<? if(!empty($successMessage)){ ?>
<p class="noErrorMsg"><?=$successMessage?></p>
<? } ?>
<? if(!empty($errorMessage)){ ?>
<p class="errorMsg"><?=$errorMessage?></p>
<? } ?> 

<?php echo "Hier kannst du die Raid IDs von deinem Charakter verlängern so dass diese am Mittwoch nach dem allgemeinen ID Reset wiederhergestellt werden.<br>
			Diese Verlängerung kostet dich <b>".$extendCost."</b> AliveCash.<br><br>"; ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>

    <td width="12"><img src="templates/WotLK/images/metalborder-top-left.gif" alt="" width="12" height="12"></td>
    <td style="background: transparent url(templates/WotLK/images/metalborder-top.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td width="12"><img src="templates/WotLK/images/metalborder-top-right.gif" alt="" width="12" height="12"></td>
</tr>
<tr>
    <td style="background: transparent url(templates/WotLK/images/metalborder-left.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td>
    <table width="100%" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="rankingHeader" colspan="3" align="center" nowrap="nowrap"><?php echo $lang['vote_acct']; ?></td>
        </tr>

		<tr>
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_curacct']; ?></td>
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_curchar']; ?></td> 
            <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['vote_points']; ?></td> 
        </tr>

        <tr>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $user['username']; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap"><?php echo $user['character_name']; ?></td>
            <td class="serverStatus1" align="left" nowrap="nowrap"><?php echo $lang['vote_balance'] ?> <?php echo $_SESSION["points"]; ?><br /><?php echo $lang['vote_apt'] ?> <?php echo $_SESSION["date_points"]; ?> </td>
        </tr>
          
                <tr>
            <td colspan="3" align="left"><br><b><center><?php echo $lang['vote_keep'] ?></center></b>
                <ul>
                    <li>Die ID-Verlängerung gilt nur für deinen Charakter und gilt nur für einen ID-Reset.</li>
				    <li>Wähle oben beim Kontextmenü den gewünschten Charakter aus, je nach Browser kann es sein dass du es zweimal machen musst.</li>
			  		<li>Unten siehst du dann die verfügbaren Raid-IDs die verlängert werden können.</li>
              		<li>Wenn Mittwoch in der Früh die IDs gelöscht werden, werden die gesicherten IDs Mittags wiederhergestellt.</li>
					<li>Sollte dein Charakter zu der Zeit online sein, musst du dich neu einloggen damit die ID angezeigt wird.</li>
					<li>Sollte dein Charakter zu diesem Zeitpunkt bereits eine ID haben wird die gespeicherte ID <b>nicht</b> wiederhergestellt um Ausnutzung des Systems zu vermeiden.</li>
                </ul>
            </td>
        </tr>
        
    
    </table>
        </td>

        <td style="background: transparent url(templates/WotLK/images/metalborder-right.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    </tr>

    <tr>
        <td><img src="templates/WotLK/images/metalborder-bot-left.gif" alt="" width="12" height="11"></td>
        <td style="background: transparent url(templates/WotLK/images/metalborder-bot.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
        <td><img src="templates/WotLK/images/metalborder-bot-right.gif" alt="" width="12" height="11"></td>

    </tr>
    </tbody>

</table><br>

<br />

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>

    <td width="12"><img src="templates/WotLK/images/metalborder-top-left.gif" alt="" width="12" height="12"></td>
    <td style="background: transparent url(templates/WotLK/images/metalborder-top.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td width="12"><img src="templates/WotLK/images/metalborder-top-right.gif" alt="" width="12" height="12"></td>
</tr>
<tr>
    <td style="background: transparent url(templates/WotLK/images/metalborder-left.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    <td>
    <table width="100%" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="rankingHeader" colspan="5" align="center" nowrap="nowrap">Bestehende Raid IDs von <?php echo $user['character_name']; ?></td>
        </tr>
		
		<tr>
            <td class="rankingHeader" align="center" nowrap="nowrap">ID</td> 
            <td class="rankingHeader" align="center" nowrap="nowrap">Raid Instanz</td>
            <td class="rankingHeader" align="center" nowrap="nowrap">Schwierigkeit</td> 
            <td class="rankingHeader" align="center" nowrap="nowrap">Andere Mitglieder des Raids</td> 
            <td class="rankingHeader" align="center" nowrap="nowrap">&nbsp;</td> 
        </tr>
		<? if(count($raidIds) == 0){ ?>
		<tr>
            <td class="serverStatus1" align="center" nowrap="nowrap" colspan="4">Dieser Charakter hat keine aktiven Raid IDs</td>
        </tr>
		<? } ?>
		<? foreach($raidIds as $raidId => $data){ ?>
        <tr>
            <td class="serverStatus1" align="center" nowrap="nowrap" valign="top"><?php echo $raidId; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap" valign="top"><?php echo $data["name"]; ?></td>
            <td class="serverStatus1" align="center" nowrap="nowrap" valign="top"><?php echo $data["label_difficulty"]; ?></td>
            <td class="" align="left" valign="top"><?php echo $data["others"]; ?></td>
            <td class="serverStatus1" align="left" nowrap="nowrap" valign="top"> 
			<? 
			if($data["extended"] == true)
			{ 
				?>
				<strong>Verl&auml;ngert</strong>
				<? 
			}
			else
			{ 
				?>
			
				<form action="<?=url_for("account","id-extend")?>" method="post">
					<input type="hidden" name="instance_id" value="<?php echo $raidId; ?>" />
					<input type='submit' name='extend' value='Verl&auml;ngern' />
				</form>
				<? 
			} 
			?>
			</td>
        </tr>
		<? } ?>
    </table>
        </td>

        <td style="background: transparent url(templates/WotLK/images/metalborder-right.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
    </tr>

    <tr>
        <td><img src="templates/WotLK/images/metalborder-bot-left.gif" alt="" width="12" height="11"></td>
        <td style="background: transparent url(templates/WotLK/images/metalborder-bot.gif) repeat scroll 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;"></td>
        <td><img src="templates/WotLK/images/metalborder-bot-right.gif" alt="" width="12" height="11"></td>

    </tr>
    </tbody>

</table><br>


<?php builddiv_end() ?>
