	<div class="section-title">
		<span>Alive Accountdienste</span>
		<p>Benutze die Accountdienste, die für World of Warcraft verfügbar sind, um einfach deine Spiele upzugraden oder auf verschiedene, 
			zusätzliche Accountoptionen zuzugreifen.</p>
	</div>		
	<div class="main-services">
		<a href="<?=url_for("community", "vote")?>" class="main-services-banner left-bnr" 
			style="background-image:url('/<?=$currtmp?>/images/boxes/thumb-wallpaper.jpg');">
			<div class="panel">
			<span class="wrapper">
				<span class="banner-title">Voten</span>
				<span class="banner-desc">Vote f&uuml;r ALive damit mehr Spieler auf den Server kommen und die Gemeinde weiter w&auml;t.</span>
			</span>
			</div>
		</a>
		<a href="<?=url_for("community", "item")?>" class="main-services-banner right-bnr" 
			style="background-image:url('/<?=$currtmp?>/images/boxes/thumb-main-services-5.jpg');">
			<span class="banner-title">Vote Shop</span>
			<span class="banner-desc">Besuche den Alive Vote Shop und wirf einen Blick auf die M&ouml;glichkeiten die wir anbieten.</span>
		</a>
		
		<a href="<?=url_for("account", "manage")?>" class="main-services-banner left-bnr" 
			style="background-image:url('/<?=$currtmp?>/images/boxes/thumb-main-services-1.jpg');">
			<span class="banner-title">Accountverwaltung</span>
			<span class="banner-desc">Hier kannst du einstellen welche Charaktere f&uuml;r andere Forenbenutzer sichtbar sein sollen.</span>
		</a>
		<a href="<?=url_for("account", "ct")?>" class="main-services-banner right-bnr" 
			style="background-image:url('/<?=$currtmp?>/images/boxes/thumb-main-content-2.jpg');" >
			<span class="banner-title">Transferantrag</span>
			<span class="banner-desc">Details und Informationen wie du deinen bisherigen Charakter auf unseren Server &uuml;n kannst.</span>
		
		</a>
		
		<span class="clear"><!-- --></span>
	</div>
