<br>
<?php builddiv_start(1, $lang['admin_panel']) ?>

<div class="left-col">
<div class="services-content">

<? if(false){ ?>
<ul style="font-weight:bold;"><h2>Forum Manager</h2>
  <li><a href="<?=url_for("admin", "forum")?>"><?php echo $lang['admin_forum'];?> Admin</a></li>
  <li><a href="index.php?n=forum&sub=post&action=newtopic&f=<?php echo (int)$MW->getConfig->generic_values->forum->news_forum_id;?>"><?php echo $lang['news_add'];?></a></li>
  <li><a href="index.php?n=forum&sub=viewforum&fid=<?php echo (int)$MW->getConfig->generic_values->forum->news_forum_id;?>"><?php echo $lang['news_manage'];?></a></li>
</ul>
<? } ?>

<? write_subheader("GM Tools"); ?>
<ul style="font-weight:bold;">
  <li><a href="<?=url_for("server", "bann")?>">Bannliste</a></li>
  <li><a href="<?=url_for("admin", "tickets")?>">GM Ticket Manager</a></li>    
  <li><a href="<?=url_for("admin", "members")?>"><?php echo $lang['users_manage'];?></a></li>
</ul>

<? write_subheader("Transfer Tools"); ?>
<ul style="font-weight:bold;">
  	<?php
  	if ((int)$MW->getConfig->core_work->enable){
  	?>
  <li><a href="/admin/transferlist/#page=1">Character-Transfer GM-Area</a></li>
  <li><a href="<?=url_for("admin", "transfscriptgm")?>">Character to new Account </a></li>
<? if(false){ ?>
  <li><a href="<?=url_for("admin", "gmview")?>">GM Logs viewer</a></li> 
  <li><a href="<?=url_for("server", "glm")?>">Loot Mobs NH/H</a></li>
  <li><a href="<?=url_for("server", "glb")?>">Loot Boss 10NH/H 25NH/H</a></li>
  <li><a href="<?=url_for("admin", "chathandler")?>">Chathandler</a></li>
<? } ?>
</ul>

<? write_subheader("Admin Tools"); ?>
<ul style="font-weight:bold;">
  <li><a href="<?=url_for("admin", "sh")?>">Shortboxinfo</a></li>
  <li><a href="<?=url_for("admin", "clin")?>">Changelog inserts</a></li>
  <li><a href="<?=url_for("admin", "fwfv")?>">"Wirb einen Freund" Aktion</a></li>
  <li><a href="<?=url_for("admin", "members2")?>"><?php echo $lang['users_manage'];?></a></li>
  <li><a href="<?=url_for("admin", "config")?>"><?php echo $lang['site_config'];?></a></li>
  <li><a href="<?=url_for("admin", "realms")?>"><?php echo $lang['realms_manage'];?></a></li>
  <li><a href="<?=url_for("admin", "keys")?>"><?php echo $lang['regkeys_manage'];?></a></li>
  <li><a href="<?=url_for("admin", "donate")?>"><?php echo $lang['donate'];?> Admin</a></li>
  <li><a href="<?=url_for("admin", "langs")?>"><?php echo $lang['langs_manage'];?></a></li>
  <li><a href="<?=url_for("admin", "viewlogs")?>">View GM Logs</a></li>
  <?php } ?>
  <li><a href="<?=url_for("admin", "backup")?>">Backup management</a></li>
  <li><a href="<?=url_for("admin", "chartools")?>">Character Rename</a></li>
  <li><a href="<?=url_for("admin", "chartransfer")?>">Character Transfer</a></li>
</ul>

</div>
</div>

<?php builddiv_end() ?>
