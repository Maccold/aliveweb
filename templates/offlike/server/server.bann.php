<br>
<?php builddiv_start(0, $lang['bann']) ?>
<?php $MANG = new Mangos; ?>

<style type="text/css">
  a.server { border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; font-weight: bold; }
  td.serverStatus1 { font-size: 0.8em; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; }
  td.serverStatus2 { font-size: 0.8em; border-style: solid; border-width: 0px 1px 1px 0px; border-color: #D8BF95; background-color: #C3AD89; }
  td.rankingHeader { color: #C7C7C7; font-size: 10pt; font-family: arial,helvetica,sans-serif; font-weight: bold; background-color: #2E2D2B; border-style: solid; border-width: 1px; border-color: #5D5D5D #5D5D5D #1E1D1C #1E1D1C; padding: 3px;}
</style>

<?php write_metalborder_header(); ?>
    <table cellpadding='3' cellspacing='0' width='100%'>
    <tbody>
       <tr> 
       <td class="rankingHeader" align='left' colspan='6'><?php echo  $lang['post_pages'];?>: <?php echo  $pages_str; ?></td>
       </tr>
       <tr> 
      <td class="rankingHeader" align="center" colspan='6' nowrap="nowrap">Realm: <?php echo $realm_info_new['name']; ?></td>          
    </tr>
    <tr>
     
      <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['id'];?>&nbsp;</td>
      <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['bannedby'];?>&nbsp;</td>
      <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['banreason'];?>&nbsp;</td>
      <td class="rankingHeader" align="center" nowrap="nowrap"><?php echo $lang['active'];?>&nbsp;</td>
    </tr>
<?php foreach($item_res as $item): ?>
    <tr>
            <td class="serverStatus<?php echo $item['res_color'] ?>" align="center"><b style="color: rgb(35, 67, 3);"><?php echo $item['id']; ?></b></td>
            <td class="serverStatus<?php echo $item['res_color'] ?>" align="center"><b style="color: rgb(35, 67, 3);"><?php echo $item['bannedby']; ?></b></td>
            <td class="serverStatus<?php echo $item['res_color'] ?>" align="center"><b style="color: rgb(35, 67, 3);"><?php echo $item['banreason']; ?></b></td>
            <td class="serverStatus<?php echo $item['res_color'] ?>" align="center"><b style="color: rgb(35, 67, 3);"><?php echo $item['active']; ?></b></td>
    </tr>
<?php endforeach; unset($item_res, $item); ?>
<?php unset($realm_info_new); ?>
    </tbody>
    </table>
<?php write_metalborder_footer(); ?>

<?php unset($MANG); ?>
<?php builddiv_end() ?>