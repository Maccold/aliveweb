<?

require_once($_SERVER['DOCUMENT_ROOT']."/public_html/tools/includes/common.php");
require_once($_SERVER['DOCUMENT_ROOT']."/public_html/tools/includes/lootItem.class.php");

class Monster{
	
	public $id = 0;
	public $allDifficultyIds = array();
	public $name;
	public $rank = "", $race = "";
	public $type = "Monster";
	public $zone = "";
	public $databaseUrl; 
	public $lootItems = array();
	public $lootItemsSN = array();		// Loot from Monster with the same name
	public $lootItemsGrey = array(), $lootItemsWhite = array(), 
			$lootItemsGreen = array(), $lootItemsBlue = array(), 
			$lootItemsPurple = array(), $lootItemsOrange = array();
	public $lootChances = array();
	public $lootGroups = array();
	public $minLootChances = array(), $minGroupChances = array();
	public $sameNameMonsterIds = array();
	public $debug = array();
	public $elderItems;
	public $copiedFromMonster;
	public $hasNormalLoot = false;
	public $hasHeroicLoot = false;
	public $hasNormal25Loot = false, $hasHeroic25Loot = false;
	
	public $dropQuelDelar = false;
	
	public function Monster($id, $rank = "boss"){
		$this->id = $id;
		$debug[] = "\nMobID: ".$this->id;
		$this->allDifficultyIds = array(
			N10 => intval($id), 
			N25 => intval($id."01"), 
			H10 => intval($id."02"), 
			H25 => intval($id."03")
		);
		
		$this->rank = $rank;
		
		$this->databaseUrl = "http://www.wowhead.com/";		
		
		if($this->id == 195631)
			$this->type = "Object";	
		
	}
	
	/*
		Get Loot from Database
	*/
	public function parseDatabaseData(){
		
		if($this->type == "Object")
			$mobArray = file($this->databaseUrl."object=".$this->id);
		else
			$mobArray = file($this->databaseUrl."npc=".$this->id);
		
		$mobString = implode("",$mobArray);
		
		$regexMobName = '@<title>([^<]+)</title>@';
		//echo "\n<br>Regex: $regexAdbMobName";
		if(preg_match($regexMobName, $mobString, $matches)){
			//print_r($matches);
			$this->name = str_replace(" - NPC - World of Warcraft", "", $matches[1]);
			$debug[] = "\n<br>Found name: ".$this->name;
		}
		
		if(substr_count($mobString, '4809: {') > 0 || substr_count($mobString, '4813: {') > 0 || substr_count($mobString, '4820: {') > 0){
			$this->dropQuelDelar = true;		
			$this->zone = "icc5";
		}
		if(substr_count($mobString, '4812: {') > 0){
			$this->zone = "icc";
		}
		if(substr_count($mobString, '4722: {') > 0){
			$this->zone = "pdk";
		}
		
		$regexMobItem = 
			//{"classs":15,"id":47242,"level":80,"name":"3Trophy of the Crusade","slot":0,"source":[2],"sourcemore":[{"z":4722}],"subclass":-2,looted:{"mode":80,"4":{"count":19298,"outof":39447},"16":{"count":16639,"outof":17285},"64":{"count":2659,"outof":2722}},count:19298,stack:[1,1]},
			//                              1                                 2            3             4             5    6                               7
			
			'@\{(?:"armor":\d+,)?
				"classs":(\d+),
				(?:"displayid":\d+,)?
				(?:"dps":[^,]+,)?
				("heroic":\d,)?
				"id":(\d+),
				"level":(\d+),
				"name":"(\d)([^"]+)",
				(?:"reqlevel":\d+,)?
				("reqrace":(\d+),)?
				("side":\d,)?
				(?:"skill":\d+,)?
				(?:"slot":\d+,)?
				(?:.*?)?
				modes:\{"mode":\d+,
    				("(\d+)":\{"count":(\d+),"outof":(\d+)\},?)
    				("(\d+)":\{"count":(\d+),"outof":(\d+)\},?)?
    				("(\d+)":\{"count":(\d+),"outof":(\d+)\},?)?
    				("(\d+)":\{"count":(\d+),"outof":(\d+)\},?)?
    				("(\d+)":\{"count":(\d+),"outof":(\d+)\},?)?
    			},count:@x';
			
        if(preg_match_all($regexMobItem, $mobString, $lootMatches)){
			//echo "<br>Found Item";
			//print_r($lootMatches);
			
			$countLoot = count($lootMatches[1]);
			for($j = 0; $j < $countLoot; $j++){
				$itemID = $lootMatches[3][$j];
				//echo "\n<BR> Item $itemID";
				$lootItem = $this->getLootItem($itemID);
				$lootItem->matchWowhead($lootMatches, $j);
				//echo print_r($lootItem,true);
				if($lootItem->dropNormal)
					$this->hasNormalLoot = true;
				if($lootItem->dropHeroic)
					$this->hasHeroicLoot = true;
			}
		}
	} 
	
	
	public function complementLoot(){
		
		if($this->dropQuelDelar){
			// Ally-Griff
			$lootItem = $this->getLootItem(50379);
			$lootItem->name = "Battered Hilt (Alliance)";
			$lootItem->dropHeroic = true;
			$lootItem->chanceHeroic = 0.1;
			$lootItem->lootCondition[0] = 6;
			$lootItem->lootCondition[1] = 469;
			$lootItem->lootCondition[2] = 0;
			
			// Horde Griff
			$lootItem = $this->getLootItem(50380);
			$lootItem->name = "Battered Hilt (Horde)";
			$lootItem->dropHeroic = true;
			$lootItem->chanceHeroic = 0.1;
			$lootItem->lootCondition[0] = 6;
			$lootItem->lootCondition[1] = 67;
			$lootItem->lootCondition[2] = 0;
		}
				
	}
	
		
	public function getLootItem($itemID){
		if(!isset($this->lootItems[$itemID])){
			$this->lootItems[$itemID] = new LootItem($itemID, $this->id);
		}
		return $this->lootItems[$itemID];
	}
	
	public function sortLootByRarity(){
		$all = $this->lootItems;
		
		foreach($all as $id => $item){
			switch($item->rarity){
				case ORANGE:
					$this->lootItemsOrange[$id] = $item; break;
				case PURPLE:
					$this->lootItemsPurple[$id] = $item; break;
				case BLUE:
					$this->lootItemsBlue[$id] = $item; break;
				case GREEN:
					$this->lootItemsGreen[$id] = $item; break;
				case WHITE:
					$this->lootItemsWhite[$id] = $item; break;
				case GREY:
					$this->lootItemsGrey[$id] = $item; break;
			}
		}
	}
	
	public function calcPercentages(){
		
		$sums = array();
		$counts = array();
		
		
		for($i = GREY; $i <= PURPLE; $i++){
			//echo "\n<br>LootChance $i ".$sums[$i]."/".$counts[$i];
			
			if($sums[$i] < $this->minGroupChances[$i])
				$sums[$i] = $this->minGroupChances[$i];
			
			if(!$counts[$i]){
				$debug[] = " => keine Items"; continue;
			}
			$this->lootChances[$i] = max(round($sums[$i]/$counts[$i],8), $this->minLootChances[$i]);
			$debug[] = "= ".$this->lootChances[$i];
		}
	}
	
	public function calcGroups(){
		
		$groups = array();
		$groups[0] = array();
		
		foreach($this->lootItems as $lootItem){
			//$h = ($lootItem->heroic) ? 1 : 0;
			$l = ($lootItem->itemLevel > 0) ? $lootItem->itemLevel : 0;
			
			/*
				47241, 47556, 47242 => Emblem, Crusader Orb, Trophy
				47557, 47558, 47559 => regalia
			*/
			$g = 0;
			if(in_array($lootItem->id, array(47241,47556,47242, 47557, 47558, 47559))){
				// nix
			}
			/*
				49908 => saronit
				52025,52026,5202 => T10.264 Marke
				52028,52029,52030 => T10.277 Marke
			*/
			elseif(in_array($lootItem->id, array(49908,52025,52026,52027,52028,52029,52030))){
				// nix
			}
			elseif($lootItem->cat == 9){
				// Rezept/Plan
				$lootItem->group = 0;
				$groups[0][$lootItem->id] = $lootItem;
			}
			elseif($lootItem->cat == 12){
				// Quest-Item
				$lootItem->group = 0;
				$groups[0][$lootItem->id] = $lootItem;
			}
			elseif($lootItem->id == 43102){
				// Frozen Orb
				$lootItem->group = 0;
				$groups[0][$lootItem->id] = $lootItem;
			}
			else{
			  	if($lootItem->dropNormal){
					$lootItem->group = 1;
					$groups[1][$lootItem->id] = $lootItem;
			  	}
			  	if($lootItem->dropHeroic){
					$lootItem->group = 2;
					$groups[2][$lootItem->id] = $lootItem;
			  	}
				if($lootItem->dropNormal25){
					$lootItem->group = 3;
					$groups[3][$lootItem->id] = $lootItem;
					$this->hasNormal25Loot = true;
			  	}
				if($lootItem->dropHeroic25){
					$lootItem->group = 4;
					$groups[4][$lootItem->id] = $lootItem;
					$this->hasHeroic25Loot = true;
			  	}
			}
			
			//$groups[$l][$h] = $lootItem->id;
		}
		
		/*
		if($this->hasHeroicLoot){
			// Emblem des Triumphs
			$lootItem = $this->getLootItem(47241);
			$groups[2][$lootItem->id] = $lootItem;
			
		}*/
		$this->lootGroups = $groups;
		//print_r($groups);
	}
	
	public function getLoots(){
		$string = "";
		$items = $this->lootItems;
		
		//print_r($items);
		
		$sortStrings = array();
		foreach($this->lootItems as $lootItem){
			if($lootItem->id <= 0 || empty($lootItem->id))
				continue;
			$sortStrings[$lootItem->sortString] = $lootItem->id;
		}
		ksort($sortStrings);
		$sortStrings = array_reverse($sortStrings);
		
		$string .="
			<table>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Group</th>
				<th>%</th>
				<th>Rarity</th>
				<th>Itemlevel</th>
				<th>QuestItem</th>
				<th>Cat</th>
				<th>Drops</th>
			</tr>";
		foreach($sortStrings as $lootItemId){
			$lootItem = $this->lootItems[$lootItemId];
				
			$string .= "
			<tr>
				<td>{$lootItem->id}</td>
				<td>{$lootItem->name}</td>
				<td>{$lootItem->getMinMax()}</td> 
				<td>{$lootItem->group}</td> 
				<td>{$lootItem->chance}%</td> 
				<td>{$lootItem->rarity}</td>
				<td>{$lootItem->itemLevel}</td> 
				<td>{$lootItem->questItem}</td>
				<td>{$lootItem->cat})</td>
				<td>".($lootItem->dropNormal ? "Normal (".$lootItem->chanceNormal."%)" : "").
					" ".($lootItem->dropHeroic ? "Heroic (".$lootItem->chanceHeroic."%)" : "").
					" ".($lootItem->dropNormal25 ? "Normal25" : "").
					" ".($lootItem->dropHeroic25 ? "Heroic25" : "")."</td>
			</tr>";
		}
		$string .= "</table>";
		//$string .= "\nItem [{$lootItem->id}] Name {$lootItem->name} {$lootItem->getMinMax()}x {$lootItem->getChance()}%";
		return $string;
	}
	
	/*
		Macht eine Loot Tabelle und das SQL
	*/	
	public function getFinalLoot(){
		
		$difficulties = array();
		$refs = array();
		$ids = array();
		
		if($this->hasNormal25Loot){
			if($this->hasHeroic25Loot){
				// PDK/ICC
				$difficulties = array(
					"Normal 10" => array("group" => 1, "id" => $this->id, "ref" => $this->id."01", "count" => 3),
					"Normal 25" => array("group" => 3, "id" => $this->id."01", "ref" => $this->id."02", "count" => 4),
					"Heroic 10" => array("group" => 2, "id" => $this->id."02", "ref" => $this->id."03", "count" => 3),
					"Heroic 25" => array("group" => 4, "id" => $this->id."03", "ref" => $this->id."04", "count" => 4),
				);
			}
			else{
				// Alte Raids
				$difficulties = array(
					"Normal 10" => array("group" => 1, "id" => $this->id, "ref" => $this->id."01", "count" => 3),
					"Normal 25" => array("group" => 3, "id" => $this->id."01", "ref" => $this->id."02", "count" => 4),
				);
			}
		}
		else{
			// 5er Ini
			$difficulties = array(
				"Normal" => array("group" => 1, "id" => $this->id, "ref" => $this->id."01", "count" => 2, "countTrash" => 1),
				"Heroic" => array("group" => 2, "id" => $this->id."01", "ref" => $this->id."02", "count" => 2, "countTrash" => 2)
			);
		}
		
		/*
			ICC Endbosse der Viertel (T10 Markenbosse) + LK
		*/	
		if(in_array($this->id, array(37813, 36678, 37955, 36853, 36597))){
			$iccQuarterEndBoss = true;
		}
		else{
			$iccQuarterEndBoss = false;
		}
		
		
		foreach($difficulties as $array){
			$refs[] = $array["ref"];
			$ids[] = $array["id"];
		}
		
		$countDifficulties = count(array_keys($this->allDifficultyIds));
		
		if($this->hasLoot() == false)
			return false;
				
		$string = "\n\n-- [{$this->id}] {$this->name} (".count($this->lootItems).")";
		
		foreach($difficulties as $difficulty => $diffArray){
			
			$monster_id = $diffArray["id"];
			$string .= ($this->type == "Monster") 
				? "\nUPDATE `creature_template` SET `LootID`= ".$monster_id." WHERE `entry` = ".$monster_id.";"			
				: "\nUPDATE `gameobject_template` SET `data1`= ".$monster_id." WHERE `entry` = ".$monster_id.";";			
		}
		
		
		
		$string .= ($this->type == "Monster") 
				? "\nDELETE FROM `creature_loot_template` WHERE `entry` IN(".implode(", ", $ids).");
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `Groupid`, `mincountOrRef`, `maxcount`, `lootcondition`, `condition_value1`, `condition_value2`) VALUES"	
				: "\nDELETE FROM `gameobject_loot_template` WHERE `entry` IN(".implode(", ", $ids).");
INSERT INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `Groupid`, `mincountOrRef`, `maxcount`, `lootcondition`, `condition_value1`, `condition_value2`) VALUES";;
		
		/*
			Schleife..
		*/
		//print_r($this->lootGroups);
		foreach($difficulties as $difficulty => $diffArray){
			$group = $diffArray["group"];
			$monster_id = $diffArray["id"];
			$ref_id = $diffArray["ref"];
			$count = $diffArray["count"];
			
			if(!isset($this->lootGroups[$group]) || count($this->lootGroups[$group]) <= 0)
				continue;
			
			$string .= "\n# $difficulty";
			
			
			if($this->isRaidMonster() && $this->rank == "boss"){
				if($this->zone == "pdk"){
					$string .= "\n(".$monster_id.", 47241, 100, 0, 2, 2, 0,0,0), --  Emblem des Triumphs";		
					if($difficulty == "Normal 25" || $difficulty == "Heroic 25"){
						$string .= "\n(".$monster_id.", 47242, 100, 0, 1, 1, 0,0,0), --  Trophy";
						$string .= "\n(".$monster_id.", 47556, 20, 0, 1, 1, 0,0,0), --  Crusader Orb";
					}
				}
				if($this->zone == "icc"){
					$string .= "\n(".$monster_id.", 49426, 100, 0, 2, 2, 0,0,0), --  Emblem des Frostes";	
					$chance = ($difficulty == "Normal 10") ? 5 : 20;
					$string .= "\n(".$monster_id.", 49908, ".$chance.", 0, 2, 2, 0,0,0), --  Saronit";		
				
					if($difficulty == "Normal 25"){
						$string .= "\n(".$monster_id.", 50274, -5, 0, 1, 1, 0,0,0), --  Shadowfrost Shard (Quest)";
					}
					elseif($difficulty == "Heroic 25"){
						$string .= "\n(".$monster_id.", 50274, -10, 0, 1, 1, 0,0,0), --  Shadowfrost Shard (Quest)";
					}
				}
				
				
			}
			if($difficulty == "Heroic" || $this->isRaidMonster()){
				$string .= "\n(".$monster_id.", 47241, 100, 0, 2, 2, 0,0,0), --  Emblem des Triumphs";		
			}
			if(!$this->isRaidMonster()){
				$string .= "\n(".$monster_id.", 43228, 100, 0, 3, 3, 0,0,0), --  Splitter des Steinbewahrers";		
			}
			
			if($this->rank == "boss"){
				
				if($iccQuarterEndBoss && $difficulty != "Normal 10"){
					
					if($difficulty == "Normal 25"){
						$string .= "\n(".$monster_id.", ".$ref_id.", 100, 0, -".$ref_id.", 2, 0,0,0), --  Lootreferenz $difficulty";	
						$string .= "\n(".$monster_id.", 3781305, 100, 0, -3781305, 2, 0,0,0), --  Lootreferenz T10.264";	
					}
					elseif($difficulty == "Heroic 10"){
						$string .= "\n(".$monster_id.", ".$ref_id.", 100, 0, -".$ref_id.", 1, 0,0,0), --  Lootreferenz $difficulty";	
						$string .= "\n(".$monster_id.", 3781305, 100, 0, -3781305, 2, 0,0,0), --  Lootreferenz T10.264";	
					}
					elseif($difficulty == "Heroic 25"){
						$string .= "\n(".$monster_id.", ".$ref_id.", 100, 0, -".$ref_id.", 2, 0,0,0), --  Lootreferenz $difficulty";	
						$string .= "\n(".$monster_id.", 3781305, 100, 0, -3781305, 2, 0,0,0), --  Lootreferenz T10.264";	
						$string .= "\n(".$monster_id.", 3781306, 100, 0, -3781306, 1, 0,0,0), --  Lootreferenz T10.277";	
					}
					
				}
				else{
					$string .= "\n(".$monster_id.", ".$ref_id.", 100, 0, -".$ref_id.", ".$count.", 0,0,0), --  Lootreferenz $difficulty";	
				}
				// Allgemeine Items
				foreach($this->lootGroups[0] as $lootItem){
					
					$min = $lootItem->min;
					$max = $lootItem->max;
					
					if($lootItem->cat == 12)
						$chance = "-100";
					elseif($difficulty == "Normal" && $lootItem->dropNormal)
						$chance = $lootItem->chanceNormal;
					elseif($difficulty == "Heroic" && $lootItem->dropHeroic)
						$chance = $lootItem->chanceHeroic;
					elseif($difficulty == "Normal 10" && $lootItem->dropNormal)
						$chance = $lootItem->chanceNormal;
					elseif($difficulty == "Heroic 10" && $lootItem->dropHeroic)
						$chance = $lootItem->chanceHeroic;
					elseif($difficulty == "Normal 25" && $lootItem->dropNormal25)
						$chance = $lootItem->chanceNormal25;
					elseif($difficulty == "Heroic 25" && $lootItem->dropHeroic25)
						$chance = $lootItem->chanceHeroic25;
					else
						$chance = 0;
					
					$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$chance.", 0, ".$min.", ".$max.", ".$lootItem->lootCondition[0].", ".$lootItem->lootCondition[1].", ".$lootItem->lootCondition[2]."), /* (".$lootItem->itemLevel.") ".$lootItem->name." */";
					/*
					if($lootItem->cat == 12)
						$string .= "\n(".$monster_id.", ".$lootItem->id.", -100, 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";
					elseif($difficulty == "Normal" && $lootItem->dropNormal)
						$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$lootItem->chanceNormal.", 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";
				elseif($difficulty == "Heroic" && $lootItem->dropHeroic)
					$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$lootItem->chanceHeroic.", 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";
					
				elseif($difficulty == "Normal 10" && $lootItem->dropNormal)
					$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$lootItem->chanceNormal.", 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";
					
				elseif($difficulty == "Heroic 10" && $lootItem->dropHeroic)
					$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$lootItem->chanceHeroic.", 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";
					
				elseif($difficulty == "Normal 25" && $lootItem->dropNormal25)
					$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$lootItem->chanceNormal25.", 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";
					
				elseif($difficulty == "Heroic 25" && $lootItem->dropHeroic25)
					$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$lootItem->chanceHeroic25.", 0, ".$lootItem->min.", ".$lootItem->max.", 0, 0, 0), --  (".$lootItem->itemLevel.") ".$lootItem->name.";";*/
				
				}
			}
			else{
				/* Kein Boss => Keine Lootgruppen*/
				foreach($this->lootItems as $lootItem){
					$min = $lootItem->min;
					$max = $lootItem->max;
					
					if($lootItem->cat == 12)
						$chance = "-100";
					elseif($difficulty == "Normal" && $lootItem->dropNormal)
						$chance = $lootItem->chanceNormal;
					elseif($difficulty == "Heroic" && $lootItem->dropHeroic)
						$chance = $lootItem->chanceHeroic;
					elseif($difficulty == "Normal 10" && $lootItem->dropNormal)
						$chance = $lootItem->chanceNormal;
					elseif($difficulty == "Heroic 10" && $lootItem->dropHeroic)
						$chance = $lootItem->chanceHeroic;
					elseif($difficulty == "Normal 25" && $lootItem->dropNormal25)
						$chance = $lootItem->chanceNormal25;
					elseif($difficulty == "Heroic 25" && $lootItem->dropHeroic25)
						$chance = $lootItem->chanceHeroic25;
					else
						$chance = 0;
					if($chance !== 0){
						$string .= "\n(".$monster_id.", ".$lootItem->id.", ".$chance.", 0, ".$min.", ".$max.", ".$lootItem->lootCondition[0].", ".$lootItem->lootCondition[1].", ".$lootItem->lootCondition[2]."), /* (".$lootItem->itemLevel.") ".$lootItem->name." */";
					}
					
				}
			}	
		}
		
		// Letztes "," durch ";" ersetzen
		$string = substr_replace($string, ");", strrpos($string, "),"), 2);
		
		if($this->rank == "boss"){
			$string .= "
		
DELETE FROM `reference_loot_template` WHERE `entry` IN(".implode(", ",$refs).");
INSERT INTO `reference_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`groupid`, `mincountOrRef`,`maxcount`, `lootcondition`, `condition_value1`,`condition_value2`) VALUES";
		
			foreach($difficulties as $difficulty => $diffArray){
				$group = $diffArray["group"];
				$ref_id = $diffArray["ref"];
				$count = $diffArray["count"];
			
				if(!isset($this->lootGroups[$group]) || count($this->lootGroups[$group]) <= 0)
					continue;
				
				$string .= "
# Referenz $difficulty";

				foreach($this->lootGroups[$group] as $lootItem){
					$string .= "			
($ref_id, ".$lootItem->id.", 0, 1, 1, 1, 0, 0, 0), /* (".$lootItem->itemLevel.") ".$lootItem->name." */";		
			
				}
			}
			// Letztes "," durch ";" ersetzen
			$string = substr_replace($string, ");", strrpos($string, "),"), 2);
		}
		
		
		
		return $string;
	}
	
	public function isRaidMonster(){
		if($this->hasNormal25Loot)
			return true;
		return false;	
	}
		
	public function hasLoot(){ return ((count($this->lootItems) > 0)); }
	
	public function hasItem($searchId){ 
		if(isset($this->lootItems[$searchId])){
			return true;
		}
		return false;
	}
	
}

?>