<?
define("GREY", 1);
define("WHITE", 2);
define("GREEN", 3);
define("BLUE", 4);
define("PURPLE", 5);
define("ORANGE", 6);
define("GOLD", 10);
define("ARTIFACT", 11);
define("HEIRLOOM", 12);
define("UNKNOWN", -1);

define("N10", 1);
define("N25", 2);
define("H10", 3);
define("H25", 4);
?>