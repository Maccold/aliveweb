<br>
<?php builddiv_start(1, $lang['statistic']) ?>
<center>

<?php if($num_chars == 0): ?>
    0 Characters
<?php else: ?>
<center>

<table width="90%">
	<tr>
		<td colspan="2" align="left" style="padding-left: 20px;"><img src="/templates/WoW-Alive_WOTLK/images/stat/battlegrounds-alliance.jpg" alt="Alliance" /></td>
		<td colspan="2" align="right" style="padding-right: 20px;"><img src="/templates/WoW-Alive_WOTLK/images/stat/battlegrounds-horde.jpg" alt="Horde" /></td>
	</tr>
	<tr>
		<td colspan="2" align="left" style="padding-left: 20px;">
            <?php echo $lang['Alliance']; ?>: <?php echo $num_ally; ?> (<?php echo $pc_ally; ?>%)
        </td>
		<td colspan="2" align="right" style="padding-right: 20px;">
            <?php echo $lang['Horde']; ?>: <?php echo $num_horde; ?> (<?php echo $pc_horde; ?>%)
        </td>
	</tr>
	<tr>
		<td align="left"><?=icon_race(1, 0)?></td>
		<td align="left"><?php echo $races[1]; ?> (<?php echo $pc_human; ?>%)</td>
		<td align="right"><?php echo $races[2]; ?> (<?php echo $pc_oraces; ?>%)</td>
		<td align="right"><?=icon_race(2, 0)?></td>
	</tr>
	<tr>
        <td align="left"><?=icon_race(3, 0)?></td>
        <td align="left"><?php echo $races[3]; ?> (<?php echo $pc_dwarf; ?>%)</td>
        <td align="right"><?php echo $races[5]; ?> (<?php echo $pc_undead; ?>%)</td>
        <td align="right"><?=icon_race(5, 0)?></td>
    </tr>
    <tr>
        <td align="left"><?=icon_race(4, 0)?></td>
        <td align="left"><?php echo $races[4]; ?> (<?php echo $pc_ne; ?>%)</td>
        <td align="right"><?php echo $races[6]; ?> (<?php echo $pc_tauren; ?>%)</td>
        <td align="right"><?=icon_race(6, 0)?></td>
    </tr>
    <tr>
        <td align="left"><?=icon_race(7, 0)?></td>
        <td align="left"><?php echo $races[7]; ?> (<?php echo $pc_gnome; ?>%)</td>
        <td align="right"><?php echo $races[8]; ?> (<?php echo $pc_troll; ?>%)</td>
        <td align="right"><?=icon_race(8, 0)?></td>
    </tr>
    <tr>
        <td align="left"><?=icon_race(11, 0)?></td>
        <td align="left"><?php echo $races[11]; ?> (<?php echo $pc_dranei; ?>%)</td>
        <td align="right"><?php echo $races[10]; ?> (<?php echo $pc_be; ?>%)</td>
        <td align="right"><?=icon_race(10, 0)?></td>
    </tr>
	<tr>
		<td colspan="2" align="left" style="padding-left: 20px;"></td>
		<td colspan="2" align="right" style="padding-right: 20px;"></td>
	</tr>
</table>
</center>
<?php endif; ?>

</center>
<?php builddiv_end() ?>
<br><br>
