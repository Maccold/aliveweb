<br>
<?php builddiv_start(0, "Server Info") ?>
<style type="text/css">
    table.serverstatus1 td, table.serverstatus1 th{
        font-size: 0.8em;
        border-style: solid;
        border-width: 0px 2px 2px 1px;
        border-color: #010101;
    }
    table.serverstatus1 td.value {
        text-align: center;
    }
    table.serverstatus1 th {
        text-align:center;
        font-weight: bolder;
        color: #FFFFFF;
        background-color: #010101;
    }
</style>
<div class="table">
<table class="serverstatus1" align="center" cellpadding="0">
    <tr>
		<th colspan="2">Server Info</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_straight as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? htmlspecialchars($config_details[$key]) : ''; ?></td>
	</tr>
<?php endforeach; ?>
    <tr>
		<th colspan="2">Horde und Alliance Zusammenarbeit</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_true_false as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? (($config_details[$key] == 1) ? 'Ja' : 'Nein') : ''; ?></td>
	</tr>
<?php endforeach; ?>
    <tr>
		<th colspan="2">Server Raten</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_blizzlike as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? htmlspecialchars($config_details[$key]) . ' x BlizzLike   .' : ''; ?></td>
	</tr>
<?php endforeach; ?>
    <tr>
		<th colspan="2">Berufsskill</th>
	</tr>
<?php 
	$rowclass = "row2";
	foreach($write_skillchances as $key=>$value): $rowclass = ($rowclass == "row1") ? "row2" : "row1"; ?>
    <tr class="<?=$rowclass?>">
		<td><?php echo htmlspecialchars($value); ?></td>
		<td class="value"><?php echo isset($config_details[$key]) ? htmlspecialchars($config_details[$key]) . '%' : ''; ?></td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<?php builddiv_end() ?><br>
