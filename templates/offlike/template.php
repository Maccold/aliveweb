<?php
if(!defined("INCLUDED"))
	die();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<!--
/*************************************************************************/
/* You may copy, spread the givenned project, 
/* in accordance with GNU GPL, however any change 
/* the code as a whole or a part of the code given project, 
/* advisable produce with co-ordinations of the author of the project
/*
/* (c) Sasha aka LordSc. lordsc@yandex.ru, updated by TGM and Peec
/*************************************************************************/
-->
<base href="http://portal.wow-alive.de">
<meta http-equiv="content-type" content="text/html; charset=<?php echo (string)$MW->getConfig->generic->site_encoding;?>"/>
<link rel="shortcut icon" href="<?php echo $currtmp; ?>/images/favicon.ico"/>
<script src="http://portal.wow-alive.de/wowhead/power.js"></script>
<link rel="alternate" href="<?php echo $this_rss_url; ?>" type="application/rss+xml" title="<?=$page_rss_title?>"/>
<title><?php echo $page_title; ?></title>
<script type="text/javascript" src="http://forum.wow-alive.de/static-wow/local-common/js/third-party/jquery-1.4.4.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="http://forum.wow-alive.de/css/humanity/humanity-1.8.9.css" />
<script language="javascript" type="text/javascript" src="http://forum.wow-alive.de/js/jquery-ui-1.8.9.custom.min.js"></script>

<!-- TS3 Viewer Sideboard -->
<link rel="stylesheet" type="text/css" href="http://forum.wow-alive.de/css/sideboard.css" />
<script type="text/javascript" src="http://static.tsviewer.com/short_expire/js/ts3viewer_loader.js"></script>
<script type="text/javascript" src="/js/sideboard.js"></script>
<!-- END TS3 Viewer Sideboard -->

<style media="screen" title="currentStyle" type="text/css">
    @import url("<?php echo $currtmp; ?>/css/newhp.css");
    @import url("<?php echo $currtmp; ?>/css/newhp_basic.css");
    @import url("<?php echo $currtmp; ?>/css/newhp_icons.css");
    @import url("<?php echo $currtmp; ?>/css/newhp_layout.css");
    @import url("<?php echo $currtmp; ?>/css/newhp_specific.css");
    @import url("<?php echo $currtmp; ?>/css/additional_optimisation.css");
</style>
<script type="text/javascript"><!--
    var SITE_HREF = '<?php echo $MW->getConfig->temp->site_href;?>';
    var DOMAIN_PATH = '<?php echo $MW->getConfig->temp->site_domain;?>';
    var SITE_PATH = '<?php echo $MW->getConfig->temp->site_href;?>';
--></script>
<script src="<?php echo $currtmp; ?>/js/detection.js" type="text/javascript"></script>
<script src="<?php echo $currtmp; ?>/js/functions.js" type="text/javascript"></script>
<script src="js/global.js" type="text/javascript"></script>
<script type="text/javascript" src="js/compressed/prototype.js"></script>
<!--<script type="text/javascript" src="js/compressed/scriptaculous.js?load=effects,slider"></script>-->
<script type="text/javascript" src="js/compressed/behaviour.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="<?php echo $currtmp; ?>/js/template_rules.js"></script>
<script type="text/javascript"><!--
    if (is_ie)
        document.write('<link rel="stylesheet" type="text/css" href="<?php echo $currtmp; ?>/css/additional_win_ie.css" media="screen, projection" />');
    if(is_opera)
        document.write('<link rel="stylesheet" type="text/css" href="<?php echo $currtmp; ?>/css/additional_opera.css" media="screen, projection" />');
    function loadPage(list) {
        location.href=list.options[list.selectedIndex].value
    }
--></script>
</head>

<body id="<?=$ext."-".$sub?>">

<!-- Teamspeak -->
<div class="sb_overlay sb_slide" id="ts_overlay">
	<img class="ts_waitload_overlay" src="http://forum.wow-alive.de/images/widget/ajax-loader.gif"> 
</div>

<div id="ts_control" class="sb_passive_container"> 
	<div id="ts_button"> 	
		<div id="ts_label"></div>     
		<!--<span id="sb_passive_count" class="sb_passive_1n" style="">4</span> -->  
		<div id="sb_passive_large"></div> 
	</div> 
	<img class="sb_waitload" src="http://forum.wow-alive.de/images/widget/ajax-loader.gif"> 
</div>
<!-- /Teamspeak -->

  <!-- Top Navbar Start -->
  <script>
	var global_nav_lang = '<?php echo ""; ?>'; 
	var site_name = '<?php echo (string)$MW->getConfig->generic->site_title ?>';
	var site_link = '<?php echo "http://www.wow-alive.de/" ?>';
	var forum_link = '<?php echo "http://forum.wow-alive.de/forum.php" ?>';
	var armory_link = '<?php echo "http://arsenal.wow-alive.de/" ?>';</script>
<style type="text/css">
	@import "<?php echo $currtmp; ?>/css/topnav.css"; 
</style>
<div id="shared_topnav">
	<script src="<?php echo $currtmp; ?>/js/buildtopnav.js"></script>
</div>

  <!-- TOOLTIP start --> 
<div id="contents">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
  <td><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
  <td bgcolor="#000000"></td>
  <td><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
</tr>
<tr>
  <td bgcolor="#000000"></td>
  <td>
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
        <td width="1" height="1" bgcolor="#000000"></td>
        <td bgcolor="#D5D5D7" height="1"><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
        <td width="1" height="1" bgcolor="#000000"></td>
        </tr>
        <tr>
        <td bgcolor="#A5A5A5" width="1"><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
        <td valign="top" class="trans_div"><div id="tooltiptext"></div></td> 
        <td bgcolor="#A5A5A5" width="1"><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
        </tr>
        <tr>
        <td width="1" height="1" bgcolor="#000000"></td>
        <td bgcolor="#4F4F4F"><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="2" alt="" /></td>
        <td width="1" height="1" bgcolor="#000000"></td>
        </tr>
      </table>
  </td>
  <td bgcolor="#000000"></td>
</tr>
<tr>
  <td><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
  <td bgcolor="#000000"></td>
  <td><img src="<?php echo $currtmp; ?>/images/pixel.gif" width="1" height="1" alt="" /></td>
</tr>
</table>
</div>
<script src="<?php echo $currtmp; ?>/js/tooltip.js" type="text/javascript"></script>
<!-- TOOLTIP end --> 
<?php 
	// print something like that when use redirect('link',0,3); <meta http-equiv=refresh content="'.$wait_sec.';url='.$linkto.'">
	echo $GLOBALS['redirect'];
?>
    <div style="background: url(<?php echo $currtmp; ?>/images/page-bg-top.jpg) repeat-x 0 0; height: 88px; position: relative; width: 100%; "></div>
    <center>
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <div id="hp">
              <div class="top-nav-container">
                <div id="loginbox">
                  <div class="loginboxleft"></div>
                  <div class="loginboxbg">
                    <form action="<?php echo url_for('account', 'login'); ?>" method="post">
					<?php
					if( $userObject->isLoggedIn() ){
    					?>
                    	<input type="hidden" name="action" value="logout"/>
                    	<?php echo $userObject->getName(); ?>
						<?php if ($userObject->can["g_use_pm"]): ?>
                   			| <a href="<?php echo url_for('account', 'pms'); ?>"<?php echo($userObject->pm_num > 0 ? ' style="color:red;"' : '');?>><?php echo $userObject->pm_num; ?> <?php echo $lang['newpms'];?></a>
						<?php endif; ?>
                      	<a href="<?php echo url_for('account', 'manage'); ?>"><img src="<?php echo $currtmp; ?>/images/button-profile.gif" alt="Profile"/></a> 
                      	<input type="image" src="<?php echo $currtmp; ?>/images/button-logout.gif" value="Logout"/>
						<?php 
					} else { 
						?>
                      <input type="hidden" name="action" value="login"/>
                      Login: <input name="login" size="14" type="text"/>
                      Password: <input name="pass" size="14" type="password"/>
                      <input type="image" src="<?php echo $currtmp; ?>/images/button-login.gif" value="Login"/>
						<?php 
					} 
					?>
                    </form>
                  </div>
                  <div class="loginboxright"></div>
                </div>

                <div onmouseover="myshow('countrydropdown');" id="droppf" onmouseout="myhide('countrydropdown');">
                  <div style="overflow: hidden; visibility: inherit; display: block; cursor: default; background-color: transparent; background-image: url(<?php echo $currtmp; ?>/images/countrymenu-bg.gif); height: 19px; padding-left: 9px; padding-top: 2px;"><a class="menufillertop"><?php lang('choose_lang'); ?></a><img src="<?php echo $currtmp; ?>/images/pixel.gif" alt=""/></div>
                  <div id="countrydropdown" style="height: auto; visibility:hidden; display: none;">
<?php foreach($languages as $lang_s=>$lang_name): ?>
<div OnMouseOver="this.style.backgroundColor='rgb(100, 100, 100)';" OnMouseOut="this.style.backgroundColor='rgb(29, 28, 27)';" style="cursor: pointer; background-color: rgb(29, 28, 27); color: rgb(244, 196, 0); font-family: arial,comic sans ms,technical; font-size: 12px; font-style: normal; text-align: left; background-image: url(<?php echo $currtmp; ?>/images/bullet-trans-bg.gif); width: 136px; height: 15px; padding-left: 9px; padding-top: 0px; left: 1px; top: 1px;">
                    <a class="menuLink" style="display:block;" href="javascript:setcookie('Language', '<?php echo $lang_s;?>'); window.location.reload();"><?php echo ($GLOBALS['user_cur_lang']==$lang_s?'&gt; '.$lang_name:$lang_name);?></a>
					</div>
					<?php endforeach; ?>
                  </div>
                </div>
                <div onmouseover="myshow('contextdropdown');" id="dropps" onmouseout="myhide('contextdropdown');">
                  <div style="overflow: hidden; visibility: inherit; display: block; cursor: default; background-color: transparent; background-image: url(<?php echo $currtmp; ?>/images/countrymenu-bg.gif); height: 19px; padding-left: 9px; padding-top: 2px;"><a class="menufillertop"><?php lang('context_menu'); ?>:</a><img src="<?php echo $currtmp; ?>/images/pixel.gif" alt=""/></div>
                  <div id="contextdropdown" style="height: auto; visibility:hidden; display: none;">
<?php foreach($GLOBALS['context_menu'] as $cmenuitem): ?>
<div OnMouseOver="this.style.backgroundColor='rgb(100, 100, 100)';" OnMouseOut="this.style.backgroundColor='rgb(29, 28, 27)';" style="cursor: pointer; background-color: rgb(29, 28, 27); color: rgb(244, 196, 0); font-family: arial,comic sans ms,technical; font-size: 12px; font-style: normal; text-align: left; background-image: url(<?php echo $currtmp; ?>/images/bullet-trans-bg.gif); width: 136px; height: 15px; padding-left: 9px; padding-top: 0px; left: 1px; top: 1px;">
                    <a class="menuLink" style="display:block;" href="<?php echo $cmenuitem['link'];?>"><?php echo $cmenuitem['title'];?></a> 
</div><?php endforeach; ?>
                  </div>
                </div>

<?php if(count($GLOBALS['characters']) > 0): ?>
<div onmouseover="myshow('characterdropdown');" id="droppt" onmouseout="myhide('characterdropdown');">
	<div style="overflow: hidden; visibility: inherit; display: block; cursor: default; background-color: transparent; background-image: url(<?php echo $currtmp; ?>/images/countrymenu-bg.gif); height: 19px; padding-left: 9px; padding-top: 2px;">
		<a class="menufillertop"><?php lang('character_menu'); ?>:</a>
		<img src="<?php echo $currtmp; ?>/images/pixel.gif" alt=""/>
	</div>
	<div id="characterdropdown" style="height: auto; visibility:hidden; display: none;">
	<?php foreach($GLOBALS['characters'] as $character): ?>
		<div OnMouseOver="this.style.backgroundColor='rgb(100, 100, 100)';" OnMouseOut="this.style.backgroundColor='rgb(29, 28, 27)';" style="cursor: pointer; background-color: rgb(29, 28, 27); color: rgb(244, 196, 0); font-family: arial,comic sans ms,technical; font-size: 12px; font-style: normal; text-align: left; background-image: url(<?php echo $currtmp; ?>/images/bullet-trans-bg.gif); width: 136px; height: 15px; padding-left: 9px; padding-top: 0px; left: 1px; top: 1px;">
			<a class="menuLink" style="display:block;" href="javascript:setcookie('cur_selected_character', '<?php echo $character['guid'];?>'); window.location.reload();">
				<?php echo (($userObject->character_id == $character["guid"]) ? "&gt; ":"").$character['name'];?>
			</a> 
	</div>
	<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>

<?php
if ((int)$MW->getConfig->generic_values->realm_info->multirealm):
?>
<div onmouseover="myshow('realmdropdown');" id="droppfo" onmouseout="myhide('realmdropdown');">
	<div style="overflow: hidden; visibility: inherit; display: block; cursor: default; background-color: transparent; background-image: url(<?php echo $currtmp; ?>/images/countrymenu-bg.gif); height: 19px; padding-left: 9px; padding-top: 2px;">
		<a class="menufillertop"><?php lang('realm_menu'); ?>:</a>
		<img src="<?php echo $currtmp; ?>/images/pixel.gif" alt=""/>
	</div>
	<div id="realmdropdown" style="height: auto; visibility:hidden; display: none;">
	<?php foreach($realms as $realm): ?>
		<div OnMouseOver="this.style.backgroundColor='rgb(100, 100, 100)';" OnMouseOut="this.style.backgroundColor='rgb(29, 28, 27)';" style="cursor: pointer; background-color: rgb(29, 28, 27); color: rgb(244, 196, 0); font-family: arial,comic sans ms,technical; font-size: 12px; font-style: normal; text-align: left; background-image: url(<?php echo $currtmp; ?>/images/bullet-trans-bg.gif); width: 136px; height: 15px; padding-left: 9px; padding-top: 0px; left: 1px; top: 1px;">
         	<a class="menuLink" style="display:block;" href="javascript:setcookie('cur_selected_realmd', '<?php echo $realm['id'];?>'); window.location.reload();">
				<?php echo ($user['cur_selected_realmd']==$realm['id']?'&gt; '.$realm['name']:$realm['name']);?>
			</a> 
		</div>
	<?php endforeach; ?>
	</div>
</div>
<?php
    unset($realms);
endif;
?>

                <div style="position: absolute; top: 0; left: 0; z-index: 20002;">
                  <div id="wow-logo">
                    <a href="./"><img title="WoW Alive" alt="wowlogo" height="103" width="252" src="<?php echo $currtmp; ?>/images/pixel000.gif"/></a>
                  </div>
                </div>
              </div>
              <div>
                <div id="hpwrapper">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td valign="top">
                        <div id="navwrapper">
                          <div id="nav">
                            <div id="left-bg"></div>
                            <div id="leftmenu">
                              <div id="leftmenucontainer">
                                <?php echo $main_menu; ?>
                              </div>
                            </div>
                          </div>
                          <div style="clear: both;"></div>
                        </div>
                      </td><td valign="top">
                        <div id="mainwrapper">
						<div id="main">
                            <div id="main-content-wrapper">
                              <div id="main-content">
                                <table cellspacing="0" cellpadding="0" border="0">
                                  <tr>
                                    <td>
                                      <div id="main-top">
                                        <div>
                                          <div><div></div></div>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div id="contentpadding">
                                        <div id="cnt">
                                          <div id="cnt-wrapper">
                                            <div id="contentcontainer">
                                              <table cellspacing="0" cellpadding="0" border="0" width="99%">
                                                <tr>
                                                  <td valign="top">
                                                    <div id="cntmain">
                                                      <div id="cnt-top">
                                                        <div>
                                                          <div></div>
                                                        </div>
                                                      </div>
                                                      <div id="content">
                                                        <div id="content-left">
                                                          <div id="content-right">
                                                            <div style="padding-right:10px; margin-left:11px;" id="compcont"> 
                                                            <div style="clear:both;display:block;position:relative;width:100%;margin-top:-4px;">
                                                            <!-- Pathway -->
                                                      	<?php if($ext != "frontpage"){ ?>
															<div class="redbannerbg"><div class="redbannerleft"></div><div class="redbannerlabel"><?php echo $pathway_str;?></div><div class="redbannerright"></div></div>
														<?php } ?>
                                                        <?php echo $GLOBALS['messages']; ?>
                                                            
															<!-- Component body BEGIN -->
                                                            
                                                    		<? echo $content; ?>
													
                                                            <!-- Component body END -->
                                                            </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div id="cnt-bot">
                                                        <div>
                                                          <div>&nbsp;</div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </td>
<?php if($show_sidebar): ?>
	<td valign="top">
		<?php echo $sidebar_content; ?>
	</td>
<?php endif; ?>
                                                </tr>
                                              </table>                      
                                            </div>
                                          </div>
                                        </div>
                                        <div style="clear: both; font-size: 1px;">&nbsp;</div>
                                        <center>
										

                                              
                                            <span class="textlinks">
                                            <small>
                                              <?php echo $lang['pagegenerated'];?> <?php echo round($exec_time,4);?> sec.
                                              Query's: (RDB: <?php echo $DB->_statistics['count']; ?>,
                                              WSDB: <?php echo $WSDB->_statistics['count']?>,
                                              CHDB: <?php echo $CHDB->_statistics['count']?>)<br/>
                                              <b>&copy; <?php echo (string)$MW->getConfig->generic->copyright; ?></b>
                                              <br/>
                                            </small></span><br><br><br>
                                          </div>
                                        </center>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div id="main-bottom">
                                        <div>
                                          <div>
                                          </div>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
 <!--                           <div style="position: relative; z-index: 10;">
                             <img style="position: absolute; top: -445px; left: -243px;" alt="statue" src="./templates/WotLK/images/statue.png"/></div> -->
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </center>
    <div id="ironframe" style="z-index: 11;"></div>
    <div id="pageend"></div>
      <script type="text/javascript" src="js/wz_tooltip.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15760448-1']);
  _gaq.push(['_setDomainName', '.wow-alive.de']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
  </body>
</html>
