<div class="wiki-tooltip"> 
	<span class="icon-frame frame-36 thumb-<?=$zone->label?>" style="background-image: url(http://portal.wow-alive.de/templates/Shattered-World/images/wiki/zone/thumbnails.png);"></span>
	<h3> 
		<span class="float-right color-q0"> Stufe <?=$zone->getLevel()?> 
		<? 
		if($zone->isHeroic()){ 
			if($zone->getHeroicLevel())
			 	echo '('.$zone->getHeroicLevel().'<span class="icon-heroic-skull"></span>)';
			else 
			 	echo '<span class="icon-heroic-skull"></span>';
		} ?>
		</span> 
		<?=$zone->name?> 
	</h3>
	<? if($zone->expansion == 2){ ?>
	<span class="expansion-name color-ex2"> <a href="javascript:;" class="color-ex2">Benötigt Wrath of the Lich King</a> </span>
	<? } else if($zone->expansion == 1){ ?>
	<span class="expansion-name color-ex1"> <a href="javascript:;" class="color-ex1">Benötigt The Burning Crusade</a> </span>
	<? } ?>
	<div class="color-tooltip-yellow"> <?=$zone->intro?> </div>
	<ul class="item-specs">
		<li> 
			<span class="color-tooltip-yellow">Typ:</span> <?=$zone->getType()?>
			<? if($zone->isHeroic()){?> (Heroisch) <span class="icon-heroic-skull"></span><? } ?>  
		</li>
		<? if(!empty($zone->location)){ ?>
		<li> <span class="color-tooltip-yellow">Ort:</span> <?=$zone->location?> </li>
		<? } ?>
	</ul>
</div>
