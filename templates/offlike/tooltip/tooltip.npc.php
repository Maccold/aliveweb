<div class="wiki-tooltip"> <span class="icon-frame frame-56" style="background-image: url(http://portal.wow-alive.de/templates/Shattered-World/images/npcs/creature<?=$npc->entry?>.jpg)"></span>
	<h3> <?=$npc->name?> </h3>
	<? if(!empty($npc->description)){ ?>
	<div class="color-tooltip-yellow"><?=$npc->description?></div>
	<? } ?>
	<ul class="item-specs">
		<? if( $npc->closed ) { ?>
		<li> <span class="color-tooltip-red">Dieser Boss ist zur Zeit geschlossen. </span></li>
		<? } ?>
		<li> <span class="color-tooltip-yellow">Stufe:</span> <?=$npc->getLevel()?> <?=$npc->getRank()?> </li>
		<? if($npc->hp > 0){ ?>
		<li> 
			<span class="color-tooltip-yellow">Lebenspunkte:</span> <?=$npc->hp?>
		<? if( $npc->hp_hero > 0 ) { ?>(<span class="color-tooltip-green"><?=$npc->hp_hero?></span> Heroisch)<? } ?>
		</li>
		<? } ?>
		<li> <span class="color-tooltip-yellow">Typ:</span> <?=$npc->getType()?> </li>
		<? if(!empty($npc->location)){ ?>
		<li> <span class="color-tooltip-yellow">Ort:</span> <?=$npc->location?> </li>
		<? } ?>
	</ul>
</div>