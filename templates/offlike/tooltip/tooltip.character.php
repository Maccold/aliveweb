<div class="character-tooltip">
	<span class="icon-frame frame-56">
		<img src="<?=$char["image"]?>" alt="" width="56" height="56"/>
		<span class="frame"/>
	</span>
	<h3><?=$char["name"]?></h3>
	<div class="color-c<?=$char["class"]?>"><?=$char["level"].", ".$char["raceLabel"].", ".$char["classLabel"]?></div>
	<div class="color-tooltip-<?=$char["css_faction"]?>">Norgannon</div>
	<span class="character-achievementpoints"><?=$char["achievementPoints"]?></span>
	<span class="clear"><!-- --></span>
		
	<span class="character-talents">
		<span class="icon">
			<span class="icon-frame frame-12">
			<? if(empty($char["talentIcon"])) { ?>
				<img src="/images/icons/18/inv_misc_questionmark.jpg" alt="" width="12" height="12"/>
			<? } else { ?>
				<img src="http://arsenal.wow-alive.de/wow-icons/_images/43x43/<?=$char["talentIcon"]?>.png" alt="" width="12" height="12"/>
			<? } ?>
			</span>
		</span>
		<span class="points"><?=$char["treeOne"]?><ins>/</ins><?=$char["treeTwo"]?><ins>/</ins><?=$char["treeThree"]?></span>
		<span class="clear"><!-- --></span>
	</span>
</div>