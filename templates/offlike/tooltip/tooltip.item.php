<div class="wiki-tooltip">
	<span class="icon-frame frame-56" style="background-image: url(http://portal.wow-alive.de/images/icons/56/<?=$item->icon?>.jpg);"> </span>
	<h3 class="color-q<?=$item->overallQualityId?>"><?=$item->name?></h3>
	<ul class="item-specs" style="margin: 0">
		<?=$item->getData("glyphType")?>
		<?=$item->getData("zoneBound")?> <!-- todo -->
		<?=$item->getData("heroic")?> 
		<?=$item->getData("instanceBound")?>
		<?=$item->getData("conjured")?> 
		<?=$item->getData("bonding")?> 
		<?=$item->getData("maxCount")?>
		<?=$item->getData("requiredSkill")?> 
		<?=$item->getData("requiredAbility")?>
		<?=$item->getData("startQuestId")?>
		<?=$item->getData("inventoryType")?>
		<?=$item->getData("damageData")?>
		<?=$item->getData("armor")?>
		<?=$item->getData("blockValue")?>
		<?=$item->getData("bonusStrength")?>
		<?=$item->getData("bonusAgility")?>
		<?=$item->getData("bonusStamina")?>
		<?=$item->getData("bonusIntellect")?>
		<?=$item->getData("bonusSpirit")?>
		<?=$item->getData("heirloomInfo")?> <!-- todo -->
		<?=$item->getData("fireResist")?>
		<?=$item->getData("natureResist")?>
		<?=$item->getData("frostResist")?>
		<?=$item->getData("shadowResist")?>
		<?=$item->getData("arcaneResist")?>
		<?=$item->getData("enchant")?> <!-- todo -->
		<?=$item->getData("randomEnchantData")?> 
		<?=$item->getData("socketData")?>
		<?=$item->getData("gemProperties")?>
		<?=$item->getData("durabilityCurrent")?>
		<?=$item->getData("allowableRaces")?>
		<?=$item->getData("allowableClasses")?>
		<?=$item->getData("requiredLevel")?>
		<?=$item->getData("requiredFaction")?>
		<?=$item->getData("itemLevel")?>
		<?=$item->getData("requiredPersonalRating")?>
		
		<?=$item->getData("bonusDefenseSkillRating")?>
		<?=$item->getData("bonusDodgeRating")?>
		<?=$item->getData("bonusParryRating")?>
		<?=$item->getData("bonusBlockRating")?>
		<?=$item->getData("bonusHitMeleeRating")?>
		<?=$item->getData("bonusHitRangedRating")?>
		<?=$item->getData("bonusHitSpellRating")?>
		<?=$item->getData("bonusCritMeleeRating")?>
		<?=$item->getData("bonusCritRangedRating")?>
		<?=$item->getData("bonusCritSpellRating")?>
		<?=$item->getData("bonusHitTakenMeleeRating")?>
		<?=$item->getData("bonusHitTakenRangedRating")?>
		<?=$item->getData("bonusHitTakenSpellRating")?>
		<?=$item->getData("bonusCritTakenMeleeRating")?>
		<?=$item->getData("bonusCritTakenRangedRating")?>
		<?=$item->getData("bonusCritTakenSpellRating")?>
		<?=$item->getData("bonusHasteMeleeRating")?>
		<?=$item->getData("bonusHasteRangedRating")?>
		<?=$item->getData("bonusHasteSpellRating")?>
		<?=$item->getData("bonusHitRating")?>
		<?=$item->getData("bonusCritRating")?>
		<?=$item->getData("bonusHitTakenRating")?>
		<?=$item->getData("bonusCritTakenRating")?>
		<?=$item->getData("bonusResilienceRating")?>
		<?=$item->getData("bonusHasteRating")?>
		<?=$item->getData("bonusSpellPower")?>
		<?=$item->getData("bonusAttackPower")?>
		<?=$item->getData("bonusFeralAttackPower")?>
		<?=$item->getData("bonusFeralAttackPower")?>
		<?=$item->getData("bonusManaRegen")?>
		<?=$item->getData("bonusArmorPenetration")?>
		<?=$item->getData("bonusBlockValue")?>
		<?=$item->getData("bonusHealthRegen")?>
		<?=$item->getData("bonusSpellPenetration")?>
		<?=$item->getData("bonusExpertiseRating")?>
		<?=$item->getData("spellData")?>
	
		<?=$item->getData("sellPrice")?>
		<? if(isset($item->setData) && count($item->itemsetItems) > 0 ){ ?>
		<ul class="item-specs">
			<li class="color-tooltip-yellow"><? echo  $item->itemsetName." (".$item->numSetPieces."/".$item->itemsetCount.")"; ?></li>
			<? if($item->requiredSkillId > 0 && $item->meetsSkillReq) { ?>
			<li><span class="color-d4"><? echo $lang_strings['armory.item-tooltip.requires']." ".$item->requiredSkill; ?></span></li>
			<? } elseif($item->requiredSkillId > 0) { ?>
			<li><span class="color-tooltip-red"><? echo $lang_strings['armory.item-tooltip.requires']." ".$item->requiredSkill; ?></span></li>
			<? } ?>
			<? foreach($item->itemsetItems as $setItem){ ?>
			<li class="indent">
				<? if($setItem["equipped"]){ ?>
				<span class="color-q1"><?=$setItem["name"]?></span>
				<? } else { ?>
				<span class="color-d4"><?=$setItem["name"]?></span>
				<? } ?>
			</li>
			<? } ?>
			<li class="indent-top"> </li>
			<? 
			foreach($item->itemsetBonus as $order => $bonus){
				if($item->numSetPieces >= $bonus["threshold"])
					echo '<li class="color-tooltip-green">('.$bonus["threshold"].') '.$lang_strings['armory.item-tooltip.set'].': '.$bonus["desc"].'</li>';
				else
					echo '<li class="color-d4">('.$bonus["threshold"].') '.$lang_strings['armory.item-tooltip.set'].': '.$bonus["desc"].'</li>';	
				
			} ?>
		</ul>
		<? } ?>
	
		<?=$item->getData("desc")?>
	</ul>
<? 
/*if(is_array($item->source) && count($item->source) > 0 ){ ?>
<ul class="item-specs">
	<? if($item->source["value"] == "sourceType.creatureDrop") { ?>
	<li> 
		<span class="color-tooltip-yellow">Boss:</span> <?=$item->source["creatureName"]?> 
		<? if($item->source["is_heroic"]){ ?>
			<span data-tooltip="Heroisch" class="icon-heroic-skull tip"></span> 
		<? } ?>
	</li>
	<li> 
		<span class="color-tooltip-yellow">Schlachtzug:</span> <?=$item->source["areaName"]?> 
	</li>
	<li><span class="color-tooltip-yellow">Drop Chance:</span> <?=$lang_strings["armory.item-info.drop-rate.".$item->source["dropRate"]]?> </li>
	<? } else if($item->source["value"] != "sourceType.none") { ?>
	<li><span class="color-tooltip-yellow"><?=$lang_strings["armory.searchColumn.source"]?>:</span> <?=$lang_strings["armory.searchColumn.".$item->source["value"]]?></li>
	<? } else if($item->source["value"] != "sourceType.none") { ?>
	<li><span class="color-tooltip-yellow"><?=$lang_strings["armory.searchColumn.source"]?>:</span> Unbekannt</li>
	<? } ?>
</ul>
<? }*/ ?>
</div>