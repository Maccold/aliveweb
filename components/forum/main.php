<?php
$com_content['forum'] = array(
    'index' => array(
        '', // g_ option require for view     [0]
        'forums', // loc name (key)                [1]
        url_for('forum'), // Link to                 [2]
        '6-menuForums', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),
    'post' => array(
        '', 
        'post', 
        url_for("forum", "post"),
        '',
        0
    ),
    'viewforum' => array(
        '', 
        'viewforum', 
        url_for("forum", "viewforum"),
        '',
        0
    ),
    'viewtopic' => array(
        '', 
        'viewtopic', 
        url_for("forum", "viewtopic"),
        '',
        1
    ),
    'attach' => array(
        '', 
        'attachs', 
        url_for("forum", "attach"),
        '',
        0
    )
);
?>