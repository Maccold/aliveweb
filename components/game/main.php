<?php


$pathway_info[] = array('title' => "Spiel", 'link' => url_for("game"));


$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}


$com_content['game'] = array(
    'index' => array(
        '', // g_ option require for view     [0]
        'server', // loc name (key)                [1]
        url_for('server'), // Link to                 [2]
        '', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),
	'zone' => array('', 'Zone', url_for('game', 'zone'),'',0),
	'item' => array('', 'Gegenstand', url_for('game', 'item'),'',0),
    'character' => array('', '', url_for("game", "character"), '', 0),
    'guild' => array('', '', url_for("game", "guild"), '', 0),
    'search' => array('', '', url_for("game", "search"), '', 0),
);
