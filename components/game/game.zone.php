<?php
if(INCLUDED!==true)
	exit;

include($_SERVER['DOCUMENT_ROOT']."/core/data/data.zones.php");
include($_SERVER['DOCUMENT_ROOT']."/core/data/data.npcs.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.zone.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");


if(false){
	include("../../core/data/data.zones.php");
	include("../../core/data/data.npcs.php");
	include("../../core/class.zone.php");
	include("../../templates/Shattered-World/game/game.zone.php");
}

$pathway_info[] = array('title' => "Dungeons &amp; Schlachtzüge", 'link' => url_for("game"));

$css_files[] = "/".$currtmp."/css/wiki.css";
$css_files[] = "/".$currtmp."/css/lightbox.css";

$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

if( !empty($zone) ){
	$zone = urldecode($zone);
}
else{
	echo "<!-- Empty $zone -->";
	$req_tpl = false;	// Show 404
	return;
}

/*switch($zone)
{
	case "trial-of-the-crusader":
		$zone_id = 4722;
		break;
	default:
		$zone_id = $zone;
}*/

if(is_numeric($zone))
	$zone_id = $aDB->selectCell("SELECT id FROM armory_instance_template WHERE id = ?d", $zone);
else
	$zone_id = $aDB->selectCell("SELECT id FROM armory_instance_template WHERE label LIKE ?", $zone);

echo "<!-- Zone ID: ".$zone_id." --";		

// Gather Data
if($zone_id && isset($data_zone[$zone_id]) ){
	
	$zone_data = $data_zone[$zone_id];
	$zone = new Zone($zone_id, $zone_data, true);
	$zone->getZoneDetails();
	
}
else{
	echo "<!-- ".print_r($aDB,true)." -->";
	$req_tpl = false;	// Show 404
	return;
}

if(isset($zone->expansion) && $zone->expansion == 2)
	$pathway_info[] = array('title' => "Wrath of the Lich King", 'link' => url_for("game")."/#expansion-2");
if(isset($zone->expansion) && $zone->expansion == 1)
	$pathway_info[] = array('title' => "The Burning Crusader", 'link' => url_for("game")."/#expansion-1");

if($zone->id > 0)
	$req_tpl = true;

if($zone_detail == "loot")
{
	$noBody = true;
	$loot_content = "";
	$loot_count = 0;
	
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/core/cache/loot/zone.".$zone->id.".html")){
		$loot_content = file_get_contents($_SERVER['DOCUMENT_ROOT']."/core/cache/loot/zone.".$zone->id.".html");
		
		$loot_count = substr_count($loot_content, "<tr class");
	}
	else{
	
		$zone->getLoot();
	
	}
	
	//$loot_content = str_replace("http://portal.wow-alive.de/item/", "http://de.wowhead.com/item=", $loot_content);
	
	$view_file = getLayout("/game/game.zone-loot.php");
}
elseif($zone_detail == "bugs")
{
	$noBody = true;
	$bugs = array();
	
	if($zone_id == 4722){
		$bugs[] = array(
			"date" => "24.10.2011", "text" => "	Anub'arak ist derzeit geschlossen da der Kampf den Server crasht."
		);
	}
	
	$view_file = getLayout("/game/game.zone-bugs.php");
}
