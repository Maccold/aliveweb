<?php
if(INCLUDED!==true)exit;

include($_SERVER['DOCUMENT_ROOT']."/core/class.character.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.arenateam.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.prototype.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.php");
include($_SERVER['DOCUMENT_ROOT']."/core/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");
include($_SERVER['DOCUMENT_ROOT']."/core/data/data.races.php");


$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

// Additional CSS
$css_files[] = "/".$currtmp."/css/profile.css";

// Config Variables
$debug = true;

if($search_realm != "norgannon")
	$search_realm = "norgannon";

if(empty($search_for)){
	$req_tpl = false;	// 404
	return;
}

// Find character
$char = new Character($search_for, $char_mode, $debug);
if(!$char){
	$req_tpl = false;	// 404
	return;
}


$pathway_info[] = array('title' => $char->GetName()." @ ".$char->realmName, 'link' => url_for("game", "character", array("search_for" => $char->GetName())));

// Base Variables
$talent_spec = array();
$current_tree = array();
$talent_spec = NULL;
		

if($char_mode == "simple" || $char_mode == "advanced"){
	/*
	 * ***************************************
	 *   S  U  M  M  A  R  Y
	 * ***************************************
	 */
	$css_files[] = "/".$currtmp."/css/summary.css";
	
	$talent_data = $char->GetTalentData();
	$activeSpec = $char->GetActiveSpec();
	$char->BuildCharacter();

	if($talent_data && is_array($talent_data)) {
		$specCount = $char->GetSpecCount();
		for($i = 0; $i < $specCount; $i++) {
			$current_tree[$i] = GetMaxArray($talent_data['points'][$i]);
			$icon = $char->ReturnTalentTreeIcon($current_tree[$i]);
			
			if(!file_exists($_SERVER['DOCUMENT_ROOT']."/images/icons/36/".$icon.".jpg")){
				GetItemIcon($icon, 36);
			}
			
			$talent_spec[$i] = array(
				'group' => $i+1,
				"active" => ($n == $char->GetActiveSpec()),
				'icon'  => "/images/icons/36/".$icon.".jpg",
				'prim'  => $char->ReturnTalentTreesNames($current_tree[$i]),
				"role" => $char->GetTalentSpecRole($current_tree[$i]),
				'treeOne' => $talent_data['points'][$i][$char->GetTalentTab(0)],
				'treeTwo' => $talent_data['points'][$i][$char->GetTalentTab(1)],
				'treeThree' => $talent_data['points'][$i][$char->GetTalentTab(2)]
			);    
			if($activeSpec == $i) {
				$talent_spec[$i]['active'] = 1;
			}
		}
		
		if(!is_array($talent_spec)) {
			$icon = 'ability_seal';
			if(!file_exists($_SERVER['DOCUMENT_ROOT']."/images/icons/36/".$icon.".jpg")){
				GetItemIcon($icon, 36);
			}
			$talent_spec[0] = array(
				'group' => 1,
				'icon' => "/images/icons/36/".$icon.".jpg",
				'prim' => null,
				'treeOne' => 0,
				'treeTwo' => 0,
				'treeThree' => 0
			);
		}
	}
	
	// Professions
	$character_professions = $char->GetCharacterProfessions();
	
	// Equip
	$slots = array(
		INV_HEAD,
		INV_NECK,
		INV_SHOULDER,
		INV_BACK,
		INV_CHEST,
		INV_SHIRT,
		INV_TABARD,
		INV_BRACERS,
		
		INV_GLOVES,
		INV_BELT,
		INV_LEGS,
		INV_BOOTS,
		INV_RING_1,
		INV_RING_2,
		INV_TRINKET_1,
		INV_TRINKET_2,
		
		INV_MAIN_HAND,
		INV_OFF_HAND,
		INV_RANGED_RELIC,
	);
	$equipped_items = array();
	$itemsets = array();
	$itemsInSets = array();
	
	foreach($slots as $slot){
		
		if($item_info = $char->GetCharacterItemInfo($slot)){
			$item_info["icon_raw"] = $item_info["icon"];
			GetItemIcon($item_info["icon"], 56);
			GetItemIcon($item_info["icon"], 18);
			
			$item_info["icon"] = "/images/icons/56/".$item_info["icon"];
			if($item_info["set"] > 0){
				$itemsets[$item_info["set"]][] = $item_info["id"];
				$itemsInSets[$slot] = $item_info["set"];
			}
		}
		else{
			$item_info = array(
				"slot" 			=> $slot,
				"inventoryType" => $char->GetInventoryTypeBySlotId($slot),
				"slot_style"	=> $char->GetCharacterEquipStyle($slot),
				"empty"			=> true,
				"displayInfoId" => 0,
			);
		}
		$equipped_items[$slot] = $item_info;
	}
	// Item sets
	if(count($itemsets) > 0){
		foreach($itemsInSets as $slot_id => $set_id){
			$equipped_items[$slot_id]["paramData"]["set"] = implode(",",$itemsets[$set_id]);
		}	
	}
	
	$gemCounts = array();
	$gemData = array();
	$missingEnchSlot = array();
	$missingGemSlot = array();
	$missingBeltBucket = array();
	$missingGemSum = 0;
	
	foreach($equipped_items as $slot_id => $item){
		if(!empty($item["paramData"])){
			$params = array();
			foreach($item["paramData"] as $key => $value)
				$params[] = $key."=".$value;
			$item["params"] = implode("&amp;", $params);
		}
		else
			$item["params"] = "";
		
		// CSS
		$classes = "slot-".$item["inventoryType"];
		$classes .= " item-quality-".$item["rarity"];
		if(in_array($item["slot"],array(9,5,6,7,10,11,12,13,15))){
			$classes .= " slot-align-right";
			$item["side"] = "right";				
		}
		else{
			$item["side"] = "left";				
		}
		$item["css"] = $classes;				
			
		// Advanced Mode
		if($char_mode == "advanced"){
			
			// Defaults
			$item["auditRight"] = false;
			$item["auditLeft"] = false;
			
			$gemCount = 0;
			for($i = 0; $i < 3; $i++) { 
				//debug("Gem $gem_id");
				if(isset($item["gem".$i."Id"]) && $item["gem".$i."Id"] > 0) {
					$gemCount++;
					$gem_id = $item["gem".$i."Id"];
					if(!isset($gemData[$gem_id])){
						$gemCounts[$gem_id] = 1;
						GetItemIcon($item["gem".$i."Icon"],18);
						
						// get gem name and quality
						$gemRow = $WSDB->selectRow("SELECT `name`,`Quality` FROM `item_template` WHERE `entry`=?d LIMIT 1", $gem_id);
						
						if(!$gemRow)
							continue;
						
						$gemName = $gemRow["name"];
						$gemQuality = $gemRow["Quality"];
						
						$locale = $WSDB->selectCell("SELECT `name_loc3` FROM `locales_item` WHERE `entry`=?d LIMIT 1", $gem_id);
						if($locale)
							$gemName = $locale;
						
						$gemData[$gem_id] = array(
							"name" => $gemName,
							"quality" => $gemQuality,
							"color" => $item["gem".$i."Color"],
							"icon" => $item["gem".$i."Icon"],
						);
					}
					else {
						$gemCounts[$gem_id]++;
					}
				}
			}
			
			// Check Enchantment
			if($item["permanentenchant"] <= 0){
				
				if($item["inventoryType"] == INV_TYPE_WAIST && ($item["gemCount"] <= $item["socketCount"]) ){
					debug("Waist: Item",$item);
					$missingBeltBucket[$slot_id] = $slot_id.":1";
					/*if($item["side"] == "right")
						$item["auditLeft"] = true;
					else
						$item["auditRight"] = true;*/
				}
				
				if($char->IsEnchanter() && $item["inventoryType"] == INV_TYPE_FINGER){
					$missingEnchSlot[$slot_id] = $slot_id.":1";
					if($item["side"] == "right")
						$item["auditRight"] = true;
					else
						$item["auditLeft"] = true;
				}
				else if(in_array($item["inventoryType"], array(
					INV_TYPE_HEAD,
					INV_TYPE_SHOULDER,
					INV_TYPE_CHEST,
					INV_TYPE_LEGS,
					INV_TYPE_FEET,
					INV_TYPE_WRISTS,
					INV_TYPE_HANDS,
					INV_TYPE_WEAPON,
					INV_TYPE_SHIELD,
					INV_TYPE_RANGED,
					INV_TYPE_BACK,
					INV_TYPE_TWOHAND,
					INV_TYPE_MAINHAND,
					//INV_TYPE_RANGED_RIGHT,
				))){
					$missingEnchSlot[$slot_id] = $slot_id.":1";
					if($item["side"] == "right")
						$item["auditLeft"] = true;
					else
						$item["auditRight"] = true;
				}
			}
			
			// Missing gem?
			if($gemCount < $item["socketCount"]){
				$missingGemSum +=($item["socketCount"]-$gemCount);
				$missingGemSlot[$slot_id] = ($item["socketCount"]-$gemCount);
			}
		}
		$equipped_items[$slot_id] = $item;
	}
	arsort($gemCounts);
	
	// Load necessary images
	GetImage("character/summary/backgrounds/race/".$char->GetRace().".jpg");
	GetImage("2d/profilemain/race/".$char->raw["race"]."-".$char->GetGender().".jpg");
	GetImage("2d/inset/".$char->raw["race"]."-".$char->GetGender().".jpg");
	
	
} else if($char_mode == "talent"){
	/*
	 * ***************************************
	 *   T  A  L  E  N  T  S
	 * ***************************************
	 */
	$css_files[] = "/".$currtmp."/css/talent-calculator.css";
	$pathway_info[] = array('title' => "Talente &amp; Glyphen", 'link' => url_for("game", "character", array("search_for" => $char->GetName(), "char_mode" => "talent")));
	
	$view_file = getLayout("/game/game.character-talents.php");
		
	$shownSpec = (isset($char_detail) && $char_detail == "secondary") ? 1 : 0;
	
	$talent_data = $char->GetTalentData();
	$talentSpecs = array();
	$talentTabs = array();
	$talentCells = array();
	$talentIcons = array();
	$talentAbilities = array();
	$talentRequires = array();
	$talentCol = array();
	$talentRow = array();
	$shownBuild = 0;
	
	
	$json = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/core/data/talent-data/3.3.5/class-".$char->GetClass().".js");
	$json = json_decode($json);
	if(!$json){
		debug("json fehler");
	}
	
	if($talent_data && is_array($talent_data)) {
		$specCount = $char->GetSpecCount();
		$firstSpec = true;
		for($i = 0; $i < $specCount; $i++) {
			$current_tree[$i] = GetMaxArray($talent_data['points'][$i]);
			$icon = $char->ReturnTalentTreeIcon($current_tree[$i]);
			
			if(!file_exists($_SERVER['DOCUMENT_ROOT']."/images/icons/36/".$icon.".jpg")){
				GetItemIcon($icon, 36);
			}
			
			$talentSpecs[$i] = array(
				'group' => $i+1,
				"active" => ($i == $activeSpec),
				"shown" => ($i == $shownSpec),
				"type" => ($firstSpec) ? "primary" : "secondary",
				"typeText" => ($firstSpec) ? "Primär" : "Sekundär",
				'icon'  => "/images/icons/36/".$icon.".jpg",
				'name'  => $char->ReturnTalentTreesNames($current_tree[$i]),
				"role" => $char->GetTalentSpecRole($current_tree[$i]),
				'treeOne' => $talent_data['points'][$i][$char->GetTalentTab(0)],
				'treeTwo' => $talent_data['points'][$i][$char->GetTalentTab(1)],
				'treeThree' => $talent_data['points'][$i][$char->GetTalentTab(2)]
			);    
			$firstSpec = false;
			
			// Details
			if($i == $shownSpec){
				
				$n = 0;
				foreach ($talent_data["points"][$i] as $tabId => $tabPoints) {
					$tab = array(
						"n" => $n,
						"active" => ($n == $current_tree[$i]),
						"points" => $tabPoints,
						"icon" => "/images/icons/36/".$char->ReturnTalentTreeIcon($n).".jpg",
						"name" => $char->ReturnTalentTreesNames($n),
						"role" => $char->GetTalentSpecRole($n),
					);
					
					// Get Talents of this TalentTab
					$talentCells[$n] = $char->GetTalentTabCells($tabId, $i);
					
					// Get Images
					GetItemIcon($char->ReturnTalentTreeIcon($n), 36);
					
					$talentTabs[$tabId] = $tab;
					$n++;
				}
				
				$talentBuild = $char->CalculateCharacterTalentBuild();
				$shownBuild = $talentBuild[$shownSpec];
			}
		}
		
		// Process Information from JSON
		$jsonTalentTrees = $json->talentData->talentTrees;
		foreach($jsonTalentTrees as $tree){
			$treeNo = $tree->treeNo;
			foreach($tree->talents as $talent){
				GetItemIcon($talent->icon, 36);
				GetGreyscaleIcon($talent->icon, 36);
				
				if($talent->keyAbility){
					$talentAbilities[] = $talent->id;
				}
				if(isset($talent->req) && $talent->req > 0){
					$talentRequires[$talent->id] = $talent->req;
				}
				$talentCol[$talent->id] = $talent->x;
				$talentRow[$talent->id] = $talent->y;
				
				$talentIcons[$talent->id] = $talent->icon;
			}
		}
		//debug("Icons",$talentIcons);
		
		// Talent Cells mit Icons versorgen
		foreach($talentCells as $tab => $tabCells){
			foreach($tabCells as $i => $cell){
				$talentId = $cell["TalentID"];
				
				$cell["ability"] = (in_array($talentId,$talentAbilities));
				
				if(isset($talentIcons[$talentId])){
					$cell["icon"] = $talentIcons[$talentId];
				}
				
				if(isset($talentRequires[$talentId]) && $talentRequires[$talentId] > 0){
					// Arrow creation
					$requiredTalent = $talentRequires[$talentId];
					$posFromCol = $talentCol[$requiredTalent];
					$posFromRow = $talentRow[$requiredTalent];
					
					$posToCol = $talentCol[$talentId];
					$posToRow = $talentRow[$talentId];
						
					// Same column
					if($posFromCol == $posToCol){
						$diff = $posToRow - $posFromRow - 1;
						$cell["arrow"] = '<span class="arrow arrow-down" style="width: 40px; height: '.(14+(53*$diff)).'px; left: 7px; top: -'.(6+(53*$diff)).'px;"></span>';
					}
					elseif($posFromRow == $posToRow){
						
						// Requirement is left of the talent
						if($posFromCol < $posToCol){
							$diff = $posToCol - $posFromCol;
							$cell["arrow"] = '<span class="arrow arrow-right" style="width: 13px; height: 40px; left: -6px; top: 7px;"></span>';
						}
						// Requirement is left of the talent
						elseif($posFromCol > $posToCol){
							$diff = $posToCol - $posFromCol;
							$cell["arrow"] = '<span class="arrow arrow-left" style="width: 13px; height: 40px; left: 47px; top: 7px;"></span>';
						}
					}
					else{
						// Diagonal requirement -.-
							$colDiff = $posToCol - $posFromCol;
							$rowDiff = $posToRow - $posFromRow;						
							
						if($posFromCol < $posToCol){
							$cell["arrow"] = '<span class="arrow arrow-right-down" style="width: '.(53*$colDiff).'px; height: '.(31+(53*($rowDiff-1))).'px; left: -'.(6+(53*($colDiff-1))).'px; top: -'.(24+(53*($rowDiff-1))).'px;"><ins></ins><em></em></span>';
						}
						if($posFromCol > $posToCol){
							$colDiff = $posFromCol - $posToCol;
							$rowDiff = $posToRow - $posFromRow;						
							$cell["arrow"] = '<span class="arrow arrow-left-down" style="width: '.(53*$colDiff).'px; height: '.(31+(53*($rowDiff-1))).'px; left: '.(6+(53*($colDiff-1))).'px; top: -'.(24+(53*($rowDiff-1))).'px;"><em></em><ins></ins></span>';
						}
							
					}
					
				}
				
				// Update
				$talentCells[$tab][$i] = $cell;
			}
		}
		
		if(!is_array($talent_spec)) {
			$icon = 'ability_seal';
			if(!file_exists($_SERVER['DOCUMENT_ROOT']."/images/icons/36/".$icon.".jpg")){
				GetItemIcon($icon, 36);
			}
			$talent_spec[0] = array(
				'group' => 1,
				'icon' => "/images/icons/36/".$icon.".jpg",
				'prim' => null,
				'treeOne' => 0,
				'treeTwo' => 0,
				'treeThree' => 0
			);
		}
		
		// Glyphs
		$glyphs = $char->GetCharacterGlyphs($shownSpec);
		//$charGlyphString = $glyphs["glyphString"][$shownSpec];
		
		$majorGlyphs = array();
		$minorGlyphs = array();
		$shownGlyphs = array();
		$jsonGlyphData = $json->glyphs[0];
		
		//debug($jsonGlyphData);
		
		foreach($glyphs[$shownSpec] as $i =>$glyph){
			
			$glyphId = $glyph["id"];
			
			if(isset($jsonGlyphData->$glyphId)){
				$data = $jsonGlyphData->$glyphId;
				$glyph["item"] = $data->itemId;
				$glyph["icon"] = $data->icon;
				$glyph["name"] = $data->name;
				$glyph["desc"] = $data->description;
			}
			
			if(empty($glyph["desc"]))
				$glyph["desc"] = $glyph["effect"];
			
			if($glyph["type"] == "major")
				$majorGlyphs[] = $glyph;
			if($glyph["type"] == "minor")
				$minorGlyphs[] = $glyph;
			
			$shownGlyphs[] = $glyph["id"];
			
			GetItemIcon($glyph["icon"], 36);
		}
		
		
		$charGlyphString = implode(", ", $shownGlyphs);
	}
} else if($char_mode == "pvp"){
	/*
	 * ***************************************
	 *   P  V  P
	 * ***************************************
	 */
	$css_files[] = "/".$currtmp."/css/pvp.css";
	$pathway_info[] = array('title' => "PvP", 'link' => url_for("game", "character", array("search_for" => $char->GetName(), "char_mode" => "pvp")));
	
	$view_file = getLayout("/game/game.character-pvp.php");
	
	// Find all teams of this Character
	$teamIds = $CHDB->select('SELECT arenaTeamId,personalRating FROM arena_team_member WHERE guid = ?d', $char->GetGUID());
	
	$arenaTeams = array();
	$bestWeeklyRating = -1;
	$bestWeeklyType = -1;
	
	foreach($teamIds as $row){
		
		if($arenaTeam = new ArenaTeam($row["arenaTeamId"])){
			$arenaTeam->LoadMembers();
			$arenaTeam->personalRating = $row["personalRating"];
			$arenaTeams[$arenaTeam->type] = $arenaTeam;
			
			if($arenaTeam->personalRating > $bestWeeklyRating){
				$bestWeeklyRating = $arenaTeam->personalRating;
				$bestWeeklyType = $arenaTeam->type;
			}
		}
		
	}
	
	ksort($arenaTeams);
	
	$first = true;
	foreach($arenaTeams as $type => $team){
		if($first == true){
			$first = false;
			$arenaTeams[$type]->selected = true;
			break;
		}
	}
} else if($char_mode == "achievement"){
	/*
	 * ***************************************
	 *   A C H I E V E M E N T S
	 * ***************************************
	 */
	
	require_once($_SERVER["DOCUMENT_ROOT"]."/core/class.achievements.php");
	
	$css_files[] = "/".$currtmp."/css/achievement.css";
	$pathway_info[] = array('title' => "Erfolge", 'link' => url_for("game", "character", array("search_for" => $char->GetName(), "char_mode" => "achievements")));
	
	$view_file = getLayout("/game/game.character-achievements.php");
	
	$cacheType = CACHETYPE_CHAR_ACHIEVEMENT;
	$cacheId = $char->GetGUID();
	
	if(!empty($char_detail) && is_numeric($char_detail)){
		$achievementCategory = $char_detail;
	}
	else{
		$achievementCategory = 0;
	}
	
	$achievements = $char->GetAchievementMgr();
	
	$summaryCats = array();
	$lastAchievements = array();
	
	if($achievementCategory == 0){
		/* Summary Page **/
		$summary = $char->GetAchievementMgr()->GetSummaryAchievementData(0);
		
		$info_categories = array(92, 96, 97, 95, 168, 169, 201, 155, 81);
		foreach($info_categories as $achievement_category) {
			$cat = $achievements->GetSummaryAchievementData($achievement_category);
			if($cat) {
				$cat["label"] = $lang_strings["armory.achievements.category.".$achievement_category];
				$cat["percent"] = ($cat["total"] > 0) ? round($cat["earned"]/$cat["total"]*100) : 0;
				$summaryCats[$achievement_category] = $cat;
			}
			else{
				$summaryCats[$achievement_category] = array();
			}
		}
		// Recent Achievements
		$lastAchievements = $achievements->GetLastAchievements();
		debug("last",$last_achievements);
		foreach($lastAchievements as $i => $achievement){
			$lastAchievements[$i]["link"] = "#".$achievement["categoryId"].":a".$achievement["id"];
			GetItemIcon($achievement["icon"],18);
		}
	}
	else{
		$noBody = true;
		
		$view_file = getLayout("/game/game.character-achievement-cat.php");
		
		$catLabel = $lang_strings["armory.achievements.category.".$achievementCategory];
		$achievementsPage = $achievements->LoadAchievementPage($achievementCategory, ($char->GetFaction() == 1) ? 0 : 1);
		
		debug("page",$achievementsPage);
		
		$i = 0;
		$pages = false;
		if($achievement_category == 81 && is_array($comparisonData)) {
			// Generate all available achievements
			$owner_fos = $achievements->BuildFoSListForComparison($pages);
			$pages = $owner_fos;
			$achievementsPage = $owner_fos[0];
		}
		
		$allAchievements = array();
		$pointsEarned = 0;
		$pointsTotal = 0;
		
		if(isset($achievementsPage['completed'])) {
			foreach($achievementsPage['completed'] as $achievement) {
				GetItemIcon($achievement["data"]["icon"],36);
				GetItemIcon($achievement["data"]["icon"],56);
				if(isset($achievement['display']) && $achievement['display'] == 0) {
					continue;
				}
				$pointsEarned += $achievement["points"];
				$pointsTotal += $achievement["points"];
				$achievement["expandable"] = false;
				$achievement["cssClass"] = "";
				if( ( isset($achievement["criteria"]) && count($achievement["criteria"]) > 0 ) || (isset($achievement["achievement_tree"])) ){
					$achievement["cssClass"] = "has-sub";
					$achievement["expandable"] = true;
				}
				
				$achievement["data"]["link"] = $achievement["categoryId"].":a".$achievement["id"];
				$allAchievements[] = $achievement;
				//debug("Completed Ach",$achievement);
			}
		}
		
		if(isset($achievementsPage['incompleted'])) {
			foreach($achievementsPage['incompleted'] as $achievement) {
				GetItemIcon($achievement["data"]["icon"],36);
				GetItemIcon($achievement["data"]["icon"],56);
				if(isset($achievement['display']) && $achievement['display'] == 0) {
					continue;
				}
				$pointsTotal += $achievement["points"];
				$achievement["expandable"] = false;
				$achievement["cssClass"] = "locked";
				if( ( isset($achievement["criteria"]) && count($achievement["criteria"]) > 0 ) || (isset($achievement["achievement_tree"])) ){
					$achievement["cssClass"] .= " has-sub";
					$achievement["expandable"] = true;
				}
				$achievement["data"]["link"] = $achievement["categoryId"].":a".$achievement["id"];
				$allAchievements[] = $achievement;
			}
			
		}
		$countComplete = count($completeAchievements);
		$countTotal = count($incompleteAchievements) + $countComplete;
		$percentageComplete = round($countComplete/$countTotal*100);
		
		//debug("complete",$completedAchievements);
		
		debug("x");
	}
	
}

// Sidebar
if($noBody == false){
	$sidebar_character = "";
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/game/sidebar.character.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$sidebar_character = ob_get_contents();
	ob_end_clean();
}
