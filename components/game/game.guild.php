<?php
if(INCLUDED!==true)exit;
// ==================== //
array_pop($pathway_info); // Remove "Spiel"
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
// ==================== //

include($_SERVER['DOCUMENT_ROOT']."/core/class.character.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.prototype.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.php");
include($_SERVER['DOCUMENT_ROOT']."/core/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");

$css_files[] = "/".$currtmp."/css/profile.css";
$css_files[] = "/".$currtmp."/css/guild.css";

$guild_realm = ucfirst($guild_realm);
$guild_name = urldecode($guild_name);
$sort = isset($_GET["sort"]) ? $_GET["sort"] : "rank";
$sort_dir = isset($_GET["dir"]) ? $_GET["dir"] : "a";
$view = (isset($_GET["view"]) && in_array($_GET["view"],array("profession","list"))) ? $_GET["view"] : "list";
$returnTo = false;

if( empty($guild_realm) || !in_array($guild_realm, array("Norgannon")) )
	redirect(url_for("search"));
if(empty($guild_name)){
	redirect(url_for("search"));
	return;
}


$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}


// Find the Guild
$guildRow = $CHDB->selectRow('
		SELECT g.`guildid`, g.`name`, c.`guid`, c.`name` AS "leader_name", c.class AS `leader_class`, 
			"Norgannon" AS "realm",
			IF( c.race IN(6,2,5,6,8,10) ,"1","0") AS "faction",
			BorderStyle,BorderColor,BackgroundColor,EmblemStyle,EmblemColor
			FROM guild g JOIN characters c ON(g.leaderguid = c.guid)
	 		WHERE g.name LIKE "'.$guild_name.'";');

if(!$guildRow){
	//debug($CHDB);
	$req_tpl = false;
	return;
}

$guildId = $guildRow["guildid"];

$guildRow["factionText"] = ($guildRow["faction"] == FACTION_ALLIANCE) ? "Allianz" : "Horde";
$guildRow["factionStyle"] = ($guildRow["faction"] == FACTION_ALLIANCE) ? "alliance" : "horde";

$guildRow["url"] = "/guild/".$guildRow["realm"]."/".$guildRow["name"];

// Pathway & Title
$pathway_info[] = array('title' => $guildRow["name"], 'link' => url_for("game", "guild", array(
	"guild_realm" => $guildRow["realm"], 
	"guild_name" => $guildRow["name"]. " @ " . $page_title)));
$page_title = $guildRow["name"]. " @ " . $page_title;

if(isset($_GET["character"])){
	$returnTo = "character";
	$returnToText = ucfirst(strtolower($_GET["character"]));
	$returnToUrl = "/character/".urlencode($guildRow["realm"])."/".urlencode($returnToText);
}

$totalMemberCount = $CHDB->selectCell("SELECT count(c.guid) 
	FROM guild_member m JOIN characters c ON (c.guid = m.guid) 
	WHERE guildid = ?d;", $guildId);

switch($sort){
	case "name":
		$sort_field = "c.name";
		break;
	case "level":
		$sort_field = "c.level";
		break;
	case "class":
		$sort_field = "c.class";
		break;
	case "race":
		$sort_field = "c.race";
		break;
	case "rank":
	default:
		$sort_field = "m.rank";
}
$sort_dir = ($sort_dir == "a") ? "ASC" : "DESC";


// Find the Members
$memberRows = $CHDB->select("SELECT c.guid,c.name, c.level, c.class, c.race, c.gender, m.rank 
	FROM guild_member m JOIN characters c ON(c.guid = m.guid)
	WHERE guildid = ?d
	ORDER BY $sort_field $sort_dir;", $guildId);

$i = 1;
$rowclass = "row1";
$lastResultNumber = 0;
foreach($memberRows as $n => $row){
	$lastResultNumber++;
	$memberRows[$n]["name"] = utf8_decode($row["name"]);
	$memberRows[$n]["css"] = cycle($rowclass, array("row1","row2"));
	$memberRows[$n]["url"] = "/character/".$guildRow["realm"]."/".urlencode($row["name"]);

	$achPoints = $DataDB->selectCell("SELECT achievementPoints FROM character_cache WHERE guid = ?d", $row["guid"]);
	
	if(!$achPoints){

		// Cache Daten komplett generieren wenn wir schon mal da sind.
		$char = new Character($row["guid"]);
		//$char->GetCache();
		
		$achPoints = $char->GetAchievementPoints();
	}
	$memberRows[$n]["achievementPoints"] = $achPoints;
}


