<?php
if(INCLUDED!==true)
	exit;


include($_SERVER['DOCUMENT_ROOT']."/core/class.item.prototype.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.npc.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.gameobject.php");
include($_SERVER['DOCUMENT_ROOT']."/core/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");

$css_files[] = "/".$currtmp."/css/wiki.css";
$css_files[] = "/".$currtmp."/css/item.css";
$css_files[] = "/".$currtmp."/css/lightbox.css";

$pathway_info[] = array('title' => "Gegenstände", 'link' => url_for("game"));

$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

if(false){ 
	include("../../core/defines.php"); 
	include("../../core/class.item.prototype.php"); 
	include("../../core/class.item.php"); 
	include("../../core/class.npc.php"); 
	include("../../core/class.gameobject.php"); 
	include("../../templates/Shattered-World/game/game.item.php"); 
}

if( !empty($item_id) ){
	$item_id = urldecode($item_id);
}
else{
	debug("Item nicht gefunden");
	$req_tpl = false;	// 404
	return;
}

$useDbCache = true;
$item = new Item($item_id, true);


if(!$item){
	$req_tpl = false;	// 404
	return;
}
elseif($detail == "base"){
	// Base Page
	
	// Get Rotating Image
	$item->getRotateImage();
	$item->GetFactionCounterpart();
	$item->GetAllItemSources();
	
	/*
	if($cache = $item->getCache()){
		
		echo "<!-- CACHED -->";
		
		$dbCacheData = array(
			"type" => CACHETYPE_TOOLTIP,
			"entry" => $item->entry,
		);
		
		return;
	}*/
	
	$item->CreateTooltip();

	$pathway_info[] = array('title' => $item->name, 'link' => "/item/".$item->entry);
	$page_title = $item->name." - ".$page_title;
	
}
else {
	$noBody = true;
	$loot_content = "";
	$loot_count = 0;

	$item->GetAllItemSources();
	
	if($detail == "dropCreatures"){
		$view_file = getLayout("/game/game.item-source.creature.php");
	
		$sources = $item->GetSources("creature");	
	}
	if($detail == "vendors"){
		$view_file = getLayout("/game/game.item-source.vendor.php");
	
		$sources = $item->GetSources("vendor");	
		debug("Vendors", $sources);
	}
}

