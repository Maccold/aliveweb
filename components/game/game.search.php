<?php
if(INCLUDED!==true)
	exit;


include($_SERVER['DOCUMENT_ROOT']."/core/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.prototype.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.npc.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.gameobject.php");

$css_files[] = "/".$currtmp."/css/search.css";

$pathway_info[] = array('title' => "Suche", 'link' => "");

$MANG = new Mangos;

$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$no_results = true;
$char_results = array();

// Get Variables
$raw_query = (isset($_GET["q"])) ? $_GET["q"] : "";
$search_filter = (isset($_GET["f"])) ? $_GET["f"] : "none";
$sort = (isset($sort)) ? $sort : "none";
$dir = (isset($dir)) ? $dir : "a";
$sort_dir = ($dir == "d") ? "DESC" : "ASC";

// Try Router Variables
if(empty($raw_query) && !empty($q))
	$raw_query = $q;
if(($search_filter == "none") && !empty($f))
	$search_filter = $f;
	
$search_query = sanitize($raw_query);
$search_query = trim($search_query);

// Pagination
$perPage = ($search_filter == "none") ? 3 : 25;
if(!isset($page))
	$page = 1;

$limitStart = ($page - 1) * $perPage;
$firstResultNumber = $limitStart+1;
$lastResultNumber = $limitStart;
$numPages = 1;
$rowclass = "row1";



$displayIds = array();

if( !empty($search_query) ){
	$search_query = urldecode($search_query);
	
	/*
	 * Charaktere
	 */
	$charCount = $CHDB->selectCell('SELECT count(guid) FROM characters WHERE name like "%'.$search_query.'%";');
	
	
	if($charCount > 0){

		switch($sort){
			case "subject":
				$sort_field = "name"; break;
			case "race":
				$sort_field = "race"; break;
			case "class":
				$sort_field = "class"; break;
			default:
				$sort_field = "level"; break;
		}
	
		
		$charRows = $CHDB->select('
		SELECT guid,name,race,level,gender,class, "Norgannon" as `realm` 
			FROM characters 
			WHERE name LIKE "%'.$search_query.'%" 
			ORDER BY `'.$sort_field.'` '.$sort_dir.' 
			LIMIT ?d, ?d;', $limitStart, $perPage);
		$counter = 0;
		
		foreach($charRows as $n => $row){
			$charRows[$n]["raceLabel"] = getRaceLabel($row["race"], $row["gender"]);
			$charRows[$n]["classLabel"] = getClassLabel($row["class"], $row["gender"]);
			
			$icon = "/images/icons/2d/".$row["race"]."-".$row["gender"].".jpg";

			if($row["level"] >= 80)
				$icon = "http://arsenal.wow-alive.de/images/portraits/wow-80/".$row["gender"]."-".$row["race"]."-".$row["class"].".gif";
			elseif($row["level"] >= 70)
				$icon = "http://arsenal.wow-alive.de/images/portraits/wow-70/".$row["gender"]."-".$row["race"]."-".$row["class"].".gif";
			elseif($row["level"] >= 60)
				$icon = "http://arsenal.wow-alive.de/images/portraits/wow/".$row["gender"]."-".$row["race"]."-".$row["class"].".gif";
			$charRows[$n]["icon"] = $icon;
		}
		
		if($search_filter == "character"){
			$numPages = ceil($charCount / $perPage);
			
			foreach($charRows as $n => $row){
			
				$charRows[$n]["rowClass"] = cycle($rowclass, array("row1","row2"));
				$charRows[$n]["faction"] = getFactionId($row["race"]);
				
				$guildName = $CHDB->selectCell("
				SELECT g.name 
					FROM guild g JOIN guild_member gm ON (gm.guildid = g.guildid)
					WHERE gm.guid = ?d", $row["guid"]);
				
				$charRows[$n]["guild"] = $guildName;
				
				$lastResultNumber++;
			}
		}
	}
	
	/*
	 * Items
	 */
	
	$itemCount = $WSDB->selectCell('SELECT count(it.entry) 
		FROM item_template it JOIN locales_item li ON (it.entry = li.entry) 
		WHERE name LIKE "%'.$search_query.'%" OR name_loc3 LIKE "%'.$search_query.'%";');
	
	if($itemCount > 0){

		switch($sort){
			case "level":
				$sort_field = "ItemLevel"; break;
			case "req":
				$sort_field = "RequiredLevel"; break;
			case "subject":
			default:
				$sort_field = "name"; break;
		}
	
		
		$itemRows = $WSDB->select('
		SELECT it.entry, it.name, li.name_loc3, it.ItemLevel, it.Quality as "quality", it.displayid, 
				it.RequiredLevel, InventoryType
			FROM item_template it JOIN locales_item li ON (it.entry = li.entry) 
			WHERE name LIKE "%'.$search_query.'%" OR name_loc3 LIKE "%'.$search_query.'%"
			ORDER BY `'.$sort_field.'` '.$sort_dir.' 
			LIMIT ?d, ?d;', $limitStart, $perPage);
		foreach($itemRows as $n => $row){
			$displayIds[$row["displayid"]] = array("icon" => "");
			if(!empty($row["name_loc3"]))
				$itemRows[$n]["name"] = utf8_decode($row["name_loc3"]);
			else
				$itemRows[$n]["name"] = utf8_decode($row["name"]);
			
			// List view
			if($search_filter == "item"){
				$lastResultNumber++;
				$itemRows[$n]["typeText"] = "";
				$itemRows[$n]["rowClass"] = cycle($rowclass, array("row1","row2"));
			}
		}
		
		if($search_filter == "item"){
			$numPages = ceil($itemCount / $perPage);
		}
	}
	
	
	/*
	 * Gilden
	 */
	 $guildCount = $CHDB->selectCell('SELECT count(guildid) 
		FROM guild  
		WHERE name LIKE "%'.$search_query.'%";');

	 if($guildCount > 0){

	 	switch($sort){
			case "faction":
				$sort_field = "faction"; break;
			case "leader":
				$sort_field = "leader_name"; break;
			case "subject":
			default:
				$sort = "subject";
				$sort_field = "name"; break;
		}
	
	 	$guildRows = $CHDB->select('
		SELECT g.`guildid`, g.`name`, c.`guid`, c.`name` AS "leader_name", c.class AS `leader_class`, 
			IF( c.race IN(6,2,5,6,8,10) ,"1","0") AS "faction",
			BorderStyle,BorderColor,BackgroundColor,EmblemStyle,EmblemColor
			FROM guild g JOIN characters c ON(g.leaderguid = c.guid)
	 		WHERE g.name LIKE "%'.$search_query.'%" 
			ORDER BY `'.$sort_field.'` '.$sort_dir.' 
			LIMIT ?d, ?d;', $limitStart, $perPage);
		foreach($guildRows as $n => $row){
			
	 		$guildRows[$n]["realm"] = "Norgannon";
			$guildRows[$n]["faction_style"] = ($row["faction"] == 1) ? "horde" : "alliance";
			$guildRows[$n]["name"] = utf8_decode($row["name"]);
			$guildRows[$n]["leader_name"] = utf8_decode($row["leader_name"]);
			$guildRows[$n]["leader_url"] = "/character/Norgannon/".urlencode(utf8_decode($row["leader_name"]));
			
	 		
			// List view
			if($search_filter == "guild"){
				$lastResultNumber++;
				$memberCount = $CHDB->selectCell("SELECT COUNT(guid) FROM guild_member WHERE guildid = ?d", $row["guildid"]);
				$guildRows[$n]["memberCount"] = $memberCount;
				$guildRows[$n]["rowClass"] = cycle($rowclass, array("row1","row2"));
			
			}
	 	}
	 	
	 	if($search_filter == "guild"){
	 		$numPages = ceil($guildCount / $perPage);
		}
	 }
	
	/*
	 * Arenateams
	 */
	$arenaCount = $CHDB->selectCell('SELECT count(arenaTeamId) 
		FROM arena_team a JOIN characters c ON(a.captainGuid = c.guid)
	 	WHERE a.name LIKE "%'.$search_query.'%";');
	 debug($CHDB);
	if($arenaCount > 0){
		
		switch($sort){
			case "faction":
				$sort_field = "faction"; break;
			case "leader":
				$sort_field = "leader_name"; break;
			case "subject":
			default:
				$sort_field = "name"; break;
		}
	
	 	$arenaRows = $CHDB->select('
		SELECT a.`arenaTeamId`, a.`name`, a.type, a.rating, c.`guid`, c.`name` AS "leader_name", c.class AS `leader_class`, 
			IF( c.race IN(6,2,5,6,8,10) ,"1","0") AS "faction",
			borderStyle,borderColor,backgroundColor,emblemStyle,emblemColor
			FROM arena_team a JOIN characters c ON(a.captainGuid = c.guid)
	 		WHERE a.name LIKE "%'.$search_query.'%" 
			ORDER BY `'.$sort_field.'` '.$sort_dir.' 
			LIMIT ?d, ?d;', $limitStart, $perPage);
		debug($CHDB);
	 	foreach($arenaRows as $n => $row){
			
	 		$arenaRows[$n]["name"] = utf8_decode($row["name"]);
			$arenaRows[$n]["realm"] = "Norgannon";
			$arenaRows[$n]["faction_style"] = ($row["faction"] == 1) ? "horde" : "alliance";
			
			switch($row["type"]){
				case 5:	
					$typeText = "5v5"; break;
				case 3:	
					$typeText = "3v3"; break;
				case 2:
				default:	
					$typeText = "2v2"; break;
			}
			$arenaRows[$n]["typeText"] = $typeText;
			$arenaRows[$n]["url"] = "/server/arena/".$typeText."/".urlencode($row["name"]);
			$arenaRows[$n]["backgroundColor"] = dechex($row["backgroundColor"]);
			$arenaRows[$n]["borderColor"] = dechex($row["borderColor"]);
			$arenaRows[$n]["emblemColor"] = dechex($row["emblemColor"]);
			
			// List view
			if($search_filter == "arena"){
				$lastResultNumber++;
				$arenaRows[$n]["rowClass"] = cycle($rowclass, array("row1","row2"));
			}
	 	}
	
	}
	/*
	 * Forum
	 */
	
	/*
	 * General
	 */
	
	if(count($displayIds) > 0){
		
		$iconRows = $aDB->select("SELECT `displayid`,`icon` FROM `armory_icons` WHERE `displayid`IN(?a)", array_keys($displayIds));
		foreach($iconRows as $row){
			$displayIds[$row["displayid"]]["icon"] = "/images/icons/36/".$row["icon"].".jpg";
			GetItemIcon($row["icon"], 36);
		}
	}
	
	if($charCount || $itemCount || $arenaCount || $guildCount)
		$no_results = false;
	
	
	
}


