<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title' => "Ranking", 'link' => url_for("server", "ranking") );
// ==================== //


$css_files[] = "/".$currtmp."/css/pvp.css";

// DB
$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

// some config //
$perPage = 50; 
if(!isset($page))
	$page = 1;

$start = ($page - 1) * $perPage;

switch($mode)
{
	case "itemlevel":
		$orderBy = "itemLevel"; 
		$rankField = "itemLevelRank";
		$title = "durchschnittlicher Gegenstandsstufe";
		break;
	case "achievement":
	default:
		$mode = "achievement";
		$orderBy = "achievementPoints"; 
		$rankField = "achievementRank";
		$title = "Erfolgspunkten";
		break;
}


// Count all Chars
$sumChars = $DataDB->selectCell("SELECT count(guid) as `num` FROM character_cache WHERE itemLevel > 0 AND guid > 0 AND level < 81;");

$numPages = ceil($sumChars / $perPage);

// Now the details
$rows = $DataDB->select("SELECT * FROM character_cache WHERE itemLevel > 0 AND guid > 0 AND level < 81 ORDER BY ?# DESC LIMIT ?d, ?d;", $orderBy, $start, $perPage);
$i = $start + 1;
$firstCharNumber = $i;
$characterRows = array();

foreach($rows as $row){
	$row["css"] = "row".(($i % 2)+1);
	$row["faction"] = getFactionId($row["race"]);
	$row["name"] = htmlentities($row["name"]);
	$row["factionLabel"] = ($row["faction"] == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	
	$row["old_rank"] = $row[$rankField];
	$row["rank"] = $i;
	
	$characterRows[$i] = $row; 
		
	$lastCharNumber = $i;
	$i++;
}


