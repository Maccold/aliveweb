<?php
if(INCLUDED!==true)exit;

include($_SERVER['DOCUMENT_ROOT']."/core/class.character.php");
include($_SERVER['DOCUMENT_ROOT']."/core/helpers.php");

$pathway_info[] = array('title' => 'Server','link'=>url_for("server"));
$pathway_info[] = array('title'=>'Bugtracker','link'=>url_for("server", "bugtracker"));

$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$css_files[] = "/".$currtmp."/css/wiki.css";
$css_files[] = "/".$currtmp."/css/bugtracker.css";
$availableBugStates = array("Offen", "Bearbeitung", "Erledigt", "Abgewiesen", "Nicht umsetzbar");

if(!$userObject->isLoggedIn()){
	$requiresLogin = true;
	return;
}

$action = (!isset($action)) ? "" : $action;

if($userObject->GetActiveCharName()){
	$char = new Character($userObject->GetActiveCharName(), "small");
	$charSelected = true;
}
else{
	output_message("warning","Bitte wähle zuerst einen Charakter aus damit du auf den BugTracker komplett zugreifen kannst.");
	$charSelected = false;
}

if(!empty($view) && in_array($view,array("done","in-progress", "all", "open"))){
	$view_detail = $view;
	$view = "list";
}
elseif($action == "create"){
	$view = "detail";
	
	$posterData = array(
		"name" => $char->GetName(),
		"level" => $char->GetLevel(),
		"race" => $char->GetRace(),
		"gender" => $char->GetGender(),
		"class" => $char->GetClass(),
		"realmName" => $char->GetRealmName(),
		"gm" => $userObject->isGM(),
	);
	
	$title = (empty($_POST["title"])) ? "-" : utf8_decode($_POST["title"]);
	$desc = (empty($_POST["desc"])) ? "-" : utf8_decode($_POST["desc"]);
	
	if(!empty($_POST["link2"])){
		$desc .= "\n\nWeiterer Link:\n".$_POST["link2"];
	}
	
	$row = array(
		"class" => $_POST["class"],
		"title" => $title,
		"desc" => $desc,
		"posterData" => json_encode($posterData),
		"posterAccountId" => $userObject->id,
		"date" => date("d.m.Y"),
		"createdTimestamp" => time(),
		"state" => "Offen",
		"link" => $_POST["link"]
	);
	
	
	$result = $DataDB->query("INSERT INTO bug(?#) VALUES(?a)", array_keys($row), array_values($row));
	
	if(!$result){
		output_message("warning","Es gab ein Problem beim Speichern des Eintrags.");
		debug($DataDB);
	}
}
elseif(!empty($bug) && is_numeric($bug)){
	
	$view = "detail";
	$bugId = $bug;
	$bugRow = $DataDB->selectRow("SELECT * FROM bug WHERE `id` = ?d", $bugId);
	
	$bugActions = array();
	if(!empty($bugRow["actions"])){
		$bugActions = json_decode($bugRow["actions"],true);
	}
			
	if(!$bugRow){
		$view = "list";
		output_message("warning","Diesen Bug scheint es nicht zu geben.");
	}
	else{
	
		if($action == "change-comment"){
			$commentId = sanitize($_POST["comment-id"],"int");
			$commentRow = $DataDB->selectRow("SELECT id,actions,posterAccountId FROM kommentar WHERE `id` = ?d", $commentId);
			$commentActions = array();
			if(!empty($commentRow["actions"])){
				$commentActions = json_decode($commentRow["actions"],true);
			}
			
			
			if(!$commentRow){
				output_message("alert", "Diesen Kommentar scheint es nicht zu geben");
				debug("change comment",$DataDB);
			}
			elseif(($commentRow["posterAccountId"] != $userObject->id) /*&& $userObject->isBugtrackerDev() == false*/){
				output_message("alert", "Du kannst keine fremden Kommentare bearbeiten.");
			}
			else{
				$commentActions[] = array(
					"action" => "change",
					"user" => $userObject->id,
					"name" => $char->GetName(),
					"gm" => $userObject->isGM(),
					"ts" => time(),
				);
				$result = $DataDB->query("UPDATE kommentar SET text = ?, actions = ?, changedTimestamp = ?d WHERE id = ?d;", 
					utf8_decode($_POST["new-content"]), json_encode($commentActions), time(), $commentId);
				if(!$result){
					output_message("error","Beim Speichern gab es ein Problem.");
				}
				else{
					// New "Changed" Timestamp for the bug
					$bugRow["changedTimestamp"] = time();
					$result = $DataDB->query("UPDATE bug SET changedTimestamp = ?d WHERE `id` = ?d", $bugRow["changedTimestamp"], $bugId);
				}
			}
			
		}
		
		if($userObject->isGM()){
			
			$bugActions[] = array(
				"action" => $action,
				"user" => $userObject->id,
				"name" => $char->GetName(),
				"ts" => time(),
			);
		
			if($action == "done"){
				$result = $DataDB->query("UPDATE bug SET state = ?, actions = ?, changedTimestamp = ?d WHERE id = ?d", 
					"Erledigt", json_encode($bugActions), time(), $bugId);
				if(!$result){
					output_message("error","Beim Speichern gab es ein Problem.");
				}
			}
			elseif($action == "decline"){
				$result = $DataDB->query("UPDATE bug SET state = ?, actions = ?, changedTimestamp = ?d WHERE id = ?d", 
					"Abgewiesen", json_encode($bugActions), time(), $bugId);
				if(!$result){
					output_message("error","Beim Speichern gab es ein Problem.");
					debug("DataDB",$DataDB);
				}
			}
			elseif($action == "open"){
				$result = $DataDB->query("UPDATE bug SET state = ?, actions = ?, changedTimestamp = ?d WHERE id = ?d", 
					"Offen", json_encode($bugActions), time(), $bugId);
				if(!$result){
					output_message("error","Beim Speichern gab es ein Problem.");
				}
			}
			elseif($action == "edit"){
				$view = "edit";
			}
			elseif($action == "change"){
				
				$values = array(
					"class" => sanitize($_POST["class"]),
					"title" => utf8_decode($_POST["title"]),
					"desc" => utf8_decode($_POST["desc"]),
					"complete" => str_replace("%","",$_POST["complete"]),
					"date2" => date("d.m.Y"),
					"changedTimestamp" => time(),
					"state" => sanitize($_POST["state"]),
					"link" => $_POST["link"],
					"actions" => json_encode($bugActions),
				);
				
				$result = $DataDB->query("UPDATE bug SET ?a WHERE id = ?d", 
					$values, $bugId);
				if(!$result){
					output_message("error","Beim Speichern gab es ein Problem.");
				}
				else{
					output_message("success","Der Eintrag wurde geändert.");
				}
			}
			else{
				// unknown action, delete the recorded action from the array
				array_pop($bugActions);
			}
			
			// Update Data
			$bugRow = $DataDB->selectRow("SELECT * FROM bug WHERE `id` = ?d", $bugId);
		}
	}
	
}
elseif(!empty($new)){
	$view = "new";
}
else{
	$view_detail = "all";
	$view = "list";
}




// Add a Comment to a Bug
if(isset($_POST["action"]) && $_POST["action"] == "new"){
	
	$postBugId = sanitize($_POST["bug"], "int");
	$postBugState = isset($_POST["change-state"]) ? $_POST["change-state"] : ""; 
	
	$posterData = array(
		"name" => $char->GetName(),
		"level" => $char->GetLevel(),
		"race" => $char->GetRace(),
		"gender" => $char->GetGender(),
		"class" => $char->GetClass(),
		"realmName" => $char->GetRealmName(),
		"gm" => $userObject->isGM(),
	);
	
	$row = array(
		"postid" => $postBugId,
		"text" => utf8_decode($_POST["detail"]),
		"name" => $userObject->GetActiveCharName(),
		"posterAccountId" => $userObject->id,
		"posterData" => json_encode($posterData),
		"timestamp" => time(),
	);
	
	if(empty($row["name"])){
		$row["name"] = $userObject->username;
	}
	$bugRow["date2"] = strftime("%d.%m.%Y", time());
	$bugRow["changedTimestamp"] = time();
	
	if($userObject->isGM() && ($postBugState != $bugRow["state"]) && in_array($postBugState, $availableBugStates)){
		// + action field
		$row["action"] = json_encode(array("state" => $postBugState));
	}
	
	$result = $DataDB->query("INSERT INTO kommentar(?#) VALUES(?a)", array_keys($row), array_values($row));
	
	if(!$result){
		output_message("warning","Es gab ein Problem beim Speichern des Kommentares.");
		//debug($DataDB);
	}
	
	if($userObject->isGM() && $postBugState != $bugRow["state"]){
		$bugRow["state"] = $postBugState;

		// Update the Bug state		
		$result = $DataDB->query("UPDATE bug SET changedTimestamp = ?d, date2 = ?, state = ? 
			WHERE id = ?d;", $bugRow["changedTimestamp"], $bugRow["date2"], $bugRow["state"], $postBugId);
		if(!$result){
			output_message("warning","Es gab ein Problem beim Aktualisieren des Bug Reports.");
			//debug($DataDB);
		}
	}
	else{
		// Update only the changedTimestamp of the Bug
		$result = $DataDB->query("UPDATE bug SET changedTimestamp = ?d, date2 = ? WHERE id = ?d;", $bugRow["changedTimestamp"], $bugRow["date2"], $postBugId);
		if(!$result){
			output_message("warning","Es gab ein Problem beim Aktualisieren des Bug Reports.");
			//debug($DataDB);
		}
	}
}

if($view == "new"){
	/*
	 * C R E A T E   N E W   B U G
	 */
	$pathway_info[] = array('title'=>"Neuen Bug eintragen", 'link'=>url_for("server", "bugtracker", array("new" => 1)));
	
	$view_file = getLayout("/server/server.bugtracker-create.php");
	
}
elseif($view == "edit" && is_numeric($bugId) && $userObject->isGM()){
	/*
	 * B U G   E D I T
	 */
	$pathway_info[] = array('title'=>"Bug #".$bugId, 'link'=>url_for("server", "bugtracker", array("bug" => $bugId)));
	$pathway_info[] = array('title'=>"Bearbeitung", 'link'=>url_for("server", "bugtracker", array("bug" => $bugId, "edit" => 1)));
	
	$view_file = getLayout("/server/server.bugtracker-edit.php");
	
	// Für select_field-Helper
	$post_data = $bugRow;
	$post_data["title"] = htmlentities($bugRow['title']);
	$post_data["desc"] = htmlentities($bugRow['desc']);
	
	$createdDetail = "";
	if($bugRow["createdTimestamp"] > 0){
		$createdDetail = "vor ".sec_to_dhms(time()-$bugRow["createdTimestamp"], true);
	}
	
	
}
elseif($view == "detail" && is_numeric($bugId)){
	/*
	 * B U G   D E T A I L
	 */
	
	$pathway_info[] = array('title'=>"Bug #".$bugId,'link'=>url_for("server", "bugtracker", array("bug" => $bugId)));
	
	$view_file = getLayout("/server/server.bugtracker-detail.php");
	
	
	
	$class = $bugRow['class'];
	$title = htmlentities($bugRow['title']);
	$desc = $bugRow['desc'];
	$state = $bugRow['state'];
	$complete = str_replace("%","",$bugRow['complete']);
	$complete .= "%";
	$by = $bugRow['by'];
	$date = $bugRow['date'];
	$date2 = $bugRow['date2'];
	$link = $bugRow['link'];
	$links = array($link => $link);
	$createdDetail = "";
	$changedDetail = "";
	$accountComments = array();
	
	$bugLog = array();
	$otherBugs = array();
	
	if(!empty($link)){
		if(substr_count($link, "Hier den") > 0){
			$links = array();
		}
		
		if(preg_match("@http://(de|www|old).wowhead.com/\??([^=]+)=(\d+).*@i", $link, $matches)){
			$links = array();
			debug("link matches",$matches);
			$links[] = '<a href="http://de.wowhead.com/'.$matches[2]."=".$matches[3].'" target="_blank">WoWHead</a>';
			if( $matches[2] == "zone" ){
				$links[] = '<a href="http://portal.wow-alive.de/game/zone/'.$matches[3].'" target="_blank" data-zone="'.$matches[3].'">Alive</a>';
			}
			if( $matches[2] == "item" ){
				$links[] = '<a href="http://portal.wow-alive.de/item/'.$matches[3].'" target="_blank" data-item="'.$matches[3].'">Alive</a>';
			}
		}
	}
	
	// Find other bugs to the same link
	$search = str_replace("http://","", $link);
	$search = str_replace("de.wowhead.com/","",$search);
	$search = str_replace("www.wowhead.com/","",$search);
	$search = str_replace("old.wowhead.com/","",$search);
	
	$rows = $DataDB->select('SELECT id, title FROM bug WHERE link LIKE "%'.$search.'%" AND state IN("Offen","Bearbeitung") AND id <> ?d LIMIT 0,20;', $bugId);
	
	if(!empty($link) && substr_count($link, "Hier den") <= 0 ){
		foreach($rows as $row){
			$otherBugs[] = '<a href="/server/bugtracker/bug/'.$row["id"].'/" target"_blank">'.htmlentities($row["title"]).'</a>';
		}
	}
	
	// Find links in the description
	$desc = htmlentities($desc);
	$desc = makeWowheadLinks($desc);
	
	
	switch($state){
		case "Erledigt": 
			$cssState = "color-q2"; break;
		case "Offen":
		case "Bearbeitung":
			$cssState = "color-q1"; break;
		case "Abgewiesen":
		case "nicht umsetzbar":
		case "Nicht umsetzbar":
			$cssState = "color-q0"; break;
	}
	
	if($bugRow["createdTimestamp"] > 0){
		$createdDetail = sec_to_dhms(time()-$bugRow["createdTimestamp"], true);
		if(!empty($createdDetail))
			$createdDetail = "vor ".$createdDetail;
	}
	if($bugRow["changedTimestamp"] > 0){
		$changedDetail = sec_to_dhms(time()-$bugRow["changedTimestamp"], true);
		if(!empty($changedDetail))
			$changedDetail = "vor ".$changedDetail;
	}
	
	if(!empty($bugRow["posterData"])){
		$posterData = json_decode($bugRow["posterData"]);
		debug("posterData",$posterData);
		$bugPoster = array(
			"details" => true,
			"name" => $posterData->name,
			"class" => $posterData->class,
			"url" => $char->GetUrl($posterData),
		);
	}
	else{
		$bugPoster = array(
			"details" => false,
		);
	}
	
	$commentRows = $DataDB->select("SELECT * FROM kommentar WHERE `postid` = ?d ORDER BY `id` ASC", $bugId);

	$counter = 1;
	$rowclass = "row1";
	foreach($commentRows as $i => $row){
		$rowclass = cycle($rowclass, array("row1", "row2"));
		$actionLog = array();
		
		$commentRows[$i]["id"] = $row["id"];
		$commentRows[$i]["n"] = $counter++;
		$commentRows[$i]["gm"] = false;
		$commentRows[$i]["css-row"] = $rowclass;
		$commentRows[$i]["avatar"] = "";
		$commentRows[$i]["action"] = "";
		$commentRows[$i]["lastEdit"] = "";
		$commentRows[$i]["name"] = htmlentities($row["name"]);
		$commentRows[$i]["text"] = makeWowheadLinks(htmlentities($row["text"]));
		$commentRows[$i]["date"] = ($row["timestamp"] > 60) ? "vor ".sec_to_dhms(time()-$row["timestamp"],true):"";
		
		// State changes
		if(!empty($row["action"])){
			$actions = json_decode($row["action"]);
			if(isset($actions->state)){
				$actionLog[] = "Status => ".$actions->state;
			}
		}
		// Content changes
		if(!empty($row["actions"])){
			$actions = json_decode($row["actions"]);
			$lastEdit = "";
			foreach($actions as $action){
				if($action->action == "change"){
					$name = ($action->gm) ? "[GM] ".$action->name : $action->name;
					$lastEdit = '<span class="time">von '.$name.' bearbeitet vor <span data-tooltip="'.strftime("%d.%m.%Y",$action->ts).'">'.sec_to_dhms(time()-$action->ts,true).'</span></span>';
				}
			}
			if(!empty($lastEdit)){
				$actionLog[] = $lastEdit;
			}
		}
		$commentRows[$i]["action"] = implode("<br/>", $actionLog);
		
		if(!empty($row["posterData"])){
			$posterData = json_decode($row["posterData"]);
			$commentRows[$i]["details"] = true;
			$commentRows[$i]["avatar"] = $char->GetCharacterAvatar($posterData);
			$commentRows[$i]["char_url"] = $char->GetUrl($posterData);
			$commentRows[$i]["char_class"] = $posterData->class;
		}
		
		if(!empty($row["posterAccountId"])){
			$gm = $DB->selectRow("SELECT * FROM account_access WHERE id = ?d AND gmlevel > 0 AND RealmID = 1", $row["posterAccountId"]);
			if($gm){
				$commentRows[$i]["gm"] = true;
			}
		}
		
		$bugLog[$row["timestamp"]] = $commentRows[$i];
			
	}
	
	// Combine Actions and Comments (later)
	$bugActionLog = array();
	
	if(!empty($bugRow["actions"])){
		$actions = json_decode($bugRow["actions"]);
		
		foreach($actions as $action){
			$ts = $action->ts;
			$bugLog[$ts] = '<span class="time">'.sec_to_dhms(time()-$ts, true, "vor ")."</span> ".$action->name." => Bug Report bearbeitet";
		}
		
	}
	
	ksort($bugLog);
	
}
else{
	/*
	 * L I S T
	 */
		
	$bugRows = $DataDB->select("
	SELECT `id`, `class`, `title`, `state`, `date` as `createdDate`, `date2` as `changedDate`, `createdTimestamp`, `changedTimestamp` 
		FROM bug WHERE 1 ORDER BY `id` DESC");

	$rows = array();
	$rowclass = "row1";
	$rowCount = count($bugRows);
	
	
	foreach($bugRows as $i => $row){
		$rowclass = cycle($rowclass, array("row1", "row2"));
		$row["row-css"] = $rowclass;
		$row["title"] = htmlentities($row["title"]);
		
		// Changed Date
		if($row["createdTimestamp"] == 0){
			$date = explode(".", $row["createdDate"]);
			// 30.10.2011 => array( 0 => 30, 1 => 10, 2 => 2011)
			$row["createdTimestamp"] = strtotime($date[2]."-".$date[1]."-".$date[0]);
		}
		if($row["changedTimestamp"] == 0 && !empty($row["changedDate"])){
			$date = explode(".", $row["changedDate"]);
			// 30.10.2011 => array( 0 => 30, 1 => 10, 2 => 2011)
			$row["changedTimestamp"] = strtotime($date[2]."-".$date[1]."-".$date[0]);
		}
		// No change yet? then use same date for both fields
		if($row["changedTimestamp"] == 0){
			$row["changedTimestamp"] = $row["createdTimestamp"];
		}
		if(empty($row["changedDate"])){
			$row["changedDate"] = strftime("%d.%m.%Y", $row["changedTimestamp"]);
		}
		$row["changedSort"] = strftime("%Y-%m-%d", $row["changedTimestamp"]);
		
		
		if($view_detail == "all"){
			switch($row["state"]){
				case "Erledigt":
					$row["row-css"] .= " done"; 
					$row["status-id"] = 2; 
					break;
				case "Bearbeitung":
					$row["row-css"] .= " inprogress"; 
					$row["status-id"] = 1; 
					break;
				case "nicht umsetzbar":
				case "Nicht umsetzbar":
				case "Abgewiesen":
					$row["row-css"] .= " disabled"; 
					$row["status-id"] = 3; 
					break;
				case "Offen":
				default:
					$row["row-css"] .= " fresh"; 
					$row["status-id"] = 0; 
					break;
			}
		}
		$bugRows[$i] = $row;
		
	}
	
}
