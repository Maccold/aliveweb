<?php

/*
$server = "178.63.89.20:19872";
$db_user = "portal";
$db_pass = "ap0QYqj#xs#9c5ID";
$database = "data";
$servername = "WoW Alive";

$conn = mysql_connect($server, $db_user, $db_pass) or die("Connection failed: ". mysql_error());
mysql_select_db($database, $conn) or die("Select DB failed: " . mysql_error());
*/


$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$css_files[] = "/".$currtmp."/css/server.css";


// Sidebar
$server_sidebar = "";
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/server/server.sidebar.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$server_sidebar = ob_get_contents();
	ob_end_clean();




$com_content['server'] = array(
    'index' => array(
        '', // g_ option require for view     [0]
        'server', // loc name (key)                [1]
        url_for('server'), // Link to                 [2]
        '', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),
    'error' => array(
        '', // g_ option require for view     [0]
        'error', // loc name (key)                [1]
        url_for("server","error"), // Link to                 [2]
        '', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),
    'pvp' => array(
        '', 
        'Spieler gegen Spieler', 
        url_for('server', 'pvp'),
        '',
        0
    ),
	'stalkermap' => array(
        '', 
        'Weltkarte', 
        url_for('server', 'stalkermap'),
        '',
        0
    ),
	'pvp-list' => array(
        '', 
        'Spieler gegen Spieler', 
        url_for('server', 'pvp-list'),
        '',
        0
    ),
	'arena' => array(
        '', 
        'Arena Team', 
        url_for('server', 'arena'),
        '',
        0
    ),
	'commands' => array(
        '', 
        'Availaible Commands(InGame)', 
        mw_url('server', 'commands'),
        '8-menuSupport',
        0
    ),
	'gmonline' => array(
        '', 
        'gm_online', 
        mw_url('server', 'gmonline'),
        '8-menuSupport',
        0
    ),
    'chars' => array(
        '', 
        'Characters on the server', 
        mw_url('server', 'chars'),
        '4-menuInteractive',
        0
    ),
    'realmstatus' => array(
        '', 
        'realms_status', 
        mw_url('server', 'realmstatus'),
        '4-menuInteractive',
        0
    ),
    'honor' => array(
        '', 
        'honor', 
        mw_url('server', 'honor'),
        '4-menuInteractive',
        0
    ),
    'playersonline' => array(
        '', 
        'players_online', 
        mw_url('server', 'playersonline'),
        '4-menuInteractive',
        0
    ),
	'playersonline2' 		=> array('','','','',0),
    'playermap' => array(
        '', 
        'Player Map', 
        mw_url('server', 'playermap'),
        '4-menuInteractive',
        0
    ),
    'talents' => array(
        '', 
        'Talents', 
        mw_url('server', 'talents'),
        '4-menuInteractive',
        0
    ),
    'gms' => array(
        '', 
        'gmlist', 
        mw_url('server', 'gms'),
        '8-menuSupport',
        0
    ),
    'statistic' => array(
        '', 
        'statistic', 
        mw_url('server', 'statistic'),
        '4-menuInteractive',
        0
    ),
    'howtoplay' => array(
        '', 
        'howtoplay', 
        mw_url('server', 'howtoplay'),
        '3-menuGameGuide',
        0
    ),
    'ah' => array(
        '',
        'ah',
        mw_url('server', 'ah'),
        '4-menuGameGuide',
        0
    ),
    'info' => array(
        '',
        'info',
        mw_url('server', 'info'),
        '4-menuGameGuide',
        0
    ),
    'armorsets' => array(
        '', 
        'armorsets', 
        mw_url('server', 'armorsets'),
        '4-menuGameGuide',
        0
    ),
    'rules' => array(
        '', 
        'rules', 
        mw_url('server', 'rules'),
        '4-menuGameGuide',
        0
    ),
    'bann' => array(
        'g_is_admin',
        'bann', 
        mw_url('server', 'bann'),
        '',
        0
    ),
 	'glb' => array(
        'g_is_admin',
        'glb', 
        mw_url('server', 'glb'),
        '',
        0
    ),
 	'glm' => array(
        'g_is_admin',
        'glm', 
        mw_url('server', 'glm'),
        '',
        0
    ),
    'ta' => array(
        '',
        'ta', 
        mw_url('server', 'ta'),
        '',
        0
    ),
    'fwfv' => array(
        '',
        'fwfv', 
        mw_url('server', 'fwfv'),
        '',
        0
    ),
    'ranking' => array('', '', url_for("server", "ranking"), '', 0),
    'bugtracker' => array('', '', url_for("server", "bugtracker"), '', 0),
    'roadmap' => array('', '', url_for("server", "roadmap"), '', 0),
);
