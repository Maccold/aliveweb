<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title' => "PvP", 'link' => url_for("server", "pvp") );
$pathway_info[] = array('title' => "Arena: $mode-Ladder", 'link' => url_for("server", "pvp-list", array("mode" => $mode)) );
// ==================== //


$css_files[] = "/".$currtmp."/css/pvp.css";

if( empty($mode) || !in_array($mode, array("2v2","3v3","5v5")) )
	redirect(url_for("server", "pvp"));


// some config //
$perPage = 50; 
$MANG = new Mangos;

$arenaChars = array();
$arenaTeams = array();

$type = 2;
switch($mode)
{
	case "2v2": $type = 2; break;
	case "3v3": $type = 3; break;
	case "5v5": $type = 5; break;
}

if(!isset($page))
	$page = 1;

$start = ($page - 1) * $perPage;
$sumTeams = 0;
$numPages = 1;
$firstTeamNumber = $start+1;
$lastTeamNumber = $start+$perPage;

// Count the Teams
$rows = $CHDB->select("SELECT count(arenaTeamId) as `num` FROM arena_team WHERE TYPE = $type;");
foreach($rows as $row){
	$sumTeams = $row["num"];
}

$numPages = ceil($sumTeams / $perPage);


// Find the Teams
$rows = $CHDB->select("SELECT arenaTeamId, arena_team.name, arena_team.type, arena_team.captainGUID, seasonGames, seasonWins, arena_team.rating, characters.race
	FROM arena_team JOIN characters ON(captainGUID = characters.guid) 
	WHERE TYPE = $type ORDER BY rating DESC LIMIT $start, $perPage;");
$i = $start + 1;
foreach($rows as $row){
	$row["rank"] = $i;
	
	$row["css"] = "row".(($i % 2)+1);
	$row["faction"] = getFactionId($row["race"]);
	$row["factionLabel"] = ($row["faction"] == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	
	// Find old rank / save new rank
	$cacheRows = $DataDB->select('SELECT * FROM ranking_cache WHERE type = '.CACHETYPE_TEAMRANKING.' AND id = "'.$row["arenaTeamId"].'";');
	
	if(count($cacheRows))
	{
		$cache = $cacheRows[0];
		
		$row["lastweek_rank"] = $cache["lastweek_rank"];
		
		if($cache["rank"] != $row["rank"])
		{
			$DataDB->query('UPDATE ranking_cache 
			SET time = "'.time().'", rank = "'.$row["rank"].'"  
			WHERE cache_id = "'.$cache["cache_id"].'";');
		}	
	}
	else{
		$row["lastweek_rank"] = 0;
		$sql = 'INSERT INTO `ranking_cache` (`type`, `id`, `time`, `rank`, `lastweek_rank`) 
		VALUES ("'.CACHETYPE_TEAMRANKING.'", "'.$row["arenaTeamId"].'", "'.time().'", "'.$row["rank"].'", "0");';
		$DataDB->query($sql);
	}
	
	
	$arenaTeams[$row["arenaTeamId"]] = $row;

	$lastTeamNumber = $i;
	$i++;
}



// Find all characters
$charRows = $CHDB->select("
	SELECT characters.guid, characters.name, characters.class, arena_team_member.arenaTeamId, arena_team_member.seasonGames
		FROM characters JOIN arena_team_member ON(arena_team_member.guid = characters.guid) 
		WHERE arenaTeamId IN(".implode(", ", array_keys($arenaTeams)).") ORDER BY arena_team_member.seasonGames DESC;");

foreach($charRows as $charRow){
	$teamId = $charRow["arenaTeamId"];
	$team = $arenaTeams[$teamId];
	
	if(!isset($team["members"]))
		$team["members"] = array();
	
	// Only the first 2/3/5 are to be shown
	if( count($team["members"]) < $type )
		$team["members"][] = $charRow;
	
	// Cache Character
	checkCached("character", $charRow["guid"]);
	
	$arenaTeams[$teamId] = $team;
}


