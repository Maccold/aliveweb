<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title'=>$lang['players_online'],'link'=> url_for("server","playersonline"));
// ==================== //
 
if(false){ include("../../templates/Shattered-World/server/server.playersonline2.php"); }
$css_files[] = "/".$currtmp."/css/wiki.css";


if(isset($page))
	$pid = $page;
if (isset($_GET["pid"]) && !isset($pid)) {
	$pid = $_GET["pid"];
} 

$shattered = ($currtmp == "templates/Shattered-World") ? true : false;
	
$limit = (int)$MW->getConfig->generic->users_per_page;
$limitstart = ($pid - 1) * $limit;

$MANG = new Mangos;
$res_info = array();
$query = array();
$realm_info = get_realm_byid($user['cur_selected_realmd']);
$cc = 0;
  
echo "<!--"; 
echo "P: $pid, $page, $currtmp";
echo " -->";
  

if(check_port_status($realm_info['address'], $realm_info['port'])===true && $CHDB)
{
	if($shattered){
		$sql = "SELECT guid, name, race, class, gender, level, zone  FROM `characters` WHERE `online`='1' AND (NOT `extra_flags` & 1 AND NOT `extra_flags` & 16) ORDER BY `name`";	
	}
	else{
		$sql = "SELECT guid, name, race, class, gender, level, zone  FROM `characters` WHERE `online`='1' AND (NOT `extra_flags` & 1 AND NOT `extra_flags` & 16) ORDER BY `name` LIMIT $limitstart,$limit";
	}

	$sql_count = "SELECT count(guid) as `count`  FROM `characters` WHERE `online`='1' AND (NOT `extra_flags` & 1 AND NOT `extra_flags` & 16);";	

	$rows = $CHDB->select($sql_count);
	
	$sumPlayers = $rows[0]["count"];
	
	$pageCount = ceil((int)($rows[0]["count"]) / (int)$MW->getConfig->generic->users_per_page);
	
		
	$query = $CHDB->select($sql);
    

    foreach ($query as $result) {
        if($res_color==1)
			$res_color=2;
		else
			$res_color=1;
        $cc++;     
        $res_race = $MANG->characterInfoByID['character_race'][$result['race']];
        $res_class = $MANG->characterInfoByID['character_class'][$result['class']];
        $res_pos=$MANG->get_zone_name($result['zone']);

        $res_info[$cc]["number"] = $cc;
        $res_info[$cc]["res_color"] = $res_color;
        $res_info[$cc]["name"] = $result['name'];
        $res_info[$cc]["race"] = $result['race'];
        $res_info[$cc]["class"] = $result['class'];
        $res_info[$cc]["gender"] = $result['gender'];
        $res_info[$cc]["level"] = $result['level'];
        $res_info[$cc]["pos"] = $res_pos;
        $res_info[$cc]["guid"]=$result['guid'];
		
		$res_info[$cc]["classes"] = "row".$res_color." class-".$result['class'];
		if($result['level'] >= 80)
			$res_info[$cc]["classes"] .= " is-80";
		
		
    }
    unset($query); // Free up memory.
    unset($MANG);
}
else {
	output_message('alert','Realm <b>'.$realm_info['name'].'</b> is offline <img src="./templates/WotLK/images/downarrow2.gif" border="0" align="top">');
	exit;
}  

?>
