<?php
if(INCLUDED!==true)exit;

$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title'=>'Server Info', 'link' => url_for("server", "info"));


$css_files[] = "/".$currtmp."/css/account.css";

$init = 'id_'.$user['cur_selected_realmd'];
$config_details = getMangosConfig($MW->getConfig->mangos_conf_external->$init->mangos_world_conf);

$write_straight = array(
    'StartPlayerLevel' => 'Spieler Startlevel',
    'MaxPlayerLevel' => 'Max Charakter Level',
    'StartHeroicPlayerLevel' => 'Todesritter Startlevel',
    'StartPlayerMoney' => 'Startgeld',
    'MaxPrimaryTradeSkill' => 'Anzahl der erlaubten primären Berufe',
    'MinPetitionSigns' => 'Mitunterzeichner für eine Gildengründung',
);

$write_true_false = array(
    'AllowTwoSide.Accounts' => 'Erlaube Horde und Alliance Charaktere auf einem Account',
    'AllowTwoSide.Interaction.Chat' => 'Erlaube Chat zwischen Horde und Alliance',
    'AllowTwoSide.Interaction.Channel' => 'Erlaube Channels zwischen Horde und Alliance',
    'AllowTwoSide.Interaction.Group' => 'Erlaube Gruppen zwischen Horde und Alliance',
    'AllowTwoSide.Interaction.Guild' => 'Erlaube Gilden zwischen Horde und Alliance',
	'AllowTwoSide.Interaction.Auction'  => 'Erlaube Handel zwischen Horde und Alliance',
    'AllowTwoSide.WhoList' => 'Zeige Horde und Alliance unter /who list',
);

$write_blizzlike = array(
    'Rate.Health' => 'Gesundheit Erholungsrate',
    'Rate.Mana' => 'Mana Erholungsrate',
    'Rate.Rage.Income' => 'Wut Verlust- Erholungsrate',
    'Rate.RunicPower.Income' => 'Runen Verlust- Erholungsrate',
    'SkillGain.Crafting' => 'Skillpunkte Handwerk', 
    'SkillGain.Defense' =>  'Skillpunkte Verteidigung',
    'SkillGain.Gathering' => 'Skillpunkte Ernten/Abbauen',
    'SkillGain.Weapon' =>  'Skillpunkte Waffe',
    'Rate.Rest.InGame' => 'Erholung der Ruhezeit/Exp ingame',
    'Rate.Rest.Offline.InTavernOrCity' => 'Erholung der Ruhezeit/Exp in Städten',
    'Rate.Rest.Offline.InWilderness' => 'Erholung der Ruhezeit/Exp in der Natur',
    'Rate.Talent' => 'Talentpunkte',
    'Rate.Drop.Item.Poor' => 'Drop Rate Grau',
    'Rate.Drop.Item.Normal' => 'Drop Rate Weiss',
    'Rate.Drop.Item.Uncommon' => 'Drop Rate Grün',
    'Rate.Drop.Item.Rare' => 'Drop Rate Rar',
    'Rate.Drop.Item.Epic' => 'Drop Rate Episch',
    'Rate.Drop.Item.Legendary' => 'Drop Rate Legendär',
    'Rate.Drop.Item.Artifact' => 'Drop Rate Artefakte',
    'Rate.Drop.Item.Referenced' => 'Drop Rate Questitem',
    'Rate.Drop.Money' => 'Drop Rate Geld',
    'Rate.XP.Kill' => 'EP Level 0 - 69 Mobs töten',
    'Rate.XP.PastLevel70' => 'EP Level 70 - 80 Mobs töten',
    'Rate.XP.Quest' => 'Erfahrungspunkte für Quests',
    'Rate.XP.Explore' => 'Erfahrungspunkte Landkarte erforschen',
    'Rate.Honor' =>  'Rufpunkte bei den Fraktionen',
    'Rate.Creature.Normal.HP' => 'Lebenspunkte von Mobs, Elitemobs & Bossen',
    'Rate.Creature.Normal.Damage' => 'Schaden von Mobs, Elitemobs & Bossen',
);

$write_skillchances = array(
    'SkillChance.Orange' => 'Chance das Berufsskill steigt bei orangen Rezepten',
    'SkillChance.Yellow' => 'Chance das Berufsskill steigt bei gelben Rezepten',
    'SkillChance.Green'  => 'Chance das Berufsskill steigt bei grünen Rezepten',
    'SkillChance.Grey'   => 'Chance das Berufsskill steigt bei grauen Rezepten',
);

?>
