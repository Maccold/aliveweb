<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title' => "PvP", 'link' => url_for("server", "pvp") );
$pathway_info[] = array('title' => $lang['honor'], 'link' => url_for("server", "honor"));
// ==================== //


$css_files[] = "/".$currtmp."/css/pvp.css";


// some config //
$maxDisplayChars = 100; // Only top 40 in stats

$allianceKillers = $hordeKillers = array();

// Top 20 Kills - alliance characters 
$charRows = $CHDB->select("
	SELECT guid, name, class, race, gender, totalKills FROM characters WHERE race IN(".implode(", ",getAllianceFactions()).") ORDER BY totalKills DESC LIMIT 0,$maxDisplayChars;");
$i = 1;
foreach($charRows as $row)
{
	$row["css"] = "row".($i % 2);
	$allianceKillers[$i] = $row;
	$i++;
}

// Top 20 Kills - horde characters 
$charRows = $CHDB->select("
	SELECT guid, name, class, race, gender, totalKills FROM characters WHERE race IN(".implode(", ",getHordeFactions()).") ORDER BY totalKills DESC LIMIT 0,$maxDisplayChars;");
$i = 1;
foreach($charRows as $row)
{
	$row["css"] = "row".($i % 2);
	$hordeKillers[$i] = $row;
	$i++;
}