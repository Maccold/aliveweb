<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title' => "PvP", 'link' => url_for("server", "pvp") );
$pathway_info[] = array('title' => "Arena: $mode-Ladder", 'link' => url_for("server", "pvp-list", array("mode" => $mode)) );
// ==================== //


$css_files[] = "/".$currtmp."/css/pvp.css";
$css_files[] = "/".$currtmp."/css/profile.css";

if( empty($mode) || !in_array($mode, array("2v2","3v3","5v5")) )
	redirect(url_for("server", "pvp"));

if( empty($teamname) )
	redirect(url_for("server", "pvp"));
else
	$teamname = str_replace("+", " ",$teamname);

$teamChars = array();

$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/data" ) ;
		if ( $DataDB ){
			$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
		}

switch($mode)
{
	case "2v2": $type = 2; break;
	case "3v3": $type = 3; break;
	case "5v5": $type = 5; break;
}


// Find the Team
$rows = $CHDB->select('SELECT *	FROM arena_team WHERE name LIKE "'.$teamname.'";');


$team = $rows[0];
$teamId = $team["arenaTeamId"];
$team["factionLabel"] = "";
$team["members"] = array();

// Load cached ranking
$cache = $DataDB->select('SELECT * FROM ranking_cache WHERE id = "'.$teamId.'" AND type = '.CACHETYPE_TEAMRANKING.';');

if(!count($cache)){
	$team["rank"] = 0;
	$team["lastweek_rank"] = 0;
	$team["rankPage"] = 1;
}
else{
	$team["rank"] = $cache[0]["rank"];
	$team["lastweek_rank"] = $cache[0]["lastweek_rank"];
	$team["rankPage"] = ceil($team["rank"]/50);
}

	echo "<!-- ".print_r($team,true)." -->";

// Emblem
$team["backgroundColor"] = dechex($team["backgroundColor"]);
$team["borderColor"] = dechex($team["borderColor"]);
$team["emblemColor"] = dechex($team["emblemColor"]);

// Pathway & Title
$pathway_info[] = array('title' => $team["name"], 'link' => url_for("server", "arena", array("mode" => $mode, "teamname" => $team["name"])));
$page_title = $team["name"]. " @ " . $page_title;

// Find the Members
$rows = $CHDB->select("SELECT characters.guid,name,level,class,race, gender, 
	arena_team_member.weekGames, arena_team_member.weekWins, arena_team_member.seasonGames, arena_team_member.seasonWins, arena_team_member.personalRating 
	FROM arena_team_member JOIN characters ON(characters.guid = arena_team_member.guid) 
	WHERE arenaTeamId = ?d;", $teamId);



$i = 1;
foreach($rows as $row){
	
	$row["css"] = "row".($i % 2);
	$team["faction"] = getFactionId($row["race"]);
	
	$row["weekLosses"] = $row["weekGames"] - $row["weekWins"];
	$row["weekPercentage"] = round(($row["weekWins"] / $row["weekGames"]) * 100, 2);
	$row["weekAttendance"] = round(($row["weekGames"] / $team["weekGames"]) * 100, 2);
	
	$row["seasonLosses"] = $row["seasonGames"] - $row["seasonWins"];
	$row["seasonPercentage"] = round(($row["seasonWins"] / $row["seasonGames"]) * 100, 2);
	$row["seasonAttendance"] = round(($row["seasonGames"] / $team["seasonGames"]) * 100, 2);

	$team["members"][] = $row;
	$i++;
}

$team["weekLosses"] = $team["weekGames"] - $team["weekWins"];
$team["weekPercentage"] = round(($team["weekWins"] / $team["weekGames"]) * 100, 2);

$team["seasonLosses"] = $team["seasonGames"] - $team["seasonWins"];
$team["seasonPercentage"] = round(($team["seasonWins"] / $team["seasonGames"]) * 100, 2);

$team["factionLabel"] = ($team["faction"] == FACTION_ALLIANCE) ? "Allianz" : "Horde";
$team["css_faction"] = ($team["faction"] == FACTION_ALLIANCE) ? "alliance" : "horde";


