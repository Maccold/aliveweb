<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title' => "PvP", 'link' => url_for("server", "pvp") );
// ==================== //


$css_files[] = "/".$currtmp."/css/pvp.css";


// some config //
$max_display_chars = 40; // Only top 40 in stats
$MANG = new Mangos;

$arenaChars = array();
$arenaTeams = array();
$teams2v2 = $teams3v3 = $teams5v5 = $allianceKillers = $hordeKillers = array();

$css_classes = array(
	1 => "first",
	2 => "second",
	3 => "third",
);

// Find Top 3 2v2 Teams
$rows = $CHDB->select("SELECT arenaTeamId, arena_team.name, arena_team.type, arena_team.captainGUID, arena_team.rating, characters.race
	FROM arena_team JOIN characters ON(captainGUID = characters.guid) 
	WHERE TYPE = 2 ORDER BY rating DESC LIMIT 0,3");
$i = 1;
foreach($rows as $row){
	$row["faction"] = (getFactionId($row["race"]) == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	$arenaTeams[$row["arenaTeamId"]] = $row;
	$teams2v2[$i] = $row["arenaTeamId"];
	$i++;
}

// Find Top 3 3v3 Teams
$rows = $CHDB->select("SELECT arenaTeamId, arena_team.name, arena_team.type, arena_team.captainGUID, arena_team.rating, characters.race
	FROM arena_team JOIN characters ON(captainGUID = characters.guid) 
	WHERE TYPE = 3 ORDER BY rating DESC LIMIT 0,3;");
$i = 1;
foreach($rows as $row){
	$row["faction"] = (getFactionId($row["race"]) == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	$arenaTeams[$row["arenaTeamId"]] = $row;
	$teams3v3[$i] = $row["arenaTeamId"];
	$i++;
}

// Find Top 3 5v5 Teams
$rows = $CHDB->select("SELECT arenaTeamId, arena_team.name, arena_team.type, arena_team.captainGUID, arena_team.rating, characters.race
	FROM arena_team JOIN characters ON(captainGUID = characters.guid) 
	WHERE TYPE = 5 ORDER BY rating DESC LIMIT 0,3;");
$i = 1;
foreach($rows as $row){
	$row["faction"] = (getFactionId($row["race"]) == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	$arenaTeams[$row["arenaTeamId"]] = $row;
	$teams5v5[$i] = $row["arenaTeamId"];
	$i++;
}

// Find all characters
$charRows = $CHDB->select("
	SELECT characters.guid, characters.name, characters.class, arena_team_member.arenaTeamId, arena_team_member.seasonGames
		FROM characters JOIN arena_team_member ON(arena_team_member.guid = characters.guid) 
		WHERE arenaTeamId IN(".implode(", ", array_keys($arenaTeams)).") ORDER BY arena_team_member.seasonGames DESC;");

foreach($charRows as $charRow){
	$teamId = $charRow["arenaTeamId"];
	$team = $arenaTeams[$teamId];
	
	if(!isset($team["members"]))
		$team["members"] = array();
	
	// Only the first 2/3/5 are to be shown
	if( count($team["members"]) < $team["type"] )
		$team["members"][] = $charRow;
	
	//$team["members"][] = $charRow;
	$arenaTeams[$teamId] = $team;
}

// Top 20 Kills - alliance characters 
$charRows = $CHDB->select("
	SELECT guid, name, class, race, gender, totalKills FROM characters WHERE race IN(".implode(", ",getAllianceFactions()).") ORDER BY totalKills DESC LIMIT 0,20;");
$i = 1;
foreach($charRows as $row)
{
	$row["css"] = "row".(($i % 2)+1);
	$allianceKillers[$i] = $row;
	$i++;
}

// Top 20 Kills - horde characters 
$charRows = $CHDB->select("
	SELECT guid, name, class, race, gender, totalKills FROM characters WHERE race IN(".implode(", ",getHordeFactions()).") ORDER BY totalKills DESC LIMIT 0,20;");
$i = 1;
foreach($charRows as $row)
{
	$row["css"] = "row".(($i % 2)+1);
	$hordeKillers[$i] = $row;
	$i++;
}
