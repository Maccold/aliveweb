<?php
if(INCLUDED!==true) exit;
// ==================== //
$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title'=>$lang['realms_status'],'link'=>'');
// ==================== //




// Important! This assigns a connection to the spesific connection we have.. NOT remove this!
$WSDB_EXTRA = DbSimple_Generic::connect("".$mangosALL['db_type']."://".$mangosALL['db_username'].":".$mangosALL['db_password']."@".$mangosALL['db_host'].":".$mangosALL['db_port']."/".$mangosALL['db_name']."");

if($WSDB_EXTRA)
	$WSDB_EXTRA->query("SET NAMES ".$mangosALL['db_encoding']);

$realms = array();
$rows = $DB->select("SELECT * FROM `realmlist` WHERE `id` > 0 AND id < 4 ORDER BY `name`");
$i = 0;

foreach($rows as $i => $result)
{
	if((int)$MW->getConfig->generic->use_local_ip_port_test) {
		$result['address'] = "127.0.0.1";
	}
	
	$realm_id = $result["id"];
	
	$population = 0;
	$res_color = ($res_color == 1) ? 2 : 1;
	
	$realm_type = $realm_type_def[$result['icon']];
	
	$dbinfo_mangos = explode(';', $result['dbinfo']);  // username;password;port;host;DBName

	if((int)$MW->getConfig->generic->use_archaeic_dbinfo_format) {
		//alternate config - for users upgrading from Modded MaNGOS Web
		//DBinfo column:  host;port;username;password;WorldDBname;CharDBname
	
		$mangosALL = array(
			'db_type'     => 'mysql',
			'db_host'     => $dbinfo_mangos['0'], //ip of db world
			'db_port'     => $dbinfo_mangos['1'], //port
			'db_username' => $dbinfo_mangos['2'], //world user
			'db_password' => $dbinfo_mangos['3'], //world password
			'db_name'     => $dbinfo_mangos['4'], //world db name
			'db_char'     => $dbinfo_mangos['5'], //character db name
			'db_encoding' => 'utf8',              // don't change
		);
	}
	else {
		//normal config, as outlined in how-to
		//DBinfo column:  username;password;port;host;WorldDBname;CharDBname
		$mangosALL = array(
			'db_type'     => 'mysql',
			'db_host'     => $dbinfo_mangos['3'], //ip of db world
			'db_port'     => $dbinfo_mangos['2'], //port
			'db_username' => $dbinfo_mangos['0'], //world user
			'db_password' => $dbinfo_mangos['1'], //world password
			'db_name'     => $dbinfo_mangos['4'], //world db name
			'db_char'     => $dbinfo_mangos['5'], //character db name
			'db_encoding' => 'utf8',              // don't change
		);
	}
	
	if((int)$MW->getConfig->generic->use_alternate_mangosdb_port) {
		$mangosALL['db_port'] = (int)$MW->getConfig->generic->use_alternate_mangosdb_port;
	}
	
	if(check_port_status($result['address'], $result['port'])===true)
	{
		$res_img = '/templates/WotLK/images/uparrow2.gif';
		$CHDB_EXTRA = DbSimple_Generic::connect("".$mangosALL['db_type']."://".$mangosALL['db_username'].":".$mangosALL['db_password']."@".$mangosALL['db_host'].":".$mangosALL['db_port']."/".$mangosALL['db_char']."");
		
		if( $CHDB_EXTRA ) 
		{
			$population = $CHDB_EXTRA->selectCell("SELECT count(*) FROM `characters` WHERE online = 1;");
			$uptime = time () - $DB->selectCell("select starttime FROM uptime WHERE realmid = ?d ORDER BY starttime DESC LIMIT 1;", $realm_id);
		}
	}
	else
	{
		$res_img = '/templates/WotLK/images/downarrow2.gif';
		$population_str = 'n/a';
		$uptime = 0;
	}
	
	$realms[$realm_id]['res_color'] = $res_color;
	$realms[$realm_id]['img'] = $res_img;
	$realms[$realm_id]['name'] = $result['name'];
	$realms[$realm_id]['type'] = "PvE/P";
	$realms[$realm_id]['pop'] = $population;
	$realms[$realm_id]['uptime'] = $uptime;
	unset($CHDB_EXTRA);
}

$up = '<img src="'.$currtmp.'/images/uparrow2.gif" style="vertical-align: bottom;" height="19" width="18" alt=""/> <b style="color: rgb(35, 67, 3);">' . $lang['up'] . '</b>';
$down = '<img src="'.$currtmp.'/images/downarrow2.gif" style="vertical-align: bottom;" height="19" width="18" alt=""/> <b style="color: rgb(102, 13, 2);">' . $lang['down'] . '</b>';
/** @todo: make forum link point to a configured realm status forum or external forum */
$realmstatusforum = '<a href="index.php?n=forum">' . $lang['realm_status_forum'] . '</a>';
$desc = $lang['realmstatus_desc'];
$desc = str_replace('[up]', $up, $desc);
$desc = str_replace('[down]', $down, $desc);
$desc = str_replace('[realm_status_forum]', $realmstatusforum, $desc);


