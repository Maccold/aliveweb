<?php
if(INCLUDED!==true)exit;

$css_files[] = "/".$currtmp."/css/account.css";
$pathway_info[] = array( 'title' => "Server", 'link'=> url_for("server"));
$pathway_info[] = array( 'title' => $lang['ta'], 'link' => url_for("server", "ta"));

$content = lang_resource('transferanleitung.html');

$account_sidebar = "";
if($userObject->theme == "Shattered-World"){
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/account/account.sidebar.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$account_sidebar = ob_get_contents();
	ob_end_clean();

}
?>
