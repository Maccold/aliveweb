<?php
if(INCLUDED!==true)exit;

include($_SERVER['DOCUMENT_ROOT']."/core/helpers.php");

$pathway_info[] = array('title' => 'Server','link'=>url_for("server"));
$pathway_info[] = array('title'=>'Bugtracker','link'=>url_for("server", "bugtracker"));

$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$changelogDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/changelog" ) ;
if ( $changelogDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$css_files[] = "/".$currtmp."/css/account.css";
$css_files[] = "/".$currtmp."/css/wiki.css";
$css_files[] = "/".$currtmp."/css/roadmap.css";

if(!$userObject->isLoggedIn()){
	$requiresLogin = true;
	return;
}

/*
 * Page Cache
 */
$useDbCache = true;

$cacheType = "server-roadmap";
$dbCacheData = array(
	"type" => CACHETYPE_PAGE,
	"page" => $cacheType,
	"content" => "",
	"refresh" => true,
);

$cacheRow = $DataDB->selectRow("SELECT * FROM page_cache WHERE page = ?;", $cacheType);

if($cacheRow){
	$diff = time() - $cacheRow["timestamp"];
	$cacheTime = 60*60*24;	// 1 day
	if($diff < $cacheTime){
		$dbCacheData["refresh"] = false;
		$dbCacheData["content"] = $cacheRow["html"];
	}
}

/*
 * L I S T
 */
		
$bugRows = $DataDB->select("
	SELECT `id`, `class`, `title`, `state`, `date` as `createdDate`, `date2` as `changedDate`, `createdTimestamp`, `changedTimestamp` 
		FROM bug WHERE state IN('Erledigt','Abgewiesen') ORDER BY `changedTimestamp` DESC");

$rows = array();
$rowclass = "row1";
$rowCount = count($bugRows);
$years = array();

foreach($bugRows as $i => $row){
	//$row["title"] = "Bug #".$row["id"]." ".$row["class"]." - ".htmlentities($row["title"])." - Erledigt";
	
	$num = "#".$row["id"];
	while(strlen($num) < 4){
		$num .= " ";
	}
	$row["num"] = str_replace(" ", "&nbsp;", $num);
	
	if($row["changedTimestamp"] == 0){
		$date = explode(".",$row["changedDate"]);
		$date = $date[2]."-".$date[1]."-".$date[0];
		$date = strtotime($date);
	}
	else{
		$date = $row["changedTimestamp"];	
	}
	
	$week = strftime("%W", $date);
	$year = strftime("%Y", $date);
	
	if($year < 2012){
		$year = 2011;
	}
	switch($row["state"]){
		case "Erledigt":
			$row["cssState"] = "color-tooltip-green"; break;
		case "Abgewiesen":
			$row["cssState"] = "color-q0"; break;
	}
	
	$row["text"] = '<span><a href="/server/bugtracker/bug/'.$row["id"].'/">Bug '.$row["num"].'</a>&nbsp;'.$row["class"].'</span> - '.htmlentities($row["title"]).' - <span class="'.$row["cssState"].'">'.$row["state"].'</span>';
	
	if(!isset($years[$year]))
		$years[$year] = array();
	if(!isset($years[$year][$week]))
		$years[$year][$week] = array();
	$years[$year][$week][] = $row;
	
}

// Alte Revs einspielen.
$revRows = $changelogDB->select("SELECT * FROM data WHERE Author <> 'ADB-Team' ORDER BY `Rev`");
/*
 * INSERT INTO `data` (`Rev`, `Message`, `Date`, `Author`) VALUES 
   ('$Rev_sec', 'Fixx - $kategorie - $title', '$datum_sec', 'ADB-Team'*/

foreach($revRows as $row){
	$date = strtotime($row["Date"]);
	$week = strftime("%W", $date);
	$year = strftime("%Y", $date);
	$row["text"] = $row["Message"];

	if(!isset($years[$year]))
		$years[$year] = array();
	if(!isset($years[$year][$week]))
		$years[$year][$week] = array();
	$years[$year][$week][] = $row;
}

foreach($years as $year => $rows){
	
	krsort($years[$year]);
	
}

