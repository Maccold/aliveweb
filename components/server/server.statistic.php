<?php
if(INCLUDED!==true)exit;

$pathway_info[] = array('title' => "Server", 'link' => url_for("server"));
$pathway_info[] = array('title' => $lang['statistic'], 'link' => url_for("server", "statistic"));


//initialize $num_chars variable
$num_chars = 0;

$rows = $CHDB->select("SELECT race, count(race) as `num` FROM `characters` WHERE 1 GROUP BY race;") ;
	
//$realm_param = $user['cur_selected_realmd'];

$races = array();
foreach($rows as $data)
{
    $num_chars += $data["num"];
	$races[$data["race"]] = $data["num"];
}

echo "<!-- ";
	print_r($races);
echo " -->";

// Loop thru classes, add 0 if its not defined in array.
for($i = 1; $i <= 11; $i++)
	if (!isset($races[$i]))
		$races[$i] = 0;

if ($num_chars > 0){
	$num_ally = $races[1]+$races[3]+$races[4]+$races[7]+$races[11];
	$num_horde = $races[2]+$races[5]+$races[6]+$races[8]+$races[10];
	$pc_ally =  round($num_ally/$num_chars*100,2);
	$pc_horde =  round($num_horde/$num_chars*100,2);
	$pc_human = round($races[1]/$num_chars*100,2);
	$pc_oraces = round($races[2]/$num_chars*100,2);
	$pc_dwarf = round($races[3]/$num_chars*100,2);
	$pc_ne = round($races[4]/$num_chars*100,2);
	$pc_undead = round($races[5]/$num_chars*100,2);
	$pc_tauren = round($races[6]/$num_chars*100,2);
	$pc_gnome = round($races[7]/$num_chars*100,2);
	$pc_troll = round($races[8]/$num_chars*100,2);
	$pc_be = round($races[10]/$num_chars*100,2);
	$pc_dranei = round($races[11]/$num_chars*100,2);
}


