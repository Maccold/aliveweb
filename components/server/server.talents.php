<?php
if(INCLUDED!==true)exit;

$pathway_info[] = array('title'=>$lang['talents'],'link'=>'');

$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$classimagespath = 'components/talents/class-images/';
$classlinkpath = 'components/talents/';

if(empty($class)){
	$req_tpl = false;
	return;
}



$noBody = true;




if(!isset($glyph)){
	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/core/data/talent-data/3.3.5/class-".$class.".js")){
		include($_SERVER["DOCUMENT_ROOT"]."/core/data/talent-data/3.3.5/class-".$class.".js");
	}
	
}
else{
	$glyphRows = $WSDB->select("SELECT entry,displayid,name,class,spellid_1 as spell FROM item_template where class = 16 AND subclass = ?d", $class);

	$glyphs = array();


	/* Key: baseSpell */
	$glyphData = array();

	/* Key: baseSpell => Glyph ID */
	$baseSpellToGlyph = array();
	$glyphBaseSpells = array();

	/* Item Daten, Key: baseSpell */
	$glyphItems = array();

	$spells = array();

	$displayIds = array();

	foreach($glyphRows as $row){
		$baseSpell = $row["spell"];
		$glyphBaseSpells[] = $baseSpell;
		$displayIds[$row["displayid"]] = "ability_seal";
	}

	// Finde alle Glyph Ids zu den baseSpells
	$spellRows = $aDB->select("SELECT id, EffectMiscValue_1 FROM armory_spell WHERE id IN (?a)", $glyphBaseSpells);

	foreach($spellRows as $row){
		$baseSpellId = $row["id"];
		$baseSpellToGlyph[$baseSpellId] = $row["EffectMiscValue_1"];
		$glyphData[$baseSpellId] = array(
		"glyphId" => $row["EffectMiscValue_1"],
		);
	}

	// Finde spell description und glyph spell zu allen Glyphen
	$glyphPropRows = $aDB->select("SELECT id, spell, description_en_gb FROM armory_glyphproperties WHERE id IN (?a)", $baseSpellToGlyph);

	foreach($glyphPropRows as $row){
		$glyphId = $row["id"];
		$baseSpellId = array_search($glyphId, $baseSpellToGlyph);
		$spellId = $row["spell"];

		$desc = $row["description_en_gb"];
		$wowhead = file_get_contents("http://de.wowhead.com/spell=".$spellId."&power");

		if(preg_match("@<span class=\"q\">([^<]+)</span>@", $wowhead, $matches)){
			$desc = $matches[1];
		}

		$glyphData[$baseSpellId]["spell"] = $spellId;
		$glyphData[$baseSpellId]["desc"] = $desc;

	}

	$iconRows = $aDB->select("SELECT * FROM armory_icons WHERE displayid IN (?a)", array_keys($displayIds));

	foreach($iconRows as $row){
		$displayIds[$row["displayid"]] = $row["icon"];
	}
	//debug($spells);
	
	foreach($glyphRows as $glyph){

		$baseSpellId = $glyph["spell"];
		$glyphId = $baseSpellToGlyph[$baseSpellId];
		
		$icon = $displayIds[$glyph["displayid"]];
		$data = $glyphData[$baseSpellId];
		
		if(substr_count($icon, "major") > 0)
			$type = 0;
        else
			$type = 1;
		
			
		$glyphs[$glyphId] = array(
			"name" => $glyph["name"],
			"id" => $glyphId,
			"type" => $type,
			"description" => utf8_decode($data["desc"]),
			"icon" => $icon,
			"itemId" => $glyph["entry"],
			"spellKey" => $data["spell"],
			"spellId" => $data["spell"],
			"prettyName" => "",
			"typeOrder" => 2,
		);
	}
	$json_glyph = json_encode($glyphs);
	//debug($glyphs);
	echo $json_glyph;
}
/*
{
            "name": "Glyphe 'Richturteil'",
            "id": 183,
            "type": 2,
            "description": "Euer Zauber 'Richturteil' verursacht 10% mehr Schaden.",
            "icon": "spell_holy_righteousfury",
            "itemId": 41092,
            "spellKey": 54922,
            "spellId": 54922,
            "prettyName": "Richturteil",
            "typeOrder": 2
        }, 
*/