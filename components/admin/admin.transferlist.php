<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>'Transferliste', 'link'=>url_for("admin", "transferlist"));
// ==================== //

$TransferDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
	":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
	"/" . "chartrans" . "" ) ;
if ( $TransferDB ){
	$TransferDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$TransferDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}
else{
	echo "Es konnte keine Verbindung zur Transferdatenbank hergestellt werden.";
	exit;	
}


// Fake include for dreamweaver..
if(false){ include("../../templates/Shattered-World/admin/admin.transferlist.php"); }
$css_files[] = "/".$currtmp."/css/wiki.css";

if($archive){
	// Archiv Count
	$sql_count = 'SELECT count(`id`) AS count FROM formular_basis WHERE `id` <= 2000;';
	
	$sql = 'SELECT `id`, `name`, `server`, `datum`, `gm` FROM formular_basis WHERE `id` <= 2000 ORDER BY `id` DESC;';
}
else {
	// Count
	$sql_count = 'SELECT count(`id`) AS count FROM formular_basis WHERE `id` > 2000;';
	
	$sql = 'SELECT `id`, `name`, `server`, `datum`, `gm` FROM formular_basis WHERE `id` > 2000 ORDER BY `id` DESC;';
}



$countRows = $TransferDB->select($sql_count);
$basisRows = $TransferDB->select($sql);
$transferCount = 0;

foreach($countRows as $row)
	$transferCount = $row["count"];


$transferRows = array();
$rowclass = "row2";
foreach($basisRows as $basis){
	$state = 0;
	$classes = array();
	
	$rowclass = ($rowclass == "row1") ? "row2" : "row1";
	$classes[] = $rowclass;
	
	
	if(substr_count($basis["gm"], "Erledigt von") > 0){
		$state = 2;
		$classes[] = "done";
	}
	elseif(substr_count($basis["gm"], "Ist in Bearbeitung") > 0){
		$state = 1;
		$classes[] = "inprogress";
	}
	elseif(substr_count($basis["gm"], "Gel") > 0){
		$state = 3;
		$classes[] = "deleted";
		$classes[] = "disabled";
	}
	
	$basis["classes"] = implode(" ", $classes);
	$basis["status_id"] = $state;
	$basis["detail_link"] = url_for("admin", "transfer-detail", array("id" => $basis["id"]));
	
	$transferRows[$basis["id"]] = $basis;
	
}

?>
