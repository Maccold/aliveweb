<?php

$css_files[] = "/".$currtmp."/css/admin.css";
$pathway_info[] = array( 'title' => "GameMaster", 'link'=> url_for("admin"));

$fa = "index.php?n=forum&sub=post&action=newtopic&f=".(int)$MW->getConfig->generic_values->forum->news_forum_id;
$fn = "index.php?n=forum&sub=viewforum&fid=".(int)$MW->getConfig->generic_values->forum->news_forum_id;
if (!isset($commands_forum_id))$commands_forum_id='';
$fc = "index.php?n=forum&sub=viewforum&fid=".$commands_forum_id;

$com_content['admin'] = array(
    'index' => array(
        'g_is_gm', // g_ option require for view     [0]
        'admin_panel', // loc name (key)                [1]
        url_for('admin'), // Link to                 [2]
        '', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),
    'members' => array(
        'g_is_gm', 
        'users_manage', 
        url_for("admin", "members"),
        '',
        1
    ),
    'config' => array(
        'g_is_supadmin', 
        'site_config', 
        url_for("admin", "config"),
        '',
        1
    ),
    'realms' => array(
        'g_is_supadmin', 
        'realms_manage', 
        url_for("admin", "realms"),
        '',
        1
    ),
    'forum' => array(
        'g_is_supadmin',  
        'forums_manage', 
        url_for("admin", "forum"),
        '',
        1
    ),
    'keys' => array(
        'g_is_supadmin',  
        'regkeys_manage', 
        url_for("admin", "keys"),
        '',
        1
    ),
    'langs' => array(
        'g_is_supadmin',  
        'langs_manage', 
        url_for("admin", "langs"),
        '',
        1
    ),
     'news_add' => array(
        'g_is_supadmin',  
        'news_add', 
        $fa,
        '',
        1
    ),
    'news' => array(
        'g_is_supadmin', 
        'news_manage', 
        $fn,
        '',
        1
    ),
    'commands' => array(
        'g_is_supadmin',  
        'commands_manage', 
        $fc,
        '',
        1
    ),
    'chat' => array(
        'g_is_supadmin',  
        'chat_manage', 
        url_for("admin", "chat"),
        '',
        0
    ),
    'donate' => array(
        'g_is_supadmin', 
        'donate',
        url_for("admin", "donate"),
        '',
        1
    ),
    'backup' => array(
        'g_is_supadmin', 
        'backup',
        url_for("admin", "backup"),
        '',
        1
    ),
    'viewlogs' => array(
        'g_is_supadmin', 
        'viewlogs',
        url_for("admin", "viewlogs"),
        '',
        0
    ),
    'chartools' => array(
        'g_is_supadmin', 
        'chartransfer',
        url_for("admin", "chartools"),
        '',
        0
    ),
    'tickets' => array(
        'g_is_gm',
        'tickets',
        url_for("admin", "tickets"),
        'tickets',
        0
    ),
    'chartransfer' => array(
        'g_is_supadmin', 
        'chartransfer',
        url_for("admin", "chartransfer"),
        '',
        0
    ),
    'transfscriptgm' => array(
        'g_is_admin', 
        'transfscriptgm',
        url_for("admin", "transfscriptgm"),
        '',
        0
    ),
    'gmview' => array(
        'g_is_admin', 
        'gmview',
        url_for("admin", "gmview"),
        '',
        0
    ),
    'clin' => array(
        'g_is_admin', 
        'cl',
        url_for("admin", "clin"),
        '',
        0
    ),
    'members2' => array(
        'g_is_supadmin', 
        'users_manage', 
        url_for("admin", "members2"),
        '',
        1
    ),
    'sh' => array(
        'g_is_admin', 
        'sh',
        url_for("admin", "sh"),
        '',
        0
    ),
    'fwfv' => array(
        'g_is_admin', 
        'fwfv',
        url_for("admin", "fwfv"),
        '',
        0
    ),
    'chathandler' => array(
        'g_is_admin', 
        'chathandler',
        url_for("admin", "chathandler"),
        '',
        0
    ),
    'transferlist' => array('g_is_admin', 'Transferliste', url_for("admin", "transferlist"), '', 0),
    'transfer-detail' => array(
        'g_is_admin', 
        'Transfer Detail',
        url_for("admin", "transfer-detail"),
        '',
        0
    ),
);
?>
