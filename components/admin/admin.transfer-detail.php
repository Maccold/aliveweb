<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>'Transfer Detail', 'link'=> url_for("admin","transfer-detail"));
// ==================== //

$TransferDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
	":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
	"/" . "chartrans" . "" ) ;
if ( $TransferDB ){
	$TransferDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$TransferDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}
else{
	echo "Es konnte keine Verbindung zur Transferdatenbank hergestellt werden.";
	exit;	
}

$transferId = mysql_escape_string($id);

if(!isset($transferId) || $transferId <= 0 || !is_numeric($transferId)){
	echo "Ung&uuml;ltige Transfer ID";
	exit;	
}

// Find the transfer
$sql = 'SELECT * FROM formular_basis WHERE id = '.$transferId.';';
$rows = $TransferDB->select($sql);

if(!$basedata = $rows[0]){
	echo "Diesen Transfer scheint es nicht zu geben.";
	exit;	
}

// Change the status of the transfer
$transferAction = isset($_REQUEST['transfer-status']) ? $_REQUEST['transfer-status'] : "";
$text = isset($_POST['text']) ? mysql_escape_string($_POST['text']) : "";
$message = "";

if($transferAction == "done"){
	$text = "Erledigt von ".$text;
}
elseif($transferAction == "inprogress"){
	$text = "Ist in Bearbeitung von ".$text;
}
elseif($transferAction == "delete"){
	$text = "Geloescht von ".$text;
}

if( in_array($transferAction, array("done", "inprogress", "delete")) ){
	$TransferDB->query("UPDATE formular_basis SET `gm`= '".$text."' WHERE `id`=".$transferId.";");

	$message = 'Danke fuer die Bearbeitung des Charakter Transfers ';
	
	$basedata["gm"] = $text;
}


// Find existing transfers
$other_transfers = array();
$sql = 'SELECT * FROM formular_basis WHERE name = '.$basedata["name"].';';
$rows = $TransferDB->select($sql);
foreach($rows as $row){
	$row["gm"] = str_replace("Ã¶","ö", $row["gm"]);
	$other_transfers[] = $row;
}
$transferCount = count($rows);

/* 
 * Berufe
 */


//Reiten
	if($basedata['reiten'] == "75")
		$reitenspell = '33388';
	if($basedata['reiten'] == "150")
		$reitenspell = '33391';
	if($basedata['reiten'] == "225")
		$reitenspell = '34090';
	if($basedata['reiten'] == "300")
		$reitenspell = '34091';
	if($basedata['reiten'] == "300 + Kaltwetterflug")
		$reitenspell = "34091 \n.learn 54197";

	if($basedata['beruf1skill'] <= "75")
		$beruf1maxskill = 75;
	elseif($basedata['beruf1skill'] <= "150")
		$beruf1maxskill = 150;
	elseif($basedata['beruf1skill'] <= "225")
		$beruf1maxskill = 225;
	elseif($basedata['beruf1skill'] <= "300")
		$beruf1maxskill = 300;
	elseif($basedata['beruf1skill'] <= "375")
		$beruf1maxskill = 375;
	elseif($basedata['beruf1skill'] <= "450")
		$beruf1maxskill = 450;
	//beruf2
	if($basedata['beruf2skill'] <= "75")
		$beruf2maxskill = 75;
	elseif($basedata['beruf2skill'] <= "150")
		$beruf2maxskill = 150;
	elseif($basedata['beruf2skill'] <= "225")
		$beruf2maxskill = 225;
	elseif($basedata['beruf2skill'] <= "300")
		$beruf2maxskill = 300;
	elseif($basedata['beruf2skill'] <= "375")
		$beruf2maxskill = 375;
	elseif($basedata['beruf2skill'] <= "450")
		$beruf2maxskill = 450;
	
		
	//kochen
	if($basedata['kochen'] <= "75")
		$kochenmaxskill = 75;
	elseif($basedata['kochen'] <= "150")
		$kochenmaxskill = 150;
	elseif($basedata['kochen'] <= "225")
		$kochenmaxskill = 225;
	elseif($basedata['kochen'] <= "300")
		$kochenmaxskill = 300;
	elseif($basedata['kochen'] <= "375")
		$kochenmaxskill = 375;
	elseif($basedata['kochen'] <= "450")
		$kochenmaxskill = 450;
	//angeln
	if($basedata['angeln'] <= "75")
		$angelnmaxskill = 75;
	elseif($basedata['angeln'] <= "150")
		$angelnmaxskill = 150;
	elseif($basedata['angeln'] <= "225")
		$angelnmaxskill = 225;
	elseif($basedata['angeln'] <= "300")
		$angelnmaxskill = 300;
	elseif($basedata['angeln'] <= "375")
		$angelnmaxskill = 375;
	elseif($basedata['angeln'] <= "450")
		$angelnmaxskill = 450;
	//erste hilfe
	if($basedata['erstehilfe'] <= "75")
		$hilfemaxskill = 75;
	elseif($basedata['erstehilfe'] <= "150")
		$hilfemaxskill = 150;
	elseif($basedata['erstehilfe'] <= "225")
		$hilfemaxskill = 225;
	elseif($basedata['erstehilfe'] <= "300")
		$hilfemaxskill = 300;
	elseif($basedata['erstehilfe'] <= "375")
		$hilfemaxskill = 375;
	elseif($basedata['erstehilfe'] <= "450")
		$hilfemaxskill = 450;
	//beruf1spell
	if($basedata['beruf1'] == "Schmiedekunst") {
		$beruf1 = 2018;
		$beruf1skill = 164;
	}
	if($basedata['beruf1'] == "Verzauberungskunst") {
		$beruf1 = 7411;
		$beruf1skill = 333;
	}
	if($basedata['beruf1'] == "Ingeneurskunst") {
		$beruf1 = 4036;
		$beruf1skill = 202;
	}
	if($basedata['beruf1'] == "Kraeutersammeln") {
		$beruf1 = 2366;	
		$beruf1skill = 182;
	}
	if($basedata['beruf1'] == "Juwelenschleifen") {
		$beruf1 = 25229;
		$beruf1skill = 755;
	}
	if($basedata['beruf1'] == "Lederer") {
		$beruf1 = 2108;	
		$beruf1skill = 165;
	}
	if($basedata['beruf1'] == "Bergbau") {
		$beruf1 = 2575;	
		$beruf1skill = 186;
	}
	if($basedata['beruf1'] == "Kuerschnerei") {
		$beruf1 = 8613;	
		$beruf1skill = 393;
	}
	if($basedata['beruf1'] == "Schneiderei") {
		$beruf1 = 3908;	
		$beruf1skill = 197;
	}
	if($basedata['beruf1'] == "Inschriftenkunde") {
		$beruf1 = 45357;	
		$beruf1skill = 773;
	}
	if($basedata['beruf1'] == "Alchemie") {
		$beruf1 = 2259;	
		$beruf1skill = 171;
	}
	//beruf2spell
	if($basedata['beruf2'] == "Schmiedekunst") {
		$beruf2 = 2018;
		$beruf2skill = 164;
	}
	if($basedata['beruf2'] == "Verzauberungskunst") {
		$beruf2 = 7411;
		$beruf2skill = 333;
	}
	if($basedata['beruf2'] == "Ingeneurskunst") {
		$beruf2 = 4036;
		$beruf2skill = 202;
	}
	if($basedata['beruf2'] == "Kraeutersammeln") {
		$beruf2 = 2366;	
		$beruf2skill = 182;
	}
	if($basedata['beruf2'] == "Juwelenschleifen") {
		$beruf2 = 25229;
		$beruf2skill = 755 ;
	}
	if($basedata['beruf2'] == "Lederer") {
		$beruf2 = 2108;	
		$beruf2skill = 165;
	}
	if($basedata['beruf2'] == "Bergbau") {
		$beruf2 = 2575;	
		$beruf2skill = 186;
	}
	if($basedata['beruf2'] == "Kuerschnerei") {
		$beruf2 = 8613;	
		$beruf2skill = 393;
	}
	if($basedata['beruf2'] == "Schneiderei") {
		$beruf2 = 3908;	
		$beruf2skill = 197;
	}
	if($basedata['beruf2'] == "Inschriftenkunde") {
		$beruf2 = 45357;	
		$beruf2skill = 773;
	}
	if($basedata['beruf2'] == "Alchemie") {
		$beruf2 = 2259;	
		$beruf2skill = 171;
	}
	
	$juweMax = false;
	$vzMax = false;
	$ikMax = false;
	$lederMax = false;
	$kochMax = false;
	
	if(($basedata['beruf1'] == "Juwelenschleifen" && $basedata['beruf1skill'] >= 450) || 
		($basedata['beruf2'] == "Juwelenschleifen" && $basedata['beruf2skill'] >= 450) ){
		$juweMax = true;
	}
	if(($basedata['beruf1'] == "Verzauberungskunst" && $basedata['beruf1skill'] >= 450) || 
		($basedata['beruf2'] == "Verzauberungskunst" && $basedata['beruf2skill'] >= 450) ){
		$vzMax = true;
	}
	if(($basedata['beruf1'] == "Inschriftenkunde" && $basedata['beruf1skill'] >= 450) || 
		($basedata['beruf2'] == "Inschriftenkunde" && $basedata['beruf2skill'] >= 450) ){
		$ikMax = true;
	}
	if(($basedata['beruf1'] == "Lederer" && $basedata['beruf1skill'] >= 450) || 
		($basedata['beruf2'] == "Lederer" && $basedata['beruf2skill'] >= 450) ){
		$lederMax = true;
	}
	if( ($basedata['kochen'] >= 450) ){
		$kochMax = true;
	}

/*
 * Items
 */

$itemRows = $TransferDB->select("SELECT * FROM formular_item WHERE `id` = '$transferId';");

$items = $itemRows[0];

$slots = array('kopf', 'hals', 'schulter', 'ruecken', 'brust', 'wappenrock', 'handgelenke', 'haende', 'taille', 'beine', 'fuesse', 'ring1', 'ring2', 'schmuck1', 'schmuck2', 'waffenhand', 'nebenhand', 'distanzwaffe');

$itemIds = array();
foreach($slots as $slot){
	$itemIds[$slot] = $items[$slot];
}

// Get Itemlevels
$itemLevelRows = $WSDB->select("SELECT entry, ItemLevel FROM item_template WHERE `entry` IN(".implode(", ",$itemIds).");");

foreach($itemLevelRows as $row)
	$itemLevel[$row["entry"]] = $row["ItemLevel"];

unset($itemRows, $itemLevelRows);

$itemOutput = "";
$itemIdList = "";
$itemLevelOutput = "";
foreach($slots as $slot){
	$itemId = $itemIds[$slot];
	$itemLevelOutput .= "[".$itemLevel[$itemId]."] <br/>";
	$itemIdList .= ' <a href="http://portal.wow-alive.de/item/'.$items[$slot].'">'.$items[$slot]."</a><br/>";
	$itemOutput .= '.add <a href="http://portal.wow-alive.de/item/'.$items[$slot].'">'.$items[$slot]."</a><br/>";
	
	if(in_array($slot, array("brust", "beine", "schmuck2") )){
		$itemIdList .= "<br/>";
		$itemLevelOutput .= "<br/>";
		$itemOutput .= "<br/>";
	}
}

//RND Items
$result = $TransferDB->select("SELECT * FROM formular_randomitem WHERE `id` = '$transferId';");

$randomItemRow = $result[0];
$randomItems = array();

for($i = 1; $i <= 10; $i++){
	if($randomItemRow["ri".$i] > 0)
		$randomItems[] = $randomItemRow["ri".$i];
}


//Ruf
$result = $TransferDB->select("SELECT * FROM formular_ruf WHERE `id` = '$transferId';");

$ruf = $result[0];

$sql_ruf_allianz1 = round(($ruf['Sturmwind']+$ruf['Die_Exodar']+$ruf['Eisenschmiede']+$ruf['Gnomeregangnome']+$ruf['Darnassus'])/5);
$sql_ruf_allianz2 = round(($ruf['Der_Silberbund']+$ruf['Die_Frosterben']+$ruf['Expedion_Valianz']+$ruf['Forscherliga'])/4);
$sql_ruf_horde1 = round(($ruf['Orgrimmar']+$ruf['Silbermond']+$ruf['Unterstadt']+$ruf['Donnerfels']+$ruf['Dunkelspeertrolle'])/5);
$sql_ruf_horde2 = round(($ruf['Die_Hand_der_Rache']+$ruf['Die_Sonnenhaescher']+$ruf['Die_Taunka']+$ruf['Kriegshymnenoffensive'])/4);


// Helper functions
function pre($string){
	return "<pre>".$string."</pre>";
}

?>
