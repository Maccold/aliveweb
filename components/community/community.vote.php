<?php
if(INCLUDED!==true)exit;

if(!function_exists('save_session_start'))
	require_once($_SERVER['DOCUMENT_ROOT']."/core/common.php");

// ==================== //
$pathway_info[] = array( 'title' => "Account Dienste", 'link'=> url_for("account"));
$pathway_info[] = array('title' => $lang['vote'], 'link' => url_for("community", "vote"));
// ==================== //

// CSS
$css_files[] = "/".$currtmp."/css/account.css";

// Here we chack to see if user is logged in, if not, then redirect to account login screen
if(!$userObject->isLoggedIn()){
	redirect(url_for("account","login"),1);
}

// Here we see if the site admin has the vote system enabled
if ((int)$MW->getConfig->vote_system->enable){
    $showvote = true;
}
else{ 
	$showvote = false;
}

// Check to see what realm we are using
$realm_info_new = get_realm_byid($user['cur_selected_realmd']);
$rid = $realm_info_new['id'];

// Some glabal settings. You shouldnt need to touch this stuff
$max_acc_points_per_day = $MW->getConfig->vote_system->max_points_per_day;
$ip_voting_period = 60 * 60 * 12; // IP voting period (in seconds)
$use_online_check = false; // See if the sites are online
$mangos_rev = $MW->getConfig->vote_system->mangos_revision;
$remote_access = array( 
	(string)$MW->getConfig->vote_system->vote_ra->id_1->address, 
	(string)$MW->getConfig->vote_system->vote_ra->id_1->port, 
	(string)$MW->getConfig->vote_system->vote_ra->id_1->username, 
	(string)$MW->getConfig->vote_system->vote_ra->id_1->password
);

// Lets get this session started!
save_session_start();

$_SESSION["realm_vote"] = $realm_info_new['name'];
$_SESSION["logged_voting"] = 1;
$_SESSION["panel"] = "reward";
$_SESSION["vote"] = 1;
$_SESSION["reward"] = 1;
$_SESSION["user_id"] = $user['id'];
$_SESSION["user_name"] = $user['username'];
$_SESSION["char_name"] = $user['character_name'];

// Functions
set_time_limit(0);
ini_set("default_charset", "UTF-8");
set_magic_quotes_runtime(false);

$votingEnabled = true;
if($_SESSION["date_points"] >= $max_acc_points_per_day)
	$votingEnabled = false;
$class = "row2";

// Table voting_points
$get_voting_points = $userObject->votePoints;
$justVotedPage = -1;

// Table voting
$get_voting = $DB->query("SELECT `sites`, `time` FROM `voting` WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
if (count($get_voting) > 0){
	foreach ($get_voting as $row) 
	{
		$_SESSION["time"] = $row["time"];
		if((time() - $row["time"]) > $ip_voting_period)
		{
			$DB->query("UPDATE `voting` SET `sites` = 0 WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
			$_SESSION["sites"] = 0;
		}
		else
			$_SESSION["sites"] = $row["sites"];
	}
}
else
{
	$DB->query("INSERT INTO `voting` (`user_ip`) VALUES ('".$_SERVER["REMOTE_ADDR"]."')");
	$_SESSION["sites"] = 0;
	$_SESSION["time"] = 0;
}


if(isset($_POST["site"]))
{
	$site = (int) $_POST["site"];
	$justVotedPage = $site;
	
	if(array_key_exists($site, $tab_sites))
	{
		if($use_online_check)
			$fp = @fsockopen($tab_sites[$site][0], 80, $errno, $errstr, 3);
		else
			$fp = true;
		
		$yet_voted = ($site & $_SESSION["sites"]);
		
		if(!($site & $_SESSION["sites"]) && ($_SESSION["date_points"] < $max_acc_points_per_day) && $fp)
		{
			echo "<!-- Voted $site -->";
			if($use_online_check)
				fclose($fp);
			$DB->query("UPDATE `voting` SET `sites`=(`sites` | ".$site."), `time`='".time()."' WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
			$_SESSION["sites"] += $site;
			$_SESSION["time"] = time();
			$userObject->changeVotePoints($tab_sites[$site][3], true);
		}
	}
}
debug("IP",$_SERVER["REMOTE_ADDR"]);


$votingLeft = false;

foreach($tab_sites as $key => $value)
{
	$tab_sites[$key]["voted"] = false;
	$tab_sites[$key]["alt"] = $value[0];
	$tab_sites[$key]["link"] = $value[1];
	$tab_sites[$key]["image"] = $value[2];
	$tab_sites[$key]["point_value"] = $value[3];
	$tab_sites[$key]["online"] = true;
	$tab_sites[$key]["class"] = ($class == "row1") ? "row2" : "row1";
	
	$online = true;
	
	$check = $_SESSION["sites"] & $key;
	debug($_SESSION["sites"]." & ".$key."? ".$check);
	if($check == $key){
		$tab_sites[$key]["voted"] = true;
	}
	else
		$votingLeft = true;
		
	if($use_online_check)
	{
		$fp = @fsockopen($value[0], 80, $errno, $errstr, 3);
		if($fp)
			fclose($fp);
		else
			$tab_sites[$key]["online"] = false;
	}
	
}

if( $votingLeft == false && isset($_POST["site"]) ){
	$userObject->can["vote"] = false;
	$userObject->saveCanVote();
}


if($max_acc_points_per_day < 0)
	$max_acc_points_per_day = count($tab_sites);



function initialize_user(){}


function show_sites()
{
	global $max_acc_points_per_day, $lang, $tab_sites, $use_online_check;
	
	echo write_subheader($lang["choose_site"]);
	echo "<table border=\"1\" width=\"550\" cellspacing=\"1\" cellpadding=\"3\" align=\"center\">
	<tr>
		<th>",$lang["voting_sites"],"</th>
		<th>",$lang["voted"],"</th>";
//	if($use_online_check)
//		echo "<th>",$lang["status"],"</th>";
	echo "<th>",$lang["points"],"</th>
		<th>",$language["choose"],"</th>
	</tr>";
	foreach($tab_sites as $key => $value)
	{
		
		echo "<form action=\"".url_for("community","vote")."\" method=\"post\"><input type=\"hidden\" name=\"site\" value=\"".$key."\" />
		
		<tr>
			<td><img src=\"",$value[2],"\" border=\"0\" alt=\"",$value[0],"\" /></td>
		<td>";
		if($_SESSION["date_points"] >= $max_acc_points_per_day)
			$disabled = " disabled=\"disabled\"";
		else
			$disabled = "";
		if($_SESSION["sites"] & $key)
		{
			echo "<center><font color=\"red\">",$lang["yes"],"</font></center>";
			$disabled = " disabled=\"disabled\"";
		}
		else
			echo "<center><font color=\"green\">",$lang["no"],"</font></center>";
		echo "</td>";
		if($use_online_check)
		{
			echo "<td>";
			$fp = @fsockopen($value[0], 80, $errno, $errstr, 3);
			if($fp)
			{
				echo "<center><font color=\"green\">",$lang["onlinealt"],"</font></center>";
				fclose($fp);
			}
			else
			{
				echo "<center><font color=\"red\">",$lang["offline"],"</font></center>";
				$disabled = " disabled=\"disabled\"";
			}
			echo "</td>";
		}
		echo "<td><center>",$value[3],"</center></td>
		<td><center><input type=\"submit\" name=\"submit\" value=\"",$lang["choose"],"\"",$disabled," /></center></td>
		</tr></form>";
	}
	echo "</table>";
}

