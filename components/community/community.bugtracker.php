<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>$lang['bugs'], 'link'=> url_for("community", "bugtracker"));
// ==================== //

$bug_detail = 0;
if(isset($bug) && is_numeric($bug) && $bug > 0){
	$bug_detail = $bug;
}

redirect(url_for("server","bugtracker"),1);


