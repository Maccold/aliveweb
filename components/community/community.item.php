<?php
if(INCLUDED!==true)exit;

if(!function_exists('save_session_start'))
	require_once($_SERVER['DOCUMENT_ROOT']."/core/common.php");
	
// ==================== //
$pathway_info[] = array( 'title' => "Account Dienste", 'link'=> url_for("account"));
$pathway_info[] = array( 'title' => $lang['item'], 'link' => url_for("community", "item") );
// ==================== //

// CSS
$css_files[] = "/".$currtmp."/css/account.css";

// Here we chack to see if user is logged in, if not, then redirect to account login screen
if($userObject->isLoggedIn() == false){
	redirect(url_for("account", "login"));
}

// Here we see if the site admin has the vote system enabled
if ((int)$MW->getConfig->vote_system->enable){
	$showvote = true;
} 
else{ 
	$showvote = false;
}

// Including the needed files to make the vote system work
require ($_SERVER['DOCUMENT_ROOT']."/config/voting_rewards.php");

// Check to see what realm we are using
$realm_info_new = get_realm_byid($user['cur_selected_realmd']);
$rid = $realm_info_new['id'];

// Some glabal settings. You shouldnt need to touch this stuff
$max_acc_points_per_day = $MW->getConfig->vote_system->max_points_per_day;
$ip_voting_period = 60 * 60 * 12; // IP voting period (in seconds)
$use_online_check = false; // See if the sites are online
$mangos_rev = $MW->getConfig->vote_system->mangos_revision;
$remote_access = array( (string)$MW->getConfig->vote_system->vote_ra->id_1->address, (string)$MW->getConfig->vote_system->vote_ra->id_1->port, (string)$MW->getConfig->vote_system->vote_ra->id_1->username, (string)$MW->getConfig->vote_system->vote_ra->id_1->password);


// Lets get this session started!
save_session_start();
		$_SESSION["realm_vote"] = $realm_info_new['name'];
		$_SESSION["logged_voting"] = 1;
		$_SESSION["panel"] = "reward";
		$_SESSION["vote"] = 1;
		$_SESSION["reward"] = 1;
		$_SESSION["user_id"] = $user['id'];
		$_SESSION["user_name"] = $user['username'];
		$_SESSION["char_name"] = $user['character_name'];

// Functions
set_time_limit(0);
ini_set("default_charset", "UTF-8");
set_magic_quotes_runtime(false);

// This get the vote started by getting your vote account info

// Table voting_points
$get_voting_points = $userObject->votePoints;

// Table voting
$get_voting = $DB->query("SELECT `sites`, `time` FROM `voting` WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
if (count($get_voting) > 0){
	foreach ($get_voting as $row) 
	{
		$_SESSION["time"] = $row["time"];
		if((time() - $row["time"]) > $ip_voting_period)
		{
			$DB->query("UPDATE `voting` SET `sites` = 0 WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
			$_SESSION["sites"] = 0;
		}
		else
			$_SESSION["sites"] = $row["sites"];
	}
}
else
{
	$DB->query("INSERT INTO `voting` (`user_ip`) VALUES ('".$_SERVER["REMOTE_ADDR"]."')");
	$_SESSION["sites"] = 0;
	$_SESSION["time"] = 0;
}

if($_SESSION["reward"] == 1)
{
	$reward_message = "";
	if(isset($_POST["reward"]))
		$reward_message = chose_reward((int) $_POST["reward"]);
	else if(isset($_POST["character"]))
		add_char_to_session((int) $_POST["character"]);
}

function show_rewards1()
{
	global $lang, $reward_texts1, $tab_rewards1;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Gold").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">In Geldn&ouml;ten? Tausche hier Alive-Cash in Gold.</p></td>
	</tr>';
	foreach($tab_rewards1 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts1[0][$key].'</a>';
		else
			echo $reward_texts1[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards2()
{
	global $lang, $reward_texts2, $tab_rewards2;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Spezielle Berufsitems & Materialien").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">Alles was du f&uuml;r deinen Beruf ben&ouml;tigst. Die Preise sind so hoch damit es sich f&uuml;r dich lohnt ingame einen Hersteller des Materials/Items zu suchen.</p></td>
	</tr>';
	foreach($tab_rewards2 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts2[0][$key].'</a>';
		else
			echo $reward_texts2[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards3()
{
	global $lang, $reward_texts3, $tab_rewards3;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Nordendberufe").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">Dir fehlen noch Items f&uuml;r deinen Beruf? Hier findest du berufsabh&auml;nige Items.</p></td>
	</tr>';
	foreach($tab_rewards3 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts3[0][$key].'</a>';
		else
			echo $reward_texts3[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards4()
{
	global $lang, $reward_texts4, $tab_rewards4;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Funitems").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">Hilfreiches bis sinnbefreites ;)</p></td>
	</tr>';
	foreach($tab_rewards4 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts4[0][$key].'</a>';
		else
			echo $reward_texts4[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards5()
{
	global $lang, $reward_texts5, $tab_rewards5;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Embleme").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">oO Fehlende Embleme? Hol dir die die du brauchst.</p></td>
	</tr>';
	foreach($tab_rewards5 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = " disabled";
		else
			$disabled = "";
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts5[0][$key].'</a>';
		else
			echo $reward_texts5[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards6()
{
	global $lang, $reward_texts6, $tab_rewards6;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Haustiere").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">Warum in der Wildnis alleine sein? Aber denke dran, immer sch&ouml;n f&uuml;ttern.</p></td>
	</tr>';
	foreach($tab_rewards6 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts6[0][$key].'</a>';
		else
			echo $reward_texts6[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards7()
{
	global $lang, $reward_texts7, $tab_rewards7;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Reittiere").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">Meines leuchtet aber sch&ouml;ner als deines...</p></td>
	</tr>';
	foreach($tab_rewards7 as $key => $value)
	{
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts7[0][$key].'</a>';
		else
			echo $reward_texts7[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function show_rewards8()
{
	global $lang, $reward_texts8, $tab_rewards8;
	$rowclass = "row2";
	echo '
	<tr>
		<th colspan="3" align="center">'.template_subheader("Einl&ouml;sbare Haustiere & Reittiere").'</th>
	</tr>
	<tr class="disabled">
		<td colspan="3"><p style="font-style:normal;">Diese Haus- & Reittiere kannst du dir hier kaufen oder an Events verdienen. Es gibt aber f&uuml;r Events einzigartige Haustiere die nie im Voteshop verkauft werden.</p></td>
	</tr>';
	foreach($tab_rewards8 as $key => $value)
	{ 
		if($value[2] > $_SESSION["points"])
			$disabled = ' disabled';
		else
			$disabled = '';
		$rowclass = ($rowclass == "row1") ? "row2" : "row1";
		echo '
	<tr class="'.$rowclass.'">
		<td align="left">';
		if($value[0] && $value[3] >= 0)
			echo $value[1].' x <a class="q'.$value[3].'" href="http://de.wowhead.com/?item='.$value[0].'" target="_blank">'.$reward_texts8[0][$key].'</a>';
		else
			echo $reward_texts8[0][$key];
		echo '</td>
		<td align="center">'.$value[2].' '.$lang["points2"].'</td>
		<td align="center"><form action="'.url_for("community", "item").'" method="post"><input type="hidden" name="reward" value="'.$key.'" /><button type="submit" name="submit" class="ui-button button1'.$disabled.'"><span><span>'.$lang["choose"].'</span></span></button></form></td></tr>';
	}
}
function chose_reward($reward)
{
	global $lang, $mangos_rev, $vote_realms, $remote_access, $reward_texts, $tab_rewards, $DB, $userObject;
	if(!array_key_exists($reward, $tab_rewards))
		return '';
	if(!isset($_SESSION["points"]) || ($_SESSION["points"] < $tab_rewards[$reward][2]) || $_SESSION["points"] <= 0)
		return '';
			
	$remote = $remote_access;
	$telnet = @fsockopen($remote[0], $remote[1], $errno, $errstr, 3);
	if($telnet)
	{
		// Session schon mal reduzieren damit Doppel-Einkaufen nicht möglich ist
		$_SESSION["points"] = $_SESSION["points"] - $tab_rewards[$reward][2];
		
		fgets($telnet,1024); // Motd
		if($mangos_rev)
			fputs($telnet, "".$remote[2]."\n");
		else
		{
			fgets($telnet,1024); // USER
			fputs($telnet, $remote[2]."\n");
		}
		if($mangos_rev)
			fputs($telnet, "".$remote[3]."\n");
		else
		{
			fgets($telnet,1024); // PASS
			fputs($telnet, $remote[3]."\n");
		}
		$remote_login = fgets($telnet,1024);
		if($remote_login[0] = "Willkommen auf Norganon dem PVE/P Server von WoW ALive.")		
		{
			if($tab_rewards[$reward][0])
				fputs($telnet, "send items ".$_SESSION["char_name"]." \"".$lang["mail_subject"]."\" \"".$lang["mail_message"]."\" ".$tab_rewards[$reward][0].":".$tab_rewards[$reward][1]."\n");
			else
				fputs($telnet, "send money ".$_SESSION["char_name"]." \"".$lang["mail_subject"]."\" \"".$lang["mail_message"]."\" ".$tab_rewards[$reward][1]."\n");
			sleep(5);
			$send_mail = fgets($telnet,1024);
			if(strpos($send_mail, $_SESSION["char_name"]))
			{
				//$DB->query("UPDATE `voting_points` SET `points`=(`points` - ".$tab_rewards[$reward][2].") WHERE `id` = ".$_SESSION["user_id"]." LIMIT 1");
				$userObject->changeVotePoints(-$tab_rewards[$reward][2]);
				$message = '<font color="red">'.$lang["was_given"]." ".$reward_texts[0][$reward]." ".$lang["to"]." ".$_SESSION["char_name"]." ".$lang["send"].'</font>';
			}
			else
				$message = '<font color="red">Send Mail Problem: '.$send_mail.'</font>';
		}
		else
			$message = '<font color="red">Remote Login Problem: '.$remote_login.'</font>';
		fclose($telnet);
	}
	else
		$message = '<font color="red">Telnet Connection Problem: '.$errstr.'</font>';
	return $message.'<br />';
}
?>
