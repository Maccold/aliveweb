<?php


$show_account_sidebar = false;
$account_sidebar = "";
if($userObject->theme == "Shattered-World"){
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/account/account.sidebar.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$account_sidebar = ob_get_contents();
	ob_end_clean();

}


$com_content['community'] = array(
    'index' => array(
        '', // g_ option require for view     [0]
        'community', // loc name (key)                [1] 
        url_for('community'), // Link to                 [2]
        '', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),

    'teamspeak' => array(
        '', 
        'teamspeak', 
        url_for("community", "teamspeak"),
        '7-menuCommunity',
        0
    ),
    'donate' => array(
        '',
        'donate',
        url_for("community", "donate"),
        '7-menuCommunity',
        0
    ),
	'chat' => array(
        '', 
        'chat', 
        url_for("community", "chat"),
        '7-menuCommunity',
        0
    ),
	'vote' => array(
        '',
        'vote',
        url_for("community", "vote"),
        '7-menuCommunity',
        0
    ),
	'login' => array(
        '',
        'login',
        url_for("community", "login"),
        '7-menuCommunity',
        0
    ),
	'vote_sites' => array(
        '',
        'vote_sites',
        'index.php?n=community&sub=vote?sites',
        '7-menuCommunity',
        0
    ),
	'ct' => array(
        '',
        'ct',
        url_for("community", "ct"),
        '7-menuCommunity',
        0
    ),
	'bt' => array(
        '',
        'bt',
        url_for("community", "bt"),
        '7-menuCommunity',
        0
    ),
	'3d' => array(
        '',
        '3d',
        url_for("community", "3d"),
        '7-menuCommunity',
        0
    ),
	'gm' => array(
        'g_is_admin',
        'gm',
        url_for("community", "gm"),
        '7-menuCommunity',
        0
    ),
   'changelog' => array(
        '',
        'changelog',
        url_for("community", "changelog"),
        '4-menuInteractive',
        0
    ),
   'clpage' => array(
        '',
        'clpage',
        url_for("community", "clpage"),
        '4-menuInteractive',
        0
    ),
   'clcore' => array(
        '',
        'clcore',
        url_for("community", "clcore"),
        '4-menuInteractive',
        0
    ),
   'item' => array(
        '',
        'item',
        url_for("community", "item"),
        '4-menuInteractive',
        0
    ),
    'twa001' => array(
        '',
        'twa001',
        url_for("community", "twa001"),
        '3-menuGameGuide',
        0
    ),
    'te001' => array(
        '',
        'te001',
        url_for("community", "te001"),
        '3-menuGameGuide',
        0
    ),
    'tc001' => array(
        '',
        'tc001',
        url_for("community", "tc001"),
        '3-menuGameGuide',
        0
    ),
    'player' => array(
        '',
        'player',
        url_for("community", "player"),
        '3-menuGameGuide',
        0
    ),
    'classic2' => array(
        '',
        'classic2',
        url_for("community", "classic2"),
        '3-menuGameGuide',
        0
    ),
    'burncrus' => array(
        '',
        'burncrus',
        url_for("community", "burncrus"),
        '3-menuGameGuide',
        0
    ),
    'wrath' => array(
        '',
        'wrath',
        url_for("community", "wrath"),
        '3-menuGameGuide',
        0
    ),
    'poll' => array(
        '',
        'poll',
        url_for("community", "poll"),
        '3-menuGameGuide',
        0
    ),
    'raidclassic' => array(
        '',
        'raidclassic',
        url_for("community", "raidclassic"),
        '3-menuGameGuide',
        0
    ),
    'bugtracker' => array(
        '',
        'bugs',
        url_for("community", "bugtracker"),
        '7-menuCommunity',
        0
    ),
);
?>
