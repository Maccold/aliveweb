<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>"Server", 'link'=>url_for("server"));
$pathway_info[] = array('title'=>"Media");
$pathway_info[] = array('title'=>"Videos", 'link'=>url_for("community", "tc001"));
// ==================== //

$css_files[] = "/".$currtmp."/css/account.css";

// Sidebar
$server_sidebar = "";
if($userObject->theme == "Shattered-World"){
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/server/server.sidebar.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$server_sidebar = ob_get_contents();
	ob_end_clean();

}

