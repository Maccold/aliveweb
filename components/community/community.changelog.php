<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=> "Server", 'link' => url_for("server"));
$pathway_info[] = array('title'=>$lang['changelog'], 'link'=>url_for("community", "changelog"));
// ==================== //


include($_SERVER['DOCUMENT_ROOT']."/config/config-protected.php");

$css_files[] = "/".$currtmp."/css/account.css";
$css_files[] = "/".$currtmp."/css/server.css";

// Changelog DB
$ChangelogDB = DbSimple_Generic::connect( "" . $realmd['db_type'] . "://" . $realmd['db_username'] .
	":" . $realmd['db_password'] . "@" . $realmd['db_host'] . ":" . $realmd['db_port'] .
	"/changelog" ) ;
if ( $RealmDB ){
	$RealmDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$RealmDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}


$currentServerRevision = 0;
$currentPageRevision = 0;
$serverRevisions = array();
$pageRevisions = array();

$sql = "select `CoreRev`, `PageRev` from changelog.Rev";

$rows = $ChangelogDB->select($sql);
foreach($rows as $basis){
	$currentServerRevision = $basis['CoreRev'];
	$currentPageRevision = $basis['PageRev'];
}

$sql = "select `Rev`, `Message`, `Date`, `Author` from changelog.data order by `Rev` DESC Limit 20;";

$class = "row2";
$rows = $ChangelogDB->select($sql);
foreach($rows as $basis)
{
	$class = ($class == "row1") ? "row2" : "row1";
	$basis["class"] = $class;

	$array = explode("-",$basis["Date"]);
	$basis["Date"] = $array[2].".".$array[1].".".$array[0];
	$serverRevisions[] = $basis;
}

$sql = "select `Rev`, `Message`, `Date`, `Author` from changelog.Portal order by `Rev` DESC Limit 15;";

$class = "row2";
$rows = $ChangelogDB->select($sql);
foreach($rows as $basis)
{
	$class = ($class == "row1") ? "row2" : "row1";
	$basis["class"] = $class;

	$array = explode("-",$basis["Date"]);
	$basis["Date"] = $array[2].".".$array[1].".".$array[0];
	$pageRevisions[] = $basis;
}

// Sidebar
$server_sidebar = "";
if($userObject->theme == "Shattered-World"){
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/server/server.sidebar.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$server_sidebar = ob_get_contents();
	ob_end_clean();

}
