<?php
if(INCLUDED !== true)
    exit();

$postnum = 0;
$hl = '';
$show_sidebar = true;
$sidebar_file = "frontpage.sidebar.php";

$pageId = "homepage"; // Für CSS

// Realm DB
$RealmDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
	":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
	"/trinity_realm" ) ;
if ( $RealmDB ){
	$RealmDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$RealmDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}


if((int)$MW->getConfig->generic_values->forum->news_forum_id == 0)
    output_message('alert', 'Please define forum id for news (in config/config.xml)');

$alltopics = $DB->select("
    SELECT f_topics.*,(SELECT message FROM f_posts WHERE f_topics.topic_id=f_posts.topic_id ORDER BY f_posts.posted LIMIT 1) as message
    FROM f_topics
    WHERE f_topics.forum_id=?d
    ORDER BY topic_posted DESC
    LIMIT ?d,?d", (int)$MW->getConfig->generic_values->forum->news_forum_id, 0, (int)$MW->getConfig->generic_values->news->items_per_page);

if ((int)$MW->getConfig->components->right_section->hitcounter){
    $count_my_page = "templates/offlike/hitcounter.txt";
    $hits = (int)file_get_contents($count_my_page);
    $hits++;
    file_put_contents($count_my_page, $hits);
}

$result = $DB->select("SELECT shortinfo FROM shortinfo where id=1");
$shortinfo = $result[0];

$servers = array();
$multirealms = $DB->select("SELECT * FROM `realmlist` ORDER BY id ASC");
foreach ($multirealms as $realmnow_arr){
    if((int)$MW->getConfig->components->right_section->server_information){
        $data = $DB->selectRow("SELECT address, port, timezone, icon, name, dbinfo FROM realmlist WHERE id = ? LIMIT 1", $realmnow_arr['id']);

        $realm_data_explode = explode(';', $data['dbinfo']);

        $mangosALL = array();
        if((int)$MW->getConfig->generic->use_archaeic_dbinfo_format){
            //alternate config - for users upgrading from Modded MaNGOS Web
            //DBinfo column:  host;port;username;password;WorldDBname;CharDBname
            $mangosALL = array(
                'db_type' => 'mysql',
                'db_host' => $realm_data_explode['0'],  //ip of db world
                'db_port' => $realm_data_explode['1'], //port
                'db_username' => $realm_data_explode['2'], //world user
                'db_password' => $realm_data_explode['3'], //world password
                'db_name' => $realm_data_explode['4'],  //world db name
                'db_char' => $realm_data_explode['5'], //character db name
                'db_encoding' => 'utf8'
            );
        }else{
            //normal config, as outlined in how-to
            //DBinfo column:  username;password;port;host;WorldDBname;CharDBname
            $mangosALL = array(
                'db_type' => 'mysql',
                'db_host' => $realm_data_explode['3'],  //ip of db world
                'db_port' => $realm_data_explode['2'], //port
                'db_username' => $realm_data_explode['0'], //world user
                'db_password' => $realm_data_explode['1'], //world password
                'db_name' => $realm_data_explode['4'],  //world db name
                'db_char' => $realm_data_explode['5'], //character db name
                'db_encoding' => 'utf8'
            );
        }
        unset($realm_data_explode);

        if((int)$MW->getConfig->generic->use_alternate_mangosdb_port){
            $mangosALL['db_port'] = (int)$MW->getConfig->generic->use_alternate_mangosdb_port;
        }

        $CHDB_EXTRA = DbSimple_Generic::connect("" . $mangosALL['db_type'] . "://" . $mangosALL['db_username'] . ":" . $mangosALL['db_password'] . "@" . $mangosALL['db_host'] . ":" . $mangosALL['db_port'] . "/" . $mangosALL['db_char'] . "");
        if($CHDB_EXTRA)
            $CHDB_EXTRA->query("SET NAMES " . $mangosALL['db_encoding']);
        unset($mangosALL); // Free up memory.

        $server = array();
        $server['name'] = $data['name'];
		$serverId = $realmnow_arr['id'];
        if((int)$MW->getConfig->components->server_information->realm_status){
            $checkaddress = (int)$MW->getConfig->generic->use_local_ip_port_test ? '127.0.0.1' : $data['address'];
            $server['realm_status'] = (check_port_status($checkaddress, $data['port']) === true) ? true : false;
		}
		if($server['realm_status']){
			$server['maxplayers'] = $RealmDB->selectCell("SELECT maxplayers FROM `uptime` WHERE realmid=? ORDER BY maxplayers DESC LIMIT 1;", $serverId);
		
			$starttime = $RealmDB->selectCell("SELECT starttime FROM `uptime` WHERE realmid=? ORDER BY starttime DESC LIMIT 1;", $serverId);
		
		
			$server["uptime"] = time_diff_conv($starttime, time());
	
			$server["ally_online"] = $CHDB_EXTRA->selectCell("
			SELECT count(1) FROM `characters` 
			WHERE `race` IN(".RACE_HUMAN.", ".RACE_DWARF.", ".RACE_NIGHTELF.", ".RACE_GNOME.", ".RACE_DRAENEI.") and online = 1;");
			
			$server["horde_online"] = $CHDB_EXTRA->selectCell("
			SELECT count(1) FROM `characters` 
			WHERE `race` IN(".RACE_ORC.", ".RACE_UNDEAD.", ".RACE_TAUREN.", ".RACE_TROLL.", ".RACE_BLOODELF.") and online = 1;");
			
			$server["gm_online"] = $CHDB_EXTRA->selectCell("
			SELECT count(1) FROM `characters` 
			WHERE `account` IN (SELECT `id` from trinity_realm.account_access WHERE `RealmID` = '1') and online = 1;");
			
			$total = $server["ally_online"] + $server["horde_online"];
			$server["ally_percent"] = round($server["ally_online"]/$total*100, 2);
			$server["horde_percent"] = round($server["horde_online"]/$total*100, 2);
		}

        $changerealmtoparam = array("changerealm_to" => $realmnow_arr['id']);
        if((int)$MW->getConfig->components->server_information->online){
            $server['playersonline'] = $CHDB_EXTRA->selectCell("SELECT count(1) FROM `characters` WHERE online=1");
            $server['onlineurl'] = url_for('server', 'playersonline');
        }
        if((int)$MW->getConfig->components->left_section->Playermap){
            $server['playermapurl'] = url_for('server', 'playermap');
        }
        if((int)$MW->getConfig->components->server_information->server_ip){
            $server['server_ip'] = $data['address'];
        }
        if((int)$MW->getConfig->components->server_information->type){
            $server['type'] = $realm_type_def[$data['icon']];
        }
        if((int)$MW->getConfig->components->server_information->language){
            $server['language'] = $realm_timezone_def[$data['timezone']];
        }
        if((int)$MW->getConfig->components->server_information->population){
            $server['population'] = $CHDB_EXTRA->selectCell("SELECT count(1) FROM `characters` WHERE online=1");
        }
        if((int)$MW->getConfig->components->server_information->accounts){
            $server['accounts'] = $DB->selectCell("SELECT count(*) FROM `account`");
        }
        if((int)$MW->getConfig->components->server_information->active_accounts){
            $server['active_accounts'] = $DB->selectCell("SELECT count(1) FROM `account` WHERE `last_login` > ?", date("Y-m-d", strtotime("-2 week")) . " 00:00:00");
        }
        if((int)$MW->getConfig->components->server_information->characters){
            $server['characters'] = $CHDB_EXTRA->selectCell("SELECT count(1) FROM `characters`");
        }
        unset($CHDB_EXTRA, $data); // Free up memory.
        $init = 'id_' . $realmnow_arr['id'];
        if((int)$MW->getConfig->components->right_section->server_rates && (string)$MW->getConfig->mangos_conf_external->$init->mangos_world_conf != ''){

            $server['rates'] = getMangosConfig($MW->getConfig->mangos_conf_external->$init->mangos_world_conf);
        }
        $server['moreinfo'] = (int)$MW->getConfig->components->server_information->more_info && (string)$MW->getConfig->mangos_conf_external->$init->mangos_world_conf != '';
        $servers[] = $server;
    }
}
unset($multirealms);

if((int)$MW->getConfig->components->right_section->users_on_homepage){
    $usersonhomepage = $DB->selectCell("SELECT count(1) FROM `online`");
}

/*
	Features
	
	id: Ist einfach eine einzigartige ID, identifiziert das Bild für das Feature im Ordner http://portal.wow-alive.de/images/slideshow/
	title: Titel des Features
	desc: Beschreibungstext unter der Überschrift
	url: Link zum Forumsbeitrag, Custompage oder sonstigem
*/
$features = array(
	array(
		"id" => "ruby",
		"title" => "Das Rubinsanktum",
		"desc" => "Beweist euer Movement bei Halion!",
		"url" => "http://portal.wow-alive.de/game/zone/the-ruby-sanctum/",
	),
	/*array(
		"id" => "3256",
		"title" => "Community Watch - GildenTreffen einberufen",
		"desc" => "Das Team ruft Vertreter aller Gilden zusammen f&uuml;r ein gemeinsames Meeting.",
		"url" => "http://forum.wow-alive.de/showthread.php?t=3256",
	),*/
	array(
		"id" => "icc-hero",
		"title" => "Weitere Bosse auf Heroisch freigegeben",
		"desc" => "K&auml;mpft euch im heroischen Modus bis zu Sindragosa vor.",
		"url" => "http://portal.wow-alive.de/game/zone/icecrown-citadel/",
	),
	/*array(
		"id" => "0771KTHIM8DS1328566053786",
		"title" => "Liebe liegt in der Luft",
		"desc" => "verlängert bis zum 27.02.2012",
		"url" => "http://forum.wow-alive.de/showthread.php?t=4562",
	),*/
	/*array(
		"id" => "newstyle",
		"title" => "Neue Optik für die Homepage",
		"desc" => "Alive hat ein neues Design bekommen.",
		"url" => "http://forum.wow-alive.de/showthread.php?t=4510",
	),*/
	array(
		"id" => "3254",
		"title" => "Server Wachstum",
		"desc" => "Neue Spieler str&ouml;men auf ALive",
		"url" => "http://forum.wow-alive.de/showthread.php?t=3254",
	),
	/*
	array(
		"id" => "3242",
		"title" => "Ende der Arena Saison",
		"desc" => "Welche Belohnungen gibt es am Ende einer Arena-Saison?",
		"url" => "http://forum.wow-alive.de/showthread.php?t=4497",
	),*/
	/*array(
		"id" => "3234",
		"title" => "Event: Das W&uuml;stenrennen",
		"desc" => "Am So. den 27.02.11 findet gegen 16 Uhr das W&uuml;stenrennen statt.",
		"url" => "http://forum.wow-alive.de/showthread.php?t=3234",
	),*/
	array(
		"id" => "3257",
		"title" => "Changelog",
		"desc" => "Lest die letzten &Auml;nderungen am Core im Forum.",
		"url" => "http://forum.wow-alive.de/showthread.php?t=4229",
	),
	/*array(
		"id" => "3106",
		"title" => "Tausendwinter",
		"desc" => "Die Schlacht um Tausendwinter ist nun offen.",
		"url" => "http://forum.wow-alive.de/showthread.php?t=3106",
	)*/
);

/*
	News
*/
$url = "http://forum.wow-alive.de/external_news.php";

$string = file_get_contents($url);

$start = strpos($string, '<div id="news">')+strlen('<div id="news">');
$stop = strpos($string, "</div> <!-- /news -->");
$news_string = substr($string, $start, $stop-$start);
$news_string = utf8_encode($news_string); 
$news_string = str_replace('src="/images/', 'src="http://forum.wow-alive.de/images/', $news_string);  
  
if(strpos($string, '<div id="posts">') > 0){
	$start = strpos($string, '<div id="posts">')+strlen('<div id="posts">');
	$stop = strpos($string, "</div> <!-- /posts -->");
	$posts_string = substr($string, $start, $stop-$start);
}
else{
	$news_string = ""; 
}
