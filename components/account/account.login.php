<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>$lang['login'],'link'=>'');
// ==================== //

$css_files = array();

if($_REQUEST['action']=='login'){
	$login = $_REQUEST['login'];
	$pass = sha_password($login,$_REQUEST['pass']);
	if($auth->login(array('username'=>$login,'sha_pass_hash'=>$pass)))
	{
		if(!empty($_POST["previous-page"]))
			redirect($_POST["previous-page"],1);
		else 
			redirect($_SERVER['HTTP_REFERER'],1);
	}
}
elseif($_REQUEST['action']=='logout' || $action == "logout"){
	$auth->logout();
	redirect($_SERVER['HTTP_REFERER'],1);
}

