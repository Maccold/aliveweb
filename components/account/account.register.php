<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>$lang['register'],'link'=>'');

define("STEP_START", 0);
define("STEP_FIRST", 1);
define("STEP_FORM", 2);
define("STEP_LAST", 3);

$step = (isset($_POST['step'])) ? $_POST['step'] : STEP_START;

// Language string
$js_messageStrings["registerUsernameCheck"] = sprintf($lang['reg_checklogin'],$regparams['MIN_LOGIN_L'],$regparams['MAX_LOGIN_L']);
$js_messageStrings["registerPasswordCheck"] = sprintf($lang['reg_checkpass'],$regparams['MIN_PASS_L'],$regparams['MAX_PASS_L']);

$js_messageStrings["registerUsernameExisting"] = $lang['reg_checkloginex'];
$js_messageStrings["registerPasswordMatch"] = $lang['reg_checkcpass'];
$js_messageStrings["registerEmailInvalid"] = $lang['reg_checkemail'];
$js_messageStrings["registerEmailExisting"] = $lang['reg_checkemailex'];

$form_data = array();
$form_vars = array("r_login", "r_pass", "r_cpass", "r_email", 
	"secretq1", "secreta1", 
	"secretq2", "secreta2", "r_account_type");

foreach($form_vars as $var){
	$form_data[$var] = (isset($_POST[$var])) ? $_POST[$var] : "";
}

if ((int)$MW->getConfig->generic->site_register == 0)
{
      output_message('alert','Registration: Locked');
}
else
{
	$regparams = array(
		'MIN_LOGIN_L' => 3,
		'MAX_LOGIN_L' => 16,
		'MIN_PASS_L'  => 4,
		'MAX_PASS_L'  => 16
	);
      // ==================== //
	if($userObject->isLoggedIn()){
		//redirect(url_for("account", "manage"),1);
	}
	$allow_reg = true;
	$err_array = array();
	//$err_array[0] = $lang['ref_fail'];	// Registration failed ?
		
		
	if($step > STEP_START && (int)$MW->getConfig->generic->req_reg_key)
	{
		if($auth->isvalidregkey($form_data['r_key'])!==true)
		{
			output_message('alert',$lang['bad_reg_key']);
			$allow_reg = false;
			$err_array[] = "Dein Registrationsschl�ssel ist falsch, bitte &uuml;berpr&uuml;fe nochmal ob du alles richtig eingetragen hast.";
			$err_array[] = '<a href="'.url_for("account", "register").'" class="ui-button button1 button1-next"><span><span>Account erstellen</span></span></a>';
			log_error("Account creation: On account registrer the key ".$form_data['r_key']." was not valid.");
		}
	}
	
	if((int)$MW->getConfig->generic->max_accounts_per_ip > 0)
	{
		$count_ip = $DB->selectCell("SELECT count(*) FROM account_extend WHERE registration_ip=?",$_SERVER['REMOTE_ADDR']);
		if($count_ip>=(int)$MW->getConfig->generic->max_accounts_per_ip)
		{
			output_message('alert',$lang['reg_acclimit']);
			$allow_reg = false;
			$err_array[] = $lang['reg_acclimit'];
			log_error("Account Creation: User with ip ".$_SERVER['REMOTE_ADDR']." is not allowed to create more than ".(int)$MW->getConfig->generic->max_accounts_per_ip." per IP.");
		}
	}
	
	if($step == STEP_LAST)
	{
		if($allow_reg === true)
		{
			$notreturn = FALSE; // Inizialize variable, we use this after. Use this to add extensions.
				
			// Extensions
			// Each extention you see down-under will check for specific user input,
			// In this step we set "requirements" for what user may input.
					
			// Ext 1.
			if ((int)$MW->getConfig->generic_values->account_registrer->enable_image_verfication)
			{
				$image_key =& $_POST['image_key'];
				$filename=quote_smart($_POST['filename_image']);
					
				$correctkey = $DB->selectCell("SELECT acc_creation_captcha.key FROM acc_creation_captcha WHERE filename=".$filename);
				if (strtolower($correctkey) != strtolower($image_key) || $image_key == '')
				{
					$notreturn = TRUE;
					$err_array[] = "Inputted text for Image Verification was incorrect.";
					log_error("Account Creation: Image verfication error, user didn't type the image right.In DB: ".$correctkey." , user input: ".strtolower($image_key).".");
				}
			}
			
			// Ext 2 check
			/* Check 3 - secret questions*/
			if ((int)$MW->getConfig->generic_values->account_registrer->secret_questions_input)
			{
				if ($form_data['secretq1'] && $form_data['secretq2'] && $form_data['secreta1'] && $form_data['secreta2']) 
				{
					if(check_for_symbols($form_data['secreta1']) || check_for_symbols($form_data['secreta2']))
					{
						$notreturn = TRUE;
						$err_array[] = "Bitte benutzen Sie nur normale Buchstaben und Zahlen f&uuml;r die Antworten auf die Sicherheitsfragen.";
					}
					
					if($form_data['secretq1'] == $form_data['secretq2']) 
					{
						$notreturn = TRUE;
						$err_array[] = "Bitte w&auml;hlen Sie 2 verschiedene Sicherheitsfragen aus.";
					}
					
					/*
						who cares..
					if($form_data['secreta1'] == $form_data['secreta2']) 
					{
						$notreturn = TRUE;
						$err_array[] = "Die Antworten auf die Sicherheitsfragen d�rfen.";
					}*/
					
					if(strlen($form_data['secreta1']) < 4 || strlen($form_data['secreta2']) < 4) 
					{
						$notreturn = TRUE;
						$err_array[] = "Die Antworten m&uuml;ssen mindestens 4 Zeichen lang sein.";
					}
				}
				else 
				{
					$notreturn = TRUE;
					$err_array[] = "Bitte w&auml;hlen Sie 2 Sicherheitsfragen aus und tragen jeweils Ihre Antworten ein.";
				}
			}
					
			if(!preg_match("#[A-Za-z0-9\@]+#", $form_data['r_login'])) 
			{
				$notreturn = TRUE;
				$err_array[] = "Der Benutzername darf nur aus Buchstaben und Zahlen bestehen.";
			}
					
			// Main add.
			if ($notreturn === FALSE){
				if( $auth->register( 
					array(
						'username' => $form_data['r_login'],
						'sha_pass_hash' => sha_password($form_data['r_login'],$form_data['r_pass']),
						'sha_pass_hash2' => sha_password($form_data['r_login'],$form_data['r_cpass']),
						'email' => $form_data['r_email'],
						'expansion' => $form_data['r_account_type'],
						'password' => $form_data['r_pass']
					), 
					array(
						'secretq1' => strip_if_magic_quotes($form_data['secretq1']),
						'secreta1' => strip_if_magic_quotes($form_data['secreta1']),
						'secretq2' => strip_if_magic_quotes($form_data['secretq2']), 
						'secreta2' => strip_if_magic_quotes($form_data['secreta2'])
					)) === true )
				{
					if((int)$MW->getConfig->generic->req_reg_key)
						$auth->delete_key($form_data['r_key']);
					if((int)$MW->getConfig->generic->req_reg_act == 0)
						$auth->login( array(
							'username' => $form_data['r_login'],
							'sha_pass_hash' => sha_password($form_data['r_login'],$form_data['r_pass'])
						));
					$reg_succ = true;
				}
				else{
					$reg_succ = false;
					$err_array[] = "Registrierung fehlgeschlagen: Der Benutzername ist schon vorhanden. Bitte w&auml;hle einen anderen.";
				}
			}
			else
			{
				$reg_succ = false;
			}
					
			//Error message
			if($reg_succ == false) 
			{
				$step = STEP_FORM;
				if(empty($err_array)) 
				{
					$err_array[] = "Registrierung fehlgeschlagen aus unklarem Grund. Bitte melde dich bei unserem Team.";
				}
				$output_error = implode("<br>\n",$err_array);
				output_message('alert',$output_error);
			}
		}
	}
}

