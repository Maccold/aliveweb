<?php
if(INCLUDED!==true)exit;

// ==================== //
$pathway_info[] = array('title'=>$lang['char_manage'],'link'=>'');
// ==================== //

if(!function_exists('save_session_start'))
	require_once($_SERVER['DOCUMENT_ROOT']."/core/common.php");
	
require_once("chartools/functions.php");

// Lets get this session started!
save_session_start();

$show_account_sidebar = true;

$MANG = new Mangos;

$account_id = $userObject->id;
$your_points = $userObject->votePoints;

$_SESSION["realm_vote"] = $realm_info_new['name'];
$_SESSION["logged_voting"] = 1;
$_SESSION["panel"] = "reward";
$_SESSION["vote"] = 1;
$_SESSION["reward"] = 1;
$_SESSION["user_id"] = $user['id'];
$_SESSION["user_name"] = $user['username'];
$_SESSION["char_name"] = $user['character_name'];	

// Defines
define("AT_LOGIN_RENAME", 1);
define("AT_LOGIN_CUSTOMIZE", 8);
define("AT_LOGIN_CHANGE_FACTION", 64);
define("AT_LOGIN_CHANGE_RACE", 128);

// Here we check to see if user is logged in, if not, then redirect to account login screen
if(!$userObject->isLoggedIn()){
    redirect('/account/login',1);
}else{
}

// Char Rename
if ((int)$MW->getConfig->character_tools->rename){
    $show_rename = true;
}
else{ 
	$show_rename = false;
}

// Char Customization
if ((int)$MW->getConfig->character_tools->re_customization){
    $show_custom = true;
}else{ 
	$show_custom = false;
}

// Race Changer
if ((int)$MW->getConfig->character_tools->race_changer){
    $show_changer = true;
}else{ 
	$show_changer = false;
}

// Faction Changer 
if ((int)$MW->getConfig->character_tools->faction_change){
    $allow_faction_change = true;
}else{ 
	$allow_faction_change = false;
}

// The character rename starts here
$char_rename_points = (int)$MW->getConfig->character_tools->rename_points;
$char_custom_points = (int)$MW->getConfig->character_tools->customization_points;
$char_faction_points = (int)$MW->getConfig->character_tools->faction_points;
$char_race_points = (int)$MW->getConfig->character_tools->faction_points;

// Get List of Characters
$characters = $CHDB->select("SELECT guid,name,at_login,online FROM `characters` WHERE account= '$account_id';");

/*
|---------------------------------------------------------------
| Rename
|---------------------------------------------------------------
*/

if(isset($_POST['rename'],$_POST['name'])){
	$postGuid = $_POST['name'];	// Kein mysql_string_escape nötig da diese Variable nicht im SQL benutzt wird.
	$found = false;
	$renameSuccess = false;
	$renameErrors = array();
	$charGuid = 0;
	
	if($char_rename_points > $userObject->votePoints){
	    $renameErrors[] = "Du hast nicht genug Alive-Cash um dir einen Namenswechsel zu kaufen.";
	}
	
	foreach($characters as $char){
		if($char["guid"] == $postGuid){
			$charGuid = $char["guid"];
			$found = true;
			
			if($char["at_login"] & AT_LOGIN_RENAME)
				$renameErrors[] = "Der gew&uuml;nschte Charakter hat bereits einen Rename, bitte logge dich ein und schlie&szlig;e diesen zuerst ab.";	
		
			if($char["online"] == 1)
				$renameErrors[] = "Der gew&uuml;nschte Charakter ist gerade online, bitte melde dich zuerst im Spiel ab.";	
		}
	}

	if($found == false){
		$renameErrors[] = "Der gew&uuml;nschte Charakter wurde nicht gefunden.";	
	}

	if(count($renameErrors) == 0){
		
		$CHDB->query("UPDATE `characters` SET `at_login` = `at_login` | ".AT_LOGIN_RENAME." WHERE `guid` = ".$charGuid." LIMIT 1;");
		
		if(empty($CHDB->errmsg)){
			$renameSuccess = true;
			$userObject->changeVotePoints(-$char_rename_points);
		}
		else{
			$renameErrors[] = "Es gab einen Datenbankfehler, bitte es versuch es später noch einmal. ".$CHDB->errmsg;
		}
	}
}
/*
|---------------------------------------------------------------
| Race Change
|---------------------------------------------------------------
*/

if(isset($_POST['race_change'],$_POST['char_r_name'])){
	$postGuid = $_POST['char_r_name'];	// Kein mysql_string_escape nötig da diese Variable nicht im SQL benutzt wird.
	$found = false;
	$raceChangeSuccess = false;
	$raceChangeErrors = array();
	$charGuid = 0;
	
	if($char_race_points > $userObject->votePoints){
	    $raceChangeErrors[] = "Du hast nicht genug Alive-Cash um die Rasse deines Charakters zu ändern.";
	}
	
	foreach($characters as $char){
		if($char["guid"] == $postGuid){
			$charGuid = $char["guid"];
			$found = true;
			
			if($char["at_login"] & AT_LOGIN_CHANGE_RACE)
				$raceChangeErrors[] = "Der gew&uuml;nschte Charakter hat bereits einen Rassenwechsel, bitte logge dich ein und schlie&szlig;e diesen zuerst ab.";	
		
			if($char["online"] == 1)
				$raceChangeErrors[] = "Der gew&uuml;nschte Charakter ist gerade online, bitte melde dich zuerst im Spiel ab.";	
		}
	}
	
	if($found == false){
		$raceChangeErrors[] = "Der gew&uuml;nschte Charakter wurde nicht gefunden.";	
	}
	
	if(count($raceChangeErrors) == 0){
	
		$CHDB->query("UPDATE `characters` SET `at_login` = `at_login` | ".AT_LOGIN_CHANGE_RACE." WHERE `guid` = ".$charGuid." LIMIT 1;");
		
		if(empty($CHDB->errmsg)){
			$raceChangeSuccess = true;
			$userObject->changeVotePoints(-$char_race_points);
		}
		else{
			$raceChangeErrors[] = "Es gab einen Datenbankfehler, bitte es versuch es später noch einmal. ".$CHDB->errmsg;
		}
	}
}
/*
|---------------------------------------------------------------
| Faction Change
|---------------------------------------------------------------
*/

if(isset($_POST['faction_change'],$_POST['char_f_name'])){
	$postGuid = $_POST['char_f_name'];	// Kein mysql_string_escape nötig da diese Variable nicht im SQL benutzt wird.
	$found = false;
	$factionChangeSuccess = false;
	$factionChangeErrors = array();
	$charGuid = 0;
	
	if($char_faction_points > $userObject->votePoints){
	    $factionChangeErrors[] = "Du hast nicht genug Alive-Cash um dir einen Fraktionswechsel zu kaufen.";
	}
	
	foreach($characters as $char){
		if($char["guid"] == $postGuid){
			$charGuid = $char["guid"];
			$found = true;
			
			if($char["at_login"] & AT_LOGIN_CHANGE_FACTION)
				$factionChangeErrors[] = "Der gew&uuml;nschte Charakter hat bereits einen Fraktionstransfer, bitte logge dich ein und schlie&szlig;e diesen zuerst ab.";	
		
			if($char["online"] == 1)
				$factionChangeErrors[] = "Der gew&uuml;nschte Charakter ist gerade online, bitte melde dich zuerst im Spiel ab.";	
		}
	}
	
	if($found == false){
		$factionChangeErrors[] = "Der gew&uuml;nschte Charakter wurde nicht gefunden.";	
	}
	
	if(count($factionChangeErrors) == 0){
	
		$CHDB->query("UPDATE `characters` SET `at_login` = `at_login` | ".AT_LOGIN_CHANGE_FACTION." WHERE `guid` = ".$charGuid." LIMIT 1;");
		
		if(empty($CHDB->errmsg)){
			$factionChangeSuccess = true;
			$userObject->changeVotePoints(-$char_faction_points);
		}
		else{
			$factionChangeErrors[] = "Es gab einen Datenbankfehler, bitte es versuch es später noch einmal. ".$CHDB->errmsg;
		}
	}
}
/*
|---------------------------------------------------------------
| Re-customization
|---------------------------------------------------------------
*/

if(isset($_POST['customize'],$_POST['char_c_name'])){
	$postGuid = $_POST['char_c_name'];	// Kein mysql_string_escape nötig da diese Variable nicht im SQL benutzt wird.
	$found = false;
	$customSuccess = false;
	$customErrors = array();
	$charGuid = 0;
	
	if($char_custom_points > $userObject->votePoints){
	    $customErrors[] = "Du hast nicht genug Alive-Cash um die Rasse deines Charakters zu ändern.";
	}
	
	foreach($characters as $char){
		if($char["guid"] == $postGuid){
			$charGuid = $char["guid"];
			$found = true;
			
			if($char["at_login"] & AT_LOGIN_CUSTOMIZE)
				$customErrors[] = "Der gew&uuml;nschte Charakter hat bereits eine Charakter anpassung, bitte logge dich ein und schlie&szlig;e diese zuerst ab.";	
		
			if($char["online"] == 1)
				$customErrors[] = "Der gew&uuml;nschte Charakter ist gerade online, bitte melde dich zuerst im Spiel ab.";	
		}
	}
	
	if($found == false){
		$customErrors[] = "Der gew&uuml;nschte Charakter wurde nicht gefunden.";	
	}
	
	if(count($customErrors) == 0){
	
		$CHDB->query("UPDATE `characters` SET `at_login` = `at_login` | ".AT_LOGIN_CUSTOMIZE." WHERE `guid` = ".$charGuid." LIMIT 1;");
		
		if(empty($CHDB->errmsg)){
			$customSuccess = true;
			$userObject->changeVotePoints(-$char_custom_points);
		}
		else{
			$customErrors[] = "Es gab einen Datenbankfehler, bitte es versuch es später noch einmal. ".$CHDB->errmsg;
		}
	}
}