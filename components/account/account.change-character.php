	<div id="user-plate" class="user-plate <?=$userObject->faction_css?> ajax-update" style=" <?=$userObject->active_char["bg"]?>">
		<div class="card-overlay"></div>
		<a href="<?=url_for("account","manage");?>" rel="np" class="profile-link">
			<span class="hover"></span>
		</a>
		<div class="user-meta">
			<div class="player-name">
				<a href="<?=url_for("account","manage");?>">
					<?=$userObject->getName(); ?>
				</a>
			</div>
			<div class="character">
				<a class="character-name context-link" rel="np" href="<?=$userObject->active_char["url"]?>">
				<? 
				if(empty($userObject->active_char["name"]))
					echo "Wähle einen Charakter";
				else
					echo $userObject->active_char["name"];
				?>
					<span class="arrow"></span>
				</a>
				<div id="context-1" class="ui-context character-select">
					<div class="context">
						<a href="javascript:;" class="close" onclick="return CharSelect.close(this);"></a>
						<div class="context-user">
				<? if(!empty($userObject->active_char["name"])){ ?>
							<strong><?=$userObject->active_char["name"]?></strong><br />
				<? } ?>
							<span><?=$char['realm']?></span>
						</div>
						<div class="context-links">
							<a href="<?=$userObject->active_char["url"]?>" title="Profile" rel="np" class="icon-profile link-first">Profil</a>
						<!--	<a href="search.php?$session[sessionurl]do=finduser&u=$bbuserinfo[userid]" title="Zeige meine Beiträge" rel="np" class="icon-posts"> </a> -->
							<a href="<?=url_for("account","chartools");?>" title="Charakter Tools" rel="np"
class="icon-auctions"></a>
		<!--					<a href="calendar.php" title="$vbphrase[calendar]" rel="np" class="icon-events link-last"></a>
		-->
						</div>
					</div>
					<div class="character-list">
						<div class="primary chars-pane">
							<div class="char-wrapper">
								<a href="javascript:;" class="char pinned" rel="np">
									<span class="pin"></span>
									<span class="name"><?=$userObject->active_char["name"]?></span>
									<span class="class wow-class-<?=$userObject->active_char['class']?>"><?=$userObject->active_char['level'].' '.$userObject->active_char['race'].' '.$userObject->active_char['class_label']?></span>
								</a>
								<? foreach($userObject->characters as $char_guid => $char){ 
									if($char["guid"] == $userObject->active_char["guid"])
										continue;
									?>
								<a href="" class="char" onclick="CharSelect.pin(<?=$char["guid"]?>, this); return false;" rel="np">
									<span class="pin"></span>
									<span class="name"><?=$char["name"]?></span>
									<span class="class wow-class-<?=$char['class']?>"><?=$char['level'].' '.$char['race'].' '.$char['class_label']?></span>
								</a>
								<? } ?>
							</div>
						</div>
						<div class="secondary chars-pane" style="display: none"></div>
					</div> <!-- /character-list --> 
				 </div>  <!-- /context-1 -->
			</div> <!-- /character -->
			<div class="guild">
				<? if(!empty($userObject->active_char["guild"])){ ?>
				<a class="guild-name" href="<?=$userObject->active_char["guild_url"]?>">
					<?=$userObject->active_char["guild"]?>
				</a>
				<? } ?>
			</div>
		</div>
	</div>