<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>"Raid ID Verlängerung",'link'=>'');
// ==================== //

$MANG = new Mangos;

// Here we chack to see if user is logged in, if not, then redirect to account login screen
if($user['id']<=0){
    redirect(url_for("account", "login"),1);
}

?>
<?php
// Here we see if the site admin has the rename system enabled
if ((int)$MW->getConfig->character_tools->id_extend == 0){
    redirect(url_for("account", "login"),1);
	exit;
}

session_start();
$_SESSION["user_id"] = $user['id'];
$_SESSION["user_name"] = $user['username'];
$_SESSION["char_name"] = $user['character_name'];		

$today = date("Ymd");
$_SESSION["date"] = $today;
$extend_points = (int)$MW->getConfig->character_tools->id_extend_points;

// Table voting_points
$get_voting_points = $DB->select("SELECT `points`, `date`, `date_points` FROM `voting_points` WHERE `id` = ".$_SESSION["user_id"]." LIMIT 1");

// Character
$char_row = $DB->select("SELECT `guid` FROM `characters` WHERE `name` LIKE '".$_SESSION["char_name"]."' LIMIT 1");

$char_id = $char_row["guid"];

if (count($get_voting_points) > 0)
{
	foreach ($get_voting_points as $row) 
	{
		$_SESSION["points"] = $row["points"];
		if($row["date"] <> $today)
		{
			$DB->query("UPDATE `voting_points` SET `date` = '".$today."', `date_points` = 0 WHERE `id` = ".$_SESSION["user_id"]." LIMIT 1");
			$_SESSION["date"] = $today;
			$_SESSION["date_points"] = 0;
		}
		else
		{
			$_SESSION["date"] = $row["date"];
			$_SESSION["date_points"] = $row["date_points"];
		}
	}
}
else
{
	$DB->query("INSERT INTO `voting_points` (`id`, `points`, `date`, `date_points`) VALUES (".$_SESSION["user_id"].", 200, 20100101, 0)");
	$_SESSION["points"] = 200;
	$_SESSION["date_points"] = 0;
}


// Get Instance Information
$instance_rows = $DB->select("SELECT ci.`guid`, ci.instance`, i.map, i.difficulty, i.completedEncounters  
	FROM `character_instance` as ci, `instance` as i 
	WHERE ci.`guid` = '".$char_id."' AND ci.instance = i.id AND ci.permanent = 1;");

$instance_ids = array();
foreach($instance_rows as $row){
	if($row["map"] == 631){
		$name = "Eiskronenzitdelle";
	}
	else
		continue;
	
	switch($row["difficulty"]){
		case 0:
			$difficulty = "Normal 10"; break;
		case 1:
			$difficulty = "Normal 25"; break;
		case 2:
			$difficulty = "Hero 10"; break;
		case 3:
			$difficulty = "Hero 25"; break;
	}	
		
	$instance_ids[$row["instance"]] = array(
		"name" => $name,
		"difficulty" => $difficulty,
		"bosses" => $row["completedEncounters"],
	);
}

/*
char_guid, instance_guid, map, difficulty, completedEncounters, data
*/
?>
