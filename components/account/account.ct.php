<?php
if(INCLUDED!==true)exit;

error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
ini_set( 'display_errors', 'stdout' ) ;

if(!function_exists('save_session_start'))
	require_once($_SERVER['DOCUMENT_ROOT']."/core/common.php");
require_once($_SERVER['DOCUMENT_ROOT']."/core/helpers.php");
	
save_session_start();

$show_account_sidebar = true;


// ==================== //
$pathway_info[] = array('title'=>$lang['ct'], 'link'=> url_for("account", "ct"));
// ==================== //

if(!$userObject->isLoggedIn()){
    redirect('/account/login',1);
	exit;
}

$disableTransfer = false;
$transferSuccess = false;
$costTransfer = $MW->getConfig->character_tools->character_transfer;

if(empty($_SESSION['user_actionkey']))
	$_SESSION['user_actionkey'] = rand();


$actionKey = $_SESSION['user_actionkey'];
$actionKeyPost = isset($_POST['actionkey']) ? $_POST['actionkey'] : 0;

if(!isset($userObject->votePoints)  
	|| $userObject->votePoints < $costTransfer){
	$disableTransfer = true;
}

/*
|---------------------------------------------------------------
| Transferformular abgeschickt
|---------------------------------------------------------------
*/
if($actionKeyPost > 0){

	$transferErrors = array();
	
	$TransDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
		":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
		"/" . "chartrans" . "" ) ;
	if ( $TransDB ){
		$TransDB->setErrorHandler( 'databaseErrorHandler' ) ;
		$TransDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
	}
	$server = $mangos['db_host'] . ":" . $mangos['db_port'];
	$db_user = $mangos['db_username'];
	$db_pass = $mangos['db_password'];
	$database = "chartrans";
	                 

	$conn = mysql_connect($server, $db_user, $db_pass) or die("Connection failed: ". mysql_error());
	mysql_select_db($database, $conn) or die("Select DB failed: " . mysql_error());	
	
	
	if(!isset($userObject->votePoints)  
		|| $userObject->votePoints < $costTransfer){
		$transferErrors[] = "Du hast nicht genug Punkte für einen Transfer.";
	}
	
	if(empty($_SESSION['user_actionkey']) || $actionKeyPost == 0){
		$transferErrors[] = "Neuladen der Seite wurde unterbunden.";
	}
	
	if($_SESSION['user_actionkey'] != $actionKeyPost){
		$transferErrors[] = "Ungültiger Seitenaufruf.";
	}
	
	
	
	/*
	|---------------------------------------------------------------
	| Transferdaten
	|---------------------------------------------------------------
	*/
	
	$post_data = array();
	
	// SQL Injections vermeiden (grob zumindest)
	foreach($_POST as $key => $value)
		$post_data[$key] = mysql_escape_string($value);
	
	
	//Account
	$Accountname = $post_data['Accountname'];
	$Charactername = $post_data['Charactername'];
	$Rasse = $post_data['Rasse'];
	$Klasse = $post_data['Klasse'];
	$icq = $post_data['icq'];
	$Server = $post_data['Server'];
	$Link = $post_data['Link'];
	$Amory = $post_data['Amory'];
	
	// Schon mal eingetragen
	$sql = mysql_query("SELECT `id` FROM `formular_basis` WHERE `char` = '$Charactername' AND `server` = '$Server' AND `id` > 2000 ORDER BY `id` DESC LIMIT 1;");
	if($res = mysql_fetch_array($sql)){
		$transferErrors[] = 'Dieser Charakter wurde bereits einmal transferiert.';
	}
	
	
	// Pflichtfelder
	if(empty($Accountname) || empty($Charactername) || empty($Server) || empty($Link)){
		$transferErrors[] = 'Fehlende Eingabe bei deinem Account, Charakter oder Serverlink';
	}
	// Ueberprüfung auf Eingabe vom Screenlink
	$Download=$post_data['Download'];
	if( empty($Download) || $Download == "http://" ){
		$transferErrors[] = 'Screenshots vergessen!';
	}
	
	//Bemerkung
	$Bemerkung=$post_data['Bemerkung'];
	
	//Charinfo
	$Level=$post_data['Level'];
	$Gold=$post_data['Gold'];
	$Reiten=$post_data['Reiten'];
	$Mount_boden=$post_data['Mount_boden'];
	$Mount_flug=$post_data['Mount_flug'];
	
	//Beruf1
	$Beruf1=$post_data['Beruf1'];
	$Beruf1_skill=$post_data['Beruf1_skill'];
	//Beruf2
	$Beruf2=$post_data['Beruf2'];
	$Beruf2_skill=$post_data['Beruf2_skill'];
					   
	//Sonstige Berufe
	$Kochen=$post_data['Kochen'];
	$Angeln=$post_data['Angeln'];
	$Erstehilfe=$post_data['Erstehilfe'];
	//Equip 
	$Kopf=$post_data['Kopf'];
	$Hals=$post_data['Hals'];
	$Schulter=$post_data['Schulter'];
	$Ruecken=$post_data['Ruecken'];
	$Brust=$post_data['Brust'];
	$Wappenrock=$post_data['Wappenrock'];
	$Handgelenke=$post_data['Handgelenke'];
	$Haende=$post_data['Haende'];
	$Taille=$post_data['Taille'];
	$Beine=$post_data['Beine'];
	$Fuesse=$post_data['Fuesse'];
	$Ring1=$post_data['Ring1'];
	$Ring2=$post_data['Ring2'];
	$Schmuck1=$post_data['Schmuck1'];
	$Schmuck2=$post_data['Schmuck2'];
	$Waffenhand=$post_data['Waffenhand'];
	$Nebenhand=$post_data['Nebenhand'];
	$Distanzwaffe=$post_data['Distanzwaffe'];
	//Random items ID
	$Ri1=$post_data['Ri1'];
	$Ri2=$post_data['Ri2'];
	$Ri3=$post_data['Ri3'];
	$Ri4=$post_data['Ri4'];
	$Ri5=$post_data['Ri5'];
	$Ri6=$post_data['Ri6'];
	$Ri7=$post_data['Ri7'];
	$Ri8=$post_data['Ri8'];
	$Ri9=$post_data['Ri9'];
	$Ri10=$post_data['Ri10'];
	//Random items Anz
	$Ri1a=$post_data['Ri1a'];
	$Ri2a=$post_data['Ri2a'];
	$Ri3a=$post_data['Ri3a'];
	$Ri4a=$post_data['Ri4a'];
	$Ri5a=$post_data['Ri5a'];
	$Ri6a=$post_data['Ri6a'];
	$Ri7a=$post_data['Ri7a'];
	$Ri8a=$post_data['Ri8a'];
	$Ri9a=$post_data['Ri9a'];
	$Ri10a=$post_data['Ri10a'];
	
	// Ruf
	$Argentumkreuzung=$post_data['1106'];
	$Der_Silberbund=$post_data['1094'];
	$Der_Wyrmruhpakt=$post_data['1091'];
	$Die_Frosterben=$post_data['1126'];
	$Die_Hand_der_Rache=$post_data['1067'];
	$Die_Kalu_ak=$post_data['1073'];
	$Die_Orakel=$post_data['1105'];
	$Die_Soehne_Hodir=$post_data['1119'];
	$Die_Sonnenhaescher=$post_data['1124'];
	$Die_Taunka=$post_data['1064'];
	$Expedion_der_Horde=$post_data['1052'];
	$Expedion_Valianz=$post_data['1050'];
	$Forscherliga=$post_data['1068'];
	$Kirin_Tor=$post_data['1090'];
	$Kriegshymnenoffensive=$post_data['1085'];
	$Ritter_der_schwarzen_Klinge=$post_data['1098'];
	$Shen_dralar=$post_data['809'];
	$Stamm_der_Wildherzen=$post_data['1104'];
	$Vorposten_der_Allianz=$post_data['1037'];
	$Sturmwind=$post_data['72'];
	$Die_Exodar=$post_data['930'];
	$Eisenschmiede=$post_data['47'];
	$Gnomeregangnome=$post_data['54'];
	$Darnassus=$post_data['69'];
	$Orgrimmar=$post_data['76'];
	$Silbermond=$post_data['911'];
	$Unterstadt=$post_data['68'];
	$Donnerfels=$post_data['81'];
	$Dunkelspeertrolle=$post_data['530'];
	$Das_Konsortium=$post_data['933'];
	$Das_Violette_Auge=$post_data['967'];
	$Die_Todeshoerigen=$post_data['1012'];
	$Die_Waechter_der_Sande=$post_data['990'];
	$Ehrenfeste=$post_data['946'];
	$Expedition_des_Cenarius=$post_data['942'];
	$Hueter_der_Zeit=$post_data['989'];
	$Kurenai=$post_data['978'];
	$Netherschwingen=$post_data['1015'];
	$Ogrila=$post_data['1038'];
	$Sporeggar=$post_data['970'];
	$Thrallmar=$post_data['947'];
	$Verdikt=$post_data['1156'];
	
	$factions = array("Argentumkreuzung", "Der_Silberbund", "Der_Wyrmruhpakt", "Die_Frosterben", "Die_Hand_der_Rache", "Die_Kalu_ak", "Die_Orakel", "Die_Soehne_Hodir", "Die_Sonnenhaescher", "Die_Taunka", "Expedion_der_Horde", "Expedion_Valianz", "Forscherliga", "Kirin_Tor", "Kriegshymnenoffensive", "Ritter_der_schwarzen_Klinge", "Shen_dralar", "Stamm_der_Wildherzen", "Vorposten_der_Allianz", "Sturmwind", "Die_Exodar", "Eisenschmiede", "Gnomeregangnome", "Darnassus", "Orgrimmar", "Silbermond", "Unterstadt", "Donnerfels", "Dunkelspeertrolle", "Das_Konsortium", "Das_Violette_Auge", "Die_Todeshoerigen", "Die_Waechter_der_Sande", "Ehrenfeste", "Expedition_des_Cenarius", "Hueter_der_Zeit", "Kurenai", "Netherschwingen", "Ogrila", "Sporeggar", "Thrallmar", "Verdikt");
	
	foreach($factions as $faction_name){
		$post = $$faction_name;
		$value = 0;
		switch($post){
			case "keinen Ruf": 
				$value = 0; break;
			case "Ehrfuerchtig": 
				$value = 99999; break;
			case "Freundlich": 
				$value = 6001; break;
			case "Wohlwollend": 
				$value = 12001; break;
			case "Respektvoll": 
				$value = 21000; break;
			default: 
				$value = 0; break;
		}
		
		$$faction_name = $value;
	}
	// Check Item Levels
	$slots = array('Kopf', 'Hals', 'Schulter', 'Ruecken', 'Brust', 'Wappenrock', 'Handgelenke', 'Haende', 'Taille', 'Beine', 'Fuesse', 
			'Ring1', 'Ring2', 'Schmuck1', 'Schmuck2', 
			'Waffenhand', 'Nebenhand', 'Distanzwaffe',
			'Ri1', 'Ri1a','Ri2', 'Ri2a','Ri3', 'Ri3a','Ri4', 'Ri4a','Ri5', 'Ri5a','Ri6', 'Ri6a','Ri7', 'Ri7a','Ri8', 'Ri8a','Ri9', 'Ri9a','Ri10', 'Ri10a');
	$itemIds = array();
	foreach($slots as $slot){
		if($$slot > 0)
			$itemIds[$slot] = $$slot;
	}
	
	if(count($itemIds)){
		$itemLevelRows = $WSDB->select("SELECT * FROM item_template WHERE `entry` IN(".implode(", ",$itemIds).");");

		foreach($itemLevelRows as $row){
			$slot_name = array_search($row["entry"], $itemIds);
			if(substr_count($slot_name, "Ring") == 0)
				$slot_name = str_replace("Ri", "Random Item ", $slot_name);
			
			if($row["Quality"] == 5 || $row["Quality"] == 6){
				$transferErrors[] = "Achtung, du hast bei ".$slot_name." ein Legend&auml;res-/Artefakt-Item ausgewählt, diese werden <b>nicht</b> transferiert. Bitte such dir ein anderes Item aus.";
			}
			if($row["ItemLevel"] >= 264){
				$transferErrors[] = "Achtung, du hast bei ".$slot_name." ein Item mit Stufe 264 oder h&ouml;her ausgewählt. Bitte such dir ein anderes Item aus.";
			}
			if($row["ItemLevel"] == 251){
				$isPVP = false;
				
				for($i = 1; $i <= 10; $i++){
					if($row["stat_type".$i] == 35 && $row["stat_value".$i] > 0)
						$isPVP = true;
				}
				
				if($isPVP == false)
					$transferErrors[] = "Achtung, du hast bei ".$slot_name." ein Item mit Stufe 251 ausgewählt welches kein PVP-Item ist. Bitte such dir ein anderes Item aus.";
			}
			
		}
	}
	
	//Datum
	$timestamp = time();
	$datum=date("d.m.y h:m");
	
	if(count($transferErrors) > 0){
	
	}
	else{
		//MYSQL eintragem
		$sql = "INSERT INTO formular_basis 
			(`account_id`, `character_guid`, `name`, `char`, `icq`, 
			`rasse`, `klasse`, `server`, `serverlink`, `armorylink`, `screen`, `bemerkung`, 
			`level`, `gold`, `reiten`, 
			`beruf1`, `beruf1skill`, 
			`beruf2`, `beruf2skill`, 
			`erstehilfe`, `angeln`, `kochen`, `datum`, `gm`) VALUES 
			( '$account_id', 0, '$Accountname', '$Charactername', '$icq',  
			'$Rasse', '$Klasse', '$Server', '$Link', '$Amory', '$Download', '$Bemerkung', 
			'$Level', '$Gold', '$Reiten', 
			'$Beruf1', '$Beruf1_skill', 
			'$Beruf2', '$Beruf2_skill', 
			'$Erstehilfe', '$Angeln', '$Kochen', '$datum', 'nicht bearbeitet')";
		$result = mysql_query($sql) or die (mysql_error());
		
		$sql2 = mysql_query("SELECT `id` FROM `formular_basis` WHERE `char` = '$Charactername' AND `account_id` = '$account_id' ORDER BY `id` DESC LIMIT 1;");
		if($res = mysql_fetch_array($sql2))
			$transfer_id = $res['id'];
	
	
		$sql3 ="INSERT INTO formular_item (
			`id`, `char`, 
			`kopf`, hals, schulter, ruecken, brust, wappenrock, handgelenke, haende, taille, beine, fuesse, 
			ring1, ring2, schmuck1, schmuck2, 
			waffenhand, nebenhand, distanzwaffe, 
			mount_b, mount_f ) 
			VALUES (
			'$transfer_id', '$Charactername', 
			'$Kopf', '$Hals', '$Schulter', '$Ruecken', '$Brust', '$Wappenrock', '$Handgelenke', '$Haende', '$Taille', '$Beine', '$Fuesse', 
			'$Ring1', '$Ring2', '$Schmuck1', '$Schmuck2', 
			'$Waffenhand', '$Nebenhand', '$Distanzwaffe',
			'$Mount_boden','$Mount_flug');";
		
		$err = mysql_query($sql3) or die('Es wurden nicht alle Felder ausgef&uuml;llt. Bitte nutze den Zur&uuml;ck Button deines Browsers!<br>
		Solltest du Felder leerlassen f&uuml;lle diese bitte mit 0 aus, Dankesch&ouml;n. <br>');
		
		
		$sql4="INSERT INTO formular_randomitem(
			`id`, `char`, 
			ri1, ri1a, ri2, ri2a, ri3, ri3a, ri4, ri4a, ri5, ri5a, ri6, ri6a, ri7, ri7a, ri8, ri8a, ri9, ri9a, ri10, ri10a) 
			VALUES (
			'$transfer_id', '$Charactername',
			'$Ri1', '1','$Ri2', '1','$Ri3', '1','$Ri4', '1','$Ri5', '1','$Ri6', '1','$Ri7', '1','$Ri8', '1','$Ri9', '1','$Ri10', '1')";
		
		$err= mysql_query($sql4) or die('Es wurden nicht alle Felder ausgef&uuml;llt. Bitte nutze den Zur&uuml;ck Button deines Browsers!<br>
		Solltest du Felder leerlassen f&uuml;lle diese bitte mit 0 aus, Dankesch&ouml;n. <br>');
		
		
		$sql5= "INSERT INTO formular_ruf (
			`id`, `char`,
			Argentumkreuzung, Der_Silberbund, Der_Wyrmruhpakt, Die_Frosterben, Die_Hand_der_Rache, Die_Kalu_ak, Die_Orakel, Die_Soehne_Hodir, Die_Sonnenhaescher, Die_Taunka, Expedion_der_Horde, Expedion_Valianz, Forscherliga, Kirin_Tor, Kriegshymnenoffensive, Ritter_der_schwarzen_Klinge, Shen_dralar, Stamm_der_Wildherzen, Vorposten_der_Allianz, Sturmwind, Die_Exodar, Eisenschmiede, Gnomeregangnome, Darnassus, Orgrimmar, Silbermond, Unterstadt, Donnerfels, Dunkelspeertrolle, Das_Konsortium, Das_Violette_Auge, Die_Todeshoerigen, Die_Waechter_der_Sande, Ehrenfeste, Expedition_des_Cenarius, Hueter_der_Zeit, Kurenai, Netherschwingen, Ogrila, Sporeggar, Thrallmar, Verdikt) 
			VALUES (
			'$transfer_id', '$Charactername', 
			'$Argentumkreuzung', '$Der_Silberbund', '$Der_Wyrmruhpakt', '$Die_Frosterben', '$Die_Hand_der_Rache', '$Die_Kalu_ak', '$Die_Orakel', '$Die_Soehne_Hodir', '$Die_Sonnenhaescher', '$Die_Taunka', '$Expedion_der_Horde', '$Expedion_Valianz', '$Forscherliga', '$Kirin_Tor', '$Kriegshymnenoffensive', '$Ritter_der_schwarzen_Klinge', '$Shen_dralar', '$Stamm_der_Wildherzen', '$Vorposten_der_Allianz', '$Sturmwind', '$Die_Exodar', '$Eisenschmiede', '$Gnomeregangnome', '$Darnassus', '$Orgrimmar', '$Silbermond', '$Unterstadt', '$Donnerfels', '$Dunkelspeertrolle', '$Das_Konsortium', '$Das_Violette_Auge', '$Die_Todeshoerigen', '$Die_Waechter_der_Sande', '$Ehrenfeste', '$Expedition_des_Cenarius', '$Hueter_der_Zeit', '$Kurenai', '$Netherschwingen', '$Ogrila', '$Sporeggar', '$Thrallmar', '$Verdikt');";
		
		$err= mysql_query($sql5) or die('Es wurden nicht alle Felder ausgef&uuml;llt. Bitte nutze den Zur&uuml;ck Button deines Browsers!<br>
		Solltest du Felder leerlassen f&uuml;lle diese bitte mit 0 aus, Dankesch&ouml;n. <br>');
		
		//$_SESSION['user_actionkey'] = rand();
		//$post_data = array();
	
		$userObject->changeVotePoints(-$costTransfer);
		$transferSuccess = true;			
	}
}