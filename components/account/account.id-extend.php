<?php
if(INCLUDED!==true)exit;
// ==================== //
$pathway_info[] = array('title'=>"Raid ID Verlängerung",'link'=>'');
// ==================== //

// Here we check to see if user is logged in, if not, then redirect to account login screen
if($user['id']<=0){
    redirect('index.php?n=account&sub=login',1);
	exit;
}

error_reporting(E_ALL);

// If function is disabled => redirect
if ((int)$MW->getConfig->character_tools->id_extend == 0){
    redirect('index.php?n=account&sub=login',1);
	exit;
}

$_SESSION["user_id"] = $user['id'];
$_SESSION["user_name"] = $user['username'];
$_SESSION["char_name"] = $user['character_name'];		

$today = date("Ymd");
$_SESSION["date"] = $today;
$extendCost = (int)$MW->getConfig->character_tools->id_extend_points;
$errorMessage = "";
$successMessage = "";

// Data Database
$DataDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
		":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
		"/" . "data" . "" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$DataDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}
else{
	echo "Es konnte keine Verbindung zur Datenbank hergestellt werden.";
	exit;	
}
		
// Table voting_points
$get_voting_points = $userObject->votePoints;

// Character
$result = $CHDB->select("SELECT `guid` FROM `characters` WHERE `name` = '".$_SESSION["char_name"]."' LIMIT 1");

$charId = 0;
$char_row = array();

foreach($result as $row)
	$char_row = $row;

if(empty($char_row)){
	die("Der Charakter wurde nicht gefunden.");
}
$charId = $char_row["guid"];

// Get active IDs 
$instanceRows = $CHDB->select("SELECT 
	ci.`guid`, ci.`instance`, 
	i.`map`, i.`difficulty`, i.`completedEncounters`, i.`data`   
	FROM `character_instance` as ci, `instance` as i 
	WHERE ci.`guid` = '".$charId."' AND ci.instance = i.id AND ci.permanent = 1;");

// Get extended IDs
$extendedRows = $DataDB->select("SELECT * FROM instance_extends WHERE characterId = $charId;");	

$extendedIDs = array();
if(count($result) > 0){
	foreach($extendedRows as $row){
		$name = "";
		switch($row["map"]){
			case 631:
				$name = "Eiskronenzitadelle"; break;
		}
			
		$extendedIDs[$row["instanceId"]] = $row;
		$extendedIDs[$row["instanceId"]]["name"] = $name;
		$extendedIDs[$row["instanceId"]]["label_difficulty"] = getDifficultyLabel($row["difficulty"]);	
	}
}	

$raidIds = array();
foreach($instanceRows as $row){
	
	$instanceID = $row["instance"];
	
	if($row["map"] == 631){
		$name = "Eiskronenzitadelle";
	}
	else
		continue;
	
	// Is this ID already extended?
	$extended = (isset($extendedIDs[$instanceID])) ? true : false;
	
	// Find other characters with this ID		
	$result = $CHDB->select("SELECT * FROM characters, character_instance WHERE character_instance.instance = '".$instanceID."' AND characters.guid = character_instance.guid;");
	
	$otherChars = array();
	if(count($result) > 0){
		foreach($result as $othersRow)
			$otherChars[] = $othersRow["name"];
	}
	$difficulty = getDifficultyLabel($row["difficulty"]);
		
	$raidIds[$row["instance"]] = array(
		"name" => $name,
		"label_difficulty" => $difficulty,
		"completedEncounters" => $row["completedEncounters"],
		"map" => $row["map"],
		"difficulty" => $row["difficulty"],
		"data" => $row["data"],
		"others" => implode(", ", $otherChars),
		"extended" => $extended,
	);
}

// Formular has been sent?
if(isset($_POST['extend'],$_POST['instance_id'])){
	
	$raidId = mysql_escape_string($_POST['instance_id']);

	if(empty($raidIds[$raidId])){
		$errorMessage = "Dieser Charakter hat diese ID nicht.";
	}
	elseif($raidIds[$raidId]["extended"] == true){
		$errorMessage = "Diese ID ist bereits verlängert.";	
	}
	else{
		$instance = $raidIds[$raidId];
		$result = $DataDB->query('INSERT INTO instance_extends (instanceId, characterId, map, difficulty, completedEncounters, data) VALUES 
		('.$raidId.', '.$charId.','.$instance["map"].', '.$instance["difficulty"].', '.$instance["completedEncounters"].', "'.$instance["data"].'")');
		
		if(count($result)){
			$successMessage = "Die ID wurde verlängert";
			$raidIds[$raidId]["extended"] = true;	
		}
		else
			$errorMessage = "Beim Speichern lief leider etwas schief, bitte versuch es später noch einmal.";
	}
	
		
}

function getDifficultyLabel($diff){
	switch($diff){
		case 0:
			$difficulty = "Normal 10"; break;
		case 1:
			$difficulty = "Normal 25"; break;
		case 2:
			$difficulty = "Hero 10"; break;
		case 3:
			$difficulty = "Hero 25"; break;
		default:
			$difficulty = "Normal";
	}
	return $difficulty;
}
?>