<?php


$css_files[] = "/".$currtmp."/css/account.css";
$pathway_info[] = array( 'title' => "Account Dienste", 'link'=> url_for("account"));



$show_account_sidebar = false;
$account_sidebar = "";
if($userObject->theme == "Shattered-World"){
	$sidebar_file = $_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/account/account.sidebar.php";
	
	ob_start();
		include ( $sidebar_file ) ;
		$account_sidebar = ob_get_contents();
	ob_end_clean();

}


$com_content['account'] = array(
    'index' => array(
        '', // g_ option require for view 
        'account', // loc name
        url_for("account"),
        '', // main menu name/id ('' - not show)
        0 // show in context menu (1-yes,0-no)
    ),
    'pms' => array(
        'g_use_pm', 
        'personal_messages', 
        url_for("account", "pms"),
        '',
        1
    ),
    'manage' => array(
        '', 
        'account_manage', 
        url_for("account", "manage"),
        '2-menuAccount',
        0
    ),
    'connect' => array(
        '', 
        'account_connect', 
        url_for("account", "connect"),
        '',
        0
    ),
    'view' => array(
        'g_view_profile', 
        '', 
        url_for("account", "manage"),
        '',
        0
    ),
    'register' => array(
        '', 
        'account_create', 
        url_for("account", "register"),
        '2-menuAccount',
        0
    ),
    'activate' => array(
        '', 
        'activation', 
        url_for("account", "activate"),
        '2-menuAccount',
        0
    ),
    'restore' => array(
        '', 
        'retrieve_pass', 
        url_for("account", "restore"),
        '2-menuAccount',
        0
    ),
    'userlist' => array(
        '',
        'userlist',
        url_for("account", "userlist"),
        '4-menuInteractive',
        0
    ),
    'login' => array(
        '', 
        'login', 
        url_for("account", "login"),
        '',
        0
    ),
	'chartools' => array(
        '', 
        'chartools', 
        url_for("account", "chartools"),
        '',
        0
    ),
    'id-extend' => array(
        '', // g_ option require for view 
        'id-extend', // loc name
        url_for("account", "id_extend"),
        '', // main menu name/id ('' - not show)
        0 // show in context menu (1-yes,0-no)
    ),
	/*
    'id' => array(
        '', // g_ option require for view 
        'id', // loc name
        url_for("account", "id"),
        '', // main menu name/id ('' - not show)
        0 // show in context menu (1-yes,0-no)
    ),*/
    'charcreate' => array(
        '',
        'charcreate',
        url_for("account", "charcreate"),
        '',
        0
    ),
	'ct' => array(
        '',
        'ct',
        url_for("account", "ct"),
        '',
        0
    ),
	'auswertung' => array(
        '',
        'auswertung',
        url_for("account", "auswerttung"),
        '',
        0
    ),
  'fwfv' => array(
        '', 
        'fwfv',
        url_for("account", "fwfv"),
        '',
        0
    ),
);
?>
