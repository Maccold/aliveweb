<?php
if(INCLUDED!==true)
	exit;


include($_SERVER['DOCUMENT_ROOT']."/core/class.item.prototype.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.php");
include($_SERVER['DOCUMENT_ROOT']."/core/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");

$mode = false;
$noBody = true;
$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

if(false){ 
	include("../../core/defines.php"); 
	include("../../core/class.item.prototype.php"); 
	include("../../core/class.item.php"); 
	include("../../templates/offlike/tooltip/tooltip.item.php"); 
}

if(isset($debug) && $debug == 1){
	$debug = true;
}
else
	$debug = false;

if( !empty($item_id) ){
	$item_id = urldecode($item_id);
}
else{
	echo "Item nicht gefunden";
	exit;
}

$item = new Item($item_id, $debug);

$useDbCache = true;
$dbCacheData = array(
	"type" => CACHETYPE_TOOLTIP,
	"table" => "item_cache",
	"entry" => $item->entry,
	"timestamp" => time(),
);

$item_data = array();

if(isset($_GET["e"]) && is_numeric($_GET["e"]))
	$item_data["enchant"] = $_GET["e"];
if(isset($_GET["set"]))
	$item_data["set"] = $_GET["set"];
if(isset($_GET["d"]) && is_numeric($_GET["d"]))
	$item_data["d"] = $_GET["d"];

if(isset($_GET["g0"]) && is_numeric($_GET["g0"]))
	$item_data["g0"] = $_GET["g0"];

if(isset($_GET["g1"]) && is_numeric($_GET["g1"]))
	$item_data["g1"] = $_GET["g1"];

if(isset($_GET["g2"]) && is_numeric($_GET["g2"]))
	$item_data["g2"] = $_GET["g2"];
	
//debug("Get",$_GET);

if(empty($item_data) && false && $itemCache = $item->getCache()){
	
	$dbCacheData["content"] = $itemCache;
	
	return;
}
else{
	
	$dbCacheData["refresh"] = true;
	
	$item->CreateTooltip($item_data);

}
	