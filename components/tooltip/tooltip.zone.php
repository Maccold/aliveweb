<?php
if(INCLUDED!==true)exit;

include($_SERVER['DOCUMENT_ROOT']."/core/data/data.zones.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.zone.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");


if(false){
	include("../../core/data/data.zones.php");
	include("../../core/class.zone.php");
	include("../../templates/offlike/tooltip/tooltip.zone.php");
}

$mode = false;



if( !empty($zone) ){
	$zone = urldecode($zone);
}
else{
	die("");
}


$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

if(is_numeric($zone))
	$zone_id = $aDB->selectCell("SELECT id FROM armory_instance_template WHERE id = ?d", $zone);
else
	$zone_id = $aDB->selectCell("SELECT id FROM armory_instance_template WHERE label LIKE ?", $zone);


if(isset($data_zone[$zone_id]))
{
	
	$zone_data = $data_zone[$zone_id];
	
	$zone = new Zone($zone_id, $zone_data);
}
else{
	die("");
}
