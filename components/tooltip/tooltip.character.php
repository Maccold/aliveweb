<?php
if(INCLUDED!==true)exit;

$mode = false;

if( !empty($name) ){
	$name = urldecode($name);
}
else{
	exit;
}

$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

// Find character
if(is_numeric($name)){
	$rows = $CHDB->select('
	SELECT guid, name, class, level, race, gender
		FROM characters  
		WHERE guid = "'.$name.'" LIMIT 1;');
}
else{
	$rows = $CHDB->select('
	SELECT guid, name, class, level, race, gender
		FROM characters  
		WHERE name LIKE "'.$name.'" LIMIT 1;');
}

$char = $rows[0];
$char["raceLabel"] = getRaceLabel($char["race"], $char["gender"]);
$char["classLabel"] = (getClassLabel($char["class"], $char["gender"]));
$char["achievementPoints"] = 0;
$char["talentIcon"] = "";
$char["treeOne"] = 0;
$char["treeTwo"] = 0;
$char["treeThree"] = 0;

$image= "/images/icons/2d/".$char["race"]."-".$char["gender"].".jpg";

if($char["level"] >= 80)
	$image= "http://arsenal.wow-alive.de/images/portraits/wow-80/".$char["gender"]."-".$char["race"]."-".$char["class"].".gif";
elseif($char["level"] >= 70)
	$image= "http://arsenal.wow-alive.de/images/portraits/wow-70/".$char["gender"]."-".$char["race"]."-".$char["class"].".gif";
elseif($char["level"] >= 60)
	$image= "http://arsenal.wow-alive.de/images/portraits/wow/".$char["gender"]."-".$char["race"]."-".$char["class"].".gif";

	
$char["image"] = $image;


$char["faction"] = getFactionId($char["race"]);
$char["css_faction"] = ($char["faction"] == FACTION_ALLIANCE) ? "alliance" : "horde";

$cacheRows = $DataDB->select('SELECT * FROM character_cache WHERE guid = '.$char["guid"].' AND type = '.CACHETYPE_TT_CHARACTER.';');

$refreshCache = true;
if(count($cacheRows)){

	$cache = $cacheRows[0];	
	
	$cachingPeriod = 60 * 60 * 24;
	
	if((time() - $cache["time"]) < $cachingPeriod)
		$refreshCache = false;
	
	if(empty($cache["icon"]))
		$refreshCache = true;
	
	$char["achievementPoints"] = $cache["achievementPoints"];
	$char["talentIcon"] = $cache["icon"];
	$talents = explode("/", $cache["talents"]);
	$char["treeOne"] = $talents[0];
	$char["treeTwo"] = $talents[1];
	$char["treeThree"] = $talents[2];
}

if($refreshCache){
	$xml = file("http://arsenal.wow-alive.de/character-sheet.php?r=Norgannon&cn=".$char["name"]);
	$xml_string = implode("", $xml);
		
	//echo $xml_string;
	
	// Erfolgspunkte
	$regEx = '@points="(\d+)"@';	
	if(preg_match($regEx, $xml_string, $match)){
		$char["achievementPoints"] = $match[1];
	}
	
	// Talente
	$regEx = '@<talentSpec group="(\d)" icon="([^"]+)" prim="([^"]+)" treeOne="(\d+)" treeTwo="(\d+)" treeThree="(\d+)" active="1"\s*\/>@';
	if(preg_match($regEx, $xml_string, $match)){
		
		$char["talentIcon"] = $match[2];
		$char["treeOne"] = $match[4];
		$char["treeTwo"] = $match[5];
		$char["treeThree"] = $match[6];
			
	}
	
	$talents = $char["treeOne"]."/".$char["treeTwo"]."/".$char["treeThree"];
	if(count($cacheRows)){
		$DataDB->query('UPDATE character_cache 
			SET time = "'.time().'", talents = "'.$talents.'", icon = "'.$char["talentIcon"].'", achievementPoints = "'.$char["achievementPoints"].'" 
			WHERE id = "'.$cache["id"].'";');
		echo "<!-- U -->";
	}
	else{
		$sql = 'INSERT INTO `character_cache` (`guid`, `type`, `time`, `talents`, `icon`, `achievementPoints`) 
		VALUES ("'.$char["guid"].'", "'.CACHETYPE_TT_CHARACTER.'", "'.time().'", "'.$talents.'", "'.$char["talentIcon"].'", "'.$char["achievementPoints"].'");';
		$DataDB->query($sql);
		//echo "I";
		//print_r($DataDB);
	}
	
}
else{
	echo "<!-- C -->";
}
