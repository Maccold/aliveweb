<?php
if(INCLUDED!==true)exit;

$mode = false;

if( !empty($npc_id) ){
	$npc_id = urldecode($npc_id);
}
else{
	die("NPC nicht gefunden");
}


include($_SERVER['DOCUMENT_ROOT']."/core/data/data.npcs.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.npc.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");

if(false){ 
	include("../../core/data/data.npcs.php"); 
	include("../../core/class.npc.php"); 
	include("../../templates/offlike/tooltip/tooltip.npc.php"); 
}

$closed_bosses = array();

	
$npc = new Npc($npc_id);

if($npc)
{
	if(isset($data_npcs[$npc_id])){
		$npc_data = $data_npcs[$npc_id];
	
		// Overrides
		if($npc_data["closed"])
			$npc->closed = $npc_data["closed"];
		if($npc_data["location"])
			$npc->location = $npc_data["location"];
		if($npc_data["name"])
			$npc->name = $npc_data["name"];
		if($npc_data["hp"])
			$npc->hp = $npc_data["hp"];
		if($npc_data["hp_hero"])
			$npc->hp_hero = $npc_data["hp_hero"];
	}
}
else{
	die("NPC nicht gefunden");
}
