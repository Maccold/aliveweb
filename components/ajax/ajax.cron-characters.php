<?php
if(INCLUDED!==true)
	exit;

if(empty($key) || $key != "ntephupentem4nkyquu")
	exit;

/*
$day = date("l");
if($day != "Wednesday"){
	echo "Falscher Tag";
	exit;	
}*/

$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

$accountIds = array();		// Include these
$gmAccountIds = array();	// Exclude those


// Find GM-Accounts
$resultAccounts = $DB->select("SELECT id FROM `account_access` WHERE gmlevel > 0");
foreach($resultAccounts as $row){
	$gmAccountIds[] = $row["id"];
}
unset($resultAccounts);

// Find all relevant accounts
echo "Date:".date("Y-m-d", strtotime("-1 month"));

$resultAccounts = $DB->select("SELECT id FROM `account` WHERE `last_login` > ?;", date("Y-m-d", strtotime("-2 week")) . " 00:00:00");
//$resultAccounts = $DB->select("SELECT id FROM `account` WHERE id = 5576");

foreach($resultAccounts as $row){
	if(!in_array($row["id"], $gmAccountIds))
		$accountIds[] = $row["id"];
}
unset($resultAccounts);

$resultCharacters = $CHDB->select("
	SELECT guid, name, class, level, race, gender 
	FROM characters WHERE level = 80 AND account IN(?a);", $accountIds);

echo "<br>Count:".count($resultCharacters);

foreach($resultCharacters as $row){
	$char_time_start = microtime( 1 ) ;
	
	$charGuid = $row["guid"];
	$values = array(
		"guid" => $charGuid,
		"name" => utf8_decode($row["name"]),
		"class" => $row["class"],
		"level" => $row["level"],
		"race" => $row["race"],
		"gender" => $row["gender"],
		"time" => time(),
		"type" => CACHETYPE_TT_CHARACTER,
	);
	
	$xml = file("http://arsenal.wow-alive.de/character-sheet.php?r=Norgannon&cn=".$row["name"]);
	$xml_string = implode("", $xml);
		
	//echo $xml_string;
	
	// Erfolgspunkte
	$regEx = '@points="(\d+)"@';	
	if(preg_match($regEx, $xml_string, $match)){
		$char["achievementPoints"] = $match[1];
	}
	
	// Talente
	$regEx = '@<talentSpec group="(\d)" icon="([^"]+)" prim="([^"]+)" treeOne="(\d+)" treeTwo="(\d+)" treeThree="(\d+)" active="1"\s*\/>@';
	if(preg_match($regEx, $xml_string, $match)){
		
		$char["talentIcon"] = $match[2];
		$char["treeOne"] = $match[4];
		$char["treeTwo"] = $match[5];
		$char["treeThree"] = $match[6];
			
	}
	
	$talents = $char["treeOne"]."/".$char["treeTwo"]."/".$char["treeThree"];
	
	echo "\n<br>";
	$achPoints = $CHDB->selectCell("
		SELECT SUM(aa.points)
			FROM live_char.character_achievement ca JOIN arsenal.armory_achievement aa ON(aa.id = ca.achievement)
			WHERE ca.guid= ?d", $charGuid);	
	if($itemlevel)
		$values["achievementPoints"] = $achPoints;
	$itemlevel = $CHDB->select("
		SELECT COUNT(it.entry) AS `count`, SUM(it.itemlevel) as `sum`
			FROM live_char.item_instance ii
				JOIN live_char.character_inventory bag ON (bag.item = ii.guid)
				JOIN live_world.item_template it ON(ii.itemEntry = it.entry)
			WHERE ii.owner_guid = ?d AND bag.bag=0 AND bag.slot < 18 AND bag.slot <> 3;", $charGuid);	
	if($itemlevel){
		foreach($itemlevel as $ilRow){
			if($ilRow["sum"] > 0)
				$values["itemLevelEquipped"] = round($ilRow["sum"] / $ilRow["count"],1);
		}
	}
	
	$cache = $DataDB->selectRow("SELECT time, itemlevel FROM character_cache WHERE guid = ?d AND type = ?d;", $charGuid, CACHETYPE_TT_CHARACTER);
	
	if($cache){
		
		$values["itemLevel"] = max($values["itemLevelEquipped"],$cache["itemLevel"]);
		echo "$charGuid ".$values["name"]."- Updated";
		
		$DataDB->query("UPDATE character_cache SET ?a WHERE guid = ?d AND type = ?d;", $values, $charGuid, CACHETYPE_TT_CHARACTER);
	}
	else{
		//debug($DataDB);
		$values["itemLevel"] = $values["itemLevelEquipped"];
		echo "$charGuid ".$values["name"]."- Inserted";
		
		$result = $DataDB->query("INSERT INTO character_cache (?#) VALUES (?a);", array_keys($values), array_values($values));
		//debug($DataDB);
		
	}
	
	
	$char_time_end = microtime( 1 ) ;
	$char_exec_time = $char_time_end - $char_time_start ;
	echo " [$char_exec_time]";
}
	
