<?php
if(INCLUDED!==true)exit;

if(empty($type)){
	die();
}
$search = sanitize($_GET["term"]);

if($type == "quest"){
	
	$results = $WSDB->select('SELECT `entry` AS `value`, `Title` AS `label` FROM quest_template WHERE Title LIKE "%'.$search.'%" LIMIT 0,10;');
	
	echo json_encode($results);
	
}
elseif($type == "instance"){
	include($_SERVER["DOCUMENT_ROOT"]."/core/data/data.zones.php");
	
	$results = array();
	foreach($data_zone as $id => $zone){
		$results[] = array("value" => $id, "label" => str_replace("&#39;","'",$zone["name"]));
	}
	
	echo json_encode($results);
	
}
elseif($type == "bug-comment"){
	$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/data" ) ;
	if ( $DataDB ){
		$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
	}
	
	$search = sanitize($_GET["term"],"int");
	
	$comment = $DataDB->selectRow("SELECT text FROM kommentar WHERE id = ?d", $search);
	
	if($comment){
		echo $comment["text"];
	}
	else{
		echo "-1";
		debug($DataDB);
	}
}
elseif($type == "bugs"){
	$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/data" ) ;
	if ( $DataDB ){
		$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
	}
	
	$search = $_GET["term"];
	$search = str_replace("http://","",$search);
	$search = str_replace("de.wowhead.com/","",$search);
	$search = str_replace("www.wowhead.com/","",$search);
	$search = str_replace("old.wowhead.com/","",$search);
	
	$rows = $DataDB->select('SELECT id, title FROM bug WHERE link LIKE "%'.$search.'%" AND state IN("Offen","Bearbeitung") LIMIT 0,20;');

	$results = array();
	
	foreach($rows as $row){
		$results[$row["id"]] = $row["title"];
	}
	
	echo json_encode($results);
}
elseif($type == "npc"){
	
	$results = $WSDB->select('SELECT ct.`entry` AS `value`, `name_loc3` AS `label` 
		FROM `creature_template` ct JOIN locales_creature lc ON(lc.entry = ct.entry)
		WHERE `name_loc3` LIKE "%'.$search.'%" LIMIT 0,15;');

	echo json_encode($results);
}