<?php
if(INCLUDED!==true)
	exit;

if(empty($key) || $key != "ntephupentem4nkyquu")
	exit;
	
$day = date("l");
if($day != "Wednesday")
	exit;	

$noBody = true;

$cacheRows = $DataDB->select('SELECT * FROM page_cache WHERE guid = '.$zone_id.' AND type = '.CACHETYPE_ZONE_LOOT.';');
$refreshCache = true;
if(count($cacheRows))
{
	$cache = $cacheRows[0];	
	$cachingPeriod = 60 * 60 * 24 * 7; // 1 week
			
	if((time() - $cache["time"]) < $cachingPeriod)
		$refreshCache = false;
}

if(!$refreshCache){
	exit;
}
	

include($_SERVER['DOCUMENT_ROOT']."/core/data/data.zones.php");

if(isset($data_zone[$zone_id])){
	$zone = $data_zone[$zone_id];
	
	$bossIds = array_keys($zone["bosses"]);
	$lootIds = array();
	$bossLoot = array();
	$referenceIds = array();
	
	$bossDifficulties = array();
	
	foreach($bossIds as $bossId){
	
		$rows = $WDB->select('SELECT difficulty_entry_1, difficulty_entry_2, difficulty_entry_3, lootid FROM creature_template WHERE entry = '.$bossId.';');
		
		if(count($rows)){
			$n1 = $row["difficulty_entry_1"];
			$n2 = $row["difficulty_entry_2"];
			$n3 = $row["difficulty_entry_3"];
			
			if( $row["difficulty_entry_2"] > 0 ){ 
				$bossDifficulties = array(
					"N10" => $bossId, 
					"N25" => $n1, 
					"H10" => $n2, 
					"H25" => $n3
				);
			}
			elseif( $row["difficulty_entry_1"] > 0 ){ 
				$bossDifficulties = array(
					"N10" => $bossId, 
					"N25" => $n1, 
				);
			}
			else { 
				$bossDifficulties = array(
					"N10" => $bossId, 
				);
			}
					
		}
		
		foreach($rows as $row)
			$lootIds[] = $row["lootid"];
		
		$rows = $WDB->select('SELECT item, mincountOrRef FROM creature_loot_template WHERE entry IN('.implode(", ", $lootIds).');');
		
		foreach($rows as $row){
			if($row["mincountOrRef"] < 0)
				$referenceIds[] = abs($row["mincountOrRef"]);
			else
				$bossLoot[$bossId] = array("id" => $row["item"]);
		}
		
		if(count($referenceIds) > 0){
			$rows = $WDB->select('SELECT item FROM reference_loot_template WHERE entry IN('.implode(", ", $referenceIds).');');
			foreach($rows as $row)
				$bossLoot[$bossId] = array("id" => $row["item"]);
		}
	
		// Get Item Templates
		$WDB->select('SELECT entry, class, subclass, name, displayid, Quality, Flags, FlagsExtra, InventoryType, AllowableClass, ItemLevel, RequiredLevel FROM item_template WHERE ;');
	}
	
}
?>
<tr class="row1 class-7 not-common is-equipment">
	<td data-raw="-3 Stiefel der bebenden Erde"><a href="/wow/de/item/47090" class="item-link color-q4"> <span  class="icon-frame frame-18 " style='background-image: url("http://eu.media.blizzard.com/wow/icons/18/inv_boots_chain_06.jpg");'> </span> <strong>Stiefel der bebenden Erde</strong> <span class="icon-faction-0"></span> </a></td>
	<td class="align-center" data-raw="245"> 245 </td>
	<td class="align-center" data-raw="80"> 80 </td>
	<td> Füße <em>(Kette)</em></td>
	<td data-raw="3"> Mittel </td>
	<td data-raw="1"> Normal 25 </td>
	<td><a href="/wow/de/zone/trial-of-the-crusader/faction-champions" data-npc="34461">Fraktionschampions</a></td>
</tr>
			