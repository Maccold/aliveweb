<?php
/*
include('core/ajax_lib/Php.php');
*/
$_GET['nobody'] = 1;

$com_content['ajax'] = array(
    'index' => array(
        '', // g_ option require for view     [0]
        'ajax', // loc name (key)                [1]
        url_for("ajax"), // Link to                 [2]
        '', // main menu name/id ('' - not show)        [3]
        0 // show in context menu (1-yes,0-no)          [4]
    ),
    'character' => array('', '', url_for("ajax", "character"), '', 0),
    'get-account' => array('', '', url_for("ajax", "get-account"), '', 0),
    'checkemail' => array('', '', url_for("ajax", "checkemail"), '', 0),
    'checklogin' => array('', '', url_for("ajax", "checklogin"), '', 0),
    'getquote' => array('', '', url_for("ajax", "getquote"), '', 0),
    'preview' => array('', '', url_for("ajax", "preview"), '', 0),
    'userlist' => array('', '', url_for("ajax", "userlist"), '', 0),
    'cron-arenateams' => array('', '', url_for("ajax", "cron-arenateams"), '', 0),
    'cron-characters' => array('', '', url_for("ajax", "cron-characters"), '', 0),
    'search' => array('', '', url_for("ajax", "search"), '', 0),
    'search-quest' => array('', '', url_for("ajax", "search-quest"), '', 0),
);

