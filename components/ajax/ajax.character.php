<?php
if(INCLUDED!==true)exit;

include($_SERVER['DOCUMENT_ROOT']."/core/class.character.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.arenateam.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.prototype.php");
include($_SERVER['DOCUMENT_ROOT']."/core/class.item.php");
include($_SERVER['DOCUMENT_ROOT']."/core/defines.php");
include($_SERVER['DOCUMENT_ROOT']."/lang/strings.de.php");
include($_SERVER['DOCUMENT_ROOT']."/core/data/data.races.php");


$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/data" ) ;
if ( $DataDB ){
	$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
}
$aDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/arsenal" ) ;
if ( $aDB ){
	$aDB->setErrorHandler( 'databaseErrorHandler' ) ;
}

// Config Variables
$debug = true;
$noBody = true;

if($search_realm != "norgannon")
	$search_realm = "norgannon";

if(empty($search_for)){
	$req_tpl = false;	// 404
	return;
}

// Find character
$char = new Character($search_for, $char_mode, $debug);
if(!$char){
	$req_tpl = false;	// 404
	return;
}

// Base Variables
$talent_spec = array();
$current_tree = array();
$talent_spec = NULL;
		
// Cache prüfen
$char->GetCache(TRUE, TRUE);

$data = $char->GetFullData();

echo json_encode($data);
