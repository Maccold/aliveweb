<?php
$lang_strings = array();
/*
XML: <str id="([^"]+)">([^<]+)</str>
=>
PHP: $lang_strings["$1"] = "$2";
*/

$lang_strings["armory.faction.1"] = "Horde";
$lang_strings["armory.faction.0"] = "Allianz";


$lang_strings["armory.achievements.category.1"] = "Statistiken";
$lang_strings["armory.achievements.category.21"] = "Spieler gegen Spieler";
$lang_strings["armory.achievements.category.81"] = "Heldentaten";
$lang_strings["armory.achievements.category.92"] = "Allgemein";
$lang_strings["armory.achievements.category.95"] = "Spieler gegen Spieler";
$lang_strings["armory.achievements.category.96"] = "Quests";
$lang_strings["armory.achievements.category.97"] = "Erkundung";
$lang_strings["armory.achievements.category.122"] = "Tode";
$lang_strings["armory.achievements.category.123"] = "Arenen";
$lang_strings["armory.achievements.category.124"] = "Schlachtfelder";
$lang_strings["armory.achievements.category.125"] = "Dungeons";
$lang_strings["armory.achievements.category.126"] = "Welt";
$lang_strings["armory.achievements.category.127"] = "Wiederbelebung";
$lang_strings["armory.achievements.category.128"] = "Siege";
$lang_strings["armory.achievements.category.130"] = "Charakter";
$lang_strings["armory.achievements.category.131"] = "Geselligkeit";
$lang_strings["armory.achievements.category.132"] = "Fertigkeiten";
$lang_strings["armory.achievements.category.133"] = "Quests";
$lang_strings["armory.achievements.category.134"] = "Reise";
$lang_strings["armory.achievements.category.135"] = "Kreaturen";
$lang_strings["armory.achievements.category.136"] = "Ehrenhafte Siege";
$lang_strings["armory.achievements.category.137"] = "Todesstöße";
$lang_strings["armory.achievements.category.140"] = "Vermögen";
$lang_strings["armory.achievements.category.141"] = "Kampf";
$lang_strings["armory.achievements.category.145"] = "Verbrauchsgüter";
$lang_strings["armory.achievements.category.147"] = "Ruf";
$lang_strings["armory.achievements.category.152"] = "Gewertete Arenen";
$lang_strings["armory.achievements.category.153"] = "Schlachtfelder";
$lang_strings["armory.achievements.category.154"] = "Welt";
$lang_strings["armory.achievements.category.155"] = "Weltereignisse";
$lang_strings["armory.achievements.category.156"] = "Winterhauch";
$lang_strings["armory.achievements.category.158"] = "Schlotternächte";
$lang_strings["armory.achievements.category.159"] = "Nobelgarten";
$lang_strings["armory.achievements.category.160"] = "Mondfest";
$lang_strings["armory.achievements.category.161"] = "Sonnenwende";
$lang_strings["armory.achievements.category.162"] = "Braufest";
$lang_strings["armory.achievements.category.163"] = "Kinderwoche";
$lang_strings["armory.achievements.category.165"] = "Arena";
$lang_strings["armory.achievements.category.168"] = "Dungeon & Schlachtzug";
$lang_strings["armory.achievements.category.169"] = "Berufe";
$lang_strings["armory.achievements.category.170"] = "Kochkunst";
$lang_strings["armory.achievements.category.171"] = "Angeln";
$lang_strings["armory.achievements.category.172"] = "Erste Hilfe";
$lang_strings["armory.achievements.category.173"] = "Berufe";
$lang_strings["armory.achievements.category.178"] = "Sekundäre Fertigkeiten";
$lang_strings["armory.achievements.category.187"] = "Liebe liegt in der Luft";
$lang_strings["armory.achievements.category.191"] = "Ausrüstung";
$lang_strings["armory.achievements.category.201"] = "Ruf";
$lang_strings["armory.achievements.category.14777"] = "Östliche Königreiche";
$lang_strings["armory.achievements.category.14778"] = "Kalimdor";
$lang_strings["armory.achievements.category.14779"] = "Scherbenwelt";
$lang_strings["armory.achievements.category.14780"] = "Nordend";
$lang_strings["armory.achievements.category.14801"] = "Alteractal";
$lang_strings["armory.achievements.category.14802"] = "Arathibecken";
$lang_strings["armory.achievements.category.14803"] = "Auge des Sturms";
$lang_strings["armory.achievements.category.14804"] = "Kriegshymnenschlucht";
$lang_strings["armory.achievements.category.14805"] = "The Burning Crusade";
$lang_strings["armory.achievements.category.14806"] = "Wrath of the Lich King - Dungeons";
$lang_strings["armory.achievements.category.14807"] = "Dungeon & Schlachtzug";
$lang_strings["armory.achievements.category.14808"] = "Classic";
$lang_strings["armory.achievements.category.14821"] = "Classic";
$lang_strings["armory.achievements.category.14822"] = "The Burning Crusade";
$lang_strings["armory.achievements.category.14823"] = "Wrath of the Lich King";
$lang_strings["armory.achievements.category.14861"] = "Classic";
$lang_strings["armory.achievements.category.14862"] = "The Burning Crusade";
$lang_strings["armory.achievements.category.14863"] = "Wrath of the Lich King";
$lang_strings["armory.achievements.category.14864"] = "Classic";
$lang_strings["armory.achievements.category.14865"] = "The Burning Crusade";
$lang_strings["armory.achievements.category.14866"] = "Wrath of the Lich King";
$lang_strings["armory.achievements.category.14881"] = "Strand der Uralten";
$lang_strings["armory.achievements.category.14901"] = "Tausendwinter";
$lang_strings["armory.achievements.category.14921"] = "Wrath of the Lich King - Heroische Dungeons";
$lang_strings["armory.achievements.category.14922"] = "Wrath of the Lich King - Schlachtzüge für 10 Spieler";
$lang_strings["armory.achievements.category.14923"] = "Wrath of the Lich King - Schlachtzüge für 25 Spieler";
$lang_strings["armory.achievements.category.14941"] = "Argentumturnier";
$lang_strings["armory.achievements.category.14961"] = "Geheimnisse von Ulduar - Schlachtzug für 10 Spieler";
$lang_strings["armory.achievements.category.14962"] = "Geheimnisse von Ulduar - Schlachtzug für 25 Spieler";
$lang_strings["armory.achievements.category.14963"] = "Geheimnisse v. Ulduar";
$lang_strings["armory.achievements.category.14981"] = "Die Pilgerfreuden";
$lang_strings["armory.achievements.category.15001"] = "Der Ruf des Kreuzzugs - Schlachtzug für 10 Spieler";
$lang_strings["armory.achievements.category.15002"] = "Der Ruf des Kreuzzugs - Schlachtzug für 25 Spieler";
$lang_strings["armory.achievements.category.15003"] = "Insel der Eroberung";
$lang_strings["armory.achievements.category.15021"] = "Der Ruf des Kreuzzugs";
$lang_strings["armory.achievements.category.15041"] = "Der Untergang des Lichkönigs - Schlachtzug für 10 Spieler";
$lang_strings["armory.achievements.category.15042"] = "Der Untergang des Lichkönigs - Schlachtzug für 25 Spieler";
$lang_strings["armory.achievements.category.15062"] = "Der Untergang des Lichkönigs";


$lang_strings["armory.creature.type.0"] = "";
$lang_strings["armory.creature.type.1"] = "Wildtier";
$lang_strings["armory.creature.type.2"] = "Drachkin";
$lang_strings["armory.creature.type.3"] = "D&auml;mon";
$lang_strings["armory.creature.type.4"] = "Elementar";
$lang_strings["armory.creature.type.5"] = "Riese";
$lang_strings["armory.creature.type.6"] = "Untoter";
$lang_strings["armory.creature.type.7"] = "Humanoid";
$lang_strings["armory.creature.type.8"] = "Critter";
$lang_strings["armory.creature.type.9"] = "Mechanisch";
$lang_strings["armory.creature.type.10"] = "Unklar";
$lang_strings["armory.creature.type.11"] = "Totem";
$lang_strings["armory.creature.type.12"] = "Haustier";
$lang_strings["armory.creature.type.13"] = "Gaswolke";

$lang_strings["armory.creature.rank.1"] = "Elite";
$lang_strings["armory.creature.rank.2"] = "Rar/Elite";
$lang_strings["armory.creature.rank.3"] = "Boss";
$lang_strings["armory.creature.rank.4"] = "Rar";

$lang_strings["armory.dungeons.dungeon"] = "Dungeon";
$lang_strings["armory.dungeons.raid"] = "Schlachtzug";

$lang_strings["armory.itemslot.slot.head"] = "Kopf";
$lang_strings["armory.itemslot.slot.neck"] = "Hals";
$lang_strings["armory.itemslot.slot.shoulders"] = "Schultern";
$lang_strings["armory.itemslot.slot.back"] = "R&uuml;cken";
$lang_strings["armory.itemslot.slot.chest"] = "Brust";
$lang_strings["armory.itemslot.slot.shirt"] = "Hemd";
$lang_strings["armory.itemslot.slot.tabard"] = "Wappenrock";
$lang_strings["armory.itemslot.slot.wrist"] = "Handgelenke";
$lang_strings["armory.itemslot.slot.hands"] = "H&auml;nde";
$lang_strings["armory.itemslot.slot.hand"] = "Hand";
$lang_strings["armory.itemslot.slot.waist"] = "Taille";
$lang_strings["armory.itemslot.slot.legs"] = "Beine";
$lang_strings["armory.itemslot.slot.feet"] = "F&uuml;ße";
$lang_strings["armory.itemslot.slot.finger"] = "Finger";
$lang_strings["armory.itemslot.slot.trinket"] = "Schmuck";
$lang_strings["armory.itemslot.slot.mainHand"] = "Waffenhand";
$lang_strings["armory.itemslot.slot.offHand"] = "Nebenhand";
$lang_strings["armory.itemslot.slot.ranged"] = "Distanz";
$lang_strings["armory.itemslot.slot.relic"] = "Relikt";
$lang_strings["armory.itemslot.slot.ammo"] = "Munition";

$lang_strings["currentlyEquipped"] = "Derzeit angelegt";
$lang_strings["armory.item-info.drop-rate.0"] = "Nie/Unklar";
$lang_strings["armory.item-info.drop-rate.1"] = "Extrem niedrig (1% - 2%)";
$lang_strings["armory.item-info.drop-rate.2"] = "Sehr niedrig (3% - 14%)";
$lang_strings["armory.item-info.drop-rate.3"] = "Niedrig (15% - 24%)";
$lang_strings["armory.item-info.drop-rate.4"] = "Mittel (25% - 49%)";
$lang_strings["armory.item-info.drop-rate.5"] = "Hoch (50-99%)";
$lang_strings["armory.item-info.drop-rate.6"] = "Garantiert (100%)";

$lang_strings["armory.item-info.label.name"] = "Name";
$lang_strings["armory.item-info.label.location"] = "Ort";
$lang_strings["armory.item-info.label.drop"] = "Chance";
$lang_strings["armory.item-info.label.level"] = "Stufe";
$lang_strings["armory.item-info.label.reagents"] = "Zutaten";
$lang_strings["armory.item-info.label.cost"] = "Preis";
$lang_strings["armory.item-info.label.sellPrice"] = "Verkaufspreis";
$lang_strings["armory.item-info.label.count"] = "Anzahl";
$lang_strings["armory.item-info.label.title"] = "Titel";
$lang_strings["armory.item-info.label.reqlevel"] = "Ben. Stufe";
$lang_strings["armory.item-info.label.type"] = "Typ";
$lang_strings["armory.item-info.label.enchantment"] = "Verzauberung";
$lang_strings["armory.item-info.label.effects"] = "Effekte";
$lang_strings["armory.item-info.label.items_detailed"] = "Modellanzeige";
$lang_strings["armory.item-info.label.items_list"] = "Ausr&uuml;stungsliste";

$lang_strings["armory.item-info.title.dropped"] = "Erbeutet von";
$lang_strings["armory.item-info.title.pickpocketed"] = "Gestohlen von";
$lang_strings["armory.item-info.title.skinned"] = "Gek&uuml;rschnert von";
$lang_strings["armory.item-info.title.gathered"] = "Kraut gefunden in";
$lang_strings["armory.item-info.title.mined"] = "Abbaubar in";
$lang_strings["armory.item-info.title.starts"] = "Startet";
$lang_strings["armory.item-info.title.provided"] = "Bereitgestellt f&uuml;r";
$lang_strings["armory.item-info.title.objective"] = "Ziel von";
$lang_strings["armory.item-info.title.reward"] = "Belohnung von";

$lang_strings["armory.item-info.randomenchants"] = "Zuf&auml;llige Verzauberung:";
$lang_strings["armory.item-info.itemlevel"] = "Gegenstandsstufe";
$lang_strings["armory.item-info.dropsfrom"] = "Gefunden bei:";
$lang_strings["armory.item-info.contained"] = "Enthalten in:";
$lang_strings["armory.item-info.bought"] = "Preis:";
$lang_strings["armory.item-info.sells"] = "Verkaufspreis:";
$lang_strings["armory.item-info.disenchantable"] = "Entzauberbar:";
$lang_strings["armory.item-info.heroic"] = "Heroisch";
$lang_strings["armory.item-info.soldby"] = "Verkauft von";
$lang_strings["armory.item-info.reqregeants"] = "Ben&ouml;tigt Zutaten";
$lang_strings["armory.item-info.plans"] = "Pl&auml;ne f&uuml;r";
$lang_strings["armory.item-info.regeantsfor"] = "Zutat f&uuml;r";
$lang_strings["armory.item-info.currency"] = "W&auml;hrung f&uuml;r";
$lang_strings["armory.item-info.disinto"] = "Wird entzaubert zu";
$lang_strings["armory.item-info.transinto"] = "Äquivalent der Gegenfraktion:";
$lang_strings["armory.item-info.transinto.alliance"] = "Allianz-Äquivalent:";
$lang_strings["armory.item-info.transinto.horde"] = "Horde-Äquivalent:";
$lang_strings["armory.item-info.randomwdrop"] = "Wird zuf&auml;llig von &lt;strong&gt;Humanoiden&lt;/strong&gt; und &lt;strong&gt;Monstern&lt;/strong&gt; der Stufen 49 bis 66 fallengelassen.";

$lang_strings["armory.item-info.disenchant.tooltip.before"] = "Ben&ouml;tigt Verzauberkunst (&lt;strong&gt;";
$lang_strings["armory.item-info.disenchant.tooltip.after"] = "&lt;/strong&gt;) zum Entzaubern";
$lang_strings["item.fact"] = "Gegenstandsangaben";

$lang_strings["armory.item-tooltip.conjured"] = "Herbeigezaubert";
$lang_strings["armory.item-tooltip.binds-pickup"] = "Wird beim Aufheben gebunden";
$lang_strings["armory.item-tooltip.binds-equipped"] = "Wird beim Anlegen gebunden";
$lang_strings["armory.item-tooltip.binds-used"] = "Wird bei Benutzung gebunden";
$lang_strings["armory.item-tooltip.quest-item"] = "Questgegenstand";
$lang_strings["armory.item-tooltip.unique"] = "Einzigartig";
$lang_strings["armory.item-tooltip.unique-equipped"] = "Einzigartig anlegbar";
$lang_strings["armory.item-tooltip.begins-quest"] = "Dieser Gegenstand startet eine Quest.";
$lang_strings["armory.item-tooltip.slot"] = "Platz";
$lang_strings["armory.item-tooltip.projectile"] = "Projektil";

$lang_strings["armory.item-tooltip.non-equip"] = "INDEX_NON_EQUIP_TYPE";
$lang_strings["armory.item-tooltip.one-hand"] = "Einh&auml;ndig";
$lang_strings["armory.item-tooltip.two-hand"] = "Zweih&auml;ndig";
$lang_strings["armory.item-tooltip.held-off-hand"] = "In Schildhand gef&uuml;hrt";
$lang_strings["armory.item-tooltip.thrown"] = "Geworfen";
$lang_strings["armory.item-tooltip.bag-type"] = "INDEX_BAG_TYPE";
$lang_strings["armory.item-tooltip.quiver-type"] = "INDEX_QUIVER_TYPE";

$lang_strings["armory.item-tooltip.adds"] = "Verursacht";
$lang_strings["armory.item-tooltip.dps"] = "Schaden pro Sekunde";
$lang_strings["armory.item-tooltip.speed"] = "Tempo";
$lang_strings["armory.item-tooltip.damage"] = "Schaden";
$lang_strings["armory.item-tooltip.holy-damage"] = "Heiligschaden";
$lang_strings["armory.item-tooltip.fire-damage"] = "Feuerschaden";
$lang_strings["armory.item-tooltip.nature-damage"] = "Naturschaden";
$lang_strings["armory.item-tooltip.frost-damage"] = "Frostschaden";
$lang_strings["armory.item-tooltip.shadow-damage"] = "Schattenschaden";
$lang_strings["armory.item-tooltip.arcane-damage"] = "Arkanschaden";

$lang_strings["armory.item-tooltip.armor"] = "R&uuml;stung";
$lang_strings["armory.item-tooltip.block"] = "Blocken";

$lang_strings["armory.item-tooltip.strength"] = "St&auml;rke";
$lang_strings["armory.item-tooltip.agility"] = "Beweglichkeit";
$lang_strings["armory.item-tooltip.stamina"] = "Ausdauer";
$lang_strings["armory.item-tooltip.intellect"] = "Intelligenz";
$lang_strings["armory.item-tooltip.spirit"] = "Willenskraft";
$lang_strings["armory.item-tooltip.bonusStrength"] = "St&auml;rke";
$lang_strings["armory.item-tooltip.bonusAgility"] = "Beweglichkeit";
$lang_strings["armory.item-tooltip.bonusStamina"] = "Ausdauer";
$lang_strings["armory.item-tooltip.bonusIntellect"] = "Intelligenz";
$lang_strings["armory.item-tooltip.bonusSpirit"] = "Willenskraft";

$lang_strings["armory.item-tooltip.fire-resistance"] = "Feuerwiderstand";
$lang_strings["armory.item-tooltip.nature-resistance"] = "Naturwiderstand";
$lang_strings["armory.item-tooltip.frost-resistance"] = "Frostwiderstand";
$lang_strings["armory.item-tooltip.shadow-resistance"] = "Schattenwiderstand";
$lang_strings["armory.item-tooltip.arcane-resistance"] = "Arkanwiderstand";
$lang_strings["armory.item-tooltip.socket-bonus"] = "Sockelbonus";

$lang_strings["armory.item-tooltip.meta-socket"] = "Metasockel";
$lang_strings["armory.item-tooltip.red-socket"] = "Roter Sockel";
$lang_strings["armory.item-tooltip.yellow-socket"] = "Gelber Sockel";
$lang_strings["armory.item-tooltip.blue-socket"] = "Blauer Sockel";

$lang_strings["armory.item-tooltip.random-enchant"] = "Zuf&auml;llige Verzauberung";
$lang_strings["armory.item-tooltip.durability"] = "Haltbarkeit";

$lang_strings["armory.item-tooltip.races"] = "V&ouml;lker";
$lang_strings["armory.item-tooltip.classes"] = "Klassen";
$lang_strings["armory.item-tooltip.requires-level"] = "Erfordert Stufe";
$lang_strings["armory.item-tooltip.requires"] = "Erfordert";

$lang_strings["armory.item-tooltip.requires.personalarena"] = "Ben&ouml;tigt pers&ouml;nliche Arenawertung von";

/* reputation status 0= male 1= female no number= neutral */
$lang_strings["armory.item-tooltip.hated"] = "Hasserf&uuml;llt";
$lang_strings["armory.item-tooltip.hostile"] = "Feindselig";
$lang_strings["armory.item-tooltip.unfriendly"] = "Unfreundlich";
$lang_strings["armory.item-tooltip.neutral"] = "Neutral";
$lang_strings["armory.item-tooltip.friendly"] = "Freundlich";
$lang_strings["armory.item-tooltip.honored"] = "Wohlwollend";
$lang_strings["armory.item-tooltip.revered"] = "Respektvoll";
$lang_strings["armory.item-tooltip.exalted"] = "Ehrf&uuml;rchtig";

$lang_strings["armory.item-tooltip.hated.0"] = "Hasserf&uuml;llt";
$lang_strings["armory.item-tooltip.hostile.0"] = "Feindselig";
$lang_strings["armory.item-tooltip.unfriendly.0"] = "Unfreundlich";
$lang_strings["armory.item-tooltip.neutral.0"] = "Neutral";
$lang_strings["armory.item-tooltip.friendly.0"] = "Freundlich";
$lang_strings["armory.item-tooltip.honored.0"] = "Wohlwollend";
$lang_strings["armory.item-tooltip.revered.0"] = "Respektvoll";
$lang_strings["armory.item-tooltip.exalted.0"] = "Ehrf&uuml;rchtig";

$lang_strings["armory.item-tooltip.hated.1"] = "Hasserf&uuml;llt";
$lang_strings["armory.item-tooltip.hostile.1"] = "Feindselig";
$lang_strings["armory.item-tooltip.unfriendly.1"] = "Unfreundlich";
$lang_strings["armory.item-tooltip.neutral.1"] = "Neutral";
$lang_strings["armory.item-tooltip.friendly.1"] = "Freundlich";
$lang_strings["armory.item-tooltip.honored.1"] = "Wohlwollend";
$lang_strings["armory.item-tooltip.revered.1"] = "Respektvoll";
$lang_strings["armory.item-tooltip.exalted.1"] = "Ehrf&uuml;rchtig";

$lang_strings["armory.item-tooltip.bonusDefenseSkillRating"] = "Anlegen: Erh&ouml;ht die Verteidigungswertung um";
$lang_strings["armory.item-tooltip.bonusDodgeRating"] = "Anlegen: Erh&ouml;ht Eure Ausweichwertung um";
$lang_strings["armory.item-tooltip.bonusParryRating"] = "Anlegen: Erh&ouml;ht Eure Parierwertung um";
$lang_strings["armory.item-tooltip.bonusBlockRating"] = "Anlegen: Erh&ouml;ht Eure Blockwertung um";
$lang_strings["armory.item-tooltip.bonusHitMeleeRating"] = "bonusHitMeleeRating";
$lang_strings["armory.item-tooltip.bonusHitRangedRating"] = "bonusHitRangedRating";
$lang_strings["armory.item-tooltip.bonusHitSpellRating"] = "Anlegen: Erh&ouml;ht Zaubertrefferwertung um";
$lang_strings["armory.item-tooltip.bonusCritMeleeRating"] = "bonusCritMeleeRating";
$lang_strings["armory.item-tooltip.bonusCritRangedRating"] = "bonusCritRangedRating";
$lang_strings["armory.item-tooltip.bonusCritSpellRating"] = "Anlegen: Erh&ouml;ht kritische Zaubertrefferwertung um";
$lang_strings["armory.item-tooltip.bonusHitTakenMeleeRating"] = "bonusHitTakenMeleeRating";
$lang_strings["armory.item-tooltip.bonusHitTakenRangedRating"] = "bonusHitTakenRangedRating";
$lang_strings["armory.item-tooltip.bonusHitTakenSpellRating"] = "bonusHitTakenSpellRating";
$lang_strings["armory.item-tooltip.bonusCritTakenMeleeRating"] = "bonusCritTakenMeleeRating";
$lang_strings["armory.item-tooltip.bonusCritTakenRangedRating"] = "bonusCritTakenRangedRating";
$lang_strings["armory.item-tooltip.bonusCritTakenSpellRating"] = "bonusCritTakenSpellRating";
$lang_strings["armory.item-tooltip.bonusHasteMeleeRating"] = "bonusHasteMeleeRating";
$lang_strings["armory.item-tooltip.bonusHasteRangedRating"] = "bonusHasteRangedRating";
$lang_strings["armory.item-tooltip.bonusHasteSpellRating"] = "Anlegen: Erh&ouml;ht Zaubertempowertung um";
$lang_strings["armory.item-tooltip.bonusHitRating"] = "Anlegen: Erh&ouml;ht Trefferwertung um";
$lang_strings["armory.item-tooltip.bonusCritRating"] = "Anlegen: Erh&ouml;ht kritische Trefferwertung um";
$lang_strings["armory.item-tooltip.bonusHitTakenRating"] = "Anlegen: Erh&ouml;ht Vermeidungswertung um";
$lang_strings["armory.item-tooltip.bonusCritTakenRating"] = "bonusCritTakenRating";
$lang_strings["armory.item-tooltip.bonusResilienceRating"] = "Anlegen: Erh&ouml;ht Eure Abh&auml;rtungswertung um";
$lang_strings["armory.item-tooltip.bonusHasteRating"] = "Anlegen: Erh&ouml;ht Tempowertung um";
$lang_strings["armory.item-tooltip.bonusSpellPower"] = "Anlegen: Erh&ouml;ht Zaubermacht um";

$lang_strings["armory.item-tooltip.bonusAttackPower"] = "Anlegen: Erh&ouml;ht Eure Angriffskraft um";
$lang_strings["armory.item-tooltip.bonusExpertiseRating"] = "Anlegen: Erh&ouml;ht Eure Waffenkundewertung um";
$lang_strings["armory.item-tooltip.bonusRangedAttackPower"] = "Anlegen: Erh&ouml;ht Eure Distanzangriffskraft um";
$lang_strings["armory.item-tooltip.bonusFeralAttackPower"] = "Anlegen: Erh&ouml;ht Eure Angriffskraft in Tiergestalt um";
$lang_strings["armory.item-tooltip.bonusManaRegen"] = "Anlegen: Stellt alle 5 Sek.";
$lang_strings["armory.item-tooltip.bonusManaRegenPer5"] = "Mana wieder her";
$lang_strings["armory.item-tooltip.bonusArmorPenetration"] = "Anlegen: Erh&ouml;ht Eure R&uuml;stungsdurchschlagwertung um";
$lang_strings["armory.item-tooltip.bonusBlockValue"] = "Anlegen: Erh&ouml;ht den Blockwert Eures Schildes um";
$lang_strings["armory.item-tooltip.bonusHealthRegen"] = "Anlegen: Erh&ouml;ht Eure Gesundheitsregeneration um";
$lang_strings["armory.item-tooltip.bonusSpellPenetration"] = "Anlegen: Erh&ouml;ht Eure Zauberdurchschlagskraft um";

$lang_strings["armory.item-tooltip.requiredLevelMin"] = "Ben&ouml;tigt Stufe";
$lang_strings["armory.item-tooltip.requiredLevelMax"] = "bis";

$lang_strings["armory.item-tooltip.charges"] = "Aufladungen";
$lang_strings["armory.item-tooltip.use"] = "Benutzen";
$lang_strings["armory.item-tooltip.equip"] = "Anlegen";
$lang_strings["armory.item-tooltip.chance-on-hit"] = "Trefferchance";
$lang_strings["armory.item-tooltip.set"] = "Set";

$lang_strings["armory.item-tooltip.majorglyph"] = "Erhebliche Glyphe	";
$lang_strings["armory.item-tooltip.minorglyph"] = "Geringe Glyphe";

$lang_strings["armory.item-type.Two-Handed Axes"] = "Zweihandaxt";
$lang_strings["armory.item-type.One-Handed Axes"] = "Einhandaxt";
$lang_strings["armory.item-type.Bows"] = "Bogen";
$lang_strings["armory.item-type.Bucklers"] = "Rundschild";
$lang_strings["armory.item-type.Crossbows"] = "Armbrust";
$lang_strings["armory.item-type.Daggers"] = "Dolch";
$lang_strings["armory.item-type.One-Handed Exotics"] = "Einhandexotika";
$lang_strings["armory.item-type.Two-Handed Exotics"] = "Zweihandexotika";
$lang_strings["armory.item-type.Fist Weapons"] = "Faustwaffe";
$lang_strings["armory.item-type.Guns"] = "Schusswaffe";
$lang_strings["armory.item-type.Idols"] = "G&ouml;tze";
$lang_strings["armory.item-type.Librams"] = "Buchband";
$lang_strings["armory.item-type.One-Handed Maces"] = "Einhandstreitkolben";
$lang_strings["armory.item-type.Two-Handed Maces"] = "Zweihandstreitkolben";
$lang_strings["armory.item-type.Polearms"] = "Stangenwaffe";
$lang_strings["armory.item-type.Shields"] = "Schild";
$lang_strings["armory.item-type.Spears"] = "Speer";
$lang_strings["armory.item-type.Staves"] = "Stab";
$lang_strings["armory.item-type.Two-Handed Swords"] = "Zweihandschwert";
$lang_strings["armory.item-type.One-Handed Swords"] = "Einhandschwert";
$lang_strings["armory.item-type.Totems"] = "Totem";
$lang_strings["armory.item-type.Sigils"] = "Siegel";
$lang_strings["armory.item-type.Wands"] = "Zauberstab";

$lang_strings["armory.searchColumn.source"] = "Quelle";

$lang_strings["armory.searchColumn.sourceType.questReward"] = "Questbelohnung";
$lang_strings["armory.searchColumn.sourceType.none"] = " - ";
$lang_strings["armory.searchColumn.sourceType.creatureDrop"] = "Beute";
$lang_strings["armory.searchColumn.sourceType.gameObjectDrop"] = "Beute aus Kiste";
$lang_strings["armory.searchColumn.sourceType.factionReward"] = "Rufbelohnung";
$lang_strings["armory.searchColumn.sourceType.pvpReward"] = "PvP-Belohnung";
$lang_strings["armory.searchColumn.sourceType.vendorPvP"] = "PvP-Belohnung";
$lang_strings["armory.searchColumn.sourceType.vendor"] = "H&auml;ndler";
$lang_strings["armory.searchColumn.sourceType.pickPocket"] = "Taschendiebstahl";
$lang_strings["armory.searchColumn.sourceType.creatureSkin"] = "K&uuml;rschnern";
$lang_strings["armory.searchColumn.sourceType.creatureMine"] = "Bergbau";
$lang_strings["armory.searchColumn.sourceType.creatureHerb"] = "Kr&auml;uterkunde";
$lang_strings["armory.searchColumn.sourceType.startsQuest"] = "Questgegenstand";
$lang_strings["armory.searchColumn.sourceType.providedQuest"] = "Questgegenstand";
$lang_strings["armory.searchColumn.sourceType.objQuest"] = "Questgegenstand";

$lang_strings["armory.searchColumn.sourceType.createdBySpell"] = "Hergestellt";
$lang_strings["armory.searchColumn.sourceType.reagentSpell"] = "Reagenz";
$lang_strings["armory.searchColumn.sourceType.createdByPlans"] = "Hergestellt";
$lang_strings["armory.searchColumn.sourceType.currency"] = "W&auml;hrung";

$lang_strings["armory.searchColumn.sourceType.planForItem"] = "Pl&auml;ne";
$lang_strings["armory.searchColumn.sourceType.worldDrop"] = "Drop";