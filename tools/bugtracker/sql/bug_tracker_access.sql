SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bug_tracker_access`
-- ----------------------------
DROP TABLE IF EXISTS `bug_tracker_access`;
CREATE TABLE `bug_tracker_access` (
  `acc_id` int(30) NOT NULL,
  `security_level` int(30) NOT NULL,
  `name` text NOT NULL,
  `command` text,
  PRIMARY KEY (`acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;