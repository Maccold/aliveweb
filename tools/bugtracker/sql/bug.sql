﻿SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bug`
-- ----------------------------
DROP TABLE IF EXISTS `bug`;
CREATE TABLE `bug` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `class` text NOT NULL,
  `title` text NOT NULL,
  `desc` text NOT NULL,
  `state` text NOT NULL,
  `complete` text NOT NULL,
  `by` text NOT NULL,
  `date` text NOT NULL,
  `date2` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;