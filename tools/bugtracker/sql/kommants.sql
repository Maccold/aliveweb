/*
Navicat MySQL Data Transfer

Source Server         : Home Mysql
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : soe_test_db

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-05-09 23:25:56
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `kommants`
-- ----------------------------
DROP TABLE IF EXISTS `kommants`;
CREATE TABLE `kommants` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `postid` int(32) NOT NULL,
  `text` text NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kommants
-- ----------------------------
