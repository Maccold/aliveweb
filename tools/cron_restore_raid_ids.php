<?php

$key = (isset($_GET['key'])) ? $_GET['key'] : "";
$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : -1;
$status = array();

$keyValid = ($key == 'eGtaHquWXDjm5suD4NcVzlzCckMjzJiVp4skDg42') ? true : false;
error_reporting(E_ALL);
ini_set( 'display_errors', 'stdout' ) ;

if(!$keyValid){
	die("du doof.");
	exit;
}

define( 'INCLUDED', true ) ;

include ( 'includes/common.php' ) ;
include ( '../config/config-protected.php' ) ;


include ( '../core/dbsimple/Generic.php' ) ;

$CharDB = dbsimple_Generic::connect( "" . $realmd['db_type'] . "://" . $realmd['db_username'] .
	":" . $realmd['db_password'] . "@" . $realmd['db_host'] . ":" . $realmd['db_port'] .
	"/" . "rc3_char" . "" ) ;

//echo var_dump($CharDB);

$CharDB->query( "SET NAMES " . $realmd['db_encoding'] ) ;

$DataDB = dbsimple_Generic::connect( "" . $realmd['db_type'] . "://" . $realmd['db_username'] .
	":" . $realmd['db_password'] . "@" . $realmd['db_host'] . ":" . $realmd['db_port'] .
	"/" . "data" . "" ) ;
$DataDB->query( "SET NAMES " . $realmd['db_encoding'] ) ;

$newRaidIds = array();

if($action == "restore-ids"){

	// order by defeated bosses, so if 2 players save the instance on different stages, take the latest
	$extendRows = $DataDB->select("SELECT * FROM instance_extends WHERE 1 ORDER BY completedEncounters DESC;");	
	
	if(count($extendRows)){
		foreach($extendRows as $extendRow){
		
			$characterId = $extendRow["characterId"];
			$extendId = $extendRow["instanceId"];
			
			// Check what IDs this character already got
			$existingRows = $CharDB->select("SELECT 
				ci.`guid`, ci.`instance`, 
				i.`map`, i.`difficulty`, i.`completedEncounters`, i.`data`   
				FROM `character_instance` as ci, `instance` as i 
				WHERE ci.`guid` = '".$characterId."' AND ci.instance = i.id AND ci.permanent = 1;");
			
			$gotIdAlready = false;
			
			if(count($existingRows)){
				foreach($existingRows as $existingRow){
					if($existingRow["map"] == $extendRow["map"] && $existingRow["difficulty"] == $extendRow["difficulty"])
						$gotIdAlready = true;
				}
			}
			
			// Skip extension if character already has an ID for this map/difficulty
			if($gotIdAlready){
				status("ID $extendId skipped for character $characterId - has already an ID for this map.");
				continue;
			}
			
			if(!isset($newRaidIds[$extendId])){
				// Get new tidy
				$result = $CharDB->select("SELECT id FROM instance WHERE 1 ORDER BY id DESC LIMIT 1;");
				if(count($result) > 0){
					$newRaidId = $result[0]["id"]+1;
				}
				else{
					$newRaidId = 1;
				}
				$newRaidIds[$extendId] = $newRaidId;
				
				// Insert the instance
				$result = $CharDB->query("INSERT INTO instance (id, map, difficulty, resettime, completedEncounters, data) VALUES 
				($newRaidId, ".$extendRow["map"].", ".$extendRow["difficulty"].", 1323838800, ".$extendRow["completedEncounters"].", '".$extendRow["data"]."');");
				status("Instance $extendId restored with new Id $newRaidId.");
			}
			else{
				$newRaidId = $newRaidIds[$extendId];
				status("Instance $extendId has already been restored with new Id $newRaidId.");
			}
			
			// Link the character to this instance
			$result = $CharDB->query("INSERT INTO character_instance (guid, instance, permanent) VALUES 
			($characterId, $newRaidId, 1);");
			status("Character $characterId linked to new instance $newRaidId.");
			
			if(empty($CharDB->errmsg))
				echo "success".print_r($CharDB,true);
			else 
				echo "error".print_r($CharDB,true);
			
		}
	}
		
}
/*
 * Aktualisiert alle Speicherstände der gesicherten IDs
 * sollte vor dem ID Reset ausgeführt werden
 */
elseif($action == "update-ids"){
	
	$extendRows = $DataDB->select("SELECT * FROM instance_extends WHERE 1 GROUP BY instanceId;");	
	if(count($extendRows)){
		foreach($extendRows as $extendRow){
			$instanceId = $extendRow["instanceId"];
			$resultRow = $CharDB->select("SELECT * FROM instance WHERE id = $instanceId LIMIT 1;");
			if(count($resultRow)){
				$DataDB->query("UPDATE instance_extends 
					SET data = '".$resultRow[0]["data"]."', completedEncounters = ".$resultRow[0]["completedEncounters"]." 
					WHERE instanceId = $instanceId;");
				status("Instance $instanceId updated.");
			}
		}
	}
}
//echo statusOutput();
die("<br>ScriptEnde");
				
?>