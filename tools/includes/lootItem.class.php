<?
require_once("common.php");

class LootItem{
	public $id;
	public $mobID;
	public $name;
	public $sortString;	// Combi aus Rarity und Name
	
	public $chance = -1;
	//public $pctAa = -1;
	
	public $min = 1;
	public $max = 1;
	public $rarity = 0;
	public $itemLevel = 1;
	public $heroic = false;
	public $cat = "", $subCat = "";
	public $questItem = false, $elderItem = false;
	public $dropNormal = false, $chanceNormal = 0;
	public $dropHeroic = false, $chanceHeroic = 0;
	public $dropNormal25 = false, $chanceNormal25 = 0;
	public $dropHeroic25 = false, $chanceHeroic25 = 0;
	public $lootCondition = array(0,0,0);
	
	public function LootItem($itemID = 0, $mobID = 0){
		$this->id = $itemID;
		$this->mobID = $mobID;
		//echo "\nNew Item: ".print_r($this,true);
	}
	
	public function matchWoWhead($matches, $i){
		/*
		{"classs":15,"id":47242,"level":80,"name":"3Trophy of the Crusade","slot":0,"source":[2],"sourcemore":[{"z":4722}],"subclass":-2,looted:{"mode":80,"4":{"count":19298,"outof":39447},"16":{"count":16639,"outof":17285},"64":{"count":2659,"outof":2722}},count:19298,stack:[1,1]},
		                                   
			'@\{(?:"armor":\d+,)?
		1		"classs":(\d+),		
				(?:"displayid":\d+,)?
				(?:"dps":[^,]+,)?
		2		("heroic":\d,)?
		3		"id":(\d+),
		4		"level":(\d+),
		5 6		"name":"(\d)([^"]+)",
				(?:"reqlevel":\d+,)?
		7		("reqrace":(\d+),)?
		8		("side":\d,)?
				(?:"skill":\d+,)?
				(?:"slot":\d+,)?
				(?:.*?)?
		9		(modes:\{"mode":\d+,
    	10-13		("(\d)":\{"count":(\d+),"outof":(\d+)\},?)
    	14-17		("(\d)":\{"count":(\d+),"outof":(\d+)\},?)?
    	18-21		("(\d)":\{"count":(\d+),"outof":(\d+)\},?)?
    	22-25		("(\d)":\{"count":(\d+),"outof":(\d+)\},?)?
    	26-29		("(\d)":\{"count":(\d+),"outof":(\d+)\},?)?
    			}),count:@';
				
        */
        
		
		$this->heroic = (substr_count($matches[2], "heroic") > 0);
		$this->id = $matches[3][$i];
		
		//if($this->id == 49723)
		//	print_r($matches);
		
		$this->min = 1;
		$this->max = 1;
		$this->cat = $matches[1][$i];
		$this->itemLevel = $matches[4][$i];
				
		switch($matches[5][$i]){
			case 0:	$this->rarity = HEIRLOOM; break;
			case 1:	$this->rarity = ARTIFACT; break;
			case 2:	$this->rarity = ORANGE; break;
			case 3:	$this->rarity = PURPLE; break;
			case 4:	$this->rarity = BLUE; break;
			case 5:	$this->rarity = GREEN; break;
			case 6:	$this->rarity = WHITE; break;
			case 7:	$this->rarity = GREY; break;
			default:	
				$this->rarity = UNKOWN; break;
		}
		
		$this->name = $matches[6][$i];
		$this->setSortString();
		
		$array = array(10,14,18,22,26);
		
		foreach($array as $pos){
			if($matches[$pos+1][$i] == 2 || $matches[$pos+1][$i] == 8){
				$this->dropNormal = true;
				$this->chanceNormal = round(($matches[$pos+2][$i]/$matches[$pos+3][$i])*100, 2);
			}
			elseif($matches[$pos+1][$i] == 1 || $matches[$pos+1][$i] == 32){
				$this->dropHeroic = true;
				$this->chanceHeroic = round(($matches[$pos+2][$i]/$matches[$pos+3][$i])*100, 2);
			}
			elseif($matches[$pos+1][$i] == 16){
				$this->dropNormal25 = true;
				$this->chanceNormal25 = round(($matches[$pos+2][$i]/$matches[$pos+3][$i])*100, 2);
			}
			elseif($matches[$pos+1][$i] == 64){
				$this->dropHeroic25 = true;
				$this->chanceHeroic25 = round(($matches[$pos+2][$i]/$matches[$pos+3][$i])*100, 2);
			}
		}
		/*
		if($matches[11][$i] == 1 || $matches[13][$i] == 1 || $matches[15][$i] == 1 || $matches[17][$i] == 1){
			$this->dropHeroic = true;
			
			$this->chanceHeroic = 0;
		}
		if($matches[11][$i] == 2 || $matches[13][$i] == 2 || $matches[15][$i] == 2 || $matches[17][$i] == 2)
			$this->dropNormal = true;*/
		
		//echo "\nThis A ".print_r($this,true);
		//die();
		//$this->setCat(getAdbCat($matches[11]));
	}
	
	public function getChance(){
		$i = 0;
		$pct = 0.0;
		if($this->pctAdb > 0){
			$i++;
			//$pct += $this->pctAdb;
		}
		if($this->pctYg > 0){
			$i++;
			//$pct += $this->pctYg;
		}
		return "\t\tA: ".print_r($this->pctAdb,true).", Y: ".print_r($this->pctYg,true);
		//return $pct/$i;
	}
	
	public function getMinMax(){
		return ($this->max > $this->min) ? $this->min."-".$this->max : $this->min;
	}
	
	protected function addMax($max){
		if($this->max == false)
			$this->max = $max;
		else
			$this->max = max($this->max, $max);
	}
	protected function addMin($min){
		if($this->min == false)
			$this->min = $min;
		else
			$this->min = min($this->min, $min);
	}
	
	protected function getAdbCat($cat){
		switch($cat){
			case 1: return "GREATSWORD"; break;
			case 2: return "POWER_SHARD"; break;
			case 3: return "BOOK"; break;
			case 4: return "BOW"; break;
			case 5: return "HAT"; break;
			case 6: return "DAGGER"; break;
			case 7: return "MAT"; break;			// Handwerksmaterial
			case 8: return "MACE"; break;
			case 9: return "MANASTONE"; break;
			case 10: return "NECKLACE"; break;
			case 11: return "JEWEL"; break;
			case 12: return "SPEAR"; break;
			case 13: return "SHOULDER"; break;
			case 14: return "STAFF"; break;
			case 15: return "SWORD"; break;
			case 16: return "KINAH"; break;
			case 17: return "POTION"; break;
			case 18: return "RING"; break;
			case 19: return "JUNK"; break;
			case 20: return "STIGMA"; break;
			case 21: return "LEGS"; break;
			case 22: return "BELT"; break;
			case 23: return "SHOES"; break;
		}
	}
	
	protected function setCat($cat){
		if(!isset($this->cat))
			$this->cat = $cat;	
	}
	
	protected function setId($id){
		if(!isset($this->id))
			$this->id = $id;	
	}
	
	protected function setName($name){
		if(!isset($this->name))
			$this->name = $name;	
	}
	
	public function setSortString(){
		if(	$this->rarity == 0 || empty($this->name)){
			return;
		}
		$this->sortString = $this->rarity.$this->id;
	}
	

}

?>