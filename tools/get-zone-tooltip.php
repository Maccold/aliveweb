<?
/*
echo strlen(time());

for($i = 0; $i <= 102; $i++){
	if( $i < 10)
		$s = "00$i";
	elseif( $i < 100)
		$s = "0$i";
	else
		$s = $i;
	
	echo '<img src="http://eu.battle.net/wow/static/images/arena/banners/border_'.$s.'.png">';
}*/

/*
4196 => array(
		'level' => "72–80",
		'name' => "Feste von Drak&#39;Tharon",
		'desc' => "Einst diente die Feste Drak&#39;Tharon den Drakkari-Trollen als uneinnehmbarer Außenposten am Rande ihres Imperiums, Zul'Drak.",
		'heroic' => true,
		'location' => "Grizzlyhügel",
		"class" => "draktharon-keep",
		"expansion" => 2,
	)
*/

$zones = array(
2017, 719, 1581, 1337, 717, 1477, 1176, 2717, 2677, 3429, 3428
);
/*
4196, 4416, 4723, 4415, 4494, 4277, 206, 1196, 4228, 4265, 4809, 4813, 4820, 4100, 4272, 4264, 4603, 4500, 4493, 4987, 
4812, 3456, 2159, 4722, 4273, 4131, 3790, 3792, 3789, 3791, 3716, 3715, 3717, 3848, 3847, 3849, 2367, 2366, 3713, 3714, 
3562, 3959, 4075, 3606, 3845, 3923, 3607, 3457, 3836, 209, 2437, 2557, 721, 718, 722, 491, 2100, 796, 2057, 1583, 1584, 
2017, 719, 1581, 1337, 717, 1477, 1176, 2717, 2677, 3429, 3428
*/


foreach($zones as $zone_key){

	$zone = array();
	
	$string = file_get_contents("http://eu.battle.net/wow/de/zone/".$zone_key."/tooltip");
	
	$regexZone = '@/zones/thumbnails/([a-z-_]).jpg\)"@';
	//echo "\n<br>Regex: $regexAdbMobName";
	if(preg_match($regexZone, $string, $matches)){
		//echo "\n<br>Match: ".print_r($matches,true);
		$zone["level"] = $matches[1];
	}
	
	if(substr_count($string, 'icon-heroic-skull') > 0){
		//echo "<br>heroic";
		$zone["heroic"] = true;
	}
	if(substr_count($string, 'color-ex2') > 0){
		$zone["expansion"] = 2;
	}
	elseif(substr_count($string, 'color-ex1') > 0){
		$zone["expansion"] = 1;
	}
	else
		$zone["expansion"] = 0;
	
	if(substr_count($string, 'Dungeon') > 0){
		$zone["type"] = "Dungeon";
	}
	if(substr_count($string, 'Schlachtzug') > 0){
		$zone["type"] = "Schlachtzug";
	}
	
	
	
	
	$regexZone = '@\/zones\/thumbnails\/([a-z-]+).jpg\)"@';
	//echo "\n<br>Regex: $regexAdbMobName";
	if(preg_match($regexZone, $string, $matches)){
		//echo "\n<br>Match CSS: ".print_r($matches,true);
		$zone["class"] = $matches[1];
	}
	
	$regex = '@Stufe ([^\<]+)\<@';
	if(preg_match($regex, $string, $matches)){
		//echo "\n<br>Match Level 2: ".print_r($matches,true);
		$zone["level"] = preg_replace("@[^0-9]@", "-", $matches[1]);
		$zone["level"] = preg_replace("@(\d+)[-]+(\d+)[-]+@", "$1-$2", $zone["level"]);
	}
	else{
		$regex = '@Stufe (\d+)@';
		if(preg_match($regex, $string, $matches)){
			//echo "\n<br>Match Level: ".print_r($matches,true);
			$zone["level"] = $matches[1];
		}
	}
	
	$regex = '@</span>([^<]+)</h3>@';
	if(preg_match($regex, $string, $matches)){
		//echo "\n<br>Name: ".print_r($matches,true);
		$zone["name"] = trim($matches[1]);
	}
	
	$regex = '@<div class="color-tooltip-yellow">([^<]+)</div>@';
	if(preg_match($regex, $string, $matches)){
		//echo "\n<br>Desc: ".print_r($matches,true);
		$zone["desc"] = trim($matches[1]);
		
		$zone["desc"] = utf8_decode($zone["desc"]);
	}
	
	$regex = '@Ort:</span>([^<]+)</li>@';
	if(preg_match($regex, $string, $matches)){
		//echo "\n<br>Loc: ".print_r($matches,true);
		$zone["location"] = trim($matches[1]);
		
		$zone["location"] = utf8_decode($zone["location"]);
	}

	?>
	<?=$zone_key?> => array(
		"name" => "<?=$zone["name"]?>",
		"heroic" => <?=$zone["heroic"]?>,
		"level" => "<?=$zone["level"]?>",
		"class" => "<?=$zone["class"]?>",
		"location" => "<?=$zone["location"]?>",
		"type" => "<?=$zone["type"]?>",
		"expansion" => <?=$zone["expansion"]?>,
		"desc" => "<?=$zone["desc"]?>",
	);
	<?
}

//print_r($zone);

