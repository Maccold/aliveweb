<?php

/****************************************************************************/
/*    < MangosWeb is a Web-Fonted for Mangos (mangosproject.org) >          */
/*    Copyright (C) <2007>  <Sasha,TGM,Peec,Nafe>                           */
/*                                                                          */
/*    This program is free software: you can redistribute it and/or modify  */
/*    it under the terms of the GNU General Public License as published by  */
/*    the Free Software Foundation, either version 2 of the License, or     */
/*    (at your option) any later version.                                   */
/*                                                                          */
/*    This program is distributed in the hope that it will be useful,       */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*    GNU General Public License for more details.                          */
/*                                                                          */
/*    You should have received a copy of the GNU General Public License     */
/*    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                          */
/*                                $Rev$                                     */
/****************************************************************************/
// Set error reporting to only a few things.
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
ini_set( 'display_errors', 'stdout' ) ;
// Define INCLUDED so that we can check other pages if they are included by this file
define( 'INCLUDED', true ) ;

// Start a variable that shows how fast page loaded.
$time_start = microtime( 1 ) ;
$_SERVER['REQUEST_TIME'] = time() ;

// Initialize config's.
include ( 'core/class.mangosweb.php' ) ;
$MW = new MangosWeb ; // Super global.

$GLOBALS['users_online'] = array() ;
$GLOBALS['guests_online'] = 0 ;
$GLOBALS['messages'] = '' ;
$GLOBALS['redirect'] = '' ;
$GLOBALS['sidebarmessages'] = '' ;
$GLOBALS['context_menu'] = array() ;
$GLOBALS['user_cur_lang'] = ( string )$MW->getConfig->generic->default_lang ;

/*
|---------------------------------------------------------------
| Site functions & classes
|---------------------------------------------------------------
*/
include ( 'core/common.php' ) ;
include ( 'core/mangos.class.php' ) ;
include ( 'core/class.user.php' ) ;
include ( 'core/class.auth.php' ) ;
include ( 'core/dbsimple/Generic.php' ) ;
include ( 'core/class.captcha.php' ) ;
include ( 'core/cache_class/safeIO.php' ) ;
include ( 'core/cache_class/gCache.php' ) ;
include ( 'config/routes.php' ) ;

save_session_start();

/*
|---------------------------------------------------------------
| Terms of Service
|---------------------------------------------------------------
*/
if ( ($MW->getConfig->security->require_tos == 1) && file_exists( "ToS.html" ) && ! isset( $_COOKIE['agreement_accepted'] ) )
{
	include ( 'notice.php' ) ;
	exit() ;
}


// Inizialize different variables.
global $MW, $mangos ;

$base_template = 	"templates/".(string)$MW->getConfig->generic->template;
$default_template = "templates/".(string)$MW->getConfig->generic->default_template;
$currtmp = $default_template;

/*
|---------------------------------------------------------------
| Cache handling
|---------------------------------------------------------------
*/
$cache = new gCache ;
$cache->folder = './core/cache/sites' ;
$cache->timeout = $MW->getConfig->generic->cache_expiretime ;

/*
|---------------------------------------------------------------
| Database connection
| DB layer documentation at http://en.dklab.ru/lib/DbSimple/
|---------------------------------------------------------------
*/
$DB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
	":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
	"/" . $MW->getDbInfo['db_name'] . "" ) ;
// Set error handler for $DB.
$DB->setErrorHandler( 'databaseErrorHandler' ) ;
// Also set to default encoding for $DB
$DB->query( "SET NAMES " . $MW->getDbInfo['db_encoding'] ) ;

// Play arround for IIS lake on $_SERVER['REQUEST_URI']
if ( $_SERVER['REQUEST_URI'] == "" )
{
	if ( $_SERVER['QUERY_STRING'] != "" )
	{
		$__SERVER['REQUEST_URI'] = $_SERVER["SCRIPT_NAME"] . "?" . $_SERVER['QUERY_STRING'] ;
	} else
	{
		$__SERVER['REQUEST_URI'] = $_SERVER["SCRIPT_NAME"] ;
	}
} else
{
	$__SERVER['REQUEST_URI'] = $_SERVER['REQUEST_URI'] ;
}


// Build path vars //
$MW->add_temp_confs( array( 'site_href' => str_replace( '//', '/', str_replace( '\\', '/', dirname( $_SERVER['SCRIPT_NAME'] ) . '/' ) ) ) ) ;
$MW->add_temp_confs( array( 'site_domain' => $_SERVER['HTTP_HOST'], 'email_href' =>	$_SERVER['HTTP_HOST'] ) ) ;
$MW->add_temp_confs( array( 'base_href' => 'http://' . $MW->getConfig->temp->email_href . '' . $MW->getConfig->temp->site_href, ) ) ;


/*
|---------------------------------------------------------------
| Language
|---------------------------------------------------------------
*/
if ( isset( $_COOKIE['Language'] ) )
	$GLOBALS['user_cur_lang'] = $_COOKIE['Language'] ;
loadLanguages() ;

/*
|---------------------------------------------------------------
| User
|---------------------------------------------------------------
*/
$auth = new AUTH( $DB, $MW->getConfig ) ;
$user = $auth->user ;

$userObject = new User($user);


if($userObject->isLoggedIn()){
	
	// Personal Messages
	$userObject->pm_num = $auth->check_pm();
	
	// User selected theme
	if(isset($user["theme"])){
		foreach ( $MW->getConfig->templates->template as $template )
			$currtmp2[] = $template;
		if(isset($currtmp2[$user["theme"]]))
			$currtmp = "templates/" . $currtmp2[$user["theme"]] ;
	}
}


// Load Permissions and aviable sites.
include ( 'core/default_components.php' ) ;

// Start of context menu. ( Only make an array for later output )
$GLOBALS['context_menu'][] = array( 
	'title' => $lang['mainpage'], 
	'link' => 'index.php' 
);

if ( $user['id'] <= 0 )
{
	$GLOBALS['context_menu'][] = array( 
		'title' => $lang['register'], 
		'link' => mw_url( 'account', 'register' ) 
	) ;
}

$forum_link = url_for("forum");

if( (int)$MW->getConfig->generic->externalforum ){
	$forum_link = $MW->getConfig->generic->forum_external_link;
}
$GLOBALS['context_menu'][] = array( 
	'title' => 'Forum', 
	'link' =>  $forum_link) ;

$GLOBALS['context_menu'][] = array( 
	'title' => $lang['players_online'], 
	'link' => url_for( 'server', 'playersonline' ), 
);

if ( ( isset( $user['g_is_admin'] ) || isset( $user['g_is_supadmin'] ) ) && ( $user['g_is_admin'] == 1 || $user['g_is_supadmin'] == 1 ) )
{
	$allowed_ext[] = 'admin' ;
	$GLOBALS['context_menu'][] = array( 'title' => '------------------', 'link' =>
		'#' ) ;
	$GLOBALS['context_menu'][] = array( 'title' => $lang['admin_panel'], 'link' =>
		'index.php?n=admin' ) ;
}

// for mod_rewrite query_string fix //
global $_GETVARS ;

$req_vars = parse_url( $__SERVER['REQUEST_URI'] ) ;
if ( isset( $req_vars['query'] ) )
{
	parse_str( $req_vars['query'], $req_arr ) ;
	$_GETVARS = $req_arr ;
}
unset( $req_arr, $req_vars ) ;

/*
|---------------------------------------------------------------
| Multi-Realm handling
|---------------------------------------------------------------
*/

// Finds out what realm we are viewing.
if ( ( int )$MW->getConfig->generic_values->realm_info->multirealm && isset( $_REQUEST['changerealm_to'] ) )
{
	setcookie( "cur_selected_realmd", intval( $_REQUEST['changerealm_to'] ), time() + ( 3600 * 24 ) ) ; // expire in 24 hour
	$user['cur_selected_realmd'] = intval( $_REQUEST['changerealm_to'] ) ;
} 
elseif ( ( int )$MW->getConfig->generic_values->realm_info->multirealm && isset( $_COOKIE['cur_selected_realmd'] ) )
{
	$user['cur_selected_realmd'] = intval( $_COOKIE['cur_selected_realmd'] ) ;
} 
else
{
	$user['cur_selected_realmd'] = ( int )$MW->getConfig->generic_values->realm_info->default_realm_id ;
	setcookie( "cur_selected_realmd", $user['cur_selected_realmd'], time() + ( 3600 * 24 ) ) ;
}

// Make an array from `dbinfo` column for the selected realm..
$mangos_info = $DB->selectCell( "SELECT dbinfo FROM `realmlist` WHERE id=?d", $user['cur_selected_realmd'] ) ;
$dbinfo_mangos = explode( ';', $mangos_info ) ;
if ( ( int )$MW->getConfig->generic->use_archaeic_dbinfo_format )
{
	//alternate config - for users upgrading from Modded MaNGOS Web
	//DBinfo column:  host;port;username;password;WorldDBname;CharDBname
	$mangos = array( 'db_type' => 'mysql', 'db_host' => $dbinfo_mangos['0'],
		//ip of db world
		'db_port' => $dbinfo_mangos['1'], //port
		'db_username' => $dbinfo_mangos['2'], //world user
		'db_password' => $dbinfo_mangos['3'], //world password
		'db_name' => $dbinfo_mangos['4'], //world db name
		'db_char' => $dbinfo_mangos['5'], //character db name
		'db_encoding' => 'utf8', // don't change
		) ;
} 
else
{
	//normal config, as outlined in how-to
	//DBinfo column:  username;password;port;host;WorldDBname;CharDBname
	$mangos = array( 'db_type' => 'mysql', 'db_host' => $dbinfo_mangos['3'],
		//ip of db world
		'db_port' => $dbinfo_mangos['2'], //port
		'db_username' => $dbinfo_mangos['0'], //world user
		'db_password' => $dbinfo_mangos['1'], //world password
		'db_name' => $dbinfo_mangos['4'], //world db name
		'db_char' => $dbinfo_mangos['5'], //character db name
		'db_encoding' => 'utf8', // don't change
		) ;
}
unset( $dbinfo_mangos, $mangos_info ) ; // Free up memory.

if ( ( int )$MW->getConfig->generic->use_alternate_mangosdb_port )
{
	$mangos['db_port'] = ( int )$MW->getConfig->generic->use_alternate_mangosdb_port ;
}

// Output error message and die if user has not changed info in realmd.realmlist.`dbinfo` .
if ( $mangos['db_host'] == '127.0.0.1' && $mangos['db_port'] == '3306' && $mangos['db_username'] ==
	'username' && $mangos['db_password'] == 'password' && $mangos['db_name'] ==
	'DBName' )
{
	echo "Please read README_HOWTO.txt, This is a error message btw. You must remember to setup the WORLD database information in the realm.realmlist database! :)<br />Edit the `dbinfo` to: World database info: username;password;3306;127.0.0.1;DBName" ;
	die ;
}

//Connects to WORLD DB
//echo '<pre>'.print_r($mangos,true).'</pre>';
$WSDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
	":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
	"/" . $mangos['db_name'] . "" ) ;
if ( $WSDB ){
	$WSDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$WSDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}

$CHDB = DbSimple_Generic::connect( "" . $mangos['db_type'] . "://" . $mangos['db_username'] .
	":" . $mangos['db_password'] . "@" . $mangos['db_host'] . ":" . $mangos['db_port'] .
	"/" . $mangos['db_char'] . "" ) ;
if ( $CHDB ){
	$CHDB->setErrorHandler( 'databaseErrorHandler' ) ;
	$CHDB->query( "SET NAMES " . $mangos['db_encoding'] ) ;
}

//Load characters list
if ( isset( $user['id'] ) && $user['id'] > 0 )
{
	$characters = $CHDB->select( 'SELECT guid,name FROM `characters` WHERE account=?d', $user['id'] ) ;
	if ( isset( $_COOKIE['cur_selected_character'] ) )
	{
		foreach ( $characters as $character )
		{
			if ( $character['guid'] == $_COOKIE['cur_selected_character'] && $user["character_id"] != $character['guid'] )
			{
				$DB->query( 'UPDATE account_extend SET character_id=?d,character_name=? WHERE account_id=?d',
					$character['guid'], $character['name'], $user['id'] ) ;
			}
		}
	}
} else
{
	$characters = array() ;
}


/*
|---------------------------------------------------------------
| Routing
|---------------------------------------------------------------
*/
try 
{
	if ( $oRouter->match( $oRequest->getPath()) )
	{
		foreach ($oRouter->getParameters() as $key => $value)
		{
			$$key = $value;
		}
	}
}
catch (icException $e)
{
	echo 'Error: '.$e->getMessage().'<br />';
}
if( isset( $_REQUEST['n'] ) || $ext == "index.php" || $ext == "index_dev.php" || empty($ext))
	$ext = ( isset( $_REQUEST['n'] ) ? $_REQUEST['n'] : ( string )$MW->getConfig->generic->default_component ) ;

	
if(isset( $_REQUEST['sub'] ) || empty($sub)){
	if ( strpos( $ext, '/' ) !== false )
		list( $ext, $sub ) = explode( '/', $ext ) ;
	else
		$sub = ( isset( $_REQUEST['sub'] ) ? $_REQUEST['sub'] : 'index' ) ;
}

if( $_GET['p'] ){
	if ( empty( $_GET['p'] ) or $_GET['p'] < 1 )
		$p = 1 ;
	else
		$p = $_GET['p'] ;
}
$req_tpl = false ;

/*
|---------------------------------------------------------------
| Initialize Modules
|---------------------------------------------------------------
*/

//if installing a new module, please delete the cache file
include ( 'components/modules/initialize.php' ) ;

/*
|---------------------------------------------------------------
| Template Variables
|---------------------------------------------------------------
*/
$this_rss_url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$this_rss_url = explode('/index.php',$this_rss_url);
$this_rss_url = "http://".$this_rss_url[0];
if($this_rss_url[strlen($this_rss_url)-1] != '/') {
    $this_rss_url .= '/';
}
$this_rss_url = (string)$MW->getConfig->generic_values->rss_link;
$page_rss_title = (string)$MW->getConfig->generic->site_title.' RSS News Feed';

$page_title = (string)$MW->getConfig->generic->site_title. " ". $title_str;

// Wenn das template eine eigene Version der Datei hat, benutz diese
if(file_exists($currtmp.'/body_functions.php')){
	include($currtmp.'/body_functions.php');
}
else
	include($base_template.'/body_functions.php');
		
if ((int)$MW->getConfig->generic_values->realm_info->multirealm){
    $realms = $DB->select("SELECT `id`,`name` FROM `realmlist` ORDER by id DESC");
}

/*
 * Main Menu
 */
ob_start();
echo build_main_menu();
$main_menu = ob_get_contents();
ob_end_clean();
unset($mainnav_links); // Free up memory. This is the big link array.
               
              
if ( in_array( $ext, $allowed_ext ) )
{
	// load main component
	require ( 'components/'.$ext.'/'.'main.php' ) ;
	
	$action_file_name = $ext.'.'.$sub.'.php';
	
	// The Script
	$script_file = 'components/'.$ext.'/'.$action_file_name;
	
	// The Layout
	if(file_exists($currtmp.'/'.$ext.'/'.$action_file_name))
		$view_file = $currtmp.'/'.$ext.'/'.$action_file_name;
	else
		$view_file = $base_template.'/'.$ext.'/'.$action_file_name;
	//$template_file = 'templates/' . ( string )$MW->getConfig->generic->template . '/' . $ext . '/' . $ext . '.' . $sub . '.php' ;
	
	// set defaults here to be loaded -- these can be changed via the main.php or whatnot
	// this is used especially in the case of the module system
	
	if(file_exists($currtmp.'/template.php'))
		$template_file = $currtmp.'/template.php';
	else
		$template_file = $base_template.'/template.php';

	$group_privilege = $com_content[$ext][$sub][0] ;
	$expectation = ( substr( $group_privilege, 0, 1 ) == '!' ) ? 0 : 1 ;
	if ( $expectation == 0 )
		$group_privilege = substr( $group_privilege, 1 ) ;
	if ( $group_privilege && $user[$group_privilege] != $expectation )
		exit( '<h2>Forbidden</h2><meta http-equiv=refresh content="3;url=\'./\'">' ) ;
	
	// ==================== //
	if ( isset( $_REQUEST['n'] ) && isset( $lang[$com_content[$ext]['index'][1]] ) )
		$pathway_info[] = array( 
			'title' => $lang[$com_content[$ext]['index'][1]],
			'link' => $com_content[$ext]['index'][2] ) ;
	// ==================== //
	foreach ( $com_content[( string )$ext] as $sub_name => $sub_conf )
	{
		if ( $sub_conf[4] == 1 )
		{
			if ( $sub_conf[0] )
			{
				if ( $user[$sub_conf[0]] == 1 )
				{
					$GLOBALS['context_menu'][] = array( 
						'title' => ( isset( $lang[$sub_conf[1]] ) ?	$lang[$sub_conf[1]] : $sub_conf[1] ), 
						'link' => ( isset( $sub_conf[2] ) ? $sub_conf[2] : '#' ) ) ;
				}
			} else
			{
				if ( isset( $lang[$sub_conf[1]] ) )
					$GLOBALS['context_menu'][] = array( 'title' => $lang[$sub_conf[1]], 'link' => $sub_conf[2] ) ;
			}
		}
	}
	if ( $sub && $com_content[$ext][$sub] )
	{
		if ( $com_content[$ext][$sub][0] )
		{
			if ( $user[$com_content[$ext][$sub][0]] == 1 )
			{
				$req_tpl = true ;
				@include ( $script_file ) ;
			}
		} 
		else
		{
			$req_tpl = true ;
			@include ( $script_file ) ;

		}
	}
	
	/* 
	 * Sidebar 
	 */
	if($show_sidebar || $ext == "frontpage"){
		// Quicklinks
		if( (int)$MW->getConfig->components->right_section->quicklinks ){
			$quicklinks = array(
				array(	"Account", 				url_for('account', 'manage')),	
				array(	$lang['armory'], 		"http://arsenal.wow-alive.de/"),
				array(	"Top 100", 				"http://top100.wow-alive.de/raiders.php"),
				array(  "Talentrechner",		"http://arsenal.wow-alive.de/talent-calc.php"),
				array(	$lang['vote_system'], 	url_for('community', 'vote')),
				array(	$lang['char_manage'], 	url_for('account', 'chartools')),
				array(	$lang['ct'], 			url_for('account', 'ct')),
				array(	$lang['item'], 			url_for('community', 'item')),
			);
			
		}
		
		// Screenshot of the day 
		if ((int)$MW->getConfig->components->right_section->media){
			$date_ssotd = $DB->selectCell("SELECT `date` FROM `gallery_ssotd` LIMIT 1");
			$today_ssotd = date("y.m.d");
			if ($date_ssotd != $today_ssotd) {
				$rand_ssotd = $DB->selectCell("SELECT `img` FROM `gallery` WHERE cat ='screenshot' ORDER BY RAND() LIMIT 1");
				$DB->query("UPDATE gallery_ssotd SET image = '$rand_ssotd', date = '$today_ssotd'");
			}
			$screen_otd = $DB->selectCell("SELECT `img` FROM `gallery` WHERE cat ='screenshot' ORDER BY RAND() LIMIT 1");
		}
		
		ob_start();
	
		if(file_exists($currtmp.'/sidebar.php'))
			include($currtmp.'/sidebar.php');
		else
			include($base_template.'/sidebar.php');
	
		$sidebar_content = ob_get_contents();
	
		ob_end_clean();
	}

	
	// DEBUG //
	if ( ( int )$MW->getConfig->generic->debuginfo )
	{
		output_message( 'debug', 'DEBUG://' . $DB->_statistics['count'] ) ;
		output_message( 'debug', '<pre>' . print_r( $_SERVER, true ) . '</pre>' ) ;
	}
	// =======//
	
	if ( $req_tpl && file_exists( $view_file ) )
	{
		// Only cache if user is not logged in.
		if ( $user['id'] < 0 && ( int )$MW->getConfig->generic->cache_expiretime != 0 )
		{

			// Start caching process But we want to exclude some cases.
			if ( isset( $_REQUEST['n'] ) && $_REQUEST['n'] != 'account' )
			{
				$cache->contentId = md5( 'CONTENT' . $_SERVER['REQUEST_URI'] ) ;
				if ( $cache->Valid() )
				{
					$content = $cache->content ;
				} 
				else
				{
					$cache->capture() ;
					include ( $view_file ) ;
					$content = $cache->returnCapture();
					$cache->endcapture(false);
				echo dump($view_file);
				}
			} 
			else
			{
				ob_start();
				include ( $view_file ) ;
				$content = ob_get_contents();
				ob_end_clean();
			}

		} 
		else
		{
			// Create output buffer
			ob_start() ;
			include ( $view_file ) ;
			$content = ob_get_contents();
			ob_end_clean();	
		}
	}
	else{
		echo dump($req_tpl);
		echo dump($view_file);
		die("doof");	
	}
	$time_end = microtime( 1 ) ;
	$exec_time = $time_end - $time_start ;
	//include ( 'templates/' . ( string )$MW->getConfig->generic->template.'/body_footer.php' ) ;
	
	if ( empty( $_GET['nobody'] ) ){
		if ( file_exists( $template_file ) )
			include($template_file);
	} 
	else
	{
		echo $content;
	}
} 
else
{
	echo '<h2>Forbidden</h2><meta http-equiv=refresh content="3;url=\'./\'">' ;
	echo dump($ext);
}

?>