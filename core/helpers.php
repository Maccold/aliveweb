<?php

// Helper Functions
function select_field($name, $values, $currentValue = ""){
	global $post_data;
	
	if(empty($currentValue) && isset($post_data[$name]))
		$currentValue = $post_data[$name];
	
	$string = "";
	$string .= '<select name="'.$name.'" id="'.$name.'" class="input">';
	foreach($values as $value){
		if($value == $currentValue)
			$string .= '<option selected="selected">'.$value.'</option>';
		else
			$string .= '<option>'.$value.'</option>';
		
	}
	$string .= '</select>';
	return $string;
}