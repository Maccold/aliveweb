<?php

define("CLASS_USER", true);

class User {
	
	var $id = 0;
	var $username = "";
	var $characters = array();
	var $character_id = 0;
	var $active_char = array("guid" => 0);
	var $activeChar = NULL;
	var $character_name = "";
	var $can = array("vote" => false);
	var $votePoints = 0;
	var $faction = -1;
	var $faction_css = "nochars";
	var $theme = "";
	var $isGM = false;
	var $gmLevel = 0;
		
	function User($user_array){
		global $DB;
		
		$this->id = $user_array["id"];
		$this->character_id = $user_array["character_id"];
		$this->username = $user_array["username"];
		$this->id = $user_array["id"];
		$this->theme = $user_array["theme"];
		
		$allowances = array("g_use_pm", 'g_is_admin', 'g_is_supadmin');
		foreach($allowances as $key){
			$this->can[$key] = (isset($user_array[$key]) && $user_array[$key]) ? true : false;
		}
		
		if($this->id > 0){
			// Check if user is GM (probation excluded)
			$resultRow = $DB->select("SELECT * FROM account_access WHERE id = ?d AND gmlevel > 1;", $this->id);
			if($resultRow){
				$this->isGM = true;
				$this->can["g_is_gm"] = true;
				foreach($resultRow as $row){
					if($row["RealmID"] == 1)
						$this->gmLevel = $row["gmlevel"];
				}
			}
		}
		
	}
	
	function getName(){
		return empty($this->character_name) ? $this->username : $this->character_name;	
	}
	
	function GetActiveCharUrl(){
		return $this->active_char["url"];
	}
	
	function GetActiveCharName(){
		if(empty($this->active_char["name"]))
			return false;
		else
			return $this->active_char["name"];
	}
	
	function isLoggedIn(){
		return ($this->id > 0);
	}
	
	function isGM(){ 
		return ($this->can["g_is_admin"] || $this->can["g_is_supadmin"] || $this->isGM);
	}
	
	function isBugtrackerDev(){
		if(in_array($this->id,array(5579)))
			return true;
		else
			return false;
	}
	
	function GetGmLevel(){
		return $this->gmLevel;
	}
		
	function saveCanVote()
	{
		global $DB;
		$value = ($this->can["vote"]) ? 1 : 0;
		$DB->query("UPDATE `account_extend` SET `can_vote` = $value WHERE account_id=?d;", $this->id);
		//echo "<!-- ".print_r($DB,true)." -->";
	}
	
	function getVotePoints(){
		global $DB, $MW, $tab_sites;
		
		$ip_voting_period = 60 * 60 * 12;
		
		// Get Voting Points
		$result = $DB->select("SELECT `points`, `date`, `date_points` FROM `voting_points` WHERE `id` = ".$this->id." LIMIT 1;");
	
		if (count($result) > 0){
    		$row = $result[0];
			$_SESSION["points"] = $row["points"];
			$today = date("Ymd");
			if($row["date"] != $today)
			{
				$DB->query("UPDATE `voting_points` SET `date` = '".$today."', `date_points` = 0 WHERE `id` = ".$this->id." LIMIT 1");
				$_SESSION["date"] = $today;
				$_SESSION["date_points"] = 0;
			}
			else
			{
				$_SESSION["date"] = $row["date"];
				$_SESSION["date_points"] = $row["date_points"];
			}
		}
		else
		{
			$DB->query("INSERT INTO `voting_points` (`id`, `points`, `date`, `date_points`) VALUES (".$this->id.", 200, 20100101, 0)");
			$_SESSION["points"] = 200;
			$_SESSION["date"] = date("Ymd");
			$_SESSION["date_points"] = 0;
		}
		
		$this->votePoints = $_SESSION["points"];
		
		// Get Voting of this day
		if(!isset($_SESSION["sites"]))
			$_SESSION["sites"] = 0;
			
		$get_voting = $DB->query("SELECT `sites`, `time` FROM `voting` WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
		if (count($get_voting) > 0)
		{
			foreach ($get_voting as $row) 
			{
				$_SESSION["time"] = $row["time"];
				if((time() - $row["time"]) > $ip_voting_period)
				{
					if($row["sites"] != 0)
						$DB->query("UPDATE `voting` SET `sites` = 0 WHERE `user_ip` LIKE '".$_SERVER["REMOTE_ADDR"]."' LIMIT 1");
					$this->can["vote"] = true;
					$_SESSION["sites"] = 0;
					//echo "<!-- U -->";
				}
				else
					$_SESSION["sites"] = $row["sites"];
			}
		}
		else
		{
			$DB->query("INSERT INTO `voting` (`user_ip`) VALUES ('".$_SERVER["REMOTE_ADDR"]."')");
			$_SESSION["sites"] = 0;
			$_SESSION["time"] = 0;
		}
		
		// Check all sites of there is any one left to vote for
		$votingLeft = false;
		
		foreach($tab_sites as $key => $value)
		{
			if($_SESSION["sites"] & $key)
			{
				$tab_sites[$key]["voted"] = true;
			}
			else
			{
				$this->can["vote"] = true;
					
				//echo "<!-- $key,".print_r($tab_sites,true)." -->";
			}
		}
		
		if($_SESSION["date_points"] >= $MW->getConfig->vote_system->max_points_per_day){
			//echo "<!-- bof ".$_SESSION["date_points"]." -->";
			$this->can["vote"] = false;
		}
				
	}
	
	function changeVotePoints($diff, $datePoints = false){
		global $DB, $MW;
		
		if($diff > 0){
			$this->votePoints += $diff;
			if($datePoints)
				$DB->query("UPDATE `voting_points` SET `points` = `points` + $diff, `date_points` = (`date_points` + $diff) WHERE id=?d AND points < 100000;", $this->id);
			else
				$DB->query("UPDATE `voting_points` SET `points` = `points` + $diff WHERE id=?d", $this->id);
			$_SESSION["date_points"] += $diff;
			
			if($_SESSION["date_points"] >= $MW->getConfig->vote_system->max_points_per_day){
				$this->can["vote"] = false;
			}
		}
		else if($diff < 0){
			$this->votePoints -= abs($diff);
			$DB->query("UPDATE `voting_points` SET `points` = (`points` - ".abs($diff).") WHERE id=?d AND (`points` >= ".abs($diff).") AND points < 100000;", $this->id);
		}
		$_SESSION["points"] = $this->votePoints;
		
	}
	
}