<?php

class Npc{
	
	var $entry = 0;
	var $originalId = 0;
	var $name = "";
	var $subname = "";
	var $label = "";
	var $rank;
	var $type;
	var $levelMin;
	var $levelMax;
	var $closed = false;
	var $hp = 0;
	var $hp_hero = 0;
	var $locations = array();
	var $instances = array();
	var $locationName = "";
	var $locationLabel = "";
	var $locationType = "";
	var $description = "";
	var $difficulty_entry_1 = "";
	var $difficulty_entry_2 = "";
	var $difficulty_entry_3 = "";

	function Npc($entry){
		global $WSDB;
		
		// First Check if this is a difficulty entry of another npc
		$npcDiff = $WSDB->selectCell("
			SELECT entry 
			FROM creature_template WHERE difficulty_entry_1 = ?d OR difficulty_entry_2 = ?d OR difficulty_entry_3 = ?d;", $entry, $entry, $entry);
		
		if($npcDiff){
			$this->originalId = $entry;
			$entry = $npcDiff;
			//echo "\n<!-- ".$this->originalId." = Diff => ".$entry ."-->";
		}
		else{
			//echo "<!-- Keine Diff. ".print_r($WSDB,true)." -->";
		}
		
		$npcTemplate = $WSDB->selectRow("
			SELECT entry, subname, difficulty_entry_1, difficulty_entry_2, difficulty_entry_3, name, rank, type, minlevel, maxlevel 
			FROM creature_template WHERE entry = ?d", $entry);
		if(!$npcTemplate)
			return false;
		
		$locale = $WSDB->selectRow("SELECT `name_loc3`,`subname_loc3` FROM `locales_creature` WHERE `entry`=?d LIMIT 1", $entry);
		if($locale && !empty($locale["name_loc3"])){
			$this->name = $locale["name_loc3"];
			$this->subname = $locale["subname_loc3"];
		}
		else{
			$this->name = $npcTemplate["name"];
			$this->subname = $npcTemplate["subname"];
		}
		
		$this->entry = $npcTemplate["entry"];
		$this->name = $npcTemplate["name"];
		$this->rank = $npcTemplate["rank"];
		$this->type = $npcTemplate["type"];
		
		$this->difficulty_entry_1 = $npcTemplate["difficulty_entry_1"];
		$this->difficulty_entry_2 = $npcTemplate["difficulty_entry_2"];
		$this->difficulty_entry_3 = $npcTemplate["difficulty_entry_3"];
		
		$this->levelMin = $npcTemplate["minlevel"];	
		$this->levelMax = $npcTemplate["maxlevel"];	
		
		if($this->levelMin == 83)
			$this->rank = 3;
		
		if($this->entry != $this->originalId && $this->difficulty_entry_1 > 0){
			if($this->originalId == $this->difficulty_entry_1){
				$this->locationType = "25";
			}
			elseif($this->originalId == $this->difficulty_entry_2){
				$this->locationType = "10 (Heroisch)";
			}
			elseif($this->originalId == $this->difficulty_entry_3){
				$this->locationType = "25 (Heroisch)";
			}
		}
		elseif($this->difficulty_entry_1 > 0){
			$this->locationType = "10";
		}
		
	}

	function getName(){
		
		if(!empty($this->locationLabel) && !empty($this->label)){
			return '<a href="/game/zone/'.$this->locationLabel.'/'.$this->label.'/" data-npc="'.$this->entry.'"> <strong>'.$this->name.'</strong> </a>';
		} 
		else {	
			return '<span data-npc="'.$this->entry.'"> <strong>'.$this->name.'</strong> </span>';
		}
	}
	
	function getNpcName(){
		
		if(empty($this->subname))
			return '<strong>'.$this->name.'</strong>';
		else
			return '<strong>'.$this->name.'</strong> <em>&lt;'.$this->subname.'&gt;</em>';
		
	}
	
	function getLevel(){
		if($this->levelMin < $this->levelMax)
			return $this->levelMin."-".$this->levelMax;
		else
			return $this->levelMax;	
	}
	
	function getType(){
		global $lang_strings;
		
		if(isset($lang_strings["armory.creature.type.".$this->type]))
			return $lang_strings["armory.creature.type.".$this->type];
		return "";
	}
	
	function getRank(){
		global $lang_strings;
		
		if(isset($lang_strings["armory.creature.rank.".$this->rank]))
			return $lang_strings["armory.creature.rank.".$this->rank];
		return "";
	}
	
	function getFansiteLink(){
		return '<a href="javascript:;" data-fansite="npc|'.$this->entry.'|'.$this->name.'" class="fansite-link float-right"> </a>';			
	}
	
	function getInstanceData(){
		global $aDB;
		
		$data = $aDB->selectRow("
			SELECT instance_id, at.map, ad.label as `npc_label`, at.label as `instance_label`, at.name_de_de 
			FROM armory_instance_data ad JOIN armory_instance_template at ON( ad.instance_id = at.id ) 
			WHERE ad.id = ?d", $this->entry);
		
		if($data){
			$this->label = $data["npc_label"];
			
			$data["name_de_de"] = str_replace("(10)","",$data["name_de_de"]);
			$data["name_de_de"] = str_replace("(25)","",$data["name_de_de"]);
			$this->label = $data["npc_label"];
			
			$this->locationLabel = $data["instance_label"];
		
			$instance = array(
				"name" => $data["name_de_de"],
				"id" => $data["instance_id"],
				"label" => $data["instance_label"],
			);
			
			$this->locations[$data["map"]] = $instance;
		}
	}
	
	function GetLocations(){
		global $WSDB, $aDB;
		
		$map_names = array();
		$raw_names = array();
		$maps = $WSDB->select("SELECT map, spawnMask FROM creature WHERE id = ?d GROUP by map;", $this->entry);
		
		// No spawns in database?
		if(!$maps && count($this->locations) > 0){
			foreach($this->locations as $map_id => $map){
				$raw_names[] = $map["name"];
				$map_names[] = 	'<a href="/game/zone/'.$this->locations[$map_id]["label"].'/" data-zone="'.$this->locations[$map_id]["id"].'"> '.$map["name"].' </a> '.$this->locationType;
			}
			
		}
		elseif(!$maps){
			debug("No Map Data");
			return "";
		}
		
		foreach($maps as $map){
			$map_id = $map["map"];
			//debug("Spawn Map", $map_id);
			if($map_name = $aDB->selectCell("SELECT name_de_de FROM armory_maps WHERE id = ?d", $map_id)){
				// Is already registered => dungeon or raid
				if(isset($this->locations[$map_id])){
					$type = "";
					
					$this->locations[$map_id]["type"] = $type;
					$map_names[] = 	'<a href="/game/zone/'.$this->locations[$map_id]["label"].'/" data-zone="'.$this->locations[$map_id]["id"].'"> '.$map_name.' </a> '.$this->locationType;
				}
				else{
					$this->locations[$map_id] = array("name" => $map_name, "id" => $map_id);
					$map_names[] = 	$map_name;
				}
				$raw_names[] = 	$map_name;
				
			}
		}
		
		
		
		$this->locationName = implode(",",$raw_names);
		
		
		
		return implode(", ",$map_names);
	}
	
}