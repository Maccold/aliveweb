<?php


$data_npcs = array(
	
	// ICC
	36948 => array(
		"name" => '<span class="float-right color-q0"> Allianz<span class="icon-faction-0"></span> </span>Muradin Bronzebart',
		"closed" => true,
		"desc" => "Die Luftschiffschlacht ist ein Gefecht in der Luft zwischen den Luftschiffen Himmelsbrecher und Ogrims Hammer rund um die Spitze der Eiskronenzitadelle.",
		"side_icons" => true,
	),
	
	// PDK	
	34797 => array(
		"name" => "Eisheuler",
		"hp" => "1,3Mio–3,5Mio",
		"hp_hero" => "6,7Mio–18,1Mio",
		"location" => "Prüfung des Kreuzfahrers, Eiskrone",
	),
	34796 => array(
		"name" => "Gormok der Pfähler",
		"hp" => "4,2Mio",
		"hp_hero" => "26,5Mio",
		"location" => "Prüfung des Kreuzfahrers, Eiskrone",
	),
	34780 => array(
		"name" => "Lord Jaraxxus",
		"hp" => "4,2Mio",
		"hp_hero" => "26,5Mio",
		"location" => "Prüfung des Kreuzfahrers, Eiskrone",
	),

	34461 => array(
		"name" => '<span class="float-right color-q0"> Allianz<span class="icon-faction-0"></span> </span> Tyrius Dämmerklinge',
		"hp" => "403,2K",
		"hp_hero" => "3,2Mio",
		"type" => "Humanoid, Elementar",
		"location" => "Prüfung des Kreuzfahrers, Eiskrone",
	),
		
	34496 => array(
		"name" => "Eydis Nachtbann",
		"hp" => "6,1Mio",
		"hp_hero" => "39Mio",
		"type" => "Untoter",
		"location" => "Prüfung des Kreuzfahrers, Eiskrone",
	),
		
	34564 => array(
		"name" => "Anub&#39;arak",
		"closed" => true,
		"hp" => "4,2Mio",
		"hp_hero" => "27,2Mio",
		"type" => "Untoter",
		"location" => "Prüfung des Kreuzfahrers, Eiskrone",
	),
	
	// Ulduar
	//"Die Vorkammer von Ulduar" => array(32927,32930,33515),
	32927 => array(
		"closed" => true,
	),
	32930 => array(
		"closed" => true,
	),
	33515 => array(
		"closed" => true,
	),
	//"Die Hüter von Ulduar" => array(32845,32865,32906,33350),
	32845 => array(
		"closed" => true,
	),
	32865 => array(
		"closed" => true,
	),
	32906 => array(
		"closed" => true,
	),
	33350 => array(
		"closed" => true,
	),
	//"Der Abstieg in den Wahnsinn" => array(33271,33288),
	33271 => array(
		"closed" => true,
	),
	33288 => array(
		"closed" => true,
	),
	//"Das Himmlische Planetarium" => array(32871),
	32871 => array(
		"closed" => true,
	),
		
	
);
