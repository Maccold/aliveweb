{
  "talentData" : {
    "characterClass" : {
      "classId" : 6,
      "name" : "Todesritter",
      "powerType" : "RUNIC_POWER",
      "powerTypeId" : 6,
      "powerTypeSlug" : "runic-power"
    },
    "talentTrees" : [ {
      "name" : "Blut",
      "icon" : "spell_deathknight_bloodpresence",
      "backgroundFile" : "DeathKnightBlood",
      "overlayColor" : "#ff0000",
      "description" : "Ein dunkler Wächter, der Lebensenergien manipuliert und korrumpiert, um sich selbst in der Schlacht zu stärken.",
      "treeNo" : 0,
      "roles" : {
        "tank" : true,
        "healer" : false,
        "dps" : false
      },
      "primarySpells" : [ {
        "spellId" : 55050,
        "name" : "Herzstoß",
        "icon" : "inv_weapon_shortblade_40",
        "cost" : "1 Blut",
        "range" : "Nahkampfreichweite",
        "castTime" : "Sofort",
        "requires" : "Benötigt Nahkampfwaffe",
        "description" : "Schlägt sofort das Ziel und bis zu zwei weitere nahe Gegner. Dem Primärziel werden 175% Waffenschaden plus 1433 zugefügt, jedes weitere getroffene Ziel erleidet 25% weniger Schaden als das vorhergehende Ziel. Für jede Eurer wirkenden Krankheiten wird der Schaden um 15% erhöht.",
        "id" : 55050,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 50029,
        "name" : "Veteran des Dritten Krieges",
        "icon" : "spell_misc_warsongfocus",
        "description" : "Erhöht Eure gesamte Ausdauer um 9%, Eure Waffenkunde um 6 und verringert die Abklingzeit von 'Ausbruch' um 30 Sek.",
        "id" : 50029,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 50034,
        "name" : "Blutriten",
        "icon" : "spell_deathknight_bloodtap",
        "description" : "Wenn Ihr mit Euren Fähigkeiten 'Todesstoß' oder 'Auslöschen' einen Treffer erzielt, wandeln sich Eure Frostrunen und unheiligen Runen bei ihrer Aktivierung zu Todesrunen. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune.",
        "id" : 50034,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 93099,
        "name" : "Rache",
        "icon" : "ability_paladin_shieldofvengeance",
        "description" : "Jedes Mal, wenn Ihr Schaden erleidet, gewinnt Ihr Angriffskraft in Höhe von 5% des erlittenen Schadens, bis zu einem Maximum von 10% Eurer Gesundheit.",
        "id" : 93099,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77513,
        "name" : "Blutschild",
        "icon" : "spell_holy_championsbond",
        "description" : "Jedes Mal, wenn Ihr Euch in der Blutpräsenz durch die Nutzung von 'Todesstoß' heilt, gewinnt Ihr 50% des geheilten Wertes als Schaden absorbierenden Schild. Jeder Punkt Meisterschaft verstärkt den Schild um zusätzlich 6.25%.",
        "id" : 77513,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 1939,
        "name" : "Schlächter",
        "icon" : "inv_axe_68",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Wenn Ihr einen Feind tötet, der Erfahrung oder Ehre gewährt, erzeugt Ihr bis zu 10 Runenmacht. Zusätzlich wird alle 5 Sek. 1 Runenmacht erzeugt, wenn Ihr Euch im Kampf befindet."
        }, {
          "description" : "Wenn Ihr einen Feind tötet, der Erfahrung oder Ehre gewährt, erzeugt Ihr bis zu 20 Runenmacht. Zusätzlich wird alle 5 Sek. 2 Runenmacht erzeugt, wenn Ihr Euch im Kampf befindet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2017,
        "name" : "Klingenbarriere",
        "icon" : "ability_upgrademoonglaive",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Ihr erleidet aus allen Quellen 2% weniger Schaden."
        }, {
          "description" : "Ihr erleidet aus allen Quellen 4% weniger Schaden."
        }, {
          "description" : "Ihr erleidet aus allen Quellen 6% weniger Schaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1938,
        "name" : "Klingenbewehrte Rüstung",
        "icon" : "inv_shoulder_36",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Pro 180 Rüstungswert wird Eure Angriffskraft um 2 erhöht."
        }, {
          "description" : "Pro 180 Rüstungswert wird Eure Angriffskraft um 4 erhöht."
        }, {
          "description" : "Pro 180 Rüstungswert wird Eure Angriffskraft um 6 erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 12223,
        "name" : "Verbesserte Blutwandlung",
        "icon" : "spell_deathknight_bloodtap",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Blutwandlung' um 15 Sek."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Blutwandlung' um 30 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1948,
        "name" : "Blutgeruch",
        "icon" : "ability_rogue_bloodyeye",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Ihr habt eine Chance von 15%, den Effekt 'Blutgeruch' zu erhalten, wenn Ihr direkten Schaden erlitten habt, ausgewichen seid oder pariert habt. Durch diesen Effekt erzeugt Euer nächster Nahkampftreffer 10 Runenmacht."
        }, {
          "description" : "Ihr habt eine Chance von 15%, den Effekt 'Blutgeruch' zu erhalten, wenn Ihr direkten Schaden erlitten habt, ausgewichen seid oder pariert habt. Durch diesen Effekt erzeugen Eure nächsten 2 Nahkampftreffer 10 Runenmacht."
        }, {
          "description" : "Ihr habt eine Chance von 15%, den Effekt 'Blutgeruch' zu erhalten, wenn Ihr direkten Schaden erlitten habt, ausgewichen seid oder pariert habt. Durch diesen Effekt erzeugen Eure nächsten 3 Nahkampftreffer 10 Runenmacht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7462,
        "name" : "Blutungsfieber",
        "icon" : "ability_rogue_vendetta",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Eure Blutseuche belegt Gegner mit einem Blutungsfieber, das ihren verursachten körperlichen Schaden 21 Sek. lang um 5% verringert."
        }, {
          "description" : "Eure Blutseuche belegt Gegner mit einem Blutungsfieber, das ihren verursachten körperlichen Schaden 21 Sek. lang um 10% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11270,
        "name" : "Hand der Verdammnis",
        "icon" : "ability_deathknight_desolation",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Strangulieren' um 30 Sek."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Strangulieren' um 60 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7460,
        "name" : "Blutverkrustete Klinge",
        "icon" : "ability_criticalstrike",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Eure automatischen Angriffe haben eine Chance von 10%, einen blutverkrusteten Stoß zu verursachen, der 25% Waffenschaden plus für jede Eurer auf das Ziel wirkenden Krankheiten 12.5% Schaden zufügt."
        }, {
          "description" : "Eure automatischen Angriffe haben eine Chance von 20%, einen blutverkrusteten Stoß zu verursachen, der 25% Waffenschaden plus für jede Eurer auf das Ziel wirkenden Krankheiten 12.5% Schaden zufügt."
        }, {
          "description" : "Eure automatischen Angriffe haben eine Chance von 30%, einen blutverkrusteten Stoß zu verursachen, der 25% Waffenschaden plus für jede Eurer auf das Ziel wirkenden Krankheiten 12.5% Schaden zufügt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7459,
        "name" : "Knochenschild",
        "icon" : "ability_deathknight_boneshield",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "cost" : "1 Unheilig",
          "castTime" : "Spontanzauber",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Umgibt Euch mit einer Barriere aus wirbelnden Knochen. Der Schild hat anfänglich 6 Aufladungen, jeder Schaden verursachende Angriff verbraucht jedoch eine Aufladung. So lang zumindest 1 Aufladung verbleibt, wird jeglicher von Euch erlittene Schaden um 20% verringert und Ihr verursacht mit allen Angriffen, Zaubern und Fähigkeiten 2% mehr Schaden. Hält 5 Min. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7458,
        "name" : "Zähigkeit",
        "icon" : "spell_holy_devotion",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 3%."
        }, {
          "description" : "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 7%."
        }, {
          "description" : "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2105,
        "name" : "Stärke der Monstrosität",
        "icon" : "ability_warrior_intensifyrage",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht im Umkreis von 100 Metern die Nahkampfangriffskraft aller Gruppen- und Schlachtzugsmitglieder um 10% sowie ihre Distanzangriffskraft um 5%. Erhöht zudem Eure gesamte Stärke um 1%."
        }, {
          "description" : "Erhöht im Umkreis von 100 Metern die Nahkampfangriffskraft aller Gruppen- und Schlachtzugsmitglieder um 20% sowie ihre Distanzangriffskraft um 10%. Erhöht zudem Eure gesamte Stärke um 2%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7461,
        "name" : "Seelenstärke des Blutes",
        "icon" : "ability_deathknight_sanguinfortitude",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Eure Fähigkeit 'Eisige Gegenwehr' verringert Euren erlittenen Schaden um zusätzlich 15%. Zudem werden die Aktivierungskosten um 50% Runenmacht verringert."
        }, {
          "description" : "Eure Fähigkeit 'Eisige Gegenwehr' verringert Euren erlittenen Schaden um zusätzlich 30%. Zudem kostet die Aktivierung keine Runenmacht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1960,
        "name" : "Blutparasit",
        "icon" : "spell_shadow_soulleech",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Eure Nahkampfangriffe gewähren eine Chance von 5%, dass ein Blutwurm entsteht. Der Blutwurm greift Eure Feinde an und frisst sich mit Blut voll, bis er platzt und so nahe Verbündete heilt. Hält bis zu 20 Sek. lang an."
        }, {
          "description" : "Eure Nahkampfangriffe gewähren eine Chance von 10%, dass ein Blutwurm entsteht. Der Blutwurm greift Eure Feinde an und frisst sich mit Blut voll, bis er platzt und so nahe Verbündete heilt. Hält bis zu 20 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1936,
        "name" : "Verbesserte Blutpräsenz",
        "icon" : "spell_deathknight_bloodpresence",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht Eure Runenregeneration um 10% und verringert in der Blutpräsenz Eure Chance, kritische Nahkampftreffer zu erleiden, um 3%. Zudem behaltet Ihr in der Frost- oder unheiligen Präsenz 2% der Schadensverringerung der Blutpräsenz bei."
        }, {
          "description" : "Erhöht Eure Runenregeneration um 20% und verringert in der Blutpräsenz Eure Chance, kritische Nahkampftreffer zu erleiden, um 6%. Zudem behaltet Ihr in der Frost- oder unheiligen Präsenz 4% der Schadensverringerung der Blutpräsenz bei."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1959,
        "name" : "Wille der Nekropole",
        "icon" : "ability_creature_cursed_02",
        "x" : 0,
        "y" : 4,
        "req" : 1941,
        "ranks" : [ {
          "description" : "Wenn ein Schaden verursachender Angriff Eure Gesundheit unter 30% Eurer maximalen Gesundheit senkt, wird die Abklingzeit Eurer Fähigkeit 'Runenheilung' abgeschlossen und Euer nächstes Wirken von 'Runenheilung' ist kostenlos. Zudem wird Euer gesamter erlittener Schaden 8 Sek. lang um 8% verringert. Dieser Effekt kann nur ein Mal alle 45 Sek. auftreten."
        }, {
          "description" : "Wenn ein Schaden verursachender Angriff Eure Gesundheit unter 30% Eurer maximalen Gesundheit senkt, wird die Abklingzeit Eurer Fähigkeit 'Runenheilung' abgeschlossen und Euer nächstes Wirken von 'Runenheilung' ist kostenlos. Zudem wird Euer gesamter erlittener Schaden 8 Sek. lang um 16% verringert. Dieser Effekt kann nur ein Mal alle 45 Sek. auftreten."
        }, {
          "description" : "Wenn ein Schaden verursachender Angriff Eure Gesundheit unter 30% Eurer maximalen Gesundheit senkt, wird die Abklingzeit Eurer Fähigkeit 'Runenheilung' abgeschlossen und Euer nächstes Wirken von 'Runenheilung' ist kostenlos. Zudem wird Euer gesamter erlittener Schaden 8 Sek. lang um 25% verringert. Dieser Effekt kann nur ein Mal alle 45 Sek. auftreten."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1941,
        "name" : "Runenheilung",
        "icon" : "spell_deathknight_runetap",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "cost" : "1 Blut",
          "castTime" : "Spontanzauber",
          "cooldown" : "30 Sek. Abklingzeit",
          "description" : "Wandelt 1 Blutrune in 10% Eurer maximalen Gesundheit um."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2019,
        "name" : "Vampirblut",
        "icon" : "spell_shadow_lifedrain",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Gewährt dem Todesritter zeitweise 15% seiner maximalen Gesundheit und erhöht die durch Zauber und Effekte erzeugte Gesundheit 10 Sek. lang um 25%. Wenn der Effekt endet, geht die zusätzliche Gesundheit verloren."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2259,
        "name" : "Verbesserter Todesstoß",
        "icon" : "spell_deathknight_butcher2",
        "x" : 1,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht den verursachten Schaden Eurer Fähigkeit 'Todesstoß' um 40%, ihre kritische Trefferchance um 10% und ihre gewirkte Heilung um 15%."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Fähigkeit 'Todesstoß' um 80%, ihre kritische Trefferchance um 20% und ihre gewirkte Heilung um 30%."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Fähigkeit 'Todesstoß' um 120%, ihre kritische Trefferchance um 30% und ihre gewirkte Heilung um 45%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7463,
        "name" : "Scharlachrote Geißel",
        "icon" : "spell_deathknight_bloodboil",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht den verursachten Schaden Eurer Fähigkeit 'Siedendes Blut' um 20%. Wenn Ihr ein Ziel, das bereits mit Eurer Blutseuche infiziert ist, mit einem Nahkampfangriff trefft, besteht eine Chance von 5%, dass Eure nächste Anwendung von 'Siedendes Blut' keine Runen verbraucht."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Fähigkeit 'Siedendes Blut' um 40%. Wenn Ihr ein Ziel, das bereits mit Eurer Blutseuche infiziert ist, mit einem Nahkampfangriff trefft, besteht eine Chance von 10%, dass Eure nächste Anwendung von 'Siedendes Blut' keine Runen verbraucht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1961,
        "name" : "Tanzende Runenwaffe",
        "icon" : "inv_sword_07",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "60 Runenmacht",
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "1,5 Min. Abklingzeit",
          "requires" : "Benötigt Nahkampfwaffe",
          "description" : "Beschwört eine zweite Runenwaffe, die 12 Sek. lang eigenständig kämpft und die Angriffe des Todesritters imitiert. Die Runenwaffe unterstützt, während sie aktiv ist, auch die Verteidigung ihres Meisters, indem sie zusätzlich 20% Parierchance gewährt."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 0
    }, {
      "name" : "Frost",
      "icon" : "spell_deathknight_frostpresence",
      "backgroundFile" : "DeathKnightFrost",
      "overlayColor" : "#4c7fff",
      "description" : "Ein eisiger Verdammnisbote, der Runenmacht kanalisiert und schnelle Angriffe mit Waffen beherrscht.",
      "treeNo" : 1,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 49143,
        "name" : "Froststoß",
        "icon" : "spell_deathknight_empowerruneblade2",
        "cost" : "40 Runenmacht",
        "range" : "Nahkampfreichweite",
        "castTime" : "Sofort",
        "requires" : "Benötigt Nahkampfwaffe",
        "description" : "Schlägt den Feind sofort, verursacht 130% Waffenschaden plus 361 als Frostschaden.",
        "id" : 49143,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 50887,
        "name" : "Eisige Klauen",
        "icon" : "spell_deathknight_icytalons",
        "description" : "Euer Nahkampfangriffstempo ist um 20% erhöht.",
        "id" : 50887,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 54637,
        "name" : "Blut des Nordens",
        "icon" : "inv_weapon_shortblade_79",
        "description" : "Wandelt Eure Blutrunen permanent in Todesrunen um. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune.",
        "id" : 54637,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77514,
        "name" : "Gefrorenes Herz",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht jeglichen verursachten Frostschaden um 16%. Jeder Punkt Meisterschaft erhöht den Frostschaden um zusätzlich 2.0%.",
        "id" : 77514,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 2031,
        "name" : "Meisterung der Runenmacht",
        "icon" : "spell_arcane_arcane01",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht Eure maximale Runenmacht um 10."
        }, {
          "description" : "Erhöht Eure maximale Runenmacht um 20."
        }, {
          "description" : "Erhöht Eure maximale Runenmacht um 30."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2035,
        "name" : "Eisige Reichweite",
        "icon" : "spell_frost_manarecharge",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Reichweite Eurer Zauber 'Eisige Berührung', 'Eisketten' und 'Heulende Böe' um 5 Meter."
        }, {
          "description" : "Erhöht die Reichweite Eurer Zauber 'Eisige Berührung', 'Eisketten' und 'Heulende Böe' um 10 Meter."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2022,
        "name" : "Nerven aus kaltem Stahl",
        "icon" : "ability_dualwield",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht Eure Trefferchance mit Einhandwaffen um 1% und erhöht den von Eurer Schildhandwaffe verursachten Schaden um 8%."
        }, {
          "description" : "Erhöht Eure Trefferchance mit Einhandwaffen um 2% und erhöht den von Eurer Schildhandwaffe verursachten Schaden um 16%."
        }, {
          "description" : "Erhöht Eure Trefferchance mit Einhandwaffen um 3% und erhöht den von Eurer Schildhandwaffe verursachten Schaden um 25%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2048,
        "name" : "Vernichtung",
        "icon" : "inv_weapon_hand_18",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den von Eurer Fähigkeit 'Auslöschen' verursachten Schaden um 15%."
        }, {
          "description" : "Erhöht den von Eurer Fähigkeit 'Auslöschen' verursachten Schaden um 30%."
        }, {
          "description" : "Erhöht den von Eurer Fähigkeit 'Auslöschen' verursachten Schaden um 45%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2215,
        "name" : "Lichritter",
        "icon" : "spell_shadow_raisedead",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Erfüllt Euch mit unheiligen Energien, um 10 Sek. lang untot zu werden. Im untoten Zustand seid Ihr immun gegenüber Bezauberungs-, Furcht- und Schlafeffekten."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11275,
        "name" : "Der Tod reit'",
        "icon" : "ability_mount_undeadhorse",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Ihr seid so schwer aufzuhalten, wie der Tod selbst. Die Dauer aller auf Euch wirkenden Verlangsamungseffekte ist um 15% verringert und das Tempo Eures Reittiers um 10% erhöht. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar."
        }, {
          "description" : "Ihr seid so schwer aufzuhalten, wie der Tod selbst. Die Dauer aller auf Euch wirkenden Verlangsamungseffekte ist um 30% verringert und das Tempo Eures Reittiers um 20% erhöht. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1971,
        "name" : "Endloser Winter",
        "icon" : "spell_shadow_twilight",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Die Kosten Eurer Fähigkeit 'Gedankenfrost' sind auf 10 Runenmacht verringert."
        }, {
          "description" : "Eure Fähigkeit 'Gedankenfrost' kostet keine Runenmacht mehr."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1993,
        "name" : "Gnadenloser Kampf",
        "icon" : "inv_sword_112",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Eure Fähigkeiten 'Eisige Berührung', 'Heulende Böe', 'Auslöschen' und 'Froststoß' verursachen 6% zusätzlichen Schaden, wenn das Ziel über weniger als 35% Gesundheit verfügt."
        }, {
          "description" : "Eure Fähigkeiten 'Eisige Berührung', 'Heulende Böe', 'Auslöschen' und 'Froststoß' verursachen 12% zusätzlichen Schaden, wenn das Ziel über weniger als 35% Gesundheit verfügt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1981,
        "name" : "Grabeskühle",
        "icon" : "spell_frost_frostshock",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Eure Zauber 'Eisige Berührung', 'Eisketten', 'Heulende Böe' und 'Auslöschen' erzeugen 5 zusätzliche Runenmacht."
        }, {
          "description" : "Eure Zauber 'Eisige Berührung', 'Eisketten', 'Heulende Böe' und 'Auslöschen' erzeugen 10 zusätzliche Runenmacht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2044,
        "name" : "Tötungsmaschine",
        "icon" : "inv_sword_122",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Eure automatischen Angriffe gewähren eine Chance, dass die kritische Trefferchance Eures nächstes Wirkens von 'Auslöschen' oder 'Froststoß' um 100% erhöht wird."
        }, {
          "description" : "Eure automatischen Angriffe gewähren eine Chance, dass die kritische Trefferchance Eures nächstes Wirkens von 'Auslöschen' oder 'Froststoß' um 100% erhöht wird. Dieser Effekt wird häufiger ausgelöst als 'Tötungsmaschine' (Rang 1)."
        }, {
          "description" : "Eure automatischen Angriffe gewähren eine Chance, dass die kritische Trefferchance Eures nächstes Wirkens von 'Auslöschen' oder 'Froststoß' um 100% erhöht wird. Dieser Effekt wird häufiger ausgelöst als 'Tötungsmaschine' (Rang 2)."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1992,
        "name" : "Raureif",
        "icon" : "spell_frost_freezingbreath",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Beim Wirken von 'Auslöschen' besteht eine Chance von 15%, dass Euer nächstes Wirken von 'Heulende Böe' oder 'Eisige Berührung' keine Runen verbraucht."
        }, {
          "description" : "Beim Wirken von 'Auslöschen' besteht eine Chance von 30%, dass Euer nächstes Wirken von 'Heulende Böe' oder 'Eisige Berührung' keine Runen verbraucht."
        }, {
          "description" : "Beim Wirken von 'Auslöschen' besteht eine Chance von 45%, dass Euer nächstes Wirken von 'Heulende Böe' oder 'Eisige Berührung' keine Runen verbraucht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1979,
        "name" : "Säule des Frosts",
        "icon" : "ability_deathknight_pillaroffrost",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "cost" : "1 Frost",
          "castTime" : "Sofort",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Ruft die Macht des Frosts herbei, um die Stärke des Todesritters um 20% zu erhöhen. Dicke Eiszapfen hängen schwer von der Rüstung des Todesritters herab und gewähren ihm Immunität gegen fremde Bewegungsimpulse, wie zum Beispiel Rückstöße. Hält 20 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2223,
        "name" : "Verbesserte eisige Klauen",
        "icon" : "spell_deathknight_icytalons",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht das Nahkampf- und Distanzangriffstempo aller Gruppen- oder Schlachtzugsmitglieder im Umkreis von 100 Metern um 10%, Euer Angriffstempo wird zusätzlich um 5% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1980,
        "name" : "Brüchige Knochen",
        "icon" : "ability_deathknight_brittlebones",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Eure Stärke wird um 2% erhöht. Zudem friert Euer Frostfieber Eure Opfer bis auf die Knochen durch, sodass ihr erlittener körperlicher Schaden um 2% erhöht wird."
        }, {
          "description" : "Eure Stärke wird um 4% erhöht. Zudem friert Euer Frostfieber Eure Opfer bis auf die Knochen durch, sodass ihr erlittener körperlicher Schaden um 4% erhöht wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2260,
        "name" : "Frostbeulen",
        "icon" : "spell_frost_wisp",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Opfer Eures Frostfiebers werden unterkühlt, wodurch ihr Bewegungstempo 10 Sek. lang um 25% verringert wird. Zudem macht Eure Fähigkeit 'Eisketten' Ihr Ziel 1,50 Sek. lang bewegungsunfähig."
        }, {
          "description" : "Opfer Eures Frostfiebers werden unterkühlt, wodurch ihr Bewegungstempo 10 Sek. lang um 50% verringert wird. Zudem macht Eure Fähigkeit 'Eisketten' Ihr Ziel 3 Sek. lang bewegungsunfähig."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1999,
        "name" : "Zehrende Kälte",
        "icon" : "inv_staff_15",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "cost" : "40 Runenmacht",
          "castTime" : "1,5 Sek. Zauber",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Entzieht der Erde unter dem Todesritter jegliche Wärme. Feinde innerhalb von 10 Metern werden in Eis eingeschlossen, das 10 Sek. lang jegliche ihrer Aktionen verhindert und sie mit Frostfieber ansteckt. Feinde zählen als eingefroren, jeglicher nicht von Krankheiten verursachter Schaden unterbricht den Effekt."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2029,
        "name" : "Verbesserte Frostpräsenz",
        "icon" : "spell_deathknight_frostpresence",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Gewährt Euch in der Frostpräsenz zusätzlich 2% Bonusschaden. Zudem behaltet Ihr in der Blut- oder unheiligen Präsenz 2% der erhöhten Runenmachterzeugung der Frostpräsenz bei."
        }, {
          "description" : "Gewährt Euch in der Frostpräsenz zusätzlich 5% Bonusschaden. Zudem behaltet Ihr in der Blut- oder unheiligen Präsenz 4% der erhöhten Runenmachterzeugung der Frostpräsenz bei."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2284,
        "name" : "Thassarians Drohung",
        "icon" : "ability_dualwieldspecialization",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Wenn Ihr zwei Einhandwaffen nutzt, haben Eure Fähigkeiten 'Todesstoß', 'Auslöschen', 'Seuchenstoß', 'Runenstoß', 'Blutstoß' und 'Froststoß' eine Chance von 30%, auch mit der Schildhandwaffe Schaden zu verursachen."
        }, {
          "description" : "Wenn Ihr zwei Einhandwaffen nutzt, haben Eure Fähigkeiten 'Todesstoß', 'Auslöschen', 'Seuchenstoß', 'Runenstoß', 'Blutstoß' und 'Froststoß' eine Chance von 60%, auch mit der Schildhandwaffe Schaden zu verursachen."
        }, {
          "description" : "Wenn Ihr zwei Einhandwaffen nutzt, haben Eure Fähigkeiten 'Todesstoß', 'Auslöschen', 'Seuchenstoß', 'Runenstoß', 'Blutstoß' und 'Froststoß' eine Chance von 100%, auch mit der Schildhandwaffe Schaden zu verursachen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7571,
        "name" : "Macht der gefrorenen Ödnis",
        "icon" : "inv_sword_120",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Bei Nutzung einer Zweihandwaffe verursachen Eure Nahkampfangriffe zusätzlich 3% Schaden und Eure automatischen Angriffe haben eine Chance von 15%, 10 Runenmacht zu erzeugen."
        }, {
          "description" : "Bei Nutzung einer Zweihandwaffe verursachen Eure Nahkampfangriffe zusätzlich 7% Schaden und Eure automatischen Angriffe haben eine Chance von 30%, 10 Runenmacht zu erzeugen."
        }, {
          "description" : "Bei Nutzung einer Zweihandwaffe verursachen Eure Nahkampfangriffe zusätzlich 10% Schaden und Eure automatischen Angriffe haben eine Chance von 45%, 10 Runenmacht zu erzeugen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1989,
        "name" : "Heulende Böe",
        "icon" : "spell_frost_arcticwinds",
        "x" : 1,
        "y" : 6,
        "req" : 1999,
        "ranks" : [ {
          "cost" : "1 Frost",
          "range" : "20 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "description" : "Weht dem Ziel eine eisige Böe entgegen, die ihm 1570 Frostschaden sowie allen anderen Gegnern im Umkreis von 10 Metern 785 Frostschaden zufügt."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 1
    }, {
      "name" : "Unheilig",
      "icon" : "spell_deathknight_unholypresence",
      "backgroundFile" : "DeathKnightUnholy",
      "overlayColor" : "#33cc33",
      "description" : "Ein Meister des Todes und des Verfalls, der Krankheiten verbreitet und untote Diener befehligt, die seinem Willen unterworfen sind.",
      "treeNo" : 2,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 55090,
        "name" : "Geißelstoß",
        "icon" : "spell_deathknight_scourgestrike",
        "cost" : "1 Unheilig",
        "range" : "Nahkampfreichweite",
        "castTime" : "Sofort",
        "requires" : "Benötigt Nahkampfwaffe",
        "description" : "Ein unheiliger Stoß, der 100% Waffenschaden als körperlichen Schaden plus 625 verursacht. Für jede Eurer auf das Ziel wirkenden Krankheiten fügt Ihr zusätzlich 18% des verursachten körperlichen Schadens als Schattenschaden zu.",
        "id" : 55090,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 52143,
        "name" : "Meister der Ghule",
        "icon" : "spell_shadow_animatedead",
        "description" : "Verringert die Abklingzeit Eures Zaubers 'Totenerweckung' um 60 Sek. und von diesem Zauber beschworene Ghule gelten als Begleiter und stehen unter Eurer Kontrolle. Anders als normale Ghule der Todesritter, hat dieser Ghul keine begrenzte Lebensdauer.",
        "id" : 52143,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 91107,
        "name" : "Unheilige Macht",
        "icon" : "spell_shadow_unholystrength",
        "description" : "Dunkle Macht schießt durch Eure Glieder und erhöht Eure Stärke um 25%.",
        "id" : 91107,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 56835,
        "name" : "Sensenmann",
        "icon" : "spell_shadow_shadetruesight",
        "description" : "Wenn Ihr mit Euren Fähigkeiten 'Blutstoß', 'Pestilenz' oder 'Schwärender Stoß' einen Treffer erzielt, wandeln sich die entsprechenden Runen bei ihrer Aktivierung zu einer Todesrune. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune.",
        "id" : 56835,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77515,
        "name" : "Schreckensklinge",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht jeglichen verursachten Schattenschaden um 20%. Jeder Punkt Meisterschaft erhöht den Schattenschaden um zusätzlich 2.5%.",
        "id" : 77515,
        "classMask" : 32,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 2025,
        "name" : "Unheiliges Kommando",
        "icon" : "spell_deathknight_strangulate",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Todesgriff' um 5 Sek. und verleiht Euch eine Chance von 50%, dass ihre Abklingzeit abgeschlossen wird, wenn Ihr einem Ziel den Todesstoß versetzt, das Erfahrungspunkte oder Ehre gewährt."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Todesgriff' um 10 Sek. und verleiht Euch eine Chance von 100%, dass ihre Abklingzeit abgeschlossen wird, wenn Ihr einem Ziel den Todesstoß versetzt, das Erfahrungspunkte oder Ehre gewährt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1932,
        "name" : "Virulenz",
        "icon" : "spell_shadow_burningspirit",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den von Euren Krankheiten verursachten Schaden um 10%."
        }, {
          "description" : "Erhöht den von Euren Krankheiten verursachten Schaden um 20%."
        }, {
          "description" : "Erhöht den von Euren Krankheiten verursachten Schaden um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1963,
        "name" : "Epidemie",
        "icon" : "spell_shadow_shadowwordpain",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Dauer Eurer Krankheiten 'Blutseuche' und 'Frostfieber' um 4 Sek."
        }, {
          "description" : "Erhöht die Dauer Eurer Krankheiten 'Blutseuche' und 'Frostfieber' um 8 Sek."
        }, {
          "description" : "Erhöht die Dauer Eurer Blutseuche und Eures Frostfiebers um 12 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2226,
        "name" : "Entweihung",
        "icon" : "spell_shadow_shadowfiend",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Eure Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Nekrotischer Stoß' entweihen den Boden in einem Umkreis von 7 Metern um Euer Ziel. Das Bewegungstempo von Gegnern wird um 25% verringert, solange sie auf dem unheiligen Boden stehen. Ist das Ziel gegen bewegungsverringernde Effekte immun, wird 'Entweihung' nicht ausgelöst. Hält 20 Sek. lang an."
        }, {
          "description" : "Eure Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Nekrotischer Stoß' entweihen den Boden in einem Umkreis von 7 Metern um Euer Ziel. Das Bewegungstempo von Gegnern wird um 50% verringert, solange sie auf dem unheiligen Boden stehen. Ist das Ziel gegen bewegungsverringernde Effekte immun, wird 'Entweihung' nicht ausgelöst. Hält 20 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7572,
        "name" : "Widerspenstige Infektion",
        "icon" : "ability_creature_disease_05",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Wenn Eure Krankheiten von einem Gegner gebannt werden, besteht eine Chance von 50%, dass entweder eine Frostrune aktiviert wird, wenn Euer Frostfieber entfernt wurde, oder eine unheilige Rune, wenn Eure Blutseuche entfernt wurde."
        }, {
          "description" : "Wenn Eure Krankheiten von einem Gegner gebannt werden, besteht eine Chance von 100%, dass entweder eine Frostrune aktiviert wird, wenn Euer Frostfieber entfernt wurde, oder eine unheilige Rune, wenn Eure Blutseuche entfernt wurde."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11178,
        "name" : "Morbidität",
        "icon" : "spell_shadow_deathanddecay",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den Schaden und den Heilungseffekt von 'Todesmantel' um 5% und von 'Tod und Verfall' um 10%."
        }, {
          "description" : "Erhöht den Schaden und den Heilungseffekt von 'Todesmantel' um 10% und von 'Tod und Verfall' um 20%."
        }, {
          "description" : "Erhöht den Schaden und den Heilungseffekt von 'Todesmantel' um 15% und von 'Tod und Verfall' um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2047,
        "name" : "Runenverderbnis",
        "icon" : "spell_shadow_rune",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Verringert die Kosten Eurer Fähigkeit 'Todesmantel' um 3. Zudem aktiviert Eure Fähigkeit 'Runenauffrischung' nicht länger eine entladene Rune, sondern erhöht stattdessen 3 Sek. lang Eure Runenregenerationsrate um 50%."
        }, {
          "description" : "Verringert die Kosten Eurer Fähigkeit 'Todesmantel' um 6. Zudem aktiviert Eure Fähigkeit 'Runenauffrischung' nicht länger eine entladene Rune, sondern erhöht stattdessen 3 Sek. lang Eure Runenregenerationsrate um 100%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7574,
        "name" : "Unheilige Raserei",
        "icon" : "spell_shadow_unholyfrenzy",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Versetzt ein befreundetes Gruppen- oder Schlachtzugsmitglied 30 Sek. lang in einen Blutrausch. Das Ziel wird wütend und sein Nahkampf- und Distanzangriffstempo um 20% erhöht. Es verliert jedoch alle 3 Sek. Gesundheit in Höhe von 2% seiner maximalen Gesundheit."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 12119,
        "name" : "Ansteckung",
        "icon" : "spell_shadow_plaguecloud",
        "x" : 2,
        "y" : 2,
        "req" : 1963,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eurer Krankheiten, die über 'Pestilenz' verbreitet werden um 50%."
        }, {
          "description" : "Erhöht den Schaden Eurer Krankheiten, die über 'Pestilenz' verbreitet werden um 100%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11179,
        "name" : "Schattenmacht",
        "icon" : "spell_shadow_requiem",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "description" : "Gewährt Euren erfolgreichen Anwendungen von 'Todesmantel' eine Chance von 33%, dass Euer aktiver Ghul mit Macht erfüllt wird, wodurch sein verursachter Schaden 30 Sek. lang um 6% erhöht wird. Bis zu 5-mal stapelbar."
        }, {
          "description" : "Gewährt Euren erfolgreichen Anwendungen von 'Todesmantel' eine Chance von 66%, dass Euer aktiver Ghul mit Macht erfüllt wird, wodurch sein verursachter Schaden 30 Sek. lang um 6% erhöht wird. Bis zu 5-mal stapelbar."
        }, {
          "description" : "Gewährt Euren erfolgreichen Anwendungen von 'Todesmantel' eine Chance von 100%, dass Euer aktiver Ghul mit Macht erfüllt wird, wodurch sein verursachter Schaden 30 Sek. lang um 6% erhöht wird. Bis zu 5-mal stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 15322,
        "name" : "Unaufhaltsamer Tod",
        "icon" : "spell_shadow_demonicempathy",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Sind beide Eurer unheiligen Runen verbraucht, können Euch bewegungseinschränkende Effekte nicht unter 60% Eures normalen Bewegungstempos verlangsamen."
        }, {
          "description" : "Sind beide Eurer unheiligen Runen verbraucht, können Euch bewegungseinschränkende Effekte nicht unter 75% Eures normalen Bewegungstempos verlangsamen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2009,
        "name" : "Magieunterdrückung",
        "icon" : "spell_shadow_antimagicshell",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht die Zauberschadensabsorption Eurer Fähigkeit 'Antimagische Hülle' zusätzlich um 8%. Zudem lädt von der Fähigkeit 'Antimagische Hülle' absorbierter Schaden die Runenmacht des Todesritters auf."
        }, {
          "description" : "Erhöht die Zauberschadensabsorption Eurer Fähigkeit 'Antimagische Hülle' zusätzlich um 16%. Zudem wird die erzeugte Runenmacht erhöht, wenn von der Fähigkeit 'Antimagische Hülle' Schaden absorbiert wird."
        }, {
          "description" : "Erhöht die Zauberschadensabsorption Eurer Fähigkeit 'Antimagische Hülle' zusätzlich um 25%. Zudem wird die erzeugte Runenmacht erhöht, wenn von der Fähigkeit 'Antimagische Hülle' Schaden absorbiert wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2082,
        "name" : "Totenschwurs Zorn",
        "icon" : "inv_weapon_halberd14",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eurer Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Schwärender Stoß' um 15%."
        }, {
          "description" : "Erhöht den Schaden Eurer Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Schwärender Stoß' um 30%."
        }, {
          "description" : "Erhöht den Schaden Eurer Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Schwärender Stoß' um 45%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1996,
        "name" : "Unheilige Verseuchung",
        "icon" : "spell_shadow_contagion",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Opfer Eurer Fähigkeit 'Todesmantel' werden von einer widerwärtigen Wolke unheiliger Insekten umschwärmt, durch die sie im Verlauf von 10 Sek. 10% des Schadens von 'Todesmantel' erleiden. Zudem wird durch die Insekten verhindert, dass auf das Ziel wirkende Krankheiten gebannt werden können."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2221,
        "name" : "Antimagisches Feld",
        "icon" : "spell_deathknight_antimagiczone",
        "x" : 1,
        "y" : 4,
        "req" : 2009,
        "ranks" : [ {
          "cost" : "1 Unheilig",
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Platziert ein großes, ortsgebundenes, antimagisches Feld, das den erlittenen Zauberschaden aller darin befindlichen Gruppen- oder Schlachtzugsmitglieder um 75% verringert. Das antimagische Feld hält 10 Sek. lang an oder bis es 11126 Zauberschaden absorbiert hat."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2013,
        "name" : "Verbesserte unheilige Präsenz",
        "icon" : "spell_deathknight_unholypresence",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Gewährt Euch in der unheiligen Präsenz zusätzlich 2% Tempo. Zudem behaltet Ihr in der Blut- oder Frostpräsenz 8% des erhöhten Bewegungstempos der unheiligen Präsenz bei."
        }, {
          "description" : "Gewährt Euch in der unheiligen Präsenz zusätzlich 5% Tempo. Zudem behaltet Ihr in der Blut- oder Frostpräsenz 15% des erhöhten Bewegungstempos der unheiligen Präsenz bei."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2085,
        "name" : "Dunkle Transformation",
        "icon" : "achievement_boss_festergutrotface",
        "x" : 3,
        "y" : 4,
        "req" : 11179,
        "ranks" : [ {
          "cost" : "1 Unheilig",
          "range" : "100 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "description" : "Zehrt 5 Aufladungen von 'Schattenmacht' auf Eurem Ghul auf und verwandelt ihn 30 Sek. lang in eine mächtige untote Monstrosität. Die Angriffe des Ghuls werden dadurch machtvoller und werden durch neue Funktionen ersetzt, so lang die Transformation anhält."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2043,
        "name" : "Schwarzer Seuchenbringer",
        "icon" : "ability_creature_cursed_03",
        "x" : 1,
        "y" : 5,
        "ranks" : [ {
          "description" : "Eure Fähigkeiten 'Seuchenstoß', 'Eisige Berührung', 'Eisketten' und 'Ausbruch' infizieren das Ziel ebenfalls mit der Schwarzen Seuche. Diese erhöht den durch Eure Krankheiten erlittenen Schaden um 15%, außerdem wird der erlittene Magieschaden des Ziels um 8% erhöht."
        }, {
          "description" : "Eure Fähigkeiten 'Seuchenstoß', 'Eisige Berührung', 'Eisketten' und 'Ausbruch' infizieren das Ziel ebenfalls mit der Schwarzen Seuche. Diese erhöht den durch Eure Krankheiten erlittenen Schaden um 30%, außerdem wird der erlittene Magieschaden des Ziels um 8% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7575,
        "name" : "Hereinbrechende Verdammnis",
        "icon" : "spell_shadow_painspike",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Eure automatischen Angriffe Eurer Haupthand haben eine Chance, dass Euer nächstes Wirken von 'Todesmantel' keine Runenmacht kostet."
        }, {
          "description" : "Eure automatischen Angriffe Eurer Haupthand haben eine Chance (höher als bei Rang 1), dass Euer nächstes Wirken von 'Todesmantel' keine Runenmacht kostet."
        }, {
          "description" : "Eure automatischen Angriffe Eurer Haupthand haben eine Chance (höher als bei Rang 2), dass Euer nächstes Wirken von 'Todesmantel' keine Runenmacht kostet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2000,
        "name" : "Gargoyle beschwören",
        "icon" : "ability_deathknight_summongargoyle",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "70 Runenmacht",
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Ein Gargoyle fliegt herbei und bombardiert das Ziel mit Naturschaden (modifiziert durch die Angriffskraft des Todesritters). Der Gargoyle bleibt 30 Sek. lang bestehen."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 2
    } ]
  },
  "glyphs" : [ {
    "name" : "Glyphe 'Antimagische Hülle'",
    "id" : 512,
    "type" : 0,
    "description" : "Erhöht die Dauer Eurer Fähigkeit 'Antimagische Hülle' um 2 Sek.",
    "icon" : "spell_shadow_antimagicshell",
    "itemId" : 43533,
    "spellKey" : 58623,
    "spellId" : 58623,
    "prettyName" : "Antimagische Hülle",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Herzstoß'",
    "id" : 513,
    "type" : 2,
    "description" : "Erhöht den Schaden Eurer Fähigkeit 'Herzstoß' um 30%.",
    "icon" : "inv_weapon_shortblade_40",
    "itemId" : 43534,
    "spellKey" : 58616,
    "spellId" : 58616,
    "prettyName" : "Herzstoß",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Blutwandlung'",
    "id" : 514,
    "type" : 1,
    "description" : "Die Nutzung Eurer Fähigkeit 'Blutwandlung' verursacht an Euch keinen Schaden mehr.",
    "icon" : "spell_deathknight_bloodtap",
    "itemId" : 43535,
    "spellKey" : 58640,
    "spellId" : 58640,
    "prettyName" : "Blutwandlung",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Knochenschild'",
    "id" : 515,
    "type" : 0,
    "description" : "Erhöht Euer Bewegungstempo, solange Eure Fähigkeit 'Knochenschild' aktiv ist, um 15%. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar.",
    "icon" : "ability_deathknight_boneshield",
    "itemId" : 43536,
    "spellKey" : 58673,
    "spellId" : 58673,
    "prettyName" : "Knochenschild",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Eisketten'",
    "id" : 516,
    "type" : 0,
    "description" : "Euer Zauber 'Eisketten' verursacht zudem 150 Frostschaden plus zusätzlichen Schaden, der von Eurer Angriffskraft abhängig ist.",
    "icon" : "spell_frost_chainsofice",
    "itemId" : 43537,
    "spellKey" : 58620,
    "spellId" : 58620,
    "prettyName" : "Eisketten",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Umarmung des Todes'",
    "id" : 518,
    "type" : 1,
    "description" : "Wenn Ihr Euren Zauber 'Todesmantel' nutzt, um einen verbündeten Diener zu heilen, gewinnt Ihr 20 Runenmacht.",
    "icon" : "spell_shadow_deathcoil",
    "itemId" : 43539,
    "spellKey" : 58677,
    "spellId" : 58677,
    "prettyName" : "Umarmung des Todes",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Todesgriff'",
    "id" : 519,
    "type" : 0,
    "description" : "Erhöht die Reichweite Eurer Fähigkeit 'Todesgriff' um 5 Meter.",
    "icon" : "spell_deathknight_strangulate",
    "itemId" : 43541,
    "spellKey" : 62259,
    "spellId" : 62259,
    "prettyName" : "Todesgriff",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Tod und Verfall'",
    "id" : 520,
    "type" : 2,
    "description" : "Erhöht die Dauer Eurer Fähigkeit 'Tod und Verfall' um 50%.",
    "icon" : "spell_shadow_deathanddecay",
    "itemId" : 43542,
    "spellKey" : 58629,
    "spellId" : 58629,
    "prettyName" : "Tod und Verfall",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Froststoß'",
    "id" : 521,
    "type" : 2,
    "description" : "Verringert die Runenmacht-Kosten Eures Zaubers 'Froststoß' um 8.",
    "icon" : "spell_deathknight_empowerruneblade2",
    "itemId" : 43543,
    "spellKey" : 58647,
    "spellId" : 58647,
    "prettyName" : "Froststoß",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Horn des Winters'",
    "id" : 522,
    "type" : 1,
    "description" : "Erhöht die Dauer der Fähigkeit 'Horn des Winters' um 1 Min.",
    "icon" : "inv_misc_horn_02",
    "itemId" : 43544,
    "spellKey" : 58680,
    "spellId" : 58680,
    "prettyName" : "Horn des Winters",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Eisige Berührung'",
    "id" : 524,
    "type" : 2,
    "description" : "Euer Frostfieber verursacht 20% zusätzlichen Schaden.",
    "icon" : "spell_deathknight_icetouch",
    "itemId" : 43546,
    "spellKey" : 58631,
    "spellId" : 58631,
    "prettyName" : "Eisige Berührung",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Auslöschen'",
    "id" : 525,
    "type" : 2,
    "description" : "Erhöht den Schaden Eurer Fähigkeit 'Auslöschen' um 20%.",
    "icon" : "spell_deathknight_classicon",
    "itemId" : 43547,
    "spellKey" : 58671,
    "spellId" : 58671,
    "prettyName" : "Auslöschen",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Pestilenz'",
    "id" : 526,
    "type" : 0,
    "description" : "Erhöht den Radius des Effekts Eures Zaubers 'Pestilenz' um 5 Meter.",
    "icon" : "spell_shadow_plaguecloud",
    "itemId" : 43548,
    "spellKey" : 58657,
    "spellId" : 58657,
    "prettyName" : "Pestilenz",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Totenerweckung'",
    "id" : 527,
    "type" : 2,
    "description" : "Euer Ghul erhält zusätzlich 40% Eurer Stärke und 40% Eurer Ausdauer.",
    "icon" : "spell_shadow_animatedead",
    "itemId" : 43549,
    "spellKey" : 58686,
    "spellId" : 58686,
    "prettyName" : "Totenerweckung",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Runenstoß'",
    "id" : 528,
    "type" : 2,
    "description" : "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Runenstoß' um 10%.",
    "icon" : "spell_deathknight_darkconviction",
    "itemId" : 43550,
    "spellKey" : 58669,
    "spellId" : 58669,
    "prettyName" : "Runenstoß",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Geißelstoß'",
    "id" : 529,
    "type" : 2,
    "description" : "Erhöht den Schattenschadenanteil Eurer Fähigkeit 'Geißelstoß' um 30%.",
    "icon" : "spell_deathknight_scourgestrike",
    "itemId" : 43551,
    "spellKey" : 58642,
    "spellId" : 58642,
    "prettyName" : "Geißelstoß",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Strangulieren'",
    "id" : 530,
    "type" : 0,
    "description" : "Erhöht die Dauer des Stilleeffekts Eurer Fähigkeit 'Strangulieren' um 2 Sek., wenn sie gegen ein Ziel eingesetzt wird, das gerade einen Zauber wirkt.",
    "icon" : "spell_shadow_soulleech_3",
    "itemId" : 43552,
    "spellKey" : 58618,
    "spellId" : 58618,
    "prettyName" : "Strangulieren",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Säule des Frosts'",
    "id" : 531,
    "type" : 0,
    "description" : "Erfüllt Eure 'Säule des Frosts' mit Macht und macht Euch immun gegen alle Effekte, die zum Verlust der Kontrolle über Euren Charakter führen. Ihr friert jedoch auch auf der Stelle fest, solange diese Fähigkeit aktiv ist.",
    "icon" : "ability_deathknight_pillaroffrost",
    "itemId" : 43553,
    "spellKey" : 58635,
    "spellId" : 58635,
    "prettyName" : "Säule des Frosts",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Vampirblut'",
    "id" : 532,
    "type" : 0,
    "description" : "Erhöht die erhaltene Bonusheilung, während Eure Fähigkeit 'Vampirblut' aktiv ist, zusätzlich um 15%, jedoch gewährt Euch 'Vampirblut' keine Gesundheit mehr.",
    "icon" : "spell_shadow_lifedrain",
    "itemId" : 43554,
    "spellKey" : 58676,
    "spellId" : 58676,
    "prettyName" : "Vampirblut",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Unnachgiebiger Griff'",
    "id" : 553,
    "type" : 1,
    "description" : "Wenn Eure Fähigkeit 'Todesgriff' fehlschlägt, weil das Ziel dagegen immun ist, wird ihre Abklingzeit zurückgesetzt.",
    "icon" : "spell_deathknight_strangulate",
    "itemId" : 43672,
    "spellKey" : 59309,
    "spellId" : 59309,
    "prettyName" : "Unnachgiebiger Griff",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Eisige Pfade'",
    "id" : 554,
    "type" : 1,
    "description" : "Eure Fähigkeit 'Eisige Pfade' ermöglicht Euch, von größeren Höhen zu fallen ohne Schaden zu erleiden.",
    "icon" : "spell_deathknight_pathoffrost",
    "itemId" : 43671,
    "spellKey" : 59307,
    "spellId" : 59307,
    "prettyName" : "Eisige Pfade",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Schwarzes Tor'",
    "id" : 555,
    "type" : 1,
    "description" : "Verringert die Zauberzeit Eures Zaubers 'Schwarzes Tor' um 60%.",
    "icon" : "spell_arcane_teleportundercity",
    "itemId" : 43673,
    "spellKey" : 60200,
    "spellId" : 60200,
    "prettyName" : "Schwarzes Tor",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Runenheilung'",
    "id" : 556,
    "type" : 0,
    "description" : "Eure Fähigkeit 'Runenheilung' heilt zusätzlich Eure Gruppenmitglieder um 5% ihrer maximalen Gesundheit.",
    "icon" : "spell_deathknight_runetap",
    "itemId" : 43825,
    "spellKey" : 59327,
    "spellId" : 59327,
    "prettyName" : "Runenheilung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Siedendes Blut'",
    "id" : 557,
    "type" : 0,
    "description" : "Erhöht den Radius Eurer Fähigkeit 'Siedendes Blut' um 50%.",
    "icon" : "spell_deathknight_bloodboil",
    "itemId" : 43826,
    "spellKey" : 59332,
    "spellId" : 59332,
    "prettyName" : "Siedendes Blut",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Todesstoß'",
    "id" : 558,
    "type" : 2,
    "description" : "Erhöht den Schaden Eurer Fähigkeit 'Todesstoß' pro 5 Eurer aktuellen Runenmacht um 2% (bis zu einem Maximum von 40%). Die Runenmacht wird von diesem Effekt nicht verbraucht.",
    "icon" : "spell_deathknight_butcher2",
    "itemId" : 43827,
    "spellKey" : 59336,
    "spellId" : 59336,
    "prettyName" : "Todesstoß",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Tanzende Runenwaffe'",
    "id" : 768,
    "type" : 0,
    "description" : "Erhöht die erzeugte Bedrohung, solange die Fähigkeit 'Tanzende Runenwaffe' aktiv ist, um 50%.",
    "icon" : "inv_sword_07",
    "itemId" : 45799,
    "spellKey" : 63330,
    "spellId" : 63330,
    "prettyName" : "Tanzende Runenwaffe",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Zehrende Kälte'",
    "id" : 769,
    "type" : 0,
    "description" : "Eure Fähigkeit 'Zehrende Kälte' benötigt keine Runenmacht mehr.",
    "icon" : "inv_staff_15",
    "itemId" : 45800,
    "spellKey" : 63331,
    "spellId" : 63331,
    "prettyName" : "Zehrende Kälte",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Todesmantel'",
    "id" : 771,
    "type" : 2,
    "description" : "Erhöht den Schaden oder die Heilung Eurer Fähigkeit 'Todesmantel' um 15%.",
    "icon" : "spell_shadow_deathcoil",
    "itemId" : 45804,
    "spellKey" : 63333,
    "spellId" : 63333,
    "prettyName" : "Todesmantel",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Heulende Böe'",
    "id" : 773,
    "type" : 2,
    "description" : "Eure Fähigkeit 'Heulende Böe' steckt Ziele nun mit Frostfieber an.",
    "icon" : "spell_frost_arcticwinds",
    "itemId" : 45806,
    "spellKey" : 63335,
    "spellId" : 63335,
    "prettyName" : "Heulende Böe",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Finsterer Beistand'",
    "id" : 945,
    "type" : 0,
    "description" : "Wenn Ihr in Frost- oder unheiliger Präsenz einen Gegner tötet, der Erfahrung oder Ruf gewährt, wird innerhalb von 15 Sek. die nächste Anwendung Eurer Fähigkeit 'Todesstoß' mindestens 20% Eurer maximalen Gesundheit regenerieren.",
    "icon" : "spell_deathknight_butcher2",
    "itemId" : 68793,
    "spellKey" : 96279,
    "spellId" : 96279,
    "prettyName" : "Finsterer Beistand",
    "typeOrder" : 1
  } ]
}