{
  "talentData" : {
    "characterClass" : {
      "classId" : 3,
      "name" : "Jäger",
      "powerType" : "FOCUS",
      "powerTypeId" : 2,
      "powerTypeSlug" : "focus"
    },
    "talentTrees" : [ {
      "name" : "Tierherrschaft",
      "icon" : "ability_hunter_bestialdiscipline",
      "backgroundFile" : "HunterBeastMastery",
      "overlayColor" : "#ff004c",
      "description" : "Ein Herr der Wildnis, der viele verschiedene Tiere zähmen kann, damit sie ihm im Kampf beistehen.",
      "treeNo" : 0,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 19577,
        "name" : "Einschüchterung",
        "icon" : "ability_devour",
        "cost" : "35 Fokus",
        "range" : "100 Meter Reichweite",
        "castTime" : "Sofort",
        "cooldown" : "1 Min. Abklingzeit",
        "description" : "Befiehlt Eurem Tier, sein Ziel einzuschüchtern. Verursacht ein hohes Maß an Bedrohung und betäubt das Ziel 3 Sek. lang. Hält 15 Sek. lang an.",
        "id" : 19577,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 87325,
        "name" : "Tierführer",
        "icon" : "ability_hunter_animalhandler",
        "description" : "Angriffskraft um 30% erhöht.",
        "id" : 87325,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 76657,
        "name" : "Herr der Tiere",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht den von Euren Tieren verursachten Schaden um 13%. Jeder Punkt Meisterschaft erhöht den von Euren Tieren verursachten Schaden um zusätzlich 1.67%.",
        "id" : 76657,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 9494,
        "name" : "Verbessertes Fass!",
        "icon" : "ability_hunter_silenthunter",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die kritische Trefferchance von 'Fass!' um 5%."
        }, {
          "description" : "Erhöht die kritische Trefferchance von 'Fass!' um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9490,
        "name" : "Eins mit der Natur",
        "icon" : "ability_hunter_onewithnature",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den Angriffskraftbonus Eurer Fähigkeit 'Aspekt des Falken' um 10% und den durch Eure Fähigkeit 'Aspekt des Fuchses' wiederhergestellten Fokus um 1."
        }, {
          "description" : "Erhöht den Angriffskraftbonus Eurer Fähigkeit 'Aspekt des Falken' um 20% und den durch Eure Fähigkeit 'Aspekt des Fuchses' wiederhergestellten Fokus um 2."
        }, {
          "description" : "Erhöht den Angriffskraftbonus Eurer Fähigkeit 'Aspekt des Falken' um 30% und den durch Eure Fähigkeit 'Aspekt des Fuchses' wiederhergestellten Fokus um 3."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9492,
        "name" : "Wildtierdisziplin",
        "icon" : "ability_hunter_bestialdiscipline",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Fokusregeneration Eurer Tiere um 10%."
        }, {
          "description" : "Erhöht die Fokusregeneration Eurer Tiere um 20%."
        }, {
          "description" : "Erhöht die Fokusregeneration Eurer Tiere um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9502,
        "name" : "Orientierung",
        "icon" : "ability_hunter_pathfinding2",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den Tempo-Bonus Eures 'Aspekt des Geparden' und 'Aspekt des Rudels' um 4% und erhöht Eure Reitgeschwindigkeit um 5%. Die Erhöhung der Reitgeschwindigkeit ist nicht mit anderen Effekten stapelbar."
        }, {
          "description" : "Erhöht den Tempo-Bonus Eures 'Aspekt des Geparden' und 'Aspekt des Rudels' um 8% und erhöht Eure Reitgeschwindigkeit um 10%. Die Erhöhung der Reitgeschwindigkeit ist nicht mit anderen Effekten stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9514,
        "name" : "Geistbande",
        "icon" : "ability_druid_demoralizingroar",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Regeneriert alle 10 Sek. 1% Eurer Gesundheit und der Eures Tiers, und erhöht sowohl Eure erhaltene Heilung, als auch die Eures Tiers um 5%. Der Effekt ist wirksam, so lang Euer Tier aktiv ist."
        }, {
          "description" : "Regeneriert alle 10 Sek. 2% Eurer gesamten Gesundheit und der Eures Tiers, und erhöht sowohl Eure erhaltene Heilung, als auch die Eures Tiers um 10%. Der Effekt ist wirksam, so lang Euer Tier aktiv ist."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9512,
        "name" : "Raserei",
        "icon" : "inv_misc_monsterclaw_03",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Nach dem Ausführen eines einfachen Angriffs gewinnt Euer Tier 2% Angriffstempo. Hält 10 Sek. lang an. Bis zu 5-mal stapelbar."
        }, {
          "description" : "Nach dem Ausführen eines einfachen Angriffs gewinnt Euer Tier 4% Angriffstempo. Hält 10 Sek. lang an. Bis zu 5-mal stapelbar."
        }, {
          "description" : "Nach dem Ausführen eines einfachen Angriffs gewinnt Euer Tier 6% Angriffstempo. Hält 10 Sek. lang an. Bis zu 5-mal stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9510,
        "name" : "Verbessertes Tier heilen",
        "icon" : "ability_hunter_mendpet",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Gewährt Eurem Zauber 'Tier heilen' eine Chance von 25%, pro Tick 1 Fluch-, Krankheits-, Magie- oder Gifteffekt von Eurem Tier zu entfernen."
        }, {
          "description" : "Gewährt Eurem Zauber 'Tier heilen' eine Chance von 50%, pro Tick 1 Fluch-, Krankheits-, Magie- oder Gifteffekt von Eurem Tier zu entfernen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9530,
        "name" : "Kobrastöße",
        "icon" : "ability_hunter_cobrastrikes",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Es besteht eine Chance von 5%, dass durch Treffer Eurer Fähigkeit 'Arkaner Schuss' die nächsten 2 einfachen Angriffe Eures Tiers kritische Treffer erzielen."
        }, {
          "description" : "Es besteht eine Chance von 10%, dass durch Treffer Eurer Fähigkeit 'Arkaner Schuss' die nächsten 2 einfachen Angriffe Eures Tiers kritische Treffer erzielen."
        }, {
          "description" : "Es besteht eine Chance von 15%, dass durch Treffer Eurer Fähigkeit 'Arkaner Schuss' die nächsten 2 einfachen Angriffe Eures Tiers kritische Treffer erzielen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9504,
        "name" : "Eifer",
        "icon" : "ability_hunter_aspectoftheviper",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Stellt sowohl für Euch als auch für Euren Begleiter sofort 50 Fokus wieder her."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9520,
        "name" : "Feuer konzentrieren",
        "icon" : "ability_hunter_focusfire",
        "x" : 2,
        "y" : 2,
        "req" : 9512,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "15 Sek. Abklingzeit",
          "description" : "Zehrt Rasereieffekte Eures Tiers auf, stellt dabei 4 Fokus Eures Tiers wieder her und erhöht Euer Distanztempo pro aufgezehrtem Stapel Raserei um 3%. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9534,
        "name" : "Langlebigkeit",
        "icon" : "ability_hunter_longevity",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeiten Eurer Fähigkeiten 'Zorn des Wildtiers' und 'Einschüchterung' sowie der Spezialfähigkeiten Eures Tiers um 10%."
        }, {
          "description" : "Verringert die Abklingzeiten Eurer Fähigkeiten 'Zorn des Wildtiers' und 'Einschüchterung' sowie der Spezialfähigkeiten Eures Tiers um 20%."
        }, {
          "description" : "Verringert die Abklingzeiten Eurer Fähigkeiten 'Zorn des Wildtiers' und 'Einschüchterung' sowie der Spezialfähigkeiten Eures Tiers um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9528,
        "name" : "Beißwut",
        "icon" : "achievement_bg_kill_carrier_opposing_flagroom",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn Ihr mit Eurer Fähigkeit 'Fass!' zwei Mal hintereinander einen kritischen Treffer erzielt, wird die dritte Anwendung 10% mehr Schaden verursachen und die Fokuskosten werden um 5 verringert."
        }, {
          "description" : "Wenn Ihr mit Eurer Fähigkeit 'Fass!' zwei Mal hintereinander einen kritischen Treffer erzielt, wird die dritte Anwendung 20% mehr Schaden verursachen und die Fokuskosten werden um 10 verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11714,
        "name" : "Tiger & Schimäre",
        "icon" : "ability_hunter_pet_chimera",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Wenn Ihr einen Nahkampftreffer erleidet, wird die Abklingzeit Eurer Fähigkeit 'Rückzug' sofort um 2 Sek. verringert.\n\n\n\nWenn Ihr einen Distanz- oder Zaubertreffer erleidet, wird die Abklingzeit Eurer Fähigkeit 'Abschreckung' sofort um 4 Sek. verringert.\n\n\n\nDiese Effekte haben eine Abklingzeit von 2 Sek."
        }, {
          "description" : "Wenn Ihr einen Nahkampftreffer erleidet, wird die Abklingzeit Eurer Fähigkeit 'Rückzug' sofort um 4 Sek. verringert.\n\n\n\nWenn Ihr einen Distanz- oder Zaubertreffer erleidet, wird die Abklingzeit Eurer Fähigkeit 'Abschreckung' sofort um 8 Sek. verringert.\n\n\n\nDiese Effekte haben eine Abklingzeit von 2 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9524,
        "name" : "Zorn des Wildtiers",
        "icon" : "ability_druid_ferociousbite",
        "x" : 1,
        "y" : 4,
        "req" : 9504,
        "ranks" : [ {
          "cost" : "45 Fokus",
          "range" : "100 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Versetzt Euer Tier in einen Kampfrausch, wodurch es 10 Sek. lang 20% zusätzlichen Schaden verursacht. Einmal im Kampfrausch, gibt es keine Möglichkeit mehr, Euer Tier zu stoppen, es sei denn, es wird getötet."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9518,
        "name" : "Wilde Inspiration",
        "icon" : "ability_hunter_ferociousinspiration",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Der gesamte Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um Euren Begleiter wird um 3% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9538,
        "name" : "Seelenverwandtschaft",
        "icon" : "ability_hunter_separationanxiety",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht Euren maximalen Fokus sowie den Eures Tiers um 5."
        }, {
          "description" : "Erhöht Euren maximalen Fokus sowie den Eures Tiers um 10."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9536,
        "name" : "Wildes Herz",
        "icon" : "ability_hunter_beastwithin",
        "x" : 1,
        "y" : 5,
        "req" : 9524,
        "ranks" : [ {
          "description" : "Wenn Euer Tier unter dem Einfluss von 'Zorn des Wildtiers' steht, verfallt Ihr ebenfalls in Wut und verursacht 10% zusätzlichen Schaden, während die Fokuskosten aller Eurer Schüsse und Fähigkeiten um 50% verringert sind. Hält 10 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9522,
        "name" : "Kräftigung",
        "icon" : "ability_hunter_invigeration",
        "x" : 2,
        "y" : 5,
        "req" : 9518,
        "ranks" : [ {
          "description" : "Erzielt Euer Tier mit einem einfachen Angriff einen kritischen Treffer, regeneriert Ihr sofort 3 Fokus."
        }, {
          "description" : "Erzielt Euer Tier mit einem einfachen Angriff einen kritischen Treffer, regeneriert Ihr sofort 6 Fokus."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9542,
        "name" : "Herr der Tiere",
        "icon" : "ability_hunter_beastmastery",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "description" : "Ihr habt die Kunst der Wildtierausbildung gemeistert und erhaltet die Fähigkeit exotische Tiere zu zähmen, zudem erhält jedes Eurer Tiere 4 Fertigkeitspunkte mehr."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 0
    }, {
      "name" : "Treffsicherheit",
      "icon" : "ability_hunter_focusedaim",
      "backgroundFile" : "HunterMarksmanship",
      "overlayColor" : "#4c99ff",
      "description" : "Ein meisterlicher Bogen- und Scharfschütze, der seine Feinde am besten aus großer Entfernung zur Strecke bringt.",
      "treeNo" : 1,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 19434,
        "name" : "Gezielter Schuss",
        "icon" : "inv_spear_07",
        "cost" : "50 Fokus",
        "range" : "5-40 Meter Reichweite range",
        "castTime" : "2,9 Sek. Zauber",
        "requires" : "Benötigt Distanzwaffe",
        "description" : "Ein machtvoller gezielter Schuss, der 160% Distanzwaffenschaden plus 1241 verursacht.",
        "id" : 19434,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 87326,
        "name" : "Köcher von Meisterhand",
        "icon" : "inv_misc_quiver_06",
        "description" : "Erhöht den Schaden von 'Automatischer Schuss' um 15%.",
        "id" : 87326,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 76659,
        "name" : "Pfeilregen",
        "icon" : "spell_holy_championsbond",
        "description" : "Gewährt eine Chance von 16.8%, dass Eure Distanzangriffe einen zusätzlichen Distanzangriff auslösen. Jeder Punkt Meisterschaft erhöht die Chance um zusätzlich 2.1%.",
        "id" : 76659,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 9390,
        "name" : "An die Kehle gehen",
        "icon" : "ability_hunter_goforthethroat",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Durch Eure kritischen automatischen Distanztreffer erzeugt Euer Tier 5 Fokus."
        }, {
          "description" : "Durch Eure kritischen automatischen Distanztreffer erzeugt Euer Tier 10 Fokus."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9380,
        "name" : "Effizienz",
        "icon" : "ability_hunter_focusedaim",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Fokuskosten Eurer Fähigkeit 'Arkaner Schuss' um 1 sowie die Fokuskosten Eurer Fähigkeiten 'Explosivschuss' und 'Schimärenschuss' um 2."
        }, {
          "description" : "Verringert die Fokuskosten Eurer Fähigkeit 'Arkaner Schuss' um 2 sowie die Fokuskosten Eurer Fähigkeiten 'Explosivschuss' und 'Schimärenschuss' um 4."
        }, {
          "description" : "Verringert die Fokuskosten Eurer Fähigkeit 'Arkaner Schuss' um 3 sowie die Fokuskosten Eurer Fähigkeiten 'Explosivschuss' und 'Schimärenschuss' um 6."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9378,
        "name" : "Schneller Tod",
        "icon" : "ability_hunter_rapidkilling",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Nach dem Töten eines Gegners, der Erfahrung oder Ehre gewährt, verursacht Euer nächstes Wirken von 'Gezielter Schuss', 'Zuverlässiger Schuss' oder 'Kobraschuss' 10% zusätzlichen Schaden. Hält 20 Sek. lang an."
        }, {
          "description" : "Nach dem Töten eines Gegners, der Erfahrung oder Ehre gewährt, verursacht Euer nächstes Wirken von 'Gezielter Schuss', 'Zuverlässiger Schuss' oder 'Kobraschuss' 20% zusätzlichen Schaden. Hält 20 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9396,
        "name" : "Beiß!",
        "icon" : "ability_hunter_sickem",
        "x" : 0,
        "y" : 1,
        "req" : 9390,
        "ranks" : [ {
          "description" : "Wenn Ihr mit Euren Fähigkeiten 'Arkaner Schuss', 'Gezielter Schuss' oder 'Explosivschuss' einen kritischen Treffer erzielt, werden 12 Sek. lang die Fokuskosten des nächsten einfachen Angriffs Eures Tiers um 50% verringert."
        }, {
          "description" : "Wenn Ihr mit Euren Fähigkeiten 'Arkaner Schuss', 'Gezielter Schuss' oder 'Explosivschuss' einen kritischen Treffer erzielt, werden 12 Sek. lang die Fokuskosten des nächsten einfachen Angriffs Eures Tiers um 100% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9402,
        "name" : "Verbesserter zuverlässiger Schuss",
        "icon" : "ability_hunter_improvedsteadyshot",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Sobald Ihr Eure Fähigkeit 'Zuverlässiger Schuss' 2-mal hintereinander wirkt, wird Euer Distanzangriffstempo 8 Sek. lang um 5% erhöht."
        }, {
          "description" : "Sobald Ihr Eure Fähigkeit 'Zuverlässiger Schuss' 2-mal hintereinander wirkt, wird Euer Distanzangriffstempo 8 Sek. lang um 10% erhöht."
        }, {
          "description" : "Sobald Ihr Eure Fähigkeit 'Zuverlässiger Schuss' 2-mal hintereinander wirkt, wird Euer Distanzangriffstempo 8 Sek. lang um 15% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9398,
        "name" : "Sorgfältiges Zielen",
        "icon" : "ability_hunter_zenarchery",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Wenn Euer Ziel über 90% Gesundheit oder mehr verfügt, wird die kritische Trefferchance Eurer Fähigkeiten 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Kobraschuss' um 30% erhöht."
        }, {
          "description" : "Wenn Euer Ziel über 90% Gesundheit oder mehr verfügt, wird die kritische Trefferchance Eurer Fähigkeiten 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Kobraschuss' um 60% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9424,
        "name" : "Unterdrückender Schuss",
        "icon" : "ability_theblackarrow",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "cost" : "35 Fokus",
          "range" : "5-35 Meter Reichweite range",
          "castTime" : "Spontanzauber",
          "cooldown" : "20 Sek. Abklingzeit",
          "requires" : "Benötigt Distanzwaffe",
          "description" : "Ein Schuss, der das Ziel 3 Sek. lang zum Schweigen bringt und so das Wirken von Zaubern verhindert."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9406,
        "name" : "Erschütterndes Sperrfeuer",
        "icon" : "ability_hunter_efficiency",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erfolgreiche Angriffe mit 'Schimärenschuss' und 'Mehrfachschuss' haben eine Chance von 50%, das Ziel 4 Sek. lang benommen zu machen."
        }, {
          "description" : "Erfolgreiche Angriffe mit 'Schimärenschuss' und 'Mehrfachschuss' haben eine Chance von 100%, das Ziel 4 Sek. lang benommen zu machen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11225,
        "name" : "Durchschlagende Schüsse",
        "icon" : "ability_hunter_piercingshots",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Die kritischen Treffer Eurer Fähigkeiten 'Gezielter Schuss', 'Zuverlässiger Schuss' und 'Schimärenschuss' lassen das Ziel im Verlauf von 8 Sek. für 10% des verursachten Schadens bluten."
        }, {
          "description" : "Die kritischen Treffer Eurer Fähigkeiten 'Gezielter Schuss', 'Zuverlässiger Schuss' und 'Schimärenschuss' lassen das Ziel im Verlauf von 8 Sek. für 20% des verursachten Schadens bluten."
        }, {
          "description" : "Die kritischen Treffer Eurer Fähigkeiten 'Gezielter Schuss', 'Zuverlässiger Schuss' und 'Schimärenschuss' lassen das Ziel im Verlauf von 8 Sek. für 30% des verursachten Schadens bluten."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9408,
        "name" : "Bombardement",
        "icon" : "ability_marksmanship",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erzielt Ihr mit 'Mehrfachschuss' einen kritischen Treffer, werden die Fokuskosten Eures nächsten Wirkens von 'Mehrfachschuss' um 25% verringert."
        }, {
          "description" : "Erzielt Ihr mit 'Mehrfachschuss' einen kritischen Treffer, werden die Fokuskosten Eures nächsten Wirkens von 'Mehrfachschuss' um 50% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9412,
        "name" : "Aura des Volltreffers",
        "icon" : "ability_trueshot",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht die Angriffskraft aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 20% sowie ihre Distanzangriffskraft um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9416,
        "name" : "Terminieren",
        "icon" : "ability_warrior_focusedrage",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Eure Fähigkeiten 'Zuverlässiger Schuss' und 'Kobraschuss' gewähren zusätzlich 3 Fokus, wenn Euer Ziel über 25% Gesundheit oder weniger verfügt."
        }, {
          "description" : "Eure Fähigkeiten 'Zuverlässiger Schuss' und 'Kobraschuss' gewähren zusätzlich 6 Fokus, wenn Euer Ziel über 25% Gesundheit oder weniger verfügt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9420,
        "name" : "Widerstand ist zwecklos",
        "icon" : "ability_hunter_resistanceisfutile",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn Euer markiertes Ziel versucht, zu rennen, zu flüchten oder sich zu bewegen, besteht eine Chance von 4%, dass Euer nächstes, innerhalb von 8 Sek. auf dieses Ziel gewirkte 'Fass!' die Fokuskosten zurückerstattet."
        }, {
          "description" : "Wenn Euer markiertes Ziel versucht, zu rennen, zu flüchten oder sich zu bewegen, besteht eine Chance von 8%, dass Euer nächstes, innerhalb von 8 Sek. auf dieses Ziel gewirkte 'Fass!' die Fokuskosten zurückerstattet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9422,
        "name" : "Rasche Erholung",
        "icon" : "ability_hunter_rapidregeneration",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Steht Ihr unter dem Effekt von 'Schnellfeuer', gewinnt Ihr alle 3 Sek. 6 Fokus. Wird 'Schneller Tod' ausgelöst, gewinnt Ihr sofort 25 Fokus."
        }, {
          "description" : "Steht Ihr unter dem Effekt von 'Schnellfeuer', gewinnt Ihr alle 3 Sek. 12 Fokus. Wird 'Schneller Tod' ausgelöst, gewinnt Ihr sofort 50 Fokus."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9418,
        "name" : "Meisterschütze",
        "icon" : "ability_hunter_mastermarksman",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "description" : "Bei der Nutzung von 'Zuverlässiger Schuss' besteht eine Chance von 20%, dass Ihr den Effekt 'Meisterschütze' erhaltet, der 30 Sek. lang anhält. Nachdem 5 Stapel dieses Effekts erreicht wurden, werden 10 Sek. lang die Zauberzeit und Fokuskosten Eurer nächsten Anwendung von 'Gezielter Schuss' um 100% verringert."
        }, {
          "description" : "Bei der Nutzung von 'Zuverlässiger Schuss' besteht eine Chance von 40%, dass Ihr den Effekt 'Meisterschütze' erhaltet, der 30 Sek. lang anhält. Nachdem 5 Stapel dieses Effekts erreicht wurden, werden 10 Sek. lang die Zauberzeit und Fokuskosten Eurer nächsten Anwendung von 'Gezielter Schuss' um 100% verringert."
        }, {
          "description" : "Bei der Nutzung von 'Zuverlässiger Schuss' besteht eine Chance von 60%, dass Ihr den Effekt 'Meisterschütze' erhaltet, der 30 Sek. lang anhält. Nachdem 5 Stapel dieses Effekts erreicht wurden, werden 10 Sek. lang die Zauberzeit und Fokuskosten Eurer nächsten Anwendung von 'Gezielter Schuss' um 100% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9404,
        "name" : "Bereitschaft",
        "icon" : "ability_hunter_readiness",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Bei Aktivierung schließt diese Fähigkeit die Abklingzeit aller anderen Jägerfähigkeiten sofort ab."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9426,
        "name" : "Impromptu",
        "icon" : "ability_hunter_posthaste",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Schnellfeuer' um 1 Min. und erhöht Euer Bewegungstempo nach der Nutzung von 'Rückzug' 4 Sek. lang um 15%."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Schnellfeuer' um 2 Min. und erhöht Euer Bewegungstempo nach der Nutzung von 'Rückzug' 4 Sek. lang um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9428,
        "name" : "Zum Tode verurteilt",
        "icon" : "ability_hunter_assassinate",
        "x" : 2,
        "y" : 5,
        "req" : 9418,
        "ranks" : [ {
          "description" : "Eure Fähigkeiten 'Arkaner Schuss' und 'Schimärenschuss' gewähren eine Chance von 50%, dass Euer Ziel sofort mit dem Effekt 'Zum Tode verurteilt' belegt wird.\n\n\n\nDer Effekt 'Zum Tode verurteilt' gewährt die gleichen Boni wie Eure Fähigkeit 'Mal des Jägers', kann jedoch nicht gebannt werden und schränkt weder Verstohlenheit noch Unsichtbarkeit ein. Hält 15 Sek. lang an."
        }, {
          "description" : "Eure Fähigkeiten 'Arkaner Schuss' und 'Schimärenschuss' gewähren eine Chance von 100%, dass Euer Ziel sofort mit dem Effekt 'Zum Tode verurteilt' belegt wird.\n\n\n\nDer Effekt 'Zum Tode verurteilt' gewährt die gleichen Boni wie Eure Fähigkeit 'Mal des Jägers', kann jedoch nicht gebannt werden und schränkt weder Verstohlenheit noch Unsichtbarkeit ein. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9430,
        "name" : "Schimärenschuss",
        "icon" : "ability_hunter_chimerashot2",
        "x" : 1,
        "y" : 6,
        "req" : 9418,
        "ranks" : [ {
          "cost" : "50 Fokus",
          "range" : "5-40 Meter Reichweite range",
          "castTime" : "Spontanzauber",
          "cooldown" : "10 Sek. Abklingzeit",
          "requires" : "Benötigt Distanzwaffe",
          "description" : "Ein sofortiger Schuss, der Distanzwaffenschaden als Naturschaden plus 1620 verursacht und die Dauer Eures auf das Ziel wirkenden Effekts von 'Schlangengift' auffrischt. Zudem werdet Ihr um 5% Eurer gesamten Gesundheit geheilt."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 1
    }, {
      "name" : "Überleben",
      "icon" : "ability_hunter_camouflage",
      "backgroundFile" : "HunterSurvival",
      "overlayColor" : "#ff9900",
      "description" : "Ein zäher Fährtenleser, der am liebsten Tiergifte, Sprengstoffe und Fallen als tödliche Waffen nutzt.",
      "treeNo" : 2,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 53301,
        "name" : "Explosivschuss",
        "icon" : "ability_hunter_explosiveshot",
        "cost" : "50 Fokus",
        "range" : "5-40 Meter Reichweite range",
        "castTime" : "Spontanzauber",
        "cooldown" : "6 Sek. Abklingzeit",
        "requires" : "Benötigt Distanzwaffe",
        "description" : "Feuert ein Explosivgeschoss, das einem Gegner 449 Feuerschaden zufügt. Die Sprengladungen verursachen 2 Sek. lang pro Sekunde weiteren Schaden.",
        "id" : 53301,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 84729,
        "name" : "In die Wildnis",
        "icon" : "achievement_zone_alteracmountains_01",
        "description" : "Beweglichkeit um 10% erhöht.",
        "id" : 84729,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 76658,
        "name" : "Essenz der Viper",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht jeglichen von Euch verursachten Magieschaden um 8%. Jeder Punkt Meisterschaft erhöht den Magieschaden um zusätzlich 1.0%.",
        "id" : 76658,
        "classMask" : 4,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 9442,
        "name" : "Jäger vs. Wildnis",
        "icon" : "ability_hunter_huntervswild",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht Eure gesamte Ausdauer um 5%."
        }, {
          "description" : "Erhöht Eure gesamte Ausdauer um 10%."
        }, {
          "description" : "Erhöht Eure gesamte Ausdauer um 15%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9432,
        "name" : "Mehrladewaffen",
        "icon" : "ability_hunter_improvedtracking",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht das Distanzangriffstempo um 1%"
        }, {
          "description" : "Erhöht das Distanzangriffstempo um 2%"
        }, {
          "description" : "Erhöht das Distanzangriffstempo um 3%"
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9450,
        "name" : "Verbessertes Schlangengift",
        "icon" : "ability_hunter_quickshot",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Eure Fähigkeit 'Schlangengift' verursacht zusätzlich sofort Schaden in Höhe von 15% ihres gesamten regelmäßigen Effekts.\n\n\n\nErhöht zudem die kritische Trefferchance des regelmäßigen Effekts Eurer Fähigkeit 'Schlangengift' um 5%."
        }, {
          "description" : "Eure Fähigkeit 'Schlangengift' verursacht zusätzlich sofort Schaden in Höhe von 30% ihres gesamten regelmäßigen Effekts.\n\n\n\nErhöht zudem die kritische Trefferchance des regelmäßigen Effekts Eurer Fähigkeit 'Schlangengift' um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9444,
        "name" : "Überlebenstaktik",
        "icon" : "ability_rogue_feigndeath",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Chance, dass Euren Fallen widerstanden wird, um 2% und verringert die Abklingzeit Eurer Fähigkeit 'Rückzug' um 2 Sek."
        }, {
          "description" : "Verringert die Chance, dass Euren Fallen widerstanden wird, um 4% und verringert die Abklingzeit Eurer Fähigkeit 'Rückzug' um 4 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10753,
        "name" : "Fallenbeherrschung",
        "icon" : "ability_ensnare",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Frostfalle und Eiskältefalle - Erhöht die Dauer um 10%.\n\n\n\nFeuerbrandfalle, Sprengfalle und Schwarzer Pfeil - Erhöht den regelmäßigen Schaden um 10%.\n\n\n\nSchlangenfalle - Erhöht die Anzahl der beschworenen Schlangen um 2."
        }, {
          "description" : "Frostfalle und Eiskältefalle - Erhöht die Dauer um 20%.\n\n\n\nFeuerbrandfalle, Sprengfalle und Schwarzer Pfeil - Erhöht den regelmäßigen Schaden um 20%.\n\n\n\nSchlangenfalle - Erhöht die Anzahl der beschworenen Schlangen um 4."
        }, {
          "description" : "Frostfalle und Eiskältefalle - Erhöht die Dauer um 30%.\n\n\n\nFeuerbrandfalle, Sprengfalle und Schwarzer Pfeil - Erhöht den regelmäßigen Schaden um 30%.\n\n\n\nSchlangenfalle - Erhöht die Anzahl der beschworenen Schlangen um 6."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9440,
        "name" : "Einfangen",
        "icon" : "spell_nature_stranglevines",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Sowohl Eure Frostfalle als auch Eure Schlangenfalle setzen bei ihrer Auslösung alle Ziele gefangen. Jedes der Ziele wird 2 Sek. lang bewegungsunfähig."
        }, {
          "description" : "Sowohl Eure Frostfalle als auch Eure Schlangenfalle setzen bei ihrer Auslösung alle Ziele gefangen. Jedes der Ziele wird 4 Sek. lang bewegungsunfähig."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9472,
        "name" : "Kein Entkommen",
        "icon" : "ability_hunter_pointofnoescape",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht die kritische Distanztrefferchance aller Eurer Angriffe gegen Ziele, die vom Effekt Eurer Frost- oder Eiskältefalle betroffen sind, um 3%."
        }, {
          "description" : "Erhöht die kritische Distanztrefferchance aller Eurer Angriffe gegen Ziele, die vom Effekt Eurer Frost- oder Eiskältefalle betroffen sind, um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9484,
        "name" : "Jagdfieber",
        "icon" : "ability_hunter_thrillofthehunt",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Bei der Nutzung Eurer Fähigkeiten 'Arkaner Schuss', 'Explosivschuss' oder 'Schwarzer Pfeil' besteht eine Chance von 5%, dass Ihr sofort 40% der Grundfokuskosten des Schusses zurückerlangt."
        }, {
          "description" : "Bei der Nutzung Eurer Fähigkeiten 'Arkaner Schuss', 'Explosivschuss' oder 'Schwarzer Pfeil' besteht eine Chance von 10%, dass Ihr sofort 40% der Grundfokuskosten des Schusses zurückerlangt."
        }, {
          "description" : "Bei der Nutzung Eurer Fähigkeiten 'Arkaner Schuss', 'Explosivschuss' oder 'Schwarzer Pfeil' besteht eine Chance von 15%, dass Ihr sofort 40% der Grundfokuskosten des Schusses zurückerlangt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9448,
        "name" : "Gegenangriff",
        "icon" : "ability_warrior_challange",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "cost" : "3% des Grundmanas",
          "range" : "Nahkampfreichweite",
          "castTime" : "Sofort",
          "cooldown" : "5 Sek. Abklingzeit",
          "description" : "Ein Schlag, der aktiv wird, nachdem ein gegnerischer Angriff pariert wurde. Dieser Angriff verursacht 434 Schaden und macht das Ziel 5 Sek. lang bewegungsunfähig. Auf 'Gegenangriff' kann nicht mit 'Blocken', 'Ausweichen' oder 'Parieren' reagiert werden."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9452,
        "name" : "Sichern und Laden",
        "icon" : "ability_hunter_lockandload",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wird ein Ziel durch Eure Eiskälte- oder Frostfalle gefangen, besteht eine Chance von 50%, dass Eure nächsten 2 Anwendungen von 'Arkaner Schuss' oder 'Explosivschuss' keine Abklingzeit auslösen und keinen Fokus kosten. Der Effekt hält 12 Sek. lang an."
        }, {
          "description" : "Wird ein Ziel durch Eure Eiskälte- oder Frostfalle gefangen, besteht eine Chance von 100%, dass Eure nächsten 2 Anwendungen von 'Arkaner Schuss' oder 'Explosivschuss' keine Abklingzeit auslösen und keinen Fokus kosten. Der Effekt hält 12 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9460,
        "name" : "Improvisation",
        "icon" : "ability_hunter_resourcefulness",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit aller Eurer Fallen sowie Eurer Fähigkeit 'Schwarzer Pfeil' um 2 Sek."
        }, {
          "description" : "Verringert die Abklingzeit aller Eurer Fallen sowie Eurer Fähigkeit 'Schwarzer Pfeil' um 4 Sek."
        }, {
          "description" : "Verringert die Abklingzeit aller Eurer Fallen sowie Eurer Fähigkeit 'Schwarzer Pfeil' um 6 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9482,
        "name" : "Spiegelnde Klingen",
        "icon" : "inv_weapon_shortblade_99",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn Ihr einen Zaubertreffer erleidet, während 'Abschreckung' aktiv ist, besteht eine Chance von 50%, dass der Zauber auf den Zaubernden reflektiert wird."
        }, {
          "description" : "Wenn Ihr einen Zaubertreffer erleidet, während 'Abschreckung' aktiv ist, besteht eine Chance von 100%, dass der Zauber auf den Zaubernden reflektiert wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9462,
        "name" : "T.N.T.",
        "icon" : "inv_misc_bomb_05",
        "x" : 2,
        "y" : 3,
        "req" : 9452,
        "ranks" : [ {
          "description" : "Wenn der regelmäßige Effekt Eurer Fähigkeiten 'Feuerbrandfalle', 'Sprengfalle' oder 'Schwarzer Pfeil' Schaden verursacht, besteht eine Chance von 6%, dass 'Sichern und Laden' ausgelöst wird."
        }, {
          "description" : "Wenn der regelmäßige Effekt Eurer Fähigkeiten 'Feuerbrandfalle', 'Sprengfalle' oder 'Schwarzer Pfeil' Schaden verursacht, besteht eine Chance von 12%, dass 'Sichern und Laden' ausgelöst wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9464,
        "name" : "Giftkunde",
        "icon" : "inv_alchemy_enchantedvial",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Erhöht den regelmäßigen kritischen Schaden Eurer Fähigkeiten 'Schlangengift' und 'Schwarzer Pfeil' um 50%."
        }, {
          "description" : "Erhöht den regelmäßigen kritischen Schaden Eurer Fähigkeiten 'Schlangengift' und 'Schwarzer Pfeil' um 100%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9468,
        "name" : "Wyverngift",
        "icon" : "inv_spear_02",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "cost" : "10 Fokus",
          "range" : "5-35 Meter Reichweite range",
          "castTime" : "Spontanzauber",
          "cooldown" : "1 Min. Abklingzeit",
          "requires" : "Benötigt Distanzwaffe",
          "description" : "Ein Gift, welches das Ziel 30 Sek. lang einschlafen lässt. Jeglicher erlittene Schaden hebt den Effekt wieder auf. Sobald das Ziel erwacht, verursacht der Giftpfeil im Verlauf von 6 Sek. insgesamt 2739 Naturschaden. Auf dem Ziel kann gleichzeitig nur ein Gift pro Jäger aktiv sein."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9474,
        "name" : "Kontaktgifte",
        "icon" : "ability_hunter_potentvenom",
        "x" : 2,
        "y" : 4,
        "req" : 9468,
        "ranks" : [ {
          "description" : "Erhöht Euren Distanzschaden gegen Ziele, die vom Effekt Eurer Fähigkeit 'Schlangengift' betroffen sind, um 5%.\n\n\n\nWird 'Wyverngift' gebannt, wird der Bannende ebenfalls für 25% der verbliebenen Dauer von 'Wyverngift' betroffen."
        }, {
          "description" : "Erhöht Euren Distanzschaden gegen Ziele, die vom Effekt Eurer Fähigkeit 'Schlangengift' betroffen sind, um 10%.\n\n\n\nWird 'Wyverngift' gebannt, wird der Bannende ebenfalls für 50% der verbliebenen Dauer von 'Wyverngift' betroffen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9476,
        "name" : "Jagdgesellschaft",
        "icon" : "ability_hunter_huntingparty",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "description" : "Erhöht Eure gesamte Beweglichkeit um zusätzlich 2%. Zudem wird das Nahkampf- und Distanzangriffstempo aller Gruppen- und Schlachtzugsmitglieder um 10% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9478,
        "name" : "Scharfschützentraining",
        "icon" : "ability_hunter_longshots",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Tödlicher Schuss' um 5%. Wenn Ihr 6 Sek. lang stillsteht, wird der verursachte Schaden Eurer Fähigkeiten 'Zuverlässiger Schuss' und 'Kobraschuss' 15 Sek. lang um 2% erhöht."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Tödlicher Schuss' um 10%. Wenn Ihr 6 Sek. lang stillsteht, wird der verursachte Schaden Eurer Fähigkeiten 'Zuverlässiger Schuss' und 'Kobraschuss' 15 Sek. lang um 4% erhöht."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Tödlicher Schuss' um 15%. Wenn Ihr 6 Sek. lang stillsteht, wird der verursachte Schaden Eurer Fähigkeiten 'Zuverlässiger Schuss' und 'Kobraschuss' 15 Sek. lang um 6% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11698,
        "name" : "Schlangenfächer",
        "icon" : "ability_hunter_serpentswiftness",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Ziele, die von Eurer Fähigkeit 'Mehrfachschuss' getroffen werden, erleiden auch Schaden durch Euer Schlangengift. Hält 6 Sek. lang an."
        }, {
          "description" : "Ziele, die von Eurer Fähigkeit 'Mehrfachschuss' getroffen werden, erleiden auch Schaden durch Euer Schlangengift. Hält 9 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9480,
        "name" : "Schwarzer Pfeil",
        "icon" : "spell_shadow_painspike",
        "x" : 1,
        "y" : 6,
        "req" : 9468,
        "ranks" : [ {
          "cost" : "35 Fokus",
          "range" : "5-40 Meter Reichweite range",
          "castTime" : "Spontanzauber",
          "cooldown" : "30 Sek. Abklingzeit",
          "requires" : "Benötigt Distanzwaffe",
          "description" : "Feuert einen schwarzen Pfeil auf das Ziel ab, der im Verlauf von 15 Sek. 3225 Schattenschaden verursacht. Der schwarze Pfeil teilt sich eine Abklingzeit mit anderen Feuerfallen."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 2
    } ]
  },
  "glyphs" : [ {
    "name" : "Glyphe 'Gezielter Schuss'",
    "id" : 351,
    "type" : 2,
    "description" : "Wenn Ihr mit Eurer Fähigkeit 'Gezielter Schuss' einen kritischen Treffer erzielt, gewinnt Ihr sofort 5 Fokus.",
    "icon" : "inv_spear_07",
    "itemId" : 42897,
    "spellKey" : 56824,
    "spellId" : 56824,
    "prettyName" : "Gezielter Schuss",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Arkaner Schuss'",
    "id" : 352,
    "type" : 2,
    "description" : "Der Schaden Eurer Fähigkeit 'Arkaner Schuss' wird um 12% erhöht.",
    "icon" : "ability_impalingbolt",
    "itemId" : 42898,
    "spellKey" : 56841,
    "spellId" : 56841,
    "prettyName" : "Arkaner Schuss",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Fallenschleuder'",
    "id" : 353,
    "type" : 0,
    "description" : "Verringert die Fokuskosten Eurer Fähigkeit 'Fallenschleuder' um 10.",
    "icon" : "ability_hunter_traplauncher",
    "itemId" : 42899,
    "spellKey" : 56857,
    "spellId" : 56857,
    "prettyName" : "Fallenschleuder",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Besserung'",
    "id" : 354,
    "type" : 0,
    "description" : "Erhöht die gesamte gewirkte Heilung Eurer Fähigkeit 'Tier heilen' um 60%.",
    "icon" : "ability_hunter_mendpet",
    "itemId" : 42900,
    "spellKey" : 56833,
    "spellId" : 56833,
    "prettyName" : "Besserung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Erschütternder Schuss'",
    "id" : 355,
    "type" : 0,
    "description" : "Eure Fähigkeit 'Erschütternder Schuss' beschränkt zusätzlich das maximale Lauftempo des Ziels.",
    "icon" : "spell_frost_stun",
    "itemId" : 42901,
    "spellKey" : 56851,
    "spellId" : 56851,
    "prettyName" : "Erschütternder Schuss",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Zorn des Wildtiers'",
    "id" : 356,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Zorn des Wildtiers' um 20 Sek.",
    "icon" : "ability_druid_ferociousbite",
    "itemId" : 42902,
    "spellKey" : 56830,
    "spellId" : 56830,
    "prettyName" : "Zorn des Wildtiers",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Abschreckung'",
    "id" : 357,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Abschreckung' um 10 Sek.",
    "icon" : "ability_whirlwind",
    "itemId" : 42903,
    "spellKey" : 56850,
    "spellId" : 56850,
    "prettyName" : "Abschreckung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Rückzug'",
    "id" : 358,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Rückzug' um 5 Sek.",
    "icon" : "ability_rogue_feint",
    "itemId" : 42904,
    "spellKey" : 56844,
    "spellId" : 56844,
    "prettyName" : "Rückzug",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Eiskältefalle'",
    "id" : 359,
    "type" : 0,
    "description" : "Endet der Effekt Eurer Eiskältefalle, wird das Bewegungstempo des Ziels 4 Sek. lang um 70% verringert.",
    "icon" : "spell_frost_chainsofice",
    "itemId" : 42905,
    "spellKey" : 56845,
    "spellId" : 56845,
    "prettyName" : "Eiskältefalle",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Frostfalle'",
    "id" : 360,
    "type" : 0,
    "description" : "Erhöht den Effektradius Eurer Frostfalle um 2 Meter.",
    "icon" : "spell_frost_frostnova",
    "itemId" : 42906,
    "spellKey" : 56847,
    "spellId" : 56847,
    "prettyName" : "Frostfalle",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Irreführung'",
    "id" : 361,
    "type" : 0,
    "description" : "Wenn Ihr Eure Fähigkeit 'Irreführung' auf Euer Tier anwendet, wird die Abklingzeit von 'Irreführung' sofort abgeschlossen.",
    "icon" : "ability_hunter_misdirection",
    "itemId" : 42907,
    "spellKey" : 56829,
    "spellId" : 56829,
    "prettyName" : "Irreführung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Feuerbrandfalle'",
    "id" : 362,
    "type" : 0,
    "description" : "Verringert die Dauer des Effektes Eurer Feuerbrandfalle um 6 Sek., erhöht jedoch den in dieser Zeit verursachten Schaden um 100%.",
    "icon" : "spell_fire_flameshock",
    "itemId" : 42908,
    "spellKey" : 56846,
    "spellId" : 56846,
    "prettyName" : "Feuerbrandfalle",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Benommene Beute'",
    "id" : 363,
    "type" : 2,
    "description" : "Eure Fähigkeiten 'Zuverlässiger Schuss' und 'Kobraschuss' erzeugen bei benommenen Zielen 2 zusätzlichen Fokus.",
    "icon" : "ability_rogue_cheatdeath",
    "itemId" : 42909,
    "spellKey" : 56856,
    "spellId" : 56856,
    "prettyName" : "Benommene Beute",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Unterdrückender Schuss'",
    "id" : 364,
    "type" : 0,
    "description" : "Wenn Ihr mit 'Unterdrückender Schuss' erfolgreich den gewirkten Zauber eines Gegners unterbrecht, gewinnt Ihr sofort 10 Fokus.",
    "icon" : "ability_theblackarrow",
    "itemId" : 42910,
    "spellKey" : 56836,
    "spellId" : 56836,
    "prettyName" : "Unterdrückender Schuss",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schnellfeuer'",
    "id" : 365,
    "type" : 2,
    "description" : "Erhöht das durch Eure Fähigkeit 'Schnellfeuer' erhaltene Tempo um zusätzliche 10%.",
    "icon" : "ability_hunter_runningshot",
    "itemId" : 42911,
    "spellKey" : 56828,
    "spellId" : 56828,
    "prettyName" : "Schnellfeuer",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Schlangengift'",
    "id" : 366,
    "type" : 2,
    "description" : "Erhöht die regelmäßige kritische Trefferchance Eurer Fähigkeit 'Schlangengift' um 6%.",
    "icon" : "ability_hunter_quickshot",
    "itemId" : 42912,
    "spellKey" : 56832,
    "spellId" : 56832,
    "prettyName" : "Schlangengift",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Schlangenfalle'",
    "id" : 367,
    "type" : 0,
    "description" : "Die von Eurer Schlangenfalle beschworenen Schlangen erleiden 90% weniger Schaden durch Flächeneffekte.",
    "icon" : "ability_hunter_snaketrap",
    "itemId" : 42913,
    "spellKey" : 56849,
    "spellId" : 56849,
    "prettyName" : "Schlangenfalle",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Zuverlässiger Schuss'",
    "id" : 368,
    "type" : 2,
    "description" : "Erhöht den von Eurer Fähigkeit 'Zuverlässiger Schuss' verursachten Schaden um 10%.",
    "icon" : "ability_hunter_steadyshot",
    "itemId" : 42914,
    "spellKey" : 56826,
    "spellId" : 56826,
    "prettyName" : "Zuverlässiger Schuss",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Fass!'",
    "id" : 369,
    "type" : 2,
    "description" : "Verringert die Fokuskosten Eurer Fähigkeit 'Fass!' um 3.",
    "icon" : "ability_hunter_killcommand",
    "itemId" : 42915,
    "spellKey" : 56842,
    "spellId" : 56842,
    "prettyName" : "Fass!",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Wyverngift'",
    "id" : 371,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Wyverngift' um 6 Sek.",
    "icon" : "inv_spear_02",
    "itemId" : 42917,
    "spellKey" : 56848,
    "spellId" : 56848,
    "prettyName" : "Wyverngift",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Tier wiederbeleben'",
    "id" : 439,
    "type" : 1,
    "description" : "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Anwenden Eurer Fähigkeit 'Tier wiederbeleben' um 100%.",
    "icon" : "ability_hunter_beastsoothe",
    "itemId" : 43338,
    "spellKey" : 57866,
    "spellId" : 57866,
    "prettyName" : "Tier wiederbeleben",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Kleinere Proportionen'",
    "id" : 440,
    "type" : 1,
    "description" : "Verringert die Größe Eures Tiers ein wenig.",
    "icon" : "ability_hunter_bestialdiscipline",
    "itemId" : 43350,
    "spellKey" : 57870,
    "spellId" : 57870,
    "prettyName" : "Kleinere Proportionen",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Totstellen'",
    "id" : 441,
    "type" : 1,
    "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Totstellen' um 5 Sek.",
    "icon" : "ability_rogue_feigndeath",
    "itemId" : 43351,
    "spellKey" : 57903,
    "spellId" : 57903,
    "prettyName" : "Totstellen",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Wildtier ängstigen'",
    "id" : 442,
    "type" : 1,
    "description" : "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken Eurer Fähigkeit 'Wildtier ängstigen' um 75%.",
    "icon" : "ability_druid_cower",
    "itemId" : 43356,
    "spellKey" : 57902,
    "spellId" : 57902,
    "prettyName" : "Wildtier ängstigen",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Aspekt des Rudels'",
    "id" : 443,
    "type" : 1,
    "description" : "Erhöht die Reichweite Eurer Fähigkeit 'Aspekt des Rudels' um 15 Meter.",
    "icon" : "ability_mount_whitetiger",
    "itemId" : 43355,
    "spellKey" : 57904,
    "spellId" : 57904,
    "prettyName" : "Aspekt des Rudels",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Schimärenschuss'",
    "id" : 677,
    "type" : 2,
    "description" : "Verringert die Abklingzeit der Fähigkeit 'Schimärenschuss' um 1 Sek.",
    "icon" : "ability_hunter_chimerashot2",
    "itemId" : 45625,
    "spellKey" : 63065,
    "spellId" : 63065,
    "prettyName" : "Schimärenschuss",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Explosivschuss'",
    "id" : 691,
    "type" : 2,
    "description" : "Erhöht die kritische Trefferchance der Fähigkeit 'Explosivschuss' um 6%.",
    "icon" : "ability_hunter_explosiveshot",
    "itemId" : 45731,
    "spellKey" : 63066,
    "spellId" : 63066,
    "prettyName" : "Explosivschuss",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Tödlicher Schuss'",
    "id" : 692,
    "type" : 2,
    "description" : "Wenn Eure Fähigkeit 'Tödlicher Schuss' ein Ziel nicht tötet, das über 20% Gesundheit oder weniger verfügt, wird die Abklingzeit der Fähigkeit sofort abgeschlossen. Dieser Effekt hat eine Abklingzeit von 6 Sek.",
    "icon" : "ability_hunter_assassinate2",
    "itemId" : 45732,
    "spellKey" : 63067,
    "spellId" : 63067,
    "prettyName" : "Tödlicher Schuss",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Ruf des Meisters'",
    "id" : 693,
    "type" : 0,
    "description" : "Erhöht die Dauer Eurer Fähigkeit 'Ruf des Meisters' um 4 Sek.",
    "icon" : "ability_hunter_masterscall",
    "itemId" : 45733,
    "spellKey" : 63068,
    "spellId" : 63068,
    "prettyName" : "Ruf des Meisters",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Streuschuss'",
    "id" : 694,
    "type" : 0,
    "description" : "Erhöht die Reichweite der Fähigkeit 'Streuschuss' um 3 Meter.",
    "icon" : "ability_golemstormbolt",
    "itemId" : 45734,
    "spellKey" : 63069,
    "spellId" : 63069,
    "prettyName" : "Streuschuss",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Raptorstoß'",
    "id" : 695,
    "type" : 0,
    "description" : "Verringert nach der Nutzung Eurer Fähigkeit 'Raptorstoß' den erlittenen Schaden 5 Sek. lang um 20%.",
    "icon" : "ability_meleedamage",
    "itemId" : 45735,
    "spellKey" : 63086,
    "spellId" : 63086,
    "prettyName" : "Raptorstoß",
    "typeOrder" : 1
  } ]
}