{
  "talentData" : {
    "characterClass" : {
      "classId" : 5,
      "name" : "Priester",
      "powerType" : "MANA",
      "powerTypeId" : 0,
      "powerTypeSlug" : "mana"
    },
    "talentTrees" : [ {
      "name" : "Disziplin",
      "icon" : "spell_holy_powerwordshield",
      "backgroundFile" : "PriestDiscipline",
      "overlayColor" : "#ff7f00",
      "description" : "Nutzt Magie, um Verbündete vor Schaden zu schützen und ihre Wunden zu heilen.",
      "treeNo" : 0,
      "roles" : {
        "tank" : false,
        "healer" : true,
        "dps" : false
      },
      "primarySpells" : [ {
        "spellId" : 47540,
        "name" : "Sühne",
        "icon" : "spell_holy_penance",
        "cost" : "14% des Grundmanas",
        "range" : "30 Meter Reichweite",
        "castTime" : "Kanalisiert",
        "cooldown" : "12 Sek. Abklingzeit",
        "description" : "Schleudert eine Salve heiligen Lichts, die einem feindlichen Ziel 788 Heiligschaden zufügt oder ein befreundetes Ziel sofort um 3036 heilt sowie 2 Sek. lang alle 1 Sek. um einen zusätzlichen Betrag.",
        "id" : 47540,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 84732,
        "name" : "Erleuchtung",
        "icon" : "spell_arcane_mindmastery",
        "description" : "Intelligenz um 15% erhöht.",
        "id" : 84732,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 33167,
        "name" : "Absolution",
        "icon" : "spell_holy_absolution",
        "description" : "Ermöglicht es Eurem Zauber 'Magiebannung', mit einem Wirken 2 schädliche Effekte von einem befreundeten Ziel zu bannen.",
        "id" : 33167,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 95860,
        "name" : "Meditation",
        "icon" : "spell_nature_sleep",
        "description" : "Lässt 50% Eurer durch Willenskraft erzeugten Manaregeneration weiterlaufen, während Ihr Euch im Kampf befindet.",
        "id" : 95860,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77484,
        "name" : "Schilddisziplin",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht das Potenzial Eurer Schaden absorbierenden Zauber um 20%. Jeder Punkt Meisterschaft erhöht das Absorptionspotenzial um zusätzlich 2.5%.",
        "id" : 77484,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 10736,
        "name" : "Verbessertes Machtwort: Schild",
        "icon" : "spell_holy_powerwordshield",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den von Eurem Zauber 'Machtwort: Schild' absorbierten Schaden um 10%."
        }, {
          "description" : "Erhöht den von Eurem Zauber 'Machtwort: Schild' absorbierten Schaden um 20%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8577,
        "name" : "Zwillingsdisziplinen",
        "icon" : "spell_holy_sealofvengeance",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht Euren Schatten- und Heiligzauberschaden sowie Eure Heilung um 2%."
        }, {
          "description" : "Erhöht Euren Schatten- und Heiligzauberschaden sowie Eure Heilung um 4%."
        }, {
          "description" : "Erhöht Euren Schatten- und Heiligzauberschaden sowie Eure Heilung um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8595,
        "name" : "Mentale Beweglichkeit",
        "icon" : "ability_hibernation",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Manakosten Eurer Spontanzauber um 4%."
        }, {
          "description" : "Verringert die Manakosten Eurer Spontanzauber um 7%."
        }, {
          "description" : "Verringert die Manakosten Eurer Spontanzauber um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8593,
        "name" : "Prediger",
        "icon" : "spell_holy_divineillumination",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Beim Wirken Eurer Zauber 'Heilige Pein', 'Heiliges Feuer' oder 'Gedankenschinden' gewinnt Ihr den Effekt 'Prediger', der bis zu 5-mal stapelbar ist und 20 Sek. lang anhält.\n\n\n\nPrediger (Heilige Pein, Heiliges Feuer)\n\nErhöht den Schaden Eurer Zauber 'Heilige Pein', 'Heiliges Feuer' und 'Sühne' um 2% und verringert die Manakosten dieser Zauber um 3%.\n\n\n\nDunkler Prediger (Gedankenschinden)\n\nErhöht den Schaden Eurer regelmäßigen Schattenzauber um 1%."
        }, {
          "description" : "Beim Wirken Eurer Zauber 'Heilige Pein', 'Heiliges Feuer' oder 'Gedankenschinden' gewinnt Ihr den Effekt 'Prediger', der bis zu 5-mal stapelbar ist und 20 Sek. lang anhält.\n\n\n\nPrediger (Heilige Pein, Heiliges Feuer)\n\nErhöht den Schaden Eurer Zauber 'Heilige Pein', 'Heiliges Feuer' und 'Sühne' um 4% und verringert die Manakosten dieser Zauber um 6%.\n\n\n\nDunkler Prediger (Gedankenschinden)\n\nErhöht den Schaden Eurer regelmäßigen Schattenzauber um 2%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11608,
        "name" : "Erzengel",
        "icon" : "ability_priest_archangel",
        "x" : 1,
        "y" : 1,
        "req" : 8593,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "description" : "Zehrt Euren Effekt 'Prediger' auf und löst abhängig von der Art des aufgezehrten 'Prediger'-Effekts einen weiteren Effekt aus.\n\n\n\nErzengel (Prediger)\n\nStellt sofort 1% Eures gesamten Manas wieder her und erhöht Eure hervorgerufene Heilung pro Stapel um 3%. Hält 18 Sek. lang an. 30 Sek. Abklingzeit.\n\n\n\nDunkler Erzengel (Dunkler Prediger)\n\nStellt sofort 5% Eures gesamten Manas wieder her und erhöht den Schaden Eurer Zauber 'Gedankenschinden', 'Gedankenstachel', 'Gedankenschlag' und 'Schattenwort: Tod' pro Stapel um 4%. Hält 18 Sek. lang an. 90 Sek. Abklingzeit."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8581,
        "name" : "Inneres Sanktum",
        "icon" : "spell_holy_innerfire",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Während Euer Zauber 'Inneres Feuer' aktiv ist, wird zusätzlich Euer erlittener Zauberschaden um 2% verringert. Zudem wird der Bewegungstempobonus Eures Zaubers 'Innerer Wille' um 2% erhöht."
        }, {
          "description" : "Während Euer Zauber 'Inneres Feuer' aktiv ist, wird zusätzlich Euer erlittener Zauberschaden um 4% verringert. Zudem wird der Bewegungstempobonus Eures Zaubers 'Innerer Wille' um 4% erhöht."
        }, {
          "description" : "Während Euer Zauber 'Inneres Feuer' aktiv ist, wird zusätzlich Euer erlittener Zauberschaden um 6% verringert. Zudem wird der Bewegungstempobonus Eures Zaubers 'Innerer Wille' um 6% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8607,
        "name" : "Seelenwehr",
        "icon" : "ability_priest_soulwarding",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Machtwort: Schild' um 1 Sek."
        }, {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Machtwort: Schild' um 2 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11224,
        "name" : "Erneuerte Hoffnung",
        "icon" : "spell_holy_holyprotection",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht die Chance auf kritische Effekte Eurer Zauber 'Blitzheilung', 'Heilung', 'Große Heilung' und 'Sühne' (Heilung) um 5%, wenn das Ziel von 'Geschwächte Seele' oder von Eurem Effekt 'Barmherzigkeit' betroffen ist."
        }, {
          "description" : "Erhöht die Chance auf kritische Effekte Eurer Zauber 'Blitzheilung', 'Heilung', 'Große Heilung' und 'Sühne' (Heilung) um 10%, wenn das Ziel von 'Geschwächte Seele' oder von Eurem Effekt 'Barmherzigkeit' betroffen ist."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8611,
        "name" : "Seele der Macht",
        "icon" : "spell_holy_powerinfusion",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "cost" : "16% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Erfüllt das Ziel mit Macht, erhöht das Zaubertempo um 20% und senkt die Manakosten jeglicher Zauber um 20%. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11812,
        "name" : "Abbitte",
        "icon" : "ability_priest_atonement",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Heilige Pein' oder 'Heiliges Feuer' Schaden verursacht, wird sofort ein Verbündeter um 50% des verursachten Schadens geheilt, der sich innerhalb von 15 Metern um das Ziel befindet.\n\n\n\nWird der Zaubernde selbst durch 'Abbitte' geheilt, ist der Effekt des Zaubers halbiert."
        }, {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Heilige Pein' oder 'Heiliges Feuer' Schaden verursacht, wird sofort ein Verbündeter um 100% des verursachten Schadens geheilt, der sich innerhalb von 15 Metern um das Ziel befindet.\n\n\n\nWird der Zaubernde selbst durch 'Abbitte' geheilt, ist der Effekt des Zaubers halbiert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8591,
        "name" : "Innerer Fokus",
        "icon" : "spell_frost_windwalkon",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "45 Sek. Abklingzeit",
          "description" : "Verringert die Manakosten Eures nächsten Wirkens von 'Blitzheilung', 'Verbindende Heilung', 'Große Heilung' oder 'Gebet der Heilung' um 100% und erhöht die Chance auf einen kritischen Effekt um 25%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8617,
        "name" : "Euphorie",
        "icon" : "spell_holy_rapture",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn der Effekt Eures Zaubers 'Machtwort: Schild' gebannt oder durch absorbierten Schaden beendet wird, gewinnt Ihr sofort 2% Eures Gesamtmanas. Dieser Effekt kann nur ein Mal alle 12 Sek. auftreten."
        }, {
          "description" : "Wenn der Effekt Eures Zaubers 'Machtwort: Schild' gebannt oder durch absorbierten Schaden beendet wird, gewinnt Ihr sofort 5% Eures Gesamtmanas. Dieser Effekt kann nur ein Mal alle 12 Sek. auftreten."
        }, {
          "description" : "Wenn der Effekt Eures Zaubers 'Machtwort: Schild' gebannt oder durch absorbierten Schaden beendet wird, gewinnt Ihr sofort 7% Eures Gesamtmanas. Dieser Effekt kann nur ein Mal alle 12 Sek. auftreten."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11523,
        "name" : "Zeit schinden",
        "icon" : "spell_holy_borrowedtime",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Gewährt Eurem nächsten Zauber nach dem Wirken von 'Machtwort: Schild' 7% Zaubertempo. Hält 6 Sek. lang an."
        }, {
          "description" : "Gewährt Eurem nächsten Zauber nach dem Wirken von 'Machtwort: Schild' 14% Zaubertempo. Hält 6 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8605,
        "name" : "Reflektierender Schild",
        "icon" : "ability_priest_reflectiveshield",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "22% des Schadens, den Ihr durch 'Machtwort: Schild' absorbiert, werden auf den Angreifer zurückgeworfen. Dieser Schaden verursacht keine Bedrohung."
        }, {
          "description" : "45% des Schadens, den Ihr durch 'Machtwort: Schild' absorbiert, werden auf den Angreifer zurückgeworfen. Dieser Schaden verursacht keine Bedrohung."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11813,
        "name" : "Stärkung der Seele",
        "icon" : "spell_holy_ashestoashes",
        "x" : 0,
        "y" : 4,
        "req" : 11224,
        "ranks" : [ {
          "description" : "Wenn Ihr mittels 'Heilung', 'Blitzheilung' oder 'Große Heilung' ein Ziel heilt, welches vom Effekt 'Geschwächte Seele' betroffen ist, wird die verbleibende Dauer dieses Effekts um 2 Sek. verringert.\n\n\n\nZudem werdet Ihr, wenn Ihr 'Innerer Fokus' wirkt, 3 Sek. lang gegen Stille-, Unterbrechungs- und Magieentfernungseffekte immun."
        }, {
          "description" : "Wenn Ihr mittels 'Heilung', 'Blitzheilung' oder 'Große Heilung' ein Ziel heilt, welches vom Effekt 'Geschwächte Seele' betroffen ist, wird die verbleibende Dauer dieses Effekts um 4 Sek. verringert.\n\n\n\nZudem werdet Ihr, wenn Ihr 'Innerer Fokus' wirkt, 5 Sek. lang gegen Stille-, Unterbrechungs- und Magieentfernungseffekte immun."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8609,
        "name" : "Göttliche Aegis",
        "icon" : "spell_holy_devineaegis",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "description" : "Kritische Heilungen sowie alle Heilungen durch Euren Zauber 'Gebet der Heilung' hüllen das Ziel in einen schützenden Schild, der Schaden in Höhe von 10% des geheilten Wertes absorbiert. Hält 15 Sek. lang an."
        }, {
          "description" : "Kritische Heilungen sowie alle Heilungen durch Euren Zauber 'Gebet der Heilung' hüllen das Ziel in einen schützenden Schild, der Schaden in Höhe von 20% des geheilten Wertes absorbiert. Hält 15 Sek. lang an."
        }, {
          "description" : "Kritische Heilungen sowie alle Heilungen durch Euren Zauber 'Gebet der Heilung' hüllen das Ziel in einen schützenden Schild, der Schaden in Höhe von 30% des geheilten Wertes absorbiert. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8623,
        "name" : "Schmerzunterdrückung",
        "icon" : "spell_holy_painsupression",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "cost" : "8% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Verringert die Bedrohung eines befreundeten Ziels sofort um 5%, zudem wird sein erlittener Schaden 8 Sek. lang um 40% verringert."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 12183,
        "name" : "Gedankengang",
        "icon" : "ability_mage_studentofthemind",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "description" : "Beim Wirken von 'Große Heilung' besteht eine Chance von 50%, dass die Abklingzeit Eures Zaubers 'Innerer Fokus' um 5 Sek. verringert wird.\n\n\n\nBeim Wirken von 'Heilige Pein' besteht eine Chance von 50%, dass die Abklingzeit Eures Zaubers 'Sühne' um X,5 Sek. verringert wird."
        }, {
          "description" : "Beim Wirken von 'Große Heilung' besteht eine Chance von 100%, dass die Abklingzeit Eures Zaubers 'Innerer Fokus' um 5 Sek. verringert wird.\n\n\n\nBeim Wirken von 'Heilige Pein' besteht eine Chance von 100%, dass die Abklingzeit Eures Zaubers 'Sühne' um X,5 Sek. verringert wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8621,
        "name" : "Fokussierter Wille",
        "icon" : "ability_priest_focusedwill",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Wenn Ihr durch einen Angriff Schaden in Höhe von über 10% Eurer gesamten Gesundheit erleidet oder Opfer eines unregelmäßigen kritischen Treffers werdet, erhaltet Ihr den Effekt 'Fokussierter Wille'. Dieser Effekt verringert Euren erlittenen Schaden 8 Sek. lang um 5%. Bis zu 2-mal stapelbar."
        }, {
          "description" : "Wenn Ihr durch einen Angriff Schaden in Höhe von über 10% Eurer gesamten Gesundheit erleidet oder Opfer eines unregelmäßigen kritischen Treffers werdet, erhaltet Ihr den Effekt 'Fokussierter Wille'. Dieser Effekt verringert Euren erlittenen Schaden 8 Sek. lang um 10%. Bis zu 2-mal stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8625,
        "name" : "Barmherzigkeit",
        "icon" : "spell_holy_hopeandgrace",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Eure Zauber 'Blitzheilung', 'Heilung', 'Große Heilung' und 'Sühne' segnen das Ziel mit Barmherzigkeit, die seine durch den Priester erhaltene Heilung um 4% erhöht. Dieser Effekt ist bis zu 3-mal stapelbar. Hält 15 Sek. lang an."
        }, {
          "description" : "Eure Zauber 'Blitzheilung', 'Heilung', 'Große Heilung' und 'Sühne' segnen das Ziel mit Barmherzigkeit, die seine durch den Priester erhaltene Heilung um 8% erhöht. Dieser Effekt ist bis zu 3-mal stapelbar. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 8603,
        "name" : "Machtwort: Barriere",
        "icon" : "spell_holy_powerwordbarrier",
        "x" : 1,
        "y" : 6,
        "req" : 8609,
        "ranks" : [ {
          "cost" : "30% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Beschwört eine heilige Barriere im gewählten Gebiet, die jeglichen erlittenen Schaden aller befreundeten Ziele innerhalb ihres Bereiches um 25% verringert. Innerhalb der Barriere wird das Wirken von Zaubern durch erlittenen Schaden nicht unterbrochen. Die Barriere hält 10 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 0
    }, {
      "name" : "Heilig",
      "icon" : "spell_holy_guardianspirit",
      "backgroundFile" : "PriestHoly",
      "overlayColor" : "#9999ff",
      "description" : "Ein vielseitiger Heiler, der den von Gruppenmitgliedern oder Einzelnen erlittenen Schaden rückgängig machen und sogar noch nach seinem Tode heilen kann.",
      "treeNo" : 1,
      "roles" : {
        "tank" : false,
        "healer" : true,
        "dps" : false
      },
      "primarySpells" : [ {
        "spellId" : 88625,
        "name" : "Segenswort: Züchtigung",
        "icon" : "spell_holy_chastise",
        "cost" : "15% des Grundmanas",
        "range" : "30 Meter Reichweite",
        "castTime" : "Spontanzauber",
        "cooldown" : "30 Sek. Abklingzeit",
        "description" : "Züchtigt das Ziel, verursacht 656 Heiligschaden und macht es 3 Sek. lang desorientiert.",
        "id" : 88625,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 87336,
        "name" : "Spirituelle Heilung",
        "icon" : "spell_holy_impholyconcentration",
        "description" : "Heilung um 15% erhöht.",
        "id" : 87336,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 95861,
        "name" : "Meditation",
        "icon" : "spell_nature_sleep",
        "description" : "Lässt 50% Eurer durch Willenskraft erzeugten Manaregeneration weiterlaufen, während Ihr Euch im Kampf befindet.",
        "id" : 95861,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 33167,
        "name" : "Absolution",
        "icon" : "spell_holy_absolution",
        "description" : "Ermöglicht es Eurem Zauber 'Magiebannung', mit einem Wirken 2 schädliche Effekte von einem befreundeten Ziel zu bannen.",
        "id" : 33167,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77485,
        "name" : "Echo des Lichts",
        "icon" : "spell_holy_championsbond",
        "description" : "Eure direkten Heilzauber rufen im Verlauf von 6 Sek. eine Heilung von 10% hervor. Jeder Punkt Meisterschaft gewährt im Verlauf von 6 Sek. eine zusätzliche Heilung von 1.25%.",
        "id" : 77485,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 10746,
        "name" : "Verbesserte Erneuerung",
        "icon" : "spell_holy_renew",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den durch Euren Zauber 'Erneuerung' geheilten Wert um 5%."
        }, {
          "description" : "Erhöht den durch Euren Zauber 'Erneuerung' geheilten Wert um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9553,
        "name" : "Machtvolle Heilung",
        "icon" : "spell_holy_greaterheal",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Heilung Eurer Zauber 'Blitzheilung', 'Heilung', 'Verbindende Heilung' und 'Große Heilung' um 5%."
        }, {
          "description" : "Erhöht die Heilung Eurer Zauber 'Blitzheilung', 'Heilung', 'Verbindende Heilung' und 'Große Heilung' um 10%."
        }, {
          "description" : "Erhöht die Heilung Eurer Zauber 'Blitzheilung', 'Heilung', 'Verbindende Heilung' und 'Große Heilung' um 15%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9549,
        "name" : "Göttlicher Furor",
        "icon" : "spell_holy_sealofwrath",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit Eurer Zauber 'Heilige Pein', 'Heiliges Feuer', 'Heilung' und 'Große Heilung' um X,15 Sek."
        }, {
          "description" : "Verringert die Zauberzeit Eurer Zauber 'Heilige Pein', 'Heiliges Feuer', 'Heilung' und 'Große Heilung' um X,35 Sek."
        }, {
          "description" : "Verringert die Zauberzeit Eurer Zauber 'Heilige Pein', 'Heiliges Feuer', 'Heilung' und 'Große Heilung' um X,5 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11669,
        "name" : "Verzweifeltes Gebet",
        "icon" : "spell_holy_restoration",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "cost" : "18% des Grundmanas",
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Heilt den Zaubernden sofort um 30% seiner gesamten Gesundheit."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11765,
        "name" : "Woge des Lichts",
        "icon" : "spell_holy_surgeoflight",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Beim Wirken Eurer Zauber 'Heilige Pein', 'Heilung', 'Blitzheilung', 'Verbindende Heilung' oder 'Große Heilung' besteht eine Chance von 3%, dass Euer nächstes Wirken von 'Blitzheilung' zu einem kostenlosen Spontanzauber wird."
        }, {
          "description" : "Beim Wirken Eurer Zauber 'Heilige Pein', 'Heilung', 'Blitzheilung', 'Verbindende Heilung' oder 'Große Heilung' besteht eine Chance von 6%, dass Euer nächstes Wirken von 'Blitzheilung' zu einem kostenlosen Spontanzauber wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9561,
        "name" : "Inspiration",
        "icon" : "spell_holy_layonhands",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert den erlittenen körperlichen Schaden Eures Ziels 15 Sek. lang um 5%, nachdem dieses einen kritischen Effekt von Eurem Zauber 'Blitzheilung', 'Heilung', 'Große Heilung', 'Verbindende Heilung', 'Sühne', 'Gebet der Besserung', 'Gebet der Heilung' oder 'Kreis der Heilung' erhalten hat."
        }, {
          "description" : "Verringert den erlittenen körperlichen Schaden Eures Ziels 15 Sek. lang um 10%, nachdem dieses einen kritischen Effekt von Eurem Zauber 'Blitzheilung', 'Heilung', 'Große Heilung', 'Verbindende Heilung', 'Sühne', 'Gebet der Besserung', 'Gebet der Heilung' oder 'Kreis der Heilung' erhalten hat."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9593,
        "name" : "Göttliche Berührung",
        "icon" : "ability_paladin_infusionoflight",
        "x" : 0,
        "y" : 2,
        "req" : 10746,
        "ranks" : [ {
          "description" : "Euer Zauber 'Erneuerung' heilt das Ziel sofort um 5% seines gesamten regelmäßigen Effektes."
        }, {
          "description" : "Euer Zauber 'Erneuerung' heilt das Ziel sofort um 10% seines gesamten regelmäßigen Effektes."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9577,
        "name" : "Heilige Konzentration",
        "icon" : "spell_holy_fanaticism",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht Eure auf Willenskraft basierende Manaregeneration im Kampf um zusätzlich 15%."
        }, {
          "description" : "Erhöht Eure auf Willenskraft basierende Manaregeneration im Kampf um zusätzlich 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11666,
        "name" : "Lichtbrunnen",
        "icon" : "spell_holy_summonlightwell",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "cost" : "30% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "0,5 Sek. Zauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Erschafft einen heiligen Lichtbrunnen. Befreundete Spieler können den Lichtbrunnen anklicken, um im Verlauf von 6 Sek. insgesamt 10031 Gesundheit wiederherzustellen. Erlittener Schaden, der 30% Eurer gesamten Gesundheit entspricht, bricht den Effekt ab. Der Lichtbrunnen bleibt 3 Min. lang bestehen oder bis 10 Aufladungen verbraucht sind."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 12184,
        "name" : "Foliant des Lichts",
        "icon" : "inv_misc_book_07",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Segensworte um 15%."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Segensworte um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 14738,
        "name" : "Rapide Erneuerung",
        "icon" : "ability_paladin_blessedmending",
        "x" : 0,
        "y" : 3,
        "req" : 9593,
        "ranks" : [ {
          "description" : "Verringert die globale Abklingzeit Eures Zaubers 'Erneuerung' um X,5 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11670,
        "name" : "Geist der Erlösung",
        "icon" : "inv_enchant_essenceeternallarge",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Bei seinem Tod verwandelt sich der Priester 15 Sek. lang in einen Geist der Erlösung. Der Geist kann sich weder bewegen oder angreifen, noch kann er das Ziel von Zaubern oder Effekten sein. In dieser Gestalt kann der Priester jeglichen Heilzauber ohne irgendwelche Kosten wirken. Nach Ablauf der Verwandlung stirbt der Priester."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9573,
        "name" : "Glücksfall",
        "icon" : "spell_holy_serendipity",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn Ihr 'Verbindende Heilung' oder 'Blitzheilung' wirkt, wird die Zauberzeit Eures nächsten Wirkens von 'Große Heilung' oder 'Gebet der Heilung' um 10% verringert und die Manakosten um 5% reduziert. Bis zu 2-mal stapelbar. Hält 20 Sek. lang an."
        }, {
          "description" : "Wenn Ihr 'Verbindende Heilung' oder 'Blitzheilung' wirkt, wird die Zauberzeit Eures nächsten Wirkens von 'Große Heilung' oder 'Gebet der Heilung' um 20% verringert und die Manakosten um 10% reduziert. Bis zu 2-mal stapelbar. Hält 20 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9587,
        "name" : "Körper und Geist",
        "icon" : "spell_holy_symbolofhope",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Wenn Ihr 'Machtwort: Schild' oder 'Glaubenssprung' wirkt, wird das Bewegungstempo des Ziels 4 Sek. lang um 30% erhöht. Zudem besteht eine Chance von 50%, dass Ihr beim Wirken von 'Krankheit heilen' auf Euch selbst, zusätzlich zu den Krankheiten einen Gifteffekt entfernt."
        }, {
          "description" : "Wenn Ihr 'Machtwort: Schild' oder 'Glaubenssprung' wirkt, wird das Bewegungstempo des Ziels 4 Sek. lang um 60% erhöht. Zudem besteht eine Chance von 100%, dass Ihr beim Wirken von 'Krankheit heilen' auf Euch selbst zusätzlich zu den Krankheiten einen Gifteffekt entfernt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11667,
        "name" : "Chakra",
        "icon" : "spell_priest_chakra",
        "x" : 1,
        "y" : 4,
        "req" : 9577,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "30 Sek. Abklingzeit",
          "description" : "Bei Aktivierung stimmt Euch Euer nächstes Wirken von 'Heilung', 'Blitzheilung', 'Große Heilung', 'Verbindende Heilung', 'Gebet der Heilung', 'Gebet der Besserung', 'Gedankenstachel' oder 'Heilige Pein' auf ein entsprechendes Chakra ein.\n\n\n\nEpiphanie (Heilung, Blitzheilung, Große Heilung, Verbindende Heilung)\n\nErhöht die kritische Effektchance Eurer direkten Heilzauber um 10% und Eure direkten Heilungen setzen die Dauer eines wirkenden Effekts von 'Erneuerung' auf dem Ziel zurück.\n\n\n\nRefugium (Gebet der Heilung, Gebet der Besserung)\n\nErhöht die hervorgerufene Heilung Eurer Heilzauber mit Flächeneffekt sowie 'Erneuerung' um 15% und verringert die Abklingzeit von 'Kreis der Heilung' um 2 Sek.\n\n\n\nZüchtigung (Heilige Pein, Gedankenstachel)\n\nErhöht den gesamten verursachten Schaden Eurer Schatten- und Heiligzauber um 15%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11755,
        "name" : "Offenbarungen",
        "icon" : "ability_priest_bindingprayers",
        "x" : 2,
        "y" : 4,
        "req" : 11667,
        "ranks" : [ {
          "description" : "Während Ihr auf 'Chakra: Epiphanie' oder 'Chakra: Refugium' eingestimmt seid, wandelt sich Euer Zauber 'Segenswort: Züchtigung' in einen anderen Zauber um, dessen Art von Eurer Einstimmung abhängig ist.\n\n\n\nSegenswort: Epiphanie\n\nHeilt das Ziel sofort um 5695 und erhöht die Chance auf einen kritischen Effekt Eurer Heilzauber auf diesem Ziel 6 Sek. lang um 25%. 15 Sek. Abklingzeit.\n\n\n\nSegenswort: Refugium\n\nWeiht den Boden mit einem heiligen Licht, wodurch alle Verbündeten in diesem Bereich alle 2 Sek. um 331 geheilt werden. Hält 18 Sek. lang an. Es kann jeweils nur ein Refugium gleichzeitig aktiv sein. 40 Sek. Abklingzeit."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11672,
        "name" : "Gesegnete Abhärtung",
        "icon" : "spell_holy_blessedresillience",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "description" : "Wenn Ihr durch einen Angriff Schaden in Höhe von mindestens 10% Eurer gesamten Gesundheit erleidet oder Opfer eines unregelmäßigen kritischen Treffers werdet, erhaltet Ihr den Effekt 'Gesegnete Abhärtung'. Dieser Effekt erhöht Eure gesamte erhaltene Heilung 10 Sek. lang um 15%."
        }, {
          "description" : "Wenn Ihr durch einen Angriff Schaden in Höhe von mindestens 10% Eurer gesamten Gesundheit erleidet oder Opfer eines unregelmäßigen kritischen Treffers werdet, erhaltet Ihr den Effekt 'Gesegnete Abhärtung'. Dieser Effekt erhöht Eure gesamte erhaltene Heilung 10 Sek. lang um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9597,
        "name" : "Prüfung des Glaubens",
        "icon" : "spell_holy_testoffaith",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht die erhaltene Heilung von befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 4%."
        }, {
          "description" : "Erhöht die erhaltene Heilung von befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 8%."
        }, {
          "description" : "Erhöht die erhaltene Heilung von befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 12%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11668,
        "name" : "Himmlische Stimme",
        "icon" : "ability_priest_heavanlyvoice",
        "x" : 1,
        "y" : 5,
        "req" : 11667,
        "ranks" : [ {
          "description" : "Erhöht die hervorgerufene Heilung Eures Zaubers 'Gotteshymne' um 50% und verringert die Abklingzeit des Zaubers um 2,5 Min."
        }, {
          "description" : "Erhöht die hervorgerufene Heilung Eures Zaubers 'Gotteshymne' um 100% und verringert die Abklingzeit des Zaubers um 5 Min."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9595,
        "name" : "Kreis der Heilung",
        "icon" : "spell_holy_circleofrenewal",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "cost" : "21% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "10 Sek. Abklingzeit",
          "description" : "Heilt bis zu 5 befreundete Gruppen- oder Schlachtzugsmitglieder im Umkreis von X Metern um das Ziel um 2454. Die Heilung der am schwersten verletzten Gruppenmitglieder wird priorisiert."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9601,
        "name" : "Schutzgeist",
        "icon" : "spell_holy_guardianspirit",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "6% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Ruft einen Schutzgeist herbei, der ein freundliches Ziel bewacht. Der Geist erhöht die erhaltene Heilung des Ziels um 60% und verhindert, dass das Ziel stirbt, indem er sich selbst opfert. Dieses Opfer beendet den Effekt, heilt das Ziel jedoch um 50% seiner maximalen Gesundheit. Hält 10 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 1
    }, {
      "name" : "Schatten",
      "icon" : "spell_shadow_shadowwordpain",
      "backgroundFile" : "PriestShadow",
      "overlayColor" : "#b266cc",
      "description" : "Nutzt sinistre Schattenmagien, um Feinde durch Schaden über Zeit auszulöschen.",
      "treeNo" : 2,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 15407,
        "name" : "Gedankenschinden",
        "icon" : "spell_shadow_siphonmana",
        "cost" : "8% des Grundmanas",
        "range" : "40 Meter Reichweite",
        "castTime" : "Kanalisiert",
        "description" : "Greift die Gedanken des Ziels mit Schattenenergie an. Verursacht im Verlauf von 3 Sek. 645 Schattenschaden und verringert das Bewegungstempo des Ziels um 50%.",
        "id" : 15407,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 87327,
        "name" : "Schattenmacht",
        "icon" : "spell_shadow_shadowpower",
        "description" : "Erhöht den Zauberschaden um 15%.\n\n\n\nErhöht den kritischen Schaden Eurer Schattenzauber um 100%.",
        "id" : 87327,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 95740,
        "name" : "Schattenkugeln",
        "icon" : "spell_priest_shadoworbs",
        "description" : "Jedes Mal, wenn Eure Zauber 'Schattenwort: Schmerz' oder 'Gedankenschinden' Schaden verursachen, besteht eine Chance von 10%, dass Euch eine Schattenkugel gewährt wird. Das Wirken Eurer Zauber 'Gedankenschlag' oder 'Gedankenstachel' zehrt alle Schattenkugeln auf und erhöht den Schaden dieser Zauber pro aufgezehrter Kugel um 10%. Zudem wird Euer verursachter regelmäßiger Schattenschaden 15 Sek. lang um 10% erhöht. Ihr könnt maximal 3 Kugeln ansammeln.",
        "id" : 95740,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77486,
        "name" : "Schattenkugelmacht",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht den von Euren Schattenkugeln verursachten Schaden um 11.6%. Jeder Punkt Meisterschaft erhöht den Schaden um zusätzlich 1.45%.",
        "id" : 77486,
        "classMask" : 16,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 9032,
        "name" : "Dunkelheit",
        "icon" : "ability_priest_darkness",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Zaubertempo um 1% erhöht."
        }, {
          "description" : "Zaubertempo um 2% erhöht."
        }, {
          "description" : "Zaubertempo um 3% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9036,
        "name" : "Verbessertes Schattenwort: Schmerz",
        "icon" : "spell_shadow_shadowwordpain",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eures Zaubers 'Schattenwort: Schmerz' um 3%."
        }, {
          "description" : "Erhöht den Schaden Eures Zaubers 'Schattenwort: Schmerz' um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9046,
        "name" : "Schattenschleier",
        "icon" : "spell_magic_lesserinvisibilty",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Verblassen' um 3 Sek. und verringert die Abklingzeit Eures Schattengeistes um 30 Sek."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Verblassen' um 6 Sek. und verringert die Abklingzeit Eures Schattengeistes um 60 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9040,
        "name" : "Verbesserter psychischer Schrei",
        "icon" : "spell_shadow_psychicscream",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Psychischer Schrei' um 2 Sek."
        }, {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Psychischer Schrei' um 4 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9042,
        "name" : "Verbesserter Gedankenschlag",
        "icon" : "spell_shadow_unholyfrenzy",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um X,5 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 33%, die erhaltene Heilung des Ziels 10 Sek. lang um 10% zu verringern."
        }, {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 1 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 66%, die erhaltene Heilung des Ziels 10 Sek. lang um 10% zu verringern."
        }, {
          "description" : "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 1,5 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 100%, die erhaltene Heilung des Ziels 10 Sek. lang um 10% zu verringern."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9062,
        "name" : "Verbesserte verschlingende Seuche",
        "icon" : "spell_shadow_devouringplague.",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Euer Zauber 'Verschlingende Seuche' verursacht sofort Schaden in Höhe von 15% seines gesamten regelmäßigen Effekts."
        }, {
          "description" : "Euer Zauber 'Verschlingende Seuche' verursacht sofort Schaden in Höhe von 30% seines gesamten regelmäßigen Effekts."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11673,
        "name" : "Okkultismus",
        "icon" : "spell_shadow_mindtwisting",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht Euren Schattenzauberschaden um 1% und gewährt Euch Zaubertrefferwertung in Höhe von 50% Eurer durch Gegenstände und Effekte erhaltenen Willenskraft."
        }, {
          "description" : "Erhöht Euren Schattenzauberschaden um 2% und gewährt Euch Zaubertrefferwertung in Höhe von 100% Eurer durch Gegenstände und Effekte erhaltenen Willenskraft."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9064,
        "name" : "Schattengestalt",
        "icon" : "spell_shadow_shadowform",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "cost" : "13% des Grundmanas",
          "castTime" : "Spontanzauber",
          "cooldown" : "1,5 Sek. Abklingzeit",
          "description" : "Ihr nehmt eine Schattengestalt an, erhöht Euren Schattenschaden um 15% und verringert Euren erlittenen Schaden um 15%. Zudem erhöht Ihr die Zaubertempowertung aller Gruppen- und Schlachtzugsmitglieder um 5%. Ihr könnt in dieser Gestalt jedoch keine Heiligzauber wirken."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9068,
        "name" : "Trugbild",
        "icon" : "spell_shadow_twistedfaith",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Das Wirken Eurer Fähigkeit 'Verblassen' hat nun eine Chance von 50%, jegliche bewegungseinschränkenden Effekte zu entfernen."
        }, {
          "description" : "Das Wirken Eurer Fähigkeit 'Verblassen' hat nun eine Chance von 100%, jegliche bewegungseinschränkenden Effekte zu entfernen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11606,
        "name" : "Dienstbare Schatten",
        "icon" : "inv_misc_orb_04",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht Eure Chance, eine Schattenkugel zu erhalten, wenn Ihr mit Euren Zaubern 'Gedankenschinden' oder 'Schattenwort: Schmerz' Schaden verursacht, um 4%. Zudem erhaltet Ihr eine Chance von 50%, eine Schattenkugel zu erhalten, wenn Ihr einen kritischen Treffer erleidet."
        }, {
          "description" : "Erhöht Eure Chance, eine Schattenkugel zu erhalten, wenn Ihr mit Euren Zaubern 'Gedankenschinden' oder 'Schattenwort: Schmerz' Schaden verursacht, um 8%. Zudem erhaltet Ihr eine Chance von 100%, eine Schattenkugel zu erhalten, wenn Ihr einen kritischen Treffer erleidet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9052,
        "name" : "Stille",
        "icon" : "ability_priest_silence",
        "x" : 0,
        "y" : 3,
        "req" : 9040,
        "ranks" : [ {
          "cost" : "225 Mana",
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "45 Sek. Abklingzeit",
          "description" : "Bringt das Ziel zum Schweigen, sodass es 5 Sek. lang keine Zauber wirken kann. Das Zauberwirken von Nichtspieler-Zielen ist ebenfalls 3 Sek. lang unterbrochen."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9054,
        "name" : "Vampirumarmung",
        "icon" : "spell_shadow_unsummonbuilding",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "description" : "Die Umarmung der Schattenenergie umschlingt Euch. Wenn Ihr mittels Eurer Einzelzielzauber Schaden verursacht, werdet Ihr um 6% und Eure Gruppenmitglieder um 3% des von Euch verursachten Schattenschadens geheilt."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11778,
        "name" : "Masochismus",
        "icon" : "spell_shadow_misery",
        "x" : 2,
        "y" : 3,
        "req" : 9054,
        "ranks" : [ {
          "description" : "Wenn Ihr durch einen Angriff Schaden erleidet, der 10% Eurer gesamten Gesundheit oder mehr entspricht, oder Euch selbst durch Euren Zauber 'Schattenwort: Tod' Schaden zufügt, gewinnt Ihr sofort 5% Eures gesamten Manas zurück."
        }, {
          "description" : "Wenn Ihr durch einen Angriff Schaden erleidet, der 10% Eurer gesamten Gesundheit oder mehr entspricht, oder Euch selbst durch Euren Zauber 'Schattenwort: Tod' Schaden zufügt, gewinnt Ihr sofort 10% Eures gesamten Manas zurück."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9060,
        "name" : "Gedankenschmelze",
        "icon" : "spell_shadow_skull",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eures Zaubers 'Schattenwort: Tod' bei Zielen, die über 25% Gesundheit oder weniger verfügen, um 15%. Zudem wird, wenn Ihr mittels 'Gedankenstachel' Schaden verursacht, die Zauberzeit Eures nächsten Wirkens von 'Gedankenschlag' 6 Sek. lang um 25% verringert. Der Effekt 'Gedankenschmelze' ist bis zu 2-mal stapelbar."
        }, {
          "description" : "Erhöht den Schaden Eures Zaubers 'Schattenwort: Tod' bei Zielen, die über 25% Gesundheit oder weniger verfügen, um 30%. Zudem wird, wenn Ihr mittels 'Gedankenstachel' Schaden verursacht, die Zauberzeit Eures nächsten Wirkens von 'Gedankenschlag' 6 Sek. lang um 50% verringert. Der Effekt 'Gedankenschmelze' ist bis zu 2-mal stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9076,
        "name" : "Schmerz und Leid",
        "icon" : "spell_shadow_painandsuffering",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Euer Zauber 'Gedankenschinden' hat eine Chance von 30%, die Dauer von 'Schattenwort: Schmerz' auf dem Ziel zu erneuern, zudem wird der von Euch durch Euer 'Schattenwort: Tod' erlittene Schaden um 20% verringert."
        }, {
          "description" : "Euer Zauber 'Gedankenschinden' hat eine Chance von 60%, die Dauer von 'Schattenwort: Schmerz' auf dem Ziel zu erneuern, zudem wird der von Euch durch Euer 'Schattenwort: Tod' erlittene Schaden um 40% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9074,
        "name" : "Vampirberührung",
        "icon" : "spell_holy_stoicism",
        "x" : 1,
        "y" : 4,
        "req" : 9054,
        "ranks" : [ {
          "cost" : "16% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "1,5 Sek. Zauber",
          "description" : "Fügt dem Ziel im Verlauf von 15 Sek. insgesamt 645 Schattenschaden zu. Wenn Ihr Zielen, die vom Effekt dieses Zaubers betroffen sind, mittels 'Gedankenschlag' Schaden zufügt, gewährt er bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 10 Sek. 1% ihres maximalen Manas."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11663,
        "name" : "Paralyse",
        "icon" : "ability_rogue_masterofsubtlety",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Wenn Euer Zauber 'Gedankenschlag' einen kritischen Treffer erzielt, wird das Ziel 2 Sek. lang bewegungsunfähig."
        }, {
          "description" : "Wenn Euer Zauber 'Gedankenschlag' einen kritischen Treffer erzielt, wird das Ziel 4 Sek. lang bewegungsunfähig."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9072,
        "name" : "Psychisches Entsetzen",
        "icon" : "spell_shadow_psychichorrors",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "cost" : "16% des Grundmanas",
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Ihr versetzt das Ziel 3 Sek. lang in Angst und Schrecken, sodass es vor Furcht erzittert und für 10 Sek. seine Waffenhand- und Distanzwaffe fallen lässt."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11605,
        "name" : "Sünde und Strafe",
        "icon" : "spell_holy_prayerofshadowprotection",
        "x" : 1,
        "y" : 5,
        "req" : 9074,
        "ranks" : [ {
          "description" : "Wenn Euer Zauber 'Vampirberührung' gebannt wird, besteht eine Chance von 50%, dass sowohl der Bannende als auch alle feindlichen Ziele im Umkreis von 6 Metern um ihn herum sofort 3 Sek. lang von Schrecken erfüllt flüchten.\n\n\n\nWenn Euer Zauber 'Gedankenschinden' einen kritischen Treffer erzielt, wird die verbleibende Abklingzeit Eures Schattengeists um 5 Sek. verringert."
        }, {
          "description" : "Wenn Euer Zauber 'Vampirberührung' gebannt wird, besteht eine Chance von 100%, dass sowohl der Bannende als auch alle feindlichen Ziele im Umkreis von 6 Metern um ihn herum sofort 3 Sek. lang von Schrecken erfüllt flüchten.\n\n\n\nWenn Euer Zauber 'Gedankenschinden' einen kritischen Treffer erzielt, wird die verbleibende Abklingzeit Eures Schattengeists um 10 Sek. verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9070,
        "name" : "Schattenhafte Erscheinung",
        "icon" : "ability_priest_shadowyapparition",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Schattenwort: Schmerz' regelmäßigen Schaden verursacht, besteht eine Chance von 4%, dass ein Schatten Eurer selbst beschworen wird, der sich langsam auf das Ziel zubewegt, welches vom Effekt Eures Zaubers 'Schattenwort: Schmerz' betroffen ist. Sobald die Erscheinung das Ziel erreicht, wird sie sofort 535 Schattenschaden verursachen.\n\n\n\nWährend Ihr Euch bewegt, wird die Chance, eine schattenhafte Erscheinung zu beschwören, auf 20% erhöht. Es können maximal 4 schattenhafte Erscheinungen gleichzeitig aktiv sein."
        }, {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Schattenwort: Schmerz' regelmäßigen Schaden verursacht, besteht eine Chance von 8%, dass ein Schatten Eurer selbst beschworen wird, der sich langsam auf das Ziel zubewegt, welches vom Effekt Eures Zaubers 'Schattenwort: Schmerz' betroffen ist. Sobald die Erscheinung das Ziel erreicht, wird sie sofort 535 Schattenschaden verursachen.\n\n\n\nWährend Ihr Euch bewegt, wird die Chance, eine schattenhafte Erscheinung zu beschwören, auf 40% erhöht. Es können maximal 4 schattenhafte Erscheinungen gleichzeitig aktiv sein."
        }, {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Schattenwort: Schmerz' regelmäßigen Schaden verursacht, besteht eine Chance von 12%, dass ein Schatten Eurer selbst beschworen wird, der sich langsam auf das Ziel zubewegt, welches vom Effekt Eures Zaubers 'Schattenwort: Schmerz' betroffen ist. Sobald die Erscheinung das Ziel erreicht, wird sie sofort 535 Schattenschaden verursachen.\n\n\n\nWährend Ihr Euch bewegt, wird die Chance, eine schattenhafte Erscheinung zu beschwören, auf 60% erhöht. Es können maximal 4 schattenhafte Erscheinungen gleichzeitig aktiv sein."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 9080,
        "name" : "Dispersion",
        "icon" : "spell_shadow_dispersion",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Löst Euch zu reiner Schattenenergie auf, wodurch Euer gesamter erlittener Schaden um 90% verringert wird. Ihr könnt weder angreifen noch Zauber wirken, regeneriert jedoch 6 Sek. lang alle 1 Sek. 6% Mana.\n\n\n\n'Dispersion' kann gewirkt werden, während Betäubungs-, Furcht- oder Stilleeffekte auf Euch wirken. Bei Anwendung werden alle wirkenden Verlangsamungs- und bewegungseinschränkenden Effekte entfernt und Ihr werdet für die Dauer des Effektes von 'Dispersion' gegen sie immun."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 2
    } ]
  },
  "glyphs" : [ {
    "name" : "Glyphe 'Kreis der Heilung'",
    "id" : 251,
    "type" : 0,
    "description" : "Euer Zauber 'Kreis der Heilung' heilt 1 zusätzliches Ziel, seine Manakosten werden jedoch um 20% erhöht.",
    "icon" : "spell_holy_circleofrenewal",
    "itemId" : 42396,
    "spellKey" : 55675,
    "spellId" : 55675,
    "prettyName" : "Kreis der Heilung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Magiebannung'",
    "id" : 252,
    "type" : 0,
    "description" : "Euer Zauber 'Magiebannung' heilt Euer Ziel zusätzlich um 3% seiner maximalen Gesundheit, wenn erfolgreich ein magischer Schwächungszauber gebannt wird.",
    "icon" : "spell_holy_dispelmagic",
    "itemId" : 42397,
    "spellKey" : 55677,
    "spellId" : 55677,
    "prettyName" : "Magiebannung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schleier'",
    "id" : 253,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Verblassen' um 9 Sek.",
    "icon" : "spell_magic_lesserinvisibilty",
    "itemId" : 42398,
    "spellKey" : 55684,
    "spellId" : 55684,
    "prettyName" : "Schleier",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Furchtzauberschutz'",
    "id" : 254,
    "type" : 0,
    "description" : "Verringert die Abklingzeit und Dauer Eures Zaubers 'Furchtzauberschutz' um 60 Sek.",
    "icon" : "spell_holy_excorcism",
    "itemId" : 42399,
    "spellKey" : 55678,
    "spellId" : 55678,
    "prettyName" : "Furchtzauberschutz",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Blitzheilung'",
    "id" : 255,
    "type" : 2,
    "description" : "Erhöht die Chance auf einen kritischen Effekt Eures Zaubers 'Blitzheilung' bei Zielen, die über 25% Gesundheit oder weniger verfügen, um 10%.",
    "icon" : "spell_holy_flashheal",
    "itemId" : 42400,
    "spellKey" : 55679,
    "spellId" : 55679,
    "prettyName" : "Blitzheilung",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Heilige Nova'",
    "id" : 256,
    "type" : 0,
    "description" : "Verringert die globale Abklingzeit Eures Zaubers 'Heilige Nova' um X,5 Sek.",
    "icon" : "spell_holy_holynova",
    "itemId" : 42401,
    "spellKey" : 55683,
    "spellId" : 55683,
    "prettyName" : "Heilige Nova",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Inneres Feuer'",
    "id" : 257,
    "type" : 0,
    "description" : "Erhöht die von Eurem Zauber 'Inneres Feuer' gewährte Rüstung um 50%.",
    "icon" : "spell_holy_innerfire",
    "itemId" : 42402,
    "spellKey" : 55686,
    "spellId" : 55686,
    "prettyName" : "Inneres Feuer",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Lichtbrunnen'",
    "id" : 258,
    "type" : 2,
    "description" : "Erhöht die gesamten Aufladungen Eures Lichtbrunnens um 5.",
    "icon" : "spell_holy_summonlightwell",
    "itemId" : 42403,
    "spellKey" : 55673,
    "spellId" : 55673,
    "prettyName" : "Lichtbrunnen",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Massenbannung'",
    "id" : 259,
    "type" : 0,
    "description" : "Verringert die Zauberzeit Eures Zaubers 'Massenbannung' um 1 Sek.",
    "icon" : "spell_arcane_massdispel",
    "itemId" : 42404,
    "spellKey" : 55691,
    "spellId" : 55691,
    "prettyName" : "Massenbannung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Psychisches Entsetzen'",
    "id" : 260,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Psychisches Entsetzen' um 30 Sek.",
    "icon" : "spell_shadow_psychichorrors",
    "itemId" : 42405,
    "spellKey" : 55688,
    "spellId" : 55688,
    "prettyName" : "Psychisches Entsetzen",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schattenwort: Schmerz'",
    "id" : 261,
    "type" : 2,
    "description" : "Erhöht den regelmäßigen Schaden Eures Zaubers 'Schattenwort: Schmerz' um 10%.",
    "icon" : "spell_shadow_shadowwordpain",
    "itemId" : 42406,
    "spellKey" : 55681,
    "spellId" : 55681,
    "prettyName" : "Schattenwort: Schmerz",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Machtwort: Barriere'",
    "id" : 262,
    "type" : 2,
    "description" : "Erhöht die im Effektbereich von 'Machtwort: Barriere' erhaltene Heilung um 10%.",
    "icon" : "spell_holy_powerwordbarrier",
    "itemId" : 42407,
    "spellKey" : 55689,
    "spellId" : 55689,
    "prettyName" : "Machtwort: Barriere",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Machtwort: Schild'",
    "id" : 263,
    "type" : 2,
    "description" : "Euer Zauber 'Machtwort: Schild' heilt das Ziel zusätzlich um 20% des absorbierten Wertes.",
    "icon" : "spell_holy_powerwordshield",
    "itemId" : 42408,
    "spellKey" : 55672,
    "spellId" : 55672,
    "prettyName" : "Machtwort: Schild",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Gebet der Heilung'",
    "id" : 264,
    "type" : 2,
    "description" : "Euer Zauber 'Gebet der Heilung' heilt im Verlauf von 6 Sek. um zusätzliche 20% der Initialheilung.",
    "icon" : "spell_holy_prayerofhealing02",
    "itemId" : 42409,
    "spellKey" : 55680,
    "spellId" : 55680,
    "prettyName" : "Gebet der Heilung",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Psychischer Schrei'",
    "id" : 265,
    "type" : 0,
    "description" : "Lässt die Ziele Eures Zaubers 'Psychischer Schrei' vor Furcht erstarren, anstatt dass sie flüchten, erhöht jedoch die Abklingzeit von 'Psychischer Schrei' um 3 Sek.",
    "icon" : "spell_shadow_psychicscream",
    "itemId" : 42410,
    "spellKey" : 55676,
    "spellId" : 55676,
    "prettyName" : "Psychischer Schrei",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Erneuerung'",
    "id" : 266,
    "type" : 2,
    "description" : "Erhöht den durch Euren Zauber 'Erneuerung' geheilten Wert um zusätzliche 10%.",
    "icon" : "spell_holy_renew",
    "itemId" : 42411,
    "spellKey" : 55674,
    "spellId" : 55674,
    "prettyName" : "Erneuerung",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Geißelgefangennahme'",
    "id" : 267,
    "type" : 0,
    "description" : "Verringert die Zauberzeit Eures Zaubers 'Untote fesseln' um 1.0 Sek.",
    "icon" : "spell_holy_crusade",
    "itemId" : 42412,
    "spellKey" : 55690,
    "spellId" : 55690,
    "prettyName" : "Geißelgefangennahme",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schattenwort: Tod'",
    "id" : 268,
    "type" : 2,
    "description" : "Wenn Euer Zauber 'Schattenwort: Tod' ein Ziel nicht tötet, das über 25% Gesundheit oder weniger verfügt, wird die Abklingzeit von 'Schattenwort: Tod' sofort abgeschlossen. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten.",
    "icon" : "spell_shadow_demonicfortitude",
    "itemId" : 42414,
    "spellKey" : 55682,
    "spellId" : 55682,
    "prettyName" : "Schattenwort: Tod",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Gedankenschinden'",
    "id" : 269,
    "type" : 2,
    "description" : "Erhöht den verursachten Schaden Eures Zaubers 'Gedankenschinden' um 10%.",
    "icon" : "spell_shadow_siphonmana",
    "itemId" : 42415,
    "spellKey" : 55687,
    "spellId" : 55687,
    "prettyName" : "Gedankenschinden",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Heilige Pein'",
    "id" : 270,
    "type" : 0,
    "description" : "Euer Zauber 'Heilige Pein' verursacht 20% zusätzlichen Schaden, wenn das Ziel vom Effekt Eures Zaubers 'Heiliges Feuer' betroffen ist.",
    "icon" : "spell_holy_holysmite",
    "itemId" : 42416,
    "spellKey" : 55692,
    "spellId" : 55692,
    "prettyName" : "Heilige Pein",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Gebet der Besserung'",
    "id" : 271,
    "type" : 0,
    "description" : "Das erste Ziel Eures Zaubers 'Gebet der Besserung' wird um zusätzliche 60% geheilt.",
    "icon" : "spell_holy_prayerofmendingtga",
    "itemId" : 42417,
    "spellKey" : 55685,
    "spellId" : 55685,
    "prettyName" : "Gebet der Besserung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Verblassen'",
    "id" : 458,
    "type" : 1,
    "description" : "Verringert die Manakosten Eures Zaubers 'Verblassen' um 30%.",
    "icon" : "ability_mage_invisibility",
    "itemId" : 43342,
    "spellKey" : 57985,
    "spellId" : 57985,
    "prettyName" : "Verblassen",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Levitieren'",
    "id" : 459,
    "type" : 1,
    "description" : "Für den Zauber 'Levitieren' werden keine Reagenzien mehr benötigt.",
    "icon" : "spell_holy_layonhands",
    "itemId" : 43370,
    "spellKey" : 57987,
    "spellId" : 57987,
    "prettyName" : "Levitieren",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Seelenstärke'",
    "id" : 460,
    "type" : 1,
    "description" : "Verringert die Manakosten Eures Zaubers 'Machtwort: Seelenstärke' um 50%.",
    "icon" : "spell_holy_wordfortitude",
    "itemId" : 43371,
    "spellKey" : 58009,
    "spellId" : 58009,
    "prettyName" : "Seelenstärke",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Untote fesseln'",
    "id" : 461,
    "type" : 1,
    "description" : "Erhöht die Reichweite Eures Zaubers 'Untote fesseln' um 5 Meter.",
    "icon" : "spell_nature_slow",
    "itemId" : 43373,
    "spellKey" : 57986,
    "spellId" : 57986,
    "prettyName" : "Untote fesseln",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Schattenschutz'",
    "id" : 462,
    "type" : 1,
    "description" : "Erhöht die Dauer Eures Zaubers 'Schattenschutz' um 10 Min.",
    "icon" : "spell_holy_prayerofshadowprotection",
    "itemId" : 43372,
    "spellKey" : 58015,
    "spellId" : 58015,
    "prettyName" : "Schattenschutz",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Schattengeist'",
    "id" : 463,
    "type" : 1,
    "description" : "Ihr erhaltet 5% Eures maximalen Manas, wenn Euer Schattengeist durch erlittenen Schaden stirbt.",
    "icon" : "spell_shadow_shadowfiend",
    "itemId" : 43374,
    "spellKey" : 58228,
    "spellId" : 58228,
    "prettyName" : "Schattengeist",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Dispersion'",
    "id" : 708,
    "type" : 2,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Dispersion' um 45 Sek.",
    "icon" : "spell_shadow_dispersion",
    "itemId" : 45753,
    "spellKey" : 63229,
    "spellId" : 63229,
    "prettyName" : "Dispersion",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Schutzgeist'",
    "id" : 709,
    "type" : 2,
    "description" : "Verringert die Abklingzeit Eurer Fähigkeit 'Schutzgeist' um 30 Sek.",
    "icon" : "spell_holy_guardianspirit",
    "itemId" : 45755,
    "spellKey" : 63231,
    "spellId" : 63231,
    "prettyName" : "Schutzgeist",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Sühne'",
    "id" : 710,
    "type" : 2,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Sühne' um 2 Sek.",
    "icon" : "spell_holy_penance",
    "itemId" : 45756,
    "spellKey" : 63235,
    "spellId" : 63235,
    "prettyName" : "Sühne",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Willensentzug'",
    "id" : 711,
    "type" : 0,
    "description" : "Wenn Ihr mittels Eures Zaubers 'Schattenwort: Tod' ein Ziel tötet, welches Erfahrung oder Ehre gewährt, regeneriert Ihr im Verlauf von 12 Sek. 12% Eures gesamten Manas.",
    "icon" : "spell_shadow_requiem",
    "itemId" : 45757,
    "spellKey" : 63237,
    "spellId" : 63237,
    "prettyName" : "Willensentzug",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Göttliche Präzision'",
    "id" : 712,
    "type" : 0,
    "description" : "Erhöht die Trefferchance Eurer Zauber 'Heilige Pein' und 'Heiliges Feuer' um 18%.",
    "icon" : "spell_holy_holyprotection",
    "itemId" : 45758,
    "spellKey" : 63246,
    "spellId" : 63246,
    "prettyName" : "Göttliche Präzision",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Verzweiflung'",
    "id" : 713,
    "type" : 0,
    "description" : "Ermöglicht es, dass Eure Zauber 'Schmerzunterdrückung' und 'Schutzgeist' aktiviert werden können, während Betäubungseffekte auf Euch wirken.",
    "icon" : "spell_holy_painsupression",
    "itemId" : 45760,
    "spellKey" : 63248,
    "spellId" : 63248,
    "prettyName" : "Verzweiflung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schatten'",
    "id" : 961,
    "type" : 1,
    "description" : "Verändert das Aussehen Eurer Schattengestalt.",
    "icon" : "spell_shadow_shadowform",
    "itemId" : 77101,
    "spellKey" : 107906,
    "spellId" : 107906,
    "prettyName" : "Schatten",
    "typeOrder" : 0
  } ]
}