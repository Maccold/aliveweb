{
  "talentData" : {
    "characterClass" : {
      "classId" : 9,
      "name" : "Hexenmeister",
      "powerType" : "MANA",
      "powerTypeId" : 0,
      "powerTypeSlug" : "mana"
    },
    "talentTrees" : [ {
      "name" : "Gebrechen",
      "icon" : "spell_shadow_deathcoil",
      "backgroundFile" : "WarlockCurses",
      "overlayColor" : "#00ff99",
      "description" : "Ein Meister der Schattenmagie, dessen Spezialität es sowohl ist, im Lauf der Zeit Schaden zuzufügen, als auch Furcht- und Schwächungszauber zu wirken.",
      "treeNo" : 0,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 30108,
        "name" : "Instabiles Gebrechen",
        "icon" : "spell_shadow_unstableaffliction_3",
        "cost" : "15% des Grundmanas",
        "range" : "40 Meter Reichweite",
        "castTime" : "1,5 Sek. Zauber",
        "description" : "Schattenenergie zerstört langsam das Ziel, 15 Sek. lang werden insgesamt 1210 Schaden verursacht. Sollte 'Instabiles Gebrechen' gebannt werden, erleidet der Bannende 2178 Schaden und wird 4 Sek. lang zum Schweigen gebracht. Es kann pro Hexenmeister jeweils nur ein Effekt von 'Feuerbrand' oder 'Instabiles Gebrechen' gleichzeitig auf einem beliebigen Ziel aktiv sein.",
        "id" : 30108,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 87339,
        "name" : "Schattenbeherrschung",
        "icon" : "spell_shadow_spectralsight",
        "description" : "Erhöht den Schaden von Schattenzaubern um 30%.",
        "id" : 87339,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77215,
        "name" : "Potente Gebrechen",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht jeglichen von Euch verursachten regelmäßigen Schattenschaden um 13%. Jeder Punkt Meisterschaft erhöht den regelmäßigen Schattenschaden um zusätzlich 1.63%.",
        "id" : 77215,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 11100,
        "name" : "Finstere Omen",
        "icon" : "spell_shadow_curseofsargeras",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die kritische Trefferchance Eurer Zauber 'Omen der Pein' und 'Omen der Verdammnis' um 4%."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eurer Zauber 'Omen der Pein' und 'Omen der Verdammnis' um 8%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11110,
        "name" : "Verbesserter Aderlass",
        "icon" : "spell_shadow_burningspirit",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die durch Euren Zauber 'Aderlass' gewährte Manamenge um 10%."
        }, {
          "description" : "Erhöht die durch Euren Zauber 'Aderlass' gewährte Manamenge um 20%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11104,
        "name" : "Verbesserte Verderbnis",
        "icon" : "spell_shadow_abominationexplosion",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 4%."
        }, {
          "description" : "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 8%."
        }, {
          "description" : "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 12%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11214,
        "name" : "Unglücksbringer",
        "icon" : "ability_warlock_jinx",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Euer Zauber 'Fluch der Elemente' breitet sich auch auf bis zu 15 feindliche Ziele im Umkreis von 20 Metern um das verfluchte Ziel aus.\n\n\n\nZudem erhöht Euer Zauber 'Fluch der Schwäche' zusätzlich die Energie-, Fokus-, Wut- oder Runenmachtkosten von Fähigkeiten betroffener Ziele um 5%."
        }, {
          "description" : "Euer Zauber 'Fluch der Elemente' breitet sich auch auf bis zu 15 feindliche Ziele im Umkreis von 40 Metern um das verfluchte Ziel aus.\n\n\n\nZudem erhöht Euer Zauber 'Fluch der Schwäche' zusätzlich die Energie-, Fokus-, Wut- oder Runenmachtkosten von Fähigkeiten betroffener Ziele um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11112,
        "name" : "Seelenentzug",
        "icon" : "ability_warlock_soulsiphon",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht die durch Eure Zauber 'Blutsauger' und 'Seelendieb' abgezogene Menge pro auf das Ziel wirkenden Gebrechenseffekt um zusätzliche 3%, bis zu einem Maximum von 9% zusätzlichem Effekt."
        }, {
          "description" : "Erhöht die durch Eure Zauber 'Blutsauger' und 'Seelendieb' abgezogene Menge pro auf das Ziel wirkenden Gebrechenseffekt um zusätzliche 6%, bis zu einem Maximum von 18% zusätzlichem Effekt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11420,
        "name" : "Lebensentzug",
        "icon" : "spell_shadow_requiem",
        "x" : 2,
        "y" : 1,
        "req" : 11112,
        "ranks" : [ {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Verderbnis' Schaden verursacht, besteht eine Chance von 25%, dass Ihr sofort um 2% Eurer gesamten Gesundheit geheilt werdet."
        }, {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Verderbnis' Schaden verursacht, besteht eine Chance von 50%, dass Ihr sofort um 2% Eurer gesamten Gesundheit geheilt werdet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11128,
        "name" : "Fluch der Erschöpfung",
        "icon" : "spell_shadow_grimward",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "cost" : "6% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "description" : "Verringert das Bewegungstempo des Ziels 30 Sek. lang um 30%. Es kann immer nur jeweils ein Fluch pro Hexenmeister auf einem beliebigen Ziel aktiv sein."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11114,
        "name" : "Verbesserte Furcht",
        "icon" : "spell_shadow_possession",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Euer Zauber 'Furcht' ruft beim Ende seiner Dauer den Effekt 'Alptraum' hervor. Dieser Effekt verringert 5 Sek. lang das Bewegungstempo des Ziels um 15%."
        }, {
          "description" : "Euer Zauber 'Furcht' ruft beim Ende seiner Dauer den Effekt 'Alptraum' hervor. Dieser Effekt verringert 5 Sek. lang das Bewegungstempo des Ziels um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11134,
        "name" : "Ausrottung",
        "icon" : "ability_warlock_eradication",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn Ihr durch den Zauber 'Verderbnis' Schaden verursacht, besteht eine Chance von 6%, dass Euer Zaubertempo 10 Sek. lang um 6% erhöht wird."
        }, {
          "description" : "Wenn Ihr durch den Zauber 'Verderbnis' Schaden verursacht, besteht eine Chance von 6%, dass Euer Zaubertempo 10 Sek. lang um 12% erhöht wird."
        }, {
          "description" : "Wenn Ihr durch den Zauber 'Verderbnis' Schaden verursacht, besteht eine Chance von 6%, dass Euer Zaubertempo 10 Sek. lang um 20% erhöht wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11140,
        "name" : "Verbessertes Schreckensgeheul",
        "icon" : "spell_shadow_deathscream",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit Eures Zaubers 'Schreckensgeheul' um X,8 Sek."
        }, {
          "description" : "Verringert die Zauberzeit Eures Zaubers 'Schreckensgeheul' um 1,5 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11366,
        "name" : "Seelentausch",
        "icon" : "ability_warlock_soulswap",
        "x" : 1,
        "y" : 3,
        "req" : 11112,
        "ranks" : [ {
          "cost" : "18% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "description" : "Ihr verursacht sofort 186 Schaden, und zehrt Eure auf das Ziel wirkenden regelmäßigen Schattenschadenseffekte auf.\n\n\n\nWirkt Ihr innerhalb von 20 Sek. Euren Zauber 'Seelentausch: Einhauchen' auf ein weiteres Ziel, wird es mit den aufgezehrten Effekten belegt und erleidet sofort 186 Schaden.\n\n\n\n'Seelentausch' kann nicht zweimal hintereinander auf das gleiche Ziel gewirkt werden."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11124,
        "name" : "Umschlingende Schatten",
        "icon" : "spell_shadow_shadowembrace",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 3% erhöht. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
        }, {
          "description" : "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 4% erhöht. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
        }, {
          "description" : "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 5% erhöht. Bis zu 3-mal stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11142,
        "name" : "Umarmung des Todes",
        "icon" : "spell_shadow_deathsembrace",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Wenn Ihr über 25% Gesundheit oder weniger verfügt, wird der von Eurem Zauber 'Blutsauger' geheilte Wert um 1% erhöht.\n\n\n\nErhöht den verursachten Schaden Eurer Schattenzauber um 4%, wenn Euer Ziel über 25% Gesundheit oder weniger verfügt."
        }, {
          "description" : "Wenn Ihr über 25% Gesundheit oder weniger verfügt, wird der von Eurem Zauber 'Blutsauger' geheilte Wert um 2% erhöht.\n\n\n\nErhöht den verursachten Schaden Eurer Schattenzauber um 8%, wenn Euer Ziel über 25% Gesundheit oder weniger verfügt."
        }, {
          "description" : "Wenn Ihr über 25% Gesundheit oder weniger verfügt, wird der von Eurem Zauber 'Blutsauger' geheilte Wert um 3% erhöht.\n\n\n\nErhöht den verursachten Schaden Eurer Schattenzauber um 12%, wenn Euer Ziel über 25% Gesundheit oder weniger verfügt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11122,
        "name" : "Einbruch der Nacht",
        "icon" : "spell_shadow_twilight",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "description" : "Gewährt Euren Zaubern 'Verderbnis' und 'Blutsauger' eine Chance von 2%, Euch in einen Schattentrancezustand zu versetzen, nachdem Ihr dem Feind Schaden zugefügt habt. Der Schattentrancezustand verringert die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' um 100%."
        }, {
          "description" : "Gewährt Euren Zaubern 'Verderbnis' und 'Blutsauger' eine Chance von 4%, Euch in einen Schattentrancezustand zu versetzen, nachdem Ihr dem Feind Schaden zugefügt habt. Der Schattentrancezustand verringert die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' um 100%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11419,
        "name" : "Seelenbrand: Saat der Verderbnis",
        "icon" : "spell_shadow_seedofdestruction",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Erfüllt Euren Zauber 'Saat der Verderbnis' mit der Macht des Seelenbrands.\n\n\n\nDer Detonationseffekt Eures Zaubers 'Saat der Verderbnis' wird alle feindlichen Ziele mit dem Effekt Eures Zaubers 'Verderbnis' belegen. Ist die Detonation erfolgreich, wird der Seelensplitter zurückerstattet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11150,
        "name" : "Immerwährende Gebrechen",
        "icon" : "ability_warlock_everlastingaffliction",
        "x" : 1,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht die kritische Trefferchance Eurer Zauber 'Verderbnis', 'Saat der Verderbnis' und 'Instabiles Gebrechen' um 5%.\n\n\n\nDarüber hinaus haben Eure Zauber 'Blutsauger', 'Seelendieb' und 'Heimsuchung' eine Chance von 33%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eurer Zauber 'Verderbnis', 'Saat der Verderbnis' und 'Instabiles Gebrechen' um 10%.\n\n\n\nDarüber hinaus haben Eure Zauber 'Blutsauger', 'Seelendieb' und 'Heimsuchung' eine Chance von 66%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eurer Zauber 'Verderbnis', 'Saat der Verderbnis' und 'Instabiles Gebrechen' um 15%.\n\n\n\nDarüber hinaus haben Eure Zauber 'Blutsauger', 'Seelendieb' und 'Heimsuchung' eine Chance von 100%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11200,
        "name" : "Pandemie",
        "icon" : "spell_shadow_lastingaffliction",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Verringert die globale Abklingzeit Eurer Omen und Flüche um X,25 Sek.\n\n\n\nGewährt Eurem Zauber 'Seelendieb' eine Chance von 50%, die Dauer Eures auf das Ziel wirkenden Effekts von 'Instabiles Gebrechen' aufzufrischen, wenn Euer Ziel über weniger als 25% Gesundheit verfügt."
        }, {
          "description" : "Verringert die globale Abklingzeit Eurer Omen und Flüche um X,5 Sek.\n\n\n\nGewährt Eurem Zauber 'Seelendieb' eine Chance von 100%, die Dauer Eures auf das Ziel wirkenden Effekts von 'Instabiles Gebrechen' aufzufrischen, wenn Euer Ziel über weniger als 25% Gesundheit verfügt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11152,
        "name" : "Heimsuchung",
        "icon" : "ability_warlock_haunt",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "12% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "1,5 Sek. Zauber",
          "cooldown" : "8 Sek. Abklingzeit",
          "description" : "Ihr entsendet eine verderbte Seele in das Ziel, die sowohl 989 Schattenschaden verursacht als auch 12 Sek. lang den Schaden aller Eurer regelmäßigen Schatteneffekte auf dem Ziel um 20% erhöht. Endet die Heimsuchung oder wird sie gebannt, kehrt die Seele zu Euch zurück, um Euch um 100% des von ihr verursachten Schadens zu heilen."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 0
    }, {
      "name" : "Dämonologie",
      "icon" : "spell_shadow_metamorphosis",
      "backgroundFile" : "WarlockSummoning",
      "overlayColor" : "#ff0000",
      "description" : "Ein Hexenmeister, der Schatten- und Feuermagie zusammen mit mächtigen Dämonen nutzt.",
      "treeNo" : 1,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 30146,
        "name" : "Teufelswache beschwören",
        "icon" : "spell_shadow_summonfelguard",
        "cost" : "80% des Grundmanas",
        "castTime" : "6 Sek. Zauber",
        "description" : "Beschwört eine Teufelswache, die unter dem Befehl des Hexenmeisters steht.",
        "id" : 30146,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 84740,
        "name" : "Dämonisches Wissen",
        "icon" : "spell_fire_twilightimmolation",
        "description" : "Erhöht Euren Feuer- und Schattenzauberschaden um 15%.",
        "id" : 84740,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77219,
        "name" : "Meister der Dämonologie",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht den von Euren Dämonendienern verursachten Schaden und den Schaden, den Ihr verursacht, während Ihr in einen Dämonen verwandelt seid, um 18%. Jeder Punkt Meisterschaft erhöht den Schaden um zusätzlich 2.3%.",
        "id" : 77219,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 10994,
        "name" : "Dämonische Umarmung",
        "icon" : "spell_shadow_metamorphosis",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht Eure gesamte Ausdauer um 4%."
        }, {
          "description" : "Erhöht Eure gesamte Ausdauer um 7%."
        }, {
          "description" : "Erhöht Eure gesamte Ausdauer um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10992,
        "name" : "Dunkle Künste",
        "icon" : "ability_warlock_darkarts",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit des Zaubers 'Feuerblitz' Eures Wichtels um 0.25 Sek., erhöht den verursachten Schaden der Fähigkeit 'Legionshieb' Eurer Teufelswache um 5% und erhöht den verursachten Schaden der Fähigkeit 'Schattenbiss' Eures Teufelsjägers um 5%."
        }, {
          "description" : "Verringert die Zauberzeit des Zaubers 'Feuerblitz' Eures Wichtels um 0.50 Sek., erhöht den verursachten Schaden der Fähigkeit 'Legionshieb' Eurer Teufelswache um 10% und erhöht den verursachten Schaden der Fähigkeit 'Schattenbiss' Eures Teufelsjägers um 10%."
        }, {
          "description" : "Verringert die Zauberzeit des Zaubers 'Feuerblitz' Eures Wichtels um 0.75 Sek., erhöht den verursachten Schaden der Fähigkeit 'Legionshieb' Eurer Teufelswache um 15% und erhöht den verursachten Schaden der Fähigkeit 'Schattenbiss' Eures Teufelsjägers um 15%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11206,
        "name" : "Teufelssynergie",
        "icon" : "spell_shadow_felmending",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Ihr habt eine Chance von 50%, Euren Begleiter um 15% des von Euch verursachten Zauberschadens zu heilen."
        }, {
          "description" : "Ihr habt eine Chance von 100%, Euren Begleiter um 15% des von Euch verursachten Zauberschadens zu heilen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11713,
        "name" : "Dämonische Wiedergeburt",
        "icon" : "spell_shadow_demonictactics",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Stirbt Euer beschworener Dämon, erhaltet Ihr den Effekt 'Dämonische Wiedergeburt', wodurch die Zauberzeit Eures nächsten Beschwörungszaubers um 50% verringert wird. Hält 10 Sek. lang an. Dieser Effekt hat eine Abklingzeit von 2 Min."
        }, {
          "description" : "Stirbt Euer beschworener Dämon, erhaltet Ihr den Effekt 'Dämonische Wiedergeburt', wodurch die Zauberzeit Eures nächsten Beschwörungszaubers um 100% verringert wird. Hält 10 Sek. lang an. Dieser Effekt hat eine Abklingzeit von 2 Min."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11020,
        "name" : "Mananachschub",
        "icon" : "spell_shadow_manafeed",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Wenn Euer Dämon mit seinem einfachen Angriff einen kritischen Treffer erzielt, gewinnt Ihr sofort 2% Eures gesamten Manas.\n\n\n\nWenn Ihr durch Euren Zauber 'Aderlass' Mana gewinnt, gewinnt Euer Dämon Mana in Höhe von 30% Eures erhaltenen Manas."
        }, {
          "description" : "Wenn Euer Dämon mit seinem einfachen Angriff einen kritischen Treffer erzielt, gewinnt Ihr sofort 4% Eures gesamten Manas.\n\n\n\nWenn Ihr durch Euren Zauber 'Aderlass' Mana gewinnt, gewinnt Euer Dämon Mana in Höhe von 60% Eures erhaltenen Manas."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11190,
        "name" : "Dämonische Aegis",
        "icon" : "spell_shadow_ragingscream",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht die von Eurem Zauber 'Dämonenrüstung' gewährte, durch Zauber und Effekte erzeugte Gesundheit um zusätzlich 5% und erhöht die von Eurem Zauber 'Teufelsrüstung' erzeugte Gesundheit um 50%."
        }, {
          "description" : "Erhöht die von Eurem Zauber 'Dämonenrüstung' gewährte, durch Zauber und Effekte erzeugte Gesundheit um zusätzlich 10% und erhöht die von Eurem Zauber 'Teufelsrüstung' erzeugte Gesundheit um 100%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11014,
        "name" : "Meister der Beschwörung",
        "icon" : "spell_shadow_impphaseshift",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit Eures Beschwörungszaubers für Wichtel, Leerwandler, Sukkubus, Teufelsjäger und Teufelswache um X,5 Sek. und die Manakosten um 50%."
        }, {
          "description" : "Verringert die Zauberzeit Eures Beschwörungszaubers für Wichtel, Leerwandler, Sukkubus, Teufelsjäger und Teufelswache um 1 Sek. und die Manakosten um 100%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11198,
        "name" : "Drohende Verdammnis",
        "icon" : "spell_nature_removecurse",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht die Chance, dass Euer Zauber 'Omen der Verdammnis' nach verursachtem Schaden einen Dämon beschwört, um 10%.\n\n\n\nZudem wird Euren Zaubern 'Schattenblitz', 'Hand von Gul'dan', 'Seelenfeuer' und 'Verbrennen' eine Chance von 5% gewährt, die Abklingzeit Eurer Dämonengestalt um 15 Sek. zu verringern."
        }, {
          "description" : "Erhöht die Chance, dass Euer Zauber 'Omen der Verdammnis' nach verursachtem Schaden einen Dämon beschwört, um 20%.\n\n\n\nZudem wird Euren Zaubern 'Schattenblitz', 'Hand von Gul'dan', 'Seelenfeuer' und 'Verbrennen' eine Chance von 10% gewährt, die Abklingzeit Eurer Dämonengestalt um 15 Sek. zu verringern."
        }, {
          "description" : "Erhöht die Chance, dass Euer Zauber 'Omen der Verdammnis' nach verursachtem Schaden einen Dämon beschwört, um 30%.\n\n\n\nZudem wird Euren Zaubern 'Schattenblitz', 'Hand von Gul'dan', 'Seelenfeuer' und 'Verbrennen' eine Chance von 15% gewährt, die Abklingzeit Eurer Dämonengestalt um 15 Sek. zu verringern."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11160,
        "name" : "Dämonische Energie",
        "icon" : "ability_warlock_demonicempowerment",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "cost" : "6% des Grundmanas",
          "range" : "100 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Erfüllt den beschworenen Dämon des Hexenmeisters mit Macht.\n\n\n\nWichtel - Heilt den Wichtel sofort um 30% seiner gesamten Gesundheit.\n\n\n\nLeerwandler - Erhöht die Gesundheit des Leerwandlers um 20% und die von seinen Zaubern und Angriffen verursachte Bedrohung um 20%. Hält 20 Sek. lang an.\n\n\n\nSukkubus - Verschwindet sofort in eine verbesserte Unsichtbarkeit. Das Verschwinden entfernt alle auf den Sukkubus wirkenden Betäubungen, Verlangsamungen und bewegungseinschränkenden Effekte.\n\n\n\nTeufelsjäger - Bannt alle auf den Teufelsjäger wirkenden magischen Effekte.\n\n\n\nTeufelswache - Entfernt alle auf die Teufelswache wirkenden Betäubungen, Verlangsamungen, Verbannungen, Schrecken und bewegungseinschränkenden Effekte und macht sie 15 Sek. lang dagegen immun."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10998,
        "name" : "Verbesserte Lebenslinie",
        "icon" : "spell_shadow_lifedrain",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht die durch Euren Zauber 'Lebenslinie' übertragene Gesundheit um 10% und verringert die Gesundheitskosten um 10%. Zusätzlich wird der erlittene Schaden Eures Dämons, wenn er unter dem Effekt von 'Lebenslinie' steht, um 15% verringert."
        }, {
          "description" : "Erhöht die durch Euren Zauber 'Lebenslinie' übertragene Gesundheit um 20% und verringert die Gesundheitskosten um 20%. Zusätzlich wird der erlittene Schaden Eures Dämons, wenn er unter dem Effekt von 'Lebenslinie' steht, um 30% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11024,
        "name" : "Geschmolzener Kern",
        "icon" : "ability_warlock_moltencore",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Es besteht eine Chance von 2%, dass Ihr den Effekt 'Geschmolzener Kern' erhaltet, wenn Euer Zauber 'Feuerbrand' Schaden verursacht. Der Effekt 'Geschmolzener Kern' erfüllt Eure nächsten 3 innerhalb von 15 Sek. gewirkten Zauber von 'Verbrennen' mit Macht, wodurch ihr Schaden um 6% erhöht und ihre Zauberzeit um 10% verringert wird."
        }, {
          "description" : "Es besteht eine Chance von 4%, dass Ihr den Effekt 'Geschmolzener Kern' erhaltet, wenn Euer Zauber 'Feuerbrand' Schaden verursacht. Der Effekt 'Geschmolzener Kern' erfüllt Eure nächsten 3 innerhalb von 15 Sek. gewirkten Zauber von 'Verbrennen' mit Macht, wodurch ihr Schaden um 12% erhöht und ihre Zauberzeit um 20% verringert wird."
        }, {
          "description" : "Es besteht eine Chance von 6%, dass Ihr den Effekt 'Geschmolzener Kern' erhaltet, wenn Euer Zauber 'Feuerbrand' Schaden verursacht. Der Effekt 'Geschmolzener Kern' erfüllt Eure nächsten 3 innerhalb von 15 Sek. gewirkten Zauber von 'Verbrennen' mit Macht, wodurch ihr Schaden um 18% erhöht und ihre Zauberzeit um 30% verringert wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11201,
        "name" : "Hand von Gul'dan",
        "icon" : "inv_summerfest_firespirit",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "cost" : "7% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "2 Sek. Zauber",
          "cooldown" : "12 Sek. Abklingzeit",
          "description" : "Beschwört einen Meteor auf das feindliche Ziel hinab, der 1625 Schattenfeuerschaden verursacht und in einem Umkreis von 4 Metern eine magische Aura hervorruft, welche die kritische Trefferchance der Dämonen von Hexenmeistern gegen alle Ziele innerhalb dieser Aura um 10% erhöht. Die Aura hält 15 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11814,
        "name" : "Aura des Unheils",
        "icon" : "spell_shadow_sealofkings",
        "x" : 2,
        "y" : 3,
        "req" : 11201,
        "ranks" : [ {
          "description" : "Wenn Euer Zauber 'Hand von Gul'dan' trifft, werden alle Gegner im Umkreis von 4 Metern 2 Sek. lang bewegungsunfähig. Wenn sich diese Ziele nach Ablauf von 6 Sek. noch immer innerhalb der Effektaura von 'Fluch des Gul'dan' aufhalten, werden sie für den gleichen Zeitraum der Bewegungsunfähigkeit betäubt."
        }, {
          "description" : "Wenn Euer Zauber 'Hand von Gul'dan' trifft, werden alle Gegner im Umkreis von 4 Metern 3 Sek. lang bewegungsunfähig. Wenn sich diese Ziele nach Ablauf von 6 Sek. noch immer innerhalb der Effektaura von 'Fluch des Gul'dan' aufhalten, werden sie für den gleichen Zeitraum der Bewegungsunfähigkeit betäubt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11188,
        "name" : "Uraltes Grimoire",
        "icon" : "ability_warlock_ancientgrimoire",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Erhöht die Verweildauer Eurer beschworenen Höllenbestie und Verdammniswache um 10 Sek."
        }, {
          "description" : "Erhöht die Verweildauer Eurer beschworenen Höllenbestie und Verdammniswache um 20 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11189,
        "name" : "Inferno",
        "icon" : "ability_warlock_inferno",
        "x" : 1,
        "y" : 4,
        "req" : 11201,
        "ranks" : [ {
          "description" : "Gestattet es Euch, Euren Zauber 'Höllenfeuer' aus der Bewegung heraus zu kanalisieren. Zudem wird die Effektdauer Eures Zaubers 'Feuerbrand' um 6 Sek. erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11034,
        "name" : "Dezimierung",
        "icon" : "spell_fire_fireball02",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Wenn Ihr Eure Zauber 'Schattenblitz', 'Verbrennen' oder 'Seelenfeuer' auf ein Ziel wirkt, das über 25% Gesundheit oder weniger verfügt, wird die Zauberzeit Eures Zaubers 'Seelenfeuer' 10 Sek. lang um 20% verringert."
        }, {
          "description" : "Wenn Ihr Eure Zauber 'Schattenblitz', 'Verbrennen' oder 'Seelenfeuer' auf ein Ziel wirkt, das über 25% Gesundheit oder weniger verfügt, wird die Zauberzeit Eures Zaubers 'Seelenfeuer' 10 Sek. lang um 40% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11199,
        "name" : "Einäscherung",
        "icon" : "ability_warlock_cremation",
        "x" : 1,
        "y" : 5,
        "req" : 11189,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eures Zaubers 'Höllenfeuer' um 15% und Eure 'Hand von Gul'dan' hat eine Chance von 50%, die Dauer Eures Zaubers 'Feuerbrand' auf dem Ziel zurückzusetzen."
        }, {
          "description" : "Erhöht den Schaden Eures Zaubers 'Höllenfeuer' um 30% und Eure 'Hand von Gul'dan' hat eine Chance von 100%, die Dauer Eures Zaubers 'Feuerbrand' auf dem Ziel zurückzusetzen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11042,
        "name" : "Dämonischer Pakt",
        "icon" : "spell_shadow_demonicpact",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Erhöht Euren Zauberschaden um 2% und Euer beschworener Dämon gewährt allen nahen befreundeten Gruppen- oder Schlachtzugsmitgliedern den Effekt 'Dämonischer Pakt'.\n\n\n\nDer Effekt 'Dämonischer Pakt' erhöht die Zaubermacht um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11044,
        "name" : "Metamorphose",
        "icon" : "spell_shadow_demonform",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "description" : "Ihr verwandelt Euch 30 Sek. lang in einen Dämon. Diese Gestalt erhöht Eure Rüstung um 600%, Euren Schaden um 20% und die Chance, einen kritischen Nahkampftreffer zu erleiden, wird um 6% verringert. Die Dauer auf Euch wirkender Betäubungs- und Verlangsamungseffekte wird um 50% verringert. Zusätzlich zu Euren Fähigkeiten gewinnt Ihr einige einzigartige Dämonenfähigkeiten. 3 Minuten Abklingzeit."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 1
    }, {
      "name" : "Zerstörung",
      "icon" : "spell_shadow_rainoffire",
      "backgroundFile" : "WarlockDestruction",
      "overlayColor" : "#ff7f00",
      "description" : "Ruft dämonisches Feuer hernieder, um Feinde zu verbrennen und zu zerstören.",
      "treeNo" : 2,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 17962,
        "name" : "Feuersbrunst",
        "icon" : "spell_fire_fireball",
        "cost" : "16% des Grundmanas",
        "range" : "40 Meter Reichweite",
        "castTime" : "Spontanzauber",
        "cooldown" : "10 Sek. Abklingzeit",
        "description" : "Fügt einem Gegner sofort Schaden in Höhe von 60% eines auf ihn wirkenden Effekts Eures Zaubers 'Feuerbrand' zu.",
        "id" : 17962,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 84739,
        "name" : "Kataklysmus",
        "icon" : "spell_fire_meteorstorm",
        "description" : "Erhöht Euren Feuerzauberschaden um 25%.",
        "id" : 84739,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77220,
        "name" : "Feurige Apokalypse",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht jeglichen von Euch verursachten Feuerschaden um 11%. Jeder Punkt Meisterschaft erhöht den Feuerschaden um zusätzlich 1.35%.",
        "id" : 77220,
        "classMask" : 256,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 10938,
        "name" : "Dunkle Macht",
        "icon" : "spell_shadow_deathpact",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um X,1 Sek."
        }, {
          "description" : "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um X,3 Sek."
        }, {
          "description" : "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um X,5 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10936,
        "name" : "Schatten und Flamme",
        "icon" : "spell_shadow_shadowandflame",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den verursachten Schaden Eurer Zauber 'Schattenblitz' und 'Verbrennen' um 4%. Zudem gewähren Eure Zauber 'Schattenblitz' und 'Verbrennen' eine Chance von 33%, das Ziel mit dem Effekt 'Schatten und Flamme' zu belegen.\n\n\n\nDer Effekt 'Schatten und Flamme' erhöht die kritische Zaubertrefferchance gegen dieses Ziel 30 Sek. lang um 5%."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Zauber 'Schattenblitz' und 'Verbrennen' um 8%. Zudem gewähren Eure Zauber 'Schattenblitz' und 'Verbrennen' eine Chance von 66%, das Ziel mit dem Effekt 'Schatten und Flamme' zu belegen.\n\n\n\nDer Effekt 'Schatten und Flamme' erhöht die kritische Zaubertrefferchance gegen dieses Ziel 30 Sek. lang um 5%."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Zauber 'Schattenblitz' und 'Verbrennen' um 12%. Zudem gewähren Eure Zauber 'Schattenblitz' und 'Verbrennen' eine Chance von 100%, das Ziel mit dem Effekt 'Schatten und Flamme' zu belegen.\n\n\n\nDer Effekt 'Schatten und Flamme' erhöht die kritische Zaubertrefferchance gegen dieses Ziel 30 Sek. lang um 5%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10960,
        "name" : "Verbesserter Feuerbrand",
        "icon" : "spell_fire_immolation",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den verursachten Schaden Eures Zaubers 'Feuerbrand' um 10%."
        }, {
          "description" : "Erhöht den verursachten Schaden Eures Zaubers 'Feuerbrand' um 20%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11197,
        "name" : "Nachwirkung",
        "icon" : "spell_fire_fire",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Euer Zauber 'Feuerregen' hat eine Chance von 6%, Ziele 2 Sek. lang zu betäuben und Euer Zauber 'Feuersbrunst' hat eine Chance von 50%, das Ziel 5 Sek. lang benommen zu machen."
        }, {
          "description" : "Euer Zauber 'Feuerregen' hat eine Chance von 12%, Ziele 2 Sek. lang zu betäuben und Euer Zauber 'Feuersbrunst' hat eine Chance von 100%, das Ziel 5 Sek. lang benommen zu machen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11181,
        "name" : "Glutsturm",
        "icon" : "spell_fire_selfdestruct",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit Eures Zaubers 'Seelenfeuer' um X,5 Sek. und Eures Zaubers 'Verbrennen' um 0.13 Sek."
        }, {
          "description" : "Verringert die Zauberzeit Eures Zaubers 'Seelenfeuer' um 1 Sek. und Eures Zaubers 'Verbrennen' um 0.25 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11196,
        "name" : "Verbesserter sengender Schmerz",
        "icon" : "spell_fire_soulburn",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht bei Zielen, die über 25% Gesundheit oder weniger verfügen, die kritische Trefferchance Eures Zaubers 'Sengender Schmerz' um 20%."
        }, {
          "description" : "Erhöht bei Zielen, die über 25% Gesundheit oder weniger verfügen, die kritische Trefferchance Eures Zaubers 'Sengender Schmerz' um 40%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10940,
        "name" : "Verbessertes Seelenfeuer",
        "icon" : "spell_fire_fireball02",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Seelenfeuer' Schaden verursacht habt, wird Euer Schatten- und Feuerschaden 20 Sek. lang um 4% erhöht."
        }, {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Seelenfeuer' Schaden verursacht habt, wird Euer Schatten- und Feuerschaden 20 Sek. lang um 8% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10978,
        "name" : "Pyrolyse",
        "icon" : "ability_warlock_backdraft",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn Ihr 'Feuersbrunst' wirkt, wird die Zauberzeit Eurer Zauber 'Schattenblitz', 'Verbrennen' und 'Chaosblitz' um 10% verringert. 3 Aufladungen. Hält 15 Sek. lang an."
        }, {
          "description" : "Wenn Ihr 'Feuersbrunst' wirkt, wird die Zauberzeit Eurer Zauber 'Schattenblitz', 'Verbrennen' und 'Chaosblitz' um 20% verringert. 3 Aufladungen. Hält 15 Sek. lang an."
        }, {
          "description" : "Wenn Ihr 'Feuersbrunst' wirkt, wird die Zauberzeit Eurer Zauber 'Schattenblitz', 'Verbrennen' und 'Chaosblitz' um 30% verringert. 3 Aufladungen. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10948,
        "name" : "Schattenbrand",
        "icon" : "spell_shadow_scourgebuild",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "cost" : "15% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "15 Sek. Abklingzeit",
          "description" : "Überzieht das Ziel sofort mit 788 Schattenfeuerschaden. Wenn das Ziel innerhalb von 5 Sek. nach 'Schattenbrand' stirbt und Erfahrung oder Ehrenpunkte gewährt, erhält der Zaubernde 3 Seelensplitter. Kann nur auf Gegner angewandt werden, die über weniger als 20% Gesundheit verfügen."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11182,
        "name" : "Brennende Funken",
        "icon" : "ability_warlock_burningembers",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Euer Zauber 'Seelenfeuer' und der Zauber 'Feuerblitz' Eures Wichtels belegen das Ziel mit dem Effekt 'Brennende Funken', der ihm im Verlauf von 7 Sek. regelmäßig Schaden in Höhe von 25% des verursachten Schadens zufügt.\n\n\n\nDer Effekt 'Brennende Funken' verursacht 7 Sek. lang alle 1 Sek. bis zu 20 Feuerschaden."
        }, {
          "description" : "Euer Zauber 'Seelenfeuer' und der Zauber 'Feuerblitz' Eures Wichtels belegen das Ziel mit dem Effekt 'Brennende Funken', der ihm im Verlauf von 7 Sek. regelmäßig Schaden in Höhe von 50% des verursachten Schadens zufügt.\n\n\n\nDer Effekt 'Brennende Funken' verursacht 7 Sek. lang alle 1 Sek. bis zu 39 Feuerschaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10970,
        "name" : "Seele entziehen",
        "icon" : "spell_shadow_soulleech_3",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn Eure Zauber 'Schattenbrand', 'Chaosblitz' oder 'Seelenfeuer' Schaden verursachen, stellen sie sofort 2% Eurer gesamten Gesundheit und Eures gesamten Manas wieder her. Zudem gewähren sie den Effekt 'Erfrischung'.\n\n\n\nErfrischung - Gewährt bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 10 Sek. Manaregeneration in Höhe von 1% ihres maximalen Manas. Hält 15 Sek. lang an."
        }, {
          "description" : "Wenn Eure Zauber 'Schattenbrand', 'Chaosblitz' oder 'Seelenfeuer' Schaden verursachen, stellen sie sofort 4% Eurer gesamten Gesundheit und Eures gesamten Manas wieder her. Zudem gewähren sie den Effekt 'Erfrischung'.\n\n\n\nErfrischung - Gewährt bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 10 Sek. Manaregeneration in Höhe von 1% ihres maximalen Manas. Hält 15 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10958,
        "name" : "Heimzahlen",
        "icon" : "spell_fire_playingwithfire",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Gewährt Euch eine Chance von 8%, dass durch erlittene körperliche Treffer die Zauberzeit Eures nächsten Wirkens von 'Schattenblitz' oder 'Verbrennen' um 100% verringert wird. Dieser Effekt hält 8 Sek. lang an. Kann nur ein Mal innerhalb von 8 Sek. auftreten."
        }, {
          "description" : "Gewährt Euch eine Chance von 16%, dass durch erlittene körperliche Treffer die Zauberzeit Eures nächsten Wirkens von 'Schattenblitz' oder 'Verbrennen' um 100% verringert wird. Dieser Effekt hält 8 Sek. lang an. Kann nur ein Mal innerhalb von 8 Sek. auftreten."
        }, {
          "description" : "Gewährt Euch eine Chance von 25%, dass durch erlittene körperliche Treffer die Zauberzeit Eures nächsten Wirkens von 'Schattenblitz' oder 'Verbrennen' um 100% verringert wird. Dieser Effekt hält 8 Sek. lang an. Kann nur ein Mal innerhalb von 8 Sek. auftreten."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 12120,
        "name" : "Netherzauberschutz",
        "icon" : "spell_fire_felfireward",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wandelt Euren Zauber 'Schattenzauberschutz' in 'Netherzauberschutz' um. Damit die Transformation vor sich gehen kann, muss einer Eurer Zauber 'Dämonenrüstung' oder 'Teufelsrüstung' aktiv sein.\n\n\n\nNetherzauberschutz\n\nAbsorbiert 3628 Zauberschaden. Hält 30 Sek. lang an. 30 Sek. Abklingzeit."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10984,
        "name" : "Feuer und Schwefel",
        "icon" : "ability_warlock_fireandbrimstone",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "description" : "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 5%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 5% erhöht."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 10%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 10% erhöht."
        }, {
          "description" : "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 15%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 15% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10980,
        "name" : "Schattenfuror",
        "icon" : "spell_shadow_shadowfury",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "cost" : "27% des Grundmanas",
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "20 Sek. Abklingzeit",
          "description" : "Der Schattenfuror wird entfesselt, verursacht 774 Schattenschaden und betäubt 3 Sek. lang alle Gegner im Umkreis von 8 Metern."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10964,
        "name" : "Netherschutz",
        "icon" : "spell_shadow_netherprotection",
        "x" : 3,
        "y" : 4,
        "req" : 12120,
        "ranks" : [ {
          "description" : "Wenn Ihr durch Eure Zauber 'Schattenzauberschutz', 'Netherzauberschutz' oder andere Effekte Zauberschaden absorbiert, gewinnt Ihr den Effekt 'Netherschutz', welcher jeglichen durch diese Zauberart erlittenen Schaden 12 Sek. lang um 15% verringert."
        }, {
          "description" : "Wenn Ihr durch Eure Zauber 'Schattenzauberschutz', 'Netherzauberschutz' oder andere Effekte Zauberschaden absorbiert, gewinnt Ihr den Effekt 'Netherschutz', welcher jeglichen durch diese Zauberart erlittenen Schaden 12 Sek. lang um 30% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10982,
        "name" : "Machterfüllter Wichtel",
        "icon" : "ability_warlock_empoweredimp",
        "x" : 0,
        "y" : 5,
        "req" : 11182,
        "ranks" : [ {
          "description" : "Der Zauber 'Feuerblitz' Eures Wichtels gewährt eine Chance von 2%, Euren nächsten innerhalb von 8 Sek. gewirkten Zauber 'Seelenfeuer' zu einem Spontanzauber werden zu lassen."
        }, {
          "description" : "Der Zauber 'Feuerblitz' Eures Wichtels gewährt eine Chance von 4%, Euren nächsten innerhalb von 8 Sek. gewirkten Zauber 'Seelenfeuer' zu einem Spontanzauber werden zu lassen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10962,
        "name" : "Omen der Verwüstung",
        "icon" : "ability_warlock_baneofhavoc",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "cost" : "10% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "description" : "Belegt das Ziel 5 Min. lang mit einem Omen, durch das es 15% jeglichen an anderen Zielen verursachten Schadens des Hexenmeisters erleidet.\n\n\n\nEs kann pro Hexenmeister nur jeweils ein Effekt von 'Omen der Verwüstung' gleichzeitig aktiv sein und pro Hexenmeister kann nur ein Omen auf einem Ziel gleichzeitig aktiv sein."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 10986,
        "name" : "Chaosblitz",
        "icon" : "ability_warlock_chaosbolt",
        "x" : 1,
        "y" : 6,
        "req" : 10984,
        "ranks" : [ {
          "cost" : "7% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "2,5 Sek. Zauber",
          "cooldown" : "12 Sek. Abklingzeit",
          "description" : "Schleudert einen Blitz chaotischen Feuers auf den Feind und verursacht 1549 Feuerschaden. Dem Chaosblitz kann nicht widerstanden werden und er durchschlägt alle Absorptionseffekte."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 2
    } ]
  },
  "glyphs" : [ {
    "name" : "Glyphe 'Verbrennen'",
    "id" : 272,
    "type" : 2,
    "description" : "Erhöht den von Eurem Zauber 'Verbrennen' verursachten Schaden um 5%.",
    "icon" : "spell_fire_burnout",
    "itemId" : 42453,
    "spellKey" : 56242,
    "spellId" : 56242,
    "prettyName" : "Verbrennen",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Feuersbrunst'",
    "id" : 273,
    "type" : 2,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Feuersbrunst' um 2 Sek.",
    "icon" : "spell_fire_fireball",
    "itemId" : 42454,
    "spellKey" : 56235,
    "spellId" : 56235,
    "prettyName" : "Feuersbrunst",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Verderbnis'",
    "id" : 274,
    "type" : 2,
    "description" : "Euer Zauber 'Verderbnis' hat eine Chance von 4%, Euch in eine Schattentrance fallen zu lassen, nachdem Ihr einem Gegner Schaden zugefügt habt. Die Schattentrance verringert die Zauberzeit Eures nächsten Wirkens von 'Schattenblitz' um 100%.",
    "icon" : "spell_shadow_abominationexplosion",
    "itemId" : 42455,
    "spellKey" : 56218,
    "spellId" : 56218,
    "prettyName" : "Verderbnis",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Omen der Pein'",
    "id" : 275,
    "type" : 2,
    "description" : "Erhöht die Effektdauer Eures Zaubers 'Omen der Pein' um 4 Sek.",
    "icon" : "spell_shadow_curseofsargeras",
    "itemId" : 42456,
    "spellKey" : 56241,
    "spellId" : 56241,
    "prettyName" : "Omen der Pein",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Todesmantel'",
    "id" : 276,
    "type" : 0,
    "description" : "Erhöht die Dauer Eurer Fähigkeit 'Todesmantel' um 0.5 Sek.",
    "icon" : "spell_shadow_deathcoil",
    "itemId" : 42457,
    "spellKey" : 56232,
    "spellId" : 56232,
    "prettyName" : "Todesmantel",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Furcht'",
    "id" : 277,
    "type" : 0,
    "description" : "Lässt das Ziel Eures Zaubers 'Furcht' vor Furcht erstarren, anstatt dass es flüchtet, erhöht jedoch die Abklingzeit von 'Furcht' um 5 Sek.",
    "icon" : "spell_shadow_possession",
    "itemId" : 42458,
    "spellKey" : 56244,
    "spellId" : 56244,
    "prettyName" : "Furcht",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Teufelswache'",
    "id" : 278,
    "type" : 2,
    "description" : "Erhöht den verursachten Schaden der Fähigkeit 'Legionshieb' Eurer Teufelswache um 5%.",
    "icon" : "spell_shadow_summonfelguard",
    "itemId" : 42459,
    "spellKey" : 56246,
    "spellId" : 56246,
    "prettyName" : "Teufelswache",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Teufelsjäger'",
    "id" : 279,
    "type" : 0,
    "description" : "Wendet Euer Teufelsjäger die Fähigkeit 'Magie verschlingen' an, werdet Ihr um den gleichen Betrag geheilt.",
    "icon" : "spell_shadow_summonfelhunter",
    "itemId" : 42460,
    "spellKey" : 56249,
    "spellId" : 56249,
    "prettyName" : "Teufelsjäger",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Lebenslinie'",
    "id" : 280,
    "type" : 1,
    "description" : "Verringert die durch erlittenen Schaden verursachte Zauberzeitverringerung beim Kanalisieren Eures Zaubers 'Lebenslinie' um 100%.",
    "icon" : "spell_shadow_lifedrain",
    "itemId" : 42461,
    "spellKey" : 56238,
    "spellId" : 56238,
    "prettyName" : "Lebenslinie",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Gesundheitsstein'",
    "id" : 281,
    "type" : 0,
    "description" : "Ihr gewinnt durch das Nutzen von Gesundheitssteinen 30% mehr Gesundheit.",
    "icon" : "inv_stone_04",
    "itemId" : 42462,
    "spellKey" : 56224,
    "spellId" : 56224,
    "prettyName" : "Gesundheitsstein",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schreckensgeheul'",
    "id" : 282,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Schreckensgeheul' um 8 Sek.",
    "icon" : "spell_shadow_deathscream",
    "itemId" : 42463,
    "spellKey" : 56217,
    "spellId" : 56217,
    "prettyName" : "Schreckensgeheul",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Feuerbrand'",
    "id" : 283,
    "type" : 2,
    "description" : "Erhöht den regelmäßigen Schaden Eures Zaubers 'Feuerbrand' um 10%.",
    "icon" : "spell_fire_immolation",
    "itemId" : 42464,
    "spellKey" : 56228,
    "spellId" : 56228,
    "prettyName" : "Feuerbrand",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Wichtel'",
    "id" : 284,
    "type" : 2,
    "description" : "Erhöht den verursachten Schaden des Zaubers 'Feuerblitz' Eures Wichtels um 20%.",
    "icon" : "spell_shadow_summonimp",
    "itemId" : 42465,
    "spellKey" : 56248,
    "spellId" : 56248,
    "prettyName" : "Wichtel",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Seelentausch'",
    "id" : 285,
    "type" : 0,
    "description" : "Beim Wirken des Zaubers 'Seelentausch' werden Eure auf das Ziel wirkenden Effekte, die Schaden über Zeit verursachen, nicht mehr aufgezehrt. Die Abklingzeit von 'Seelentausch' wird jedoch um 30 Sek. erhöht.",
    "icon" : "ability_warlock_soulswap",
    "itemId" : 42466,
    "spellKey" : 56226,
    "spellId" : 56226,
    "prettyName" : "Seelentausch",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schattenblitz'",
    "id" : 286,
    "type" : 0,
    "description" : "Verringert die Manakosten Eures Zaubers 'Schattenblitz' um 15%.",
    "icon" : "spell_shadow_shadowbolt",
    "itemId" : 42467,
    "spellKey" : 56240,
    "spellId" : 56240,
    "prettyName" : "Schattenblitz",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schattenbrand'",
    "id" : 287,
    "type" : 2,
    "description" : "Wenn Euer Zauber 'Schattenbrand' ein Ziel nicht tötet, das über 20% Gesundheit oder weniger verfügt, wird seine Abklingzeit sofort abgeschlossen. Dieser Effekt hat eine Abklingzeit von 6 Sek.",
    "icon" : "spell_shadow_scourgebuild",
    "itemId" : 42468,
    "spellKey" : 56229,
    "spellId" : 56229,
    "prettyName" : "Schattenbrand",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Seelenstein'",
    "id" : 289,
    "type" : 0,
    "description" : "Erhöht die Gesundheit, die Ihr durch Wiederbelebung mittels eines Seelensteins erhaltet, um zusätzliche 40%.",
    "icon" : "inv_misc_orb_04",
    "itemId" : 42470,
    "spellKey" : 56231,
    "spellId" : 56231,
    "prettyName" : "Seelenstein",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Verführung'",
    "id" : 290,
    "type" : 0,
    "description" : "Der Zauber 'Verführung' Eures Sukkubus entfernt zusätzlich alle auf das Ziel wirkenden Effekte, die Schaden über Zeit verursachen.",
    "icon" : "spell_shadow_mindsteal",
    "itemId" : 42471,
    "spellKey" : 56250,
    "spellId" : 56250,
    "prettyName" : "Verführung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Instabiles Gebrechen'",
    "id" : 291,
    "type" : 2,
    "description" : "Verringert die Zauberzeit Eures Zaubers 'Instabiles Gebrechen' um 0.2 Sek.",
    "icon" : "spell_shadow_unstableaffliction_3",
    "itemId" : 42472,
    "spellKey" : 56233,
    "spellId" : 56233,
    "prettyName" : "Instabiles Gebrechen",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Leerwandler'",
    "id" : 292,
    "type" : 0,
    "description" : "Erhöht die gesamte Gesundheit Eures Leerwandlers um 20%.",
    "icon" : "spell_shadow_summonvoidwalker",
    "itemId" : 42473,
    "spellKey" : 56247,
    "spellId" : 56247,
    "prettyName" : "Leerwandler",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Unendlicher Atem'",
    "id" : 477,
    "type" : 1,
    "description" : "Erhöht das Schwimmtempo von Zielen, die unter dem Effekt Eures Zaubers 'Unendlicher Atem' stehen, um 20%.",
    "icon" : "spell_shadow_demonbreath",
    "itemId" : 43389,
    "spellKey" : 58079,
    "spellId" : 58079,
    "prettyName" : "Unendlicher Atem",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Seelendieb'",
    "id" : 478,
    "type" : 1,
    "description" : "Wenn Ihr ein Ziel tötet, das Erfahrung oder Ehre gewährt, stellt Euer Zauber 'Seelendieb' 10% Eures gesamten Manas wieder her.",
    "icon" : "spell_shadow_haunting",
    "itemId" : 43390,
    "spellKey" : 58070,
    "spellId" : 58070,
    "prettyName" : "Seelendieb",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Auge von Kilrogg'",
    "id" : 479,
    "type" : 1,
    "description" : "Erhöht das Bewegungstempo des Auges von Kilrogg um 50% und verleiht ihm Flugfähigkeit in Gegenden, wo die Nutzung von fliegenden Reittieren möglich ist.",
    "icon" : "spell_shadow_evileye",
    "itemId" : 43391,
    "spellKey" : 58081,
    "spellId" : 58081,
    "prettyName" : "Auge von Kilrogg",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Fluch der Erschöpfung'",
    "id" : 480,
    "type" : 1,
    "description" : "Erhöht die Reichweite Eures Zaubers 'Fluch der Erschöpfung' um 5 Meter.",
    "icon" : "spell_shadow_grimward",
    "itemId" : 43392,
    "spellKey" : 58080,
    "spellId" : 58080,
    "prettyName" : "Fluch der Erschöpfung",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Dämonensklave'",
    "id" : 481,
    "type" : 1,
    "description" : "Verringert die Zauberzeit Eures Zaubers 'Dämonensklave' um 50%.",
    "icon" : "spell_shadow_enslavedemon",
    "itemId" : 43393,
    "spellKey" : 58107,
    "spellId" : 58107,
    "prettyName" : "Dämonensklave",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Ritual der Seelen'",
    "id" : 482,
    "type" : 1,
    "description" : "Verringert die Manakosten Eures Zaubers 'Ritual der Seelen' um 70%.",
    "icon" : "spell_shadow_shadesofdarkness",
    "itemId" : 43394,
    "spellKey" : 58094,
    "spellId" : 58094,
    "prettyName" : "Ritual der Seelen",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Heimsuchung'",
    "id" : 755,
    "type" : 2,
    "description" : "Erhöht den von Eurem Zauber 'Heimsuchung' gewährten Bonusschaden um zusätzliche 3%.",
    "icon" : "ability_warlock_haunt",
    "itemId" : 45779,
    "spellKey" : 63302,
    "spellId" : 63302,
    "prettyName" : "Heimsuchung",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Metamorphose'",
    "id" : 756,
    "type" : 2,
    "description" : "Erhöht die Dauer Eures Zaubers 'Metamorphose' um 6 Sek.",
    "icon" : "spell_shadow_demonform",
    "itemId" : 45780,
    "spellKey" : 63303,
    "spellId" : 63303,
    "prettyName" : "Metamorphose",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Chaosblitz'",
    "id" : 757,
    "type" : 2,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Chaosblitz' um 2 Sek.",
    "icon" : "ability_warlock_chaosbolt",
    "itemId" : 45781,
    "spellKey" : 63304,
    "spellId" : 63304,
    "prettyName" : "Chaosblitz",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Dämonischer Zirkel'",
    "id" : 758,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Dämonischer Zirkel' um 4 Sek.",
    "icon" : "spell_shadow_demoniccircleteleport",
    "itemId" : 45782,
    "spellKey" : 63309,
    "spellId" : 63309,
    "prettyName" : "Dämonischer Zirkel",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schattenflamme'",
    "id" : 759,
    "type" : 0,
    "description" : "Euer Zauber 'Schattenflamme' verlangsamt zusätzlich das Bewegungstempo der Ziele um 70%.",
    "icon" : "ability_warlock_shadowflame",
    "itemId" : 45783,
    "spellKey" : 63310,
    "spellId" : 63310,
    "prettyName" : "Schattenflamme",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Aderlass'",
    "id" : 760,
    "type" : 0,
    "description" : "Verringert die globale Abklingzeit Eures Zaubers 'Aderlass' um X,5 Sek.",
    "icon" : "spell_shadow_burningspirit",
    "itemId" : 45785,
    "spellKey" : 63320,
    "spellId" : 63320,
    "prettyName" : "Aderlass",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Seelenverbindung'",
    "id" : 761,
    "type" : 0,
    "description" : "Erhöht den Prozentsatz des Schadens, den Ihr durch den Zauber 'Seelenverbindung' mit Eurem Dämon teilt, um zusätzliche 5%.",
    "icon" : "spell_shadow_gathershadows",
    "itemId" : 45789,
    "spellKey" : 63312,
    "spellId" : 63312,
    "prettyName" : "Seelenverbindung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Schmerzenspeitsche'",
    "id" : 911,
    "type" : 2,
    "description" : "Erhöht den verursachten Schaden der Fähigkeit 'Schmerzenspeitsche' Eures Sukkubus um 25%.",
    "icon" : "spell_shadow_curse",
    "itemId" : 50077,
    "spellKey" : 70947,
    "spellId" : 70947,
    "prettyName" : "Schmerzenspeitsche",
    "typeOrder" : 2
  } ]
}