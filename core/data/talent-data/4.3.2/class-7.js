{
  "talentData" : {
    "characterClass" : {
      "classId" : 7,
      "name" : "Schamane",
      "powerType" : "MANA",
      "powerTypeId" : 0,
      "powerTypeSlug" : "mana"
    },
    "talentTrees" : [ {
      "name" : "Elementar",
      "icon" : "spell_nature_lightning",
      "backgroundFile" : "ShamanElementalCombat",
      "overlayColor" : "#cc33cc",
      "description" : "Ein Zauberwirker, der die zerstörerischen Kräfte der Natur und der Elemente kanalisiert.",
      "treeNo" : 0,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 51490,
        "name" : "Gewitter",
        "icon" : "spell_shaman_thunderstorm",
        "castTime" : "Spontanzauber",
        "cooldown" : "45 Sek. Abklingzeit",
        "description" : "Ihr ruft einen Blitzschlag hernieder, der Euch auflädt und so 8% Eures Manas gewinnen lässt. Gegner im Umkreis von 10 Metern erleiden 1691 Naturschaden, werden 20 Meter zurückgestoßen und ihr Bewegungstempo wird 5 Sek. lang um 40% verringert. Dieser Zauber kann genutzt werden, während ein Betäubungseffekt auf Euch wirkt.",
        "id" : 51490,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 62099,
        "name" : "Schamanismus",
        "icon" : "spell_unused2",
        "description" : "Eure Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavaeruption' profitieren durch Eure Zauberkraft um 36% mehr und haben eine um X,5 Sek. verringerte Zauberzeit.",
        "id" : 62099,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 60188,
        "name" : "Elementarfuror",
        "icon" : "spell_fire_volcano",
        "description" : "Erhöht den kritischen Schadensbonus Eurer Feuer-, Frost- und Naturzauber sowie des Totems der Verbrennung und des Totems des glühenden Magmas um 100%. Zudem wird die Abklingzeit Eures Zaubers 'Kettenblitz' entfernt.",
        "id" : 60188,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77222,
        "name" : "Elementare Überladung",
        "icon" : "spell_holy_championsbond",
        "description" : "Gewährt eine Chance von 16% auf das Auftreten einer Elementaren Überladung. Durch diese Elementare Überladung wird von den Zaubern 'Blitzschlag', 'Kettenblitzschlag' oder 'Lavaeruption' ein zweiter, identischer Zauber ausgelöst, der kostenlos ist, 75% des normalen Schadens verursacht und keine Bedrohung hervorruft. Jeder Punkt Meisterschaft erhöht die Chance auf eine Elementare Überladung um zusätzlich 2%.",
        "id" : 77222,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 11218,
        "name" : "Scharfsinn",
        "icon" : "spell_nature_astralrecalgroup",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die kritische Trefferchance aller Eurer Zauber und Angriffe um 1%."
        }, {
          "description" : "Erhöht die kritische Trefferchance aller Eurer Zauber und Angriffe um 2%."
        }, {
          "description" : "Erhöht die kritische Trefferchance aller Eurer Zauber und Angriffe um 3%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 564,
        "name" : "Konvektion",
        "icon" : "spell_shaman_convection",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Reduziert die Manakosten Eurer Schaden verursachenden Offensivzauber um 5%."
        }, {
          "description" : "Reduziert die Manakosten Eurer Schaden verursachenden Offensivzauber um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 563,
        "name" : "Erschütterung",
        "icon" : "spell_fire_fireball",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 2%."
        }, {
          "description" : "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 4%."
        }, {
          "description" : "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 561,
        "name" : "Ruf der Flamme",
        "icon" : "spell_fire_immolation",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eurer Feuertotems und Eures Zaubers 'Feuernova' um 10% sowie den von Eurem Zauber 'Lavaeruption' verursachten Schaden um 5%."
        }, {
          "description" : "Erhöht den Schaden Eurer Feuertotems und Eures Zaubers 'Feuernova' um 20% sowie den von Eurem Zauber 'Lavaeruption' verursachten Schaden um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1640,
        "name" : "Schutz der Elemente",
        "icon" : "spell_nature_spiritarmor",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert den erlittenen Magieschaden um 4%."
        }, {
          "description" : "Verringert den erlittenen Magieschaden um 8%."
        }, {
          "description" : "Verringert den erlittenen Magieschaden um 12%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 575,
        "name" : "Nachklingen",
        "icon" : "spell_frost_frostward",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit Eurer Schockzauber um X,5 Sek. sowie Eures Zaubers 'Windstoß' um 5 Sek."
        }, {
          "description" : "Verringert die Abklingzeit Eurer Schockzauber um 1 Sek. sowie Eures Zaubers 'Windstoß' um 10 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1685,
        "name" : "Elementare Präzision",
        "icon" : "spell_nature_elementalprecision_1",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht Euren Feuer-, Frost- und Naturschaden um 1% und gewährt Euch Zaubertrefferwertung in Höhe von 33% der Willenskraft, die Ihr durch Gegenstände oder Effekte erhaltet."
        }, {
          "description" : "Erhöht Euren Feuer-, Frost- und Naturschaden um 2% und gewährt Euch Zaubertrefferwertung in Höhe von 66% der Willenskraft, die Ihr durch Gegenstände oder Effekte erhaltet."
        }, {
          "description" : "Erhöht Euren Feuer-, Frost- und Naturschaden um 3% und gewährt Euch Zaubertrefferwertung in Höhe von 100% der Willenskraft, die Ihr durch Gegenstände oder Effekte erhaltet."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11767,
        "name" : "Rollender Donner",
        "icon" : "spell_nature_callstorm",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn Ihr mit Euren Zaubern 'Blitzschlag' oder 'Kettenblitzschlag' Schaden verursacht, während Euer 'Blitzschlagschild' aktiv ist, habt Ihr eine Chance von 30%, 2% Eures Manas zurückzuerhalten und eine zusätzliche Aufladung von 'Blitzschlagschild' zu erzeugen, bis zu einem Maximum von 9 Aufladungen."
        }, {
          "description" : "Wenn Ihr mit Euren Zaubern 'Blitzschlag' oder 'Kettenblitzschlag' Schaden verursacht, während Euer 'Blitzschlagschild' aktiv ist, habt Ihr eine Chance von 60%, 2% Eures Manas zurückzuerhalten und eine zusätzliche Aufladung von 'Blitzschlagschild' zu erzeugen, bis zu einem Maximum von 9 Aufladungen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 574,
        "name" : "Elementarfokus",
        "icon" : "spell_shadow_manaburn",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Versetzt Euch in einen Freizauberzustand, nachdem Ihr mit einem Feuer-, Frost- oder Naturschadenszauber einen unregelmäßigen kritischen Treffer erzielt habt. Der Freizauberzustand verringert die Manakosten Eurer nächsten 2 Schadens- oder Heilzauber um 40%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1641,
        "name" : "Elementare Reichweite",
        "icon" : "spell_nature_stormreach",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht die Reichweite Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Feuernova' und 'Lavaeruption' um 5 Meter und erhöht die Reichweite Eurer Schockzauber sowie Eures Totems der Verbrennung um 7 Meter."
        }, {
          "description" : "Erhöht die Reichweite Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Feuernova' und 'Lavaeruption' um 10 Meter und erhöht die Reichweite Eurer Schockzauber sowie Eures Totems der Verbrennung um 15 Meter."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2049,
        "name" : "Elementarer Schwur",
        "icon" : "spell_shaman_elementaloath",
        "x" : 1,
        "y" : 3,
        "req" : 574,
        "ranks" : [ {
          "description" : "Ist der Freizaubereffekt Eures Talents 'Elementarfokus' aktiv, wird Euer Zauberschaden um 5% erhöht. Zusätzlich wird die kritische Trefferchance aller Gruppen- und Schlachtzugsmitglieder innerhalb von 100 Metern um 3% erhöht."
        }, {
          "description" : "Ist der Freizaubereffekt Eures Talents 'Elementarfokus' aktiv, wird Euer Zauberschaden um 10% erhöht. Zusätzlich wird die kritische Trefferchance aller Gruppen- und Schlachtzugsmitglieder innerhalb von 100 Metern um 5% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2051,
        "name" : "Lavastrom",
        "icon" : "spell_shaman_lavaflow",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht den kritischen Schadensbonus Eures Zaubers 'Lavaeruption' um zusätzlich 8% und den regelmäßigen Schaden Eures Zaubers 'Flammenschock' um 20%. Zusätzlich wird, wenn Euer Zauber 'Flammenschock' gebannt wird, Euer Zaubertempo 6 Sek. lang um 30% erhöht."
        }, {
          "description" : "Erhöht den kritischen Schadensbonus Eures Zaubers 'Lavaeruption' um zusätzlich 16% und den regelmäßigen Schaden Eures Zaubers 'Flammenschock' um 40%. Zusätzlich wird, wenn Euer Zauber 'Flammenschock' gebannt wird, Euer Zaubertempo 6 Sek. lang um 60% erhöht."
        }, {
          "description" : "Erhöht den kritischen Schadensbonus Eures Zaubers 'Lavaeruption' um zusätzlich 24% und den regelmäßigen Schaden Eures Zaubers 'Flammenschock' um 60%. Zusätzlich wird, wenn Euer Zauber 'Flammenschock' gebannt wird, Euer Zaubertempo 6 Sek. lang um 90% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11769,
        "name" : "Entladung",
        "icon" : "spell_nature_unrelentingstorm",
        "x" : 0,
        "y" : 4,
        "req" : 11767,
        "ranks" : [ {
          "description" : "Wenn Ihr über mehr als 3 aktive Aufladungen von 'Blitzschlagschild' verfügt, zehrt Euer Zauber 'Erdschock' alle überzähligen Ladungen auf und fügt dem feindlichen Ziel sofort ihren gesamten Schaden zu."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 573,
        "name" : "Elementarbeherrschung",
        "icon" : "spell_nature_wispheal",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Bei Aktivierung wird Euer nächstes Wirken von 'Blitzschlag', 'Kettenblitzschlag' oder 'Lavaeruption' zu einem Spontanzauber. Zusätzlich wird 15 Sek. lang Euer Feuer-, Frost- und Naturschaden um 15% und Euer Zaubertempo um 20% erhöht."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11768,
        "name" : "Griff der Erde",
        "icon" : "spell_nature_stranglevines",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Euer Totem der Erdbindung hat eine Chance von 50%, Ziele 5 Sek. lang festzuwurzeln, wenn es aufgestellt wird."
        }, {
          "description" : "Euer Totem der Erdbindung hat eine Chance von 100%, Ziele 5 Sek. lang festzuwurzeln, wenn es aufgestellt wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 5565,
        "name" : "Totemischer Zorn",
        "icon" : "spell_fire_totemofwrath",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "description" : "Gewährt Euren Feuertotems die Eigenschaft, die Zaubermacht Eurer Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 10% zu erhöhen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11368,
        "name" : "Rückkopplung",
        "icon" : "ability_vehicle_electrocharge",
        "x" : 1,
        "y" : 5,
        "req" : 573,
        "ranks" : [ {
          "description" : "Eure Zauber 'Blitzschlag' und 'Kettenblitzschlag' verringern die verbleibende Abklingzeit Eures Talents 'Elementarbeherrschung' um 1 Sek."
        }, {
          "description" : "Eure Zauber 'Blitzschlag' und 'Kettenblitzschlag' verringern die verbleibende Abklingzeit Eures Talents 'Elementarbeherrschung' um 2 Sek."
        }, {
          "description" : "Eure Zauber 'Blitzschlag' und 'Kettenblitzschlag' verringern die verbleibende Abklingzeit Eures Talents 'Elementarbeherrschung' um 3 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 5566,
        "name" : "Lavasog",
        "icon" : "spell_shaman_lavasurge",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Gewährt dem regelmäßigen Schaden Eures Zaubers 'Flammenschock' eine Chance von 10%, die Abklingzeit Eures Zaubers 'Lavaeruption' abzuschließen."
        }, {
          "description" : "Gewährt dem regelmäßigen Schaden Eures Zaubers 'Flammenschock' eine Chance von 20%, die Abklingzeit Eures Zaubers 'Lavaeruption' abzuschließen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1687,
        "name" : "Erdbeben",
        "icon" : "spell_shaman_earthquake",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "60% des Grundmanas",
          "range" : "35 Meter Reichweite",
          "castTime" : "2,5 Sek. Zauber",
          "cooldown" : "10 Sek. Abklingzeit",
          "description" : "Ihr beschwört ein Erdbeben im Zielgebiet. Die Erde beginnt zu brechen und fügt Gegnern in einem Radius von 8 Metern alle 1 Sek. 336 körperlichen Schaden zu. Es besteht eine Chance von 10%, dass betroffene Ziele niedergestoßen werden. Hält 10 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 0
    }, {
      "name" : "Verstärkung",
      "icon" : "spell_nature_lightningshield",
      "backgroundFile" : "ShamanEnhancement",
      "overlayColor" : "#4c7fff",
      "description" : "Ein totemischer Krieger, der seine Feinde mit Waffen bekämpft, die mit Elementarmagie erfüllt wurden.",
      "treeNo" : 1,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "primarySpells" : [ {
        "spellId" : 60103,
        "name" : "Lavapeitsche",
        "icon" : "ability_shaman_lavalash",
        "cost" : "4% des Grundmanas",
        "range" : "Nahkampfreichweite",
        "castTime" : "Sofort",
        "cooldown" : "10 Sek. Abklingzeit",
        "requires" : "Benötigt Nahkampfwaffe",
        "description" : "Lädt Eure Schildhandwaffe mit Lava auf, die Eurem Ziel sofort 260% Waffenschaden dieser Waffe zufügt. Der Schaden ist um 40% höher, wenn Eure Schildhandwaffe mit dem Effekt 'Flammenzunge' verzaubert ist.",
        "id" : 60103,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 86629,
        "name" : "Beidhändigkeit",
        "icon" : "ability_dualwield",
        "description" : "Ermöglicht Euch die Verwendung von Einhand- und Schildhandwaffen in der Schildhand. Gestattet es Euch zudem, frontale Nahkampfangriffe zu parieren und erhöht Eure Nahkampftrefferchance um 6%.",
        "id" : 86629,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 30814,
        "name" : "Geistige Schnelligkeit",
        "icon" : "spell_nature_mentalquickness",
        "description" : "Eure Zaubermacht entspricht nun 55% Eurer Angriffskraft und Ihr profitiert nicht länger von anderen Quellen von Zaubermacht. Zudem sind die Manakosten Eurer spontanen Stärkungs-, Schadens- und Totemzauber um 75% verringert.",
        "id" : 30814,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 51522,
        "name" : "Urtümliche Weisheit",
        "icon" : "spell_shaman_spectraltransformation",
        "description" : "Eure Nahkampfangriffe haben eine Chance von 40%, dass Ihr sofort 5% Eures Grundmanas gewinnt.",
        "id" : 51522,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77223,
        "name" : "Verstärkte Elemente",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht jeglichen verursachten Feuer-, Frost- und Naturschaden um 20%. Jeder Punkt Meisterschaft erhöht den Schaden um zusätzlich 2.5%",
        "id" : 77223,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 611,
        "name" : "Elementarwaffen",
        "icon" : "spell_fire_flametounge",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die passiven Boni Eurer Fähigkeiten 'Waffe der Flammenzunge' und 'Waffe der Lebensgeister' um 20%, den Schaden der zusätzlichen Angriffe Eurer 'Waffe des Windzorns' um 20% und die Wirksamkeit der anhaltenden Boni Eurer Fähigkeit 'Elemente entfesseln' um 25%."
        }, {
          "description" : "Erhöht die passiven Boni Eurer Fähigkeiten 'Waffe der Flammenzunge' und 'Waffe der Lebensgeister' um 40%, den Schaden der zusätzlichen Angriffe Eurer 'Waffe des Windzorns' um 40% und die Wirksamkeit der anhaltenden Boni Eurer Fähigkeit 'Elemente entfesseln' um 50%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 5560,
        "name" : "Fokussierte Schläge",
        "icon" : "spell_shaman_focusedstrikes",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den von Euren Fähigkeiten 'Urtümlicher Schlag' und 'Sturmschlag' verursachten Schaden um 15%."
        }, {
          "description" : "Erhöht den von Euren Fähigkeiten 'Urtümlicher Schlag' und 'Sturmschlag' verursachten Schaden um 30%."
        }, {
          "description" : "Erhöht den von Euren Fähigkeiten 'Urtümlicher Schlag' und 'Sturmschlag' verursachten Schaden um 45%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 607,
        "name" : "Verbesserte Schilde",
        "icon" : "spell_nature_lightningshield",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht den Schaden der Aufladungen Eures Blitzschlagschilds um 5%, erhöht das durch die Aufladungen Eures Wasserschilds gewonnene Mana um 5% und erhöht den von den Aufladungen Eures Erdschilds geheilten Wert um 5%."
        }, {
          "description" : "Erhöht den Schaden der Aufladungen Eures Blitzschlagschilds um 10%, erhöht das durch die Aufladungen Eures Wasserschilds gewonnene Mana um 10% und erhöht den von den Aufladungen Eures Erdschilds geheilten Wert um 10%."
        }, {
          "description" : "Erhöht den Schaden der Aufladungen Eures Blitzschlagschilds um 15%, erhöht das durch die Aufladungen Eures Wasserschilds gewonnene Mana um 15% und erhöht den von den Aufladungen Eures Erdschilds geheilten Wert um 15%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11216,
        "name" : "Elementarverwüstung",
        "icon" : "spell_fire_elementaldevastation",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Kritische Treffer Eurer unregelmäßigen Offensivzauber erhöhen 10 Sek. lang die Chance auf einen kritischen Treffer mit Nahkampfangriffen um 3%."
        }, {
          "description" : "Kritische Treffer Eurer unregelmäßigen Offensivzauber erhöhen 10 Sek. lang die Chance auf einen kritischen Treffer mit Nahkampfangriffen um 6%."
        }, {
          "description" : "Kritische Treffer Eurer unregelmäßigen Offensivzauber erhöhen 10 Sek. lang die Chance auf einen kritischen Treffer mit Nahkampfangriffen um 9%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 602,
        "name" : "Schlaghagel",
        "icon" : "ability_ghoulfrenzy",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Nach dem Erzielen eines kritischen Treffers wird Euer Angriffstempo für die nächsten 3 Schwünge um 10% erhöht."
        }, {
          "description" : "Nach dem Erzielen eines kritischen Treffers wird Euer Angriffstempo für die nächsten 3 Schwünge um 20% erhöht."
        }, {
          "description" : "Nach dem Erzielen eines kritischen Treffers wird Euer Angriffstempo für die nächsten 3 Schwünge um 30% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 605,
        "name" : "Schnelligkeit der Ahnen",
        "icon" : "spell_lifegivingspeed",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Zauberzeit Eures Zaubers 'Geisterwolf' um 1 Sek. und erhöht Euer Bewegungstempo um 7%. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar."
        }, {
          "description" : "Verringert die Zauberzeit Eures Zaubers 'Geisterwolf' um 2 Sek. und erhöht Euer Bewegungstempo um 15%. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11432,
        "name" : "Totemische Reichweite",
        "icon" : "spell_nature_agitatingtotem",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht die Reichweite der Effekte Eurer Totems um 15%."
        }, {
          "description" : "Erhöht die Reichweite der Effekte Eurer Totems um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 615,
        "name" : "Zähigkeit",
        "icon" : "spell_holy_devotion",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht Eure Ausdauer um 3% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 10%."
        }, {
          "description" : "Erhöht Eure Ausdauer um 7% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 20%."
        }, {
          "description" : "Erhöht Eure Ausdauer um 10% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 901,
        "name" : "Sturmschlag",
        "icon" : "ability_shaman_stormstrike",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "cost" : "8% des Grundmanas",
          "range" : "Nahkampfreichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "8 Sek. Abklingzeit",
          "requires" : "Benötigt Nahkampfwaffe",
          "description" : "Greift das Ziel sofort mit beiden Waffen an, verursacht 225% Waffenschaden und gewährt 15 Sek. lang eine zusätzliche Chance von 25%, den Feind mit Euren Zaubern 'Blitzschlag', 'Kettenblitzschlag', 'Blitzschlagschild' und 'Erdschock' kritisch zu treffen."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2055,
        "name" : "Statischer Schock",
        "icon" : "spell_shaman_staticshock",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn Ihr Eure Fähigkeiten 'Urtümlicher Schlag', 'Sturmschlag' oder 'Lavapeitsche' einsetzt, während Euer Blitzschlagschild aktiv ist, besteht eine Chance von 15%, Schaden in Höhe einer Aufladung Eures Blitzschlagschildes zu verursachen, ohne dass eine Aufladung verbraucht wird."
        }, {
          "description" : "Wenn Ihr Eure Fähigkeiten 'Urtümlicher Schlag', 'Sturmschlag' oder 'Lavapeitsche' einsetzt, während Euer Blitzschlagschild aktiv ist, besteht eine Chance von 30%, Schaden in Höhe einer Aufladung Eures Blitzschlagschildes zu verursachen, ohne dass eine Aufladung verbraucht wird."
        }, {
          "description" : "Wenn Ihr Eure Fähigkeiten 'Urtümlicher Schlag', 'Sturmschlag' oder 'Lavapeitsche' einsetzt, während Euer Blitzschlagschild aktiv ist, besteht eine Chance von 45%, Schaden in Höhe einer Aufladung Eures Blitzschlagschildes zu verursachen, ohne dass eine Aufladung verbraucht wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11220,
        "name" : "Gefrorene Kraft",
        "icon" : "spell_fire_bluecano",
        "x" : 0,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht den Schaden Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavapeitsche' sowie aller Schockzauber um 5%, wenn das Ziel vom Effekt Eures Frostbrandangriffs betroffen ist. Zusätzlich hat Euer Zauber 'Frostschock' eine Chance von 50%, Ziele 5 Sek. lang an Ort und Stelle festzufrieren, wenn sie sich 15 Meter oder weiter entfernt von Euch befinden."
        }, {
          "description" : "Erhöht den Schaden Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavapeitsche' sowie aller Schockzauber um 10%, wenn das Ziel vom Effekt Eures Frostbrandangriffs betroffen ist. Zusätzlich hat Euer Zauber 'Frostschock' eine Chance von 100%, Ziele 5 Sek. lang an Ort und Stelle festzufrieren, wenn sie sich 15 Meter oder weiter entfernt von Euch befinden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11770,
        "name" : "Sturmböen",
        "icon" : "spell_nature_elementalabsorption",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Windstoß' erfolgreich einen gewirkten Zauber unterbrecht, wird 10 Sek. lang Euer Widerstand gegen diese Zauberart um 97 erhöht."
        }, {
          "description" : "Wenn Ihr mittels Eures Zaubers 'Windstoß' erfolgreich einen gewirkten Zauber unterbrecht, wird 10 Sek. lang Euer Widerstand gegen diese Zauberart um 195 erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2083,
        "name" : "Sengende Flammen",
        "icon" : "spell_fire_searingtotem",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Gewährt dem Zauber 'Sengender Blitz' Eures Totems der Verbrennung eine Chance von 33%, sein Ziel in Brand zu setzen und ihm so im Verlauf von 15 Sek. zusätzlich Schaden in Höhe des Trefferschadens von 'Sengender Blitz' zuzufügen. Bis zu 5-mal stapelbar."
        }, {
          "description" : "Gewährt dem Zauber 'Sengender Blitz' Eures Totems der Verbrennung eine Chance von 67%, sein Ziel in Brand zu setzen und ihm so im Verlauf von 15 Sek. zusätzlich Schaden in Höhe des Trefferschadens von 'Sengender Blitz' zuzufügen. Bis zu 5-mal stapelbar."
        }, {
          "description" : "Gewährt dem Zauber 'Sengender Blitz' Eures Totems der Verbrennung eine Chance von 100%, sein Ziel in Brand zu setzen und ihm so im Verlauf von 15 Sek. zusätzlich Schaden in Höhe des Trefferschadens von 'Sengender Blitz' zuzufügen. Bis zu 5-mal stapelbar."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2056,
        "name" : "Irdische Kraft",
        "icon" : "spell_nature_earthelemental_totem",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "description" : "Jeder Puls Eures Totems der Erdbindung hat eine Chance von 50%, jegliche auf Euch oder nahe befreundete Ziele wirkenden Verlangsamungseffekte zu entfernen."
        }, {
          "description" : "Jeder Puls Eures Totems der Erdbindung hat eine Chance von 100%, jegliche auf Euch oder nahe befreundete Ziele wirkenden Verlangsamungseffekte zu entfernen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1693,
        "name" : "Schamanistische Wut",
        "icon" : "spell_nature_shamanrage",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Verringert jeglichen erlittenen Schaden um 30% und ermöglicht Euch die kostenlose Nutzung von Fähigkeiten, Totems und Offensivzaubern. Hält 15 Sek. lang an. Diese Fähigkeit kann im betäubten Zustand eingesetzt werden."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1689,
        "name" : "Entfesselte Wut",
        "icon" : "spell_nature_unleashedrage",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "description" : "Erhöht Eure Waffenkunde um 4. Zudem wird im Umkreis von 100 Metern die Nahkampfangriffskraft aller Gruppen- und Schlachtzugsmitglieder um 10% sowie ihre Distanzangriffskraft um 5% erhöht."
        }, {
          "description" : "Erhöht Eure Waffenkunde um 8. Zudem wird im Umkreis von 100 Metern die Nahkampfangriffskraft aller Gruppen- und Schlachtzugsmitglieder um 20% sowie ihre Distanzangriffskraft um 10% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2057,
        "name" : "Waffe des Mahlstroms",
        "icon" : "spell_shaman_maelstromweapon",
        "x" : 1,
        "y" : 5,
        "ranks" : [ {
          "description" : "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance, dass die Zauberzeit und Manakosten Eures nächsten Heilzaubers oder 'Blitzschlag', 'Kettenblitzschlag' oder 'Verhexen' um 20% verringert werden. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
        }, {
          "description" : "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance (höher als bei Rang 1), dass die Zauberzeit und Manakosten Eures nächsten Heilzaubers oder 'Blitzschlag', 'Kettenblitzschlag' oder 'Verhexen' um 20% verringert werden. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
        }, {
          "description" : "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance (höher als bei Rang 2), dass die Zauberzeit und Manakosten Eures nächsten Heilzaubers oder 'Blitzschlag', 'Kettenblitzschlag' oder 'Verhexen' um 20% verringert werden. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 5563,
        "name" : "Verbesserte Lavapeitsche",
        "icon" : "spell_shaman_improvelavalash",
        "x" : 2,
        "y" : 5,
        "req" : 2083,
        "ranks" : [ {
          "description" : "Erhöht für jeden auf das Ziel wirkenden Stapel von 'Sengende Flammen' den Schaden Eures Zaubers 'Lavapeitsche' um zusätzlich 10%, die Stapel werden dabei jedoch aufgezehrt. Zudem wird durch Euren Zauber 'Lavapeitsche' Euer Effekt 'Flammenschock' vom Ziel auf bis zu 4 weitere Gegner im Umkreis von 12 Metern übertragen."
        }, {
          "description" : "Erhöht für jeden auf das Ziel wirkenden Stapel von 'Sengende Flammen' den Schaden Eures Zaubers 'Lavapeitsche' um zusätzlich 20%, die Stapel werden dabei jedoch aufgezehrt. Zudem wird durch Euren Zauber 'Lavapeitsche' Euer Effekt 'Flammenschock' vom Ziel auf bis zu 4 weitere Gegner im Umkreis von 12 Metern übertragen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2058,
        "name" : "Wildgeist",
        "icon" : "spell_shaman_feralspirit",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "12% des Grundmanas",
          "range" : "30 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Beschwört zwei Geisterwölfe, die unter dem Befehl des Schamanen stehen. Hält 30 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 1
    }, {
      "name" : "Wiederherstellung",
      "icon" : "spell_nature_magicimmunity",
      "backgroundFile" : "ShamanRestoration",
      "overlayColor" : "#33cc66",
      "description" : "Ein Heiler, der die Ahnengeister und die läuternde Kraft des Wassers herbeiruft, um die Wunden seiner Verbündeten zu heilen.",
      "treeNo" : 2,
      "roles" : {
        "tank" : false,
        "healer" : true,
        "dps" : false
      },
      "primarySpells" : [ {
        "spellId" : 974,
        "name" : "Erdschild",
        "icon" : "spell_nature_skinofearth",
        "cost" : "19% des Grundmanas",
        "range" : "40 Meter Reichweite",
        "castTime" : "Spontanzauber",
        "description" : "Schützt das Ziel mit einem Erdschild, der die erhöhte Zauberdauer oder verringerte Kanalisierungsdauer durch erlittenen Schaden um 30% verringert und Angriffe das geschützte Ziel um 1416 heilen lässt. Dieser Effekt tritt nur einmal alle paar Sekunden auf. 9 Aufladungen. Hält 10 Min. lang an. Der Erdschild kann immer nur auf einem Ziel gleichzeitig aktiv sein, und auf jedem Ziel kann nur ein Elementarschild aktiv sein.",
        "id" : 974,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : true
      }, {
        "spellId" : 16213,
        "name" : "Läuterung",
        "icon" : "spell_frost_wizardmark",
        "description" : "Erhöht die Wirksamkeit Eurer Heilzauber um 25% und verringert die Zauberzeit Eurer Zauber 'Welle der Heilung' und 'Große Welle der Heilung' um X,5 Sek.",
        "id" : 16213,
        "classMask" : 0,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      }, {
        "spellId" : 95862,
        "name" : "Meditation",
        "icon" : "spell_nature_sleep",
        "description" : "Lässt 50% Eurer durch Willenskraft erzeugten Manaregeneration weiterlaufen, während Ihr Euch im Kampf befindet.",
        "id" : 95862,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : true,
        "keyAbility" : false
      } ],
      "masteries" : [ {
        "spellId" : 77226,
        "name" : "Tiefe Heilung",
        "icon" : "spell_holy_championsbond",
        "description" : "Erhöht die Wirksamkeit Eurer Heilzauber um bis zu 24%, basierend auf der aktuellen Gesundheit Eures Ziels (Ziele mit niedriger Gesundheit erhalten mehr Heilung). Jeder Punkt Meisterschaft verstärkt Eure Heilungen um zusätzlich 3.0%.",
        "id" : 77226,
        "classMask" : 64,
        "raceMask" : 0,
        "classAbility" : false,
        "keyAbility" : false
      } ],
      "talents" : [ {
        "id" : 5568,
        "name" : "Entschlossenheit der Ahnen",
        "icon" : "spell_nature_rune",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert Euren während des Wirkens von Zaubern erlittenen Schaden um 5%."
        }, {
          "description" : "Verringert Euren während des Wirkens von Zaubern erlittenen Schaden um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 593,
        "name" : "Gezeitenfokus",
        "icon" : "spell_frost_manarecharge",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "description" : "Verringert die Manakosten Eurer Heilzauber um 2%."
        }, {
          "description" : "Verringert die Manakosten Eurer Heilzauber um 4%."
        }, {
          "description" : "Verringert die Manakosten Eurer Heilzauber um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11177,
        "name" : "Lebensfunke",
        "icon" : "spell_nature_wispsplodegreen",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht Eure gewirkte Heilung um 2% und Eure erhaltene Heilung um 5%."
        }, {
          "description" : "Erhöht Eure gewirkte Heilung um 4% und Eure erhaltene Heilung um 10%."
        }, {
          "description" : "Erhöht Eure gewirkte Heilung um 6% und Eure erhaltene Heilung um 15%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 583,
        "name" : "Rückstrom",
        "icon" : "ability_shaman_watershield",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Während Euer Zauber 'Wasserschild' aktiv ist, gewinnt Ihr Mana zurück, wenn Eure direkten Heilzauber einen kritischen Effekt erzielen. Durch einen kritischen Effekt Eurer Zauber 'Welle der Heilung' oder 'Große Welle der Heilung' gewinnt Ihr 1147 Mana, durch einen kritischen Effekt Eurer Zauber 'Heilende Woge', 'Springflut' oder 'Leben entfesseln' gewinnt Ihr 688 Mana und durch den kritischen Effekt von 'Kettenheilung' 382 Mana."
        }, {
          "description" : "Während Euer Zauber 'Wasserschild' aktiv ist, gewinnt Ihr Mana zurück, wenn Eure direkten Heilzauber einen kritischen Effekt erzielen. Durch einen kritischen Effekt Eurer Zauber 'Welle der Heilung' oder 'Große Welle der Heilung' gewinnt Ihr 2293 Mana, durch einen kritischen Effekt Eurer Zauber 'Heilende Woge', 'Springflut' oder 'Leben entfesseln' gewinnt Ihr 1376 Mana und durch den kritischen Effekt von 'Kettenheilung' 764 Mana."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 595,
        "name" : "Totemfokus",
        "icon" : "spell_nature_moonglow",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Verringert die Manakosten Eurer Totems um 15% und erhöht ihre Dauer um 20%."
        }, {
          "description" : "Verringert die Manakosten Eurer Totems um 30% und erhöht ihre Dauer um 40%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 5567,
        "name" : "Fokussierte Einsicht",
        "icon" : "spell_shaman_measuredinsight",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Nach dem Wirken eines Schockzaubers werden die Manakosten Eurer nächsten Heilung um 25% der Kosten des Schockzaubers verringert und die Heilwirkung um 10% erhöht."
        }, {
          "description" : "Nach dem Wirken eines Schockzaubers werden die Manakosten Eurer nächsten Heilung um 50% der Kosten des Schockzaubers verringert und die Heilwirkung um 20% erhöht."
        }, {
          "description" : "Nach dem Wirken eines Schockzaubers werden die Manakosten Eurer nächsten Heilung um 75% der Kosten des Schockzaubers verringert und die Heilwirkung um 30% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1699,
        "name" : "Wächter der Natur",
        "icon" : "spell_nature_natureguardian",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 5% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
        }, {
          "description" : "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 10% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
        }, {
          "description" : "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 15% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 581,
        "name" : "Heilung der Ahnen",
        "icon" : "spell_nature_undyingstrength",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Verringert den erlittenen körperlichen Schaden eines Ziels 15 Sek. lang um 5%, nachdem es einen kritischen Effekt durch einen Eurer Heilzauber erhalten hat. Zudem erhöhen Eure gewirkten Heilungen die Gesundheit Eurer Verbündeten um 5% des geheilten Werts, bis zu einem Maximum von 10% ihrer Gesundheit."
        }, {
          "description" : "Verringert den erlittenen körperlichen Schaden eines Ziels 15 Sek. lang um 10%, nachdem es einen kritischen Effekt durch einen Eurer Heilzauber erhalten hat. Zudem erhöhen Eure gewirkten Heilungen die Gesundheit Eurer Verbündeten um 10% des geheilten Werts, bis zu einem Maximum von 10% ihrer Gesundheit."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 591,
        "name" : "Schnelligkeit der Natur",
        "icon" : "spell_nature_ravenform",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "2 Min. Abklingzeit",
          "description" : "Bei Aktivierung wird Euer nächster Naturzauber mit einer Zauberzeit von weniger als 10 Sek. ein Spontanzauber."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 1696,
        "name" : "Segen der Natur",
        "icon" : "spell_nature_natureblessing",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Erhöht die Wirksamkeit Eurer direkten Heilung auf Ziele, auf denen 'Erdschild' aktiv ist, um 6%."
        }, {
          "description" : "Erhöht die Wirksamkeit Eurer direkten Heilung auf Ziele, auf denen 'Erdschild' aktiv ist, um 12%."
        }, {
          "description" : "Erhöht die Wirksamkeit Eurer direkten Heilung auf Ziele, auf denen 'Erdschild' aktiv ist, um 18%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 588,
        "name" : "Sanfter Regen",
        "icon" : "spell_nature_giftofthewaterspirit",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht den von Eurem Totem des heilenden Flusses geheilten Wert um 25% und Effekt Eures Zaubers 'Heilender Regen' um 15%."
        }, {
          "description" : "Erhöht den von Eurem Totem des heilenden Flusses geheilten Wert um 50% und Effekt Eures Zaubers 'Heilender Regen' um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2084,
        "name" : "Verbesserte Geistläuterung",
        "icon" : "ability_shaman_cleansespirit",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erfüllt Euren Zauber 'Geistläuterung' mit der Macht, auch einen magischen Effekt von seinem Ziel zu entfernen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 11435,
        "name" : "Läuterndes Wasser",
        "icon" : "spell_nature_regeneration_02",
        "x" : 3,
        "y" : 3,
        "req" : 2084,
        "ranks" : [ {
          "description" : "Verringert die Manakosten Eures Zaubers 'Geistläuterung' um 20%. Zudem wird, wenn Euer Zauber 'Geistläuterung' erfolgreich einen schädlichen Effekt entfernt, das Ziel gleichzeitig um 1414 Gesundheit geheilt. Der Heilungseffekt kann nur ein Mal alle 6 Sek. auftreten."
        }, {
          "description" : "Verringert die Manakosten Eures Zaubers 'Geistläuterung' um 40%. Zudem wird, wenn Euer Zauber 'Geistläuterung' erfolgreich einen schädlichen Effekt entfernt, das Ziel gleichzeitig um 2830 Gesundheit geheilt. Der Heilungseffekt kann nur ein Mal alle 6 Sek. auftreten."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2061,
        "name" : "Erwachen der Ahnen",
        "icon" : "spell_shaman_ancestralawakening",
        "x" : 0,
        "y" : 4,
        "req" : 581,
        "ranks" : [ {
          "description" : "Erzielt Ihr mit einem direkten Einzelziel-Heilzauber einen kritischen Effekt, wird ein Ahnengeist beschworen, der sofort das Gruppen- oder Schlachtzugsmitglied mit der geringsten Gesundheit innerhalb von 40 Metern um 10% des von Euch geheilten Wertes heilt."
        }, {
          "description" : "Erzielt Ihr mit einem direkten Einzelziel-Heilzauber einen kritischen Effekt, wird ein Ahnengeist beschworen, der sofort das Gruppen- oder Schlachtzugsmitglied mit der geringsten Gesundheit innerhalb von 40 Metern um 20% des von Euch geheilten Wertes heilt."
        }, {
          "description" : "Erzielt Ihr mit einem direkten Einzelziel-Heilzauber einen kritischen Effekt, wird ein Ahnengeist beschworen, der sofort das Gruppen- oder Schlachtzugsmitglied mit der geringsten Gesundheit innerhalb von 40 Metern um 30% des von Euch geheilten Wertes heilt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 590,
        "name" : "Totem der Manaflut",
        "icon" : "spell_frost_summonwaterelemental",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Beschwört 12 Sek. lang ein Totem der Manaflut zu Füßen des Zaubernden, das über 10% seiner Gesundheit verfügt und die Willenskraft von Gruppen- und Schlachtzugsmitgliedern in einem Umkreis von 40 Metern um einen Betrag erhöht, der 200% der Willenskraft des zaubernden Schamanen entspricht (kurzzeitige Willenskraftboni werden aus der Berechnung ausgeschlossen)."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 7705,
        "name" : "Tellurische Ströme",
        "icon" : "spell_lightning_lightningbolt01",
        "x" : 2,
        "y" : 4,
        "ranks" : [ {
          "description" : "Durch Eure Anpassung an die Energien der Natur stellt Euer Zauber 'Blitzschlag' Mana in Höhe von 20% seines verursachten Schadens her."
        }, {
          "description" : "Durch Eure Anpassung an die Energien der Natur stellt Euer Zauber 'Blitzschlag' Mana in Höhe von 40% seines verursachten Schadens her."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 15487,
        "name" : "Totem der Geistverbindung",
        "icon" : "spell_shaman_spiritlink",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "cost" : "11% des Grundmanas",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Beschwört ein Totem der Geistverbindung mit 5 Gesundheit zu Füßen des Zaubernden. Das Totem verringert den erlittenen Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 10 Metern um 10%. Alle 1 Sek. wird die Gesundheit sämtlicher betroffenen Spielercharaktere untereinander ausgeglichen, sodass jeder über den gleichen Prozentsatz seiner jeweiligen maximalen Gesundheit verfügt. Hält 6 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2063,
        "name" : "Flutwellen",
        "icon" : "spell_shaman_tidalwaves",
        "x" : 1,
        "y" : 5,
        "ranks" : [ {
          "description" : "Beim Wirken von 'Kettenheilung' oder 'Springflut' erhaltet Ihr den Effekt 'Flutwellen', der die Zauberzeit Eurer Zauber 'Welle der Heilung' und 'Große Welle der Heilung' um 10% verringert und die Chance auf einen kritischen Effekt Eures Zaubers 'Heilende Woge' um 10% erhöht. Dieser Effekt hat 2 Aufladungen."
        }, {
          "description" : "Beim Wirken von 'Kettenheilung' oder 'Springflut' erhaltet Ihr den Effekt 'Flutwellen', der die Zauberzeit Eurer Zauber 'Welle der Heilung' und 'Große Welle der Heilung' um 20% verringert und die Chance auf einen kritischen Effekt Eures Zaubers 'Heilende Woge' um 20% erhöht. Dieser Effekt hat 2 Aufladungen."
        }, {
          "description" : "Beim Wirken von 'Kettenheilung' oder 'Springflut' erhaltet Ihr den Effekt 'Flutwellen', der die Zauberzeit Eurer Zauber 'Welle der Heilung' und 'Große Welle der Heilung' um 30% verringert und die Chance auf einen kritischen Effekt Eures Zaubers 'Heilende Woge' um 30% erhöht. Dieser Effekt hat 2 Aufladungen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2060,
        "name" : "Segen der Ewigen",
        "icon" : "spell_shaman_blessingofeternals",
        "x" : 2,
        "y" : 5,
        "ranks" : [ {
          "description" : "Wenn Ihr einen Verbündeten heilt, der über weniger als 35% Gesamtgesundheit verfügt, wird Eure Chance, den Effekt von 'Lebensgeister' auszulösen, der Heilung über Zeit hervorruft, um 40% erhöht."
        }, {
          "description" : "Wenn Ihr einen Verbündeten heilt, der über weniger als 35% Gesamtgesundheit verfügt, wird Eure Chance, den Effekt von 'Lebensgeister' auszulösen, der Heilung über Zeit hervorruft, um 80% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2064,
        "name" : "Springflut",
        "icon" : "spell_nature_riptide",
        "x" : 1,
        "y" : 6,
        "ranks" : [ {
          "cost" : "10% des Grundmanas",
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "6 Sek. Abklingzeit",
          "description" : "Heilt ein freundliches Ziel um 2387 und im Verlauf von 15 Sek. um weitere 5645 Gesundheit. Euer nächstes Wirken von 'Kettenheilung' innerhalb von 15 Sek., das dieses Ziel als Hauptziel hat, zehrt den Effekt auf, der Heilung über Zeit verursacht, und erhöht den von 'Kettenheilung' geheilten Wert um 25%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 2
    } ]
  },
  "glyphs" : [ {
    "name" : "Glyphe 'Wasserschild'",
    "id" : 211,
    "type" : 2,
    "description" : "Erhöht die passive Manaregeneration Eures Zaubers 'Wasserschild' um 50%.",
    "icon" : "ability_shaman_watershield",
    "itemId" : 41541,
    "spellKey" : 55436,
    "spellId" : 55436,
    "prettyName" : "Wasserschild",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Kettenheilung'",
    "id" : 212,
    "type" : 0,
    "description" : "Erhöht die Heilung Eures Zaubers 'Kettenheilung' an allen Zielen nach dem Primärziel um 15%, verringert jedoch die Heilung auf dem Primärziel um 10%.",
    "icon" : "spell_nature_healingwavegreater",
    "itemId" : 41517,
    "spellKey" : 55437,
    "spellId" : 55437,
    "prettyName" : "Kettenheilung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Kettenblitzschlag'",
    "id" : 213,
    "type" : 0,
    "description" : "Euer Zauber 'Kettenblitzschlag' trifft jetzt 2 zusätzliche Ziele aber verursacht 10% weniger Initialschaden.",
    "icon" : "spell_nature_chainlightning",
    "itemId" : 41518,
    "spellKey" : 55449,
    "spellId" : 55449,
    "prettyName" : "Kettenblitzschlag",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Lavaeruption'",
    "id" : 214,
    "type" : 2,
    "description" : "Euer Zauber 'Lavaeruption' verursacht 10% mehr Schaden.",
    "icon" : "spell_shaman_lavaburst",
    "itemId" : 41524,
    "spellKey" : 55454,
    "spellId" : 55454,
    "prettyName" : "Lavaeruption",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Schock'",
    "id" : 215,
    "type" : 2,
    "description" : "Verringert die von Euren Schockzaubern verursachte globale Abklingzeit auf 1 Sek.",
    "icon" : "spell_nature_earthshock",
    "itemId" : 41526,
    "spellKey" : 55442,
    "spellId" : 55442,
    "prettyName" : "Schock",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Waffe der Lebensgeister'",
    "id" : 216,
    "type" : 2,
    "description" : "Erhöht die Wirksamkeit der regelmäßigen Heilung Eures Zaubers 'Waffe der Lebensgeister' um 20%.",
    "icon" : "spell_shaman_unleashweapon_life",
    "itemId" : 41527,
    "spellKey" : 55439,
    "spellId" : 55439,
    "prettyName" : "Waffe der Lebensgeister",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Totem des Feuerelementars'",
    "id" : 217,
    "type" : 2,
    "description" : "Verringert die Abklingzeit Eures Totems des Feuerelementars um 5 Min.",
    "icon" : "spell_fire_elemental_totem",
    "itemId" : 41529,
    "spellKey" : 55455,
    "spellId" : 55455,
    "prettyName" : "Totem des Feuerelementars",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Feuernova'",
    "id" : 218,
    "type" : 0,
    "description" : "Erhöht den Radius Eures Zaubers 'Feuernova' um 5 Meter.",
    "icon" : "spell_shaman_firenova",
    "itemId" : 41530,
    "spellKey" : 55450,
    "spellId" : 55450,
    "prettyName" : "Feuernova",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Flammenschock'",
    "id" : 219,
    "type" : 2,
    "description" : "Erhöht die Dauer Eures Zaubers 'Flammenschock' um 50%.",
    "icon" : "spell_fire_flameshock",
    "itemId" : 41531,
    "spellKey" : 55447,
    "spellId" : 55447,
    "prettyName" : "Flammenschock",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Waffe der Flammenzunge'",
    "id" : 220,
    "type" : 2,
    "description" : "Erhöht die kritische Zaubertrefferchance, wenn der Zauber 'Waffe der Flammenzunge' aktiv ist, um 2%.",
    "icon" : "spell_shaman_unleashweapon_flame",
    "itemId" : 41532,
    "spellKey" : 55451,
    "spellId" : 55451,
    "prettyName" : "Waffe der Flammenzunge",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Frostschock'",
    "id" : 221,
    "type" : 0,
    "description" : "Erhöht die Dauer Eures Zaubers 'Frostschock' um 2 Sek.",
    "icon" : "spell_frost_frostshock",
    "itemId" : 41547,
    "spellKey" : 55443,
    "spellId" : 55443,
    "prettyName" : "Frostschock",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Totem des heilenden Flusses'",
    "id" : 222,
    "type" : 0,
    "description" : "Euer Totem des heilenden Flusses erhöht auch den Feuer-, Frost- und Naturwiderstand aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 30 Metern um 195.",
    "icon" : "inv_spear_04",
    "itemId" : 41533,
    "spellKey" : 55456,
    "spellId" : 55456,
    "prettyName" : "Totem des heilenden Flusses",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Welle der Heilung'",
    "id" : 223,
    "type" : 0,
    "description" : "Euer Zauber 'Welle der Heilung' heilt Euch ebenfalls um 20% des geheilten Wertes, wenn Ihr einen Verbündeten heilt.",
    "icon" : "spell_nature_healingwavegreater",
    "itemId" : 41534,
    "spellKey" : 55440,
    "spellId" : 55440,
    "prettyName" : "Welle der Heilung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Totemischer Rückruf'",
    "id" : 224,
    "type" : 0,
    "description" : "Eure Fähigkeit 'Totemischer Rückruf' erstattet zusätzliche 50% der Manakosten Eurer rückgerufenen Totems zurück.",
    "icon" : "spell_shaman_totemrecall",
    "itemId" : 41535,
    "spellKey" : 55438,
    "spellId" : 55438,
    "prettyName" : "Totemischer Rückruf",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Blitzschlagschild'",
    "id" : 225,
    "type" : 0,
    "description" : "Euer Zauber 'Blitzschlagschild' kann dadurch, dass er Feinden Schaden zufügt, nicht mehr unter 3 Aufladungen sinken.",
    "icon" : "spell_nature_lightningshield",
    "itemId" : 41537,
    "spellKey" : 55448,
    "spellId" : 55448,
    "prettyName" : "Blitzschlagschild",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Blitzschlag'",
    "id" : 226,
    "type" : 2,
    "description" : "Erhöht den durch Euren Zauber 'Blitzschlag' verursachten Schaden um 4%.",
    "icon" : "spell_nature_lightning",
    "itemId" : 41536,
    "spellKey" : 55453,
    "spellId" : 55453,
    "prettyName" : "Blitzschlag",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Totem der Erdung'",
    "id" : 227,
    "type" : 0,
    "description" : "Statt einen Zauber zu absorbieren, reflektiert Euer Totem der Erdung den nächsten Schadenszauber zurück auf den Zaubernden, jedoch wird die Abklingzeit Eures Totems der Erdung um 35 Sek. erhöht.",
    "icon" : "spell_nature_groundingtotem",
    "itemId" : 41538,
    "spellKey" : 55441,
    "spellId" : 55441,
    "prettyName" : "Totem der Erdung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Sturmschlag'",
    "id" : 228,
    "type" : 2,
    "description" : "Erhöht den kritischen Schadensbonus Eurer Fähigkeit 'Sturmschlag' um zusätzliche 10%.",
    "icon" : "ability_shaman_stormstrike",
    "itemId" : 41539,
    "spellKey" : 55446,
    "spellId" : 55446,
    "prettyName" : "Sturmschlag",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Lavapeitsche'",
    "id" : 229,
    "type" : 2,
    "description" : "Erhöht den von Eurer Fähigkeit 'Lavapeitsche' verursachten Schaden um 20%.",
    "icon" : "ability_shaman_lavalash",
    "itemId" : 41540,
    "spellKey" : 55444,
    "spellId" : 55444,
    "prettyName" : "Lavapeitsche",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Elementarbeherrschung'",
    "id" : 230,
    "type" : 0,
    "description" : "Während Eure Fähigkeit 'Elementarbeherrschung' aktiv ist, wird jeglicher erlittene Schaden um 20% verringert.",
    "icon" : "spell_nature_wispheal",
    "itemId" : 41552,
    "spellKey" : 55452,
    "spellId" : 55452,
    "prettyName" : "Elementarbeherrschung",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Waffe des Windzorns'",
    "id" : 231,
    "type" : 2,
    "description" : "Erhöht die Chance pro Schwung, dass der Effekt Eures Zaubers 'Waffe des Windzorns' ausgelöst wird, um 2%.",
    "icon" : "spell_shaman_unleashweapon_wind",
    "itemId" : 41542,
    "spellKey" : 55445,
    "spellId" : 55445,
    "prettyName" : "Waffe des Windzorns",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Astraler Rückruf'",
    "id" : 470,
    "type" : 1,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Astraler Rückruf' um 7.5 Min.",
    "icon" : "spell_nature_astralrecal",
    "itemId" : 43381,
    "spellKey" : 58058,
    "spellId" : 58058,
    "prettyName" : "Astraler Rückruf",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Arktischer Wolf'",
    "id" : 471,
    "type" : 1,
    "description" : "Ändert das Erscheinungsbild Eurer Geisterwolfgestalt ab, in der Ihr daraufhin als arktischer Wolf erscheint.",
    "icon" : "ability_mount_whitedirewolf",
    "itemId" : 43386,
    "spellKey" : 58135,
    "spellId" : 58135,
    "prettyName" : "Arktischer Wolf",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Erneuertes Leben'",
    "id" : 473,
    "type" : 1,
    "description" : "Euer Zauber 'Reinkarnation' benötigt keine Reagenzien mehr.",
    "icon" : "spell_shaman_improvedreincarnation",
    "itemId" : 43385,
    "spellKey" : 58059,
    "spellId" : 58059,
    "prettyName" : "Erneuertes Leben",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Wasseratmung'",
    "id" : 474,
    "type" : 1,
    "description" : "Für den Zauber 'Wasseratmung' werden keine Reagenzien mehr benötigt.",
    "icon" : "spell_shadow_demonbreath",
    "itemId" : 43344,
    "spellKey" : 89646,
    "spellId" : 89646,
    "prettyName" : "Wasseratmung",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Wasserwandeln'",
    "id" : 476,
    "type" : 1,
    "description" : "Für den Zauber 'Wasserwandeln' werden keine Reagenzien mehr benötigt.",
    "icon" : "spell_frost_windwalkon",
    "itemId" : 43388,
    "spellKey" : 58057,
    "spellId" : 58057,
    "prettyName" : "Wasserwandeln",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Geisterwolf'",
    "id" : 552,
    "type" : 0,
    "description" : "In Geisterwolfgestalt wird Euer Bewegungstempo um zusätzlich 5% erhöht.",
    "icon" : "spell_nature_spiritwolf",
    "itemId" : 43725,
    "spellKey" : 59289,
    "spellId" : 59289,
    "prettyName" : "Geisterwolf",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Gewitter'",
    "id" : 612,
    "type" : 1,
    "description" : "Erhöht die Menge an Mana, die Ihr durch Euren Zauber 'Gewitter' erhaltet, um 2%, Feinde werden jedoch nicht mehr zurückgestoßen oder verlangsamt.",
    "icon" : "spell_shaman_thunderstorm",
    "itemId" : 44923,
    "spellKey" : 62132,
    "spellId" : 62132,
    "prettyName" : "Gewitter",
    "typeOrder" : 0
  }, {
    "name" : "Glyphe 'Donner'",
    "id" : 735,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Gewitter' um 10 Sek.",
    "icon" : "spell_shaman_thunderstorm",
    "itemId" : 45770,
    "spellKey" : 63270,
    "spellId" : 63270,
    "prettyName" : "Donner",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Wildgeist'",
    "id" : 736,
    "type" : 2,
    "description" : "Eure Geisterwölfe erhalten zusätzliche 30% Eurer Angriffskraft.",
    "icon" : "spell_shaman_feralspirit",
    "itemId" : 45771,
    "spellKey" : 63271,
    "spellId" : 63271,
    "prettyName" : "Wildgeist",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Springflut'",
    "id" : 737,
    "type" : 2,
    "description" : "Erhöht die Dauer Eures Zaubers 'Springflut' um 40%.",
    "icon" : "spell_nature_riptide",
    "itemId" : 45772,
    "spellKey" : 63273,
    "spellId" : 63273,
    "prettyName" : "Springflut",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Erdschild'",
    "id" : 751,
    "type" : 2,
    "description" : "Erhöht den von Eurem Zauber 'Erdschild' geheilten Wert um 20%.",
    "icon" : "spell_nature_skinofearth",
    "itemId" : 45775,
    "spellKey" : 63279,
    "spellId" : 63279,
    "prettyName" : "Erdschild",
    "typeOrder" : 2
  }, {
    "name" : "Glyphe 'Schamanistische Wut'",
    "id" : 752,
    "type" : 0,
    "description" : "Wenn Ihr Eure Fähigkeit 'Schamanistische Wut' aktiviert, werden gleichzeitig alle bannbaren magischen Schwächungszauber entfernt.",
    "icon" : "spell_nature_shamanrage",
    "itemId" : 45776,
    "spellKey" : 63280,
    "spellId" : 63280,
    "prettyName" : "Schamanistische Wut",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Verhexen'",
    "id" : 753,
    "type" : 0,
    "description" : "Verringert die Abklingzeit Eures Zaubers 'Verhexen' um 10 Sek.",
    "icon" : "spell_shaman_hex",
    "itemId" : 45777,
    "spellKey" : 63291,
    "spellId" : 63291,
    "prettyName" : "Verhexen",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Totem der Steinklaue'",
    "id" : 754,
    "type" : 0,
    "description" : "Euer Totem der Steinklaue belegt Euch mit einem Schaden absorbierenden Schild, der 4-mal so stark ist wie der Schild, mit dem es Eure Totems belegt.",
    "icon" : "spell_nature_stoneclawtotem",
    "itemId" : 45778,
    "spellKey" : 63298,
    "spellId" : 63298,
    "prettyName" : "Totem der Steinklaue",
    "typeOrder" : 1
  }, {
    "name" : "Glyphe 'Entfesselter Blitz'",
    "id" : 950,
    "type" : 2,
    "description" : "Gestattet, dass Euer Zauber 'Blitzschlag' aus der Bewegung heraus gewirkt werden kann.",
    "icon" : "spell_nature_lightning",
    "itemId" : 71155,
    "spellKey" : 101052,
    "spellId" : 101052,
    "prettyName" : "Entfesselter Blitz",
    "typeOrder" : 2
  } ]
}