{
    "talentData": {
        "characterClass": {
            "classId": 6,
            "name": "Todesritter",
            "powerType": "RUNIC_POWER",
            "powerTypeId": 6,
            "powerTypeSlug": "runic-power"
        },
        "talentTrees": [
            {
                "name": "Blut",
                "icon": "spell_deathknight_bloodpresence",
                "backgroundFile": "DeathKnightBlood",
                "overlayColor": "#ff0000",
                "description": "Ein dunkler Wächter, der Lebensenergien manipuliert und korrumpiert, um sich selbst in der Schlacht zu stärken.",
                "treeNo": 0,
                "roles": {
                    "tank": true,
                    "healer": false,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1939,
                        "name": "Schlächter",
                        "icon": "inv_axe_68",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Wenn Ihr einen Feind tötet, der Erfahrung oder Ehre gewährt, erzeugt Ihr bis zu 10 Runenmacht. Zusätzlich wird alle 5 Sek. 1 Runenmacht erzeugt, wenn Ihr Euch im Kampf befindet."
                            },
                            {
                                "description": "Wenn Ihr einen Feind tötet, der Erfahrung oder Ehre gewährt, erzeugt Ihr bis zu 20 Runenmacht. Zusätzlich wird alle 5 Sek. 2 Runenmacht erzeugt, wenn Ihr Euch im Kampf befindet."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1945,
                        "name": "Subversion",
                        "icon": "spell_deathknight_subversion",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Blutstoß', 'Geißelstoß', 'Herzstoß' und 'Auslöschen' um 3% und verringert die in Blut- oder unheiliger Präsenz erzeugte Bedrohung um 8%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Blutstoß', 'Geißelstoß', 'Herzstoß' und 'Auslöschen' um 6% und verringert die in Blut- oder unheiliger Präsenz erzeugte Bedrohung um 16%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Blutstoß', 'Geißelstoß', 'Herzstoß' und 'Auslöschen' um 9% und verringert die in Blut- oder unheiliger Präsenz erzeugte Bedrohung um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2017,
                        "name": "Klingenbarriere",
                        "icon": "ability_upgrademoonglaive",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Immer, wenn Eure Blutrunen von Abklingzeit betroffen sind, gewinnt Ihr den Effekt 'Klingenbarriere', der Euren erlittenen Schaden die nächsten 10 Sek. lang um 1% verringert."
                            },
                            {
                                "description": "Immer, wenn Eure Blutrunen von Abklingzeit betroffen sind, gewinnt Ihr den Effekt 'Klingenbarriere', der Euren erlittenen Schaden die nächsten 10 Sek. lang um 2% verringert."
                            },
                            {
                                "description": "Immer, wenn Eure Blutrunen von Abklingzeit betroffen sind, gewinnt Ihr den Effekt 'Klingenbarriere', der Euren erlittenen Schaden die nächsten 10 Sek. lang um 3% verringert."
                            },
                            {
                                "description": "Immer, wenn Eure Blutrunen von Abklingzeit betroffen sind, gewinnt Ihr den Effekt 'Klingenbarriere', der Euren erlittenen Schaden die nächsten 10 Sek. lang um 4% verringert."
                            },
                            {
                                "description": "Immer, wenn Eure Blutrunen von Abklingzeit betroffen sind, gewinnt Ihr den Effekt 'Klingenbarriere', der Euren erlittenen Schaden die nächsten 10 Sek. lang um 5% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1938,
                        "name": "Klingenbewehrte Rüstung",
                        "icon": "inv_shoulder_36",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Pro 180 Rüstungswert wird Eure Angriffskraft um 1 erhöht."
                            },
                            {
                                "description": "Pro 180 Rüstungswert wird Eure Angriffskraft um 2 erhöht."
                            },
                            {
                                "description": "Pro 180 Rüstungswert wird Eure Angriffskraft um 3 erhöht."
                            },
                            {
                                "description": "Pro 180 Rüstungswert wird Eure Angriffskraft um 4 erhöht."
                            },
                            {
                                "description": "Pro 180 Rüstungswert wird Eure Angriffskraft um 5 erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1948,
                        "name": "Blutgeruch",
                        "icon": "ability_rogue_bloodyeye",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Ihr habt eine Chance von 15%, den Effekt 'Blutgeruch' zu erhalten, wenn Ihr direkten Schaden erlitten habt, ausgewichen seid oder pariert habt. Durch diesen Effekt erzeugt Euer nächster Nahkampftreffer 10 Runenmacht."
                            },
                            {
                                "description": "Ihr habt eine Chance von 15%, den Effekt 'Blutgeruch' zu erhalten, wenn Ihr direkten Schaden erlitten habt, ausgewichen seid oder pariert habt. Durch diesen Effekt erzeugen Eure nächsten 2 Nahkampftreffer 10 Runenmacht."
                            },
                            {
                                "description": "Ihr habt eine Chance von 15%, den Effekt 'Blutgeruch' zu erhalten, wenn Ihr direkten Schaden erlitten habt, ausgewichen seid oder pariert habt. Durch diesen Effekt erzeugen Eure nächsten 3 Nahkampftreffer 10 Runenmacht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2217,
                        "name": "Zweihandwaffen-Spezialisierung",
                        "icon": "inv_sword_68",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1941,
                        "name": "Runenheilung",
                        "icon": "spell_deathknight_runetap",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "cost": "1 Blut",
                                "description": "Wandelt 1 Blutrune in 10% Eurer maximalen Gesundheit um."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1943,
                        "name": "Dunkle Überzeugung",
                        "icon": "spell_deathknight_darkconviction",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Waffen, Zaubern und Fähigkeiten einen kritischen Treffer zu erzielen um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen, Zaubern und Fähigkeiten einen kritischen Treffer zu erzielen um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen, Zaubern und Fähigkeiten einen kritischen Treffer zu erzielen um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen, Zaubern und Fähigkeiten einen kritischen Treffer zu erzielen um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen, Zaubern und Fähigkeiten einen kritischen Treffer zu erzielen um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2086,
                        "name": "Meister der Todesrunen",
                        "icon": "inv_sword_62",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Wenn Ihr mit Euren Fähigkeiten 'Todesstoß' oder 'Auslöschen' einen Treffer erzielt, besteht eine Chance von 33%, dass Eure Frostrunen und unheiligen Runen bei ihrer Aktivierung zu Todesrunen werden. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            },
                            {
                                "description": "Wenn Ihr mit Euren Fähigkeiten 'Todesstoß' oder 'Auslöschen' einen Treffer erzielt, besteht eine Chance von 66%, dass Eure Frostrunen und unheiligen Runen bei ihrer Aktivierung zu Todesrunen werden. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            },
                            {
                                "description": "Wenn Ihr mit Euren Fähigkeiten 'Todesstoß' oder 'Auslöschen' einen Treffer erzielt, besteht eine Chance von 100%, dass Eure Frostrunen und unheiligen Runen bei ihrer Aktivierung zu Todesrunen werden. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1942,
                        "name": "Verbesserte Runenheilung",
                        "icon": "spell_deathknight_runetap",
                        "x": 0,
                        "y": 3,
                        "req": 1941,
                        "ranks": [
                            {
                                "description": "Erhöht die durch Eure 'Runenheilung' gewährte Gesundheit um 33% und verringert ihre Abklingzeit um 10 Sek."
                            },
                            {
                                "description": "Erhöht die durch Eure 'Runenheilung' gewährte Gesundheit um 66% und verringert ihre Abklingzeit um 20 Sek."
                            },
                            {
                                "description": "Erhöht die durch Eure 'Runenheilung' gewährte Gesundheit um 100% und verringert ihre Abklingzeit um 30 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2018,
                        "name": "Zauberabwehr",
                        "icon": "spell_deathknight_spelldeflection",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Ihr habt eine Chance, die dem Wert Eurer Parierchance entspricht, dass ein direkter Schadenszauber 15% weniger Schaden verursacht."
                            },
                            {
                                "description": "Ihr habt eine Chance, die dem Wert Eurer Parierchance entspricht, dass ein direkter Schadenszauber 30% weniger Schaden verursacht."
                            },
                            {
                                "description": "Ihr habt eine Chance, die dem Wert Eurer Parierchance entspricht, dass ein direkter Schadenszauber 45% weniger Schaden verursacht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1953,
                        "name": "Vendetta",
                        "icon": "spell_deathknight_vendetta",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Heilt Euch um bis zu 2% Eurer maximalen Gesundheit, wenn Ihr ein Ziel tötet, das Erfahrung oder Ehre gewährt."
                            },
                            {
                                "description": "Heilt Euch um bis zu 4% Eurer maximalen Gesundheit, wenn Ihr ein Ziel tötet, das Erfahrung oder Ehre gewährt."
                            },
                            {
                                "description": "Heilt Euch um bis zu 6% Eurer maximalen Gesundheit, wenn Ihr ein Ziel tötet, das Erfahrung oder Ehre gewährt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2015,
                        "name": "Blutige Stöße",
                        "icon": "spell_deathknight_deathstrike",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Blutstoß' um 5%, der Fähigkeit 'Herzstoß' um 15% und erhöht den Schaden von 'Siedendes Blut' um 10%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Blutstoß' um 10%, der Fähigkeit 'Herzstoß' um 30% und erhöht den Schaden von 'Siedendes Blut' um 20%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Blutstoß' um 15%, der Fähigkeit 'Herzstoß' um 45% und erhöht den Schaden von 'Siedendes Blut' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1950,
                        "name": "Veteran des Dritten Krieges",
                        "icon": "spell_misc_warsongfocus",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Stärke um 2%, Eure Ausdauer um 1%, und Eure Waffenkunde um 2."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 4%, Eure Ausdauer um 2%, und Eure Waffenkunde um 4."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 6%, Eure Ausdauer um 3%, und Eure Waffenkunde um 6."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1949,
                        "name": "Mal des Blutes",
                        "icon": "ability_hunter_rapidkilling",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Belegt einen Feind mit einem Mal des Blutes. Wenn dieser Feind Schaden verursacht, wird sein Ziel um 4% der eigenen maximalen Gesundheit geheilt. Hält 20 Sek. lang oder bis zu 20 Treffer lang an.",
                                "cost": "1 Blut",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1944,
                        "name": "Blutige Vergeltung",
                        "icon": "ability_backstab",
                        "x": 1,
                        "y": 5,
                        "req": 1943,
                        "ranks": [
                            {
                                "description": "Gewährt Euch nach dem Erzielen eines kritischen Treffers mit einem automatischen Angriff, einem Zauber oder einer Fähigkeit 30 Sek. lang einen Bonus von 1% auf den von Euch verursachten körperlichen Schaden. Dieser Effekt ist bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Gewährt Euch nach dem Erzielen eines kritischen Treffers mit einem automatischen Angriff, einem Zauber oder einer Fähigkeit 30 Sek. lang einen Bonus von 2% auf den von Euch verursachten körperlichen Schaden. Dieser Effekt ist bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Gewährt Euch nach dem Erzielen eines kritischen Treffers mit einem automatischen Angriff, einem Zauber oder einer Fähigkeit 30 Sek. lang einen Bonus von 3% auf den von Euch verursachten körperlichen Schaden. Dieser Effekt ist bis zu 3-mal stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2105,
                        "name": "Stärke der Monstrosität",
                        "icon": "ability_warrior_intensifyrage",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die Angriffskraft aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 5%. Erhöht zudem Eure gesamte Stärke um 1%."
                            },
                            {
                                "description": "Erhöht die Angriffskraft aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 10%. Erhöht zudem Eure gesamte Stärke um 2%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1960,
                        "name": "Blutwürmer",
                        "icon": "spell_shadow_soulleech",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Eure Treffer mit Waffen haben eine Chance von 3%, dass aus dem Ziel 2-4 Blutwürmer hervorbrechen. Blutwürmer greifen Eure Feinde an und heilen Euch in Höhe des verursachten Schadens. Blutwürmer bestehen 20 Sekunden, oder bis sie getötet werden."
                            },
                            {
                                "description": "Eure Treffer mit Waffen haben eine Chance von 6%, dass aus dem Ziel 2-4 Blutwürmer hervorbrechen. Blutwürmer greifen Eure Feinde an und heilen Euch in Höhe des verursachten Schadens. Blutwürmer bestehen 20 Sekunden, oder bis sie getötet werden."
                            },
                            {
                                "description": "Eure Treffer mit Waffen haben eine Chance von 9%, dass aus dem Ziel 2-4 Blutwürmer hervorbrechen. Blutwürmer greifen Eure Feinde an und heilen Euch in Höhe des verursachten Schadens. Blutwürmer bestehen 20 Sekunden, oder bis sie getötet werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1954,
                        "name": "Bösartigkeit",
                        "icon": "spell_deathknight_bladedarmor",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "30 Meter Reichweite",
                                "description": "Versetzt eine freundliche Einheit 30 Sek. lang in einen Blutrausch. Das Ziel wird wütend und sein verursachter körperlicher Schaden um 20% erhöht. Es verliert jedoch pro Sekunde Gesundheit in Höhe von 1% seiner maximalen Gesundheit."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1936,
                        "name": "Verbesserte Blutpräsenz",
                        "icon": "spell_deathknight_bloodpresence",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "In der Frost- oder unheiligen Präsenz behaltet Ihr 2% der Heilung der Blutpräsenz bei. In Blutpräsenz wird Eure erhaltene Heilung um 5% erhöht."
                            },
                            {
                                "description": "In der Frost- oder unheiligen Präsenz behaltet Ihr 4% der Heilung der Blutpräsenz bei. In Blutpräsenz wird Eure erhaltene Heilung um 10% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2259,
                        "name": "Verbesserter Todesstoß",
                        "icon": "spell_deathknight_butcher2",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Todesstoß' um 15%, ihre kritische Trefferchance um 3% und die gewährte Heilung um 25%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Todesstoß' um 30%, ihre kritische Trefferchance um 6% und die gewährte Heilung um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1955,
                        "name": "Hereinbrechende Verdammnis",
                        "icon": "spell_shadow_painspike",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten 'Blutstoß' und 'Herzstoß' haben eine Chance von 5%, dass zusätzlich auf das Ziel der Zauber 'Todesmantel' kostenlos gewirkt wird."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Blutstoß' und 'Herzstoß' haben eine Chance von 10%, dass zusätzlich auf das Ziel der Zauber 'Todesmantel' kostenlos gewirkt wird."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Blutstoß' und 'Herzstoß' haben eine Chance von 15%, dass zusätzlich auf das Ziel der Zauber 'Todesmantel' kostenlos gewirkt wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2019,
                        "name": "Vampirblut",
                        "icon": "spell_shadow_lifedrain",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "cost": "1 Blut",
                                "description": "Gewährt dem Todesritter zeitweise 15% seiner maximalen Gesundheit und erhöht die durch Zauber und Effekte erzeugte Gesundheit 10 Sek. lang um 35%. Wenn der Effekt endet, geht die zusätzliche Gesundheit verloren."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1959,
                        "name": "Wille der Nekropole",
                        "icon": "ability_creature_cursed_02",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Schaden, der Eure Gesundheit unter 35% sinken lassen würde, oder den Ihr mit 35% Gesundheit erleidet, wird um 5% verringert."
                            },
                            {
                                "description": "Schaden, der Eure Gesundheit unter 35% sinken lassen würde, oder den Ihr mit 35% Gesundheit erleidet, wird um 10% verringert."
                            },
                            {
                                "description": "Schaden, der Eure Gesundheit unter 35% sinken lassen würde, oder den Ihr mit 35% Gesundheit erleidet, wird um 15% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1957,
                        "name": "Herzstoß",
                        "icon": "inv_weapon_shortblade_40",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "5 Meter Reichweite",
                                "cost": "1 Blut",
                                "description": "Schlägt sofort das Ziel und seinen nächsten Verbündeten. Dem Primärziel werden 50% Waffenschaden plus 125 zugefügt, dem Sekundärziel 25% Waffenschaden plus 63. Für jede Eurer auf das jeweilige Ziel wirkenden Krankheiten erleidet es zusätzlich 10% Schaden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1958,
                        "name": "Mograines Macht",
                        "icon": "spell_deathknight_classicon",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Blutstoß', 'Herzstoß', 'Todesstoß' und 'Siedendes Blut' um 15%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Blutstoß', 'Herzstoß', 'Todesstoß' und 'Siedendes Blut' um 30%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Blutstoß', 'Herzstoß', 'Todesstoß' und 'Siedendes Blut' um 45%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2034,
                        "name": "Blutsättigung",
                        "icon": "spell_nature_reincarnation",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Wenn Eure Gesundheit mehr als 75% beträgt, verursacht Ihr 2% mehr Schaden. Zusätzlich ignorieren Eure Angriffe stets bis zu 2% der Rüstung Eures Gegners."
                            },
                            {
                                "description": "Wenn Eure Gesundheit mehr als 75% beträgt, verursacht Ihr 4% mehr Schaden. Zusätzlich ignorieren Eure Angriffe stets bis zu 4% der Rüstung Eures Gegners."
                            },
                            {
                                "description": "Wenn Eure Gesundheit mehr als 75% beträgt, verursacht Ihr 6% mehr Schaden. Zusätzlich ignorieren Eure Angriffe stets bis zu 6% der Rüstung Eures Gegners."
                            },
                            {
                                "description": "Wenn Eure Gesundheit mehr als 75% beträgt, verursacht Ihr 8% mehr Schaden. Zusätzlich ignorieren Eure Angriffe stets bis zu 8% der Rüstung Eures Gegners."
                            },
                            {
                                "description": "Wenn Eure Gesundheit mehr als 75% beträgt, verursacht Ihr 10% mehr Schaden. Zusätzlich ignorieren Eure Angriffe stets bis zu 10% der Rüstung Eures Gegners."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1961,
                        "name": "Tanzende Runenwaffe",
                        "icon": "inv_sword_07",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Beschwört eine zweite Runenwaffe, die 12 Sek. lang eigenständig kämpft. Die Waffe imitiert die Angriffe des Todesritters, verursacht jedoch 50% weniger Schaden.",
                                "cost": "60 Runenmacht",
                                "castTime": "Spontanzauber",
                                "cooldown": "1,5 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Frost",
                "icon": "spell_deathknight_Frostpresence",
                "backgroundFile": "DeathKnightFrost",
                "overlayColor": "#4c7fff",
                "description": "Ein eisiger Verdammnisbote, der Runenmacht kanalisiert und schnelle Angriffe mit Waffen beherrscht.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 2031,
                        "name": "Verbesserte eisige Berührung",
                        "icon": "spell_deathknight_icetouch",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeit 'Eisige Berührung' verursacht 5% zusätzlichen Schaden und Euer Frostfieber verringert das Nahkampf- und Distanzangriffstempo um zusätzliche 2%."
                            },
                            {
                                "description": "Eure Fähigkeit 'Eisige Berührung' verursacht 10% zusätzlichen Schaden und Euer Frostfieber verringert das Nahkampf- und Distanzangriffstempo um zusätzliche 4%."
                            },
                            {
                                "description": "Eure Fähigkeit 'Eisige Berührung' verursacht 15% zusätzlichen Schaden und Euer Frostfieber verringert das Nahkampf- und Distanzangriffstempo um zusätzliche 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2020,
                        "name": "Meister der Runenmacht",
                        "icon": "spell_arcane_arcane01",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure maximale Runenmacht um 15."
                            },
                            {
                                "description": "Erhöht Eure maximale Runenmacht um 30."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1968,
                        "name": "Zähigkeit",
                        "icon": "spell_holy_devotion",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 2% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 6%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 4% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 12%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 6% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 18%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 8% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 24%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 10% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2035,
                        "name": "Eisige Reichweite",
                        "icon": "spell_Frost_manarecharge",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Eisige Berührung', 'Eisketten' und 'Heulende Böe' um 5 Meter."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Eisige Berührung', 'Eisketten' und 'Heulende Böe' um 10 Meter."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1973,
                        "name": "Schwarzes Eis",
                        "icon": "spell_shadow_darkritual",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Frost- und Schattenschaden um 2%."
                            },
                            {
                                "description": "Erhöht Euren Frost- und Schattenschaden um 4%."
                            },
                            {
                                "description": "Erhöht Euren Frost- und Schattenschaden um 6%."
                            },
                            {
                                "description": "Erhöht Euren Frost- und Schattenschaden um 8%."
                            },
                            {
                                "description": "Erhöht Euren Frost- und Schattenschaden um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2022,
                        "name": "Nerven aus kaltem Stahl",
                        "icon": "ability_dualwield",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Trefferchance mit Einhandwaffen um 1% und erhöht den von Eurer Schildhandwaffe verursachten Schaden um 8%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance mit Einhandwaffen um 2% und erhöht den von Eurer Schildhandwaffe verursachten Schaden um 16%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance mit Einhandwaffen um 3% und erhöht den von Eurer Schildhandwaffe verursachten Schaden um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2042,
                        "name": "Eisige Klauen",
                        "icon": "spell_deathknight_icytalons",
                        "x": 0,
                        "y": 2,
                        "req": 2031,
                        "ranks": [
                            {
                                "description": "Entzieht den Opfern Eures Frostfiebers die Wärme. Während ihr Nahkampfangriffstempo verringert wird, wird Eures für die nächsten 20 Sek. um 4% erhöht."
                            },
                            {
                                "description": "Entzieht den Opfern Eures Frostfiebers die Wärme. Während ihr Nahkampfangriffstempo verringert wird, wird Eures für die nächsten 20 Sek. um 8% erhöht."
                            },
                            {
                                "description": "Entzieht den Opfern Eures Frostfiebers die Wärme. Während ihr Nahkampfangriffstempo verringert wird, wird Eures für die nächsten 20 Sek. um 12% erhöht."
                            },
                            {
                                "description": "Entzieht den Opfern Eures Frostfiebers die Wärme. Während ihr Nahkampfangriffstempo verringert wird, wird Eures für die nächsten 20 Sek. um 16% erhöht."
                            },
                            {
                                "description": "Entzieht den Opfern Eures Frostfiebers die Wärme. Während ihr Nahkampfangriffstempo verringert wird, wird Eures für die nächsten 20 Sek. um 20% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2215,
                        "name": "Lichritter",
                        "icon": "spell_shadow_raisedead",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Erfüllt Euch mit unheiligen Energien, um 10 Sek. lang untot zu werden. Im untoten Zustand seid Ihr immun gegenüber Bezauberungs-, Furcht- und Schlafeffekten."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2048,
                        "name": "Vernichtung",
                        "icon": "inv_weapon_hand_18",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Nahkampf-Spezialfähigkeiten um 1%. Zusätzlich besteht eine Chance von 33%, dass Eure Fähigkeit 'Auslöschen' Schaden verursacht, ohne Krankheiten aufzuzehren."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Nahkampf-Spezialfähigkeiten um 2%. Zusätzlich besteht eine Chance von 66%, dass Eure Fähigkeit 'Auslöschen' Schaden verursacht, ohne Krankheiten aufzuzehren."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Nahkampf-Spezialfähigkeiten um 3%. Zusätzlich besteht eine Chance von 100%, dass Eure Fähigkeit 'Auslöschen' Schaden verursacht, ohne Krankheiten aufzuzehren."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2044,
                        "name": "Tötungsmaschine",
                        "icon": "inv_sword_122",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Eure Nahkampfangriffe haben eine Chance, dass Euer nächstes Wirken von 'Eisige Berührung', 'Heulende Böe' oder 'Froststoß' kritisch trifft."
                            },
                            {
                                "description": "Eure Nahkampfangriffe haben eine Chance, dass Euer nächstes Wirken von 'Eisige Berührung', 'Heulende Böe' oder 'Froststoß' kritisch trifft. Dieser Effekt wird häufiger ausgelöst als 'Tötungsmaschine' (Rang 1)."
                            },
                            {
                                "description": "Eure Nahkampfangriffe haben eine Chance, dass Euer nächstes Wirken von 'Eisige Berührung', 'Heulende Böe' oder 'Froststoß' kritisch trifft. Dieser Effekt wird häufiger ausgelöst als 'Tötungsmaschine' (Rang 2)."
                            },
                            {
                                "description": "Eure Nahkampfangriffe haben eine Chance, dass Euer nächstes Wirken von 'Eisige Berührung', 'Heulende Böe' oder 'Froststoß' kritisch trifft. Dieser Effekt wird häufiger ausgelöst als 'Tötungsmaschine' (Rang 3)."
                            },
                            {
                                "description": "Eure Nahkampfangriffe haben eine Chance, dass Euer nächstes Wirken von 'Eisige Berührung', 'Heulende Böe' oder 'Froststoß' kritisch trifft. Dieser Effekt wird häufiger ausgelöst als 'Tötungsmaschine' (Rang 4)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1981,
                        "name": "Grabeskühle",
                        "icon": "spell_Frost_Frostshock",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Eisige Berührung', 'Eisketten', 'Heulende Böe' und 'Auslöschen' erzeugen 2,5 zusätzliche Runenmacht."
                            },
                            {
                                "description": "Eure Zauber 'Eisige Berührung', 'Eisketten', 'Heulende Böe' und 'Auslöschen' erzeugen 5 zusätzliche Runenmacht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1971,
                        "name": "Endloser Winter",
                        "icon": "spell_shadow_twilight",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Eure Stärke wird um 2% erhöht und die Kosten von 'GedankenFrost' sind auf 10 Runenmacht verringert."
                            },
                            {
                                "description": "Eure Stärke wird um 4% erhöht und die Fähigkeit 'GedankenFrost' kostet keine Runenmacht mehr."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1990,
                        "name": "Eisige Schreckensplatte",
                        "icon": "inv_chest_mail_04",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Chance, dass Ihr von Nahkampfangriffen getroffen werdet, um 1%."
                            },
                            {
                                "description": "Verringert die Chance, dass Ihr von Nahkampfangriffen getroffen werdet, um 2%."
                            },
                            {
                                "description": "Verringert die Chance, dass Ihr von Nahkampfangriffen getroffen werdet, um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2030,
                        "name": "Gletscherverwesung",
                        "icon": "spell_nature_removedisease",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erkrankte Ziele erleiden durch Eure Zauber 'Eisige Berührung', 'Heulende Böe' und 'Froststoß' 7% mehr Schaden."
                            },
                            {
                                "description": "Erkrankte Ziele erleiden durch Eure Zauber 'Eisige Berührung', 'Heulende Böe' und 'Froststoß' 13% mehr Schaden."
                            },
                            {
                                "description": "Erkrankte Ziele erleiden durch Eure Zauber 'Eisige Berührung', 'Heulende Böe' und 'Froststoß' 20% mehr Schaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1980,
                        "name": "Kälte des Todes",
                        "icon": "spell_shadow_soulleech_2",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Bei Aktivierung erzielt Euer nächstes Wirken von 'Eisige Berührung', 'Heulende Böe', 'Froststoß' oder 'Auslöschen' innerhalb von 30 Sek. einen kritischen Treffer."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2223,
                        "name": "Verbesserte eisige Klauen",
                        "icon": "spell_deathknight_icytalons",
                        "x": 0,
                        "y": 5,
                        "req": 2042,
                        "ranks": [
                            {
                                "description": "Erhöht das Nahkampfangriffstempo aller Gruppen- oder Schlachtzugsmitglieder im Umkreis von 100 Metern um 20%, Euer Tempo wird zusätzlich um 5% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1993,
                        "name": "Gnadenloser Kampf",
                        "icon": "inv_sword_112",
                        "x": 1,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten 'Eisige Berührung', 'Heulende Böe', 'Auslöschen' und 'Froststoß' verursachen 6% zusätzlichen Schaden, wenn das Ziel über weniger als 35% Gesundheit verfügt."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Eisige Berührung', 'Heulende Böe', 'Auslöschen' und 'Froststoß' verursachen 12% zusätzlichen Schaden, wenn das Ziel über weniger als 35% Gesundheit verfügt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1992,
                        "name": "Raureif",
                        "icon": "spell_Frost_freezingbreath",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Eisige Berührung' und 'Auslöschen' um 5%. Zudem besteht beim Wirken von 'Auslöschen' eine Chance von 5%, dass die Abklingzeit Eurer Fähigkeit 'Heulende Böe' abgeschlossen wird und beim nächsten Wirken von 'Heulende Böe' keine Runen verbraucht werden."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Eisige Berührung' und 'Auslöschen' um 10%. Zudem besteht beim Wirken von 'Auslöschen' eine Chance von 10%, dass die Abklingzeit Eurer Fähigkeit 'Heulende Böe' abgeschlossen wird und beim nächsten Wirken von 'Heulende Böe' keine Runen verbraucht werden."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Eisige Berührung' und 'Auslöschen' um 15%. Zudem besteht beim Wirken von 'Auslöschen' eine Chance von 15%, dass die Abklingzeit Eurer Fähigkeit 'Heulende Böe' abgeschlossen wird und beim nächsten Wirken von 'Heulende Böe' keine Runen verbraucht werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2260,
                        "name": "Frostbeulen",
                        "icon": "spell_Frost_wisp",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Opfer Eures Frostfiebers werden von Kälte betroffen, welche ihr Bewegungstempo 10 Sek. lang um 15% verringert."
                            },
                            {
                                "description": "Opfer Eures Frostfiebers werden von Kälte betroffen, welche ihr Bewegungstempo 10 Sek. lang um 30% verringert."
                            },
                            {
                                "description": "Opfer Eures Frostfiebers werden von Kälte betroffen, welche ihr Bewegungstempo 10 Sek. lang um 50% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1999,
                        "name": "Zehrende Kälte",
                        "icon": "inv_staff_15",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "cost": "40 Runenmacht",
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "description": "Entzieht der Erde unter dem Todesritter jegliche Wärme. Feinde innerhalb von 10 Metern werden in Eis eingeschlossen, das 10 Sek. lang jegliche ihrer Aktionen verhindert und sie mit Frostfieber ansteckt. Feinde zählen als eingefroren, jeglicher nicht von Krankheiten verursachter Schaden unterbricht den Effekt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2029,
                        "name": "Verbesserte Frostpräsenz",
                        "icon": "spell_deathknight_Frostpresence",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "In der Blut- oder unheiligen Präsenz behaltet Ihr 4% der Ausdauer der Frostpräsenz bei. In Frostpräsenz wird Euer erlittener Schaden um zusätzliche 1% verringert."
                            },
                            {
                                "description": "In der Blut- oder unheiligen Präsenz behaltet Ihr 8% der Ausdauer der Frostpräsenz bei. In Frostpräsenz wird Euer erlittener Schaden um zusätzliche 2% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2284,
                        "name": "Thassarians Drohung",
                        "icon": "ability_dualwieldspecialization",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Wenn Ihr zwei Einhandwaffen nutzt, haben Eure Fähigkeiten 'Todesstoß', 'Auslöschen', 'Seuchenstoß', 'Runenstoß', 'Blutstoß' und 'Froststoß' eine Chance von 30%, auch mit der Schildhandwaffe Schaden zu verursachen."
                            },
                            {
                                "description": "Wenn Ihr zwei Einhandwaffen nutzt, haben Eure Fähigkeiten 'Todesstoß', 'Auslöschen', 'Seuchenstoß', 'Runenstoß', 'Blutstoß' und 'Froststoß' eine Chance von 60%, auch mit der Schildhandwaffe Schaden zu verursachen."
                            },
                            {
                                "description": "Wenn Ihr zwei Einhandwaffen nutzt, haben Eure Fähigkeiten 'Todesstoß', 'Auslöschen', 'Seuchenstoß', 'Runenstoß', 'Blutstoß' und 'Froststoß' eine Chance von 100%, auch mit der Schildhandwaffe Schaden zu verursachen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2210,
                        "name": "Blut des Nordens",
                        "icon": "inv_weapon_shortblade_79",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden von 'Blutstoß' und 'Froststoß' um 3%. Wenn Ihr mit Euren Fähigkeiten 'Blutstoß' oder 'Pestilenz' einen Treffer erzielt, besteht eine Chance von 30%, dass eine Blutrune bei ihrer Aktivierung zu einer Todesrune wird. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            },
                            {
                                "description": "Erhöht den Schaden von 'Blutstoß' und 'Froststoß' um 6%. Wenn Ihr mit Euren Fähigkeiten 'Blutstoß' oder 'Pestilenz' einen Treffer erzielt, besteht außerdem eine Chance von 60%, dass eine Blutrune bei ihrer Aktivierung zu einer Todesrune wird. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            },
                            {
                                "description": "Erhöht den Schaden von 'Blutstoß' und 'Froststoß' um 10%. Wenn Ihr mit Euren Fähigkeiten 'Blutstoß' oder 'Pestilenz' einen Treffer erzielt, besteht eine Chance von 100%, dass eine Blutrune bei ihrer Aktivierung zu einer Todesrune wird. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1979,
                        "name": "Undurchdringliche Rüstung",
                        "icon": "inv_armor_helm_plate_naxxramas_raidwarrior_c_01",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "cost": "1 Frost",
                                "description": "Verstärkt Eure Rüstung 20 Sek. lang durch eine dicke Eisschicht, die Eure Rüstung um 25% und Eure Stärke um 20% erhöht."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1997,
                        "name": "Akklimatisierung",
                        "icon": "spell_fire_elementaldevastation",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Werdet Ihr von einem Zauber getroffen, besteht eine Chance von 10%, dass Euer Widerstand gegen diese Art von Magie 18 Sek. lang erhöht wird. Bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Werdet Ihr von einem Zauber getroffen, besteht eine Chance von 20%, dass Euer Widerstand gegen diese Art von Magie 18 Sek. lang erhöht wird. Bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Werdet Ihr von einem Zauber getroffen, besteht eine Chance von 30%, dass Euer Widerstand gegen diese Art von Magie 18 Sek. lang erhöht wird. Bis zu 3-mal stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1975,
                        "name": "Froststoß",
                        "icon": "spell_deathknight_empowerruneblade2",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "5 Meter Reichweite",
                                "cost": "40 Runenmacht",
                                "description": "Schlägt den Feind sofort, verursacht 55% Waffenschaden plus 48 als Frostschaden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2040,
                        "name": "Blutschattens Arglist",
                        "icon": "inv-sword_53",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Blutstoß', 'Froststoß', 'Heulende Böe' und 'Auslöschen' um 15% und erhöht die Dauer Eurer Fähigkeit 'Eisige Gegenwehr' um 2 Sek."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Blutstoß', 'Froststoß', 'Heulende Böe' und 'Auslöschen' um 30% und erhöht die Dauer Eurer Fähigkeit 'Eisige Gegenwehr' um 4 Sek."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Blutstoß', 'Froststoß', 'Heulende Böe' und 'Auslöschen' um 45% und erhöht die Dauer Eurer Fähigkeit 'Eisige Gegenwehr' um 6 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1998,
                        "name": "Tundrajäger",
                        "icon": "spell_nature_tranquility",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an Frostfieber erkrankt sind, 3% mehr Schaden. Erhöht zudem Eure Waffenkunde um 1."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an Frostfieber erkrankt sind, 6% mehr Schaden. Erhöht zudem Eure Waffenkunde um 2."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an Frostfieber erkrankt sind, 9% mehr Schaden. Erhöht zudem Eure Waffenkunde um 3."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an Frostfieber erkrankt sind, 12% mehr Schaden. Erhöht zudem Eure Waffenkunde um 4."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an Frostfieber erkrankt sind, 15% mehr Schaden. Erhöht zudem Eure Waffenkunde um 5."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1989,
                        "name": "Heulende Böe",
                        "icon": "spell_Frost_arcticwinds",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "8 Sek. Abklingzeit",
                                "range": "20 Meter Reichweite",
                                "cost": "1 Unheilig, 1 Frost",
                                "description": "Weht dem Ziel eine eisige Böe entgegen, die allen Gegnern innerhalb von 10 Metern 198 bis 214 Frostschaden zufügt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Unheilig",
                "icon": "spell_deathknight_unholypresence",
                "backgroundFile": "DeathKnightUnholy",
                "overlayColor": "#33cc33",
                "description": "Ein Meister des Todes und des Verfalls, der Krankheiten verbreitet und untote Diener befehligt, die seinem Willen unterworfen sind.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 2082,
                        "name": "Heimtückische Stöße",
                        "icon": "spell_deathknight_plaguestrike",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Die kritische Trefferchance Eurer Fähigkeiten 'Seuchenstoß' und 'Geißelstoß' wird um 3%, ihr kritischer Schadensbonus um 15% erhöht."
                            },
                            {
                                "description": "Die kritische Trefferchance Eurer Fähigkeiten 'Seuchenstoß' und 'Geißelstoß' wird um 6%, ihr kritischer Schadensbonus um 30% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1932,
                        "name": "Virulenz",
                        "icon": "spell_shadow_burningspirit",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubertrefferchance um 1% und verringert die Chance, dass Eure Krankheiten, die Schaden im Laufe der Zeit verursachen, aufgehoben werden, um 10%."
                            },
                            {
                                "description": "Erhöht Eure Zaubertrefferchance um 2% und verringert die Chance, dass Eure Krankheiten, die Schaden im Laufe der Zeit verursachen, aufgehoben werden, um 20%."
                            },
                            {
                                "description": "Erhöht Eure Zaubertrefferchance um 3% und verringert die Chance, dass Eure Krankheiten, die Schaden im Laufe der Zeit verursachen, aufgehoben werden, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2218,
                        "name": "Vorahnung",
                        "icon": "spell_nature_mirrorimage",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance um 1%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 3%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1963,
                        "name": "Epidemie",
                        "icon": "spell_shadow_shadowwordpain",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Dauer Eurer Krankheiten 'Blutseuche' und 'Frostfieber' um 3 Sek."
                            },
                            {
                                "description": "Erhöht die Dauer Eurer Krankheiten 'Blutseuche' und 'Frostfieber' um 6 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1933,
                        "name": "Morbidität",
                        "icon": "spell_shadow_deathanddecay",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden und den Heilungseffekt von 'Todesmantel' um 5% und verringert die Abklingzeit von 'Tod und Verfall' um 5 Sek."
                            },
                            {
                                "description": "Erhöht den Schaden und den Heilungseffekt von 'Todesmantel' um 10% und verringert die Abklingzeit von 'Tod und Verfall' um 10 Sek."
                            },
                            {
                                "description": "Erhöht den Schaden und den Heilungseffekt von 'Todesmantel' um 15% und verringert die Abklingzeit von 'Tod und Verfall' um 15 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2025,
                        "name": "Unheiliges Kommando",
                        "icon": "spell_deathknight_strangulate",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Todesgriff' um 5 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Todesgriff' um 10 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1934,
                        "name": "Gierige Tote",
                        "icon": "spell_deathknight_gnaw_ghoul",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Stärke um 1% sowie den Bonus, den Eure Ghule durch Eure Stärke und Ausdauer erhalten, um 20%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 2% sowie den Bonus, den Eure Ghule durch Eure Stärke und Ausdauer erhalten, um 40%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 3% sowie den Bonus, den Eure Ghule durch Eure Stärke und Ausdauer erhalten, um 60%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2008,
                        "name": "Krankheitsausbruch",
                        "icon": "spell_shadow_plaguecloud",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Der Schaden von 'Seuchenstoß' wird um 10%, der Schaden von 'Geißelstoß' um 7% erhöht."
                            },
                            {
                                "description": "Der Schaden von 'Seuchenstoß' wird um 20%, der Schaden von 'Geißelstoß' um 13% erhöht."
                            },
                            {
                                "description": "Der Schaden von 'Seuchenstoß' wird um 30%, der Schaden von 'Geißelstoß' um 20% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2047,
                        "name": "Nekrose",
                        "icon": "inv_weapon_shortblade_60",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Eure automatischen Angriffe verursachen zusätzlich 4% Schattenschaden."
                            },
                            {
                                "description": "Eure automatischen Angriffe verursachen zusätzlich 8% Schattenschaden."
                            },
                            {
                                "description": "Eure automatischen Angriffe verursachen zusätzlich 12% Schattenschaden."
                            },
                            {
                                "description": "Eure automatischen Angriffe verursachen zusätzlich 16% Schattenschaden."
                            },
                            {
                                "description": "Eure automatischen Angriffe verursachen zusätzlich 20% Schattenschaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1985,
                        "name": "Leichenexplosion",
                        "icon": "ability_creature_disease_02",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Bringt einen Leichnam zum Bersten und fügt allen Feinden innerhalb von 10 Metern 166 Schattenschaden zu. Wird einen nahen Leichnam nutzen, wenn das Ziel kein Leichnam ist. Der Zauber kann nicht auf die Überreste von mechanischen Einheiten oder Elementaren angewandt werden.",
                                "cost": "40 Runenmacht",
                                "castTime": "Spontanzauber",
                                "cooldown": "5 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2039,
                        "name": "Der Tod reit'",
                        "icon": "spell_deathknight_summondeathcharger",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Ihr seid so schwer aufzuhalten, wie der Tod selbst. Die Dauer aller auf Euch wirkenden Betäubungs- und Furchteffekte ist um 10% verringert und das Tempo Eures Reittiers um 10% erhöht. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar."
                            },
                            {
                                "description": "Ihr seid so schwer aufzuhalten, wie der Tod selbst. Die Dauer aller auf Euch wirkenden Betäubungs- und Furchteffekte ist um 20% verringert und das Tempo Eures Reittiers um 20% erhöht. Dieser Effekt ist mit anderen Effekten, die das Bewegungstempo erhöhen, nicht stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2004,
                        "name": "Blutverkrustete Klinge",
                        "icon": "ability_criticalstrike",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Eure automatischen Angriffe haben eine Chance von 10%, einen blutverkrusteten Stoß zu verursachen, der 25% Waffenschaden plus für jede Eurer auf das Ziel wirkenden Krankheiten 12.5% Schaden zufügt."
                            },
                            {
                                "description": "Eure automatischen Angriffe haben eine Chance von 20%, einen blutverkrusteten Stoß zu verursachen, der 25% Waffenschaden plus für jede Eurer auf das Ziel wirkenden Krankheiten 12.5% Schaden zufügt."
                            },
                            {
                                "description": "Eure automatischen Angriffe haben eine Chance von 30%, einen blutverkrusteten Stoß zu verursachen, der 25% Waffenschaden plus für jede Eurer auf das Ziel wirkenden Krankheiten 12.5% Schaden zufügt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2225,
                        "name": "Nacht der Toten",
                        "icon": "spell_deathknight_armyofthedead",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Totenerweckung' um 45 Sek. und die Abklingzeit von 'Armee der Toten' um 2 Min. Verringert zudem den von Eurem Begleiter durch Kreaturen erlittenen Flächenschaden um 45%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Totenerweckung' um 90 Sek. und die Abklingzeit von 'Armee der Toten' um 4 Min. Verringert zudem den von Eurem Begleiter durch Kreaturen erlittenen Flächenschaden um 90%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1996,
                        "name": "Unheilige Verseuchung",
                        "icon": "spell_shadow_contagion",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Opfer Eurer Fähigkeit 'Todesmantel' werden von einer widerwärtigen Wolke unheiliger Insekten umschwärmt, durch die sie im Verlauf von 10 Sek. 10% des Schadens von 'Todesmantel' erleiden. Zudem wird durch die Insekten verhindert, dass auf das Ziel wirkende Krankheiten gebannt werden können."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2005,
                        "name": "Unreinheit",
                        "icon": "spell_shadow_shadowandflame",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Der Angriffskraftbonus Eurer Zauber wird um 4% erhöht."
                            },
                            {
                                "description": "Der Angriffskraftbonus Eurer Zauber wird um 8% erhöht."
                            },
                            {
                                "description": "Der Angriffskraftbonus Eurer Zauber wird um 12% erhöht."
                            },
                            {
                                "description": "Der Angriffskraftbonus Eurer Zauber wird um 16% erhöht."
                            },
                            {
                                "description": "Eure Zauber erhalten durch Eure Angriffskraft einen zusätzlichen Bonus von 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2011,
                        "name": "Grabgesang",
                        "icon": "spell_shadow_shadesofdarkness",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Todesstoß' erzeugen 2,5 zusätzliche Runenmacht."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Seuchenstoß', 'Geißelstoß' und 'Todesstoß' erzeugen 5 zusätzliche Runenmacht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2226,
                        "name": "Entweihung",
                        "icon": "spell_shadow_shadowfiend",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten 'Seuchenstoß' und 'Geißelstoß' verursachen den Effekt 'Entweihter Boden'. Das Bewegungstempo von Zielen in diesem Bereich wird von den gierig klammernden Armen der Toten um 25% verringert. Hält 20 Sek. lang an."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Seuchenstoß' und 'Geißelstoß' verursachen den Effekt 'Entweihter Boden'. Das Bewegungstempo von Zielen in diesem Bereich wird von den gierig klammernden Armen der Toten um 50% verringert. Hält 20 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2009,
                        "name": "Magieunterdrückung",
                        "icon": "spell_shadow_antimagicshell",
                        "x": 1,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Ihr erleidet 2% weniger Schaden durch jegliche Magie. Zudem absorbiert 'Antimagische Hülle' 8% zusätzlichen Zauberschaden."
                            },
                            {
                                "description": "Ihr erleidet 4% weniger Schaden durch jegliche Magie. Zudem absorbiert 'Antimagische Hülle' 16% zusätzlichen Zauberschaden."
                            },
                            {
                                "description": "Ihr erleidet 6% weniger Schaden durch jegliche Magie. Zudem absorbiert 'Antimagische Hülle' 25% zusätzlichen Zauberschaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2001,
                        "name": "Sensenmann",
                        "icon": "spell_shadow_shadetruesight",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Wenn Ihr mit Euren Fähigkeiten 'Blutstoß' oder 'Pestilenz' einen Treffer erzielt, besteht eine Chance von 33%, dass sich eine Blutrune bei ihrer Aktivierung zu einer Todesrune wandelt. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            },
                            {
                                "description": "Wenn Ihr mit Euren Fähigkeiten 'Blutstoß' oder 'Pestilenz' einen Treffer erzielt, besteht eine Chance von 66%, dass sich eine Blutrune bei ihrer Aktivierung zu einer Todesrune wandelt. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            },
                            {
                                "description": "Wenn Ihr mit Euren Fähigkeiten 'Blutstoß' oder 'Pestilenz' einen Treffer erzielt, besteht eine Chance von 100%, dass sich eine Blutrune bei ihrer Aktivierung zu einer Todesrune wandelt. Eine Todesrune zählt als Blut-, Frost- oder unheilige Rune."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1984,
                        "name": "Meister der Ghule",
                        "icon": "spell_shadow_animatedead",
                        "x": 3,
                        "y": 5,
                        "req": 2225,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Totenerweckung' um 1 Min. und von diesem Zauber beschworene Ghule gelten als Begleiter und stehen unter Eurer Kontrolle. Anders als normale Ghule der Todesritter, hat dieser Ghul keine begrenzte Lebensdauer."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2285,
                        "name": "Verwüstung",
                        "icon": "spell_shadow_unholyfrenzy",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Durch Eure Fähigkeit 'Blutstoß' verursacht Ihr die nächsten 20 Sek. lang mit allen Angriffen 1% zusätzlichen Schaden."
                            },
                            {
                                "description": "Durch Eure Fähigkeit 'Blutstoß' verursacht Ihr die nächsten 20 Sek. lang mit allen Angriffen 2% zusätzlichen Schaden."
                            },
                            {
                                "description": "Durch Eure Fähigkeit 'Blutstoß' verursacht Ihr die nächsten 20 Sek. lang mit allen Angriffen 3% zusätzlichen Schaden."
                            },
                            {
                                "description": "Durch Eure Fähigkeit 'Blutstoß' verursacht Ihr die nächsten 20 Sek. lang mit allen Angriffen 4% zusätzlichen Schaden."
                            },
                            {
                                "description": "Durch Eure Fähigkeit 'Blutstoß' verursacht Ihr die nächsten 20 Sek. lang mit allen Angriffen 5% zusätzlichen Schaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2221,
                        "name": "Antimagisches Feld",
                        "icon": "spell_deathknight_antimagiczone",
                        "x": 1,
                        "y": 6,
                        "req": 2009,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "cost": "1 Unheilig",
                                "description": "Platziert ein großes, ortsgebundenes, antimagisches Feld, das den erlittenen Zauberschaden aller darin befindlichen Gruppen- oder Schlachtzugsmitglieder um 75% verringert. Das antimagische Feld hält 10 Sek. lang an, oder bis 10052 Zauberschaden absorbiert wurden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2013,
                        "name": "Verbesserte unheilige Präsenz",
                        "icon": "spell_deathknight_unholypresence",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "In der Blut- oder Frostpräsenz behaltet Ihr 8% des erhöhten Bewegungstempos der unheiligen Präsenz bei. In unheiliger Präsenz schließen Eure Runen ihre Abklingzeiten 5% schneller ab."
                            },
                            {
                                "description": "In der Blut- oder Frostpräsenz behaltet Ihr 15% des erhöhten Bewegungstempos der unheiligen Präsenz bei. In unheiliger Präsenz schließen Eure Runen ihre Abklingzeiten 10% schneller ab."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2085,
                        "name": "Ghulfuror",
                        "icon": "ability_ghoulfrenzy",
                        "x": 3,
                        "y": 6,
                        "req": 1984,
                        "ranks": [
                            {
                                "description": "Gewährt Eurem Begleiter 30 Sek. lang 25% Tempo und heilt ihn während der Dauer des Effektes um 60% seiner Gesundheit.",
                                "cost": "1 Unheilig",
                                "castTime": "Spontanzauber",
                                "cooldown": "10 Sek. Abklingzeit",
                                "range": "45 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1962,
                        "name": "Gruftfieber",
                        "icon": "spell_nature_nullifydisease",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Krankheiten rufen zusätzlich ein Gruftfieber hervor, das den durch Krankheiten erlittenen Schaden des Ziels um 10% erhöht."
                            },
                            {
                                "description": "Eure Krankheiten rufen zusätzlich ein Gruftfieber hervor, das den durch Krankheiten erlittenen Schaden des Ziels um 20% erhöht."
                            },
                            {
                                "description": "Eure Krankheiten rufen zusätzlich ein Gruftfieber hervor, das den durch Krankheiten erlittenen Schaden des Ziels um 30% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2007,
                        "name": "Knochenschild",
                        "icon": "inv_chest_leather_13",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "cost": "1 Unheilig",
                                "description": "Der Todesritter wird von 3 wirbelnden Knochen umgeben. So lang zumindest 1 Knochen verbleibt, wird jeglicher erlittene Schaden um 20% verringert und der Todesritter verursacht mit allen Angriffen, Zaubern und Fähigkeiten 2% mehr Schaden. Jeder Treffer, der Schaden verursacht, braucht einen Knochen auf. Hält 5 Min. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2003,
                        "name": "Wandernde Seuche",
                        "icon": "spell_shadow_callofbone",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Wenn von Euch hervorgerufene Krankheiten an einem feindlichen Ziel Schaden verursachen, besteht eine Chance, die Eurer kritischen Nahkampftrefferchance entspricht, dass die Krankheiten am Ziel und allen Feinden im Umkreis von 8 Metern 33% zusätzlichen Schaden verursachen. Dieser Zauber ignoriert Ziele, auf denen Effekte wirken, die durch Schadenseinwirkung unterbrochen werden."
                            },
                            {
                                "description": "Wenn von Euch hervorgerufene Krankheiten an einem feindlichen Ziel Schaden verursachen, besteht eine Chance, die Eurer kritischen Nahkampftrefferchance entspricht, dass die Krankheiten am Ziel und allen Feinden im Umkreis von 8 Metern 66% zusätzlichen Schaden verursachen. Dieser Zauber ignoriert Ziele, auf denen Effekte wirken, die durch Schadenseinwirkung unterbrochen werden."
                            },
                            {
                                "description": "Wenn von Euch hervorgerufene Krankheiten an einem feindlichen Ziel Schaden verursachen, besteht eine Chance, die Eurer kritischen Nahkampftrefferchance entspricht, dass die Krankheiten am Ziel und allen Feinden im Umkreis von 8 Metern 100% zusätzlichen Schaden verursachen. Dieser Zauber ignoriert Ziele, auf denen Effekte wirken, die durch Schadenseinwirkung unterbrochen werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2043,
                        "name": "Schwarzer Seuchenbringer",
                        "icon": "ability_creature_cursed_03",
                        "x": 1,
                        "y": 8,
                        "req": 1962,
                        "ranks": [
                            {
                                "description": "Euer Gruftfieber wandelt sich zur schwarzen Seuche. Zusätzlich zum erhöhten erlittenen Krankheitsschaden, wird der erlittene Magieschaden des Ziels um 4% erhöht. Zusätzlich wird Eure kritische Trefferchance mit Waffen und Zaubern dauerhaft um 1% erhöht."
                            },
                            {
                                "description": "Euer Gruftfieber wandelt sich zur schwarzen Seuche. Zusätzlich zum erhöhten erlittenen Krankheitsschaden, wird der erlittene Magieschaden des Ziels um 9% erhöht. Zusätzlich wird Eure kritische Trefferchance mit Waffen und Zaubern dauerhaft um 2% erhöht."
                            },
                            {
                                "description": "Euer Gruftfieber wandelt sich zur schwarzen Seuche. Zusätzlich zum erhöhten erlittenen Krankheitsschaden, wird der erlittene Magieschaden des Ziels um 13% erhöht. Zusätzlich wird Eure kritische Trefferchance mit Waffen und Zaubern dauerhaft um 3% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2216,
                        "name": "Geißelstoß",
                        "icon": "spell_deathknight_scourgestrike",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "5 Meter Reichweite",
                                "cost": "1 Unheilig, 1 Frost",
                                "description": "Ein unheiliger Stoß, der 70% Waffenschaden als körperlichen Schaden plus 238 verursacht. Für jede Eurer auf das Ziel wirkenden Krankheiten fügt Ihr zusätzlich 12% des verursachten körperlichen Schadens als Schattenschaden zu."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2036,
                        "name": "Totenschwurs Zorn",
                        "icon": "inv_weapon_halberd14",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an der Blutseuche erkrankt sind, 2% mehr Schaden. Erhöht zudem Eure Waffenkunde um 1."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an der Blutseuche erkrankt sind, 4% mehr Schaden. Erhöht zudem Eure Waffenkunde um 2."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an der Blutseuche erkrankt sind, 6% mehr Schaden. Erhöht zudem Eure Waffenkunde um 3."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an der Blutseuche erkrankt sind, 8% mehr Schaden. Erhöht zudem Eure Waffenkunde um 4."
                            },
                            {
                                "description": "Eure Fähigkeiten und Zauber verursachen an Zielen, die an der Blutseuche erkrankt sind, 10% mehr Schaden. Erhöht zudem Eure Waffenkunde um 5."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2000,
                        "name": "Gargoyle beschwören",
                        "icon": "ability_hunter_pet_bat",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Ein Gargoyle fliegt herbei und bombardiert das Ziel mit Naturschaden (modifiziert durch die Angriffskraft des Todesritters). Der Gargoyle bleibt 30 Sek. lang bestehen.",
                                "cost": "60 Runenmacht",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [
        {
            "511": {
                "name": "Glyphe 'Dunkler Befehl'",
                "id": "511",
                "type": 0,
                "description": "Increases the chance for your Dark Command ability to work successfully by 8%.",
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43538",
                "spellKey": "58613",
                "spellId": "58613",
                "prettyName": "",
                "typeOrder": 2
            },
            "512": {
                "name": "Glyphe 'Antimagische Hülle'",
                "id": "512",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43533",
                "spellKey": "58623",
                "spellId": "58623",
                "prettyName": "",
                "typeOrder": 2
            },
            "513": {
                "name": "Glyphe 'Herzstoß'",
                "id": "513",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43534",
                "spellKey": "58616",
                "spellId": "58616",
                "prettyName": "",
                "typeOrder": 2
            },
            "514": {
                "name": "Glyphe 'Blutwandlung'",
                "id": "514",
                "type": 1,
                "description": null,
                "icon": "inv_glyph_minordeathknight",
                "itemId": "43535",
                "spellKey": "58640",
                "spellId": "58640",
                "prettyName": "",
                "typeOrder": 2
            },
            "515": {
                "name": "Glyphe 'Knochenschild'",
                "id": "515",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43536",
                "spellKey": "58673",
                "spellId": "58673",
                "prettyName": "",
                "typeOrder": 2
            },
            "516": {
                "name": "Glyphe 'Eisketten'",
                "id": "516",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43537",
                "spellKey": "58620",
                "spellId": "58620",
                "prettyName": "",
                "typeOrder": 2
            },
            "518": {
                "name": "Glyphe 'Umarmung des Todes'",
                "id": "518",
                "type": 1,
                "description": null,
                "icon": "inv_glyph_minordeathknight",
                "itemId": "43539",
                "spellKey": "58677",
                "spellId": "58677",
                "prettyName": "",
                "typeOrder": 2
            },
            "519": {
                "name": "Glyphe 'Todesgriff'",
                "id": "519",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43541",
                "spellKey": "62259",
                "spellId": "62259",
                "prettyName": "",
                "typeOrder": 2
            },
            "520": {
                "name": "Glyphe 'Tod und Verfall'",
                "id": "520",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43542",
                "spellKey": "58629",
                "spellId": "58629",
                "prettyName": "",
                "typeOrder": 2
            },
            "521": {
                "name": "Glyphe 'Froststoß'",
                "id": "521",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43543",
                "spellKey": "58647",
                "spellId": "58647",
                "prettyName": "",
                "typeOrder": 2
            },
            "522": {
                "name": "Glyphe 'Horn des Winters'",
                "id": "522",
                "type": 1,
                "description": null,
                "icon": "inv_glyph_minordeathknight",
                "itemId": "43544",
                "spellKey": "58680",
                "spellId": "58680",
                "prettyName": "",
                "typeOrder": 2
            },
            "523": {
                "name": "Glyphe 'Eisige Gegenwehr'",
                "id": "523",
                "type": 0,
                "description": "Your Icebound Fortitude now always grants at least 30% damage reduction, regardless of your defense skill.",
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43545",
                "spellKey": "58625",
                "spellId": "58625",
                "prettyName": "",
                "typeOrder": 2
            },
            "524": {
                "name": "Glyphe 'Eisige Berührung'",
                "id": "524",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43546",
                "spellKey": "58631",
                "spellId": "58631",
                "prettyName": "",
                "typeOrder": 2
            },
            "525": {
                "name": "Glyphe 'Auslöschen'",
                "id": "525",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43547",
                "spellKey": "58671",
                "spellId": "58671",
                "prettyName": "",
                "typeOrder": 2
            },
            "526": {
                "name": "Glyphe 'Seuchenstoß'",
                "id": "526",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43548",
                "spellKey": "58657",
                "spellId": "58657",
                "prettyName": "",
                "typeOrder": 2
            },
            "527": {
                "name": "Glyph of Raise Dead",
                "id": "527",
                "type": 1,
                "description": "",
                "icon": "inv_inscription_minorglyph13",
                "itemId": "44432",
                "spellKey": null,
                "spellId": null,
                "prettyName": "",
                "typeOrder": 2
            },
            "528": {
                "name": "Glyphe 'Runenstoß'",
                "id": "528",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43550",
                "spellKey": "58669",
                "spellId": "58669",
                "prettyName": "",
                "typeOrder": 2
            },
            "529": {
                "name": "Glyphe 'Geißelstoß'",
                "id": "529",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43551",
                "spellKey": "58642",
                "spellId": "58642",
                "prettyName": "",
                "typeOrder": 2
            },
            "530": {
                "name": "Glyphe 'Strangulieren'",
                "id": "530",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43552",
                "spellKey": "58618",
                "spellId": "58618",
                "prettyName": "",
                "typeOrder": 2
            },
            "531": {
                "name": "Glyphe 'Undurchdringliche Rüstung'",
                "id": "531",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43553",
                "spellKey": "58635",
                "spellId": "58635",
                "prettyName": "",
                "typeOrder": 2
            },
            "532": {
                "name": "Glyphe 'Vampirblut'",
                "id": "532",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43554",
                "spellKey": "58676",
                "spellId": "58676",
                "prettyName": "",
                "typeOrder": 2
            },
            "553": {
                "name": "Glyphe 'Pestilenz'",
                "id": "553",
                "type": 1,
                "description": null,
                "icon": "inv_glyph_minordeathknight",
                "itemId": "43672",
                "spellKey": "59309",
                "spellId": "59309",
                "prettyName": "",
                "typeOrder": 2
            },
            "554": {
                "name": "Glyphe 'Leichenexplosion'",
                "id": "554",
                "type": 1,
                "description": null,
                "icon": "inv_glyph_minordeathknight",
                "itemId": "43671",
                "spellKey": "59307",
                "spellId": "59307",
                "prettyName": "",
                "typeOrder": 2
            },
            "555": {
                "name": "Glyphe 'Totenerweckung'",
                "id": "555",
                "type": 1,
                "description": "Verringert die Zauberzeit Eures Zaubers \\'Schwarzes Tor\\' um 60%.",
                "icon": "inv_glyph_minordeathknight",
                "itemId": "43673",
                "spellKey": "60200",
                "spellId": "60200",
                "prettyName": "",
                "typeOrder": 2
            },
            "556": {
                "name": "Glyphe 'Runenheilung'",
                "id": "556",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43825",
                "spellKey": "59327",
                "spellId": "59327",
                "prettyName": "",
                "typeOrder": 2
            },
            "557": {
                "name": "Glyphe 'Blutstoß'",
                "id": "557",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43826",
                "spellKey": "59332",
                "spellId": "59332",
                "prettyName": "",
                "typeOrder": 2
            },
            "558": {
                "name": "Glyphe 'Todesstoß'",
                "id": "558",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "43827",
                "spellKey": "59336",
                "spellId": "59336",
                "prettyName": "",
                "typeOrder": 2
            },
            "768": {
                "name": "Glyphe 'Tanzende Runenwaffe'",
                "id": "768",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "45799",
                "spellKey": "63330",
                "spellId": "63330",
                "prettyName": "",
                "typeOrder": 2
            },
            "769": {
                "name": "Glyphe 'Zehrende Kälte'",
                "id": "769",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "45800",
                "spellKey": "63331",
                "spellId": "63331",
                "prettyName": "",
                "typeOrder": 2
            },
            "770": {
                "name": "Glyphe 'Unheilige Verseuchung'",
                "id": "770",
                "type": 0,
                "description": "Increases the duration of Unholy Blight by 10 sec.",
                "icon": "inv_glyph_majordeathknight",
                "itemId": "45803",
                "spellKey": "63332",
                "spellId": "63332",
                "prettyName": "",
                "typeOrder": 2
            },
            "771": {
                "name": "Glyphe 'Dunkler Tod'",
                "id": "771",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "45804",
                "spellKey": "63333",
                "spellId": "63333",
                "prettyName": "",
                "typeOrder": 2
            },
            "772": {
                "name": "Glyphe 'Krankheit'",
                "id": "772",
                "type": 0,
                "description": "Your Pestilence ability now refreshes disease durations on your primary target back to their maximum duration.",
                "icon": "inv_glyph_majordeathknight",
                "itemId": "45805",
                "spellKey": "63334",
                "spellId": "63334",
                "prettyName": "",
                "typeOrder": 2
            },
            "773": {
                "name": "Glyphe 'Heulende Böe'",
                "id": "773",
                "type": 0,
                "description": null,
                "icon": "inv_glyph_majordeathknight",
                "itemId": "45806",
                "spellKey": "63335",
                "spellId": "63335",
                "prettyName": "",
                "typeOrder": 2
            },
            "": {
                "name": "Deprecated Test Glyph",
                "id": null,
                "type": 1,
                "description": "",
                "icon": "axe_1h_draenei_b_01",
                "itemId": "37301",
                "spellKey": null,
                "spellId": null,
                "prettyName": "",
                "typeOrder": 2
            }
        }
    ]
}