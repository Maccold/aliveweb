{
    "talentData": {
        "characterClass": {
            "classId": 7,
            "name": "Schamane",
            "powerType": "MANA",
            "powerTypeId": 0,
            "powerTypeSlug": "mana"
        },
        "talentTrees": [
            {
                "name": "Elementar",
                "icon": "spell_nature_lightning",
                "backgroundFile": "ShamanElementalCombat",
                "overlayColor": "#cc33cc",
                "description": "Ein Zauberwirker, der die zerstörerischen Kräfte der Natur und der Elemente kanalisiert.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 564,
                        "name": "Konvektion",
                        "icon": "spell_nature_wispsplode",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Lavaeruption' und 'Windstoß' um 2%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Lavaeruption' und 'Windstoß' um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Lavaeruption' und 'Windstoß' um 6%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Lavaeruption' und 'Windstoß' um 8%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Lavaeruption' und 'Windstoß' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 563,
                        "name": "Erschütterung",
                        "icon": "spell_fire_fireball",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 1%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schockzauber sowie Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Gewitter' und 'Lavaeruption' um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 561,
                        "name": "Ruf der Flamme",
                        "icon": "spell_fire_immolation",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Feuertotems und Eures Zaubers 'Feuernova' um 5% sowie den von Eurem Zauber 'Lavaeruption' verursachten Schaden um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Feuertotems und Eures Zaubers 'Feuernova' um 10% sowie den von Eurem Zauber 'Lavaeruption' verursachten Schaden um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Feuertotems und Eures Zaubers 'Feuernova' um 15% sowie den von Eurem Zauber 'Lavaeruption' verursachten Schaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1640,
                        "name": "Schutz der Elemente",
                        "icon": "spell_nature_spiritarmor",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 2%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 4%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1645,
                        "name": "Elementarverwüstung",
                        "icon": "spell_fire_elementaldevastation",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Kritische Treffer Eurer unregelmäßigen Offensivzauber erhöhen 10 Sek. lang die Chance auf einen kritischen Treffer mit Nahkampfangriffen um 3%."
                            },
                            {
                                "description": "Kritische Treffer Eurer unregelmäßigen Offensivzauber erhöhen 10 Sek. lang die Chance auf einen kritischen Treffer mit Nahkampfangriffen um 6%."
                            },
                            {
                                "description": "Kritische Treffer Eurer unregelmäßigen Offensivzauber erhöhen 10 Sek. lang die Chance auf einen kritischen Treffer mit Nahkampfangriffen um 9%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 575,
                        "name": "Nachklingen",
                        "icon": "spell_frost_frostward",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Schockzauber sowie von 'Windstoß' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Schockzauber sowie von 'Windstoß' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Schockzauber sowie von 'Windstoß' um 0.6 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Schockzauber sowie von 'Windstoß' um 0.8 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Schockzauber sowie von 'Windstoß' um 1 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 574,
                        "name": "Elementarfokus",
                        "icon": "spell_shadow_manaburn",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Versetzt Euch in einen Freizauberzustand, nachdem Ihr mit einem Feuer-, Frost- oder Naturschadenszauber einen unregelmäßigen kritischen Treffer erzielt habt. Der Freizauberzustand verringert die Manakosten Eurer nächsten 2 Schadens- oder Heilzauber um 40%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 565,
                        "name": "Elementarfuror",
                        "icon": "spell_fire_volcano",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Feuer-, Frost- und Naturzauber sowie des Totems der Verbrennung und des Totems des glühenden Magmas um 20%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Feuer-, Frost- und Naturzauber sowie des Totems der Verbrennung und des Totems des glühenden Magmas um 40%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Feuer-, Frost- und Naturzauber sowie des Totems der Verbrennung und des Totems des glühenden Magmas um 60%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Feuer-, Frost- und Naturzauber sowie des Totems der Verbrennung und des Totems des glühenden Magmas um 80%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Feuer-, Frost- und Naturzauber sowie des Totems der Verbrennung und des Totems des glühenden Magmas um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 567,
                        "name": "Verbesserte Feuernova",
                        "icon": "spell_fire_sealoffire",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Feuernova' um 10% und verringert seine Abklingzeit um 2 Sek."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Feuernova' um 20% und verringert seine Abklingzeit um 4 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1642,
                        "name": "Auge des Sturms",
                        "icon": "spell_shadow_soulleech_2",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken der Zauber 'Blitzschlag', 'Kettenblitzschlag', Lavaeruption' und 'Verhexen' um 23%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken der Zauber 'Blitzschlag', 'Kettenblitzschlag', Lavaeruption' und 'Verhexen' um 46%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken der Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Lavaeruption' und 'Verhexen' um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1641,
                        "name": "Elementare Reichweite",
                        "icon": "spell_nature_stormreach",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Feuernova' und 'Lavaeruption' um 3 Meter, erhöht den Radius Eures Zaubers 'Gewitter' um 10% und erhöht die Reichweite des Zaubers 'Flammenschock' um 7 Meter."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag', 'Feuernova' und 'Lavaeruption' um 6 Meter, erhöht den Radius Eures Zaubers 'Gewitter' um 20% und erhöht die Reichweite des Zaubers 'Flammenschock' um 15 Meter."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 562,
                        "name": "Ruf des Donners",
                        "icon": "spell_nature_callstorm",
                        "x": 1,
                        "y": 4,
                        "req": 574,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Gewitter' um zusätzliche 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1682,
                        "name": "Unerbittlicher Sturm",
                        "icon": "spell_nature_unrelentingstorm",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Regeneriert alle 5 Sek. Mana entsprechend 4% Eurer Intelligenz, selbst während des Zauberwirkens."
                            },
                            {
                                "description": "Regeneriert alle 5 Sek. Mana entsprechend 8% Eurer Intelligenz, selbst während des Zauberwirkens."
                            },
                            {
                                "description": "Regeneriert alle 5 Sek. Mana entsprechend 12% Eurer Intelligenz, selbst während des Zauberwirkens."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1685,
                        "name": "Elementare Präzision",
                        "icon": "spell_nature_elementalprecision_1",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Trefferchance mit Feuer-, Frost- und Naturzaubern um 1%. Außerdem wird die von Euren Feuer-, Frost- und Naturzaubern verursachte Bedrohung um 10% verringert."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance mit Feuer-, Frost- und Naturzaubern um 2%. Außerdem wird die von Euren Feuer-, Frost- und Naturzaubern verursachte Bedrohung um 20% verringert."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance mit Feuer-, Frost- und Naturzaubern um 3%. Außerdem wird die von Euren Feuer-, Frost- und Naturzaubern verursachte Bedrohung um 30% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 721,
                        "name": "Blitzschlagbeherrschung",
                        "icon": "spell_lightning_lightningbolt01",
                        "x": 2,
                        "y": 5,
                        "req": 565,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavaeruption' um 0.1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavaeruption' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavaeruption' um 0.3 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavaeruption' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavaeruption' um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 573,
                        "name": "Elementarbeherrschung",
                        "icon": "spell_nature_wispheal",
                        "x": 1,
                        "y": 6,
                        "req": 562,
                        "ranks": [
                            {
                                "description": "Bei Aktivierung wird Euer nächstes Wirken von 'Blitzschlag', 'Kettenblitzschlag' oder 'Lavaeruption' zu einem Spontanzauber. Zusätzlich wird Euer Zaubertempo 15 Sek. lang um 15% erhöht. 'Elementarbeherrschung' und 'Schnelligkeit der Natur' unterliegen derselben Abklingzeit."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2052,
                        "name": "Sturm, Erde und Feuer",
                        "icon": "spell_shaman_stormearthfire",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Kettenblitzschlag' um 0,75 Sek. Zudem hat Euer Totem der Erdbindung hat eine Chance von 33%, Ziele 5 Sek. lang festzuwurzeln, wenn es aufgestellt wird, und der regelmäßige Schaden Eures Zaubers 'Flammenschock' wird um 20% erhöht."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Kettenblitzschlag' um 1,5 Sek. Zudem hat Euer Totem der Erdbindung hat eine Chance von 66%, Ziele 5 Sek. lang festzuwurzeln, wenn es aufgestellt wird, und der regelmäßige Schaden Eures Zaubers 'Flammenschock' wird um 40% erhöht."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Kettenblitzschlag' um 2,5 Sek. Zudem hat Euer Totem der Erdbindung hat eine Chance von 100%, Ziele 5 Sek. lang festzuwurzeln, wenn es aufgestellt wird, und der regelmäßige Schaden Eures Zaubers 'Flammenschock' wird um 60% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2262,
                        "name": "Donnernde Echos",
                        "icon": "spell_fire_blueflamering",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Flammenschock' und 'Frostschock' um zusätzliche 1 Sek. und erhöht ihren direkten Schaden um zusätzliche 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Flammenschock' und 'Frostschock' um zusätzliche 2 Sek. und erhöht ihren direkten Schaden um zusätzliche 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2049,
                        "name": "Elementarer Schwur",
                        "icon": "spell_shaman_elementaloath",
                        "x": 1,
                        "y": 7,
                        "req": 573,
                        "ranks": [
                            {
                                "description": "Ist der Freizaubereffekt Eures Talents 'Elementarfokus' aktiv, wird Euer Zauberschaden um 5% erhöht. Zusätzlich wird die kritische Zaubertrefferchance aller Gruppen- und Schlachtzugsmitglieder innerhalb von 100 Metern um 3% erhöht."
                            },
                            {
                                "description": "Ist der Freizaubereffekt Eures Talents 'Elementarfokus' aktiv, wird Euer Zauberschaden um 10% erhöht. Zusätzlich wird die kritische Zaubertrefferchance aller Gruppen- und Schlachtzugsmitglieder innerhalb von 100 Metern um 5% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1686,
                        "name": "Blitzüberladung",
                        "icon": "spell_nature_lightningoverload",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verleiht Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' eine Chance von 11%, einen zweiten, identischen Zauber auf das gleiche Ziel auszulösen, ohne zusätzliche Kosten. Dieser verursacht nur halben Schaden und keine Bedrohung."
                            },
                            {
                                "description": "Verleiht Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' eine Chance von 22%, einen zweiten, identischen Zauber auf das gleiche Ziel auszulösen, ohne zusätzliche Kosten. Dieser verursacht nur halben Schaden und keine Bedrohung."
                            },
                            {
                                "description": "Verleiht Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' eine Chance von 33%, einen zweiten, identischen Zauber auf das gleiche Ziel auszulösen, ohne zusätzliche Kosten. Dieser verursacht nur halben Schaden und keine Bedrohung."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2050,
                        "name": "Astralwandler",
                        "icon": "spell_shaman_astralshift",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Wenn Betäubungs-, Furcht- oder Stilleeffekte auf Euch wirken, werdet Ihr zum Teil in die astrale Ebene verschoben und verringert somit den erlittenen Schaden für die Dauer des Betäubungs-, Furcht- oder Stilleeffektes um 10%."
                            },
                            {
                                "description": "Wenn Betäubungs-, Furcht- oder Stilleeffekte auf Euch wirken, werdet Ihr zum Teil in die astrale Ebene verschoben und verringert somit den erlittenen Schaden für die Dauer des Betäubungs-, Furcht- oder Stilleeffektes um 20%."
                            },
                            {
                                "description": "Wenn Betäubungs-, Furcht- oder Stilleeffekte auf Euch wirken, werdet Ihr zum Teil in die astrale Ebene verschoben und verringert somit den erlittenen Schaden für die Dauer des Betäubungs-, Furcht- oder Stilleeffektes um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1687,
                        "name": "Totem des Ingrimms",
                        "icon": "spell_fire_totemofwrath",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cost": "5% des Grundmanas",
                                "description": "Beschwört ein Totem des Ingrimms mit 5 Gesundheit zu Füßen des Zaubernden. Das Totem erhöht die Zaubermacht aller Gruppen- und Schlachtzugsmitglieder um 100 und erhöht die kritische Trefferchance aller Angriffe gegen alle Feinde im Umkreis von 40 Metern um 3%. Hält 5 Min. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2051,
                        "name": "Lavastrom",
                        "icon": "spell_shaman_lavaflow",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus von 'Lavaeruption' um zusätzlich 6%, und wenn Euer Zauber 'Flammenschock' gebannt wird, wird Euer Zaubertempo 6 Sek. lang um 10% erhöht."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus von 'Lavaeruption' um zusätzlich 12%, und wenn Euer Zauber 'Flammenschock' gebannt wird, wird Euer Zaubertempo 6 Sek. lang um 20% erhöht."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus von 'Lavaeruption' um zusätzlich 24%, und wenn Euer Zauber 'Flammenschock' gebannt wird, wird Euer Zaubertempo 6 Sek. lang um 30% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2252,
                        "name": "Schamanismus",
                        "icon": "spell_unused2",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' werden zusätzliche 4%, Eurem Zauber 'Lavaeruption' zusätzliche 5% Eurer Bonusschadenseffekte gewährt."
                            },
                            {
                                "description": "Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' werden zusätzliche 8%, Eurem Zauber 'Lavaeruption' zusätzliche 10% Eurer Bonusschadenseffekte gewährt."
                            },
                            {
                                "description": "Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' werden zusätzliche 12%, Eurem Zauber 'Lavaeruption' zusätzliche 15% Eurer Bonusschadenseffekte gewährt."
                            },
                            {
                                "description": "Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' werden zusätzliche 16%, Eurem Zauber 'Lavaeruption' zusätzliche 20% Eurer Bonusschadenseffekte gewährt."
                            },
                            {
                                "description": "Euren Zaubern 'Blitzschlag' und 'Kettenblitzschlag' werden zusätzliche 20%, Eurem Zauber 'Lavaeruption' zusätzliche 25% Eurer Bonusschadenseffekte gewährt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2053,
                        "name": "Gewitter",
                        "icon": "spell_shaman_thunderstorm",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                            	"cooldown": "45 Sek. Abklingzeit",
                                "description": "Ihr ruft einen Blitzschlag hernieder, der Euch auflädt und bei allen Feinden im Umkreis von 10 Metern Schaden verursacht. Stellt 8% Eures Manas wieder her, alle Feinde erleiden 551 bis 629 Naturschaden und werden 20 Meter zurückgestoßen. Dieser Zauber kann genutzt werden, während ein Betäubungseffekt auf Euch wirkt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Verstärkung",
                "icon": "spell_nature_lightningshield",
                "backgroundFile": "ShamanEnhancement",
                "overlayColor": "#4c7fff",
                "description": "Ein totemischer Krieger, der seine Feinde mit Waffen bekämpft, die mit Elementarmagie erfüllt wurden.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 610,
                        "name": "Stärkungstotems",
                        "icon": "spell_nature_earthbindtotem",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt Eures Totems der Erdstärke und Totem der Flammenzunge um 5%."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Totems der Erdstärke und Totems der Flammenzunge um 10%."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Totems der Erdstärke und Totems der Flammenzunge um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2101,
                        "name": "Griff der Erde",
                        "icon": "spell_nature_stoneclawtotem",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Gesundheit Eures Totems der Steinklaue um 25% und den Radius Eures Totems der Erdbindung um 10%. Zudem wird die Abklingzeit beider Totems um 15% verringert."
                            },
                            {
                                "description": "Erhöht die Gesundheit Eures Totems der Steinklaue um 50% und den Radius Eures Totems der Erdbindung um 20%. Zudem wird die Abklingzeit beider Totems um 30% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 614,
                        "name": "Wissen der Ahnen",
                        "icon": "spell_shadow_grimward",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Intelligenz um 2%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 4%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 6%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 8%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 609,
                        "name": "Wächtertotems",
                        "icon": "spell_nature_stoneskintotem",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurem Totem der Steinhaut gewährten Rüstungswert um 10%. Verringert die Abklingzeit Eures Totems der Erdung um 1 Sek."
                            },
                            {
                                "description": "Erhöht den von Eurem Totem der Steinhaut gewährten Rüstungswert um 20%. Verringert die Abklingzeit Eures Totems der Erdung um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 613,
                        "name": "Donnernde Stöße",
                        "icon": "ability_thunderbolt",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Angriffen und Zaubern um 1%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Angriffen und Zaubern um 2%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Angriffen und Zaubern um 3%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Angriffen und Zaubern um 4%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Angriffen und Zaubern um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 605,
                        "name": "Verbesserter Geisterwolf",
                        "icon": "spell_nature_spiritwolf",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Geisterwolf' um 1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Geisterwolf' um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 607,
                        "name": "Verbesserte Schilde",
                        "icon": "spell_nature_lightningshield",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden der Aufladungen Eures Blitzschlagschilds um 5%, erhöht das durch die Aufladungen Eures Wasserschilds gewonnene Mana um 5% und erhöht den von den Aufladungen Eures Erdschilds geheilten Wert um 5%."
                            },
                            {
                                "description": "Erhöht den Schaden der Aufladungen Eures Blitzschlagschilds um 10%, erhöht das durch die Aufladungen Eures Wasserschilds gewonnene Mana um 10% und erhöht den von den Aufladungen Eures Erdschilds geheilten Wert um 10%."
                            },
                            {
                                "description": "Erhöht den Schaden der Aufladungen Eures Blitzschlagschilds um 15%, erhöht das durch die Aufladungen Eures Wasserschilds gewonnene Mana um 15% und erhöht den von den Aufladungen Eures Erdschilds geheilten Wert um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 611,
                        "name": "Elementarwaffen",
                        "icon": "spell_fire_flametounge",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den vom Effekt von 'Waffe des Windzorns' verursachten Schaden um 13%, erhöht den Zauberschaden von 'Waffe der Flammenzunge' um 10% und den Heilbonus von 'Waffe der Lebensgeister' um 10%."
                            },
                            {
                                "description": "Erhöht den vom Effekt von 'Waffe des Windzorns' verursachten Schaden um 27%, erhöht den Zauberschaden von 'Waffe der Flammenzunge' um 20% und den Heilbonus von 'Waffe der Lebensgeister' um 20%."
                            },
                            {
                                "description": "Erhöht den vom Effekt von 'Waffe des Windzorns' verursachten Schaden um 40%, erhöht den Zauberschaden von 'Waffe der Flammenzunge' um 30% und den Heilbonus von 'Waffe der Lebensgeister' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 617,
                        "name": "Schamanistischer Fokus",
                        "icon": "spell_nature_elementalabsorption",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Schockzauber um 45%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 601,
                        "name": "Vorahnung",
                        "icon": "spell_nature_mirrorimage",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance um 1% und verringert die Dauer aller auf Euch wirkenden Entwaffnungseffekte um 16%. Dieser Effekt ist mit anderen Effekten, welche die Entwaffnungsdauer verringern, nicht stapelbar."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 2% und verringert die Dauer aller auf Euch wirkenden Entwaffnungseffekte um 25%. Dieser Effekt ist mit anderen Effekten, welche die Entwaffnungsdauer verringern, nicht stapelbar."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 3% und verringert die Dauer aller auf Euch wirkenden Entwaffnungseffekte um 50%. Dieser Effekt ist mit anderen Effekten, welche die Entwaffnungsdauer verringern, nicht stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 602,
                        "name": "Schlaghagel",
                        "icon": "ability_ghoulfrenzy",
                        "x": 1,
                        "y": 3,
                        "req": 613,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 6%, nachdem Ihr einen kritischen Treffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 12%, nachdem Ihr einen kritischen Treffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 18%, nachdem Ihr einen kritischen Treffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 24%, nachdem Ihr einen kritischen Treffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 30%, nachdem Ihr einen kritischen Treffer zugefügt habt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 615,
                        "name": "Zähigkeit",
                        "icon": "spell_holy_devotion",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausdauer um 2% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 6%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 4% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 12%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 6% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 18%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 8% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 24%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 10% und verringert die Dauer von auf Euch wirkenden bewegungsverlangsamenden Effekten um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1647,
                        "name": "Verbessertes Totem des Windzorns",
                        "icon": "spell_nature_windfury",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die von Eurem Totem des Windzorns gewährte Erhöhung des Nahkampfangriffstempos um 2%."
                            },
                            {
                                "description": "Erhöht die von Eurem Totem des Windzorns gewährte Erhöhung des Nahkampfangriffstempos um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 616,
                        "name": "Waffen der Geister",
                        "icon": "ability_parry",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance, feindliche Nahkampfangriffe zu parieren, und verringert jegliche verursachte Bedrohung um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2083,
                        "name": "Geistige Gewandtheit",
                        "icon": "spell_nature_bloodlust",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Angriffskraft um 33% Eurer Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Angriffskraft um 66% Eurer Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Angriffskraft um 100% Eurer Intelligenz."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1689,
                        "name": "Entfesselte Wut",
                        "icon": "spell_nature_unleashedrage",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Waffenkunde um 3. Erhöht die Angriffskraft aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um den Schamanen um 4%."
                            },
                            {
                                "description": "Erhöht Eure Waffenkunde um 6. Erhöht die Angriffskraft aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um den Schamanen um 7%."
                            },
                            {
                                "description": "Erhöht Eure Waffenkunde um 9. Erhöht die Angriffskraft aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um den Schamanen um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1643,
                        "name": "Waffenbeherrschung",
                        "icon": "ability_hunter_swiftstrike",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden, den Ihr mit allen Waffen zufügt, um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit allen Waffen zufügt, um 7%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit allen Waffen zufügt, um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2263,
                        "name": "Gefrorene Kraft",
                        "icon": "spell_fire_bluecano",
                        "x": 3,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavapeitsche' sowie aller Schockzauber um 5%, wenn das Ziel unter dem Effekt Eures Frostbrandangriffs steht. Zusätzlich hat Euer Zauber 'Frostschock' eine Chance von 50%, Ziele 5 Sek.. lang an Ort und Stelle festzufrieren, wenn sie sich 15 Meter oder weiter entfernt von Euch befinden."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Blitzschlag', 'Kettenblitzschlag' und 'Lavapeitsche' sowie aller Schockzauber um 10%, wenn das Ziel unter dem Effekt Eures Frostbrandangriffs steht. Zusätzlich hat Euer Zauber 'Frostschock' eine Chance von 100%, Ziele 5 Sek.. lang an Ort und Stelle festzufrieren, wenn sie sich 15 Meter oder weiter entfernt von Euch befinden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1692,
                        "name": "Beidhändigkeitsspezialisierung",
                        "icon": "ability_dualwieldspecialization",
                        "x": 0,
                        "y": 6,
                        "req": 1690,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Trefferchance beim beidhändigen Führen von Waffen um zusätzliche 2%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance beim beidhändigen Führen von Waffen um zusätzliche 4%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance beim beidhändigen Führen von Waffen um zusätzliche 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1690,
                        "name": "Beidhändigkeit",
                        "icon": "ability_dualwield",
                        "x": 1,
                        "y": 6,
                        "req": 616,
                        "ranks": [
                            {
                                "description": "Ermöglicht die Verwendung von einhändigen Waffen und Schildhandwaffen in der Schildhand."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 901,
                        "name": "Sturmschlag",
                        "icon": "ability_shaman_stormstrike",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Greift das Ziel sofort mit beiden Waffen an. Zusätzlich werden die nächsten 4 Quellen des Naturschadens, den das Ziel durch den Schamanen erleidet, um 20% erhöht. Hält 12 Sek. lang an.",
                                "cost": "8% mana",
                                "castTime": "Spontanzauber",
                                "cooldown": "8 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2055,
                        "name": "Statischer Schock",
                        "icon": "spell_shaman_staticshock",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Ihr habt eine Chance von 2%, Euer Ziel mit einer Aufladung Eures Blitzschlagschildes zu treffen, wenn Ihr mit Nahkampfangriffen oder -fähigkeiten Schaden verursacht. Euer Blitzschlagschild gewinnt zusätzlich 2 Aufladungen."
                            },
                            {
                                "description": "Ihr habt eine Chance von 4%, Euer Ziel mit einer Aufladung Eures Blitzschlagschildes zu treffen, wenn Ihr mit Nahkampfangriffen oder -fähigkeiten Schaden verursacht. Euer Blitzschlagschild gewinnt zusätzlich 4 Aufladungen."
                            },
                            {
                                "description": "Ihr habt eine Chance von 6%, Euer Ziel mit einer Aufladung Eures Blitzschlagschildes zu treffen, wenn Ihr mit Nahkampfangriffen oder -fähigkeiten Schaden verursacht. Euer Blitzschlagschild gewinnt zusätzlich 6 Aufladungen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2249,
                        "name": "Lavapeitsche",
                        "icon": "ability_shaman_lavalash",
                        "x": 1,
                        "y": 7,
                        "req": 1690,
                        "ranks": [
                            {
                                "description": "Lädt Eure Schildhandwaffe mit Lava auf, verursacht sofort 100% Waffenschaden der Schildhandwaffe. Der Schaden ist um 25% erhöht, wenn Eure Schildhandwaffe mit 'Flammenzunge' verzaubert ist.",
                                "cost": "4% mana",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2054,
                        "name": "Verbesserter Sturmschlag",
                        "icon": "spell_shaman_improvedstormstrike",
                        "x": 2,
                        "y": 7,
                        "req": 901,
                        "ranks": [
                            {
                                "description": "Wenn Ihr die Fähigkeit 'Sturmschlag' nutzt, besteht eine Chance von 50%, dass Ihr sofort 20% Eures Grundmanas gewinnt."
                            },
                            {
                                "description": "Wenn Ihr die Fähigkeit 'Sturmschlag' nutzt, besteht eine Chance von 100%, dass Ihr sofort 20% Eures Grundmanas gewinnt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1691,
                        "name": "Geistige Schnelligkeit",
                        "icon": "spell_nature_mentalquickness",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer spontanen Schamanenzauber um 2% und erhöht Eure Zaubermacht um einen Betrag, der 10% Eurer Angriffskraft entspricht."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer spontanen Schamanenzauber um 4% und erhöht Eure Zaubermacht um einen Betrag, der 20% Eurer Angriffskraft entspricht."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer spontanen Schamanenzauber um 6% und erhöht Eure Zaubermacht um einen Betrag, der 30% Eurer Angriffskraft entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1693,
                        "name": "Schamanistische Wut",
                        "icon": "spell_nature_shamanrage",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "description": "Reduziert sämtlichen erlittenen Schaden um 30% und gewährt erfolgreichen Nahkampfangriffen die Chance, Mana entsprechend 15% Eurer Angriffskraft zu regenerieren. Dieser Zauber kann genutzt werden, während Betäubungseffekte auf Euch wirken. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2056,
                        "name": "Irdische Kraft",
                        "icon": "spell_nature_earthelemental_totem",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Jeder Puls Eures Totems der Erdbindung hat eine Chance von 50%, jegliche auf Euch oder nahe befreundete Ziele wirkende Verlangsamungseffekte zu entfernen und Euer Zauber 'Erdschock' verringert das Angriffstempo von Gegnern um zusätzliche 5%."
                            },
                            {
                                "description": "Jeder Puls Eures Totems der Erdbindung hat eine Chance von 100%, jegliche auf Euch oder nahe befreundete Ziele wirkende Verlangsamungseffekte zu entfernen und Euer Zauber 'Erdschock' verringert das Angriffstempo von Gegnern um zusätzliche 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2057,
                        "name": "Waffe des Mahlstroms",
                        "icon": "spell_shaman_maelstromweapon",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance, dass die Zauberzeit Eures nächsten Wirkens von 'Blitzschlag', 'Kettenblitzschlag', 'Geringe Welle der Heilung', 'Kettenheilung', 'Welle der Heilung' oder 'Verhexen' um 20% verringert wird. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance (höher als bei Rang 1), dass die Zauberzeit Eures nächsten Wirkens von 'Blitzschlag', 'Kettenblitzschlag', 'Geringe Welle der Heilung', 'Kettenheilung', 'Welle der Heilung' oder 'Verhexen' um 20% verringert wird. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance (höher als bei Rang 2), dass die Zauberzeit Eures nächsten Wirkens von 'Blitzschlag', 'Kettenblitzschlag', 'Geringe Welle der Heilung', 'Kettenheilung', 'Welle der Heilung' oder 'Verhexen' um 20% verringert wird. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance (höher als bei Rang 3), dass die Zauberzeit Eures nächsten Wirkens von 'Blitzschlag', 'Kettenblitzschlag', 'Geringe Welle der Heilung', 'Kettenheilung', 'Welle der Heilung' oder 'Verhexen' um 20% verringert wird. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."

                            },
                            {
                                "description": "Wenn Ihr mit einer Nahkampfwaffe Schaden verursacht, besteht eine Chance (höher als bei Rang 4), dass die Zauberzeit Eures nächsten Wirkens von 'Blitzschlag', 'Kettenblitzschlag', 'Geringe Welle der Heilung', 'Kettenheilung', 'Welle der Heilung' oder 'Verhexen' um 20% verringert wird. Bis zu 5-mal stapelbar. Hält 30 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2058,
                        "name": "Wildgeist",
                        "icon": "spell_shaman_feralspirit",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Beschwört zwei Schattenwölfe, die unter dem Befehl des Schamanen stehen. Hält 45 Sek. lang an.",
                                "cost": "12% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Wiederherstellung",
                "icon": "spell_nature_magicimmunity",
                "backgroundFile": "ShamanRestoration",
                "overlayColor": "#33cc66",
                "description": "Ein Heiler, der die Ahnengeister und die läuternde Kraft des Wassers herbeiruft, um die Wunden seiner Verbündeten zu heilen.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": true,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 586,
                        "name": "Verbesserte Welle der Heilung",
                        "icon": "spell_nature_magicimmunity",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Welle der Heilung' um 0.1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Welle der Heilung' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Welle der Heilung' um 0.3 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Welle der Heilung' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Welle der Heilung' um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 595,
                        "name": "Totemfokus",
                        "icon": "spell_nature_moonglow",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Totems um 5%."

                            },
                            {
                                "description": "Verringert die Manakosten Eurer Totems um 10%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Totems um 15%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Totems um 20%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Totems um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 589,
                        "name": "Verbesserte Reinkarnation",
                        "icon": "spell_nature_reincarnation",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Reinkarnation' um 7 Minuten und erhöht die Gesundheit und das Mana, über das Ihr direkt nach der Reinkarnation verfügt, um 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Reinkarnation' um 15 Minuten und erhöht die Gesundheit und das Mana, über das Ihr direkt nach der Reinkarnation verfügt, um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1646,
                        "name": "Geschick der Heilung",
                        "icon": "spell_nature_healingtouch",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die durch Eure Heilzauber verursachte Bedrohung um 5% und die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 10%."
                            },
                            {
                                "description": "Verringert die durch Eure Heilzauber verursachte Bedrohung um 10% und die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 20%."
                            },
                            {
                                "description": "Verringert die durch Eure Heilzauber verursachte Bedrohung um 15% und die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 593,
                        "name": "Gezeitenfokus",
                        "icon": "spell_frost_manarecharge",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Heilzauber um 1%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Heilzauber um 2%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Heilzauber um 3%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Heilzauber um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Heilzauber um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 583,
                        "name": "Verbesserter Wasserschild",
                        "icon": "ability_shaman_watershield",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Wenn Ihr mit 'Welle der Heilung' oder 'Springflut' einen kritischen Effekt erzielt, besteht eine Chance von 33%, dass Ihr sofort Mana gewinnt, als wäre eine Kugel des Wasserschilds verbraucht worden. Wenn Ihr mit 'Geringe Welle der Heilung' einen kritischen Effekt erzielt, liegt die Chance, diesen Effekt auszulösen, bei 20% und bei 10%, wenn Ihr mit 'Kettenheilung' einen kritischen Effekt erzielt."
                            },
                            {
                                "description": "Wenn Ihr mit 'Welle der Heilung' oder 'Springflut' einen kritischen Effekt erzielt, besteht eine Chance von 66%, dass Ihr sofort Mana gewinnt, als wäre eine Kugel des Wasserschilds verbraucht worden. Wenn Ihr mit 'Geringe Welle der Heilung' einen kritischen Effekt erzielt, liegt die Chance, diesen Effekt auszulösen, bei 40% und bei 20%, wenn Ihr mit 'Kettenheilung' einen kritischen Effekt erzielt."
                            },
                            {
                                "description": "Wenn Ihr mit 'Welle der Heilung' oder 'Springflut' einen kritischen Effekt erzielt, besteht eine Chance von 100%, dass Ihr sofort Mana gewinnt, als wäre eine Kugel des Wasserschilds verbraucht worden. Wenn Ihr mit 'Geringe Welle der Heilung' einen kritischen Effekt erzielt, liegt die Chance, diesen Effekt auszulösen, bei 60% und bei 30%, wenn Ihr mit 'Kettenheilung' einen kritischen Effekt erzielt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 587,
                        "name": "Heilfokus",
                        "icon": "spell_nature_healingwavelesser",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken von Schamanenheilzaubern um 23%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken von Schamanenheilzaubern um 46%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken von Schamanenheilzaubern um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 582,
                        "name": "Kraft der Gezeiten",
                        "icon": "spell_frost_frostbolt",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Welle der Heilung', 'Geringe Welle der Heilung' und 'Kettenheilung' um 60%. Jede kritische Heilung verringert die Chance um 20%. Hält 20 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 581,
                        "name": "Heilung der Ahnen",
                        "icon": "spell_nature_undyingstrength",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden eines Ziels 15 Sek. lang um 3%, nachdem es einen kritischen Effekt von einem Eurer Heilzauber erhalten hat."
                            },
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden eines Ziels 15 Sek. lang um 7%, nachdem es einen kritischen Effekt von einem Eurer Heilzauber erhalten hat."
                            },
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden eines Ziels 15 Sek. lang um 10%, nachdem es einen kritischen Effekt von einem Eurer Heilzauber erhalten hat."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 588,
                        "name": "Regenerationstotems",
                        "icon": "spell_nature_manaregentotem",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt Eures Totems der Manaquelle um 7% und den durch Euer Totem des heilenden Flusses geheilten Wert um 15%."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Totems der Manaquelle um 12% und den durch Euer Totem des heilenden Flusses geheilten Wert um 30%."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Totems der Manaquelle um 20% und den durch Euer Totem des heilenden Flusses geheilten Wert um 45%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 594,
                        "name": "Gezeitenbeherrschung",
                        "icon": "spell_nature_tranquility",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance für einen kritischen Effekt Eurer Heil- und Blitzschlagzauber um 1%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Effekt Eurer Heil- und Blitzschlagzauber um 2%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Effekt Eurer Heil- und Blitzschlagzauber um 3%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Effekt Eurer Heil- und Blitzschlagzauber um 4%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Effekt Eurer Heil- und Blitzschlagzauber um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1648,
                        "name": "Pfad der Heilung",
                        "icon": "spell_nature_healingway",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurem Zauber 'Welle der Heilung' geheilten Wert um 8%."
                            },
                            {
                                "description": "Erhöht den von Eurem Zauber 'Welle der Heilung' geheilten Wert um 16%."
                            },
                            {
                                "description": "Erhöht den von Eurem Zauber 'Welle der Heilung' geheilten Wert um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 591,
                        "name": "Schnelligkeit der Natur",
                        "icon": "spell_nature_ravenform",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Bei Aktivierung wird Euer nächster Naturzauber mit einer Zauberzeit von weniger als 10 Sek. ein Spontanzauber. 'Schnelligkeit der Natur' und 'Elementarbeherrschung' unterliegen derselben Abklingzeit."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1695,
                        "name": "Fokussierte Gedanken",
                        "icon": "spell_nature_focusedmind",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Wirkungsdauer jeglicher Stille- und Unterbrechungseffekte gegen den Schamanen um 10%. Dieser Effekt ist nicht mit anderen Effekten dieser Art stapelbar."
                            },
                            {
                                "description": "Verringert die Wirkungsdauer jeglicher Stille- und Unterbrechungseffekte gegen den Schamanen um 20%. Dieser Effekt ist nicht mit anderen Effekten dieser Art stapelbar."
                            },
                            {
                                "description": "Verringert die Wirkungsdauer jeglicher Stille- und Unterbrechungseffekte gegen den Schamanen um 30%. Dieser Effekt ist nicht mit anderen Effekten dieser Art stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 592,
                        "name": "Läuterung",
                        "icon": "spell_frost_wizardmark",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Heilzauber um 2%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Heilzauber um 4%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Heilzauber um 6%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Heilzauber um 8%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Heilzauber um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1699,
                        "name": "Wächter der Natur",
                        "icon": "spell_nature_natureguardian",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 3% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
                            },
                            {
                                "description": "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 6% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
                            },
                            {
                                "description": "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 9% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
                            },
                            {
                                "description": "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 12% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
                            },
                            {
                                "description": "Wenn Ihr einen Angriff erleidet, der Eure Gesundheit unter 30% senkt, wird Eure maximale Gesundheit 10 Sek. lang um 15% erhöht und Eure Bedrohung gegenüber dem Angreifer verringert. 30 Sek. Abklingzeit."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 590,
                        "name": "Totem der Manaflut",
                        "icon": "spell_frost_summonwaterelemental",
                        "x": 1,
                        "y": 6,
                        "req": 588,
                        "ranks": [
                            {
                                "description": "Beschwört ein Totem der Manaflut mit 10% der Gesundheit des Zaubernden zu seinen Füßen, das für Gruppenmitglieder in einem Umkreis von 30 Metern alle 3 Sek. 6% des gesamten Manas wiederherstellt. Hält 12 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2084,
                        "name": "Geistläuterung",
                        "icon": "ability_shaman_cleansespirit",
                        "x": 2,
                        "y": 6,
                        "req": 592,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "40 Meter Reichweite",
                                "cost": "7% des Grundmanas",
                                "description": "Läutert den Geist eines befreundeten Ziels, entfernt einen Gift-, einen Krankheits- und einen Flucheffekt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2060,
                        "name": "Segen der Ewigen",
                        "icon": "spell_shaman_blessingofeternals",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber um 2% und erhöht die Chance, bei einem Ziel mit 35% Gesundheit oder weniger den Effekt von 'Lebensgeister', der Heilung über Zeit verursacht, auszulösen um 40%."
                            },
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber um 4% und erhöht die Chance, bei einem Ziel mit 35% Gesundheit oder weniger den Effekt von 'Lebensgeister', der Heilung über Zeit verursacht, auszulösen um 80%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1697,
                        "name": "Verbesserte Kettenheilung",
                        "icon": "spell_nature_healingwavegreater",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht den durch 'Kettenheilung' geheilten Wert um 10%."
                            },
                            {
                                "description": "Erhöht den durch 'Kettenheilung' geheilten Wert um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1696,
                        "name": "Segen der Natur",
                        "icon": "spell_nature_natureblessing",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Heilung um einen Betrag, der 5% Eurer Intelligenz entspricht."
                            },
                            {
                                "description": "Erhöht Eure Heilung um einen Betrag, der 10% Eurer Intelligenz entspricht."
                            },
                            {
                                "description": "Erhöht Eure Heilung um einen Betrag, der 15% Eurer Intelligenz entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2061,
                        "name": "Erwachen der Ahnen",
                        "icon": "spell_shaman_ancestralawakening",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erzielt Ihr mit 'Welle der Heilung', 'Geringe Welle der Heilung' oder 'Springflut' einen kritischen Effekt, wird ein Ahnengeist beschworen, der sofort das Gruppen- oder Schlachtzugsmitglied mit der geringsten Gesundheit innerhalb von 40 Metern um 10% des von Euch geheilten Wertes heilt."
                            },
                            {
                                "description": "Erzielt Ihr mit 'Welle der Heilung', 'Geringe Welle der Heilung' oder 'Springflut' einen kritischen Effekt, wird ein Ahnengeist beschworen, der sofort das Gruppen- oder Schlachtzugsmitglied mit der geringsten Gesundheit innerhalb von 40 Metern um 20% des von Euch geheilten Wertes heilt."
                            },
                            {
                                "description": "Erzielt Ihr mit 'Welle der Heilung', 'Geringe Welle der Heilung' oder 'Springflut' einen kritischen Effekt, wird ein Ahnengeist beschworen, der sofort das Gruppen- oder Schlachtzugsmitglied mit der geringsten Gesundheit innerhalb von 40 Metern um 30% des von Euch geheilten Wertes heilt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1698,
                        "name": "Erdschild",
                        "icon": "spell_nature_skinofearth",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "40 Meter Reichweite",
                                "cost": "15% des Grundmanas",
                                "description": "Schützt das Ziel mit einem Erdschild, der die erhöhte Zauberdauer oder verringerte Kanalisierungsdauer durch erlittenen Schaden um 30% verringert und Angriffe das geschützte Ziel um 150 heilen lässt. Dieser Effekt tritt nur einmal alle paar Sekunden auf. 6 Aufladungen. Hält 10 Min. lang an. Der Erdschild kann immer nur auf einem Ziel gleichzeitig aktiv sein, und auf jedem Ziel kann nur ein Elementarschild aktiv sein."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2059,
                        "name": "Verbesserter Erdschild",
                        "icon": "spell_nature_skinofearth",
                        "x": 2,
                        "y": 8,
                        "req": 1698,
                        "ranks": [
                            {
                                "description": "Erhöht die Aufladungen Eures Zaubers 'Erdschild' um 1 und erhöht seine verursachte Heilung um 5%."
                            },
                            {
                                "description": "Erhöht die Aufladungen Eures Zaubers 'Erdschild' um 2 und erhöht seine verursachte Heilung um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2063,
                        "name": "Flutwellen",
                        "icon": "spell_shaman_tidalwaves",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Beim Wirken von 'Kettenheilung' oder 'Springflut' besteht eine Chance von 20%, dass die Zauberzeit von 'Welle der Heilung' um 30% verringert und die Chance auf einen kritischen Effekt von 'Geringe Welle der Heilung' um 25% erhöht wird. Bleibt aktiv, bis zwei Zauber dieser Art gewirkt wurden. Zusätzlich werden die Bonusheilungseffekte von 'Welle der Heilung' um 4% und von 'Geringe Welle der Heilung' um 2% erhöht."
                            },
                            {
                                "description": "Beim Wirken von 'Kettenheilung' oder 'Springflut' besteht eine Chance von 40%, dass die Zauberzeit von 'Welle der Heilung' um 30% verringert und die Chance auf einen kritischen Effekt von 'Geringe Welle der Heilung' um 25% erhöht wird. Bleibt aktiv, bis zwei Zauber dieser Art gewirkt wurden. Zusätzlich werden die Bonusheilungseffekte von 'Welle der Heilung' um 8% und von 'Geringe Welle der Heilung' um 4% erhöht."
                            },
                            {
                                "description": "Beim Wirken von 'Kettenheilung' oder 'Springflut' besteht eine Chance von 60%, dass die Zauberzeit von 'Welle der Heilung' um 30% verringert und die Chance auf einen kritischen Effekt von 'Geringe Welle der Heilung' um 25% erhöht wird. Bleibt aktiv, bis zwei Zauber dieser Art gewirkt wurden. Zusätzlich werden die Bonusheilungseffekte von 'Welle der Heilung' um 12% und von 'Geringe Welle der Heilung' um 6% erhöht."
                            },
                            {
                                "description": "Beim Wirken von 'Kettenheilung' oder 'Springflut' besteht eine Chance von 80%, dass die Zauberzeit von 'Welle der Heilung' um 30% verringert und die Chance auf einen kritischen Effekt von 'Geringe Welle der Heilung' um 25% erhöht wird. Bleibt aktiv, bis zwei Zauber dieser Art gewirkt wurden. Zusätzlich werden die Bonusheilungseffekte von 'Welle der Heilung' um 16% und von 'Geringe Welle der Heilung' um 8% erhöht."
                            },
                            {
                                "description": "Beim Wirken von 'Kettenheilung' oder 'Springflut' besteht eine Chance von 100%, dass die Zauberzeit von 'Welle der Heilung' um 30% verringert und die Chance auf einen kritischen Effekt von 'Geringe Welle der Heilung' um 25% erhöht wird. Bleibt aktiv, bis zwei Zauber dieser Art gewirkt wurden. Zusätzlich werden die Bonusheilungseffekte von 'Welle der Heilung' um 20% und von 'Geringe Welle der Heilung' um 10% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2064,
                        "name": "Springflut",
                        "icon": "spell_nature_riptide",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Heilt ein freundliches Ziel um 639 bis 691 und im Verlauf von 15 Sek. um weitere 665 Gesundheit. Euer nächstes Wirken von 'Kettenheilung' innerhalb von 15 Sek., das dieses Ziel als Hauptziel hat, zehrt den Effekt auf, der Heilung über Zeit verursacht, und erhöht den von 'Kettenheilung' geheilten Wert um 25%.",
                                "cost": "18% mana",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "211": {
        "name": "Glyphe 'Wasserbeherrschung'",
        "id": "211",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41541",
        "spellKey": "55436",
        "spellId": "55436",
        "prettyName": "",
        "typeOrder": 2
    },
    "212": {
        "name": "Glyphe 'Kettenheilung'",
        "id": "212",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41517",
        "spellKey": "55437",
        "spellId": "55437",
        "prettyName": "",
        "typeOrder": 2
    },
    "213": {
        "name": "Glyphe 'Kettenblitzschlag'",
        "id": "213",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41518",
        "spellKey": "55449",
        "spellId": "55449",
        "prettyName": "",
        "typeOrder": 2
    },
    "214": {
        "name": "Glyphe 'Lava'",
        "id": "214",
        "type": 0,
        "description": "Euer Zauber \\'Lavaeruption\\' verursacht 10% mehr Schaden.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "41524",
        "spellKey": "55454",
        "spellId": "55454",
        "prettyName": "",
        "typeOrder": 2
    },
    "215": {
        "name": "Glyphe 'Schock'",
        "id": "215",
        "type": 0,
        "description": "Verringert die von Euren Schockzaubern verursachte globale Abklingzeit auf 1 Sek.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "41526",
        "spellKey": "55442",
        "spellId": "55442",
        "prettyName": "",
        "typeOrder": 2
    },
    "216": {
        "name": "Glyphe 'Waffe der Lebensgeister'",
        "id": "216",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41527",
        "spellKey": "55439",
        "spellId": "55439",
        "prettyName": "",
        "typeOrder": 2
    },
    "217": {
        "name": "Glyphe 'Totem des Feuerelementars'",
        "id": "217",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Totems des Feuerelementars um 5 Min.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "41529",
        "spellKey": "55455",
        "spellId": "55455",
        "prettyName": "",
        "typeOrder": 2
    },
    "218": {
        "name": "Glyphe 'Totem der Feuernova'",
        "id": "218",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41530",
        "spellKey": "55450",
        "spellId": "55450",
        "prettyName": "",
        "typeOrder": 2
    },
    "219": {
        "name": "Glyphe 'Flammenschock'",
        "id": "219",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41531",
        "spellKey": "55447",
        "spellId": "55447",
        "prettyName": "",
        "typeOrder": 2
    },
    "220": {
        "name": "Glyphe 'Waffe der Flammenzunge'",
        "id": "220",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41532",
        "spellKey": "55451",
        "spellId": "55451",
        "prettyName": "",
        "typeOrder": 2
    },
    "221": {
        "name": "Glyphe 'Frostschock'",
        "id": "221",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41547",
        "spellKey": "55443",
        "spellId": "55443",
        "prettyName": "",
        "typeOrder": 2
    },
    "222": {
        "name": "Glyphe 'Totem des heilenden Flusses'",
        "id": "222",
        "type": 0,
        "description": "Your Healing Stream Totem heals for an additional 20%.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "41533",
        "spellKey": "55456",
        "spellId": "55456",
        "prettyName": "",
        "typeOrder": 2
    },
    "223": {
        "name": "Glyphe 'Welle der Heilung'",
        "id": "223",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41534",
        "spellKey": "55440",
        "spellId": "55440",
        "prettyName": "",
        "typeOrder": 2
    },
    "224": {
        "name": "Glyphe 'Geringe Welle der Heilung'",
        "id": "224",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41535",
        "spellKey": "55438",
        "spellId": "55438",
        "prettyName": "",
        "typeOrder": 2
    },
    "225": {
        "name": "Glyphe 'Blitzschlagschild'",
        "id": "225",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41537",
        "spellKey": "55448",
        "spellId": "55448",
        "prettyName": "",
        "typeOrder": 2
    },
    "226": {
        "name": "Glyphe 'Blitzschlag'",
        "id": "226",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41536",
        "spellKey": "55453",
        "spellId": "55453",
        "prettyName": "",
        "typeOrder": 2
    },
    "227": {
        "name": "Glyphe 'Totem der Manaflut'",
        "id": "227",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41538",
        "spellKey": "55441",
        "spellId": "55441",
        "prettyName": "",
        "typeOrder": 2
    },
    "228": {
        "name": "Glyphe 'Sturmschlag'",
        "id": "228",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41539",
        "spellKey": "55446",
        "spellId": "55446",
        "prettyName": "",
        "typeOrder": 2
    },
    "229": {
        "name": "Glyphe 'Lavapeitsche'",
        "id": "229",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41540",
        "spellKey": "55444",
        "spellId": "55444",
        "prettyName": "",
        "typeOrder": 2
    },
    "230": {
        "name": "Glyphe 'Elementarbeherrschung'",
        "id": "230",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41552",
        "spellKey": "55452",
        "spellId": "55452",
        "prettyName": "",
        "typeOrder": 2
    },
    "231": {
        "name": "Glyphe 'Waffe des Windzorns'",
        "id": "231",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "41542",
        "spellKey": "55445",
        "spellId": "55445",
        "prettyName": "",
        "typeOrder": 2
    },
    "470": {
        "name": "Glyphe 'Astraler Rückruf'",
        "id": "470",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorshaman",
        "itemId": "43381",
        "spellKey": "58058",
        "spellId": "58058",
        "prettyName": "",
        "typeOrder": 2
    },
    "473": {
        "name": "Glyphe 'Erneuertes Leben'",
        "id": "473",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorshaman",
        "itemId": "43385",
        "spellKey": "58059",
        "spellId": "58059",
        "prettyName": "",
        "typeOrder": 2
    },
    "474": {
        "name": "Glyphe 'Wasseratmung'",
        "id": "474",
        "type": 1,
        "description": "Your Water Breathing spell no longer requires a reagent.",
        "icon": "inv_glyph_minorshaman",
        "itemId": "43344",
        "spellKey": "58055",
        "spellId": "58055",
        "prettyName": "",
        "typeOrder": 2
    },
    "475": {
        "name": "Glyphe 'Wasserschild'",
        "id": "475",
        "type": 1,
        "description": "Increases the number of charges on your Water Shield spell by 1.",
        "icon": "inv_glyph_minorshaman",
        "itemId": "43386",
        "spellKey": "58063",
        "spellId": "58063",
        "prettyName": "",
        "typeOrder": 2
    },
    "476": {
        "name": "Glyphe 'Wasserwandeln'",
        "id": "476",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorshaman",
        "itemId": "43388",
        "spellKey": "58057",
        "spellId": "58057",
        "prettyName": "",
        "typeOrder": 2
    },
    "552": {
        "name": "Glyphe 'Geisterwolf'",
        "id": "552",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorshaman",
        "itemId": "43725",
        "spellKey": "59289",
        "spellId": "59289",
        "prettyName": "",
        "typeOrder": 2
    },
    "612": {
        "name": "Glyphe 'Gewitter'",
        "id": "612",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorshaman",
        "itemId": "44923",
        "spellKey": "62132",
        "spellId": "62132",
        "prettyName": "",
        "typeOrder": 2
    },
    "735": {
        "name": "Glyphe 'Donner'",
        "id": "735",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Gewitter\\' um 10 Sek.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "45770",
        "spellKey": "63270",
        "spellId": "63270",
        "prettyName": "",
        "typeOrder": 2
    },
    "736": {
        "name": "Glyphe 'Wildgeist'",
        "id": "736",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "45771",
        "spellKey": "63271",
        "spellId": "63271",
        "prettyName": "",
        "typeOrder": 2
    },
    "737": {
        "name": "Glyphe 'Springflut'",
        "id": "737",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "45772",
        "spellKey": "63273",
        "spellId": "63273",
        "prettyName": "",
        "typeOrder": 2
    },
    "751": {
        "name": "Glyphe 'Erdschild'",
        "id": "751",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "45775",
        "spellKey": "63279",
        "spellId": "63279",
        "prettyName": "",
        "typeOrder": 2
    },
    "752": {
        "name": "Glyphe 'Totem des Ingrimms'",
        "id": "752",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorshaman",
        "itemId": "45776",
        "spellKey": "63280",
        "spellId": "63280",
        "prettyName": "",
        "typeOrder": 2
    },
    "753": {
        "name": "Glyphe 'Verhexen'",
        "id": "753",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Verhexen\\' um 10 Sek.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "45777",
        "spellKey": "63291",
        "spellId": "63291",
        "prettyName": "",
        "typeOrder": 2
    },
    "754": {
        "name": "Glyphe 'Totem der Steinklaue'",
        "id": "754",
        "type": 0,
        "description": "Euer Totem der Steinklaue belegt Euch mit einem Schaden absorbierenden Schild, der 4-mal so stark ist wie der Schild, mit dem es Eure Totems belegt.",
        "icon": "inv_glyph_majorshaman",
        "itemId": "45778",
        "spellKey": "63298",
        "spellId": "63298",
        "prettyName": "",
        "typeOrder": 2
    }
}]
}