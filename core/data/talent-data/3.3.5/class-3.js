{
    "talentData": {
        "characterClass": {
            "classId": 3,
            "name": "Jäger",
            "powerType": "FOCUS",
            "powerTypeId": 2,
            "powerTypeSlug": "focus"
        },
        "talentTrees": [
            {
                "name": "Tierherrschaft",
                "icon": "ability_hunter_bestialdiscipline",
                "backgroundFile": "HunterBeastMastery",
                "overlayColor": "#ff004c",
                "description": "Ein Herr der Wildnis, der viele verschiedene Tiere zähmen kann, damit sie ihm im Kampf beistehen.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1382,
                        "name": "Verbesserter Aspekt des Falken",
                        "icon": "spell_nature_ravenform",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Sind 'Aspekt des Falken' oder 'Aspekt des Drachenfalken' aktiv, besteht bei allen normalen Distanzangriffen eine Chance von 10%, dass das Distanzangriffstempo 12 Sek. lang um 3% erhöht wird."
                            },
                            {
                                "description": "Sind 'Aspekt des Falken' oder 'Aspekt des Drachenfalken' aktiv, besteht bei allen normalen Distanzangriffen eine Chance von 10%, dass das Distanzangriffstempo 12 Sek. lang um 6% erhöht wird."
                            },
                            {
                                "description": "Sind 'Aspekt des Falken' oder 'Aspekt des Drachenfalken' aktiv, besteht bei allen normalen Distanzangriffen eine Chance von 10%, dass das Distanzangriffstempo 12 Sek. lang um 9% erhöht wird."
                            },
                            {
                                "description": "Sind 'Aspekt des Falken' oder 'Aspekt des Drachenfalken' aktiv, besteht bei allen normalen Distanzangriffen eine Chance von 10%, dass das Distanzangriffstempo 12 Sek. lang um 12% erhöht wird."
                            },
                            {
                                "description": "Sind 'Aspekt des Falken' oder 'Aspekt des Drachenfalken' aktiv, besteht bei allen normalen Distanzangriffen eine Chance von 10%, dass das Distanzangriffstempo 12 Sek. lang um 15% erhöht wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1389,
                        "name": "Belastbarkeitsausbildung",
                        "icon": "spell_nature_reincarnation",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Gesundheit Eures Tiers um 2% und Eure gesamte Gesundheit um 1%."
                            },
                            {
                                "description": "Erhöht die Gesundheit Eures Tiers um 4% und Eure gesamte Gesundheit um 2%."
                            },
                            {
                                "description": "Erhöht die Gesundheit Eures Tiers um 6% und Eure gesamte Gesundheit um 3%."
                            },
                            {
                                "description": "Erhöht die Gesundheit Eures Tiers um 8% und Eure gesamte Gesundheit um 4%."
                            },
                            {
                                "description": "Erhöht die Gesundheit Eures Tiers um 10% und Eure gesamte Gesundheit um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1624,
                        "name": "Konzentriertes Feuer",
                        "icon": "ability_hunter_silenthunter",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Jeglicher von Euch verursachte Schaden wird um 1% erhöht, während Euer Tier aktiv ist. Die kritische Trefferchance der Spezialfähigkeiten Eures Tieres wird um 10% erhöht, während 'Fass!' aktiv ist."
                            },
                            {
                                "description": "Jeglicher von Euch verursachte Schaden wird um 2% erhöht, während Euer Tier aktiv ist. Die kritische Trefferchance der Spezialfähigkeiten Eures Tieres wird um 20% erhöht, während 'Fass!' aktiv ist."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1381,
                        "name": "Verbesserter Aspekt des Affen",
                        "icon": "ability_hunter_aspectofthemonkey",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Ausweichbonus von 'Aspekt des Affen' und 'Aspekt des Drachenfalken' um 2%."
                            },
                            {
                                "description": "Erhöht den Ausweichbonus von 'Aspekt des Affen' und 'Aspekt des Drachenfalken' um 4%."
                            },
                            {
                                "description": "Erhöht den Ausweichbonus von 'Aspekt des Affen' und 'Aspekt des Drachenfalken' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1395,
                        "name": "Dickes Fell",
                        "icon": "inv_misc_pelt_bear_03",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Rüstungswertung Eurer Tiere um 7% und Euren Rüstungsbonus durch Gegenstände um 4%."
                            },
                            {
                                "description": "Erhöht die Rüstungswertung Eurer Tiere um 14% und Euren Rüstungsbonus durch Gegenstände um 7%."
                            },
                            {
                                "description": "Erhöht die Rüstungswertung Eurer Tiere um 20% und Euren Rüstungsbonus durch Gegenstände um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1625,
                        "name": "Verbessertes Tier wiederbeleben",
                        "icon": "ability_hunter_beastsoothe",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Tier wiederbeleben' um 3 Sek., reduziert die Manakosten um 20% und erhöht die Gesundheit, mit der das Tier wiederkehrt um zusätzliche 15%."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Tier wiederbeleben' um 6 Sek., reduziert die Manakosten um 40% und erhöht die Gesundheit, mit der das Tier wiederkehrt um zusätzliche 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1384,
                        "name": "Orientierung",
                        "icon": "ability_mount_jungletiger",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den Tempo-Bonus Eures 'Aspekt des Geparden' und 'Aspekt des Rudels' um 4% und erhöht Eure Reitgeschwindigkeit um 5%. Die Erhöhung der Reitgeschwindigkeit ist nicht mit anderen Effekten stapelbar."
                            },
                            {
                                "description": "Erhöht den Tempo-Bonus Eures 'Aspekt des Geparden' und 'Aspekt des Rudels' um 8% und erhöht Eure Reitgeschwindigkeit um 10%. Die Erhöhung der Reitgeschwindigkeit ist nicht mit anderen Effekten stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2138,
                        "name": "Meister der Aspekte",
                        "icon": "ability_hunter_aspectmastery",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Aspekt der Viper - Verringert den Schadensabzug um 10%.<br/><br/>Aspekt des Affen - Verringert den erlittenen Schaden um 5%.<br/><br/>Aspekt des Falken - Erhöht den Angriffskraftbonus um 30%.<br/><br/>Aspekt des Drachenfalken - Kombiniert die Boni von 'Aspekt des Affen' und 'Aspekt des Falken'."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1396,
                        "name": "Entfesselter Zorn",
                        "icon": "ability_bullrush",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Tieren angerichteten Schaden um 3%."
                            },
                            {
                                "description": "Erhöht den von Euren Tieren angerichteten Schaden um 6%."
                            },
                            {
                                "description": "Erhöht den von Euren Tieren angerichteten Schaden um 9%."
                            },
                            {
                                "description": "Erhöht den von Euren Tieren angerichteten Schaden um 12%."
                            },
                            {
                                "description": "Erhöht den von Euren Tieren angerichteten Schaden um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1385,
                        "name": "Verbessertes Tier heilen",
                        "icon": "ability_hunter_mendpet",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eures Zaubers 'Tier heilen' um 10% und gewährt dem Zauber 'Tier heilen' eine Chance von 25%, 1 Fluch, Krankheit, magischen oder Gifteffekt von Eurem Tier zu entfernen."
                            },
                            {
                                "description": "Verringert die Manakosten Eures Zaubers 'Tier heilen' um 20% und gewährt dem Zauber 'Tier heilen' eine Chance von 50%, 1 Fluch, Krankheit, magischen oder Gifteffekt von Eurem Tier zu entfernen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1393,
                        "name": "Wildheit",
                        "icon": "inv_misc_monsterclaw_04",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Tiere um 2%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Tiere um 4%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Tiere um 6%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Tiere um 8%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Tiere um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1388,
                        "name": "Geistbande",
                        "icon": "ability_druid_demoralizingroar",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Regeneriert alle 10 Sek. 1% Eurer Gesundheit und der Eures Tiers, und erhöht sowohl Eure erhaltene Heilung, als auch die Eures Tiers um 5%. Der Effekt ist wirksam, so lang Euer Tier aktiv ist."
                            },
                            {
                                "description": "Regeneriert alle 10 Sek. 2% Eurer gesamten Gesundheit und der Eures Tiers, und erhöht sowohl Eure erhaltene Heilung, als auch die Eures Tiers um 10%. Der Effekt ist wirksam, so lang Euer Tier aktiv ist."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1387,
                        "name": "Einschüchterung",
                        "icon": "ability_devour",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Befiehlt Eurem Tier, sein Ziel einzuschüchtern. Verursacht ein hohes Maß an Bedrohung und betäubt das Ziel 3 Sek. lang. Hält 15 Sek. lang an.",
                                "cost": "8% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "range": "100 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1390,
                        "name": "Wildtierdisziplin",
                        "icon": "spell_nature_abolishmagic",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Fokusregeneration Eurer Tiere um 50%."
                            },
                            {
                                "description": "Erhöht die Fokusregeneration Eurer Tiere um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1799,
                        "name": "Tiere abrichten",
                        "icon": "ability_hunter_animalhandler",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die Angriffskraft Eures Tieres um 5% und erhöht die Dauer Eurer Fähigkeit 'Ruf des Meisters' um 3 Sek."
                            },
                            {
                                "description": "Erhöht die Angriffskraft Eures Tieres um 10% und erhöht die Dauer Eurer Fähigkeit 'Ruf des Meisters' um 6 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1397,
                        "name": "Raserei",
                        "icon": "inv_misc_monsterclaw_03",
                        "x": 2,
                        "y": 5,
                        "req": 1393,
                        "ranks": [
                            {
                                "description": "Gewährt Eurem Tier eine Chance von 20%, 8 Sek. lang eine Zunahme des Angriffstempos um 30% zu erhalten, nachdem es einen kritischen Treffer ausgeführt hat."
                            },
                            {
                                "description": "Gewährt Eurem Tier eine Chance von 40%, 8 Sek. lang eine Zunahme des Angriffstempos um 30% zu erhalten, nachdem es einen kritischen Treffer ausgeführt hat."
                            },
                            {
                                "description": "Gewährt Eurem Tier eine Chance von 60%, 8 Sek. lang eine Zunahme des Angriffstempos um 30% zu erhalten, nachdem es einen kritischen Treffer ausgeführt hat."
                            },
                            {
                                "description": "Gewährt Eurem Tier eine Chance von 80%, 8 Sek. lang eine Zunahme des Angriffstempos um 30% zu erhalten, nachdem es einen kritischen Treffer ausgeführt hat."
                            },
                            {
                                "description": "Gewährt Eurem Tier eine Chance von 100%, 8 Sek. lang eine Zunahme des Angriffstempos um 30% zu erhalten, nachdem es einen kritischen Treffer ausgeführt hat."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1800,
                        "name": "Wilde Eingebung",
                        "icon": "ability_hunter_ferociousinspiration",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Der Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern Eures Begleiters wird um 1% erhöht. Erhöht zusätzlich den Schaden Eurer Fähigkeiten 'Arkaner Schuss' und 'Zuverlässiger Schuss' um 3%."
                            },
                            {
                                "description": "Der Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern Eures Begleiters wird um 2% erhöht. Erhöht zusätzlich den Schaden Eurer Fähigkeiten 'Arkaner Schuss' und 'Zuverlässiger Schuss' um 6%."
                            },
                            {
                                "description": "Der Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern Eures Begleiters wird um 3% erhöht. Erhöht zusätzlich den Schaden Eurer Fähigkeiten 'Arkaner Schuss' und 'Zuverlässiger Schuss' um 9%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1386,
                        "name": "Zorn des Wildtiers",
                        "icon": "ability_druid_ferociousbite",
                        "x": 1,
                        "y": 6,
                        "req": 1387,
                        "ranks": [
                            {
                                "description": "Versetzt Euer Tier in einen Kampfrausch, wodurch es 10 Sek. lang 50% zusätzlichen Schaden verursacht. Einmal im Kampfrausch, gibt es keine Möglichkeit mehr, es zu stoppen, es sei denn, es wird getötet.",
                                "cost": "10% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "range": "100 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1801,
                        "name": "Katzenhafte Reflexe",
                        "icon": "ability_hunter_catlikereflexes",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance um 1% und die Eures Tieres um zusätzliche 3%. Außerdem wird die Abklingzeit Eurer Fähigkeit 'Fass!' um 10 Sek. verringert."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 2% und die Eures Tieres um zusätzliche 6%. Außerdem wird die Abklingzeit Eurer Fähigkeit 'Fass!' um 20 Sek. verringert."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 3% und die Eures Tieres um zusätzliche 9%. Außerdem wird die Abklingzeit Eurer Fähigkeit 'Fass!' um 30 Sek. verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2136,
                        "name": "Kräftigung",
                        "icon": "ability_hunter_invigeration",
                        "x": 0,
                        "y": 7,
                        "req": 1800,
                        "ranks": [
                            {
                                "description": "Erzielt Euer Tier mit einer Spezialfähigkeit einen kritischen Treffer, besteht eine Chance von 50%, dass sofort 1% Eures Mana wiederhergestellt werden."
                            },
                            {
                                "description": "Erzielt Euer Tier mit einer Spezialfähigkeit einen kritischen Treffer, besteht eine Chance von 100%, dass sofort 1% Eures Mana wiederhergestellt werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1802,
                        "name": "Gewandtheit der Schlange",
                        "icon": "ability_hunter_serpentswiftness",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Distanzangriffstempo um 4% und das Nahkampfangriffstempo Eures Tieres um 4%."
                            },
                            {
                                "description": "Erhöht Euer Distanzangriffstempo um 8% und das Nahkampfangriffstempo Eures Tieres um 8%."
                            },
                            {
                                "description": "Erhöht Euer Distanzangriffstempo um 12% und das Nahkampfangriffstempo Eures Tieres um 12%."
                            },
                            {
                                "description": "Erhöht Euer Distanzangriffstempo um 16% und das Nahkampfangriffstempo Eures Tieres um 16%."
                            },
                            {
                                "description": "Erhöht Euer Distanzangriffstempo um 20% und das Nahkampfangriffstempo Eures Tieres um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2140,
                        "name": "Langlebigkeit",
                        "icon": "ability_hunter_longevity",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeiten Eurer Fähigkeiten 'Zorn des Wildtiers' und 'Einschüchterung' sowie der Spezialfähigkeiten Eures Tiers um 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeiten Eurer Fähigkeiten 'Zorn des Wildtiers' und 'Einschüchterung' sowie der Spezialfähigkeiten Eures Tiers um 20%."
                            },
                            {
                                "description": "Verringert die Abklingzeiten Eurer Fähigkeiten 'Zorn des Wildtiers' und 'Einschüchterung' sowie der Spezialfähigkeiten Eures Tiers um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1803,
                        "name": "Wildes Herz",
                        "icon": "ability_hunter_beastwithin",
                        "x": 1,
                        "y": 8,
                        "req": 1386,
                        "ranks": [
                            {
                                "description": "Erhöht jeglichen von Euch verursachten Schaden um 10%. Zudem verfallt Ihr, wenn Euer Tier unter dem Einfluss von 'Zorn des Wildtiers' steht, ebenfalls in Wut und verursacht 10 Sek. lang 10% zusätzlichen Schaden bei um 50% verringerten Manakosten aller Fähigkeiten. Während Ihr wütend seid, spürt Ihr kein Mitleid, Gnade oder Furcht und könnt nur durch den Tod aufgehalten werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2137,
                        "name": "Kobrastöße",
                        "icon": "ability_hunter_cobrastrikes",
                        "x": 2,
                        "y": 8,
                        "req": 1802,
                        "ranks": [
                            {
                                "description": "Ihr habt eine Chance von 20%, dass kritische Treffer Eurer Fähigkeiten 'Arkaner Schuss', 'Zuverlässiger Schuss' oder 'Tödlicher Schuss' die nächsten 2 Spezialangriffe Eures Tiers kritisch treffen lassen."
                            },
                            {
                                "description": "Ihr habt eine Chance von 40%, dass kritische Treffer Eurer Fähigkeiten 'Arkaner Schuss', 'Zuverlässiger Schuss' oder 'Tödlicher Schuss' die nächsten 2 Spezialangriffe Eures Tiers kritisch treffen lassen."
                            },
                            {
                                "description": "Ihr habt eine Chance von 60%, dass kritische Treffer Eurer Fähigkeiten 'Arkaner Schuss', 'Zuverlässiger Schuss' oder 'Tödlicher Schuss' die nächsten 2 Spezialangriffe Eures Tiers kritisch treffen lassen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2227,
                        "name": "Seelenverwandtschaft",
                        "icon": "ability_hunter_separationanxiety",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eures Tieres um 4% und, wenn Euer Tier aktiv ist, Euer Bewegungstempo und das Eures Tieres um 2%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Tieres um 8% und, wenn Euer Tier aktiv ist, Euer Bewegungstempo und das Eures Tieres um 4%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Tieres um 12% und, wenn Euer Tier aktiv ist, Euer Bewegungstempo und das Eures Tieres um 6%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Tieres um 16% und, wenn Euer Tier aktiv ist, Euer Bewegungstempo und das Eures Tieres um 8%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Tieres um 20% und, wenn Euer Tier aktiv ist, Euer Bewegungstempo und das Eures Tieres um 10%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2139,
                        "name": "Herr der Tiere",
                        "icon": "ability_hunter_beastmastery",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Ihr habt die Kunst der Wildtierausbildung gemeistert und erhaltet die Fähigkeit exotische Tiere zu zähmen. Die Gesamtzahl der Fertigkeitspunkte Eures Tiers wird um 4 erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Treffsicherheit",
                "icon": "ability_hunter_focusedaim",
                "backgroundFile": "HunterMarksmanship",
                "overlayColor": "#4c99ff",
                "description": "Ein meisterlicher Bogen- und Scharfschütze, der seine Feinde am besten aus großer Entfernung zur Strecke bringt.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1341,
                        "name": "Verbesserter erschütternder Schuss",
                        "icon": "spell_frost_stun",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Betäubungsdauer Eurer Fähigkeit 'Erschütternder Schuss' um 1 Sek."
                            },
                            {
                                "description": "Erhöht die Betäubungsdauer Eurer Fähigkeit 'Erschütternder Schuss' um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2197,
                        "name": "Konzentriertes Zielen",
                        "icon": "ability_hunter_focusedaim",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken von 'Zuverlässiger Schuss' um 23% und erhöht Eure Trefferchance um 1%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken von 'Zuverlässiger Schuss' um 46% und erhöht Eure Trefferchance um 2%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeiterhöhung beim Wirken von 'Zuverlässiger Schuss' um 70% und erhöht Eure Trefferchance um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1344,
                        "name": "Tödliche Schüsse",
                        "icon": "ability_searingarrow",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Distanzwaffen einen kritischen Treffer zu erzielen, um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Distanzwaffen einen kritischen Treffer zu erzielen, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Distanzwaffen einen kritischen Treffer zu erzielen, um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Distanzwaffen einen kritischen Treffer zu erzielen, um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Distanzwaffen einen kritischen Treffer zu erzielen, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1806,
                        "name": "Sorgfältiges Zielen",
                        "icon": "ability_hunter_zenarchery",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Distanzangriffskraft um einen Betrag, der 33% Eurer gesamten Intelligenz entspricht."
                            },
                            {
                                "description": "Erhöht Eure Distanzangriffskraft um einen Betrag, der 66% Eurer gesamten Intelligenz entspricht."
                            },
                            {
                                "description": "Erhöht Eure Distanzangriffskraft um einen Betrag, der 100% Eurer gesamten Intelligenz entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1343,
                        "name": "Verbessertes Mal des Jägers",
                        "icon": "ability_hunter_snipershot",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Angriffskraftbonus Eurer Fähigkeit 'Mal des Jägers' um 10% und verringert die Manakosten um 33%."
                            },
                            {
                                "description": "Erhöht den Angriffskraftbonus Eurer Fähigkeit 'Mal des Jägers' um 20% und verringert die Manakosten um 66%."
                            },
                            {
                                "description": "Erhöht den Angriffskraftbonus Eurer Fähigkeit 'Mal des Jägers' um 30% und verringert die Manakosten um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1349,
                        "name": "Todbringende Schüsse",
                        "icon": "ability_piercedamage",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Distanzfähigkeiten um 6%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Distanzfähigkeiten um 12%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Distanzfähigkeiten um 18%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Distanzfähigkeiten um 24%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Distanzfähigkeiten um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1818,
                        "name": "An die Kehle gehen",
                        "icon": "ability_hunter_goforthethroat",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Durch Eure kritischen Distanztreffer erhält Euer Tier 25 Fokus."
                            },
                            {
                                "description": "Durch Eure kritischen Distanztreffer erhält Euer Tier 50 Fokus."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1346,
                        "name": "Verbesserter arkaner Schuss",
                        "icon": "ability_impalingbolt",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Arkaner Schuss' um 5%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Arkaner Schuss' um 10%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Arkaner Schuss' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1345,
                        "name": "Gezielter Schuss",
                        "icon": "inv_spear_07",
                        "x": 2,
                        "y": 2,
                        "req": 1349,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "10 Sek. Abklingzeit",
                                "range": "5-35 Meter Reichweite",
                                "cost": "8% des Grundmanas",
                                "description": "Ein gezielter Schuss, der den Distanzschaden um 5 erhöht und auf das Ziel gewirkte Heileffekte um 50% verringert. Hält 10 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1819,
                        "name": "Schneller Tod",
                        "icon": "ability_hunter_rapidkilling",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Die Abklingzeit Eurer Fähigkeit 'Schnellfeuer' wird um 1 Min. verringert. Nach dem Töten eines Gegners, der Erfahrung oder Ehre gewährt, verursacht Euer nächster 'Gezielter Schuss', 'Arkaner Schuss' oder 'Schimärenschuss' 10% mehr Schaden. Der Effekt hält bis zu 20 Sek. lang an."
                            },
                            {
                                "description": "Die Abklingzeit Eurer Fähigkeit 'Schnellfeuer' wird um 2 Min. verringert. Nach dem Töten eines Gegners, der Erfahrung oder Ehre gewährt, verursacht Euer nächster 'Gezielter Schuss', 'Arkaner Schuss' oder 'Schimärenschuss' 20% mehr Schaden. Der Effekt hält bis zu 20 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1348,
                        "name": "Verbesserte Stiche und Bisse",
                        "icon": "ability_hunter_quickshot",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Schlangenbiss' und 'Stich des Flügeldrachen' verursachten Schaden um 10% und das von 'Vipernbiss' abgezogene Mana um 10%. Verringert zusätzlich die Chance, dass Stiche und Bisse, die Schaden im Laufe der Zeit verursachen, gebannt werden um 10%."
                            },
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Schlangenbiss' und 'Stich des Flügeldrachen' verursachten Schaden um 20% und das von 'Vipernbiss' abgezogene Mana um 20%. Verringert zusätzlich die Chance, dass Stiche und Bisse, die Schaden im Laufe der Zeit verursachen, gebannt werden um 20%."
                            },
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Schlangenbiss' und 'Stich des Flügeldrachen' verursachten Schaden um 30% und das von 'Vipernbiss' abgezogene Mana um 30%. Verringert zusätzlich die Chance, dass Stiche und Bisse, die Schaden im Laufe der Zeit verursachen, gebannt werden um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1342,
                        "name": "Effizienz",
                        "icon": "spell_frost_wizardmark",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Schüsse und Stiche um 3%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schüsse und Stiche um 6%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schüsse und Stiche um 9%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schüsse und Stiche um 12%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Schüsse und Stiche um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1351,
                        "name": "Erschütterndes Sperrfeuer",
                        "icon": "spell_arcane_starfire",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erfolgreiche Angriffe mit 'Schimärenschuss' und 'Mehrfachschuss' haben eine Chance von 50%, das Ziel 4 Sek. lang benommen zu machen."
                            },
                            {
                                "description": "Erfolgreiche Angriffe mit 'Schimärenschuss' und 'Mehrfachschuss' haben eine Chance von 100%, das Ziel 4 Sek. lang benommen zu machen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1353,
                        "name": "Bereitschaft",
                        "icon": "ability_hunter_readiness",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Bei Aktivierung schließt diese Fähigkeit die Abklingzeit aller anderen Jägerfähigkeiten, mit Ausnahme von 'Zorn des Wildtiers', sofort ab."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1347,
                        "name": "Sperrfeuer",
                        "icon": "ability_upgrademoonglaive",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Mehrfachschuss', 'Gezielter Schuss' und 'Salve' um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Mehrfachschuss', 'Gezielter Schuss' und 'Salve' um 8%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Mehrfachschuss', 'Gezielter Schuss' und 'Salve' um 12%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1804,
                        "name": "Kampferfahrung",
                        "icon": "ability_hunter_combatexperience",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit und Intelligenz um 2%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit und Intelligenz um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1362,
                        "name": "Distanzwaffen-Spezialisierung",
                        "icon": "inv_weapon_rifle_06",
                        "x": 3,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Distanzwaffen zufügt, um 1%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Distanzwaffen zufügt, um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Distanzwaffen zufügt, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2130,
                        "name": "Durchschlagende Schüsse",
                        "icon": "ability_hunter_piercingshots",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Die kritischen Treffer Eurer Fähigkeiten 'Gezielter Schuss', 'Zuverlässiger Schuss' und 'Schimärenschuss' lassen das Ziel im Verlauf von 8 Sek. für 10% des verursachten Schadens bluten."
                            },
                            {
                                "description": "Die kritischen Treffer Eurer Fähigkeiten 'Gezielter Schuss', 'Zuverlässiger Schuss' und 'Schimärenschuss' lassen das Ziel im Verlauf von 8 Sek. für 20% des verursachten Schadens bluten."
                            },
                            {
                                "description": "Die kritischen Treffer Eurer Fähigkeiten 'Gezielter Schuss', 'Zuverlässiger Schuss' und 'Schimärenschuss' lassen das Ziel im Verlauf von 8 Sek. für 30% des verursachten Schadens bluten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1361,
                        "name": "Aura des Volltreffers",
                        "icon": "ability_trueshot",
                        "x": 1,
                        "y": 6,
                        "req": 1353,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "description": "Erhöht die Angriffskraft von Gruppen- und Schlachtzugsmitgliedern in einem Umkreis von 100 Metern um 10%. Hält bis Abbruch an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1821,
                        "name": "Verbessertes Sperrfeuer",
                        "icon": "ability_upgrademoonglaive",
                        "x": 2,
                        "y": 6,
                        "req": 1347,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Mehrfachschuss' und 'Gezielter Schuss' um 4% und verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeitverringerung beim Kanalisieren von 'Salve' um 33%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Mehrfachschuss' und 'Gezielter Schuss' um 8% und verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeitverringerung beim Kanalisieren von 'Salve' um 66%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Mehrfachschuss' und 'Gezielter Schuss' um 12% und verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeitverringerung beim Kanalisieren von 'Salve' um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1807,
                        "name": "Meisterschütze",
                        "icon": "ability_hunter_mastermarksman",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Eure kritische Trefferchance um 1% und verringert die Manakosten von 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Schimärenschuss' um 5%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance um 2% und verringert die Manakosten von 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Schimärenschuss' um 10%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance um 3% und verringert die Manakosten von 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Schimärenschuss' um 15%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance um 4% und verringert die Manakosten von 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Schimärenschuss' um 20%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance um 5% und verringert die Manakosten von 'Zuverlässiger Schuss', 'Gezielter Schuss' und 'Schimärenschuss' um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2131,
                        "name": "Rasche Erholung",
                        "icon": "ability_hunter_rapidregeneration",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Steht Ihr unter dem Effekt von 'Schnellfeuer', werden alle 3 Sek. 2% Eures Manas wiederhergestellt. Wird 'Schneller Tod' ausgelöst, gewinnt Ihr 6 Sek. lang alle 2 Sek. 1% Eures Manas."
                            },
                            {
                                "description": "Steht Ihr unter dem Effekt von 'Schnellfeuer', werden alle 3 Sek. 4% Eures Manas wiederhergestellt. Wird 'Schneller Tod' ausgelöst, gewinnt Ihr 6 Sek. lang alle 2 Sek. 2% Eures Manas."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2132,
                        "name": "Pfeilhagel",
                        "icon": "ability_hunter_wildquiver",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Wenn Ihr durch 'Automatischer Schuss' Schaden verursacht, besteht eine Chance von 4%, dass ein zusätzlicher Schuss abgefeuert wird, der 80% Waffenschaden als Naturschaden verursacht. Pfeilhagel verbraucht keine Munition."
                            },
                            {
                                "description": "Wenn Ihr durch 'Automatischer Schuss' Schaden verursacht, besteht eine Chance von 8%, dass ein zusätzlicher Schuss abgefeuert wird, der 80% Waffenschaden als Naturschaden verursacht. Pfeilhagel verbraucht keine Munition."
                            },
                            {
                                "description": "Wenn Ihr durch 'Automatischer Schuss' Schaden verursacht, besteht eine Chance von 12%, dass ein zusätzlicher Schuss abgefeuert wird, der 80% Waffenschaden als Naturschaden verursacht. Pfeilhagel verbraucht keine Munition."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1808,
                        "name": "Unterdrückender Schuss",
                        "icon": "ability_theblackarrow",
                        "x": 1,
                        "y": 8,
                        "req": 1807,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "range": "5-35 Meter Reichweite",
                                "cost": "6% des Grundmanas",
                                "description": "Ein Schuss, der 50% Waffenschaden zufügt und das Ziel 3 Sek. lang zum Schweigen bringt. Das Zauberwirken von Nichtspieler-Zielen ist ebenfalls 3 Sek. lang unterbrochen."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2133,
                        "name": "Verbesserter zuverlässiger Schuss",
                        "icon": "ability_hunter_improvedsteadyshot",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Treffer Eurer Fähigkeit 'Zuverlässiger Schuss' haben eine Chance von 5%, den verursachten Schaden Eures nächsten 'Gezielter Schuss', 'Arkaner Schuss' oder 'Schimärenschuss' um 15% zu erhöhen und die Manakosten dieses Schusses um 20% zu verringern."
                            },
                            {
                                "description": "Treffer Eurer Fähigkeit 'Zuverlässiger Schuss' haben eine Chance von 10%, den verursachten Schaden Eures nächsten 'Gezielter Schuss', 'Arkaner Schuss' oder 'Schimärenschuss' um 15% zu erhöhen und die Manakosten dieses Schusses um 20% zu verringern."
                            },
                            {
                                "description": "Treffer Eurer Fähigkeit 'Zuverlässiger Schuss' haben eine Chance von 15%, den verursachten Schaden Eures nächsten 'Gezielter Schuss', 'Arkaner Schuss' oder 'Schimärenschuss' um 15% zu erhöhen und die Manakosten dieses Schusses um 20% zu verringern."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2134,
                        "name": "Zum Tode verurteilt",
                        "icon": "ability_hunter_assassinate",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Schüsse und der Spezialfähigkeiten Eures Tieres an markierten Zielen um 1% und erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Gezielter Schuss', 'Arkaner Schuss', 'Zuverlässiger Schuss', 'Tödlicher Schuss' und 'Schimärenschuss' um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schüsse und der Spezialfähigkeiten Eures Tieres an markierten Zielen um 2% und erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Gezielter Schuss', 'Arkaner Schuss', 'Zuverlässiger Schuss', 'Tödlicher Schuss' und 'Schimärenschuss' um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schüsse und der Spezialfähigkeiten Eures Tieres an markierten Zielen um 3% und erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Gezielter Schuss', 'Arkaner Schuss', 'Zuverlässiger Schuss', 'Tödlicher Schuss' und 'Schimärenschuss' um 6%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schüsse und der Spezialfähigkeiten Eures Tieres an markierten Zielen um 4% und erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Gezielter Schuss', 'Arkaner Schuss', 'Zuverlässiger Schuss', 'Tödlicher Schuss' und 'Schimärenschuss' um 8%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Schüsse und der Spezialfähigkeiten Eures Tieres an markierten Zielen um 5% und erhöht den kritischen Schadensbonus Eurer Fähigkeiten 'Gezielter Schuss', 'Arkaner Schuss', 'Zuverlässiger Schuss', 'Tödlicher Schuss' und 'Schimärenschuss' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2135,
                        "name": "Schimärenschuss",
                        "icon": "ability_hunter_chimerashot2",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "10 Sek. Abklingzeit",
                                "range": "5-35 Meter Reichweite",
                                "cost": "12% des Grundmanas",
                                "description": "Verursacht 125% Waffenschaden, frischt die Dauer des auf das Ziel wirkenden Stiches oder Bisses auf und löst einen Effekt aus:<br/><br/>Schlangenbiss - Verursacht sofort 40% des von 'Schlangenbiss' verursachten Schadens.<br/><br/>Vipernbiss - Stellt Euer Mana in Höhe von 60% des gesamten von 'Vipernbiss' entzogenen Mana wieder her.<br/><br/>Skorpidstich - Versucht, das Ziel 10 Sek. lang zu entwaffnen. Dieser Effekt kann pro Minute nur einmal auftreten."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Überleben",
                "icon": "ability_hunter_camouflage",
                "backgroundFile": "HunterSurvival",
                "overlayColor": "#ff9900",
                "description": "Ein zäher Fährtenleser, der am liebsten Tiergifte, Sprengstoffe und Fallen als tödliche Waffen nutzt.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1623,
                        "name": "Verbessertes Fährtenlesen",
                        "icon": "ability_hunter_improvedtracking",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Während die Fährte von Dämonen, Drachen, Elementaren, Humanoiden, Riesen, Untoten oder Wildtieren verfolgt wird, ist jeglicher verursachter Schaden gegen diese Ziele um 1% erhöht."
                            },
                            {
                                "description": "Während die Fährte von Dämonen, Drachen, Elementaren, Humanoiden, Riesen, Untoten oder Wildtieren verfolgt wird, ist jeglicher verursachter Schaden gegen diese Ziele um 2% erhöht."
                            },
                            {
                                "description": "Während die Fährte von Dämonen, Drachen, Elementaren, Humanoiden, Riesen, Untoten oder Wildtieren verfolgt wird, ist jeglicher verursachter Schaden gegen diese Ziele um 3% erhöht."
                            },
                            {
                                "description": "Während die Fährte von Dämonen, Drachen, Elementaren, Humanoiden, Riesen, Untoten oder Wildtieren verfolgt wird, ist jeglicher verursachter Schaden gegen diese Ziele um 4% erhöht."
                            },
                            {
                                "description": "Während die Fährte von Dämonen, Drachen, Elementaren, Humanoiden, Riesen, Untoten oder Wildtieren verfolgt wird, ist jeglicher verursachter Schaden gegen diese Ziele um 5% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1820,
                        "name": "Falkenauge",
                        "icon": "ability_townwatch",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Distanzwaffen um 2 Meter."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Distanzwaffen um 4 Meter."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Distanzwaffen um 6 Meter."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1621,
                        "name": "Wilde Schläge",
                        "icon": "ability_racial_bloodrage",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Fähigkeiten 'Raptorstoß', 'Mungobiss' und 'Gegenangriff' um 10%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Fähigkeiten 'Raptorstoß', 'Mungobiss' und 'Gegenangriff' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1310,
                        "name": "Sicherer Stand",
                        "icon": "ability_kick",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer von bewegungseinschränkenden Effekten um 10%."
                            },
                            {
                                "description": "Verringert die Dauer von bewegungseinschränkenden Effekten um 20%."
                            },
                            {
                                "description": "Verringert die Dauer von bewegungseinschränkenden Effekten um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1304,
                        "name": "Einfangen",
                        "icon": "spell_nature_stranglevines",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Sowohl Eure Frostfalle als auch Eure Schlangenfalle setzen bei ihrer Auslösung alle Ziele gefangen. Jedes der Ziele wird 2 Sek. lang bewegungsunfähig."
                            },
                            {
                                "description": "Sowohl Eure Frostfalle als auch Eure Schlangenfalle setzen bei ihrer Auslösung alle Ziele gefangen. Jedes der Ziele wird 3 Sek. lang bewegungsunfähig."
                            },
                            {
                                "description": "Sowohl Eure Frostfalle als auch Eure Schlangenfalle setzen bei ihrer Auslösung alle Ziele gefangen. Jedes der Ziele wird 4 Sek. lang bewegungsunfähig."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1305,
                        "name": "Fallenbeherrschung",
                        "icon": "ability_ensnare",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Frostfalle und Eiskältefalle - Erhöht die Dauer um 10%.<br/><br/>Feuerbrandfalle, Sprengfalle und Schwarzer Pfeil - Erhöht den regelmäßigen Schaden um 10%.<br/><br/>Schlangenfalle - Erhöht die Anzahl der beschworenen Schlangen um 2."
                            },
                            {
                                "description": "Frostfalle und Eiskältefalle - Erhöht die Dauer um 20%.<br/><br/>Feuerbrandfalle, Sprengfalle und Schwarzer Pfeil - Erhöht den regelmäßigen Schaden um 20%.<br/><br/>Schlangenfalle - Erhöht die Anzahl der beschworenen Schlangen um 4."
                            },
                            {
                                "description": "Frostfalle und Eiskältefalle - Erhöht die Dauer um 30%.<br/><br/>Feuerbrandfalle, Sprengfalle und Schwarzer Pfeil - Erhöht den regelmäßigen Schaden um 30%.<br/><br/>Schlangenfalle - Erhöht die Anzahl der beschworenen Schlangen um 6."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1810,
                        "name": "Überlebensinstinkte",
                        "icon": "ability_hunter_survivalinstincts",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 2% und erhöht die kritische Trefferchance von 'Arkaner Schuss', 'Zuverlässiger Schuss' und 'Explosivschuss' um 2%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 4% und erhöht die kritische Trefferchance von 'Arkaner Schuss', 'Zuverlässiger Schuss' und 'Explosivschuss' um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1622,
                        "name": "Überlebenskünstler",
                        "icon": "spell_shadow_twilight",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausdauer um 2%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 4%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 6%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 8%."
                            },
                            {
                                "description": "Erhöht Eure Ausdauer um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1814,
                        "name": "Streuschuss",
                        "icon": "ability_golemstormbolt",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Ein einzelner Kurzdistanzschuss, der 50% Waffenschaden zufügt und das Ziel 4 Sek. lang die Orientierung verlieren lässt. Jeglicher erlittene Schaden wird den Effekt aufheben. Stoppt Euren Angriff bei Benutzung.",
                                "cost": "8% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "range": "15 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1311,
                        "name": "Abwehr",
                        "icon": "ability_parry",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Parierchance um 1% und verringert die Dauer aller auf Euch angewandten Entwaffnungseffekte um 16%. Dieser Effekt ist mit anderen Effekten, welche die Entwaffnungsdauer verringern, nicht stapelbar."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 2% und verringert die Dauer aller auf Euch angewandten Entwaffnungseffekte um 25%. Dieser Effekt ist mit anderen Effekten, welche die Entwaffnungsdauer verringern, nicht stapelbar."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 3% und verringert die Dauer aller auf Euch angewandten Entwaffnungseffekte um 50%. Dieser Effekt ist mit anderen Effekten, welche die Entwaffnungsdauer verringern, nicht stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1309,
                        "name": "Überlebenstaktik",
                        "icon": "ability_rogue_feigndeath",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Chance, dass Eurer Fähigkeit 'Totstellen' und Euren Fallen widerstanden wird, um 2% und verringert die Abklingzeit von 'Rückzug' um 2 Sek."
                            },
                            {
                                "description": "Verringert die Chance, dass Eurer Fähigkeit 'Totstellen' und Euren Fallen widerstanden wird, um 4% und verringert die Abklingzeit von 'Rückzug' um 4 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2229,
                        "name": "T.N.T.",
                        "icon": "inv_misc_bomb_05",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Feuerbrandfalle', 'Sprengfalle', 'Explosivschuss' und 'Schwarzer Pfeil' um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Feuerbrandfalle', 'Sprengfalle', 'Explosivschuss' und 'Schwarzer Pfeil' um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Feuerbrandfalle', 'Sprengfalle', 'Explosivschuss' und 'Schwarzer Pfeil' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1306,
                        "name": "Sichern und Laden",
                        "icon": "ability_hunter_lockandload",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Wird ein Ziel durch Eure Eiskälte- oder Frostfalle oder Eure Fähigkeit 'Eiskältepfeil' gefangen, besteht eine Chance von 33%; oder erleidet es durch den regelmäßigen Effekt Eurer Feuerbrandfalle, Sprengfalle oder Eurer Fähigkeit 'Schwarzer Pfeil' Schaden, besteht eine Chance von 2%, dass Eure nächsten 2 Anwendungen von 'Arkaner Schuss' oder 'Explosivschuss' keine Abklingzeit auslösen, kein Mana kosten und keine Munition verbrauchen. Dieser Effekt hat eine Abklingzeit von 22 Sek."
                            },
                            {
                                "description": "Wird ein Ziel durch Eure Eiskälte- oder Frostfalle oder Eure Fähigkeit 'Eiskältepfeil' gefangen, besteht eine Chance von 66%; oder erleidet es durch den regelmäßigen Effekt Eurer Feuerbrandfalle, Sprengfalle oder Eurer Fähigkeit 'Schwarzer Pfeil' Schaden, besteht eine Chance von 4%, dass Eure nächsten 2 Anwendungen von 'Arkaner Schuss' oder 'Explosivschuss' keine Abklingzeit auslösen, kein Mana kosten und keine Munition verbrauchen. Dieser Effekt hat eine Abklingzeit von 22 Sek."
                            },
                            {
                                "description": "Wird ein Ziel durch Eure Eiskälte- oder Frostfalle oder Eure Fähigkeit 'Eiskältepfeil' gefangen, besteht eine Chance von 100%; oder erleidet es durch den regelmäßigen Effekt Eurer Feuerbrandfalle, Sprengfalle oder Eurer Fähigkeit 'Schwarzer Pfeil' Schaden, besteht eine Chance von 6%, dass Eure nächsten 2 Anwendungen von 'Arkaner Schuss' oder 'Explosivschuss' keine Abklingzeit auslösen, kein Mana kosten und keine Munition verbrauchen. Dieser Effekt hat eine Abklingzeit von 22 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2228,
                        "name": "Jäger vs. Natur",
                        "icon": "ability_hunter_huntervswild",
                        "x": 0,
                        "y": 4,
                        "req": 1622,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Nahkampf- und Distanzangriffskraft und die Eures Tieres um einen Wert, der 10% Eurer gesamten Ausdauer entspricht."
                            },
                            {
                                "description": "Erhöht Eure Nahkampf- und Distanzangriffskraft und die Eures Tieres um einen Wert, der 20% Eurer gesamten Ausdauer entspricht."
                            },
                            {
                                "description": "Erhöht Eure Nahkampf- und Distanzangriffskraft und die Eures Tieres um einen Wert, der 30% Eurer gesamten Ausdauer entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1321,
                        "name": "Tötungstrieb",
                        "icon": "spell_holy_blessingofstamina",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance mit jeglichem Angriff einen kritischen Treffer zu erzielen um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance mit jeglichem Angriff einen kritischen Treffer zu erzielen um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance mit jeglichem Angriff einen kritischen Treffer zu erzielen um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1312,
                        "name": "Gegenangriff",
                        "icon": "ability_warrior_challange",
                        "x": 2,
                        "y": 4,
                        "req": 1311,
                        "ranks": [
                            {
                                "description": "Ein Schlag, der aktiv wird, nachdem ein gegnerischer Angriff pariert wurde. Dieser Angriff verursacht 53 Schaden und macht das Ziel 5 Sek. lang bewegungsunfähig. Auf 'Gegenangriff' kann nicht mit 'Blocken', 'Ausweichen' oder 'Parieren' reagiert werden.",
                                "cost": "3% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "5 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1303,
                        "name": "Blitzartige Reflexe",
                        "icon": "spell_nature_invisibilty",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Beweglichkeit um 3%."
                            },
                            {
                                "description": "Erhöht Eure Beweglichkeit um 6%."
                            },
                            {
                                "description": "Erhöht Eure Beweglichkeit um 9%."
                            },
                            {
                                "description": "Erhöht Eure Beweglichkeit um 12%."
                            },
                            {
                                "description": "Erhöht Eure Beweglichkeit um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1809,
                        "name": "Improvisation",
                        "icon": "ability_hunter_resourcefulness",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten aller Fallen, aller Nahkampffähigkeiten und der Fähigkeit 'Schwarzer Pfeil' um 20% und die Abklingzeit aller Fallen sowie von 'Schwarzer Pfeil' um 2 Sek."
                            },
                            {
                                "description": "Verringert die Manakosten aller Fallen, aller Nahkampffähigkeiten und der Fähigkeit 'Schwarzer Pfeil' um 40% und die Abklingzeit aller Fallen sowie von 'Schwarzer Pfeil' um 4 Sek."
                            },
                            {
                                "description": "Verringert die Manakosten aller Fallen, aller Nahkampffähigkeiten und der Fähigkeit 'Schwarzer Pfeil' um 60% und die Abklingzeit aller Fallen sowie von 'Schwarzer Pfeil' um 6 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1812,
                        "name": "Schwäche aufdecken",
                        "icon": "ability_rogue_findweakness",
                        "x": 0,
                        "y": 6,
                        "req": 1303,
                        "ranks": [
                            {
                                "description": "Eure kritischen Distanztreffer haben eine Chance von 33%, dem Ziel den Effekt 'Schwäche aufdecken' zuzufügen. Dieser Effekt erhöht Eure Angriffskraft gegen dieses Ziel 7 Sek. lang um 25% Eurer Beweglichkeit."
                            },
                            {
                                "description": "Eure kritischen Distanztreffer haben eine Chance von 66%, dem Ziel den Effekt 'Schwäche aufdecken' zuzufügen. Dieser Effekt erhöht Eure Angriffskraft gegen dieses Ziel 7 Sek. lang um 25% Eurer Beweglichkeit."
                            },
                            {
                                "description": "Eure kritischen Distanztreffer haben eine Chance von 100%, dem Ziel den Effekt 'Schwäche aufdecken' zuzufügen. Dieser Effekt erhöht Eure Angriffskraft gegen dieses Ziel 7 Sek. lang um 25% Eurer Beweglichkeit."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1325,
                        "name": "Stich des Flügeldrachen",
                        "icon": "inv_spear_02",
                        "x": 1,
                        "y": 6,
                        "req": 1321,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "range": "5-35 Meter Reichweite",
                                "cost": "8% des Grundmanas",
                                "description": "Verschießt einen Stachel, der das Ziel 30 Sek. lang einschläfert. Jeglicher erlittene Schaden hebt den Effekt wieder auf. Wenn das Ziel wieder aufwacht, verursacht der Stich im Verlauf von 6 Sek. insgesamt 300 Naturschaden. Auf dem Ziel kann gleichzeitig nur ein Biss oder Stich pro Jäger aktiv sein."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1811,
                        "name": "Jagdfieber",
                        "icon": "ability_hunter_thrillofthehunt",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Wenn einer Eurer Schüsse einen kritischen Treffer erzielt, besteht eine Chance von 33%, dass Ihr 40% der Manakosten zurückerhaltet."
                            },
                            {
                                "description": "Wenn einer Eurer Schüsse einen kritischen Treffer erzielt, besteht eine Chance von 66%, dass Ihr 40% der Manakosten zurückerhaltet."
                            },
                            {
                                "description": "Wenn einer Eurer Schüsse einen kritischen Treffer erzielt, besteht eine Chance von 100%, dass Ihr 40% der Manakosten zurückerhaltet."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1813,
                        "name": "Meister der Taktik",
                        "icon": "ability_hunter_mastertactitian",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure erfolgreichen Distanzangriffe haben eine Chance von 10%, Eure kritische Trefferchance mit allen Angriffen 8 Sek. lang um 2% zu erhöhen."
                            },
                            {
                                "description": "Eure erfolgreichen Distanzangriffe haben eine Chance von 10%, Eure kritische Trefferchance mit allen Angriffen 8 Sek. lang um 4% zu erhöhen."
                            },
                            {
                                "description": "Eure erfolgreichen Distanzangriffe haben eine Chance von 10%, Eure kritische Trefferchance mit allen Angriffen 8 Sek. lang um 6% zu erhöhen."
                            },
                            {
                                "description": "Eure erfolgreichen Distanzangriffe haben eine Chance von 10%, Eure kritische Trefferchance mit allen Angriffen 8 Sek. lang um 8% zu erhöhen."
                            },
                            {
                                "description": "Eure erfolgreichen Distanzangriffe haben eine Chance von 10%, Eure kritische Trefferchance mit allen Angriffen 8 Sek. lang um 10% zu erhöhen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2141,
                        "name": "Giftpfeile",
                        "icon": "ability_hunter_potentvenom",
                        "x": 1,
                        "y": 7,
                        "req": 1325,
                        "ranks": [
                            {
                                "description": "Wird 'Stich des Flügeldrachen' gebannt, wird der Bannende ebenfalls für 16% der verbliebenen Dauer von 'Stich des Flügeldrachen' betroffen. Erhöht zudem jeglichen Schaden um 1%, den Ihr an Zielen verursacht, die von Eurem 'Schlangenbiss' betroffen sind."
                            },
                            {
                                "description": "Wird 'Stich des Flügeldrachen' gebannt, wird der Bannende ebenfalls für 25% der verbliebenen Dauer von 'Stich des Flügeldrachen' betroffen. Erhöht zudem jeglichen Schaden um 2%, den Ihr an Zielen verursacht, die von Eurem 'Schlangenbiss' betroffen sind."
                            },
                            {
                                "description": "Wird 'Stich des Flügeldrachen' gebannt, wird der Bannende ebenfalls für 50% der verbliebenen Dauer von 'Stich des Flügeldrachen' betroffen. Erhöht zudem jeglichen Schaden um 3%, den Ihr an Zielen verursacht, die von Eurem 'Schlangenbiss' betroffen sind."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2142,
                        "name": "Kein Entkommen",
                        "icon": "ability_hunter_pointofnoescape",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance aller Eurer Angriffe gegen alle Ziele, die von Euren Fähigkeiten 'Frostfalle', 'Eiskältefalle' und 'Eiskältepfeil' betroffen sind, um 3%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Eurer Angriffe gegen alle Ziele, die von Euren Fähigkeiten 'Frostfalle', 'Eiskältefalle' und 'Eiskältepfeil' betroffen sind, um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1322,
                        "name": "Schwarzer Pfeil",
                        "icon": "spell_shadow_painspike",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "range": "5-35 Meter Reichweite",
                                "cost": "6% des Grundmanas",
                                "description": "Feuert einen schwarzen Pfeil auf das Ziel ab, der Euren verursachten Schaden am Ziel um 6% erhöht und im Verlauf von 15 Sek. 786 Schattenschaden zufügt. Der schwarze Pfeil teilt sich eine Abklingzeit mit Fallenzaubern."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2143,
                        "name": "Scharfschützentraining",
                        "icon": "ability_hunter_longshots",
                        "x": 3,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Tödlicher Schuss' um 5%. Wenn Ihr 6 Sek. lang stillsteht, erhöht das Scharfschützentraining den verursachten Schaden Eurer Fähigkeiten 'Zuverlässiger Schuss', 'Gezielter Schuss', 'Schwarzer Pfeil' und 'Explosivschuss' um 2%. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Tödlicher Schuss' um 10%. Wenn Ihr 6 Sek. lang stillsteht, erhöht das Scharfschützentraining den verursachten Schaden Eurer Fähigkeiten 'Zuverlässiger Schuss', 'Gezielter Schuss', 'Schwarzer Pfeil' und 'Explosivschuss' um 4%. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeit 'Tödlicher Schuss' um 15%. Wenn Ihr 6 Sek. lang stillsteht, erhöht das Scharfschützentraining den verursachten Schaden Eurer Fähigkeiten 'Zuverlässiger Schuss', 'Gezielter Schuss', 'Schwarzer Pfeil' und 'Explosivschuss' um 6%. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2144,
                        "name": "Jagdgesellschaft",
                        "icon": "ability_hunter_huntingparty",
                        "x": 2,
                        "y": 9,
                        "req": 1811,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um zusätzliche 1%. Die kritischen Treffer Eurer Fähigkeiten 'Arkaner Schuss', 'Explosivschuss' und 'Zuverlässiger Schuss' haben eine Chance von 33%, bei bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 5 Sek. 1% ihres maximalen Manas wiederherzustellen. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um zusätzliche 2%. Die kritischen Treffer Eurer Fähigkeiten 'Arkaner Schuss', 'Explosivschuss' und 'Zuverlässiger Schuss' haben eine Chance von 66%, bei bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 5 Sek. 1% ihres maximalen Manas wiederherzustellen. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um zusätzliche 3%. Die kritischen Treffer Eurer Fähigkeiten 'Arkaner Schuss', 'Explosivschuss' und 'Zuverlässiger Schuss' haben eine Chance von 100%, bei bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 5 Sek. 1% ihres maximalen Manas wiederherzustellen. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2145,
                        "name": "Explosivschuss",
                        "icon": "ability_hunter_explosiveshot",
                        "x": 1,
                        "y": 10,
                        "req": 1322,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "5-35 Meter Reichweite",
                                "cost": "7% des Grundmanas",
                                "description": "Feuert ein Explosivgeschoss, das beim feindlichen Ziel 146 bis 174 Feuerschaden verursacht. Die Sprengladungen verursachen 2 Sek. lang pro Sekunde weiteren Schaden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "351": {
        "name": "Glyphe 'Gezielter Schuss'",
        "id": "351",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42897",
        "spellKey": "56824",
        "spellId": "56824",
        "prettyName": "",
        "typeOrder": 2
    },
    "352": {
        "name": "Glyphe 'Arkaner Schuss'",
        "id": "352",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42898",
        "spellKey": "56841",
        "spellId": "56841",
        "prettyName": "",
        "typeOrder": 2
    },
    "353": {
        "name": "Glyphe 'Wildtier'",
        "id": "353",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42899",
        "spellKey": "56857",
        "spellId": "56857",
        "prettyName": "",
        "typeOrder": 2
    },
    "354": {
        "name": "Glyphe 'Heilen'",
        "id": "354",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42900",
        "spellKey": "56833",
        "spellId": "56833",
        "prettyName": "",
        "typeOrder": 2
    },
    "355": {
        "name": "Glyphe 'Aspekt der Viper'",
        "id": "355",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42901",
        "spellKey": "56851",
        "spellId": "56851",
        "prettyName": "",
        "typeOrder": 2
    },
    "356": {
        "name": "Glyphe 'Zorn des Wildtiers'",
        "id": "356",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42902",
        "spellKey": "56830",
        "spellId": "56830",
        "prettyName": "",
        "typeOrder": 2
    },
    "357": {
        "name": "Glyphe 'Abschreckung'",
        "id": "357",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42903",
        "spellKey": "56850",
        "spellId": "56850",
        "prettyName": "",
        "typeOrder": 2
    },
    "358": {
        "name": "Glyphe 'Rückzug'",
        "id": "358",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42904",
        "spellKey": "56844",
        "spellId": "56844",
        "prettyName": "",
        "typeOrder": 2
    },
    "359": {
        "name": "Glyphe 'Eiskältefalle'",
        "id": "359",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42905",
        "spellKey": "56845",
        "spellId": "56845",
        "prettyName": "",
        "typeOrder": 2
    },
    "360": {
        "name": "Glyphe 'Frostfalle'",
        "id": "360",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42906",
        "spellKey": "56847",
        "spellId": "56847",
        "prettyName": "",
        "typeOrder": 2
    },
    "361": {
        "name": "Glyphe 'Mal des Jägers'",
        "id": "361",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42907",
        "spellKey": "56829",
        "spellId": "56829",
        "prettyName": "",
        "typeOrder": 2
    },
    "362": {
        "name": "Glyphe 'Feuerbrandfalle'",
        "id": "362",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42908",
        "spellKey": "56846",
        "spellId": "56846",
        "prettyName": "",
        "typeOrder": 2
    },
    "363": {
        "name": "Glyphe 'Falke'",
        "id": "363",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42909",
        "spellKey": "56856",
        "spellId": "56856",
        "prettyName": "",
        "typeOrder": 2
    },
    "364": {
        "name": "Glyphe 'Mehrfachschuss'",
        "id": "364",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42910",
        "spellKey": "56836",
        "spellId": "56836",
        "prettyName": "",
        "typeOrder": 2
    },
    "365": {
        "name": "Glyphe 'Schnellfeuer'",
        "id": "365",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42911",
        "spellKey": "56828",
        "spellId": "56828",
        "prettyName": "",
        "typeOrder": 2
    },
    "366": {
        "name": "Glyphe 'Schlangenbiss'",
        "id": "366",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42912",
        "spellKey": "56832",
        "spellId": "56832",
        "prettyName": "",
        "typeOrder": 2
    },
    "367": {
        "name": "Glyphe 'Schlangenfalle'",
        "id": "367",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42913",
        "spellKey": "56849",
        "spellId": "56849",
        "prettyName": "",
        "typeOrder": 2
    },
    "368": {
        "name": "Glyphe 'Zuverlässiger Schuss'",
        "id": "368",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42914",
        "spellKey": "56826",
        "spellId": "56826",
        "prettyName": "",
        "typeOrder": 2
    },
    "369": {
        "name": "Glyphe 'Aura des Volltreffers'",
        "id": "369",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42915",
        "spellKey": "56842",
        "spellId": "56842",
        "prettyName": "",
        "typeOrder": 2
    },
    "370": {
        "name": "Glyphe 'Salve'",
        "id": "370",
        "type": 0,
        "description": "Decreases the mana cost of Volley by 20%.",
        "icon": "inv_glyph_majorhunter",
        "itemId": "42916",
        "spellKey": "56838",
        "spellId": "56838",
        "prettyName": "",
        "typeOrder": 2
    },
    "371": {
        "name": "Glyphe 'Stich des Flügeldrachen'",
        "id": "371",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "42917",
        "spellKey": "56848",
        "spellId": "56848",
        "prettyName": "",
        "typeOrder": 2
    },
    "439": {
        "name": "Glyphe 'Tier wiederbeleben'",
        "id": "439",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorhunter",
        "itemId": "43338",
        "spellKey": "57866",
        "spellId": "57866",
        "prettyName": "",
        "typeOrder": 2
    },
    "440": {
        "name": "Glyphe 'Tier heilen'",
        "id": "440",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorhunter",
        "itemId": "43350",
        "spellKey": "57870",
        "spellId": "57870",
        "prettyName": "",
        "typeOrder": 2
    },
    "441": {
        "name": "Glyphe 'Totstellen'",
        "id": "441",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorhunter",
        "itemId": "43351",
        "spellKey": "57903",
        "spellId": "57903",
        "prettyName": "",
        "typeOrder": 2
    },
    "442": {
        "name": "Glyphe 'Wildtier ängstigen'",
        "id": "442",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorhunter",
        "itemId": "43356",
        "spellKey": "57902",
        "spellId": "57902",
        "prettyName": "",
        "typeOrder": 2
    },
    "443": {
        "name": "Glyphe 'Aspekt des Rudels'",
        "id": "443",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorhunter",
        "itemId": "43355",
        "spellKey": "57904",
        "spellId": "57904",
        "prettyName": "",
        "typeOrder": 2
    },
    "444": {
        "name": "Glyphe 'Besessene Stärke'",
        "id": "444",
        "type": 1,
        "description": "Increases the damage your pet inflicts while using Eyes of the Beast by 50%.",
        "icon": "inv_glyph_minorhunter",
        "itemId": "43354",
        "spellKey": "57900",
        "spellId": "57900",
        "prettyName": "",
        "typeOrder": 2
    },
    "677": {
        "name": "Glyphe 'Schimärenschuss'",
        "id": "677",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "45625",
        "spellKey": "63065",
        "spellId": "63065",
        "prettyName": "",
        "typeOrder": 2
    },
    "691": {
        "name": "Glyphe 'Explosivschuss'",
        "id": "691",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "45731",
        "spellKey": "63066",
        "spellId": "63066",
        "prettyName": "",
        "typeOrder": 2
    },
    "692": {
        "name": "Glyphe 'Tödlicher Schuss'",
        "id": "692",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "45732",
        "spellKey": "63067",
        "spellId": "63067",
        "prettyName": "",
        "typeOrder": 2
    },
    "693": {
        "name": "Glyphe 'Sprengfalle'",
        "id": "693",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "45733",
        "spellKey": "63068",
        "spellId": "63068",
        "prettyName": "",
        "typeOrder": 2
    },
    "694": {
        "name": "Glyphe 'Streuschuss'",
        "id": "694",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "45734",
        "spellKey": "63069",
        "spellId": "63069",
        "prettyName": "",
        "typeOrder": 2
    },
    "695": {
        "name": "Glyphe 'Raptorstoß'",
        "id": "695",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorhunter",
        "itemId": "45735",
        "spellKey": "63086",
        "spellId": "63086",
        "prettyName": "",
        "typeOrder": 2
    }
}]
}