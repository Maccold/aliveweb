{
  "talentData" : {
    "talentTrees" : [ {
      "name" : "Wildheit",
      "icon" : "ability_druid_kingofthejungle",
      "backgroundFile" : "HUNTERPETFEROCITY",
      "overlayColor" : "#ff19ff",
      "description" : "",
      "treeNo" : 0,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "petFamilies" : [ {
        "familyId" : 7,
        "name" : "Aasvogel",
        "icon" : "ability_hunter_pet_vulture",
        "petTypeId" : 0,
        "categoryBit" : 4,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.9,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 50,
        "name" : "Fuchs",
        "icon" : "inv_misc_monstertail_07",
        "petTypeId" : 0,
        "categoryBit" : 27,
        "minScale" : 0.6,
        "minScaleLevel" : 1,
        "maxScale" : 0.9,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 46,
        "name" : "Geisterbestie",
        "icon" : "ability_druid_primalprecision",
        "petTypeId" : 0,
        "categoryBit" : 58,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.1,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 52,
        "name" : "Hund",
        "icon" : "inv_jewelry_necklace_22",
        "petTypeId" : 0,
        "categoryBit" : 29,
        "minScale" : 0.6,
        "minScaleLevel" : 1,
        "maxScale" : 0.8,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 68,
        "name" : "Hydra",
        "icon" : "trade_archaeology_whitehydrafigurine",
        "petTypeId" : 0,
        "categoryBit" : 55,
        "minScale" : 0.5,
        "minScaleLevel" : 0,
        "maxScale" : 0.63,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 25,
        "name" : "Hyäne",
        "icon" : "ability_hunter_pet_hyena",
        "petTypeId" : 0,
        "categoryBit" : 10,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 0.9,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 2,
        "name" : "Katze",
        "icon" : "ability_hunter_pet_cat",
        "petTypeId" : 0,
        "categoryBit" : 5,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.1,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 45,
        "name" : "Kernhund",
        "icon" : "ability_hunter_pet_corehound",
        "petTypeId" : 0,
        "categoryBit" : 59,
        "minScale" : 0.3,
        "minScaleLevel" : 1,
        "maxScale" : 0.5,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 37,
        "name" : "Motte",
        "icon" : "ability_hunter_pet_moth",
        "petTypeId" : 0,
        "categoryBit" : 11,
        "minScale" : 0.35,
        "minScaleLevel" : 1,
        "maxScale" : 0.65,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 11,
        "name" : "Raptor",
        "icon" : "ability_hunter_pet_raptor",
        "petTypeId" : 0,
        "categoryBit" : 13,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.8,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 39,
        "name" : "Teufelssaurier",
        "icon" : "ability_hunter_pet_devilsaur",
        "petTypeId" : 0,
        "categoryBit" : 25,
        "minScale" : 0.3,
        "minScaleLevel" : 1,
        "maxScale" : 0.5,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 12,
        "name" : "Weitschreiter",
        "icon" : "ability_hunter_pet_tallstrider",
        "petTypeId" : 0,
        "categoryBit" : 19,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.8,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 66,
        "name" : "Wespe",
        "icon" : "ability_hunter_pet_wasp",
        "petTypeId" : 0,
        "categoryBit" : 60,
        "minScale" : 0.2,
        "minScaleLevel" : 1,
        "maxScale" : 0.3,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 1,
        "name" : "Wolf",
        "icon" : "ability_hunter_pet_wolf",
        "petTypeId" : 0,
        "categoryBit" : 23,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.0,
        "maxScaleLevel" : 60
      } ],
      "primarySpells" : [ ],
      "masteries" : [ ],
      "talents" : [ {
        "id" : 2107,
        "name" : "Kobrareflexe",
        "icon" : "ability_hunter_serpentswiftness",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht das Angriffstempo Eures Tiers um 5%."
        }, {
          "description" : "Erhöht das Angriffstempo Eures Tiers um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2109,
        "name" : "Spurt",
        "icon" : "ability_druid_dash",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "cost" : "30 Fokus",
          "castTime" : "Spontanzauber",
          "cooldown" : "32 Sek. Abklingzeit",
          "description" : "Erhöht das Bewegungstempo Eures Tiers 16 Sek. lang um 80%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 2055742496,
        "categoryMask1" : 209715200
      }, {
        "id" : 2203,
        "name" : "Sinkflug",
        "icon" : "spell_shadow_burningspirit",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "cost" : "30 Fokus",
          "castTime" : "Spontanzauber",
          "cooldown" : "32 Sek. Abklingzeit",
          "description" : "Erhöht das Bewegungstempo Eures Tiers 16 Sek. lang um 80%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 2064,
        "categoryMask1" : 268435456
      }, {
        "id" : 2112,
        "name" : "Große Ausdauer",
        "icon" : "spell_nature_unyeildingstamina",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 4%."
        }, {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 8%."
        }, {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 12%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2113,
        "name" : "Natürliche Rüstung",
        "icon" : "spell_nature_spiritarmor",
        "x" : 3,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Rüstung Eures Tiers um 5%."
        }, {
          "description" : "Erhöht die Rüstung Eures Tiers um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2124,
        "name" : "Verbessertes Ducken",
        "icon" : "ability_druid_cower",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Die Verringerung des Bewegungstempos Eures Tiers durch die Fähigkeit 'Ducken' wird um 50% gesenkt."
        }, {
          "description" : "Die Verringerung des Bewegungstempos Eures Tiers durch die Fähigkeit 'Ducken' wird um 100% gesenkt."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2128,
        "name" : "Blutdurstig",
        "icon" : "ability_druid_primaltenacity",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Die Angriffe Eures Tiers haben eine Chance von 10%, es im Verlauf von 5 Sek. um 5% seiner gesamten Gesundheit zu heilen."
        }, {
          "description" : "Die Angriffe Eures Tiers haben eine Chance von 20%, es im Verlauf von 5 Sek. um 5% seiner gesamten Gesundheit zu heilen."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2125,
        "name" : "Stachelhalsband",
        "icon" : "inv_jewelry_necklace_22",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 3%."
        }, {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 6%."
        }, {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 9%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2151,
        "name" : "Tempo des Ebers",
        "icon" : "ability_hunter_pet_boar",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht das Bewegungstempo Eures Tiers um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2106,
        "name" : "Herdenschlachtung",
        "icon" : "inv_misc_monsterhorn_06",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 1% erhöht."
        }, {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 2% erhöht."
        }, {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 3% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2152,
        "name" : "Löwenherz",
        "icon" : "inv_bannerpvp_02",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Verringert die Dauer aller auf Euer Tier wirkenden Betäubungs- und Furchteffekte um 15%."
        }, {
          "description" : "Verringert die Dauer aller auf Euer Tier wirkenden Betäubungs- und Furchteffekte um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2111,
        "name" : "Sturmangriff",
        "icon" : "ability_hunter_pet_bear",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "cost" : "35 Fokus",
          "range" : "8-25 Meter Reichweite range",
          "castTime" : "Sofort",
          "cooldown" : "25 Sek. Abklingzeit",
          "description" : "Euer Tier stürmt auf sein Ziel zu und macht es 1 Sek. lang bewegungsunfähig. Zusätzlich wird die Nahkampfangriffskraft seines nächsten Angriffs um 25% erhöht."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 713565216,
        "categoryMask1" : 243269632
      }, {
        "id" : 2219,
        "name" : "Sturzflug",
        "icon" : "ability_hunter_pet_dragonhawk",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "cost" : "35 Fokus",
          "range" : "8-25 Meter Reichweite range",
          "castTime" : "Sofort",
          "cooldown" : "25 Sek. Abklingzeit",
          "description" : "Euer Tier fliegt im Sturzflug auf sein Ziel zu und macht es 1 Sek. lang unbeweglich. Zusätzlich wird die Nahkampfangriffskraft seines nächsten Angriffs um 25% erhöht."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 2064,
        "categoryMask1" : 268435456
      }, {
        "id" : 2156,
        "name" : "Herz des Phönix",
        "icon" : "inv_misc_pheonixpet_01",
        "x" : 1,
        "y" : 3,
        "req" : 2128,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "8 Min. Abklingzeit",
          "description" : "Lässt bei Benutzung Euer totes Tier auf wundersame Weise mit voller Gesundheit ins Leben zurückkehren"
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2129,
        "name" : "Spinnenbiss",
        "icon" : "ability_hunter_pet_spider",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Erhöht die kritische Trefferchance Eures Tiers um 3%."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eures Tiers um 6%."
        }, {
          "description" : "Erhöht die kritische Trefferchance Eures Tiers um 9%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2154,
        "name" : "Großer Widerstand",
        "icon" : "spell_nature_resistnature",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 5% weniger Schaden."
        }, {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 10% weniger Schaden."
        }, {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 15% weniger Schaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2155,
        "name" : "Tollwut",
        "icon" : "ability_druid_berserk",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "45 Sek. Abklingzeit",
          "description" : "Euer Tier gerät in einen tödlichen Blutrausch. Erfolgreiche Angriffe haben eine Chance, die Angriffskraft um 5% zu erhöhen. Der Effekt ist bis zu 5-mal stapelbar. Hält 20 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2153,
        "name" : "Wunden lecken",
        "icon" : "ability_hunter_mendpet",
        "x" : 1,
        "y" : 4,
        "req" : 2156,
        "ranks" : [ {
          "castTime" : "Kanalisiert",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Euer Tier heilt sich selbst im Verlauf von 5 Sek. um 100% seiner gesamten Gesundheit, so lang es diese Fähigkeit kanalisiert."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2157,
        "name" : "Ruf der Wildnis",
        "icon" : "ability_druid_kingofthejungle",
        "x" : 2,
        "y" : 4,
        "req" : 2129,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "5 Min. Abklingzeit",
          "description" : "Euer Tier brüllt und erhöht sowohl seine eigene als auch Eure Nahkampf- und Distanzangriffskraft um 10%. Hält 20 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2254,
        "name" : "Haiangriff",
        "icon" : "inv_misc_fish_35",
        "x" : 0,
        "y" : 5,
        "ranks" : [ {
          "description" : "Die Angriffe Eures Begleiters verursachen zusätzlich 3% Schaden."
        }, {
          "description" : "Die Angriffe Eures Begleiters verursachen zusätzlich 6% Schaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2253,
        "name" : "Wilde Jagd",
        "icon" : "inv_misc_horn_04",
        "x" : 2,
        "y" : 5,
        "req" : 2157,
        "ranks" : [ {
          "description" : "Wenn Euer Tier über 50 Fokus oder mehr verfügt, verursachen die einfachen Angriffe Eures Tiers 60% mehr Schaden, kosten jedoch 50% mehr Fokus."
        }, {
          "description" : "Wenn Euer Tier über 50 Fokus oder mehr verfügt, verursachen die einfachen Angriffe Eures Tiers 120% mehr Schaden, kosten jedoch 100% mehr Fokus."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 0
    }, {
      "name" : "Hartnäckigkeit",
      "icon" : "ability_druid_demoralizingroar",
      "backgroundFile" : "HunterPetTenacity",
      "overlayColor" : "#ff0000",
      "description" : "",
      "treeNo" : 1,
      "roles" : {
        "tank" : true,
        "healer" : false,
        "dps" : false
      },
      "petFamilies" : [ {
        "familyId" : 4,
        "name" : "Bär",
        "icon" : "ability_hunter_pet_bear",
        "petTypeId" : 1,
        "categoryBit" : 1,
        "minScale" : 0.6,
        "minScaleLevel" : 1,
        "maxScale" : 1.0,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 5,
        "name" : "Eber",
        "icon" : "ability_hunter_pet_boar",
        "petTypeId" : 1,
        "categoryBit" : 3,
        "minScale" : 0.6,
        "minScaleLevel" : 1,
        "maxScale" : 1.0,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 9,
        "name" : "Gorilla",
        "icon" : "ability_hunter_pet_gorilla",
        "petTypeId" : 1,
        "categoryBit" : 9,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.0,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 8,
        "name" : "Krebs",
        "icon" : "ability_hunter_pet_crab",
        "petTypeId" : 1,
        "categoryBit" : 6,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.4,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 6,
        "name" : "Krokilisk",
        "icon" : "ability_hunter_pet_crocolisk",
        "petTypeId" : 1,
        "categoryBit" : 7,
        "minScale" : 0.4,
        "minScaleLevel" : 1,
        "maxScale" : 0.6,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 53,
        "name" : "Käfer",
        "icon" : "inv_scarab_silver",
        "petTypeId" : 1,
        "categoryBit" : 30,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.7,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 43,
        "name" : "Rhinozeros",
        "icon" : "ability_hunter_pet_rhino",
        "petTypeId" : 1,
        "categoryBit" : 61,
        "minScale" : 0.35,
        "minScaleLevel" : 1,
        "maxScale" : 0.56,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 55,
        "name" : "Schieferspinne",
        "icon" : "ability_hunter_pet_spider",
        "petTypeId" : 1,
        "categoryBit" : 57,
        "minScale" : 1.0,
        "minScaleLevel" : 1,
        "maxScale" : 1.45,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 21,
        "name" : "Schildkröte",
        "icon" : "ability_hunter_pet_turtle",
        "petTypeId" : 1,
        "categoryBit" : 21,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.72,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 20,
        "name" : "Skorpid",
        "icon" : "ability_hunter_pet_scorpid",
        "petTypeId" : 1,
        "categoryBit" : 15,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.0,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 32,
        "name" : "Sphärenjäger",
        "icon" : "ability_hunter_pet_warpstalker",
        "petTypeId" : 1,
        "categoryBit" : 21,
        "minScale" : 0.45,
        "minScaleLevel" : 1,
        "maxScale" : 0.6,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 42,
        "name" : "Wurm",
        "icon" : "ability_hunter_pet_worm",
        "petTypeId" : 1,
        "categoryBit" : 62,
        "minScale" : 0.7,
        "minScaleLevel" : 1,
        "maxScale" : 1.0,
        "maxScaleLevel" : 60
      } ],
      "primarySpells" : [ ],
      "masteries" : [ ],
      "talents" : [ {
        "id" : 2114,
        "name" : "Kobrareflexe",
        "icon" : "ability_hunter_serpentswiftness",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht das Angriffstempo Eures Tiers um 5%."
        }, {
          "description" : "Erhöht das Angriffstempo Eures Tiers um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2237,
        "name" : "Sturmangriff",
        "icon" : "ability_hunter_pet_bear",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "cost" : "35 Fokus",
          "range" : "8-25 Meter Reichweite range",
          "castTime" : "Sofort",
          "cooldown" : "25 Sek. Abklingzeit",
          "description" : "Euer Tier stürmt auf sein Ziel zu und macht es 1 Sek. lang bewegungsunfähig. Zusätzlich wird die Nahkampfangriffskraft seines nächsten Angriffs um 25% erhöht."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2116,
        "name" : "Große Ausdauer",
        "icon" : "spell_nature_unyeildingstamina",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 4%."
        }, {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 8%."
        }, {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 12%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2117,
        "name" : "Natürliche Rüstung",
        "icon" : "spell_nature_spiritarmor",
        "x" : 3,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Rüstung Eures Tiers um 5%."
        }, {
          "description" : "Erhöht die Rüstung Eures Tiers um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2126,
        "name" : "Stachelhalsband",
        "icon" : "inv_jewelry_necklace_22",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 3%."
        }, {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 6%."
        }, {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 9%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2160,
        "name" : "Tempo des Ebers",
        "icon" : "ability_hunter_pet_boar",
        "x" : 1,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht das Bewegungstempo Eures Tiers um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2173,
        "name" : "Blut des Rhinozeros",
        "icon" : "spell_shadow_lifedrain",
        "x" : 2,
        "y" : 1,
        "req" : 2116,
        "ranks" : [ {
          "description" : "Erhöht die gesamte erhaltene Heilung Eures Tieres um 20%."
        }, {
          "description" : "Erhöht die gesamte erhaltene Heilung Eures Tieres um 40%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2122,
        "name" : "Elefantenhaut",
        "icon" : "inv_helmet_94",
        "x" : 3,
        "y" : 1,
        "req" : 2117,
        "ranks" : [ {
          "description" : "Erhöht die Rüstung Eures Tiers um 5% und seine Ausweichchance um 1%."
        }, {
          "description" : "Erhöht die Rüstung Eures Tiers um 10% und seine Ausweichchance um 2%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2110,
        "name" : "Herdenschlachtung",
        "icon" : "inv_misc_monsterhorn_06",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 1% erhöht."
        }, {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 2% erhöht."
        }, {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 3% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2123,
        "name" : "Wachhund",
        "icon" : "ability_physical_taunt",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Das Knurren Eures Tiers erzeugt 10% zusätzliche Bedrohung."
        }, {
          "description" : "Das Knurren Eures Tiers erzeugt 20% zusätzliche Bedrohung."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2162,
        "name" : "Löwenherz",
        "icon" : "inv_bannerpvp_02",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "description" : "Verringert die Dauer aller auf Euer Tier wirkenden Betäubungs- und Furchteffekte um 15%."
        }, {
          "description" : "Verringert die Dauer aller auf Euer Tier wirkenden Betäubungs- und Furchteffekte um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2277,
        "name" : "Donnerstampfer",
        "icon" : "ability_golemthunderclap",
        "x" : 3,
        "y" : 2,
        "ranks" : [ {
          "cost" : "20 Fokus",
          "range" : "Nahkampfreichweite",
          "castTime" : "Sofort",
          "cooldown" : "10 Sek. Abklingzeit",
          "description" : "Lässt den Boden mit donnernder Macht erbeben, die allen Gegnern im Umkreis von 8 Metern 187 Naturschaden zufügt. Diese Fähigkeit verursacht eine durchschnittliche Menge zusätzlicher Bedrohung."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2163,
        "name" : "Gewandtheit der Gottesanbeterin",
        "icon" : "inv_misc_ahnqirajtrinket_02",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Verringert die Chance, dass Euer Tier einen kritischen Nahkampftreffer erleidet, um 3%."
        }, {
          "description" : "Verringert die Chance, dass Euer Tier einen kritischen Nahkampftreffer erleidet, um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2161,
        "name" : "Großer Widerstand",
        "icon" : "spell_nature_resistnature",
        "x" : 3,
        "y" : 3,
        "ranks" : [ {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 5% weniger Schaden."
        }, {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 10% weniger Schaden."
        }, {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 15% weniger Schaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2171,
        "name" : "Letztes Gefecht",
        "icon" : "spell_nature_shamanrage",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "castTime" : "Sofort",
          "cooldown" : "6 Min. Abklingzeit",
          "description" : "Eurem Tier werden 20 Sek. lang 30% seiner maximalen Gesundheit gewährt. Wenn der Effekt abklingt, geht die zusätzliche Gesundheit wieder verloren."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2170,
        "name" : "Spott",
        "icon" : "spell_nature_reincarnation",
        "x" : 1,
        "y" : 4,
        "req" : 2123,
        "ranks" : [ {
          "range" : "Nahkampfreichweite",
          "castTime" : "Sofort",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Euer Tier verspottet das Ziel, um 3 Sek. lang von ihm angegriffen zu werden."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2172,
        "name" : "Brüllen der Aufopferung",
        "icon" : "ability_hunter_fervor",
        "x" : 2,
        "y" : 4,
        "req" : 2163,
        "ranks" : [ {
          "range" : "40 Meter Reichweite",
          "castTime" : "Sofort",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Schützt ein befreundetes Ziel davor, kritische Treffer zu erleiden. Gegen das Ziel geführte Angriffe können nicht kritisch treffen, jedoch wird 20% des gesamten erlittenen Schadens auch auf Euer Tier übertragen. Hält 12 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2169,
        "name" : "Einschreiten",
        "icon" : "ability_hunter_pet_turtle",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "cost" : "20 Fokus",
          "range" : "8-25 Meter Reichweite range",
          "castTime" : "Sofort",
          "cooldown" : "30 Sek. Abklingzeit",
          "description" : "Euer Tier läuft mit großer Geschwindigkeit auf ein Gruppenmitglied zu und verringert den Schaden des gegen dieses Gruppenmitglied gerichteten Angriffs um 50% der gesamten Gesundheit des Tieres."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2258,
        "name" : "Silberrücken",
        "icon" : "ability_hunter_pet_gorilla",
        "x" : 1,
        "y" : 5,
        "ranks" : [ {
          "description" : "Das Knurren Eures Tieres heilt es zusätzlich um 1% seiner gesamten Gesundheit."
        }, {
          "description" : "Das Knurren Eures Tieres heilt es zusätzlich um 2% seiner gesamten Gesundheit."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2255,
        "name" : "Wilde Jagd",
        "icon" : "inv_misc_horn_04",
        "x" : 2,
        "y" : 5,
        "req" : 2172,
        "ranks" : [ {
          "description" : "Wenn Euer Tier über 50 Fokus oder mehr verfügt, verursachen die einfachen Angriffe Eures Tiers 60% mehr Schaden, kosten jedoch 50% mehr Fokus."
        }, {
          "description" : "Wenn Euer Tier über 50 Fokus oder mehr verfügt, verursachen die einfachen Angriffe Eures Tiers 120% mehr Schaden, kosten jedoch 100% mehr Fokus."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 1
    }, {
      "name" : "Gerissenheit",
      "icon" : "ability_eyeoftheowl",
      "backgroundFile" : "HunterPetCunning",
      "overlayColor" : "#0099ff",
      "description" : "",
      "treeNo" : 2,
      "roles" : {
        "tank" : false,
        "healer" : false,
        "dps" : true
      },
      "petFamilies" : [ {
        "familyId" : 51,
        "name" : "Affe",
        "icon" : "ability_hunter_aspectofthemonkey",
        "petTypeId" : 2,
        "categoryBit" : 28,
        "minScale" : 0.3,
        "minScaleLevel" : 1,
        "maxScale" : 0.5,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 30,
        "name" : "Drachenfalke",
        "icon" : "ability_hunter_pet_dragonhawk",
        "petTypeId" : 2,
        "categoryBit" : 8,
        "minScale" : 0.35,
        "minScaleLevel" : 1,
        "maxScale" : 0.65,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 31,
        "name" : "Felshetzer",
        "icon" : "ability_hunter_pet_ravager",
        "petTypeId" : 2,
        "categoryBit" : 14,
        "minScale" : 0.65,
        "minScaleLevel" : 1,
        "maxScale" : 0.9,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 24,
        "name" : "Fledermaus",
        "icon" : "ability_hunter_pet_bat",
        "petTypeId" : 2,
        "categoryBit" : 0,
        "minScale" : 0.4,
        "minScaleLevel" : 1,
        "maxScale" : 0.63,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 34,
        "name" : "Netherrochen",
        "icon" : "ability_hunter_pet_netherray",
        "petTypeId" : 2,
        "categoryBit" : 12,
        "minScale" : 0.35,
        "minScaleLevel" : 1,
        "maxScale" : 0.55,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 26,
        "name" : "Raubvogel",
        "icon" : "ability_hunter_pet_owl",
        "petTypeId" : 2,
        "categoryBit" : 2,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.8,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 38,
        "name" : "Schimäre",
        "icon" : "ability_hunter_pet_chimera",
        "petTypeId" : 2,
        "categoryBit" : 24,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.63,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 35,
        "name" : "Schlange",
        "icon" : "spell_nature_guardianward",
        "petTypeId" : 2,
        "categoryBit" : 16,
        "minScale" : 0.6,
        "minScaleLevel" : 1,
        "maxScale" : 0.8,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 59,
        "name" : "Silithid",
        "icon" : "ability_hunter_pet_silithid",
        "petTypeId" : 2,
        "categoryBit" : 63,
        "minScale" : 0.3,
        "minScaleLevel" : 1,
        "maxScale" : 0.4,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 3,
        "name" : "Spinne",
        "icon" : "ability_hunter_pet_spider",
        "petTypeId" : 2,
        "categoryBit" : 17,
        "minScale" : 0.4,
        "minScaleLevel" : 1,
        "maxScale" : 0.6,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 33,
        "name" : "Sporensegler",
        "icon" : "ability_hunter_pet_sporebat",
        "petTypeId" : 2,
        "categoryBit" : 18,
        "minScale" : 0.6,
        "minScaleLevel" : 1,
        "maxScale" : 0.9,
        "maxScaleLevel" : 60
      }, {
        "familyId" : 27,
        "name" : "Windnatter",
        "icon" : "ability_hunter_pet_windserpent",
        "petTypeId" : 2,
        "categoryBit" : 22,
        "minScale" : 0.5,
        "minScaleLevel" : 1,
        "maxScale" : 0.7,
        "maxScaleLevel" : 60
      } ],
      "primarySpells" : [ ],
      "masteries" : [ ],
      "talents" : [ {
        "id" : 2118,
        "name" : "Kobrareflexe",
        "icon" : "ability_hunter_serpentswiftness",
        "x" : 0,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht das Angriffstempo Eures Tiers um 5%."
        }, {
          "description" : "Erhöht das Angriffstempo Eures Tiers um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2201,
        "name" : "Sinkflug",
        "icon" : "spell_shadow_burningspirit",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "cost" : "30 Fokus",
          "castTime" : "Spontanzauber",
          "cooldown" : "32 Sek. Abklingzeit",
          "description" : "Erhöht das Bewegungstempo Eures Tiers 16 Sek. lang um 80%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 21238021,
        "categoryMask1" : 0
      }, {
        "id" : 2119,
        "name" : "Spurt",
        "icon" : "ability_druid_dash",
        "x" : 1,
        "y" : 0,
        "ranks" : [ {
          "cost" : "30 Fokus",
          "castTime" : "Spontanzauber",
          "cooldown" : "32 Sek. Abklingzeit",
          "description" : "Erhöht das Bewegungstempo Eures Tiers 16 Sek. lang um 80%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 268648448,
        "categoryMask1" : -2147483648
      }, {
        "id" : 2120,
        "name" : "Große Ausdauer",
        "icon" : "spell_nature_unyeildingstamina",
        "x" : 2,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 4%."
        }, {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 8%."
        }, {
          "description" : "Erhöht die gesamte Gesundheit Eures Tiers um 12%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2121,
        "name" : "Natürliche Rüstung",
        "icon" : "spell_nature_spiritarmor",
        "x" : 3,
        "y" : 0,
        "ranks" : [ {
          "description" : "Erhöht die Rüstung Eures Tiers um 5%."
        }, {
          "description" : "Erhöht die Rüstung Eures Tiers um 10%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2165,
        "name" : "Tempo des Ebers",
        "icon" : "ability_hunter_pet_boar",
        "x" : 0,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht das Bewegungstempo Eures Tiers um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2208,
        "name" : "Wendigkeit",
        "icon" : "ability_hunter_animalhandler",
        "x" : 1,
        "y" : 1,
        "req" : 2201,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit der Fähigkeit 'Sinkflug' Eures Tiers um 8 Sek."
        }, {
          "description" : "Verringert die Abklingzeit der Fähigkeit 'Sinkflug' Eures Tiers um 16 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 21238021,
        "categoryMask1" : 0
      }, {
        "id" : 2207,
        "name" : "Wendigkeit",
        "icon" : "ability_hunter_animalhandler",
        "x" : 1,
        "y" : 1,
        "req" : 2119,
        "ranks" : [ {
          "description" : "Verringert die Abklingzeit der Fähigkeit 'Spurt' Eures Tiers um 8 Sek."
        }, {
          "description" : "Verringert die Abklingzeit der Fähigkeit 'Spurt' Eures Tiers um 16 Sek."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 268648448,
        "categoryMask1" : -2147483648
      }, {
        "id" : 2182,
        "name" : "Eulenfokus",
        "icon" : "ability_hunter_pet_owl",
        "x" : 2,
        "y" : 1,
        "ranks" : [ {
          "description" : "Euer Tier hat eine Chance von 15%, dass nach dem Nutzen eines einfachen Angriffs der nächste einfache Angriff keinen Fokus kostet, wenn er innerhalb von 8 Sek. genutzt wird."
        }, {
          "description" : "Euer Tier hat eine Chance von 30%, dass nach dem Nutzen eines einfachen Angriffs der nächste einfache Angriff keinen Fokus kostet, wenn er innerhalb von 8 Sek. genutzt wird."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2127,
        "name" : "Stachelhalsband",
        "icon" : "inv_jewelry_necklace_22",
        "x" : 3,
        "y" : 1,
        "ranks" : [ {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 3%."
        }, {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 6%."
        }, {
          "description" : "Erhöht den Schaden der einfachen Angriffe Eures Tiers um 9%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2166,
        "name" : "Herdenschlachtung",
        "icon" : "inv_misc_monsterhorn_06",
        "x" : 0,
        "y" : 2,
        "ranks" : [ {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 1% erhöht."
        }, {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 2% erhöht."
        }, {
          "description" : "Wenn ein einfacher Angriff Eures Tiers einen kritischen Treffer erzielt, wird Euer verursachter Schaden und der Eures Tiers 10 Sek. lang um 3% erhöht."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2167,
        "name" : "Löwenherz",
        "icon" : "inv_bannerpvp_02",
        "x" : 1,
        "y" : 2,
        "ranks" : [ {
          "description" : "Verringert die Dauer aller auf Euer Tier wirkenden Betäubungs- und Furchteffekte um 15%."
        }, {
          "description" : "Verringert die Dauer aller auf Euer Tier wirkenden Betäubungs- und Furchteffekte um 30%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2206,
        "name" : "Aasfresser",
        "icon" : "ability_racial_cannibalize",
        "x" : 2,
        "y" : 2,
        "ranks" : [ {
          "range" : "5 Meter Reichweite",
          "castTime" : "Sofort",
          "cooldown" : "30 Sek. Abklingzeit",
          "description" : "Euer Tier kann durch das Fressen eines Leichnams Gesundheit wiederherstellen. Kann nicht auf die Überreste von mechanischen Einheiten oder Elementaren angewandt werden."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2168,
        "name" : "Großer Widerstand",
        "icon" : "spell_nature_resistnature",
        "x" : 1,
        "y" : 3,
        "ranks" : [ {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 5% weniger Schaden."
        }, {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 10% weniger Schaden."
        }, {
          "description" : "Euer Tier erleidet durch Arkan-, Feuer-, Frost-, Natur- und Schattenzauber 15% weniger Schaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2177,
        "name" : "In die Ecke gedrängt",
        "icon" : "ability_hunter_survivalinstincts",
        "x" : 2,
        "y" : 3,
        "ranks" : [ {
          "description" : "Besitzt Euer Tier 35% Gesundheit oder weniger, verursacht es 25% mehr Schaden und die Chance, dass es kritisch getroffen wird, ist um 30% verringert."
        }, {
          "description" : "Besitzt Euer Tier 35% Gesundheit oder weniger, verursacht es 50% mehr Schaden und die Chance, dass es kritisch getroffen wird, ist um 60% verringert."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2183,
        "name" : "Fressattacke",
        "icon" : "inv_misc_fish_48",
        "x" : 3,
        "y" : 3,
        "req" : 2127,
        "ranks" : [ {
          "description" : "Euer Tier verursacht bei Zielen, die über weniger als 35% Gesundheit verfügen, 8% zusätzlichen Schaden."
        }, {
          "description" : "Euer Tier verursacht bei Zielen, die über weniger als 35% Gesundheit verfügen, 16% zusätzlichen Schaden."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2181,
        "name" : "Marderbiss",
        "icon" : "ability_druid_lacerate",
        "x" : 0,
        "y" : 4,
        "ranks" : [ {
          "range" : "Nahkampfreichweite",
          "castTime" : "Sofort",
          "cooldown" : "10 Sek. Abklingzeit",
          "description" : "Ein wilder Angriff, der 1 Schaden verursacht. Euer Tier kann diesen Angriff ausführen, nachdem es einen kritischen Treffer erzielt hat. Kann nicht geblockt oder pariert und ihm kann nicht ausgewichen werden."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2184,
        "name" : "Brüllen der Erholung",
        "icon" : "ability_druid_mastershapeshifter",
        "x" : 1,
        "y" : 4,
        "ranks" : [ {
          "range" : "40 Meter Reichweite",
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Das kräftigende Brüllen Eures Tiers stellt im Verlauf von 9 Sek. 30 Fokus wieder her."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2175,
        "name" : "Stur wie ein Ochse",
        "icon" : "ability_warrior_bullrush",
        "x" : 2,
        "y" : 4,
        "req" : 2177,
        "ranks" : [ {
          "castTime" : "Spontanzauber",
          "cooldown" : "3 Min. Abklingzeit",
          "description" : "Entfernt alle bewegungseinschränkenden Effekte sowie Effekte, die zum Kontrollverlust über Euer Tier führen. Verringert zudem 12 Sek. lang den erlittenen Schaden Eures Tiers um 20%."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2257,
        "name" : "Gewandtheit der Gottesanbeterin",
        "icon" : "inv_misc_ahnqirajtrinket_02",
        "x" : 3,
        "y" : 4,
        "ranks" : [ {
          "description" : "Verringert die Chance, dass Euer Tier einen kritischen Nahkampftreffer erleidet, um 3%."
        }, {
          "description" : "Verringert die Chance, dass Euer Tier einen kritischen Nahkampftreffer erleidet, um 6%."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2256,
        "name" : "Wilde Jagd",
        "icon" : "inv_misc_horn_04",
        "x" : 0,
        "y" : 5,
        "req" : 2181,
        "ranks" : [ {
          "description" : "Wenn Euer Tier über 50 Fokus oder mehr verfügt, verursachen die einfachen Angriffe Eures Tiers 60% mehr Schaden, kosten jedoch 50% mehr Fokus."
        }, {
          "description" : "Wenn Euer Tier über 50 Fokus oder mehr verfügt, verursachen die einfachen Angriffe Eures Tiers 120% mehr Schaden, kosten jedoch 100% mehr Fokus."
        } ],
        "keyAbility" : false,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      }, {
        "id" : 2278,
        "name" : "Brüllen der Aufopferung",
        "icon" : "ability_hunter_fervor",
        "x" : 3,
        "y" : 5,
        "req" : 2257,
        "ranks" : [ {
          "range" : "40 Meter Reichweite",
          "castTime" : "Sofort",
          "cooldown" : "1 Min. Abklingzeit",
          "description" : "Schützt ein befreundetes Ziel davor, kritische Treffer zu erleiden. Gegen das Ziel geführte Angriffe können nicht kritisch treffen, jedoch wird 20% des gesamten erlittenen Schadens auch auf Euer Tier übertragen. Hält 12 Sek. lang an."
        } ],
        "keyAbility" : true,
        "categoryMask0" : 0,
        "categoryMask1" : 0
      } ],
      "index" : 2
    } ]
  }
}