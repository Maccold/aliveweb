{
    "talentData": {
        "characterClass": {
            "classId": 8,
            "name": "Magier",
            "powerType": "MANA",
            "powerTypeId": 0,
            "powerTypeSlug": "mana"
        },
        "talentTrees": [
            {
                "name": "Arkan",
                "icon": "spell_holy_magicalsentry",
                "backgroundFile": "MageArcane",
                "overlayColor": "#b233ff",
                "description": "Manipuliert arkane Energien und spielt mit dem Wesen von Raum und Zeit selbst.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 74,
                        "name": "Arkanes Feingefühl",
                        "icon": "spell_holy_dispelmagic",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 15% und Eure Arkanzauber verursachen 20% weniger Bedrohung."
                            },
                            {
                                "description": "Verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 30% und Eure Arkanzauber verursachen 40% weniger Bedrohung."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 76,
                        "name": "Arkaner Fokus",
                        "icon": "spell_holy_devotion",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Trefferchance und verringert die Manakosten Eurer Arkanzauber um 1%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance und verringert die Manakosten Eurer Arkanzauber um 2%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance und verringert die Manakosten Eurer Arkanzauber um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 80,
                        "name": "Arkane Stabilität",
                        "icon": "spell_nature_starfall",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Arkanschlag' oder die Zauberzeitverringerung beim Kanalisieren von 'Arkane Geschosse' um 20%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Arkanschlag' oder die Zauberzeitverringerung beim Kanalisieren von 'Arkane Geschosse' um 40%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Arkanschlag' oder die Zauberzeitverringerung beim Kanalisieren von 'Arkane Geschosse' um 60%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Arkanschlag' oder die Zauberzeitverringerung beim Kanalisieren von 'Arkane Geschosse' um 80%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Arkanschlag' oder die Zauberzeitverringerung beim Kanalisieren von 'Arkane Geschosse' um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 85,
                        "name": "Arkane Seelenstärke",
                        "icon": "spell_arcane_arcaneresilience",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Rüstung um einen Betrag, der 50% Eurer Intelligenz entspricht."
                            },
                            {
                                "description": "Erhöht Eure Rüstung um einen Betrag, der 100% Eurer Intelligenz entspricht."
                            },
                            {
                                "description": "Erhöht Eure Rüstung um einen Betrag, der 150% Eurer Intelligenz entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1650,
                        "name": "Magische Vereinnahmung",
                        "icon": "spell_nature_astralrecalgroup",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht alle Widerstandsarten um 0,5 pro Stufe. Durch jeden Zauber, dem Ihr vollständig widersteht, werden 1% Eures gesamten Manas wiederhergestellt. 1 Sek. Abklingzeit."
                            },
                            {
                                "description": "Erhöht alle Widerstandsarten um 1 pro Stufe. Durch jeden Zauber, dem Ihr vollständig widersteht, werden 2% Eures gesamten Manas wiederhergestellt. 1 Sek. Abklingzeit."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 75,
                        "name": "Arkane Konzentration",
                        "icon": "spell_shadow_manaburn",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 2%, in einen Freizauberzustand zu gelangen, nachdem ein beliebiger Schaden zufügender Zauber ein Ziel getroffen hat. Der Freizauberzustand verringert die Manakosten Eures nächsten Schadenszaubers um 100%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 4%, in einen Freizauberzustand zu gelangen, nachdem ein beliebiger Schaden zufügender Zauber ein Ziel getroffen hat. Der Freizauberzustand verringert die Manakosten Eures nächsten Schadenszaubers um 100%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 6%, in einen Freizauberzustand zu gelangen, nachdem ein beliebiger Schaden zufügender Zauber ein Ziel getroffen hat. Der Freizauberzustand verringert die Manakosten Eures nächsten Schadenszaubers um 100%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 8%, in einen Freizauberzustand zu gelangen, nachdem ein beliebiger Schaden zufügender Zauber ein Ziel getroffen hat. Der Freizauberzustand verringert die Manakosten Eures nächsten Schadenszaubers um 100%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 10%, in einen Freizauberzustand zu gelangen, nachdem ein beliebiger Schaden zufügender Zauber ein Ziel getroffen hat. Der Freizauberzustand verringert die Manakosten Eures nächsten Schadenszaubers um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 82,
                        "name": "Einklang der Magie",
                        "icon": "spell_nature_abolishmagic",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Arkanzauber um 3 Meter und die Effekte Eurer Zauber 'Magie verstärken' und 'Magie dämpfen' um 25%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Arkanzauber um 6 Meter und die Effekte Eurer Zauber 'Magie verstärken' und 'Magie dämpfen' um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 81,
                        "name": "Zaubereinschlag",
                        "icon": "spell_nature_wispsplode",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Arkane Explosion', 'Arkanschlag', 'Druckwelle', 'Feuerschlag', 'Versengen', 'Feuerball', 'Eislanze' und 'Kältekegel' um zusätzliche 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Arkane Explosion', 'Arkanschlag', 'Druckwelle', 'Feuerschlag', 'Versengen', 'Feuerball', 'Eislanze' und 'Kältekegel' um zusätzliche 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Arkane Explosion', 'Arkanschlag', 'Druckwelle', 'Feuerschlag', 'Versengen', 'Feuerball', 'Eislanze' und 'Kältekegel' um zusätzliche 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1845,
                        "name": "Geisteswissenschaften",
                        "icon": "ability_mage_studentofthemind",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 4%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 7%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2211,
                        "name": "Magie fokussieren",
                        "icon": "spell_arcane_studentofmagic",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "cost": "6% des Grundmanas",
                                "description": "Erhöht die kritische Zaubertrefferchance des Ziels um 3%. Erzielt das Ziel einen kritischen Treffer, wird die kritische Zaubertrefferchance des Zaubernden 10 Sek. lang um 3% erhöht. Kann nur auf Andere gewirkt werden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 83,
                        "name": "Arkane Schilde",
                        "icon": "spell_shadow_detectlesserinvisibility",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert bei aktiviertem Manaschild den Manaverlust für jeden absorbierten Schadenspunkt um 17% und erhöht die von der magischen Rüstung gewährten Widerstände um 25%."
                            },
                            {
                                "description": "Verringert bei aktiviertem Manaschild den Manaverlust für jeden absorbierten Schadenspunkt um 33% und erhöht die von der magischen Rüstung gewährten Widerstände um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 88,
                        "name": "Verbesserter Gegenzauber",
                        "icon": "spell_frost_iceshock",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Euer 'Gegenzauber' bringt das Ziel auch 2 Sek. lang zum Schweigen."
                            },
                            {
                                "description": "Euer 'Gegenzauber' bringt das Ziel auch 4 Sek. lang zum Schweigen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1142,
                        "name": "Arkane Meditation",
                        "icon": "spell_shadow_siphonmana",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Ermöglicht, dass 17% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            },
                            {
                                "description": "Ermöglicht, dass 33% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            },
                            {
                                "description": "Ermöglicht, dass 50% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2222,
                        "name": "Die Schwachen quälen",
                        "icon": "ability_mage_tormentoftheweak",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Frostblitz', 'Feuerball', 'Frostfeuerblitz', 'Pyroschlag', 'Arkane Geschosse', 'Arkanschlag' und 'Arkanbeschuss' fügen verlangsamten oder bewegungseingeschränkten Zielen 4% mehr Schaden zu."
                            },
                            {
                                "description": "Eure Zauber 'Frostblitz', 'Feuerball', 'Frostfeuerblitz', 'Pyroschlag', 'Arkane Geschosse', 'Arkanschlag' und 'Arkanbeschuss' fügen verlangsamten oder bewegungseingeschränkten Zielen 8% mehr Schaden zu."
                            },
                            {
                                "description": "Eure Zauber 'Frostblitz', 'Feuerball', 'Frostfeuerblitz', 'Pyroschlag', 'Arkane Geschosse', 'Arkanschlag' und 'Arkanbeschuss' fügen verlangsamten oder bewegungseingeschränkten Zielen 12% mehr Schaden zu."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1724,
                        "name": "Verbessertes Blinzeln",
                        "icon": "spell_arcane_blink",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten von 'Blinzeln' um 25% und verringert nach dem Blinzeln 4 Sek. lang die Chance, dass Ihr von Zaubern und Angriffen getroffen werdet, um 15%."
                            },
                            {
                                "description": "Verringert die Manakosten von 'Blinzeln' um 50% und verringert nach dem Blinzeln 4 Sek. lang die Chance, dass Ihr von Zaubern und Angriffen getroffen werdet, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 86,
                        "name": "Geistesgegenwart",
                        "icon": "spell_nature_enchantarmor",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Bei Aktivierung wird Euer nächster Magierzauber mit einer Zauberzeit von weniger als 10 Sekunden ein Sofortzauber."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 77,
                        "name": "Arkaner Geist",
                        "icon": "spell_shadow_charm",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 3%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 6%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 9%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 12%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1726,
                        "name": "Prismatischer Mantel",
                        "icon": "spell_arcane_prismaticcloak",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 2% und die Verblassungszeit Eures Zaubers 'Unsichtbarkeit' um 1 Sek."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 4% und die Verblassungszeit Eures Zaubers 'Unsichtbarkeit' um 2 Sek."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 6% und verringert die Dauer des Verblassens Eures Zaubers 'Unsichtbarkeit' um 3 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 421,
                        "name": "Arkane Instabilität",
                        "icon": "spell_shadow_teleport",
                        "x": 1,
                        "y": 5,
                        "req": 86,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber und Eure kritische Trefferchance um 1%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber und Eure kritische Trefferchance um 2%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber und Eure kritische Trefferchance um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1725,
                        "name": "Arkane Kraft",
                        "icon": "spell_arcane_arcanepotency",
                        "x": 2,
                        "y": 5,
                        "req": 86,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Zaubertrefferchance Eures nächsten Zaubers, nachdem der Freizauberzustand oder 'Geistesgegenwart' aktiv wurde, um 15%."
                            },
                            {
                                "description": "Erhöht die kritische Zaubertrefferchance Eures nächsten Zaubers, nachdem der Freizauberzustand oder 'Geistesgegenwart' aktiv wurde, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1727,
                        "name": "Arkane Ermächtigung",
                        "icon": "spell_nature_starfall",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Arkane Geschosse' um einen Wert, der 15% Eurer Zaubermacht entspricht und den Schaden Eures Zaubers 'Arkanschlag' um 3% Eurer Zaubermacht. Zudem wird der verursachte Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 1% erhöht."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Arkane Geschosse' um einen Wert, der 30% Eurer Zaubermacht entspricht und den Schaden Eures Zaubers 'Arkanschlag' um 6% Eurer Zaubermacht. Zudem wird der verursachte Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 2% erhöht."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Arkane Geschosse' um einen Wert, der 45% Eurer Zaubermacht entspricht und den Schaden Eures Zaubers 'Arkanschlag' um 9% Eurer Zaubermacht. Zudem wird der verursachte Schaden aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 3% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 87,
                        "name": "Arkane Macht",
                        "icon": "spell_nature_lightning",
                        "x": 1,
                        "y": 6,
                        "req": 421,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Bei Aktivierung fügen Eure Zauber 20% mehr Schaden zu, jedoch kostet das Wirken 20% mehr Mana. Dieser Effekt hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1844,
                        "name": "Absorption des Beschwörers",
                        "icon": "ability_mage_incantersabsorbtion",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Wenn ein Effekt Eurer Fähigkeiten 'Manaschild', 'Frostzauberschutz', 'Feuerzauberschutz' oder 'Eisbarriere' Schaden absorbiert, wird Euer Zauberschaden 10 Sek. lang um 5% des absorbierten Betrags erhöht."
                            },
                            {
                                "description": "Wenn ein Effekt Eurer Fähigkeiten 'Manaschild', 'Frostzauberschutz', 'Feuerzauberschutz' oder 'Eisbarriere' Schaden absorbiert, wird Euer Zauberschaden 10 Sek. lang um 10% des absorbierten Betrags erhöht."
                            },
                            {
                                "description": "Wenn ein Effekt Eurer Fähigkeiten 'Manaschild', 'Frostzauberschutz', 'Feuerzauberschutz' oder 'Eisbarriere' Schaden absorbiert, wird Euer Zauberschaden 10 Sek. lang um 15% des absorbierten Betrags erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1843,
                        "name": "Arkaner Fluss",
                        "icon": "ability_mage_potentspirit",
                        "x": 1,
                        "y": 7,
                        "req": 87,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Geistesgegenwart', 'Arkane Macht' und 'Unsichtbarkeit' um 15% und die Abklingzeit des Zaubers 'Hervorrufung' um 1 Min."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Geistesgegenwart', 'Arkane Macht' und 'Unsichtbarkeit' um 30% und die Abklingzeit des Zaubers 'Hervorrufung' um 2 Min."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1728,
                        "name": "Geistesbeherrschung",
                        "icon": "spell_arcane_mindmastery",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Zaubermacht wird um 3% Eurer gesamten Intelligenz erhöht."
                            },
                            {
                                "description": "Eure Zaubermacht wird um 6% Eurer gesamten Intelligenz erhöht."
                            },
                            {
                                "description": "Eure Zaubermacht wird um 9% Eurer gesamten Intelligenz erhöht."
                            },
                            {
                                "description": "Eure Zaubermacht wird um 12% Eurer gesamten Intelligenz erhöht."
                            },
                            {
                                "description": "Eure Zaubermacht wird um 15% Eurer gesamten Intelligenz erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1729,
                        "name": "Verlangsamen",
                        "icon": "spell_nature_slow",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "cost": "12% des Grundmanas",
                                "description": "Verlangsamt das Bewegungstempo des Ziels um 60%, erhöht die Zeit zwischen Distanzangriffen um 60% und erhöht die Zauberzeit um 30%. Hält 15 Sek. lang an. 'Verlangsamen' kann immer nur auf ein Ziel wirken."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2209,
                        "name": "Geschosssalve",
                        "icon": "ability_mage_missilebarrage",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verleiht Eurem Zauber 'Arkanschlag' eine Chance von 8% sowie Euren Zaubern 'Arkanbeschuss', 'Feuerball', 'Frostblitz' und 'Frostfeuerblitz' eine Chance von 4%, die Kanalisierungsdauer Eures nächsten Wirkens von 'Arkane Geschosse' um 2.5 Sek. zu verringern, die Manakosten um 100% zu verringern und alle 0,5 Sek. Geschosse zu feuern."
                            },
                            {
                                "description": "Verleiht Eurem Zauber 'Arkanschlag' eine Chance von 16% sowie Euren Zaubern 'Arkanbeschuss', 'Feuerball', 'Frostblitz' und 'Frostfeuerblitz' eine Chance von 8%, die Kanalisierungsdauer Eures nächsten Wirkens von 'Arkane Geschosse' um 2.5 Sek. zu verringern, die Manakosten um 100% zu verringern und alle 0,5 Sek. Geschosse zu feuern."
                            },
                            {
                                "description": "Verleiht Eurem Zauber 'Arkanschlag' eine Chance von 24% sowie Euren Zaubern 'Arkanbeschuss', 'Feuerball', 'Frostblitz' und 'Frostfeuerblitz' eine Chance von 12%, die Kanalisierungsdauer Eures nächsten Wirkens von 'Arkane Geschosse' um 2.5 Sek. zu verringern, die Manakosten um 100% zu verringern und alle 0,5 Sek. Geschosse zu feuern."
                            },
                            {
                                "description": "Verleiht Eurem Zauber 'Arkanschlag' eine Chance von 32% sowie Euren Zaubern 'Arkanbeschuss', 'Feuerball', 'Frostblitz' und 'Frostfeuerblitz' eine Chance von 16%, die Kanalisierungsdauer Eures nächsten Wirkens von 'Arkane Geschosse' um 2.5 Sek. zu verringern, die Manakosten um 100% zu verringern und alle 0,5 Sek. Geschosse zu feuern."
                            },
                            {
                                "description": "Verleiht Eurem Zauber 'Arkanschlag' eine Chance von 40% sowie Euren Zaubern 'Arkanbeschuss', 'Feuerball', 'Frostblitz' und 'Frostfeuerblitz' eine Chance von 20%, die Kanalisierungsdauer Eures nächsten Wirkens von 'Arkane Geschosse' um 2.5 Sek. zu verringern, die Manakosten um 100% zu verringern und alle 0,5 Sek. Geschosse zu feuern."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1846,
                        "name": "Präsenz des Netherwinds",
                        "icon": "ability_mage_netherwindpresence",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Zaubertempo um 2%."
                            },
                            {
                                "description": "Erhöht Euer Zaubertempo um 4%."
                            },
                            {
                                "description": "Erhöht Euer Zaubertempo um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1826,
                        "name": "Zauberkraft",
                        "icon": "spell_arcane_arcanetorrent",
                        "x": 2,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den Schadensbonus bei kritischen Treffern aller Zauber um 25%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus bei kritischen Treffern aller Zauber um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1847,
                        "name": "Arkanbeschuss",
                        "icon": "ability_mage_arcanebarrage",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Schleudert mehrere Geschosse auf das feindliche Ziel und verursacht 386 bis 470 Arkanschaden.",
                                "cost": "18% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Feuer",
                "icon": "spell_fire_firebolt02",
                "backgroundFile": "MageFire",
                "overlayColor": "#ff7f00",
                "description": "Setzt seine Feinde mit Feuerbällen und Drachenatem außer Gefecht.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 27,
                        "name": "Verbesserter Feuerschlag",
                        "icon": "spell_fire_fireball",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Feuerschlag' um 1 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Feuerschlag' um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1141,
                        "name": "Verbrennung",
                        "icon": "spell_fire_flameshock",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Feuerschlag', 'Versengen', 'Arkanschlag' und 'Kältekegel' um 2%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Feuerschlag', 'Versengen', 'Arkanschlag' und 'Kältekegel' um 4%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Feuerschlag', 'Versengen', 'Arkanschlag' und 'Kältekegel' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 26,
                        "name": "Verbesserter Feuerball",
                        "icon": "spell_fire_flamebolt",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Feuerball' um 0.1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Feuerball' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Feuerball' um 0.3 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Feuerball' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Feuerball' um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 34,
                        "name": "Entzünden",
                        "icon": "spell_fire_incinerate",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Eure kritischen Treffer mit Feuerzaubern lassen das Ziel 4 Sek. lang brennen, es erleidet währenddessen zusätzlich 8% des Schadens des Zaubers."
                            },
                            {
                                "description": "Eure kritischen Treffer mit Feuerzaubern lassen das Ziel 4 Sek. lang brennen, es erleidet währenddessen zusätzlich 16% des Schadens des Zaubers."
                            },
                            {
                                "description": "Eure kritischen Treffer mit Feuerzaubern lassen das Ziel 4 Sek. lang brennen, es erleidet währenddessen zusätzlich 24% des Schadens des Zaubers."
                            },
                            {
                                "description": "Eure kritischen Treffer mit Feuerzaubern lassen das Ziel 4 Sek. lang brennen, es erleidet währenddessen zusätzlich 32% des Schadens des Zaubers."
                            },
                            {
                                "description": "Eure kritischen Treffer mit Feuerzaubern lassen das Ziel 4 Sek. lang brennen, es erleidet währenddessen zusätzlich 40% des Schadens des Zaubers."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2212,
                        "name": "Brennende Entschlossenheit",
                        "icon": "spell_fire_totemofwrath",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Werdet Ihr beim Zauberwirken unterbrochen oder zum Schweigen gebracht, habt Ihr eine Chance von 50%, 20 Sek. lang gegen Stille- oder Unterbrechungseffekte immun zu werden."
                            },
                            {
                                "description": "Werdet Ihr beim Zauberwirken unterbrochen oder zum Schweigen gebracht, habt Ihr eine Chance von 100%, 20 Sek. lang gegen Stille- oder Unterbrechungseffekte immun zu werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 31,
                        "name": "Welt in Flammen",
                        "icon": "ability_mage_worldinflames",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Flammenstoß', 'Pyroschlag', 'Druckwelle', 'Drachenodem', 'Lebende Bombe', 'Blizzard' und 'Arkane Explosion' um 2%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Flammenstoß', 'Pyroschlag', 'Druckwelle', 'Drachenodem', 'Lebende Bombe', 'Blizzard' und 'Arkane Explosion' um 4%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Flammenstoß', 'Pyroschlag', 'Druckwelle', 'Drachenodem', 'Lebende Bombe', 'Blizzard' und 'Arkane Explosion' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 28,
                        "name": "Flammenwerfen",
                        "icon": "spell_fire_flare",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Feuerzauber, mit Ausnahme von 'Frostfeuerblitz', um 3 Meter."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Feuerzauber, mit Ausnahme von 'Frostfeuerblitz', um 6 Meter."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 30,
                        "name": "Einschlag",
                        "icon": "spell_fire_meteorstorm",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Gewährt Euren Schaden verursachenden Zaubern eine Chance von 4%, dass das nächste Wirken von 'Feuerschlag' das Ziel 2 Sek. lang betäubt."
                            },
                            {
                                "description": "Gewährt Euren Schaden verursachenden Zaubern eine Chance von 7%, dass das nächste Wirken von 'Feuerschlag' das Ziel 2 Sek. lang betäubt."
                            },
                            {
                                "description": "Gewährt Euren Schaden verursachenden Zaubern eine Chance von 10%, dass das nächste Wirken von 'Feuerschlag' das Ziel 2 Sek. lang betäubt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 29,
                        "name": "Pyroschlag",
                        "icon": "spell_fire_fireball02",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "5 Sek. Zauberzeit",
                                "range": "35 Meter Reichweite",
                                "cost": "22% des Grundmanas",
                                "description": "Schleudert einen immensen feurigen Felsen, der 141 bis 187 Feuerschaden sowie zusätzlich 12 Sek. lang insgesamt 56 Feuerschaden verursacht."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 23,
                        "name": "Brennende Seele",
                        "icon": "spell_fire_fire",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Feuerzaubern um 35% und die von Euren Feuerzaubern verursachte Bedrohung um 10%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Feuerzaubern um 70% und die von Euren Feuerzaubern verursachte Bedrohung um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 25,
                        "name": "Verbessertes Versengen",
                        "icon": "spell_fire_soulburn",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Versengen', 'Feuerball' und 'Frostfeuerblitz' um 1%. Zudem hat Euer Zauber 'Versengen' eine Chance von 33%, dass sein Ziel für Zauberschaden verwundbar wird, wodurch die kritische Zaubertrefferchance gegen dieses Ziel 30 Sek. lang um 5% erhöht wird."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Versengen', 'Feuerball' und 'Frostfeuerblitz' um 2%. Zudem hat Euer Zauber 'Versengen' eine Chance von 66%, dass sein Ziel für Zauberschaden verwundbar wird, wodurch die kritische Zaubertrefferchance gegen dieses Ziel 30 Sek. lang um 5% erhöht wird."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Versengen', 'Feuerball' und 'Frostfeuerblitz' um 3%. Zudem hat Euer Zauber 'Versengen' eine Chance von 100%, dass sein Ziel für Zauberschaden verwundbar wird, wodurch die kritische Zaubertrefferchance gegen dieses Ziel 30 Sek. lang um 5% erhöht wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 24,
                        "name": "Glühende Schilde",
                        "icon": "spell_fire_firearmor",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verleiht Euren Zaubern 'Feuerzauberschutz' und 'Frostzauberschutz' eine Chance von 15%, die jeweils betroffene Zauberart zu reflektieren. Zusätzlich hat Eure 'Glühende Rüstung' eine Chance von 50%, auch Distanzangriffe und Schadenszauber zu betreffen."
                            },
                            {
                                "description": "Verleiht Euren Zaubern 'Feuerzauberschutz' und 'Frostzauberschutz' eine Chance von 30%, die jeweils betroffene Zauberart zu reflektieren. Zusätzlich hat 'Glühende Rüstung' eine Chance von 100%, auch Distanzangriffe und Offensivzauber zu betreffen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1639,
                        "name": "Meister der Elemente",
                        "icon": "spell_fire_masterofelements",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Durch kritische Zaubertreffer erhaltet Ihr 10% der Grundmanakosten des Zaubers zurück."
                            },
                            {
                                "description": "Durch kritische Zaubertreffer erhaltet Ihr 20% der Grundmanakosten des Zaubers zurück."
                            },
                            {
                                "description": "Durch kritische Zaubertreffer erhaltet Ihr 30% der Grundmanakosten des Zaubers zurück."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1730,
                        "name": "Spiel mit dem Feuer",
                        "icon": "spell_fire_playingwithfire",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht jeglichen verursachten Zauberschaden um 1% und jeglichen erlittenen Zauberschaden um 1%."
                            },
                            {
                                "description": "Erhöht jeglichen verursachten Zauberschaden um 2% und jeglichen erlittenen Zauberschaden um 2%."
                            },
                            {
                                "description": "Erhöht jeglichen verursachten Zauberschaden um 3% und jeglichen erlittenen Zauberschaden um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 33,
                        "name": "Kritische Masse",
                        "icon": "spell_nature_wispheal",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Feuerzauber um 2%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Feuerzauber um 4%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Feuerzauber um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 32,
                        "name": "Druckwelle",
                        "icon": "spell_holy_excorcism_02",
                        "x": 2,
                        "y": 4,
                        "req": 29,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "cost": "7% des Grundmanas",
                                "description": "Eine Flammenwelle geht vom Zaubernden aus. Alle Feinde, die sich im Radius der Flammen befinden, erleiden 154 bis 186 Feuerschaden, werden zurückgestoßen und sind 6 Sek. lang benommen."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1731,
                        "name": "Heiße Sohlen",
                        "icon": "spell_fire_burningspeed",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Verleiht Euch eine Chance von 5%, dass nach einem erlittenen Nahkampf- oder Distanztreffer Euer Bewegungstempo um 50% erhöht wird und alle bewegungseinschränkenden Effekte entfernt werden. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Verleiht Euch eine Chance von 10%, dass nach einem erlittenen Nahkampf- oder Distanztreffer Euer Bewegungstempo um 50% erhöht wird und alle bewegungseinschränkenden Effekte entfernt werden. Hält 8 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 35,
                        "name": "Feuermacht",
                        "icon": "spell_fire_immolation",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Feuerzauber um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Feuerzauber um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Feuerzauber um 6%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Feuerzauber um 8%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Feuerzauber um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1733,
                        "name": "Brandstifter",
                        "icon": "spell_fire_burnout",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance um 1% und gestattet, dass 17% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance um 2% und gestattet, dass 33% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance um 3% und gestattet, dass 50% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 36,
                        "name": "Einäschern",
                        "icon": "spell_fire_sealoffire",
                        "x": 1,
                        "y": 6,
                        "req": 33,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Wenn aktiviert, wird der kritische Schadensbonus Eurer Feuerzauber um 50% erhöht und jeder Feuerschadenszauber, den Ihr wirkt, erhöht die kritische Trefferchance Eurer Feuerschadenszauber um 10%. Dieser Effekt hält an, bis Ihr 3 unregelmäßige kritische Treffer mit Feuerzaubern erzielt habt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1732,
                        "name": "Glühender Zorn",
                        "icon": "spell_fire_moltenblood",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden aller Zauber gegen Ziele, die über weniger als 35% Gesundheit verfügen um 6%."
                            },
                            {
                                "description": "Erhöht den Schaden aller Zauber gegen Ziele, die über weniger als 35% Gesundheit verfügen um 12%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1848,
                        "name": "Feurige Rache",
                        "icon": "ability_mage_fierypayback",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Besitzt Ihr weniger als 35% Gesundheit, wird jeglicher erlittene Schaden um 10% verringert. Zusätzlich wird die Zauberzeit von 'Pyroschlag' um 1.75 Sek. verringert, während die Abklingzeit um 2.5 Sek. erhöht wird. Zudem besteht eine Chance von 5%, dass bei gegen Euch geführten Nahkampf- oder Distanzangriffen die Waffenhand- und Distanzwaffe des Angreifers entwaffnet wird."
                            },
                            {
                                "description": "Besitzt Ihr weniger als 35% Gesundheit, wird jeglicher erlittene Schaden um 20% verringert. Zusätzlich wird die Zauberzeit von 'Pyroschlag' um 3.5 Sek. verringert, während die Abklingzeit um 5 Sek. erhöht wird. Zudem besteht eine Chance von 10%, dass bei gegen Euch geführten Nahkampf- oder Distanzangriffen die Waffenhand- und Distanzwaffe des Angreifers entwaffnet wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1734,
                        "name": "Machtvolles Feuer",
                        "icon": "spell_fire_flamebolt",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Feuerball', 'Frostfeuerblitz' und 'Pyroschlag' um einen Wert, der 5% Eurer Zaubermacht entspricht. Zudem besteht jedes Mal, wenn Euer Talent 'Entzünden' Schaden verursacht, eine Chance von 33%, dass Ihr 2% Eures Grundmanas gewinnt."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Feuerball', 'Frostfeuerblitz' und 'Pyroschlag' um einen Wert, der 10% Eurer Zaubermacht entspricht. Zudem besteht jedes Mal, wenn Euer Talent 'Entzünden' Schaden verursacht, eine Chance von 67%, dass Ihr 2% Eures Grundmanas gewinnt."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Feuerball', 'Frostfeuerblitz' und 'Pyroschlag' um einen Wert, der 15% Eurer Zaubermacht entspricht. Zudem besteht jedes Mal, wenn Euer Talent 'Entzünden' Schaden verursacht, eine Chance von 100%, dass Ihr 2% Eures Grundmanas gewinnt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1849,
                        "name": "Feuerteufel",
                        "icon": "ability_mage_firestarter",
                        "x": 0,
                        "y": 8,
                        "req": 1735,
                        "ranks": [
                            {
                                "description": "Wenn Eure Zauber 'Druckwelle' und 'Drachenodem' Schaden verursachen, besteht eine Chance von 50%, dass Euer nächstes Wirken von 'Flammenstoß' zu einem Spontanzauber wird und kein Mana kostet. Hält 10 Sek. lang an."
                            },
                            {
                                "description": "Wenn Eure Zauber 'Druckwelle' und 'Drachenodem' Schaden verursachen, besteht eine Chance von 100%, dass Euer nächstes Wirken von 'Flammenstoß' zu einem Spontanzauber wird und kein Mana kostet. Hält 10 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1735,
                        "name": "Drachenodem",
                        "icon": "inv_misc_head_dragon_01",
                        "x": 1,
                        "y": 8,
                        "req": 36,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "cost": "7% des Grundmanas",
                                "description": "Zielen in einem kegelförmigen Bereich vor dem Zaubernden wird 370 bis 430 Feuerschaden zugefügt und sie werden 5 Sek. lang desorientiert. Jeder Angriff, der direkten Schaden zufügt, lässt das Ziel wieder zu sich kommen. Stoppt Euren Angriff bei Benutzung."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1850,
                        "name": "Kampfeshitze",
                        "icon": "ability_mage_hotstreak",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Jedes Mal, wenn Ihr mit 'Feuerball', 'Feuerschlag', 'Versengen', 'Lebende Bombe' oder 'Frostfeuerblitz' 2 unregelmäßige kritische Zaubertreffer in Folge erzielt, besteht eine Chance von 33%, dass Euer nächstes Wirken von 'Pyroschlag' innerhalb von 10 Sek. zu einem Spontanzauber wird."
                            },
                            {
                                "description": "Jedes Mal, wenn Ihr mit 'Feuerball', 'Feuerschlag', 'Versengen', 'Lebende Bombe' oder 'Frostfeuerblitz' 2 unregelmäßige kritische Zaubertreffer in Folge erzielt, besteht eine Chance von 66%, dass Euer nächstes Wirken von 'Pyroschlag' innerhalb von 10 Sek. zu einem Spontanzauber wird."
                            },
                            {
                                "description": "Jedes Mal, wenn Ihr mit 'Feuerball', 'Feuerschlag', 'Versengen', 'Lebende Bombe' oder 'Frostfeuerblitz' 2 unregelmäßige kritische Zaubertreffer in Folge erzielt, besteht eine Chance von 100%, dass Euer nächstes Wirken von 'Pyroschlag' innerhalb von 10 Sek. zu einem Spontanzauber wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1851,
                        "name": "Ausgebrannt",
                        "icon": "ability_mage_burnout",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Zauber um 10%, Eure unregelmäßigen kritischen Zaubertreffer kosten jedoch zusätzliche 1% der Manakosten des jeweiligen Zaubers."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Zauber um 20%, Eure unregelmäßigen kritischen Zaubertreffer kosten jedoch zusätzliche 2% der Manakosten des jeweiligen Zaubers."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Zauber um 30%, Eure unregelmäßigen kritischen Zaubertreffer kosten jedoch zusätzliche 3% der Manakosten des jeweiligen Zaubers."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Zauber um 40%, Eure unregelmäßigen kritischen Zaubertreffer kosten jedoch zusätzliche 4% der Manakosten des jeweiligen Zaubers."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Zauber um 50%, Eure unregelmäßigen kritischen Zaubertreffer kosten jedoch zusätzliche 5% der Manakosten des jeweiligen Zaubers."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1852,
                        "name": "Lebende Bombe",
                        "icon": "ability_mage_livingbomb",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "35 Meter Reichweite",
                                "cost": "22% des Grundmanas",
                                "description": "Das Ziel wird zu einer lebenden Bombe und erleidet im Verlauf von 12 Sek. 612 Feuerschaden. Nach Ablauf von 12 Sek., oder wenn der Zauber gebannt wird, explodiert das Ziel und fügt allen Feinden im Umkreis von 10 Metern 306 Feuerschaden zu."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Frost",
                "icon": "spell_frost_frostbolt02",
                "backgroundFile": "MageFrost",
                "overlayColor": "#4c99ff",
                "description": "Friert seine Feinde fest und zerbricht sie durch Frostmagie.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 38,
                        "name": "Erfrierung",
                        "icon": "spell_frost_frostarmor",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Gewährt Euren Kälteeffekten eine Chance von 5%, das Ziel 5 Sek. lang einzufrieren."
                            },
                            {
                                "description": "Gewährt Euren Kälteeffekten eine Chance von 10%, das Ziel 5 Sek. lang einzufrieren."
                            },
                            {
                                "description": "Gewährt Euren Kälteeffekten eine Chance von 15%, das Ziel 5 Sek. lang einzufrieren."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 37,
                        "name": "Verbesserter Frostblitz",
                        "icon": "spell_frost_frostbolt02",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Frostblitz' um 0.1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Frostblitz' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Frostblitz' um 0.3 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Frostblitz' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Frostblitz' um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 62,
                        "name": "Eisschollen",
                        "icon": "spell_frost_icefloes",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Frostnova', 'Kältekegel', 'Eisige Adern' und 'Eisblock' um 7%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Frostnova', 'Kältekegel', 'Eisige Adern' und 'Eisblock' um 14%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Frostnova', 'Kältekegel', 'Eisige Adern' und 'Eisblock' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 73,
                        "name": "Eissplitter",
                        "icon": "spell_frost_iceshard",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schadensbonus Eurer kritischen Treffer mit Frostzaubern um 33%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus Eurer kritischen Treffer mit Frostzaubern um 66%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus Eurer kritischen Treffer mit Frostzaubern um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 70,
                        "name": "Schutz des Frostes",
                        "icon": "spell_frost_frostward",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die von Euren Zaubern 'Frostrüstung' und 'Eisrüstung' verliehene Rüstung und Widerstände um 25%. Verleiht Euren Zaubern 'Frostzauberschutz' und 'Feuerzauberschutz' eine Chance von 15%, die jeweils geschützte Zauberart zu negieren und Mana in Höhe des absorbierten Schadens zu regenerieren."
                            },
                            {
                                "description": "Erhöht die von Euren Zaubern 'Frostrüstung' und 'Eisrüstung' verliehene Rüstung und Widerstände um 50%. Verleiht Euren Zaubern 'Frostzauberschutz' und 'Feuerzauberschutz' eine Chance von 30%, die jeweils geschützte Zauberart zu negieren und Mana in Höhe des absorbierten Schadens zu regenerieren."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1649,
                        "name": "Präzision",
                        "icon": "spell_ice_magicdamage",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten und erhöht Eure Zaubertrefferchance um 1%."
                            },
                            {
                                "description": "Verringert die Manakosten und erhöht Eure Zaubertrefferchance um 2%."
                            },
                            {
                                "description": "Verringert die Manakosten und erhöht Eure Zaubertrefferchance um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 65,
                        "name": "Dauerfrost",
                        "icon": "spell_frost_wisp",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Dauer Eurer Kälteeffekte um 1 Sekunde, verlangsamt das Tempo des Ziels zusätzlich um 4% und verringert die erhaltene Heilung des Ziels um 7%."
                            },
                            {
                                "description": "Erhöht die Dauer Eurer Kälteeffekte um 2 Sekunden, verlangsamt das Tempo des Ziels zusätzlich um 7% und verringert die erhaltene Heilung des Ziels um 13%."
                            },
                            {
                                "description": "Erhöht die Dauer Eurer Kälteeffekte um 3 Sekunden, verlangsamt das Tempo des Ziels zusätzlich um 10% und verringert die erhaltene Heilung des Ziels um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 61,
                        "name": "Stechendes Eis",
                        "icon": "spell_frost_frostbolt",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Frostzauber um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Frostzauber um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Frostzauber um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 69,
                        "name": "Eisige Adern",
                        "icon": "spell_frost_coldhearted",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "cost": "3% des Grundmanas",
                                "description": "Beschleunigt Euer Wirken von Zaubern, erhöht das Zaubertempo um 20% und verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Zaubern um 100%. Hält 20 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 63,
                        "name": "Verbesserter Blizzard",
                        "icon": "spell_frost_icestorm",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Fügt Eurem Zauber 'Blizzard' einen Kälteeffekt hinzu. Dieser Effekt verringert das Bewegungstempo des Ziels um 25%. Hält 1.50 Sek. lang an."
                            },
                            {
                                "description": "Fügt Eurem Zauber 'Blizzard' einen Kälteeffekt hinzu. Dieser Effekt verringert das Bewegungstempo des Ziels um 40%. Hält 1.50 Sek. lang an."
                            },
                            {
                                "description": "Fügt Eurem Zauber 'Blizzard' einen Kälteeffekt hinzu. Dieser Effekt verringert das Bewegungstempo des Ziels um 50%. Hält 1.50 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 741,
                        "name": "Arktische Reichweite",
                        "icon": "spell_shadow_darkritual",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Frostblitz', 'Eislanze', 'Tieffrieren' und 'Blizzard' und den Radius Eurer Zauber 'Frostnova' und 'Kältekegel' um 10%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Frostblitz', 'Eislanze', 'Tieffrieren' und 'Blizzard' und den Radius Eurer Zauber 'Frostnova' und 'Kältekegel' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 66,
                        "name": "Frostkanalisierung",
                        "icon": "spell_frost_stun",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten aller Zauber um 4% und verringert die von Euren Frostzaubern verursachte Bedrohung um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Zauber um 7% und verringert die von Euren Frostzaubern verursachte Bedrohung um 7%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Zauber um 10% und verringert die von Euren Frostzaubern verursachte Bedrohung um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 67,
                        "name": "Zertrümmern",
                        "icon": "spell_frost_frostshock",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance aller Eurer Zauber gegen gefrorene Ziele um 17%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Eurer Zauber gegen gefrorene Ziele um 34%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Eurer Zauber gegen gefrorene Ziele um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 72,
                        "name": "Kälteeinbruch",
                        "icon": "spell_frost_wizardmark",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "8 Min. Abklingzeit",
                                "description": "Bei Aktivierung schließt dieser Zauber die Abklingzeit all Eurer gerade gewirkten Frostzauber ab."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 64,
                        "name": "Verbesserter Kältekegel",
                        "icon": "spell_frost_glacier",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden, den Euer Zauber 'Kältekegel' zufügt, um 15%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Euer Zauber 'Kältekegel' zufügt, um 25%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Euer Zauber 'Kältekegel' zufügt, um 35%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1736,
                        "name": "Gefrorener Kern",
                        "icon": "spell_frost_frozencore",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen durch Zauber erlittenen Schaden um 2%."
                            },
                            {
                                "description": "Verringert jeglichen durch Zauber erlittenen Schaden um 4%."
                            },
                            {
                                "description": "Verringert jeglichen durch Zauber erlittenen Schaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1737,
                        "name": "Eiseskälte",
                        "icon": "ability_mage_coldasice",
                        "x": 0,
                        "y": 5,
                        "req": 72,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Kälteeinbruch', 'Eisbarriere' und 'Wasserelementar beschwören' um 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Kälteeinbruch', 'Eisbarriere' und 'Wasserelementar beschwören' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 68,
                        "name": "Winterkälte",
                        "icon": "spell_frost_chillingblast",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eures Zaubers 'Frostblitz' zusätzlich um 1% und verleiht Euren Frostschadenszaubern eine Chance von 33%, den Effekt 'Winterkälte' hervorzurufen, der die kritische Zaubertrefferchance gegen das Ziel 15 Sek. lang um 1% erhöht. Bis zu 5-mal stapelbar."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eures Zaubers 'Frostblitz' zusätzlich um 2% und verleiht Euren Frostschadenszaubern eine Chance von 66%, den Effekt 'Winterkälte' hervorzurufen, der die kritische Zaubertrefferchance gegen das Ziel 15 Sek. lang um 1% erhöht. Bis zu 5-mal stapelbar."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eures Zaubers 'Frostblitz' zusätzlich um 3% und verleiht Euren Frostschadenszaubern eine Chance von 100%, den Effekt 'Winterkälte' hervorzurufen, der die kritische Zaubertrefferchance gegen das Ziel 15 Sek. lang um 1% erhöht. Bis zu 5-mal stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2214,
                        "name": "Splitternde Barriere",
                        "icon": "ability_mage_shattershield",
                        "x": 0,
                        "y": 6,
                        "req": 71,
                        "ranks": [
                            {
                                "description": "Verleiht Eurer 'Eisbarriere' eine Chance von 50%, 8 Sek. lang alle Feinde im Umkreis von 10 Metern festzufrieren, wenn sie zerstört wird."
                            },
                            {
                                "description": "Verleiht Eurer 'Eisbarriere' eine Chance von 100%, 8 Sek. lang alle Feinde im Umkreis von 10 Metern festzufrieren, wenn sie zerstört wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 71,
                        "name": "Eisbarriere",
                        "icon": "spell_ice_lament",
                        "x": 1,
                        "y": 6,
                        "req": 72,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "cost": "21% des Grundmanas",
                                "description": "Schirmt Euch sofort ab und absorbiert 438 Schaden. Hält 1 Min. lang an. Solange der Schild hält, werden Zauber durch erlittenen Schaden nicht verzögert."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1738,
                        "name": "Arktische Winde",
                        "icon": "spell_frost_arcticwinds",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht sämtlichen von Euch verursachten Frostschaden um 1% und verringert die Chance, dass Euch Nahkampf- und Distanzangriffe treffen, um 1%."
                            },
                            {
                                "description": "Erhöht sämtlichen von Euch verursachten Frostschaden um 2% und verringert die Chance, dass Euch Nahkampf- und Distanzangriffe treffen, um 2%."
                            },
                            {
                                "description": "Erhöht sämtlichen von Euch verursachten Frostschaden um 3% und verringert die Chance, dass Euch Nahkampf- und Distanzangriffe treffen, um 3%."
                            },
                            {
                                "description": "Erhöht sämtlichen von Euch verursachten Frostschaden um 4% und verringert die Chance, dass Euch Nahkampf- und Distanzangriffe treffen, um 4%."
                            },
                            {
                                "description": "Erhöht sämtlichen von Euch verursachten Frostschaden um 5% und verringert die Chance, dass Euch Nahkampf- und Distanzangriffe treffen, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1740,
                        "name": "Machtvoller Frostblitz",
                        "icon": "spell_frost_frostbolt02",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Frostblitz' um einen Wert, der 5% Eurer Zaubermacht entspricht und verringert seine Zauberzeit um 0.1 Sek."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Frostblitz' um einen Wert, der 10% Eurer Zaubermacht entspricht und verringert seine Zauberzeit um 0.2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1853,
                        "name": "Eisige Finger",
                        "icon": "ability_mage_wintersgrasp",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verleiht Euren Kälteeffekten eine Chance von 7%, den Effekt 'Eisige Finger' hervorzurufen. Die eisigen Finger bewirken, dass Eure nächsten 2 gewirkten Zauber einen Effekt haben, den sie auf ein gefrorenes Ziel hätten. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Verleiht Euren Kälteeffekten eine Chance von 15%, den Effekt 'Eisige Finger' hervorzurufen. Die eisigen Finger bewirken, dass Eure nächsten 2 gewirkten Zauber einen Effekt haben, den sie auf ein gefrorenes Ziel hätten. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1854,
                        "name": "Hirnfrost",
                        "icon": "ability_mage_brainfreeze",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Eure Frostzauber mit Kälteeffekten haben eine Chance von 5%, Euer nächstes Wirken von 'Feuerball' oder 'Frostfeuerblitz' zu einem Spontanzauber werden zu lassen, der kein Mana kostet."
                            },
                            {
                                "description": "Eure Frostzauber mit Kälteeffekten haben eine Chance von 10%, Euer nächstes Wirken von 'Feuerball' oder 'Frostfeuerblitz' zu einem Spontanzauber werden zu lassen, der kein Mana kostet."
                            },
                            {
                                "description": "Eure Frostzauber mit Kälteeffekten haben eine Chance von 15%, Euer nächstes Wirken von 'Feuerball' oder 'Frostfeuerblitz' zu einem Spontanzauber werden zu lassen, der kein Mana kostet."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1741,
                        "name": "Wasserelementar beschwören",
                        "icon": "spell_frost_summonwaterelemental_2",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "cost": "16% des Grundmanas",
                                "description": "Beschwört einen Wasserelementar, der 45 Sek. lang für den Zaubernden kämpft."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1855,
                        "name": "Anhaltender Winter",
                        "icon": "spell_frost_summonwaterelemental_2",
                        "x": 2,
                        "y": 8,
                        "req": 1741,
                        "ranks": [
                            {
                                "description": "Erhöht die Dauer Eures Zaubers 'Wasserelementar beschwören' um 5 Sek. Euer Zauber 'Frostblitz' hat eine Chance von 33%, 10 Gruppen- oder Schlachtzugsmitgliedern einen Manaregenerationseffekt zu gewähren, der 15 Sek. alle 5 Sek. 1% ihres maximalen Manas wiederherstellt. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Erhöht die Dauer Eures Zaubers 'Wasserelementar beschwören' um 10 Sek. Euer Zauber 'Frostblitz' hat eine Chance von 66%, 10 Gruppen- oder Schlachtzugsmitgliedern einen Manaregenerationseffekt zu gewähren, der 15 Sek. alle 5 Sek. 1% ihres maximalen Manas wiederherstellt. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Erhöht die Dauer Eures Zaubers 'Wasserelementar beschwören' um 15 Sek. Euer Zauber 'Frostblitz' hat eine Chance von 100%, 10 Gruppen- oder Schlachtzugsmitgliedern einen Manaregenerationseffekt zu gewähren, der 15 Sek. alle 5 Sek. 1% ihres maximalen Manas wiederherstellt. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1856,
                        "name": "Durchgefroren",
                        "icon": "ability_mage_chilledtothebone",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Zaubern 'Frostblitz', 'Frostfeuerblitz' und 'Eislanze' verursachten Schaden um 1% und verringert das Bewegungstempo aller Ziele, welche von Kälteeffekten betroffen sind, um zusätzliche 2%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Frostblitz', 'Frostfeuerblitz' und 'Eislanze' verursachten Schaden um 2% und verringert das Bewegungstempo aller Ziele, welche von Kälteeffekten betroffen sind, um zusätzliche 4%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Frostblitz', 'Frostfeuerblitz' und 'Eislanze' verursachten Schaden um 3% und verringert das Bewegungstempo aller Ziele, welche von Kälteeffekten betroffen sind, um zusätzliche 6%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Frostblitz', 'Frostfeuerblitz' und 'Eislanze' verursachten Schaden um 4% und verringert das Bewegungstempo aller Ziele, welche von Kälteeffekten betroffen sind, um zusätzliche 8%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Frostblitz', 'Frostfeuerblitz' und 'Eislanze' verursachten Schaden um 5% und verringert das Bewegungstempo aller Ziele, welche von Kälteeffekten betroffen sind, um zusätzliche 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1857,
                        "name": "Tieffrieren",
                        "icon": "ability_mage_deepfreeze",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Betäubt das Ziel 5 Sek. lang. Funktioniert nur bei eingefrorenen Zielen. Fügt Zielen, die dauerhaft gegen Betäubungseffekte immun sind, 1469 bis 1741 Schaden zu.",
                                "cost": "9% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "311": {
        "name": "Glyphe 'Arkane Explosion'",
        "id": "311",
        "type": 0,
        "description": "Reduces mana cost of Arcane Explosion by 10%.",
        "icon": "inv_glyph_majormage",
        "itemId": "42734",
        "spellKey": "56360",
        "spellId": "56360",
        "prettyName": "",
        "typeOrder": 2
    },
    "312": {
        "name": "Glyphe 'Arkane Geschosse'",
        "id": "312",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42735",
        "spellKey": "56363",
        "spellId": "56363",
        "prettyName": "",
        "typeOrder": 2
    },
    "313": {
        "name": "Glyphe 'Arkane Macht'",
        "id": "313",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42736",
        "spellKey": "56381",
        "spellId": "56381",
        "prettyName": "",
        "typeOrder": 2
    },
    "314": {
        "name": "Glyphe 'Blinzeln'",
        "id": "314",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42737",
        "spellKey": "56365",
        "spellId": "56365",
        "prettyName": "",
        "typeOrder": 2
    },
    "315": {
        "name": "Glyphe 'Hervorrufung'",
        "id": "315",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42738",
        "spellKey": "56380",
        "spellId": "56380",
        "prettyName": "",
        "typeOrder": 2
    },
    "316": {
        "name": "Glyphe 'Feuerball'",
        "id": "316",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42739",
        "spellKey": "56368",
        "spellId": "56368",
        "prettyName": "",
        "typeOrder": 2
    },
    "317": {
        "name": "Glyphe 'Feuerschlag'",
        "id": "317",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42740",
        "spellKey": "56369",
        "spellId": "56369",
        "prettyName": "",
        "typeOrder": 2
    },
    "318": {
        "name": "Glyphe 'Frostnova'",
        "id": "318",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42741",
        "spellKey": "56376",
        "spellId": "56376",
        "prettyName": "",
        "typeOrder": 2
    },
    "319": {
        "name": "Glyphe 'Frostblitz'",
        "id": "319",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42742",
        "spellKey": "56370",
        "spellId": "56370",
        "prettyName": "",
        "typeOrder": 2
    },
    "320": {
        "name": "Glyphe 'Eisrüstung'",
        "id": "320",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42743",
        "spellKey": "56384",
        "spellId": "56384",
        "prettyName": "",
        "typeOrder": 2
    },
    "321": {
        "name": "Glyphe 'Eisblock'",
        "id": "321",
        "type": 0,
        "description": "Die Abklingzeit Eures Zaubers \\'Frostnova\\' wird jedes Mal abgeschlossen, wenn Ihr den Zauber \\'Eisblock\\' nutzt.",
        "icon": "inv_glyph_majormage",
        "itemId": "42744",
        "spellKey": "56372",
        "spellId": "56372",
        "prettyName": "",
        "typeOrder": 2
    },
    "322": {
        "name": "Glyphe 'Eislanze'",
        "id": "322",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42745",
        "spellKey": "56377",
        "spellId": "56377",
        "prettyName": "",
        "typeOrder": 2
    },
    "323": {
        "name": "Glyphe 'Eisige Adern'",
        "id": "323",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42746",
        "spellKey": "56374",
        "spellId": "56374",
        "prettyName": "",
        "typeOrder": 2
    },
    "324": {
        "name": "Glyphe 'Verbessertes Versengen'",
        "id": "324",
        "type": 0,
        "description": "The Improved Scorch talent now generates 5 applications of the Improved Scorch effect each time Scorch is cast.",
        "icon": "inv_glyph_majormage",
        "itemId": "42747",
        "spellKey": "56371",
        "spellId": "56371",
        "prettyName": "",
        "typeOrder": 2
    },
    "325": {
        "name": "Glyphe 'Unsichtbarkeit'",
        "id": "325",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42748",
        "spellKey": "56366",
        "spellId": "56366",
        "prettyName": "",
        "typeOrder": 2
    },
    "326": {
        "name": "Glyphe 'Magische Rüstung'",
        "id": "326",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42749",
        "spellKey": "56383",
        "spellId": "56383",
        "prettyName": "",
        "typeOrder": 2
    },
    "327": {
        "name": "Glyphe 'Manaedelstein'",
        "id": "327",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42750",
        "spellKey": "56367",
        "spellId": "56367",
        "prettyName": "",
        "typeOrder": 2
    },
    "328": {
        "name": "Glyphe 'Glühende Rüstung'",
        "id": "328",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42751",
        "spellKey": "56382",
        "spellId": "56382",
        "prettyName": "",
        "typeOrder": 2
    },
    "329": {
        "name": "Glyphe 'Verwandlung'",
        "id": "329",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42752",
        "spellKey": "56375",
        "spellId": "56375",
        "prettyName": "",
        "typeOrder": 2
    },
    "330": {
        "name": "Glyphe 'Fluch aufheben'",
        "id": "330",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "42753",
        "spellKey": "56364",
        "spellId": "56364",
        "prettyName": "",
        "typeOrder": 2
    },
    "331": {
        "name": "Glyphe 'Wasserelementar'",
        "id": "331",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Drachenodem\\' um 3 Sek.",
        "icon": "inv_glyph_majormage",
        "itemId": "42754",
        "spellKey": "56373",
        "spellId": "56373",
        "prettyName": "",
        "typeOrder": 2
    },
    "445": {
        "name": "Glyphe 'Arkane Intelligenz'",
        "id": "445",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers \\'Arkane Brillanz\\' um 50%.",
        "icon": "inv_glyph_minormage",
        "itemId": "43339",
        "spellKey": "57924",
        "spellId": "57924",
        "prettyName": "",
        "typeOrder": 2
    },
    "446": {
        "name": "Glyphe 'Feuerzauberschutz'",
        "id": "446",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minormage",
        "itemId": "43357",
        "spellKey": "57926",
        "spellId": "57926",
        "prettyName": "",
        "typeOrder": 2
    },
    "447": {
        "name": "Glyphe 'Frostzauberschutz'",
        "id": "447",
        "type": 1,
        "description": "Euer Zauber \\'Verwandlung\\' verwandelt das Ziel in einen Affen, anstatt in ein Schaf.",
        "icon": "inv_glyph_minormage",
        "itemId": "43360",
        "spellKey": "57927",
        "spellId": "57927",
        "prettyName": "",
        "typeOrder": 2
    },
    "448": {
        "name": "Glyphe 'Frostrüstung'",
        "id": "448",
        "type": 1,
        "description": "Verringert die Manakosten Eurer Herbeizauberungszauber um 50%.",
        "icon": "inv_glyph_minormage",
        "itemId": "43359",
        "spellKey": "57928",
        "spellId": "57928",
        "prettyName": "",
        "typeOrder": 2
    },
    "449": {
        "name": "Glyphe 'Bärenjunges'",
        "id": "449",
        "type": 1,
        "description": null,
        "icon": "inv_inscription_minorglyph15",
        "itemId": "43362",
        "spellKey": "58136",
        "spellId": "58136",
        "prettyName": "",
        "typeOrder": 2
    },
    "450": {
        "name": "Glyphe 'Pinguin'",
        "id": "450",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minormage",
        "itemId": "43361",
        "spellKey": "52648",
        "spellId": "52648",
        "prettyName": "",
        "typeOrder": 2
    },
    "451": {
        "name": "Glyphe 'Langsamer Fall'",
        "id": "451",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minormage",
        "itemId": "43364",
        "spellKey": "57925",
        "spellId": "57925",
        "prettyName": "",
        "typeOrder": 2
    },
    "591": {
        "name": "Glyphe 'Frostfeuer'",
        "id": "591",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "44684",
        "spellKey": "61205",
        "spellId": "61205",
        "prettyName": "",
        "typeOrder": 2
    },
    "611": {
        "name": "Glyphe 'Druckwelle'",
        "id": "611",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minormage",
        "itemId": "44920",
        "spellKey": "62126",
        "spellId": "62126",
        "prettyName": "",
        "typeOrder": 2
    },
    "651": {
        "name": "Glyphe 'Arkanschlag'",
        "id": "651",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "44955",
        "spellKey": "62210",
        "spellId": "62210",
        "prettyName": "",
        "typeOrder": 2
    },
    "696": {
        "name": "Glyphe 'Tieffrieren'",
        "id": "696",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "45736",
        "spellKey": "63090",
        "spellId": "63090",
        "prettyName": "",
        "typeOrder": 2
    },
    "697": {
        "name": "Glyphe 'Lebende Bombe'",
        "id": "697",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "45737",
        "spellKey": "63091",
        "spellId": "63091",
        "prettyName": "",
        "typeOrder": 2
    },
    "698": {
        "name": "Glyphe 'Arkanbeschuss'",
        "id": "698",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "45738",
        "spellKey": "63092",
        "spellId": "63092",
        "prettyName": "",
        "typeOrder": 2
    },
    "699": {
        "name": "Glyphe 'Spiegelbild'",
        "id": "699",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "45739",
        "spellKey": "63093",
        "spellId": "63093",
        "prettyName": "",
        "typeOrder": 2
    },
    "700": {
        "name": "Glyphe 'Eisbarriere'",
        "id": "700",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majormage",
        "itemId": "45740",
        "spellKey": "63095",
        "spellId": "63095",
        "prettyName": "",
        "typeOrder": 2
    },
    "871": {
        "name": "Glyph of Eternal Water",
        "id": "871",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Manaschild\\' um 2 Sek.",
        "icon": "inv_glyph_majormage",
        "itemId": "50045",
        "spellKey": "70937",
        "spellId": "70937",
        "prettyName": "",
        "typeOrder": 2
    }
}]
}