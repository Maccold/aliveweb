{
    "talentData": {
        "characterClass": {
            "classId": 1,
            "name": "Krieger",
            "powerType": "RAGE",
            "powerTypeId": 1,
            "powerTypeSlug": "rage"
        },
        "talentTrees": [
            {
                "name": "Waffen",
                "icon": "ability_warrior_savageblow",
                "backgroundFile": "WarriorArms",
                "overlayColor": "#ffb719",
                "description": "Ein kampfgestählter Meister im Kampf mit Zweihandwaffen, der sein Geschick und seine überwältigenden Angriffe nutzt, um seine Gegner niederzuzwingen.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 124,
                        "name": "Verbesserter heldenhafter Stoß",
                        "icon": "ability_rogue_ambush",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeit 'Heldenhafter Stoß' um 1 Wut."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeit 'Heldenhafter Stoß' um 2 Wut."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeit 'Heldenhafter Stoß' um 3 Wut."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 130,
                        "name": "Abwehr",
                        "icon": "ability_parry",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Parierchance um 1%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 3%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 127,
                        "name": "Verbessertes Verwunden",
                        "icon": "ability_gouge",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Eure Fähigkeit 'Verwunden' erzielten Blutungsschaden um 10%."
                            },
                            {
                                "description": "Erhöht den durch Eure Fähigkeit 'Verwunden' erzielten Blutungsschaden um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 126,
                        "name": "Verbesserter Sturmangriff",
                        "icon": "ability_warrior_charge",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Menge der durch Eure Fähigkeit 'Sturmangriff' erzeugten Wut um 5."
                            },
                            {
                                "description": "Erhöht die Menge der durch Eure Fähigkeit 'Sturmangriff' erzeugten Wut um 10."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 641,
                        "name": "Eiserner Wille",
                        "icon": "spell_magic_magearmor",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer gegen Euch angewandter Betäubungs- und Bezauberungseffekte um 7%."
                            },
                            {
                                "description": "Verringert die Dauer gegen Euch angewandter Betäubungs- und Bezauberungseffekte um 14%."
                            },
                            {
                                "description": "Verringert die Dauer gegen Euch angewandter Betäubungs- und Bezauberungseffekte um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 128,
                        "name": "Taktiker",
                        "icon": "spell_nature_enchantarmor",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Ihr behaltet bis zu 5 zusätzliche Wutpunkte, wenn Ihr die Haltung ändert. Zusätzlich wird die durch Eure Fähigkeiten 'Blutdurst' und 'Tödlicher Stoß' erzeugte Bedrohung stark erhöht, wenn Ihr Euch in der Verteidigungshaltung befindet."
                            },
                            {
                                "description": "Ihr behaltet bis zu 10 zusätzliche Wutpunkte, wenn Ihr die Haltung ändert. Zusätzlich wird die durch Eure Fähigkeiten 'Blutdurst' und 'Tödlicher Stoß' erzeugte Bedrohung stark erhöht, wenn Ihr Euch in der Verteidigungshaltung befindet (Wirkungsvoller als Rang 1)."
                            },
                            {
                                "description": "Ihr behaltet bis zu 15 zusätzliche Wutpunkte, wenn Ihr die Haltung ändert. Zusätzlich wird die durch Eure Fähigkeiten 'Blutdurst' und 'Tödlicher Stoß' erzeugte Bedrohung stark erhöht, wenn Ihr Euch in der Verteidigungshaltung befindet (Wirkungsvoller als Rang 2)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 131,
                        "name": "Verbessertes Überwältigen",
                        "icon": "inv_sword_05",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer mit Eurer Fähigkeit 'Überwältigen' um 25%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer mit Eurer Fähigkeit 'Überwältigen' um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 137,
                        "name": "Aggressionskontrolle",
                        "icon": "spell_holy_blessingofstamina",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erzeugt alle 3 Sekunden 1 Wutpunkt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 662,
                        "name": "Durchbohren",
                        "icon": "ability_searingarrow",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten um 10%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Fähigkeiten um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 121,
                        "name": "Tiefe Wunden",
                        "icon": "ability_backstab",
                        "x": 3,
                        "y": 2,
                        "req": 662,
                        "ranks": [
                            {
                                "description": "Eure kritischen Treffer lassen das Ziel bluten und verursachen 6 Sek. lang 16% des durchschnittlichen Schadens Eurer Nahkampfwaffe."
                            },
                            {
                                "description": "Eure kritischen Treffer lassen das Ziel bluten und verursachen 6 Sek. lang 32% des durchschnittlichen Schadens Eurer Nahkampfwaffe."
                            },
                            {
                                "description": "Eure kritischen Treffer lassen das Ziel bluten und verursachen 6 Sek. lang 48% des durchschnittlichen Schadens Eurer Nahkampfwaffe."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 136,
                        "name": "Zweihandwaffen-Spezialisierung",
                        "icon": "inv_axe_09",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2232,
                        "name": "Verlangen nach Blut",
                        "icon": "ability_rogue_hungerforblood",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Wann immer Eure Fähigkeit 'Verwunden' Schaden verursacht, besteht eine Chance von 33%, dass 9 Sek. lang Eure Fähigkeit 'Überwältigen' genutzt werden kann. Eine Aufladung. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Wann immer Eure Fähigkeit 'Verwunden' Schaden verursacht, besteht eine Chance von 66%, dass 9 Sek. lang Eure Fähigkeit 'Überwältigen' genutzt werden kann. Eine Aufladung. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Wann immer Eure Fähigkeit 'Verwunden' Schaden verursacht, besteht eine Chance von 100%, dass 9 Sek. lang Eure Fähigkeit 'Überwältigen' genutzt werden kann. Eine Aufladung. Dieser Effekt kann nur ein Mal alle 6 Sek. auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 132,
                        "name": "Axt- und Stangenspezialisierung",
                        "icon": "inv_axe_06",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Äxten und Stangenwaffe einen kritischen Treffer zu erzielen sowie den verursachten kritischen Schaden um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Äxten und Stangenwaffe einen kritischen Treffer zu erzielen sowie den verursachten kritischen Schaden um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Äxten und Stangenwaffe einen kritischen Treffer zu erzielen sowie den verursachten kritischen Schaden um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Äxten und Stangenwaffe einen kritischen Treffer zu erzielen sowie den verursachten kritischen Schaden um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Äxten und Stangenwaffe einen kritischen Treffer zu erzielen sowie den verursachten kritischen Schaden um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 133,
                        "name": "Weitreichende Stöße",
                        "icon": "ability_rogue_slicedice",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "cost": "30 Wut",
                                "description": "Eure nächsten 5 Nahkampfangriffe treffen einen weiteren in der Nähe befindlichen Gegner."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 125,
                        "name": "Streitkolben-Spezialisierung",
                        "icon": "inv_mace_01",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 3% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 6% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 9% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 12% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 15% der Rüstung Eures Feindes."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 123,
                        "name": "Schwert-Spezialisierung",
                        "icon": "inv_sword_27",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 2%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert einen Treffer erzielt habt. Dieser Effekt kann nicht mehr als einmal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 4%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert einen Treffer erzielt habt. Dieser Effekt kann nicht mehr als einmal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 6%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert einen Treffer erzielt habt. Dieser Effekt kann nicht mehr als einmal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 8%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert einen Treffer erzielt habt. Dieser Effekt kann nicht mehr als einmal alle 6 Sek. auftreten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 10%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert einen Treffer erzielt habt. Dieser Effekt kann nicht mehr als einmal alle 6 Sek. auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 134,
                        "name": "Waffenbeherrschung",
                        "icon": "ability_warrior_weaponmastery",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Verringert die Chance, Euren Angriffen auszuweichen, um 1% und reduziert die Wirkungsdauer aller Entwaffnungseffekte gegen Euch um 25%. Dieser Effekt ist nicht mit anderen Effekten, welche die Wirkungsdauer von Entwaffnung verringern stapelbar."
                            },
                            {
                                "description": "Verringert die Chance, Euren Angriffen auszuweichen, um 2% und reduziert die Wirkungsdauer aller Entwaffnungseffekte gegen Euch um 50%. Dieser Effekt ist nicht mit anderen Effekten, welche die Wirkungsdauer von Entwaffnung verringern stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 129,
                        "name": "Verbesserte Kniesehne",
                        "icon": "ability_shockwave",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Gewährt Eurer Fähigkeit 'Kniesehne' eine Chance von 5%, das Ziel 5 Sek. lang unbeweglich zu machen."
                            },
                            {
                                "description": "Gewährt Eurer Fähigkeit 'Kniesehne' eine Chance von 10%, das Ziel 5 Sek. lang unbeweglich zu machen."
                            },
                            {
                                "description": "Gewährt Eurer Fähigkeit 'Kniesehne' eine Chance von 15%, das Ziel 5 Sek. lang unbeweglich zu machen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1859,
                        "name": "Trauma",
                        "icon": "ability_warrior_bloodnova",
                        "x": 3,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Eure kritischen Nahkampftreffer erhöhen die Wirksamkeit von Blutungseffekten auf dem Ziel 1 Min. lang um 15%."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer erhöhen die Wirksamkeit von Blutungseffekten auf dem Ziel 1 Min. lang um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1663,
                        "name": "Kräfte sammeln",
                        "icon": "ability_hunter_harass",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Jedes Mal, wenn Ihr von einem Betäubungs- oder Unbeweglichkeitseffekt getroffen werdet, erzeugt Ihr 10 Sek. lang insgesamt 10 Wut und 5% Eurer gesamten Gesundheit."
                            },
                            {
                                "description": "Jedes Mal, wenn Ihr von einem Betäubungs- oder Unbeweglichkeitseffekt getroffen werdet, erzeugt Ihr 10 Sek. lang insgesamt 20 Wut und 10% Eurer gesamten Gesundheit."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 135,
                        "name": "Tödlicher Stoß",
                        "icon": "ability_warrior_savageblow",
                        "x": 1,
                        "y": 6,
                        "req": 133,
                        "ranks": [
                            {
                                "description": "Ein heimtückischer Stoß, der Waffenschaden plus 85 verursacht und das Ziel verwundet sowie die Wirksamkeit jeglicher Heilung 10 Sek. lang um 50% verringert.",
                                "cost": "30 Wut",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1862,
                        "name": "Waffenstärke",
                        "icon": "ability_warrior_offensivestance",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Stärke und Ausdauer um 2% sowie Eure Waffenkunde um 2."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke und Ausdauer um 4% sowie Eure Waffenkunde um 4."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2233,
                        "name": "Verbessertes Zerschmettern",
                        "icon": "ability_warrior_decisivestrike",
                        "x": 3,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Schwungzeit Eurer Fähigkeit 'Zerschmettern' um 0.5 Sek."
                            },
                            {
                                "description": "Verringert die Schwungzeit Eurer Fähigkeit 'Zerschmettern' um 1 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2283,
                        "name": "Dampfwalze",
                        "icon": "ability_warrior_bullrush",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeit 'Sturmangriff' kann nun im Kampf verwendet werden, ihre Abklingzeit wird jedoch um 5 Sek. erhöht. Nach dem Sturmangriff hat innerhalb von 10 Sek. Euer nächstes Wirken von 'Zerschmettern' oder 'Tödlicher Stoß' eine zusätzlich um 25% erhöhte kritische Trefferchance."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1824,
                        "name": "Verbesserter tödlicher Stoß",
                        "icon": "ability_warrior_savageblow",
                        "x": 1,
                        "y": 7,
                        "req": 135,
                        "ranks": [
                            {
                                "description": "Erhöht den von 'Tödlicher Stoß' verursachten Schaden um 3% und verringert die Abklingzeit dieser Fähigkeit um 0.3 Sek."
                            },
                            {
                                "description": "Erhöht den von 'Tödlicher Stoß' verursachten Schaden um 6% und verringert die Abklingzeit dieser Fähigkeit um 0.7 Sek."
                            },
                            {
                                "description": "Erhöht den von 'Tödlicher Stoß' verursachten Schaden um 10% und verringert die Abklingzeit dieser Fähigkeit um 1 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1860,
                        "name": "Unerbittliche Gewalt",
                        "icon": "ability_warrior_unrelentingassault",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeiten Eurer Fähigkeiten 'Überwältigen' und 'Rache' um 2 Sek. und erhöht ihren Schaden um 10%. Zudem wird, wenn Ihr einen Spieler mit 'Überwältigen' trefft, während er einen Zauber wirkt, sein Zauberschaden und seine Heilung 6 Sek. lang um 25% verringert."
                            },
                            {
                                "description": "Verringert die Abklingzeiten Eurer Fähigkeiten 'Überwältigen' und 'Rache' um 4 Sek. und erhöht ihren Schaden um 20%. Zudem wird, wenn Ihr einen Spieler mit 'Überwältigen' trefft, während er einen Zauber wirkt, sein Zauberschaden und seine Heilung 6 Sek. lang um 50% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1662,
                        "name": "Plötzlicher Tod",
                        "icon": "ability_warrior_improveddisciplines",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Eure Nahkampftreffer haben eine Chance von 3%, dass 'Hinrichten' unabhängig vom Gesundheitswert des Ziels angewendet werden kann. Zusätzlich behaltet Ihr nach seiner Nutzung mindestens 3 Wut."
                            },
                            {
                                "description": "Eure Nahkampftreffer haben eine Chance von 6%, dass 'Hinrichten' unabhängig vom Gesundheitswert des Ziels angewendet werden kann. Zusätzlich behaltet Ihr nach seiner Nutzung mindestens 7 Wut."
                            },
                            {
                                "description": "Eure Nahkampftreffer haben eine Chance von 9%, dass 'Hinrichten' unabhängig vom Gesundheitswert des Ziels angewendet werden kann. Zusätzlich behaltet Ihr nach seiner Nutzung mindestens 10 Wut."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1661,
                        "name": "Endlose Wut",
                        "icon": "ability_warrior_endlessrage",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Ihr erhaltet 25% mehr Wut durch verursachten Schaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1664,
                        "name": "Blutraserei",
                        "icon": "ability_warrior_bloodfrenzy",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Nahkampfangriffstempo um 5%. Zusätzlich erhöhen Eure Fähigkeiten 'Verwunden' und 'Tiefe Wunden' jeglichen körperlichen Schaden gegen das Ziel um 2%."
                            },
                            {
                                "description": "Erhöht Euer Nahkampfangriffstempo um 10%. Zusätzlich erhöhen Eure Fähigkeiten 'Verwunden' und 'Tiefe Wunden' jeglichen körperlichen Schaden gegen das Ziel um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2231,
                        "name": "Abrisskommando",
                        "icon": "ability_warrior_trauma",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Eure kritischen Nahkampftreffer machen Euch wütend, sodass 12 Sek. lang Euer verursachter Schaden um 2% erhöht wird. Dieser Effekt ist nicht stapelbar mit 'Wutanfall'."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer machen Euch wütend, sodass 12 Sek. lang Euer verursachter Schaden um 4% erhöht wird. Dieser Effekt ist nicht stapelbar mit 'Wutanfall'."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer machen Euch wütend, sodass 12 Sek. lang Euer verursachter Schaden um 6% erhöht wird. Dieser Effekt ist nicht stapelbar mit 'Wutanfall'."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer machen Euch wütend, sodass 12 Sek. lang Euer verursachter Schaden um 8% erhöht wird. Dieser Effekt ist nicht stapelbar mit 'Wutanfall'."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer machen Euch wütend, sodass 12 Sek. lang Euer verursachter Schaden um 10% erhöht wird. Dieser Effekt ist nicht stapelbar mit 'Wutanfall'."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1863,
                        "name": "Klingensturm",
                        "icon": "ability_warrior_bladestorm",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1,5 Min. Abklingzeit",
                                "cost": "25 Wut",
                                "description": "Greift sofort bis zu 4 nahe Ziele an. Für die nächsten 6 Sek. werdet Ihr alle 1 Sek. einen Wirbelwindangriff durchführen. Während 'Klingensturm' aktiv ist, könnt Ihr Euch bewegen, aber während dieser Zeit keine anderen Fähigkeiten einsetzen. Ihr fühlt weder Mitleid, noch Bedauern oder Furcht und es gibt keine Möglichkeit, Euch aufzuhalten, es sei denn, Ihr werdet getötet."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Furor",
                "icon": "ability_warrior_innerrage",
                "backgroundFile": "WarriorFury",
                "overlayColor": "#ff0000",
                "description": "Ein tobender Berserker, der mit einer Waffe in jeder Hand seine Feinde in einem Wirbel aus Stahl in Fetzen zerlegt.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 2250,
                        "name": "Bis an die Zähne bewaffnet",
                        "icon": "inv_shoulder_22",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Pro 108 Rüstungswert wird Eure Angriffskraft um 1 erhöht."
                            },
                            {
                                "description": "Pro 108 Rüstungswert wird Eure Angriffskraft um 2 erhöht."
                            },
                            {
                                "description": "Pro 108 Rüstungswert wird Eure Angriffskraft um 3 erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 158,
                        "name": "Donnernde Stimme",
                        "icon": "spell_nature_purge",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Dauer und den Wirkungsbereich von 'Schlachtruf', 'Demoralisierender Ruf' und 'Befehlsruf' um 25%."
                            },
                            {
                                "description": "Erhöht die Dauer und den Wirkungsbereich von 'Schlachtruf', 'Demoralisierender Ruf' und 'Befehlsruf' um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 157,
                        "name": "Grausamkeit",
                        "icon": "ability_rogue_eviscerate",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Nahkampfwaffen einen kritischen Treffer zu erzielen, um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Nahkampfwaffen einen kritischen Treffer zu erzielen, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Nahkampfwaffen einen kritischen Treffer zu erzielen, um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Nahkampfwaffen einen kritischen Treffer zu erzielen, um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Nahkampfwaffen einen kritischen Treffer zu erzielen, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 161,
                        "name": "Verbesserter demoralisierender Ruf",
                        "icon": "ability_warrior_warcry",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Nahkampfangriffskraftreduzierung Eures demoralisierenden Rufs um 8%."
                            },
                            {
                                "description": "Erhöht die Nahkampfangriffskraftreduzierung Eures demoralisierenden Rufs um 16%."
                            },
                            {
                                "description": "Erhöht die Nahkampfangriffskraftreduzierung Eures demoralisierenden Rufs um 24%."
                            },
                            {
                                "description": "Erhöht die Nahkampfangriffskraftreduzierung Eures demoralisierenden Rufs um 32%."
                            },
                            {
                                "description": "Erhöht die Nahkampfangriffskraftreduzierung Eures demoralisierenden Rufs um 40%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 159,
                        "name": "Entfesselter Zorn",
                        "icon": "spell_nature_stoneclawtotem",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance, einen zusätzlichen Wutpunkt zu generieren, wenn Ihr mit einer Waffe Nahkampfschaden zufügt."
                            },
                            {
                                "description": "Gewährt Euch eine Chance, einen zusätzlichen Wutpunkt zu generieren, wenn Ihr mit einer Waffe Nahkampfschaden zufügt. Der Effekt wird öfter ausgelöst als bei 'Entfesselter Zorn' (Rang 1)."
                            },
                            {
                                "description": "Gewährt Euch eine Chance, einen zusätzlichen Wutpunkt zu generieren, wenn Ihr mit einer Waffe Nahkampfschaden zufügt. Der Effekt wird öfter ausgelöst als bei 'Entfesselter Zorn' (Rang 2)."
                            },
                            {
                                "description": "Gewährt Euch eine Chance, einen zusätzlichen Wutpunkt zu generieren, wenn Ihr mit einer Waffe Nahkampfschaden zufügt. Der Effekt wird öfter ausgelöst als bei 'Entfesselter Zorn' (Rang 3)."
                            },
                            {
                                "description": "Gewährt Euch eine Chance, einen zusätzlichen Wutpunkt zu generieren, wenn Ihr mit einer Waffe Nahkampfschaden zufügt. Der Effekt wird öfter ausgelöst als bei 'Entfesselter Zorn' (Rang 4)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 166,
                        "name": "Verbessertes Spalten",
                        "icon": "ability_warrior_cleave",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Spalten' zusätzlich angerichteten Schaden um 40%."
                            },
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Spalten' zusätzlich angerichteten Schaden um 80%."
                            },
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Spalten' zusätzlich angerichteten Schaden um 120%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 160,
                        "name": "Durchdringendes Heulen",
                        "icon": "spell_shadow_deathscream",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cost": "10 Wut",
                                "description": "Macht 6 Sek. lang alle Gegner in einem Umkreis von 10 Metern benommen, verlangsamt ihr Bewegungstempo um 50%."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 661,
                        "name": "Blutwahnsinn",
                        "icon": "spell_shadow_summonimp",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Stellt im Verlauf von 6 Sek. 2% Eurer Gesundheit wieder her, nachdem Ihr Opfer eines kritischen Treffers wurdet."
                            },
                            {
                                "description": "Stellt im Verlauf von 6 Sek. 4% Eurer Gesundheit wieder her, nachdem Ihr Opfer eines kritischen Treffers wurdet."
                            },
                            {
                                "description": "Stellt im Verlauf von 6 Sek. 6% Eurer Gesundheit wieder her, nachdem Ihr Opfer eines kritischen Treffers wurdet."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 154,
                        "name": "Gebieterische Stimme",
                        "icon": "spell_nature_focusedmind",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den Nahkampfangriffskraftbonus Eures Schlachtrufs und den Gesundheitsbonus Eures Befehlsrufs um 5%."
                            },
                            {
                                "description": "Erhöht den Nahkampfangriffskraftbonus Eures Schlachtrufs und den Gesundheitsbonus Eures Befehlsrufs um 10%."
                            },
                            {
                                "description": "Erhöht den Nahkampfangriffskraftbonus Eures Schlachtrufs und den Gesundheitsbonus Eures Befehlsrufs um 15%."
                            },
                            {
                                "description": "Erhöht den Nahkampfangriffskraftbonus Eures Schlachtrufs und den Gesundheitsbonus Eures Befehlsrufs um 20%."
                            },
                            {
                                "description": "Erhöht den Nahkampfangriffskraftbonus Eures Schlachtrufs und den Gesundheitsbonus Eures Befehlsrufs um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1581,
                        "name": "Beidhändigkeits-Spezialisierung",
                        "icon": "ability_dualwield",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 5%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 10%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 15%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 20%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1542,
                        "name": "Verbessertes Hinrichten",
                        "icon": "inv_sword_48",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Wutkosten Eurer Fähigkeit 'Hinrichten' um 2."
                            },
                            {
                                "description": "Verringert die Wutkosten Eurer Fähigkeit 'Hinrichten' um 5."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 155,
                        "name": "Wutanfall",
                        "icon": "spell_shadow_unholyfrenzy",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verleiht Euch eine Chance von 30%, nach dem Erleiden eines Schaden verursachenden Angriffs 12 Sek. lang einen Schadensbonus von 2% zu erhalten. Dieser Effekt ist nicht stapelbar mit 'Abrisskommando'."
                            },
                            {
                                "description": "Verleiht Euch eine Chance von 30%, nach dem Erleiden eines Schaden verursachenden Angriffs 12 Sek. lang einen Schadensbonus von 4% zu erhalten. Dieser Effekt ist nicht stapelbar mit 'Abrisskommando'."
                            },
                            {
                                "description": "Verleiht Euch eine Chance von 30%, nach dem Erleiden eines Schaden verursachenden Angriffs 12 Sek. lang einen Schadensbonus von 6% zu erhalten. Dieser Effekt ist nicht stapelbar mit 'Abrisskommando'."
                            },
                            {
                                "description": "Verleiht Euch eine Chance von 30%, nach dem Erleiden eines Schaden verursachenden Angriffs 12 Sek. lang einen Schadensbonus von 8% zu erhalten. Dieser Effekt ist nicht stapelbar mit 'Abrisskommando'."
                            },
                            {
                                "description": "Verleiht Euch eine Chance von 30%, nach dem Erleiden eines Schaden verursachenden Angriffs 12 Sek. lang einen Schadensbonus von 10% zu erhalten. Dieser Effekt ist nicht stapelbar mit 'Abrisskommando'."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1657,
                        "name": "Präzision",
                        "icon": "ability_marksmanship",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Trefferchance mit Nahkampfwaffen um 1%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance mit Nahkampfwaffen um 2%."
                            },
                            {
                                "description": "Erhöht Eure Trefferchance mit Nahkampfwaffen um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 165,
                        "name": "Todeswunsch",
                        "icon": "spell_shadow_deathpact",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "cost": "10 Wut",
                                "description": "Erhöht bei Aktivierung Euren verursachten körperlichen Schaden um 20%, erhöht jedoch jeglichen erlittenen Schaden um 5%. Hält 30 Sek. an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1543,
                        "name": "Verbessertes Abfangen",
                        "icon": "ability_rogue_sprint",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Abfangen' um 5 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Abfangen' um 10 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1541,
                        "name": "Verbesserte Berserkerwut",
                        "icon": "spell_nature_ancestralguardian",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Eure Berserkerfähigkeit wird bei Benutzung 10 Wut generieren."
                            },
                            {
                                "description": "Eure Berserkerfähigkeit wird bei Benutzung 20 Wut generieren."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 156,
                        "name": "Schlaghagel",
                        "icon": "ability_ghoulfrenzy",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 5%, nachdem Ihr einen kritischen Nahkampftreffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 10%, nachdem Ihr einen kritischen Nahkampftreffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 15%, nachdem Ihr einen kritischen Nahkampftreffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 20%, nachdem Ihr einen kritischen Nahkampftreffer zugefügt habt."
                            },
                            {
                                "description": "Erhöht Euer Angriffstempo für die nächsten 3 Waffenschwünge um 25%, nachdem Ihr einen kritischen Nahkampftreffer zugefügt habt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1864,
                        "name": "Wut verstärken",
                        "icon": "ability_warrior_endlessrage",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Blutrausch', 'Berserkerwut', 'Tollkühnheit' und 'Todeswunsch' um 11%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Blutrausch', 'Berserkerwut', 'Tollkühnheit' und 'Todeswunsch' um 22%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Blutrausch', 'Berserkerwut', 'Tollkühnheit' und 'Todeswunsch' um 33%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 167,
                        "name": "Blutdurst",
                        "icon": "spell_nature_bloodlust",
                        "x": 1,
                        "y": 6,
                        "req": 165,
                        "ranks": [
                            {
                                "description": "Greift das Ziel sofort an und verursacht 13 Schaden. Zusätzlich werden die nächsten erfolgreichen 3 Nahkampfangriffe 1% der maximalen Gesundheit wiederherstellen. Dieser Effekt hält 8 Sek. lang an. Schaden basiert auf Angriffskraft.",
                                "cost": "20 Wut",
                                "castTime": "Spontanzauber",
                                "cooldown": "4 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1655,
                        "name": "Verbesserter Wirbelwind",
                        "icon": "ability_whirlwind",
                        "x": 3,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Wirbelwind' um 10%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Wirbelwind' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1865,
                        "name": "Wütende Angriffe",
                        "icon": "ability_warrior_furiousresolve",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure normalen Nahkampfangriffe haben eine Chance, jegliche erhaltenen Heileffekte des Ziels 10 Sek. lang um 25% zu verringern. Bis zu 2-mal stapelbar."
                            },
                            {
                                "description": "Eure normalen Nahkampfangriffe haben eine Chance, jegliche erhaltene Heilung des Ziels 10 Sek. lang um 25% zu verringern. Bis zu 2-mal stapelbar. Tritt häufiger auf als 'Wütende Angriffe' (Rang 1)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1658,
                        "name": "Verbesserte Berserkerhaltung",
                        "icon": "ability_racial_avatar",
                        "x": 3,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht in der Berserkerhaltung die Stärke um 4% und verringert die von Euch in dieser Haltung erzeugte Bedrohung um 2%."
                            },
                            {
                                "description": "Erhöht in der Berserkerhaltung die Stärke um 8% und verringert die von Euch in dieser Haltung erzeugte Bedrohung um 4%."
                            },
                            {
                                "description": "Erhöht in der Berserkerhaltung die Stärke um 12% und verringert die von Euch in dieser Haltung erzeugte Bedrohung um 6%."
                            },
                            {
                                "description": "Erhöht in der Berserkerhaltung die Stärke um 16% und verringert die von Euch in dieser Haltung erzeugte Bedrohung um 8%."
                            },
                            {
                                "description": "Erhöht in der Berserkerhaltung die Stärke um 20% und verringert die von Euch in dieser Haltung erzeugte Bedrohung um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1868,
                        "name": "Heldenhafter Furor",
                        "icon": "ability_heroicleap",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "45 Sek. Abklingzeit",
                                "description": "Entfernt jegliche Effekte, die Bewegungsunfähigkeit verursachen und schließt die Abklingzeit von 'Abfangen' ab."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1659,
                        "name": "Toben",
                        "icon": "ability_warrior_rampage",
                        "x": 1,
                        "y": 8,
                        "req": 167,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Nahkampf- und Distanztrefferchance aller Gruppen- und Schlachtzugsmitglieder innerhalb von 100 Metern um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1866,
                        "name": "Schäumendes Blut",
                        "icon": "ability_warrior_bloodsurge",
                        "x": 2,
                        "y": 8,
                        "req": 167,
                        "ranks": [
                            {
                                "description": "Mit den Fähigkeiten 'Heldenhafter Stoß', 'Blutdurst' und 'Wirbelwind' erzielte Treffer haben eine Chance von 7%, dass das nächste 'Zerschmettern' innerhalb der nächsten 5 Sek. spontan gewirkt werden kann."
                            },
                            {
                                "description": "Mit den Fähigkeiten 'Heldenhafter Stoß', 'Blutdurst' und 'Wirbelwind' erzielte Treffer haben eine Chance von 13%, dass das nächste 'Zerschmettern' innerhalb der nächsten 5 Sek. spontan gewirkt werden kann."
                            },
                            {
                                "description": "Mit den Fähigkeiten 'Heldenhafter Stoß', 'Blutdurst' und 'Wirbelwind' erzielte Treffer haben eine Chance von 20%, dass das nächste 'Zerschmettern' innerhalb der nächsten 5 Sek. spontan gewirkt werden kann."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2234,
                        "name": "Unendlicher Furor",
                        "icon": "ability_warrior_intensifyrage",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Zerschmettern', 'Wirbelwind' und 'Blutdurst' um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Zerschmettern', 'Wirbelwind' und 'Blutdurst' um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Zerschmettern', 'Wirbelwind' und 'Blutdurst' um 6%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Zerschmettern', 'Wirbelwind' und 'Blutdurst' um 8%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Zerschmettern', 'Wirbelwind' und 'Blutdurst' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1867,
                        "name": "Titanengriff",
                        "icon": "ability_warrior_titansgrip",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Erlaubt es Euch, Zweihandäxte, Zweihandstreitkolben und Zweihandschwerter in einer Hand zu führen. Während in einer Hand eine Zweihandwaffe ausgerüstet ist, ist Euer verursachter körperlicher Schaden um 10% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Schutz",
                "icon": "ability_warrior_defensivestance",
                "backgroundFile": "WarriorProtection",
                "overlayColor": "#4c7fff",
                "description": "Ein tapferer Beschützer, der einen Schild nutzt, um sich und seine Verbündeten zu schützen.",
                "treeNo": 2,
                "roles": {
                    "tank": true,
                    "healer": false,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 142,
                        "name": "Verbesserter Blutrausch",
                        "icon": "ability_racial_bloodrage",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die von Eurer Fähigkeit 'Blutrausch' sofort erzeugte Wut um 25%."
                            },
                            {
                                "description": "Erhöht die von Eurer Fähigkeit 'Blutrausch' sofort erzeugte Wut um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1601,
                        "name": "Schild-Spezialisierung",
                        "icon": "inv_shield_06",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, Angriffe mit einem Schild zu blocken, um 1% und gewährt eine 20% Chance 5 Wut zu generieren, wenn erfolgreich geblockt, ausgewichen oder pariert wurde."
                            },
                            {
                                "description": "Erhöht Eure Chance, Angriffe mit einem Schild zu blocken, um 2% und gewährt eine 40% Chance 5 Wut zu generieren, wenn erfolgreich geblockt, ausgewichen oder pariert wurde."
                            },
                            {
                                "description": "Erhöht Eure Chance, Angriffe mit einem Schild zu blocken, um 3% und gewährt eine 60% Chance 5 Wut zu generieren, wenn erfolgreich geblockt, ausgewichen oder pariert wurde."
                            },
                            {
                                "description": "Erhöht Eure Chance, Angriffe mit einem Schild zu blocken, um 4% und gewährt eine 80% Chance 5 Wut zu generieren, wenn erfolgreich geblockt, ausgewichen oder pariert wurde."
                            },
                            {
                                "description": "Erhöht Eure Chance, Angriffe mit einem Schild zu blocken, um 5% und gewährt eine 100% Chance 5 Wut zu generieren, wenn erfolgreich geblockt, ausgewichen oder pariert wurde."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 141,
                        "name": "Verbesserter Donnerknall",
                        "icon": "ability_thunderclap",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeit 'Donnerknall' um 1 Wut, erhöht den Schaden um 10% und den Verlangsamungseffekt um zusätzliche 4%."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeit 'Donnerknall' um 2 Wut, erhöht den Schaden um 20% und den Verlangsamungseffekt um zusätzliche 7%."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeit 'Donnerknall' um 4 Wut, erhöht den Schaden um 30% und den Verlangsamungseffekt um zusätzliche 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 144,
                        "name": "Anstacheln",
                        "icon": "ability_warrior_incite",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Heldenhafter Stoß', 'Donnerknall' und 'Spalten' um 5%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Heldenhafter Stoß', 'Donnerknall' und 'Spalten' um 10%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Fähigkeiten 'Heldenhafter Stoß', 'Donnerknall' und 'Spalten' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 138,
                        "name": "Vorahnung",
                        "icon": "spell_nature_mirrorimage",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance um 1%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 3%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 153,
                        "name": "Letztes Gefecht",
                        "icon": "spell_holy_ashestoashes",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Wenn diese Fähigkeit aktiviert wird, gewährt sie Euch 20 Sek. lang vorübergehend 30% Eurer maximalen Gesundheit. Wenn der Effekt abklingt, geht die zusätzliche Gesundheit wieder verloren."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 147,
                        "name": "Verbesserte Rache",
                        "icon": "ability_warrior_revenge",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Rache' um 30% und lässt 'Rache' ein weiteres Ziel treffen, das 50% des Schadens erleidet."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeit 'Rache' um 60% und lässt 'Rache' ein weiteres Ziel treffen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1654,
                        "name": "Schildbeherrschung",
                        "icon": "ability_warrior_shieldmastery",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Blockwert um 15% und verringert die Abklingzeit Eurer Fähigkeit 'Schildblock' um 10 Sek."
                            },
                            {
                                "description": "Erhöht Euren Blockwert um 30% und verringert die Abklingzeit Eurer Fähigkeit 'Schildblock' um 20 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 140,
                        "name": "Zähigkeit",
                        "icon": "spell_holy_devotion",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 2% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 6%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 4% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 12%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 6% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 18%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 8% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 24%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 10% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2247,
                        "name": "Verbesserte Zauberreflexion",
                        "icon": "ability_warrior_shieldreflection",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert Eure Chance, einen Zaubertreffer zu erleiden, um 2% und die Fähigkeit reflektiert bei Anwendung auch den ersten Zauber, der auf die nächsten 2 Gruppenmitglieder innerhalb von 20 Metern gewirkt wurde."
                            },
                            {
                                "description": "Verringert Eure Chance, einen Zaubertreffer zu erleiden, um 4% und die Fähigkeit reflektiert bei Anwendung auch den ersten Zauber, der auf die nächsten 4 Gruppenmitglieder innerhalb von 20 Metern gewirkt wurde."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 151,
                        "name": "Verbessertes Entwaffnen",
                        "icon": "ability_warrior_disarm",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Entwaffnen' um 10 Sek. und lässt das Ziel, während es entwaffnet ist, 5% zusätzlichen Schaden erleiden."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Entwaffnen' um 20 Sek. und lässt das Ziel, während es entwaffnet ist, 10% zusätzlichen Schaden erleiden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 146,
                        "name": "Durchstechen",
                        "icon": "ability_warrior_sunder",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Wutkosten Eurer Fähigkeiten 'Rüstung zerreißen' und 'Verwüsten' um 1."
                            },
                            {
                                "description": "Verringert die Wutkosten Eurer Fähigkeiten 'Rüstung zerreißen' und 'Verwüsten' um 2."
                            },
                            {
                                "description": "Verringert die Wutkosten Eurer Fähigkeiten 'Rüstung zerreißen' und 'Verwüsten' um 3."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 150,
                        "name": "Verbesserte Disziplinen",
                        "icon": "ability_warrior_shieldwall",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Schildwall', 'Gegenschlag' und 'Tollkühnheit' um 30 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Schildwall', 'Gegenschlag' und 'Tollkühnheit' um 1 Min."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 152,
                        "name": "Erschütternder Schlag",
                        "icon": "ability_thunderbolt",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Betäubt 5 Sek. lang den Gegner und verursacht 10 Schaden (basierend auf Angriffskraft).",
                                "cost": "15 Wut",
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 149,
                        "name": "Auf die Kehle zielen",
                        "icon": "ability_warrior_shieldbash",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verleiht Eurer Fähigkeit 'Schildhieb' und 'Heldenhafter Wurf' eine Chance von 50%, das Ziel 3 Sek. lang verstummen zu lassen und erhöht den Schaden Eurer Fähigkeit 'Schildschlag' um 5%."
                            },
                            {
                                "description": "Verleiht Eurer Fähigkeit 'Schildhieb' und 'Heldenhafter Wurf' eine Chance von 100%, das Ziel 3 Sek. lang verstummen zu lassen und erhöht den Schaden Eurer Fähigkeit 'Schildschlag' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 702,
                        "name": "Einhandwaffen-Spezialisierung",
                        "icon": "inv_sword_20",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den körperlichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 2%."
                            },
                            {
                                "description": "Erhöht den körperlichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 4%."
                            },
                            {
                                "description": "Erhöht den körperlichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 6%."
                            },
                            {
                                "description": "Erhöht den körperlichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 8%."
                            },
                            {
                                "description": "Erhöht den körperlichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1652,
                        "name": "Verbesserte Verteidigungshaltung",
                        "icon": "ability_warrior_defensivestance",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "In der Verteidigungshaltung wird jeglicher erlittene Zauberschaden um 3% verringert. Wenn Ihr einen Angriff blockt, pariert oder ihm ausweicht, besteht eine Chance von 50%, dass Ihr wütend werdet und Euer verursachter körperlicher Schaden 12 Sek. lang um 5% erhöht wird."
                            },
                            {
                                "description": "In der Verteidigungshaltung wird jeglicher erlittene Zauberschaden um 6% verringert. Wenn Ihr einen Angriff blockt, pariert oder ihm ausweicht, besteht eine Chance von 100%, dass Ihr wütend werdet und Euer verursachter körperlicher Schaden 12 Sek. lang um 10% erhöht wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 148,
                        "name": "Wachsamkeit",
                        "icon": "ability_warrior_vigilance",
                        "x": 1,
                        "y": 6,
                        "req": 152,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "description": "Fokussiert Euren schützenden Blick auf ein Gruppen- oder Schlachtzugsmitglied, verringert so seinen erlittenen Schaden um 3% und überträgt -10% seiner erzeugten Bedrohung auf Euch. Zusätzlich wird jedes Mal, wenn das Ziel von einem Angriff getroffen wird, die Abklingzeit Eurer Fähigkeit 'Spott' beendet. Hält 30 Min. lang an. Dieser Effekt kann nur auf jeweils einem Ziel wirken."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1660,
                        "name": "Fokussierte Wut",
                        "icon": "ability_warrior_focusedrage",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Wutkosten aller Angriffsfähigkeiten um 1."
                            },
                            {
                                "description": "Verringert die Wutkosten aller Angriffsfähigkeiten um 2."
                            },
                            {
                                "description": "Verringert die Wutkosten aller Angriffsfähigkeiten um 3."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1653,
                        "name": "Vitalität",
                        "icon": "inv_helmet_21",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Stärke um 2%, Eure Ausdauer um 3% und Eure Waffenkunde um 2."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 4%, Eure Ausdauer um 6% und Eure Waffenkunde um 4."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 6%, Eure Ausdauer um 9% und Eure Waffenkunde um 6."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1870,
                        "name": "Sicherung",
                        "icon": "ability_warrior_safeguard",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verringert den erlittenen Schaden des von Eurer Fähigkeit 'Einschreiten' betroffenen Ziels 6 Sek. lang um 15%."
                            },
                            {
                                "description": "Verringert den erlittenen Schaden des von Eurer Fähigkeit 'Einschreiten' betroffenen Ziels 6 Sek. lang um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2236,
                        "name": "Kriegstreiber",
                        "icon": "ability_warrior_warbringer",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten 'Sturmangriff', 'Abfangen' und 'Einschreiten' sind nun während des Kampfes und in jeder Haltung nutzbar. Zusätzlich entfernt Eure Fähigkeit 'Einschreiten' jegliche bewegungseinschränkenden Effekte."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1666,
                        "name": "Verwüsten",
                        "icon": "inv_sword_11",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "5 Meter Reichweite",
                                "cost": "15 Wut",
                                "description": "Zerreißt die Rüstung des Ziels und ruft den Effekt 'Rüstung zerreißen' hervor. Verursacht zusätzlich pro Stapel dieses Effektes auf dem Ziel 120% Waffenschaden plus 58. 'Rüstung zerreißen' ist bis zu 5-mal stapelbar."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1893,
                        "name": "Kritisches Blocken",
                        "icon": "ability_warrior_criticalblock",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Jedes erfolgreiche Blocken hat eine Chance von 20%, das Doppelte der normalen Menge zu blocken und erhöht Eure Chance, mit Eurer Fähigkeit 'Schildschlag' einen kritischen Treffer zu erzielen, um zusätzliche 5%."
                            },
                            {
                                "description": "Jedes erfolgreiche Blocken hat eine Chance von 40%, das Doppelte der normalen Menge zu blocken und erhöht Eure Chance, mit Eurer Fähigkeit 'Schildschlag' einen kritischen Treffer zu erzielen, um zusätzliche 10%."
                            },
                            {
                                "description": "Jedes erfolgreiche Blocken hat eine Chance von 60%, das Doppelte der normalen Menge zu blocken und erhöht Eure Chance, mit Eurer Fähigkeit 'Schildschlag' einen kritischen Treffer zu erzielen, um zusätzliche 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1871,
                        "name": "Schwert und Schild",
                        "icon": "ability_warrior_swordandboard",
                        "x": 1,
                        "y": 9,
                        "req": 1666,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance von 'Verwüsten' um 5%, und wenn Eure Fähigkeit 'Verwüsten' oder 'Rache' Schaden verursacht, besteht eine Chance von 10%, dass die Abklingzeit von 'Schildschlag' abgeschlossen und die Kosten der Fähigkeit 5 Sek. lang um 100% verringert werden."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance von 'Verwüsten' um 10%, und wenn Eure Fähigkeit 'Verwüsten' oder 'Rache' Schaden verursacht, besteht eine Chance von 20%, dass die Abklingzeit von 'Schildschlag' abgeschlossen und die Kosten der Fähigkeit 5 Sek. lang um 100% verringert werden."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance von 'Verwüsten' um 15%, und wenn Eure Fähigkeit 'Verwüsten' oder 'Rache' Schaden verursacht, besteht eine Chance von 30%, dass die Abklingzeit von 'Schildschlag' abgeschlossen und die Kosten der Fähigkeit 5 Sek. lang um 100% verringert werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2246,
                        "name": "Schadensschild",
                        "icon": "inv_shield_31",
                        "x": 2,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Jedes Mal, wenn Ihr Schaden durch einen Nahkampfangriff erleidet oder ihn blockt, verursacht Ihr Schaden in Höhe von 10% Eures Blockwerts."
                            },
                            {
                                "description": "Jedes Mal, wenn Ihr Schaden durch einen Nahkampfangriff erleidet oder ihn blockt, verursacht Ihr Schaden in Höhe von 20% Eures Blockwerts."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1872,
                        "name": "Schockwelle",
                        "icon": "ability_warrior_shockwave",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "cost": "15 Wut",
                                "description": "Schickt dem Krieger eine Druckwelle voraus, die 20 Schaden verursacht (basierend auf Angriffskraft) und in einem kegelförmigen Bereich von 10 Metern alle gegnerischen Ziele 4 Sek. lang betäubt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "483": {
        "name": "Glyphe 'Schlachtruf'",
        "id": "483",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "43395",
        "spellKey": "58095",
        "spellId": "58095",
        "prettyName": "",
        "typeOrder": 2
    },
    "484": {
        "name": "Glyphe 'Blutrausch'",
        "id": "484",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "43396",
        "spellKey": "58096",
        "spellId": "58096",
        "prettyName": "",
        "typeOrder": 2
    },
    "485": {
        "name": "Glyphe 'Sturmangriff'",
        "id": "485",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "43397",
        "spellKey": "58097",
        "spellId": "58097",
        "prettyName": "",
        "typeOrder": 2
    },
    "486": {
        "name": "Glyphe 'Spöttischer Schlag'",
        "id": "486",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "43398",
        "spellKey": "58099",
        "spellId": "58099",
        "prettyName": "",
        "typeOrder": 2
    },
    "487": {
        "name": "Glyphe 'Donnerknall'",
        "id": "487",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "43399",
        "spellKey": "58098",
        "spellId": "58098",
        "prettyName": "",
        "typeOrder": 2
    },
    "488": {
        "name": "Glyphe 'Beständiger Sieg'",
        "id": "488",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "43400",
        "spellKey": "58104",
        "spellId": "58104",
        "prettyName": "",
        "typeOrder": 2
    },
    "489": {
        "name": "Glyphe 'Tödlicher Stoß'",
        "id": "489",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43421",
        "spellKey": "58368",
        "spellId": "58368",
        "prettyName": "",
        "typeOrder": 2
    },
    "490": {
        "name": "Glyphe 'Blutdurst'",
        "id": "490",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43412",
        "spellKey": "58369",
        "spellId": "58369",
        "prettyName": "",
        "typeOrder": 2
    },
    "491": {
        "name": "Glyphe 'Schneller Sturmangriff'",
        "id": "491",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43413",
        "spellKey": "58355",
        "spellId": "58355",
        "prettyName": "",
        "typeOrder": 2
    },
    "492": {
        "name": "Glyphe 'Spalten'",
        "id": "492",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43414",
        "spellKey": "58366",
        "spellId": "58366",
        "prettyName": "",
        "typeOrder": 2
    },
    "493": {
        "name": "Glyphe 'Verwüsten'",
        "id": "493",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43415",
        "spellKey": "58388",
        "spellId": "58388",
        "prettyName": "",
        "typeOrder": 2
    },
    "494": {
        "name": "Glyphe 'Hinrichten'",
        "id": "494",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43416",
        "spellKey": "58367",
        "spellId": "58367",
        "prettyName": "",
        "typeOrder": 2
    },
    "495": {
        "name": "Glyphe 'Kniesehne'",
        "id": "495",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43417",
        "spellKey": "58372",
        "spellId": "58372",
        "prettyName": "",
        "typeOrder": 2
    },
    "496": {
        "name": "Glyphe 'Heldenhafter Stoß'",
        "id": "496",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43418",
        "spellKey": "58357",
        "spellId": "58357",
        "prettyName": "",
        "typeOrder": 2
    },
    "497": {
        "name": "Glyphe 'Einschreiten'",
        "id": "497",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43419",
        "spellKey": "58377",
        "spellId": "58377",
        "prettyName": "",
        "typeOrder": 2
    },
    "498": {
        "name": "Glyphe 'Barbarische Beleidigungen'",
        "id": "498",
        "type": 0,
        "description": "Your Mocking Blow ability generates 100% additional threat.",
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43420",
        "spellKey": "58365",
        "spellId": "58365",
        "prettyName": "",
        "typeOrder": 2
    },
    "499": {
        "name": "Glyphe 'Überwältigen'",
        "id": "499",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43422",
        "spellKey": "58386",
        "spellId": "58386",
        "prettyName": "",
        "typeOrder": 2
    },
    "500": {
        "name": "Glyphe 'Verwunden'",
        "id": "500",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43423",
        "spellKey": "58385",
        "spellId": "58385",
        "prettyName": "",
        "typeOrder": 2
    },
    "501": {
        "name": "Glyphe 'Rache'",
        "id": "501",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43424",
        "spellKey": "58364",
        "spellId": "58364",
        "prettyName": "",
        "typeOrder": 2
    },
    "502": {
        "name": "Glyphe 'Blocken'",
        "id": "502",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43425",
        "spellKey": "58375",
        "spellId": "58375",
        "prettyName": "",
        "typeOrder": 2
    },
    "503": {
        "name": "Glyphe 'Letztes Gefecht'",
        "id": "503",
        "type": 0,
        "description": "Reduces the cooldown of your Last Stand ability by 1 min.",
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43426",
        "spellKey": "58376",
        "spellId": "58376",
        "prettyName": "",
        "typeOrder": 2
    },
    "504": {
        "name": "Glyphe 'Rüstung zerreißen'",
        "id": "504",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43427",
        "spellKey": "58387",
        "spellId": "58387",
        "prettyName": "",
        "typeOrder": 2
    },
    "505": {
        "name": "Glyphe 'Weitreichende Stöße'",
        "id": "505",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43428",
        "spellKey": "58384",
        "spellId": "58384",
        "prettyName": "",
        "typeOrder": 2
    },
    "506": {
        "name": "Glyphe 'Spott'",
        "id": "506",
        "type": 0,
        "description": "Increases the chance for your Taunt ability to succeed by 8%.",
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43429",
        "spellKey": "58353",
        "spellId": "58353",
        "prettyName": "",
        "typeOrder": 2
    },
    "507": {
        "name": "Glyphe 'Nachhallende Kraft'",
        "id": "507",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43430",
        "spellKey": "58356",
        "spellId": "58356",
        "prettyName": "",
        "typeOrder": 2
    },
    "508": {
        "name": "Glyphe 'Siegesrausch'",
        "id": "508",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43431",
        "spellKey": "58382",
        "spellId": "58382",
        "prettyName": "",
        "typeOrder": 2
    },
    "509": {
        "name": "Glyphe 'Wirbelwind'",
        "id": "509",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "43432",
        "spellKey": "58370",
        "spellId": "58370",
        "prettyName": "",
        "typeOrder": 2
    },
    "762": {
        "name": "Glyphe 'Klingensturm'",
        "id": "762",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "45790",
        "spellKey": "63324",
        "spellId": "63324",
        "prettyName": "",
        "typeOrder": 2
    },
    "763": {
        "name": "Glyphe 'Schockwelle'",
        "id": "763",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "45792",
        "spellKey": "63325",
        "spellId": "63325",
        "prettyName": "",
        "typeOrder": 2
    },
    "764": {
        "name": "Glyphe 'Wachsamkeit'",
        "id": "764",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "45793",
        "spellKey": "63326",
        "spellId": "63326",
        "prettyName": "",
        "typeOrder": 2
    },
    "765": {
        "name": "Glyphe 'Wütende Regeneration'",
        "id": "765",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "45794",
        "spellKey": "63327",
        "spellId": "63327",
        "prettyName": "",
        "typeOrder": 2
    },
    "766": {
        "name": "Glyphe 'Zauberreflexion'",
        "id": "766",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "45795",
        "spellKey": "63328",
        "spellId": "63328",
        "prettyName": "",
        "typeOrder": 2
    },
    "767": {
        "name": "Glyphe 'Schildwall'",
        "id": "767",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarrior",
        "itemId": "45797",
        "spellKey": "63329",
        "spellId": "63329",
        "prettyName": "",
        "typeOrder": 2
    },
    "851": {
        "name": "Glyph of Command",
        "id": "851",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarrior",
        "itemId": "49084",
        "spellKey": "68164",
        "spellId": "68164",
        "prettyName": "",
        "typeOrder": 2
    }
}]
}