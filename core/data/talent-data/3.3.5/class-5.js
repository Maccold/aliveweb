{
    "talentData": {
        "characterClass": {
            "classId": 5,
            "name": "Priester",
            "powerType": "MANA",
            "powerTypeId": 0,
            "powerTypeSlug": "mana"
        },
        "talentTrees": [
            {
                "name": "Disziplin",
                "icon": "spell_holy_powerwordshield",
                "backgroundFile": "PriestDiscipline",
                "overlayColor": "#ff7f00",
                "description": "Nutzt Magie, um Verbündete vor Schaden zu schützen und ihre Wunden zu heilen.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": true,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 342,
                        "name": "Unbezwingbarer Wille",
                        "icon": "spell_magic_magearmor",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer auf Euch wirkender Betäubungs-, Furcht- und Stilleeffekte um weitere 6%."
                            },
                            {
                                "description": "Verringert die Dauer auf Euch wirkender Betäubungs-, Furcht- und Stilleeffekte um weitere 12%."
                            },
                            {
                                "description": "Verringert die Dauer auf Euch wirkender Betäubungs-, Furcht- und Stilleeffekte um weitere 18%."
                            },
                            {
                                "description": "Verringert die Dauer auf Euch wirkender Betäubungs-, Furcht- und Stilleeffekte um weitere 24%."
                            },
                            {
                                "description": "Verringert die Dauer auf Euch wirkender Betäubungs-, Furcht- und Stilleeffekte um weitere 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1898,
                        "name": "Zwillingsdisziplinen",
                        "icon": "spell_holy_sealofvengeance",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer Spontanzauber um 1%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer Spontanzauber um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer Spontanzauber um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer Spontanzauber um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer Spontanzauber um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 352,
                        "name": "Schweigsame Entschlossenheit",
                        "icon": "spell_nature_manaregentotem",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die von Euren Heilig- und Disziplinzaubern verursachte Bedrohung um 7% und die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 10%."
                            },
                            {
                                "description": "Verringert die von Euren Heilig- und Disziplinzaubern verursachte Bedrohung um 14% und die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 20%."
                            },
                            {
                                "description": "Verringert die von Euren Heilig- und Disziplinzaubern verursachte Bedrohung um 20% und die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 346,
                        "name": "Verbessertes inneres Feuer",
                        "icon": "spell_holy_innerfire",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt Eures Zaubers 'Inneres Feuer' um 15% und erhöht die Anzahl an Aufladungen um 4."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Zaubers 'Inneres Feuer' um 30% und erhöht die Anzahl an Aufladungen um 8."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Zaubers 'Inneres Feuer' um 45% und erhöht die Anzahl an Aufladungen um 12."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 344,
                        "name": "Verbessertes Machtwort: Seelenstärke",
                        "icon": "spell_holy_wordfortitude",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt Eurer Zauber 'Machtwort: Seelenstärke' und 'Gebet der Seelenstärke' um 15% und erhöht Eure gesamte Ausdauer um 2%."
                            },
                            {
                                "description": "Erhöht den Effekt Eurer Zauber 'Machtwort: Seelenstärke' und 'Gebet der Seelenstärke' um 30% und erhöht Eure gesamte Ausdauer um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 321,
                        "name": "Märtyrertum",
                        "icon": "spell_nature_tranquility",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 50%, den Effekt 'Fokussiertes Zauberwirken' zu erhalten, der 6 Sek. lang anhält, nachdem Ihr Opfer eines kritischen Nahkampf- oder Distanzangriffs wart. Der Effekt 'Fokussiertes Zauberwirken' verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Priesterzaubern und die Dauer von Unterbrechungseffekten um 10%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 100%, den Effekt 'Fokussiertes Zauberwirken' zu erhalten, der 6 Sek. lang anhält, nachdem Ihr Opfer eines kritischen Nahkampf- oder Distanzangriffs wart. Der Effekt 'Fokussiertes Zauberwirken' verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Priesterzaubern und die Dauer von Unterbrechungseffekten um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 347,
                        "name": "Meditation",
                        "icon": "spell_nature_sleep",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Ermöglicht, dass 17% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            },
                            {
                                "description": "Ermöglicht, dass 33% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            },
                            {
                                "description": "Ermöglicht, dass 50% Eurer Manaregeneration während des Zauberwirkens weiterläuft."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 348,
                        "name": "Innerer Fokus",
                        "icon": "spell_frost_windwalkon",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Bei Aktivierung werden die Manakosten Eures nächsten Zaubers um 100% verringert und seine Chance auf einen kritischen Effekt um 25% erhöht, sofern er in der Lage ist, einen kritischen Effekt zu verursachen."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 343,
                        "name": "Verbessertes Machtwort: Schild",
                        "icon": "spell_holy_powerwordshield",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurem Zauber 'Machtwort: Schild' absorbierten Schaden um 5%."
                            },
                            {
                                "description": "Erhöht den von Eurem Zauber 'Machtwort: Schild' absorbierten Schaden um 10%."
                            },
                            {
                                "description": "Erhöht den von Eurem Zauber 'Machtwort: Schild' absorbierten Schaden um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1769,
                        "name": "Absolution",
                        "icon": "spell_holy_absolution",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Magiebannung', 'Krankheit heilen', 'Krankheit aufheben' und 'Massenbannung' um 5%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Magiebannung', 'Krankheit heilen', 'Krankheit aufheben' und 'Massenbannung' um 10%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Magiebannung', 'Krankheit heilen', 'Krankheit aufheben' und 'Massenbannung' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 341,
                        "name": "Mentale Beweglichkeit",
                        "icon": "ability_hibernation",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Sofortzauber um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Sofortzauber um 7%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Sofortzauber um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 350,
                        "name": "Verbesserter Manabrand",
                        "icon": "spell_shadow_manaburn",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Die Zauberzeit Eures Zaubers 'Manabrand' wird um 0.5 Sek. verringert."
                            },
                            {
                                "description": "Die Zauberzeit Eures Zaubers 'Manabrand' wird um 1 Sek. verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2268,
                        "name": "Reflektierender Schild",
                        "icon": "spell_holy_powerwordshield",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "22% des Schadens, den Ihr durch 'Machtwort: Schild' absorbiert, werden auf den Angreifer zurückgeworfen. Dieser Schaden verursacht keine Bedrohung."
                            },
                            {
                                "description": "45% des Schadens, den Ihr durch 'Machtwort: Schild' absorbiert, werden auf den Angreifer zurückgeworfen. Dieser Schaden verursacht keine Bedrohung."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1201,
                        "name": "Mentale Stärke",
                        "icon": "spell_nature_enchantarmor",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 3%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 6%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 9%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 12%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Intelligenz um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 351,
                        "name": "Seelenwehr",
                        "icon": "spell_holy_pureofheart",
                        "x": 2,
                        "y": 4,
                        "req": 343,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Machtwort: Schild' um 4 Sek. und verringert seine Manakosten um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1771,
                        "name": "Fokussierte Macht",
                        "icon": "spell_shadow_focusedpower",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Zauberschaden und Eure Heilung um 2%. Zusätzlich wird die Zauberzeit von 'Massenbannung' um 0.5 Sek. verringert."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden und Eure Heilung um 4%. Zusätzlich wird die Zauberzeit von 'Massenbannung' um 1 Sek. verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1772,
                        "name": "Erleuchtung",
                        "icon": "spell_arcane_mindmastery",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 2% und Euer Zaubertempo um 2%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 4% und Euer Zaubertempo um 4%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 6% und Euer Zaubertempo um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1858,
                        "name": "Fokussierter Wille",
                        "icon": "spell_arcane_focusedpower",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber um 1%. Nach einem erlittenen kritischen Treffer wird Euch der Effekt 'Fokussierter Wille' gewährt, wodurch jeglicher erlittene Schaden um 2% verringert und erhaltene Heileffekte um 3% erhöht werden. Bis zu 3-mal stapelbar. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber um 2%. Nach einem erlittenen kritischen Treffer wird Euch der Effekt 'Fokussierter Wille' gewährt, wodurch jeglicher erlittene Schaden um 3% verringert und erhaltene Heileffekte um 4% erhöht werden. Bis zu 3-mal stapelbar. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber um 3%. Nach einem erlittenen kritischen Treffer wird Euch der Effekt 'Fokussierter Wille' gewährt, wodurch jeglicher erlittene Schaden um 4% verringert und erhaltene Heileffekte um 5% erhöht werden. Bis zu 3-mal stapelbar. Hält 8 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 322,
                        "name": "Seele der Macht",
                        "icon": "spell_holy_powerinfusion",
                        "x": 1,
                        "y": 6,
                        "req": 1201,
                        "ranks": [
                            {
                                "description": "Erfüllt das Ziel mit Macht, erhöht das Zaubertempo um 20% und senkt die Manakosten jeglicher Zauber um 20%. Hält 15 Sek. lang an.",
                                "cost": "16% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1773,
                        "name": "Verbesserte Blitzheilung",
                        "icon": "spell_holy_chastise",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eures Zaubers 'Blitzheilung' um 5% und erhöht seine Chance auf einen kritischen Effekt bei befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten Eures Zaubers 'Blitzheilung' um 10% und erhöht seine Chance auf einen kritischen Effekt bei befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 7%."
                            },
                            {
                                "description": "Verringert die Manakosten Eures Zaubers 'Blitzheilung' um 15% und erhöht seine Chance auf einen kritischen Effekt bei befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2235,
                        "name": "Erneuerte Hoffnung",
                        "icon": "spell_holy_holyprotection",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber 'Blitzheilung', 'Große Heilung' und 'Sühne' (Heilung) um 2%, wenn das Ziel von 'Geschwächte Seele' betroffen ist. Zudem besteht beim Wirken von 'Machtwort: Schild' eine Chance von 50%, jeglichen erlittenen Schaden aller Gruppen- und Schlachtzugsmitglieder 1 Min. lang um 3% zu verringern. Dieser Effekt hat eine Abklingzeit von 15 Sek."
                            },
                            {
                                "description": "Erhöht die Chance auf kritische Effekte Eurer Zauber 'Blitzheilung', 'Große Heilung' und 'Sühne' (Heilung) um 4%, wenn das Ziel von 'Geschwächte Seele' betroffen ist. Zudem besteht beim Wirken von 'Machtwort: Schild' eine Chance von 100%, jeglichen erlittenen Schaden aller Gruppen- und Schlachtzugsmitglieder 1 Min. lang um 3% zu verringern. Dieser Effekt hat eine Abklingzeit von 15 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1896,
                        "name": "Euphorie",
                        "icon": "spell_holy_rapture",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Wenn der Effekt Eures Zaubers 'Machtwort: Schild' gebannt oder durch absorbierten Schaden beendet wird, gewinnt Ihr sofort 1,5% Eures maximalen Manas. Es besteht eine Chance von 33%, dass ein schildgeschütztes Ziel 2% seines gesamten Manas, 8 Wut, 16 Energie oder 32 Runenmacht gewinnt. Dieser Effekt kann nur ein Mal alle 12 Sek. auftreten."
                            },
                            {
                                "description": "Wenn der Effekt Eures Zaubers 'Machtwort: Schild' gebannt oder durch absorbierten Schaden beendet wird, gewinnt Ihr sofort 2% Eures maximalen Manas. Es besteht eine Chance von 66%, dass ein schildgeschütztes Ziel 2% seines gesamten Manas, 8 Wut, 16 Energie oder 32 Runenmacht gewinnt. Dieser Effekt kann nur ein Mal alle 12 Sek. auftreten."
                            },
                            {
                                "description": "Wenn der Effekt Eures Zaubers 'Machtwort: Schild' gebannt oder durch absorbierten Schaden beendet wird, gewinnt Ihr sofort 2,5% Eures maximalen Manas. Es besteht eine Chance von 100%, dass ein schildgeschütztes Ziel 2% seines gesamten Manas, 8 Wut, 16 Energie oder 32 Runenmacht gewinnt. Dieser Effekt kann nur ein Mal alle 12 Sek. auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1894,
                        "name": "Aspiration",
                        "icon": "spell_holy_aspiration",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Innerer Fokus', 'Seele der Macht', 'Schmerzunterdrückung' und 'Sühne' um 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Zauber 'Innerer Fokus', 'Seele der Macht', 'Schmerzunterdrückung' und 'Sühne' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1895,
                        "name": "Göttliche Aegis",
                        "icon": "spell_holy_devineaegis",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Kritische Heilungen hüllen das Ziel in einen schützenden Schild, der Schaden in Höhe von 10% des geheilten Wertes absorbiert. Hält 12 Sek. lang an."
                            },
                            {
                                "description": "Kritische Heilungen hüllen das Ziel in einen schützenden Schild, der Schaden in Höhe von 20% des geheilten Wertes absorbiert. Hält 12 Sek. lang an."
                            },
                            {
                                "description": "Kritische Heilungen hüllen das Ziel in einen schützenden Schild, der Schaden in Höhe von 30% des geheilten Wertes absorbiert. Hält 12 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1774,
                        "name": "Schmerzunterdrückung",
                        "icon": "spell_holy_painsupression",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verringert die Bedrohung eines befreundeten Ziels sofort um 5%, 8 Sek. lang wird jeglicher erlittene Schaden um 40% verringert und die Chance, Bannzaubern zu widerstehen, um 65% erhöht.",
                                "cost": "8% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1901,
                        "name": "Barmherzigkeit",
                        "icon": "spell_holy_hopeandgrace",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Blitzheilung', 'Große Heilung' und 'Sühne' haben eine Chance von 50%, das Ziel mit Barmherzigkeit zu segnen, die seine durch den Priester erhaltene Heilung um 3% erhöht. Dieser Effekt ist bis zu 3-mal stapelbar und hält 15 Sek. lang an. 'Barmherzigkeit' kann jeweils nur auf ein Ziel wirken."
                            },
                            {
                                "description": "Eure Zauber 'Blitzheilung', 'Große Heilung' und 'Sühne' haben eine Chance von 100%, das Ziel mit Barmherzigkeit zu segnen, die seine durch den Priester erhaltene Heilung um 3% erhöht. Dieser Effekt ist bis zu 3-mal stapelbar und hält 15 Sek. lang an. 'Barmherzigkeit' kann jeweils nur auf ein Ziel wirken."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1202,
                        "name": "Zeit schinden",
                        "icon": "spell_holy_borrowedtime",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Gewährt Euch nach dem Wirken von 'Machtwort: Schild' 5% Zaubertempo und erhöht den von Eurem 'Machtwort: Schild' absorbierten Schaden um 8% Eurer Zaubermacht."
                            },
                            {
                                "description": "Gewährt Euch nach dem Wirken von 'Machtwort: Schild' 10% Zaubertempo und erhöht den von Eurem 'Machtwort: Schild' absorbierten Schaden um 16% Eurer Zaubermacht."
                            },
                            {
                                "description": "Gewährt Euch nach dem Wirken von 'Machtwort: Schild' 15% Zaubertempo und erhöht den von Eurem 'Machtwort: Schild' absorbierten Schaden um 24% Eurer Zaubermacht."
                            },
                            {
                                "description": "Gewährt Euch nach dem Wirken von 'Machtwort: Schild' 20% Zaubertempo und erhöht den von Eurem 'Machtwort: Schild' absorbierten Schaden um 32% Eurer Zaubermacht."
                            },
                            {
                                "description": "Gewährt Euch nach dem Wirken von 'Machtwort: Schild' 25% Zaubertempo und erhöht den von Eurem 'Machtwort: Schild' absorbierten Schaden um 40% Eurer Zaubermacht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1897,
                        "name": "Sühne",
                        "icon": "spell_holy_penance",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Kanalisiert",
                                "cooldown": "12 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite",
                                "cost": "16% des Grundmanas",
                                "description": "Schleudert eine Salve heiligen Lichts auf das Ziel, die an einem Feind 240 Heiligschaden verursacht oder ein freundliches Ziel sofort um 670 bis 756 heilt sowie 2 Sek. lang alle 1 Sek."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Heilig",
                "icon": "spell_holy_guardianspirit",
                "backgroundFile": "PriestHoly",
                "overlayColor": "#9999ff",
                "description": "Ein vielseitiger Heiler, der den von Gruppenmitgliedern oder Einzelnen erlittenen Schaden rückgängig machen und sogar noch nach seinem Tode heilen kann.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": true,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 410,
                        "name": "Heilfokus",
                        "icon": "spell_holy_healingfocus",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Heilzaubern um 35%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von Heilzaubern um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 406,
                        "name": "Verbesserte Erneuerung",
                        "icon": "spell_holy_renew",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Euren Zauber 'Erneuerung' geheilten Wert um 5%."
                            },
                            {
                                "description": "Erhöht den durch Euren Zauber 'Erneuerung' geheilten Wert um 10%."
                            },
                            {
                                "description": "Erhöht den durch Euren Zauber 'Erneuerung' geheilten Wert um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 401,
                        "name": "Macht des Glaubens",
                        "icon": "spell_holy_sealofsalvation",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 1%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 2%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 3%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 4%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 411,
                        "name": "Zauberschutz",
                        "icon": "spell_holy_spellwarding",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Zauberschaden um 2%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Zauberschaden um 4%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Zauberschaden um 6%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Zauberschaden um 8%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Zauberschaden um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1181,
                        "name": "Göttlicher Furor",
                        "icon": "spell_holy_sealofwrath",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilen' und 'Große Heilung' um 0.1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilen' und 'Große Heilung' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilen' und 'Große Heilung' um 0.3 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilen' und 'Große Heilung' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilen' und 'Große Heilung' um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 442,
                        "name": "Verzweifeltes Gebet",
                        "icon": "spell_holy_restoration",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "cost": "21% des Grundmanas",
                                "description": "Heilt den Zaubernden sofort um 263 bis 325."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1636,
                        "name": "Gesegnete Erholung",
                        "icon": "spell_holy_blessedrecovery",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Heilt Euch nach dem Erleiden eines kritischen Nahkampf- oder Distanzangrifftreffers im Verlauf von 6 Sek. um 5% des zugefügten Schadens. Zusätzlich während dieser Dauer erlittene kritische Treffer erhöhen die erhaltene Heilung."
                            },
                            {
                                "description": "Heilt Euch nach dem Erleiden eines kritischen Nahkampf- oder Distanzangrifftreffers im Verlauf von 6 Sek. um 10% des zugefügten Schadens. Zusätzlich während dieser Dauer erlittene kritische Treffer erhöhen die erhaltene Heilung."
                            },
                            {
                                "description": "Heilt Euch nach dem Erleiden eines kritischen Nahkampf- oder Distanzangrifftreffers im Verlauf von 6 Sek. um 15% des zugefügten Schadens. Zusätzlich während dieser Dauer erlittene kritische Treffer erhöhen die erhaltene Heilung."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 361,
                        "name": "Inspiration",
                        "icon": "spell_holy_layonhands",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden Eures Ziels 15 Sek. lang um 3%, nachdem dieses einen kritischen Effekt von Eurem Zauber 'Blitzheilung', 'Heilen', 'Große Heilung', 'Verbindende Heilung', 'Sühne', 'Gebet der Besserung', 'Gebet der Heilung' oder 'Kreis der Heilung' erhalten hat."
                            },
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden Eures Ziels 15 Sek. lang um 7%, nachdem dieses einen kritischen Effekt von Eurem Zauber 'Blitzheilung', 'Heilen', 'Große Heilung', 'Verbindende Heilung', 'Sühne', 'Gebet der Besserung', 'Gebet der Heilung' oder 'Kreis der Heilung' erhalten hat."
                            },
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden Eures Ziels 15 Sek. lang um 10%, nachdem dieses einen kritischen Effekt von Eurem Zauber 'Blitzheilung', 'Heilen', 'Große Heilung', 'Verbindende Heilung', 'Sühne', 'Gebet der Besserung', 'Gebet der Heilung' oder 'Kreis der Heilung' erhalten hat."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1635,
                        "name": "Heilige Reichweite",
                        "icon": "spell_holy_purify",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer' sowie den Radius der Zauber 'Gebet der Heilung', 'Heilige Nova', 'Gotteshymne' und 'Kreis der Heilung' um 10%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer' sowie den Radius der Zauber 'Gebet der Heilung', 'Heilige Nova', 'Gotteshymne' und 'Kreis der Heilung' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 408,
                        "name": "Verbesserte Heilung",
                        "icon": "spell_holy_heal02",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Geringes Heilen', 'Heilen', 'Große Heilung', 'Gotteshymne' und 'Sühne' um 5%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Geringes Heilen', 'Heilen', 'Große Heilung', 'Gotteshymne' und 'Sühne' um 10%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Geringes Heilen', 'Heilen', 'Große Heilung', 'Gotteshymne' und 'Sühne' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 403,
                        "name": "Sengendes Licht",
                        "icon": "spell_holy_searinglightpriest",
                        "x": 2,
                        "y": 3,
                        "req": 1181,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilige Nova' und 'Sühne' um 5%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Göttliche Pein', 'Heiliges Feuer', 'Heilige Nova' und 'Sühne' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 413,
                        "name": "Heilende Gebete",
                        "icon": "spell_holy_prayerofhealing02",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Gebet der Heilung' und 'Gebet der Besserung' um 10%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Gebet der Heilung' und 'Gebet der Besserung' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1561,
                        "name": "Geist der Erlösung",
                        "icon": "inv_enchant_essenceeternallarge",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die gesamte Willenskraft um 5% und beim Tod verwandelt sich der Priester 15 Sek. lang in einen Geist der Erlösung. Der Geist kann sich weder bewegen oder angreifen, noch kann er das Ziel von Zaubern oder Effekten sein. In dieser Gestalt kann der Priester jeglichen Heilzauber ohne irgendwelche Kosten wirken. Nach Ablauf der Verwandlung stirbt der Priester."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 402,
                        "name": "Geistige Führung",
                        "icon": "spell_holy_spiritualguidence",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Zaubermacht um einen Betrag, der 5% Eurer gesamten Willenskraft entspricht."
                            },
                            {
                                "description": "Erhöht die Zaubermacht um einen Betrag, der 10% Eurer gesamten Willenskraft entspricht."
                            },
                            {
                                "description": "Erhöht die Zaubermacht um einen Betrag, der 15% Eurer gesamten Willenskraft entspricht."
                            },
                            {
                                "description": "Erhöht die Zaubermacht um einen Betrag, der 20% Eurer gesamten Willenskraft entspricht."
                            },
                            {
                                "description": "Erhöht die Zaubermacht um einen Betrag, der 25% Eurer gesamten Willenskraft entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1766,
                        "name": "Woge des Lichts",
                        "icon": "spell_holy_surgeoflight",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Eure kritischen Zaubertreffer haben eine Chance von 25%, dass das nächste Wirken von 'Göttliche Pein' oder 'Blitzheilung' ein Spontanzauber ist und kein Mana kostet, aber keinen kritischen Treffer erzielen kann. Der Effekt hält 10 Sek. lang an."
                            },
                            {
                                "description": "Eure kritischen Zaubertreffer haben eine Chance von 50%, dass das nächste Wirken von 'Göttliche Pein' oder 'Blitzheilung' ein Spontanzauber ist und kein Mana kostet, aber keinen kritischen Treffer erzielen kann. Der Effekt hält 10 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 404,
                        "name": "Spirituelle Heilung",
                        "icon": "spell_nature_moonglow",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Eure Heilzauber geheilten Wert um 2%."
                            },
                            {
                                "description": "Erhöht den durch Eure Heilzauber geheilten Wert um 4%."
                            },
                            {
                                "description": "Erhöht den durch Eure Heilzauber geheilten Wert um 6%."
                            },
                            {
                                "description": "Erhöht den durch Eure Heilzauber geheilten Wert um 8%."
                            },
                            {
                                "description": "Erhöht den durch Eure Heilzauber geheilten Wert um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1768,
                        "name": "Heilige Konzentration",
                        "icon": "spell_holy_fanaticism",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erzielt Ihr mit 'Blitzheilung', 'Große Heilung', 'Verbindende Heilung' oder 'Machterfüllte Erneuerung' einen kritischen Effekt, wird die Manaregeneration durch Eure Willenskraft 8 Sek. lang um 16% erhöht."
                            },
                            {
                                "description": "Erzielt Ihr mit 'Blitzheilung', 'Große Heilung', 'Verbindende Heilung' oder 'Machterfüllte Erneuerung' einen kritischen Effekt, wird die Manaregeneration durch Eure Willenskraft 8 Sek. lang um 32% erhöht."
                            },
                            {
                                "description": "Erzielt Ihr mit 'Blitzheilung', 'Große Heilung', 'Verbindende Heilung' oder 'Machterfüllte Erneuerung' einen kritischen Effekt, wird die Manaregeneration durch Eure Willenskraft 8 Sek. lang um 50% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1637,
                        "name": "Brunnen des Lichts",
                        "icon": "spell_holy_summonlightwell",
                        "x": 1,
                        "y": 6,
                        "req": 1561,
                        "ranks": [
                            {
                                "description": "Erschafft einen heiligen Brunnen des Lichts. Befreundete Spieler können den Brunnen des Lichts anklicken, um im Verlauf von 6 Sek. insgesamt 801 Gesundheit wiederherzustellen. Erlittener Schaden, der 30% Eurer gesamten Gesundheit entspricht, bricht den Effekt ab. Der Brunnen des Lichts bleibt 3 Min. lang bestehen oder bis 10 Aufladungen verbraucht sind.",
                                "cost": "17% des Grundmanas",
                                "castTime": "500",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1765,
                        "name": "Gesegnete Abhärtung",
                        "icon": "spell_holy_blessedresillience",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht die Effektivität Eurer Heilzauber um 1%. Erleidet Ihr einen kritischen Treffer, besteht eine Chance von 20%, dass 6 Sek. lang weitere kritische Treffer gegen Euch verhindert werden."
                            },
                            {
                                "description": "Erhöht die Effektivität Eurer Heilzauber um 2%. Erleidet Ihr einen kritischen Treffer, besteht eine Chance von 40%, dass 6 Sek. lang weitere kritische Treffer gegen Euch verhindert werden."
                            },
                            {
                                "description": "Erhöht die Effektivität Eurer Heilzauber um 3%. Erleidet Ihr einen kritischen Treffer, besteht eine Chance von 60%, dass 6 Sek. lang weitere kritische Treffer gegen Euch verhindert werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2279,
                        "name": "Körper und Geist",
                        "icon": "spell_holy_symbolofhope",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Wenn Ihr 'Machtwort: Schild' wirkt, wird das Bewegungstempo des Ziels 4 Sek. lang um 30% erhöht. Zudem besteht eine Chance von 50%, dass Ihr beim Wirken von 'Krankheit aufheben' auf Euch selbst, zusätzlich zu den Krankheiten einen Gifteffekt entfernt."
                            },
                            {
                                "description": "Wenn Ihr 'Machtwort: Schild' wirkt, wird das Bewegungstempo des Ziels 4 Sek. lang um 60% erhöht. Zudem besteht eine Chance von 100%, dass Ihr beim Wirken von 'Krankheit aufheben' auf Euch selbst, zusätzlich zu den Krankheiten einen Gifteffekt entfernt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1767,
                        "name": "Machtvolle Heilung",
                        "icon": "spell_holy_greaterheal",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure 'Große Heilung' erhält zusätzliche 8% und Eure 'Blitzheilung' und 'Verbindende Heilung' zusätzliche 4% Bonus durch Heilung erhöhende Effekte."
                            },
                            {
                                "description": "Eure 'Große Heilung' erhält zusätzliche 16% und Eure 'Blitzheilung' und 'Verbindende Heilung' zusätzliche 8% Bonus durch Heilung erhöhende Effekte."
                            },
                            {
                                "description": "Eure 'Große Heilung' erhält zusätzliche 24% und Eure 'Blitzheilung' und 'Verbindende Heilung' zusätzliche 12% Bonus durch Heilung erhöhende Effekte."
                            },
                            {
                                "description": "Eure 'Große Heilung' erhält zusätzliche 32% und Eure 'Blitzheilung' und 'Verbindende Heilung' zusätzliche 16% Bonus durch Heilung erhöhende Effekte."
                            },
                            {
                                "description": "Eure 'Große Heilung' erhält zusätzliche 40% und Eure 'Blitzheilung' und 'Verbindende Heilung' zusätzliche 20% Bonus durch Heilung erhöhende Effekte."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1904,
                        "name": "Glücksfall",
                        "icon": "spell_holy_serendipity",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Wenn Ihr 'Verbindende Heilung' oder 'Blitzheilung' wirkt, wird die Zauberzeit Eures nächsten Wirkens von 'Große Heilung' oder 'Gebet der Heilung' um 4% verringert. Bis zu 3-mal stapelbar. Hält 20 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr 'Verbindende Heilung' oder 'Blitzheilung' wirkt, wird die Zauberzeit Eures nächsten Wirkens von 'Große Heilung' oder 'Gebet der Heilung' um 8% verringert. Bis zu 3-mal stapelbar. Hält 20 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr 'Verbindende Heilung' oder 'Blitzheilung' wirkt, wird die Zauberzeit Eures nächsten Wirkens von 'Große Heilung' oder 'Gebet der Heilung' um 12% verringert. Bis zu 3-mal stapelbar. Hält 20 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1902,
                        "name": "Machterfüllte Erneuerung",
                        "icon": "ability_paladin_infusionoflight",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Erneuerung' erhält zusätzliche 5% Eurer Bonusheilungseffekte und heilt das Ziel sofort um 5% des gesamten regelmäßigen Effektes."
                            },
                            {
                                "description": "Euer Zauber 'Erneuerung' erhält zusätzliche 10% Eurer Bonusheilungseffekte und heilt das Ziel sofort um 10% des gesamten regelmäßigen Effektes."
                            },
                            {
                                "description": "Euer Zauber 'Erneuerung' erhält zusätzliche 15% Eurer Bonusheilungseffekte und heilt das Ziel sofort um 15% des gesamten regelmäßigen Effektes."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1815,
                        "name": "Kreis der Heilung",
                        "icon": "spell_holy_circleofrenewal",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Heilt bis zu 5 befreundete Gruppen- oder Schlachtzugsmitglieder innerhalb von 15 Metern um das Ziel um 343 bis 379.",
                                "cost": "21% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1903,
                        "name": "Prüfung des Glaubens",
                        "icon": "spell_holy_testoffaith",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht die erhaltene Heilung von befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 4%."
                            },
                            {
                                "description": "Erhöht die erhaltene Heilung von befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 8%."
                            },
                            {
                                "description": "Erhöht die erhaltene Heilung von befreundeten Zielen, deren Gesundheit 50% oder weniger beträgt, um 12%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1905,
                        "name": "Göttliche Vorsehung",
                        "icon": "spell_holy_divineprovidence",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Zaubern 'Kreis der Heilung', 'Verbindende Heilung', 'Heilige Nova', 'Gebet der Heilung', 'Gotteshymne' und 'Gebet der Besserung' geheilten Wert um 2% und verringert die Abklingzeit von 'Gebet der Besserung' um 6%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Kreis der Heilung', 'Verbindende Heilung', 'Heilige Nova', 'Gebet der Heilung', 'Gotteshymne' und 'Gebet der Besserung' geheilten Wert um 4% und verringert die Abklingzeit von 'Gebet der Besserung' um 12%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Kreis der Heilung', 'Verbindende Heilung', 'Heilige Nova', 'Gebet der Heilung', 'Gotteshymne' und 'Gebet der Besserung' geheilten Wert um 6% und verringert die Abklingzeit von 'Gebet der Besserung' um 18%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Kreis der Heilung', 'Verbindende Heilung', 'Heilige Nova', 'Gebet der Heilung', 'Gotteshymne' und 'Gebet der Besserung' geheilten Wert um 8% und verringert die Abklingzeit von 'Gebet der Besserung' um 24%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Kreis der Heilung', 'Verbindende Heilung', 'Heilige Nova', 'Gebet der Heilung', 'Gotteshymne' und 'Gebet der Besserung' geheilten Wert um 10% und verringert die Abklingzeit von 'Gebet der Besserung' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1911,
                        "name": "Schutzgeist",
                        "icon": "spell_holy_guardianspirit",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Ruft einen Schutzgeist herbei, der ein freundliches Ziel bewacht. Der Geist erhöht die erhaltene Heilung des Ziels um 40% und verhindert, dass das Ziel stirbt, indem er sich selbst opfert. Dieses Opfer beendet den Effekt, heilt das Ziel jedoch um 50% seiner maximalen Gesundheit. Hält 10 Sek. lang an.",
                                "cost": "6% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Schatten",
                "icon": "spell_shadow_shadowwordpain",
                "backgroundFile": "PriestShadow",
                "overlayColor": "#b266cc",
                "description": "Nutzt sinistre Schattenmagien, um Feinde durch Schaden über Zeit auszulöschen.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 465,
                        "name": "Willensentzug",
                        "icon": "spell_shadow_requiem",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 33%, einen Bonus von 100% für Eure Willenskraft zu gewinnen, nachdem Ihr ein Ziel getötet habt, das Erfahrung oder Ehre gewährt. In dieser Zeit regeneriert sich Euer Mana während des Zauberwirkens mit 83% der normalen Geschwindigkeit. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 66%, einen Bonus von 100% für Eure Willenskraft zu gewinnen, nachdem Ihr ein Ziel getötet habt, das Erfahrung oder Ehre gewährt. In dieser Zeit regeneriert sich Euer Mana während des Zauberwirkens mit 83% der normalen Geschwindigkeit. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 100%, einen Bonus von 100% für Eure Willenskraft zu gewinnen, nachdem Ihr ein Ziel getötet habt, das Erfahrung oder Ehre gewährt. In dieser Zeit regeneriert sich Euer Mana während des Zauberwirkens mit 83% der normalen Geschwindigkeit. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2027,
                        "name": "Verbesserter Willensentzug",
                        "icon": "spell_shadow_requiem",
                        "x": 1,
                        "y": 0,
                        "req": 465,
                        "ranks": [
                            {
                                "description": "Die kritischen Treffer Eurer Zauber 'Gedankenschlag' und 'Schattenwort: Tod' haben eine Chance von 100% und die kritischen Treffer Eures Zaubers 'Gedankenschinden' eine Chance von 50%, Eure gesamte Willenskraft um 5% zu erhöhen. Während der Effekt andauert, wird Eure Manaregeneration während des Zauberwirkens auf 17% erhöht. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Die kritischen Treffer Eurer Zauber 'Gedankenschlag' und 'Schattenwort: Tod' haben eine Chance von 100% und die kritischen Treffer Eures Zaubers 'Gedankenschinden' eine Chance von 50%, Eure gesamte Willenskraft um 10% zu erhöhen. Während der Effekt andauert, wird Eure Manaregeneration während des Zauberwirkens auf 33% erhöht. Hält 8 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 462,
                        "name": "Dunkelheit",
                        "icon": "spell_shadow_twilight",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Schattenzauber-Schaden um 2%."
                            },
                            {
                                "description": "Erhöht Euren Schattenzauber-Schaden um 4%."
                            },
                            {
                                "description": "Erhöht Euren Schattenzauber-Schaden um 6%."
                            },
                            {
                                "description": "Erhöht Euren Schattenzauber-Schaden um 8%."
                            },
                            {
                                "description": "Erhöht Euren Schattenzauber-Schaden um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 466,
                        "name": "Schattenaffinität",
                        "icon": "spell_shadow_shadowward",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die von Euren Schattenzaubern verursachte Bedrohung um 8% und Ihr gewinnt 5% Eures Grundmanas, wenn Eure Zauber 'Schattenwort: Schmerz' oder 'Vampirberührung' gebannt werden."
                            },
                            {
                                "description": "Verringert die von Euren Schattenzaubern verursachte Bedrohung um 16% und Ihr gewinnt 10% Eures Grundmanas, wenn Eure Zauber 'Schattenwort: Schmerz' oder 'Vampirberührung' gebannt werden."
                            },
                            {
                                "description": "Verringert die von Euren Schattenzaubern verursachte Bedrohung um 25% und Ihr gewinnt 15% Eures Grundmanas, wenn Eure Zauber 'Schattenwort: Schmerz' oder 'Vampirberührung' gebannt werden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 482,
                        "name": "Verbessertes Schattenwort: Schmerz",
                        "icon": "spell_shadow_shadowwordpain",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Schattenwort: Schmerz' um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Schattenwort: Schmerz' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 463,
                        "name": "Schattenfokus",
                        "icon": "spell_shadow_burningspirit",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Schattenzaubern zu treffen um 1% und verringert die Manakosten Eurer Schattenzauber um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Schattenzaubern zu treffen um 2% und verringert die Manakosten Eurer Schattenzauber um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Schattenzaubern zu treffen um 3% und verringert die Manakosten Eurer Schattenzauber um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 542,
                        "name": "Verbesserter psychischer Schrei",
                        "icon": "spell_shadow_psychicscream",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Psychischer Schrei' um 2 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Psychischer Schrei' um 4 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 481,
                        "name": "Verbesserter Gedankenschlag",
                        "icon": "spell_shadow_unholyfrenzy",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 0.5 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 20%, die erhaltene Heilung des Ziels 10 Sek. lang um 20% zu verringern."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 1 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 40%, die erhaltene Heilung des Ziels 10 Sek. lang um 20% zu verringern."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 1.5 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 60%, die erhaltene Heilung des Ziels 10 Sek. lang um 20% zu verringern."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 2 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 80%, die erhaltene Heilung des Ziels 10 Sek. lang um 20% zu verringern."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Gedankenschlag' um 2.5 Sek. Zudem hat in Schattengestalt Euer Zauber 'Gedankenschlag' eine Chance von 100%, die erhaltene Heilung des Ziels 10 Sek. lang um 20% zu verringern."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 501,
                        "name": "Gedankenschinden",
                        "icon": "spell_shadow_siphonmana",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Kanalisiert",
                                "range": "30 Meter Reichweite",
                                "cost": "9% des Grundmanas",
                                "description": "Greift die Gedanken des Ziels mit Schattenenergie an, verursacht im Verlauf von 3 Sek. 45 Schattenschaden und verringert das Bewegungstempo des Ziels um 50%."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 483,
                        "name": "Schattenschleier",
                        "icon": "spell_magic_lesserinvisibilty",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Verblassen' um 3 Sek. und verringert die Abklingzeit Eures Schattengeistes um 1 Min."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Verblassen' um 6 Sek. und verringert die Abklingzeit Eures Schattengeistes um 2 Min."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 881,
                        "name": "Schattenreichweite",
                        "icon": "spell_shadow_chilltouch",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer offensiven Schattenzauber um 10%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer offensiven Schattenzauber um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 461,
                        "name": "Schattenwirken",
                        "icon": "spell_shadow_blackplague",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Eure Schattenschadenszauber haben eine Chance von 33%, Euren verursachten Schattenschaden 15 Sek. lang um 2% zu erhöhen. Bis zu 5-mal stapelbar."
                            },
                            {
                                "description": "Eure Schattenschadenszauber haben eine Chance von 66%, Euren verursachten Schattenschaden 15 Sek. lang um 2% zu erhöhen. Bis zu 5-mal stapelbar."
                            },
                            {
                                "description": "Eure Schattenschadenszauber haben eine Chance von 100%, Euren verursachten Schattenschaden 15 Sek. lang um 2% zu erhöhen. Bis zu 5-mal stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 541,
                        "name": "Stille",
                        "icon": "spell_shadow_impphaseshift",
                        "x": 0,
                        "y": 4,
                        "req": 542,
                        "ranks": [
                            {
                                "description": "Bringt das Ziel zum Schweigen, sodass es 5 Sek. lang keine Zauber wirken kann. Das Zauberwirken von Nichtspieler-Zielen ist ebenfalls 3 Sek. lang unterbrochen.",
                                "cost": "225 Mana",
                                "castTime": "Spontanzauber",
                                "cooldown": "45 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 484,
                        "name": "Vampirumarmung",
                        "icon": "spell_shadow_unsummonbuilding",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Die Umarmung der Schattenenergie umschlingt Euch, wodurch Ihr Euch um 15% und andere Gruppenmitglieder um 3% des von Euch auf ein einzelnes Ziel verursachten Schattenschadens heilt. Hält 30 Min. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1638,
                        "name": "Verbesserte Vampirumarmung",
                        "icon": "spell_shadow_improvedvampiricembrace",
                        "x": 2,
                        "y": 4,
                        "req": 484,
                        "ranks": [
                            {
                                "description": "Erhöht die durch 'Vampirumarmung' erhaltene Heilung um 33%."
                            },
                            {
                                "description": "Erhöht die durch 'Vampirumarmung' erhaltene Heilung um 67%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1777,
                        "name": "Fokussierte Gedanken",
                        "icon": "spell_nature_focusedmind",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Gedankenschlag', 'Gedankenkontrolle', 'Gedankenschinden' und 'Gedankenexplosion' um 5%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Gedankenschlag', 'Gedankenkontrolle', 'Gedankenschinden' und 'Gedankenexplosion' um 10%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Gedankenschlag', 'Gedankenkontrolle', 'Gedankenschinden' und 'Gedankenexplosion' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1781,
                        "name": "Gedankenschmelze",
                        "icon": "spell_shadow_skull",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Gedankenexplosion' um 2%. Erhöht zudem die regelmäßige kritische Trefferchance Eurer Zauber 'Vampirberührung', 'Verschlingende Seuche' und 'Schattenwort: Schmerz' um 3%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Gedankenexplosion' um 4%. Erhöht zudem die regelmäßige kritische Trefferchance Eurer Zauber 'Vampirberührung', 'Verschlingende Seuche' und 'Schattenwort: Schmerz' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2267,
                        "name": "Verbesserte verschlingende Seuche",
                        "icon": "spell_shadow_devouringplague.",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den regelmäßigen Schaden Eures Zaubers 'Verschlingende Seuche' um 5%, zudem verursacht der Zauber beim Wirken sofort Schaden in Höhe von 10% des gesamten regelmäßigen Effekts."
                            },
                            {
                                "description": "Erhöht den regelmäßigen Schaden Eures Zaubers 'Verschlingende Seuche' um 10%, zudem verursacht der Zauber beim Wirken sofort Schaden in Höhe von 20% des gesamten regelmäßigen Effekts."
                            },
                            {
                                "description": "Erhöht den regelmäßigen Schaden Eures Zaubers 'Verschlingende Seuche' um 15%, zudem verursacht der Zauber beim Wirken sofort Schaden in Höhe von 30% des gesamten regelmäßigen Effekts."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 521,
                        "name": "Schattengestalt",
                        "icon": "spell_shadow_shadowform",
                        "x": 1,
                        "y": 6,
                        "req": 484,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1,5 Sek. Abklingzeit",
                                "cost": "13% des Grundmanas",
                                "description": "Ihr nehmt eine Schattengestalt an, erhöht Euren Schattenschaden um 15% und verringert den von Euch erlittenen Schaden um 15% sowie Eure erzeugte Bedrohung um 30%. Ihr könnt in dieser Gestalt außer 'Krankheit heilen' und 'Krankheit aufheben' jedoch keine Heiligzauber wirken. Gewährt dem regelmäßigen Schaden Eurer Zauber 'Schattenwort: Schmerz', 'Verschlingende Seuche' und 'Vampirberührung' für um 100% erhöhten Schaden kritisch zu treffen sowie 'Verschlingende Seuche' und 'Vampirberührung' die Fähigkeit, von Tempo zu profitieren."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1778,
                        "name": "Schattenmacht",
                        "icon": "spell_shadow_shadowpower",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Schattenwort: Tod' um 20%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Schattenwort: Tod' um 40%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Schattenwort: Tod' um 60%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Schattenwort: Tod' um 80%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Schattenwort: Tod' um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1906,
                        "name": "Verbesserte Schattengestalt",
                        "icon": "spell_shadow_summonvoidwalker",
                        "x": 0,
                        "y": 7,
                        "req": 521,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeit 'Verblassen' hat in Schattengestalt nun eine Chance von 50%, jegliche bewegungseinschränkende Effekte zu entfernen und die erhöhte Zauberdauer und verringerte Kanalisierungsdauer durch erlittenen Schaden wird um 35% verringert, wenn Ihr in Schattengestalt Schattenzauber wirkt."
                            },
                            {
                                "description": "Eure Fähigkeit 'Verblassen' hat in Schattengestalt nun eine Chance von 100%, jegliche bewegungseinschränkende Effekte zu entfernen und die erhöhte Zauberdauer und verringerte Kanalisierungsdauer durch erlittenen Schaden wird um 70% verringert, wenn Ihr in Schattengestalt Schattenzauber wirkt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1816,
                        "name": "Elend",
                        "icon": "spell_shadow_misery",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Schattenwort: Schmerz', 'Gedankenschinden' und 'Vampirberührung' erhöhen 24 Sek. lang zusätzlich die Trefferchance von Schadenszaubern um 1%. Erhöht zudem den Bonus, den Eure Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Gedankenexplosion' durch Zaubermacht erhalten, um 5%."
                            },
                            {
                                "description": "Eure Zauber 'Schattenwort: Schmerz', 'Gedankenschinden' und 'Vampirberührung' erhöhen 24 Sek. lang zusätzlich die Trefferchance von Schadenszaubern um 2%. Erhöht zudem den Bonus, den Eure Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Gedankenexplosion' durch Zaubermacht erhalten, um 10%."
                            },
                            {
                                "description": "Eure Zauber 'Schattenwort: Schmerz', 'Gedankenschinden' und 'Vampirberührung' erhöhen 24 Sek. lang zusätzlich die Trefferchance von Schadenszaubern um 3%. Erhöht zudem den Bonus, den Eure Zauber 'Gedankenschlag', 'Gedankenschinden' und 'Gedankenexplosion' durch Zaubermacht erhalten, um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1908,
                        "name": "Psychisches Entsetzen",
                        "icon": "spell_shadow_psychichorrors",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Ihr versetzt das Ziel 3 Sek. lang in Angst und Schrecken, sodass es vor Furcht erzittert und für 10 Sek. seine Waffenhand- und Distanzwaffe fallen lässt.",
                                "cost": "16% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1779,
                        "name": "Vampirberührung",
                        "icon": "spell_holy_stoicism",
                        "x": 1,
                        "y": 8,
                        "req": 521,
                        "ranks": [
                            {
                                "castTime": "1,5 Sek. Zauberzeit",
                                "range": "30 Meter Reichweite",
                                "cost": "16% des Grundmanas",
                                "description": "Fügt dem Ziel im Verlauf von 15 Sek. insgesamt 450 Schattenschaden zu und gewährt bis zu 10 Gruppen- oder Schlachtzugsmitgliedern alle 5 Sek. 1% ihres maximalen Manas, wenn Ihr durch 'Gedankenschlag' Schaden verursacht. Zusätzlich erleidet das von 'Vampirberührung' betroffene Ziel 720 Schaden, wenn der Effekt gebannt wird."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1909,
                        "name": "Schmerz und Leid",
                        "icon": "spell_shadow_painandsuffering",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Gedankenschinden' hat eine Chance von 33%, die Dauer von 'Schattenwort: Schmerz' auf dem Ziel zu erneuern, zudem wird der von Euch durch Euer 'Schattenwort: Tod' erlittene Schaden um 10% verringert."
                            },
                            {
                                "description": "Euer Zauber 'Gedankenschinden' hat eine Chance von 66%, die Dauer von 'Schattenwort: Schmerz' auf dem Ziel zu erneuern, zudem wird der von Euch durch Euer 'Schattenwort: Tod' erlittene Schaden um 20% verringert."
                            },
                            {
                                "description": "Euer Zauber 'Gedankenschinden' hat eine Chance von 100%, die Dauer von 'Schattenwort: Schmerz' auf dem Ziel zu erneuern, zudem wird der von Euch durch Euer 'Schattenwort: Tod' erlittene Schaden um 30% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1907,
                        "name": "Okkultismus",
                        "icon": "spell_shadow_mindtwisting",
                        "x": 2,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubermacht um 4% Eurer gesamten Willenskraft und der von Euren Zaubern 'Gedankenschinden' und 'Gedankenschlag' verursachte Schaden wird um 2% erhöht, wenn das Ziel von 'Schattenwort: Schmerz' betroffen ist."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 8% Eurer gesamten Willenskraft und der von Euren Zaubern 'Gedankenschinden' und 'Gedankenschlag' verursachte Schaden wird um 4% erhöht, wenn das Ziel von 'Schattenwort: Schmerz' betroffen ist."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 12% Eurer gesamten Willenskraft und der von Euren Zaubern 'Gedankenschinden' und 'Gedankenschlag' verursachte Schaden wird um 6% erhöht, wenn das Ziel von 'Schattenwort: Schmerz' betroffen ist."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 16% Eurer gesamten Willenskraft und der von Euren Zaubern 'Gedankenschinden' und 'Gedankenschlag' verursachte Schaden wird um 8% erhöht, wenn das Ziel von 'Schattenwort: Schmerz' betroffen ist."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 20% Eurer gesamten Willenskraft und der von Euren Zaubern 'Gedankenschinden' und 'Gedankenschlag' verursachte Schaden wird um 10% erhöht, wenn das Ziel von 'Schattenwort: Schmerz' betroffen ist."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1910,

                        "name": "Dispersion",
                        "icon": "spell_shadow_dispersion",
                        "x": 1,
                        "y": 10,
                        "req": 1779,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Löst Euch zu reiner Schattenenergie auf und verringert jeglichen erlittenen Schaden um 90%. Ihr könnt weder angreifen noch Zauber wirken, regeneriert jedoch 6 Sek. lang alle 1 Sek. 6% Mana. 'Dispersion' kann gewirkt werden, während Betäubungs-, Furcht- oder Stilleeffekte auf Euch wirken. Bei Anwendung werden alle wirkenden Verlangsamungs- und bewegungseinschränkenden Effekte entfernt und Ihr werdet für die Dauer des Effektes von 'Dispersion' gegen sie immun."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "251": {
        "name": "Glyphe 'Kreis der Heilung'",
        "id": "251",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42396",
        "spellKey": "55675",
        "spellId": "55675",
        "prettyName": "",
        "typeOrder": 2
    },
    "252": {
        "name": "Glyphe 'Magie bannen'",
        "id": "252",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42397",
        "spellKey": "55677",
        "spellId": "55677",
        "prettyName": "",
        "typeOrder": 2
    },
    "253": {
        "name": "Glyphe 'Schleier'",
        "id": "253",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Verblassen\\' um 9 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "42398",
        "spellKey": "55684",
        "spellId": "55684",
        "prettyName": "",
        "typeOrder": 2
    },
    "254": {
        "name": "Glyphe 'Furchtzauberschutz'",
        "id": "254",
        "type": 0,
        "description": "Verringert die Abklingzeit und Dauer Eures Zaubers \\'Furchtzauberschutz\\' um 60 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "42399",
        "spellKey": "55678",
        "spellId": "55678",
        "prettyName": "",
        "typeOrder": 2
    },
    "255": {
        "name": "Glyphe 'Blitzheilung'",
        "id": "255",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42400",
        "spellKey": "55679",
        "spellId": "55679",
        "prettyName": "",
        "typeOrder": 2
    },
    "256": {
        "name": "Glyphe 'Heilige Nova'",
        "id": "256",
        "type": 0,
        "description": "Verringert die globale Abklingzeit Eures Zaubers \\'Heilige Nova\\' um 0,5 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "42401",
        "spellKey": "55683",
        "spellId": "55683",
        "prettyName": "",
        "typeOrder": 2
    },
    "257": {
        "name": "Glyphe 'Inneres Feuer'",
        "id": "257",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42402",
        "spellKey": "55686",
        "spellId": "55686",
        "prettyName": "",
        "typeOrder": 2
    },
    "258": {
        "name": "Glyphe 'Brunnen des Lichts'",
        "id": "258",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42403",
        "spellKey": "55673",
        "spellId": "55673",
        "prettyName": "",
        "typeOrder": 2
    },
    "259": {
        "name": "Glyphe 'Massenbannung'",
        "id": "259",
        "type": 0,
        "description": "Verringert die Zauberzeit Eures Zaubers \\'Massenbannung\\' um 1 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "42404",
        "spellKey": "55691",
        "spellId": "55691",
        "prettyName": "",
        "typeOrder": 2
    },
    "260": {
        "name": "Glyphe 'Gedankenkontrolle'",
        "id": "260",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Psychisches Entsetzen\\' um 30 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "42405",
        "spellKey": "55688",
        "spellId": "55688",
        "prettyName": "",
        "typeOrder": 2
    },
    "261": {
        "name": "Glyphe 'Gedankenschinden'",
        "id": "261",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42406",
        "spellKey": "55681",
        "spellId": "55681",
        "prettyName": "",
        "typeOrder": 2
    },
    "262": {
        "name": "Glyphe 'Schatten'",
        "id": "262",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42407",
        "spellKey": "55689",

        "spellId": "55689",
        "prettyName": "",
        "typeOrder": 2
    },
    "263": {
        "name": "Glyphe 'Machtwort: Schild'",
        "id": "263",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42408",
        "spellKey": "55672",
        "spellId": "55672",
        "prettyName": "",
        "typeOrder": 2
    },
    "264": {
        "name": "Glyphe 'Gebet der Heilung'",
        "id": "264",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42409",
        "spellKey": "55680",
        "spellId": "55680",
        "prettyName": "",
        "typeOrder": 2
    },
    "265": {
        "name": "Glyphe 'Psychischer Schrei'",
        "id": "265",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42410",
        "spellKey": "55676",
        "spellId": "55676",
        "prettyName": "",
        "typeOrder": 2
    },
    "266": {
        "name": "Glyphe 'Erneuerung'",
        "id": "266",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42411",
        "spellKey": "55674",
        "spellId": "55674",
        "prettyName": "",
        "typeOrder": 2
    },
    "267": {
        "name": "Glyphe 'Geißelgefangennahme'",
        "id": "267",
        "type": 0,
        "description": "Verringert die Zauberzeit Eures Zaubers \\'Untote fesseln\\' um 1.0 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "42412",
        "spellKey": "55690",
        "spellId": "55690",
        "prettyName": "",
        "typeOrder": 2
    },
    "268": {
        "name": "Glyphe 'Schattenwort: Tod'",
        "id": "268",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42414",
        "spellKey": "55682",
        "spellId": "55682",
        "prettyName": "",
        "typeOrder": 2
    },
    "269": {
        "name": "Glyphe 'Schattenwort: Schmerz'",
        "id": "269",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42415",
        "spellKey": "55687",
        "spellId": "55687",
        "prettyName": "",
        "typeOrder": 2
    },
    "270": {
        "name": "Glyphe 'Göttliche Pein'",
        "id": "270",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42416",
        "spellKey": "55692",
        "spellId": "55692",
        "prettyName": "",
        "typeOrder": 2
    },
    "271": {
        "name": "Glyphe 'Geist der Erlösung'",
        "id": "271",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "42417",
        "spellKey": "55685",
        "spellId": "55685",
        "prettyName": "",
        "typeOrder": 2
    },
    "458": {
        "name": "Glyphe 'Verblassen'",
        "id": "458",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers \\'Verblassen\\' um 30%.",
        "icon": "inv_glyph_minorpriest",
        "itemId": "43342",
        "spellKey": "57985",
        "spellId": "57985",
        "prettyName": "",
        "typeOrder": 2
    },
    "459": {
        "name": "Glyphe 'Levitieren'",
        "id": "459",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorpriest",
        "itemId": "43370",
        "spellKey": "57987",
        "spellId": "57987",
        "prettyName": "",
        "typeOrder": 2
    },
    "460": {
        "name": "Glyphe 'Seelenstärke'",
        "id": "460",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorpriest",
        "itemId": "43371",
        "spellKey": "58009",
        "spellId": "58009",
        "prettyName": "",
        "typeOrder": 2
    },
    "461": {
        "name": "Glyphe 'Untote fesseln'",
        "id": "461",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorpriest",
        "itemId": "43373",
        "spellKey": "57986",
        "spellId": "57986",
        "prettyName": "",
        "typeOrder": 2
    },
    "462": {
        "name": "Glyphe 'Schattenschutz'",
        "id": "462",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorpriest",
        "itemId": "43372",
        "spellKey": "58015",
        "spellId": "58015",
        "prettyName": "",
        "typeOrder": 2
    },
    "463": {
        "name": "Glyphe 'Schattengeist'",
        "id": "463",
        "type": 1,
        "description": "Ihr erhaltet 5% Eures maximalen Manas, wenn Euer Schattengeist durch erlittenen Schaden stirbt.",
        "icon": "inv_glyph_minorpriest",
        "itemId": "43374",
        "spellKey": "58228",
        "spellId": "58228",
        "prettyName": "",
        "typeOrder": 2
    },
    "708": {
        "name": "Glyphe 'Dispersion'",
        "id": "708",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Dispersion\\' um 45 Sek.",
        "icon": "inv_glyph_majorpriest",
        "itemId": "45753",
        "spellKey": "63229",
        "spellId": "63229",
        "prettyName": "",
        "typeOrder": 2
    },
    "709": {
        "name": "Glyphe 'Schutzgeist'",
        "id": "709",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "45755",
        "spellKey": "63231",
        "spellId": "63231",
        "prettyName": "",
        "typeOrder": 2
    },
    "710": {
        "name": "Glyphe 'Sühne'",
        "id": "710",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "45756",
        "spellKey": "63235",
        "spellId": "63235",
        "prettyName": "",
        "typeOrder": 2
    },
    "711": {
        "name": "Glyphe 'Gedankenexplosion'",
        "id": "711",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "45757",
        "spellKey": "63237",
        "spellId": "63237",
        "prettyName": "",
        "typeOrder": 2
    },
    "712": {
        "name": "Glyphe 'Hymne der Hoffnung'",
        "id": "712",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "45758",
        "spellKey": "63246",
        "spellId": "63246",
        "prettyName": "",
        "typeOrder": 2
    },
    "713": {
        "name": "Glyphe 'Schmerzunterdrückung'",
        "id": "713",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpriest",
        "itemId": "45760",
        "spellKey": "63248",
        "spellId": "63248",
        "prettyName": "",
        "typeOrder": 2
    }
}]
}