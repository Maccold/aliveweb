{
    "talentData": {
        "characterClass": {
            "classId": 2,
            "name": "Paladin",
            "powerType": "des Grundmanas",
            "powerTypeId": 0,
            "powerTypeSlug": "des Grundmanas"
        },
        "talentTrees": [
            {
                "name": "Heilig",
                "icon": "spell_holy_holybolt",
                "backgroundFile": "PALADINHOLY",
                "overlayColor": "#ff7f00",
                "description": "Ruft zum Schutz und zur Heilung die Mächte des Lichts herbei.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": true,
                    "dps": false
                },
                "primarySpells": [
                    {},
                    {},
                    {}
                ],
                "masteries": [
                    {}
                ],
                "talents": [
                    {
                        "id": 1432,
                        "name": "Spiritueller Fokus",
                        "icon": "spell_arcane_blink",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Lichtblitz' und 'Heiliges Licht' um 14%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Lichtblitz' und 'Heiliges Licht' um 28%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Lichtblitz' und 'Heiliges Licht' um 42%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Lichtblitz' und 'Heiliges Licht' um 56%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Lichtblitz' und 'Heiliges Licht' um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1463,
                        "name": "Siegel der Reinen",
                        "icon": "ability_thunderbolt",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Zaubern 'Siegel der Rechtschaffenheit', 'Siegel der Vergeltung' und 'Siegel der Verderbnis' verursachten Schaden sowie die Effekte ihrer Richturteile um 3%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Siegel der Rechtschaffenheit', 'Siegel der Vergeltung' und 'Siegel der Verderbnis' verursachten Schaden sowie die Effekte ihrer Richturteile um 6%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Siegel der Rechtschaffenheit', 'Siegel der Vergeltung' und 'Siegel der Verderbnis' verursachten Schaden sowie die Effekte ihrer Richturteile um 9%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Siegel der Rechtschaffenheit', 'Siegel der Vergeltung' und 'Siegel der Verderbnis' verursachten Schaden sowie die Effekte ihrer Richturteile um 12%."
                            },
                            {
                                "description": "Erhöht den von Euren Zaubern 'Siegel der Rechtschaffenheit', 'Siegel der Vergeltung' und 'Siegel der Verderbnis' verursachten Schaden sowie die Effekte ihrer Richturteile um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1444,
                        "name": "Heilendes Licht",
                        "icon": "spell_holy_holybolt",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Eure Zauber 'Heiliges Licht' und 'Lichtblitz' geheilten Wert und die Wirksamkeit Eures Zaubers 'Heiliger Schock' um 4%."
                            },
                            {
                                "description": "Erhöht den durch Eure Zauber 'Heiliges Licht' und 'Lichtblitz' geheilten Wert und die Wirksamkeit Eures Zaubers 'Heiliger Schock' um 8%."
                            },
                            {
                                "description": "Erhöht den durch Eure Zauber 'Heiliges Licht' und 'Lichtblitz' geheilten Wert und die Wirksamkeit Eures Zaubers 'Heiliger Schock' um 12%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1449,
                        "name": "Göttliche Weisheit",
                        "icon": "spell_nature_sleep",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Intelligenzwert um 2%."
                            },
                            {
                                "description": "Erhöht Euren Intelligenzwert um 4%."
                            },
                            {
                                "description": "Erhöht Euren Intelligenzwert um 6%."
                            },
                            {
                                "description": "Erhöht Euren Intelligenzwert um 8%."
                            },
                            {
                                "description": "Erhöht Euren Intelligenzwert um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1628,
                        "name": "Unumstößlicher Glaube",
                        "icon": "spell_holy_unyieldingfaith",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer aller Furcht- und Desorientierungseffekte um 15%."
                            },
                            {
                                "description": "Verringert die Dauer aller Furcht- und Desorientierungseffekte um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1435,
                        "name": "Aurenbeherrschung",
                        "icon": "spell_holy_auramastery",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "Verleiht allen durch Eure Fähigkeit 'Aura der Konzentration' gestärkten Zielen Immunität gegen Unterbrechungs- und Stilleeffekte. Die Effekte aller anderen Auren werden um 100% erhöht. Hält 6 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1461,
                        "name": "Illumination",
                        "icon": "spell_holy_greaterheal",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Nach einem kritischen Effekt Eurer Zauber 'Lichtblitz', 'Heiliges Licht' oder dem Heileffekt von 'Heiliger Schock' besteht eine Chance von 20%, dass Mana in Höhe von 30% der Grundkosten des Zaubers wiederhergestellt wird."
                            },
                            {
                                "description": "Nach einem kritischen Effekt Eurer Zauber 'Lichtblitz', 'Heiliges Licht' oder dem Heileffekt von 'Heiliger Schock' besteht eine Chance von 40%, dass Mana in Höhe von 30% der Grundkosten des Zaubers wiederhergestellt wird."
                            },
                            {
                                "description": "Nach einem kritischen Effekt Eurer Zauber 'Lichtblitz', 'Heiliges Licht' oder dem Heileffekt von 'Heiliger Schock' besteht eine Chance von 60%, dass Mana in Höhe von 30% der Grundkosten des Zaubers wiederhergestellt wird."
                            },
                            {
                                "description": "Nach einem kritischen Effekt Eurer Zauber 'Lichtblitz', 'Heiliges Licht' oder dem Heileffekt von 'Heiliger Schock' besteht eine Chance von 80%, dass Mana in Höhe von 30% der Grundkosten des Zaubers wiederhergestellt wird."
                            },
                            {
                                "description": "Nach einem kritischen Effekt Eurer Zauber 'Lichtblitz', 'Heiliges Licht' oder dem Heileffekt von 'Heiliger Schock' besteht eine Chance von 100%, dass Mana in Höhe von 30% der Grundkosten des Zaubers wiederhergestellt wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1443,
                        "name": "Verbesserte Handauflegung",
                        "icon": "spell_holy_layonhands",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden des Ziels Eures Zaubers 'Handauflegung' 15 Sek. lang um 10%. Außerdem wird die Abklingzeit von 'Handauflegung' um 2 Min. verringert."
                            },
                            {
                                "description": "Verringert den erlittenen körperlichen Schaden des Ziels Eures Zaubers 'Handauflegung' 15 Sek. lang um 20%. Außerdem wird die Abklingzeit von 'Handauflegung' um 4 Min. verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1450,
                        "name": "Verbesserte Aura der Konzentration",
                        "icon": "spell_holy_mindsooth",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt Eurer Fähigkeit 'Aura der Konzentration' um 5%. Zudem wird die Wirkungsdauer jeglicher Stille- und Unterbrechungseffekte gegen ein Gruppenmitglied, das unter dem Einfluss einer Eurer Auren steht, um 10% verringert. Die Verringerung der Wirkungsdauer ist nicht mit anderen Effekten stapelbar."
                            },
                            {
                                "description": "Erhöht den Effekt Eurer Fähigkeit 'Aura der Konzentration' um 10%. Zudem wird die Wirkungsdauer jeglicher Stille- und Unterbrechungseffekte gegen ein Gruppenmitglied, das unter dem Einfluss einer Eurer Auren steht, um 20% verringert. Die Verringerung der Wirkungsdauer ist nicht mit anderen Effekten stapelbar."
                            },
                            {
                                "description": "Erhöht den Effekt Eurer Fähigkeit 'Aura der Konzentration' um 15%. Zudem wird die Wirkungsdauer jeglicher Stille- und Unterbrechungseffekte gegen ein Gruppenmitglied, das unter dem Einfluss einer Eurer Auren steht, um 30% verringert. Die Verringerung der Wirkungsdauer ist nicht mit anderen Effekten stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1446,
                        "name": "Verbesserter Segen der Weisheit",
                        "icon": "spell_holy_sealofwisdom",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt von 'Segen der Weisheit' um 10%."
                            },
                            {
                                "description": "Erhöht den Effekt von 'Segen der Weisheit' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2198,
                        "name": "Gesegnete Hände",
                        "icon": "ability_paladin_blessedhands",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten der Zauber 'Hand der Freiheit', 'Hand der Aufopferung' und 'Hand der Erlösung' um 15%, erhöht die Wirksamkeit von 'Hand der Erlösung' um 50% und die Wirksamkeit von 'Hand der Aufopferung' um zusätzliche 5%."
                            },
                            {
                                "description": "Verringert die Manakosten der Zauber 'Hand der Freiheit', 'Hand der Aufopferung' und 'Hand der Erlösung' um 30%, erhöht die Wirksamkeit von 'Hand der Erlösung' um 100% und die Wirksamkeit von 'Hand der Aufopferung' um zusätzliche 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1742,
                        "name": "Reinen Herzens",
                        "icon": "spell_holy_pureofheart",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer von Fluch-, Krankheits- und Gifteffekten um 15%."
                            },
                            {
                                "description": "Verringert die Dauer von Fluch-, Krankheits- und Gifteffekten um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1433,
                        "name": "Göttliche Gunst",
                        "icon": "spell_holy_heal",
                        "x": 1,
                        "y": 4,
                        "req": 1461,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "cost": "3% des Grundmanas",
                                "description": "Bei Aktivierung hat Euer nächster Zauber 'Lichtblitz', 'Heiliges Licht' oder 'Heiliger Schock' eine Chance von 100% auf einen kritischen Effekt."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1465,
                        "name": "Geweihtes Licht",
                        "icon": "spell_holy_healingaura",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Heiliges Licht' und 'Heiliger Schock' um 2%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Heiliges Licht' und 'Heiliger Schock' um 4%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Heiliges Licht' und 'Heiliger Schock' um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1743,
                        "name": "Reinigende Macht",
                        "icon": "spell_holy_purifyingpower",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Reinigung des Glaubens', 'Läutern' und 'Weihe' um 5% und verringert die Abklingzeit Eurer Zauber 'Exorzismus' und 'Heiliger Zorn' um 17%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Reinigung des Glaubens', 'Läutern' und 'Weihe' um 10% und verringert die Abklingzeit Eurer Zauber 'Exorzismus' und 'Heiliger Zorn' um 33%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1627,
                        "name": "Heilige Macht",
                        "icon": "spell_holy_power",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 1%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 2%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 3%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 4%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt mit Heiligzaubern um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1745,
                        "name": "Anmut des Lichts",
                        "icon": "spell_holy_lightsgrace",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verleiht Eurem Zauber 'Heiliges Licht' eine Chance von 33%, die Zauberzeit Eures nächsten Zaubers 'Heiliges Licht' um 0.5 Sek. zu verringern. Dieser Effekt hält 15 Sek. lang an."
                            },
                            {
                                "description": "Verleiht Eurem Zauber 'Heiliges Licht' eine Chance von 66%, die Zauberzeit Eures nächsten Zaubers 'Heiliges Licht' um 0.5 Sek. zu verringern. Dieser Effekt hält 15 Sek. lang an."
                            },
                            {
                                "description": "Verleiht Eurem Zauber 'Heiliges Licht' eine Chance von 100%, die Zauberzeit Eures nächsten Zaubers 'Heiliges Licht' um 0.5 Sek. zu verringern. Dieser Effekt hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1502,
                        "name": "Heiliger Schock",
                        "icon": "spell_holy_searinglight",
                        "x": 1,
                        "y": 6,
                        "req": 1433,
                        "ranks": [
                            {
                                "description": "Trifft das Ziel mit heiliger Energie. Verursacht 314 bis 340 Heiligschaden bei Feinden oder heilt 481 bis 519 bei Verbündeten.",
                                "cost": "18% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "20 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1744,
                        "name": "Gesegnetes Leben",
                        "icon": "spell_holy_blessedlife",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Alle Angriffe gegen Euch haben eine Chance von 4%, halben Schaden zu verursachen."
                            },
                            {
                                "description": "Alle Angriffe gegen Euch haben eine Chance von 7%, halben Schaden zu verursachen."
                            },
                            {
                                "description": "Alle Angriffe gegen Euch haben eine Chance von 10%, halben Schaden zu verursachen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2190,
                        "name": "Heilige Läuterung",
                        "icon": "ability_paladin_sacredcleansing",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Reinigung' hat eine Chance von 10%, 10 Sek. lang den Widerstand des Ziels gegen Krankheiten, Magie und Gifte um 30% zu erhöhen."
                            },
                            {
                                "description": "Euer Zauber 'Reinigung' hat eine Chance von 20%, 10 Sek. lang den Widerstand des Ziels gegen Krankheiten, Magie und Gifte um 30% zu erhöhen."
                            },
                            {
                                "description": "Euer Zauber 'Reinigung' hat eine Chance von 30%, 10 Sek. lang den Widerstand des Ziels gegen Krankheiten, Magie und Gifte um 30% zu erhöhen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1746,
                        "name": "Heilige Führung",
                        "icon": "spell_holy_holyguidance",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubermacht um 4% Eurer gesamten Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 8% Eurer gesamten Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 12% Eurer gesamten Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 16% Eurer gesamten Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 20% Eurer gesamten Intelligenz."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1747,
                        "name": "Göttliche Eingebung",
                        "icon": "spell_holy_divineillumination",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Verringert die Manakosten aller Zauber 15 Sek. lang um 50%."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2199,
                        "name": "Richturteile des Reinen",
                        "icon": "ability_paladin_judgementofthepure",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Siegel- und Richturteilszauber um 5%, zudem erhöhen Eure Richturteile Euer Nahkampfangriffs- und Zaubertempo 1 Min. lang um 3%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Siegel- und Richturteilszauber um 10%, zudem erhöhen Eure Richturteile Euer Nahkampfangriffs- und Zaubertempo 1 Min. lang um 6%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Siegel- und Richturteilszauber um 15%, zudem erhöhen Eure Richturteile Euer Nahkampfangriffs- und Zaubertempo 1 Min. lang um 9%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Siegel- und Richturteilszauber um 20%, zudem erhöhen Eure Richturteile Euer Nahkampfangriffs- und Zaubertempo 1 Min. lang um 12%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Siegel- und Richturteilszauber um 25%, zudem erhöhen Eure Richturteile Euer Nahkampfangriffs- und Zaubertempo 1 Min. lang um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2193,
                        "name": "Lichtenergie",
                        "icon": "ability_paladin_infusionoflight",
                        "x": 1,
                        "y": 9,
                        "req": 1502,
                        "ranks": [
                            {
                                "description": "Kritische Treffer Eures Zaubers 'Heiliger Schock' verringern die Zauberzeit Eures nächsten Wirkens von 'Lichtblitz' um 0.75 Sek. oder erhöhen die kritische Trefferchance Eures nächsten Wirkens von 'Heiliges Licht' um 10%. Zudem wird Euer Zauber 'Lichtblitz' Ziele, auf denen der Effekt 'Geheiligter Schild' wirkt, im Verlauf von 12 Sek. zusätzlich um 50% heilen."
                            },
                            {
                                "description": "Kritische Treffer Eures Zaubers 'Heiliger Schock' verringern die Zauberzeit Eures nächsten Wirkens von 'Lichtblitz' um 1.5 Sek. oder erhöhen die kritische Trefferchance Eures nächsten Wirkens von 'Heiliges Licht' um 20%. Zudem wird Euer Zauber 'Lichtblitz' Ziele, auf denen der Effekt 'Geheiligter Schild' wirkt, im Verlauf von 12 Sek. zusätzlich um 100% heilen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2191,
                        "name": "Erleuchtete Richturteile",
                        "icon": "ability_paladin_enlightenedjudgements",
                        "x": 2,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Richturteil des Lichts' und 'Richturteil der Weisheit' um 15 Meter und erhöht Eure Trefferchance um 2%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Zauber 'Richturteil des Lichts' und 'Richturteil der Weisheit' um 30 Meter und erhöht Eure Trefferchance um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2192,
                        "name": "Flamme des Glaubens",
                        "icon": "ability_paladin_beaconoflight",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "60 Meter Reichweite",
                                "cost": "35% des Grundmanas",
                                "description": "Das Ziel wird für alle Gruppen- und Schlachtzugsmitglieder in einem Radius von 60 Metern zu einer Flamme des Glaubens. Alle auf diese Ziele gewirkten Heilungen heilen auch die Flamme um 100% der gewirkten Heilung. Es kann immer nur ein Ziel zur gleichen Zeit eine Flamme des Glaubens sein. Hält 1 Min. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Schutz",
                "icon": "ability_paladin_shieldofthetemplar",
                "backgroundFile": "PALADINPROTECTION",
                "overlayColor": "#4c7fff",
                "description": "Nutzt heilige Magie, um sich und seine Verbündeten vor Angreifern zu schützen.",
                "treeNo": 1,
                "roles": {
                    "tank": true,
                    "healer": false,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1442,
                        "name": "Göttlichkeit",
                        "icon": "spell_holy_blindingheal",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht jegliche von Euch verursachte und Eure erhaltene Heilung um 1%."
                            },
                            {
                                "description": "Erhöht jegliche von Euch verursachte und Eure erhaltene Heilung um 2%."
                            },
                            {
                                "description": "Erhöht jegliche von Euch verursachte und Eure erhaltene Heilung um 3%."
                            },
                            {
                                "description": "Erhöht jegliche von Euch verursachte und Eure erhaltene Heilung um 4%."
                            },
                            {
                                "description": "Erhöht jegliche von Euch verursachte und Eure erhaltene Heilung um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2185,
                        "name": "Göttliche Stärke",
                        "icon": "ability_golemthunderclap",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Stärke um 3%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 6%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 9%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 12%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Stärke um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1748,
                        "name": "Gleichmut",
                        "icon": "spell_holy_stoicism",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Wirkungsdauer aller auf Euch wirkenden Betäubungseffekte um zusätzlich 10% und verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 10%."
                            },
                            {
                                "description": "Verringert die Wirkungsdauer aller auf Euch wirkenden Betäubungseffekte um zusätzlich 20% und verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 20%."
                            },
                            {
                                "description": "Verringert die Wirkungsdauer aller auf Euch wirkenden Betäubungseffekte um zusätzlich 30% und verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1425,
                        "name": "Gunst des Hüters",
                        "icon": "spell_holy_sealofprotection",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Hand des Schutzes' um 60 Sek. und erhöht die Dauer Eures Zaubers 'Hand der Freiheit' um 2 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eures Zaubers 'Hand des Schutzes' um 2 Min. und erhöht die Dauer Eures Zaubers 'Hand der Freiheit' um 4 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1629,
                        "name": "Vorahnung",
                        "icon": "spell_magic_lesserinvisibilty",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance um 1%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 3%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2280,
                        "name": "Heilige Opferung",
                        "icon": "spell_holy_powerwordbarrier",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "description": "30% jeglichen von Gruppenmitgliedern im Umkreis von 30 Metern erlittenen Schadens wird auf den Paladin umgelenkt (bis zu einem Maximum von 40% der Gesundheit des Paladins multipliziert mit der Anzahl der Gruppenmitglieder). Schaden, der die Gesundheit des Paladins auf unter 20% senkt, wird den Effekt abbrechen. Hält 10 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1501,
                        "name": "Verbesserter Zorn der Gerechtigkeit",
                        "icon": "spell_holy_sealoffury",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Ist 'Zorn der Gerechtigkeit' aktiv, wird jeglicher erlittene Schaden um 2% verringert."
                            },
                            {
                                "description": "Ist 'Zorn der Gerechtigkeit' aktiv, wird jeglicher erlittene Schaden um 4% verringert."
                            },
                            {
                                "description": "Ist 'Zorn der Gerechtigkeit' aktiv, wird jeglicher erlittene Schaden um 6% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1423,
                        "name": "Zähigkeit",
                        "icon": "spell_holy_devotion",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 2% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 6%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 4% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 12%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 6% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 18%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 8% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 24%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände erzielten Rüstungswert um 10% und verringert die Dauer aller bewegungsverlangsamenden Effekte um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2281,
                        "name": "Heiliger Wächter",
                        "icon": "spell_holy_powerwordbarrier",
                        "x": 0,
                        "y": 3,
                        "req": 2280,
                        "ranks": [
                            {
                                "description": "Ist Euer Zauber 'Heilige Opferung' aktiviert, wird der erlittene Schaden von Gruppen- und Schlachtzugsmitgliedern innerhalb von 30 Metern 6 Sek. lang um 10% verringert. Zudem wird die Dauer Eures Zaubers 'Geheiligter Schild' um 50% und der absorbierte Betrag um 10% erhöht."
                            },
                            {
                                "description": "Ist Euer Zauber 'Heilige Opferung' aktiviert, wird der erlittene Schaden von Gruppen- und Schlachtzugsmitgliedern innerhalb von 30 Metern 6 Sek. lang um 20% verringert. Zudem wird die Dauer Eures Zaubers 'Geheiligter Schild' um 100% und der absorbierte Betrag um 20% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1521,
                        "name": "Verbesserter Hammer der Gerechtigkeit",
                        "icon": "spell_holy_sealofmight",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit von 'Hammer der Gerechtigkeit' um 10 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit von 'Hammer der Gerechtigkeit' um 20 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1422,
                        "name": "Verbesserte Aura der Hingabe",
                        "icon": "spell_holy_devotionaura",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Rüstungsbonus Eurer Fähigkeit 'Aura der Hingabe' um 17% und erhöht die erhaltene Heilung von Zielen, die unter dem Einfluss einer Eurer Auren stehen, um 2%."
                            },
                            {
                                "description": "Erhöht den Rüstungsbonus Eurer Fähigkeit 'Aura der Hingabe' um 34% und erhöht die erhaltene Heilung von Zielen, die unter dem Einfluss einer Eurer Auren stehen, um 4%."
                            },
                            {
                                "description": "Erhöht den Rüstungsbonus Eurer Fähigkeit 'Aura der Hingabe' um 50% und erhöht die erhaltene Heilung von Zielen, die unter dem Einfluss einer Eurer Auren stehen, um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1431,
                        "name": "Segen des Refugiums",
                        "icon": "spell_nature_lightningshield",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "cost": "7% des Grundmanas",
                                "description": "Belegt ein befreundetes Ziel mit einem Segen, der jeglichen erlittenen Schaden des Ziels 10 Min. lang um bis zu 3% verringert sowie die Ausdauer und Stärke um 10% erhöht. Zusätzlich gewinnt das Ziel durch Nahkampfangriffe, die es blockt, pariert, oder denen es ausweicht, 2% des angezeigten maximalen Manas. Spieler können pro Paladin immer nur mit einem einzigen Segen belegt sein."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1426,
                        "name": "Abrechnung",
                        "icon": "spell_holy_blessingofstrength",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 2%, innerhalb von 8 Sek. nach Erleiden eines Schaden zufügenden Treffers durch die nächsten 4 Waffenschwünge einen zusätzlichen Angriff zu erhalten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 4%, innerhalb von 8 Sek. nach Erleiden eines Schaden zufügenden Treffers durch die nächsten 4 Waffenschwünge einen zusätzlichen Angriff zu erhalten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 6%, innerhalb von 8 Sek. nach Erleiden eines Schaden zufügenden Treffers durch die nächsten 4 Waffenschwünge einen zusätzlichen Angriff zu erhalten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 8%, innerhalb von 8 Sek. nach Erleiden eines Schaden zufügenden Treffers durch die nächsten 4 Waffenschwünge einen zusätzlichen Angriff zu erhalten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 10%, dass die nächsten 4 Waffenschwünge innerhalb von 8 Sek. nach erlittenem Schaden oder einem geblockten Angriff jeweils einen zusätzlichen Angriff erzeugen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1750,
                        "name": "Heilige Pflicht",
                        "icon": "spell_holy_divineintervention",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Ausdauer um 2% und verringert die Abklingzeit Eurer Zauber 'Gottesschild' und 'Göttlicher Schutz' um 30 Sek."
                            },
                            {
                                "description": "Erhöht Eure gesamte Ausdauer um 4% und verringert die Abklingzeit Eurer Zauber 'Gottesschild' und 'Göttlicher Schutz' um 60 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1429,
                        "name": "Einhandwaffen-Spezialisierung",
                        "icon": "inv_sword_20",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht jeglichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 4%."
                            },
                            {
                                "description": "Erhöht jeglichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 7%."
                            },
                            {
                                "description": "Erhöht jeglichen zugefügten Schaden, wenn Ihr eine Einhandnahkampfwaffe angelegt habt, um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2282,
                        "name": "Einklang des Geistes",
                        "icon": "spell_holy_revivechampion",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Eine passive Fähigkeit, die dem Paladin Mana gewährt, wenn er von anderen befreundeten Zielen geheilt wird. Das erhaltene Mana entspricht 5% des geheilten Betrages."
                            },
                            {
                                "description": "Eine passive Fähigkeit, die dem Paladin Mana gewährt, wenn er von anderen befreundeten Zielen geheilt wird. Das erhaltene Mana entspricht 10% des geheilten Betrages."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1430,
                        "name": "Heiliger Schild",
                        "icon": "spell_holy_blessingofprotection",
                        "x": 1,
                        "y": 6,
                        "req": 1431,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "8 Sek. Abklingzeit",
                                "cost": "10% des Grundmanas",
                                "description": "Erhöht die Blockchance 10 Sek. lang um 30% und verursacht, solang aktiv, mit jedem geblockten Angriff 79 Heiligschaden. Jedes Blocken verbraucht eine der 8 Aufladungen."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1751,
                        "name": "Unermüdlicher Verteidiger",
                        "icon": "spell_holy_ardentdefender",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erlittener Schaden, der Eure Gesundheit unter 35% senkt, wird um 7% verringert. Zusätzlich werdet Ihr durch Angriffe, die Euch normalerweise töten würden, um bis zu 10% Eurer maximalen Gesundheit geheilt (der geheilte Wert basiert auf Verteidigung). Dieser Effekt kann nur ein Mal alle 2 Min. auftreten."
                            },
                            {
                                "description": "Erlittener Schaden, der Eure Gesundheit unter 35% senkt, wird um 13% verringert. Zusätzlich werdet Ihr durch Angriffe, die Euch normalerweise töten würden, um bis zu 20% Eurer maximalen Gesundheit geheilt (der geheilte Wert basiert auf Verteidigung). Dieser Effekt kann nur ein Mal alle 2 Min. auftreten."
                            },
                            {
                                "description": "Erlittener Schaden, der Eure Gesundheit unter 35% senkt, wird um 20% verringert. Zusätzlich werdet Ihr durch Angriffe, die Euch normalerweise töten würden, um bis zu 30% Eurer maximalen Gesundheit geheilt (der geheilte Wert basiert auf Verteidigung). Dieser Effekt kann nur ein Mal alle 2 Min. auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1421,
                        "name": "Verschanzen",
                        "icon": "ability_defend",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Blockwert um 10%. Nahkampf- und Distanzangriffe, durch die Ihr Schaden erleidet, haben eine Chance von 10%, Eure Blockchance um 10% zu erhöhen. Hält 10 Sek. lang oder bis 5-mal geblockt wurde."
                            },
                            {
                                "description": "Erhöht Euren Blockwert um 20%. Nahkampf- und Distanzangriffe, durch die Ihr Schaden erleidet, haben eine Chance von 10%, Eure Blockchance um 20% zu erhöhen. Hält 10 Sek. lang oder bis 5-mal geblockt wurde."
                            },
                            {
                                "description": "Erhöht Euren Blockwert um 30%. Nahkampf- und Distanzangriffe, durch die Ihr Schaden erleidet, haben eine Chance von 10%, Eure Blockchance um 30% zu erhöhen. Hält 10 Sek. lang oder bis 5-mal geblockt wurde."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1753,
                        "name": "Kampfexperte",
                        "icon": "spell_holy_weaponmastery",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Waffenkunde um 2, Eure gesamte Ausdauer und kritische Trefferchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Waffenkunde um 4, Eure gesamte Ausdauer und kritische Trefferchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Waffenkunde um 6, Eure gesamte Ausdauer und kritische Trefferchance um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2195,
                        "name": "Vom Licht berührt",
                        "icon": "ability_paladin_touchedbylight",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubermacht um einen Wert, der 20% Eurer Stärke entspricht und erhöht die von Euren kritischen Heilzaubern gewirkte Heilung um 10%."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um einen Wert, der 40% Eurer Stärke entspricht und erhöht die von Euren kritischen Heilzaubern gewirkte Heilung um 20%."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um einen Wert, der 60% Eurer Stärke entspricht und erhöht die von Euren kritischen Heilzaubern gewirkte Heilung um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1754,
                        "name": "Schild des Rächers",
                        "icon": "spell_holy_avengersshield",
                        "x": 1,
                        "y": 8,
                        "req": 1430,
                        "ranks": [
                            {
                                "description": "Schleudert einen heiligen Schild auf den Gegner, der 442 bis 538 Heiligschaden verursacht, ihn benommen macht, und dann auf weitere nahe Gegner überspringt. Wirkt auf insgesamt 3 Ziele. Hält 10 Sek. lang an.",
                                "cost": "26% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2194,
                        "name": "Vom Licht behütet",
                        "icon": "ability_paladin_gaurdedbythelight",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verringert den erlittenen Zauberschaden um 3% und gewährt eine Chance von 50%, dass die Dauer von 'Göttliche Bitte' aufgefrischt wird, wenn Ihr einen Feind schlagt. Zusätzlich wird die Chance, dass 'Göttliche Bitte' gebannt wird, um 50% verringert."
                            },
                            {
                                "description": "Verringert den erlittenen Zauberschaden um 6% und gewährt eine Chance von 100%, dass die Dauer von 'Göttliche Bitte' aufgefrischt wird, wenn Ihr einen Feind schlagt. Zusätzlich wird die Chance, dass 'Göttliche Bitte' gebannt wird, um 100% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2204,
                        "name": "Schild des Templers",
                        "icon": "ability_paladin_shieldofthetemplar",
                        "x": 1,
                        "y": 9,
                        "req": 1754,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 1% und gewährt Eurem Zauber 'Schild des Rächers' eine Chance von 33%, Eure Ziele 3 Sek. lang zum Schweigen zu bringen."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 2% und gewährt Eurem Zauber 'Schild des Rächers' eine Chance von 66%, Eure Ziele 3 Sek. lang zum Schweigen zu bringen."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 3% und gewährt Eurem Zauber 'Schild des Rächers' eine Chance von 100%, Eure Ziele 3 Sek. lang zum Schweigen zu bringen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2200,
                        "name": "Richturteil des Gerechten",
                        "icon": "ability_paladin_judgementsofthejust",
                        "x": 2,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit von 'Hammer der Gerechtigkeit' um 5 Sek. und erhöht die Dauer des Effektes von 'Siegel der Gerechtigkeit' um 0.5 Sek., zudem verringern Eure Richturteile das Nahkampfangriffstempo des Ziels um 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeit von 'Hammer der Gerechtigkeit' um 10 Sek. und erhöht die Dauer des Effektes von 'Siegel der Gerechtigkeit' um 1 Sek., zudem verringern Eure Richturteile das Nahkampfangriffstempo des Ziels um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2196,
                        "name": "Hammer der Rechtschaffenen",
                        "icon": "ability_paladin_hammeroftherighteous",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "cost": "6% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite",
                                "description": "Trifft das aktuelle Ziel sowie bis zu 2 zusätzliche nahe Ziele und verursacht den 4-fachen Schaden pro Sekunde Eurer Waffenhandwaffe als Heiligschaden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Vergeltung",
                "icon": "spell_holy_auraoflight",
                "backgroundFile": "PALADINCOMBAT",
                "overlayColor": "#ff0000",
                "description": "Ein rechtschaffener Kreuzfahrer, der seine Gegner mit Waffen und heiliger Magie richtet und straft.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1403,
                        "name": "Abwehr",
                        "icon": "ability_parry",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Parierchance um 1%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 3%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1407,
                        "name": "Segnung",
                        "icon": "spell_frost_windwalkon",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten aller Spontanzauber um 2%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Spontanzauber um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Spontanzauber um 6%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Spontanzauber um 8%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Spontanzauber um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1631,
                        "name": "Verbesserte Richturteile",
                        "icon": "spell_holy_righteousfury",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Richturteile um 1 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Richturteile um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1464,
                        "name": "Herz des Kreuzfahrers",
                        "icon": "spell_holy_holysmite",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Neben ihren normalen Effekten erhöhen Eure Richturteile die kritische Trefferchance aller Angriffe gegen das betroffene Ziel um zusätzliche 1%."
                            },
                            {
                                "description": "Neben ihren normalen Effekten erhöhen Eure Richturteile die kritische Trefferchance aller Angriffe gegen das betroffene Ziel um zusätzliche 2%."
                            },
                            {
                                "description": "Neben ihren normalen Effekten erhöhen Eure Richturteile die kritische Trefferchance aller Angriffe gegen das betroffene Ziel um zusätzliche 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1401,
                        "name": "Verbesserter Segen der Macht",
                        "icon": "spell_holy_fistofjustice",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Bonus auf die Angriffskraft Eures Zaubers 'Segen der Macht' um 12%."
                            },
                            {
                                "description": "Erhöht den Bonus auf die Angriffskraft Eures Zaubers 'Segen der Macht' um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1633,
                        "name": "Rechtschaffene Schwächung",
                        "icon": "spell_holy_vindication",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Gewährt den Schaden verursachenden Angriffen des Paladins eine Chance, die Angriffskraft des Ziels 10 Sek. lang um 23 zu verringern."
                            },
                            {
                                "description": "Gewährt den Schaden verursachenden Angriffen des Paladins eine Chance, die Angriffskraft des Ziels 10 Sek. lang um 46 zu verringern."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1411,
                        "name": "Überzeugung",
                        "icon": "spell_holy_retributionaura",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit allen Angriffen und Zaubern einen kritischen Treffer zu erzielen, um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit allen Angriffen und Zaubern einen kritischen Treffer zu erzielen, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit allen Angriffen und Zaubern einen kritischen Treffer zu erzielen, um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit allen Angriffen und Zaubern einen kritischen Treffer zu erzielen, um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit allen Angriffen und Zaubern einen kritischen Treffer zu erzielen, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1481,
                        "name": "Siegel des Befehls",
                        "icon": "ability_warrior_innerrage",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cost": "14% des Grundmanas",
                                "description": "Alle Nahkampfangriffe verursachen 1 bis 2 zusätzlichen Heiligschaden. Wird dieser Zauber mit Angriffen oder Fähigkeiten genutzt, die ein einzelnes Ziel treffen, trifft der zusätzliche Heiligschaden bis zu 2 zusätzliche Ziele. Hält 30 Min. lang an.<br/><br/>Die Entfesselung der Energie dieses Siegels richtet einen Feind und verursacht sofort 3 bis 3 Heiligschaden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1634,
                        "name": "Streben nach Gerechtigkeit",
                        "icon": "spell_holy_persuitofjustice",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer aller Entwaffnungseffekte um 25% und erhöht Euer normales Bewegungstempo und Euer Bewegungstempo beim Reiten um 8%. Ergänzt sich nicht mit anderen Effekten, die das Bewegungstempo erhöhen."
                            },
                            {
                                "description": "Verringert die Dauer aller Entwaffnungseffekte um 50% und erhöht Euer normales Bewegungstempo und Euer Bewegungstempo beim Reiten um 15%. Ergänzt sich nicht mit anderen Effekten, die das Bewegungstempo erhöhen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1632,
                        "name": "Auge um Auge",
                        "icon": "spell_holy_eyeforaneye",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Ein Angreifer erleidet durch kritische Treffer gegen Euch 5% seines verursachten Schadens. Der von 'Auge um Auge' verursachte Schaden kann nie 50% der gesamten Gesundheit des Paladins überschreiten."
                            },
                            {
                                "description": "Ein Angreifer erleidet durch kritische Treffer gegen Euch 10% seines verursachten Schadens. Der von 'Auge um Auge' verursachte Schaden kann nie 50% der gesamten Gesundheit des Paladins überschreiten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1761,
                        "name": "Geheiligter Kampf",
                        "icon": "spell_holy_holysmite",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Zaubern und Angriffen um 1% und erhöht den Schaden von 'Exorzismus' und 'Kreuzfahrerstoß' um 5%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Zaubern und Angriffen um 2% und erhöht den Schaden von 'Exorzismus' und 'Kreuzfahrerstoß' um 10%."
                            },
                            {
                                "description": "Erhöht Eure kritische Trefferchance mit allen Zaubern und Angriffen um 3% und erhöht den Schaden von 'Exorzismus' und 'Kreuzfahrerstoß' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1755,
                        "name": "Kreuzzug",
                        "icon": "spell_holy_crusade",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht jeglichen verursachten Schaden um 1% und den Schaden an Humanoiden, Dämonen, Untoten und Elementaren zusätzlich um 1%."
                            },
                            {
                                "description": "Erhöht jeglichen verursachten Schaden um 2% und den Schaden an Humanoiden, Dämonen, Untoten und Elementaren zusätzlich um 2%."
                            },
                            {
                                "description": "Erhöht jeglichen verursachten Schaden um 3% und den Schaden an Humanoiden, Dämonen, Untoten und Elementaren zusätzlich um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1410,
                        "name": "Zweihandwaffen-Spezialisierung",
                        "icon": "inv_hammer_04",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden, den Ihr mit Zweihand-Nahkampfwaffen zufügt, um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1756,
                        "name": "Geweihte Vergeltung",
                        "icon": "spell_holy_mindvision",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Fähigkeit 'Aura der Vergeltung' um 50% und den verursachten Schaden von befreundeten Zielen, die unter dem Einfluss einer Eurer Auren stehen, um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1402,
                        "name": "Rache",
                        "icon": "ability_racial_avatar",
                        "x": 1,
                        "y": 5,
                        "req": 1411,
                        "ranks": [
                            {
                                "description": "Gewährt Euch, nachdem Ihr einen kritischen Treffer mit einem Waffenangriff, Zauber oder einer Fähigkeit zugefügt habt, 30 Sek. lang einen Bonus von 1% auf von Euch zugefügte körperliche Schäden und Heiligschäden. Dieser Effekt ist bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Gewährt Euch, nachdem Ihr einen kritischen Treffer mit einem Waffenangriff, Zauber oder einer Fähigkeit zugefügt habt, 30 Sek. lang einen Bonus von 2% auf von Euch zugefügte körperliche Schäden und Heiligschäden. Dieser Effekt ist bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Gewährt Euch, nachdem Ihr einen kritischen Treffer mit einem Waffenangriff, Zauber oder einer Fähigkeit zugefügt habt, 30 Sek. lang einen Bonus von 3% auf von Euch zugefügte körperliche Schäden und Heiligschäden. Dieser Effekt ist bis zu 3-mal stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1757,
                        "name": "Göttliche Bestimmung",
                        "icon": "spell_holy_divinepurpose",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Verringert Eure Chance, von Zaubern und Distanzangriffen getroffen zu werden, um 2% und verleiht Eurem Zauber 'Hand der Freiheit' eine Chance von 50%, einen Betäubungseffekt vom Ziel zu entfernen."
                            },
                            {
                                "description": "Verringert Eure Chance, von Zaubern und Distanzangriffen getroffen zu werden, um 4% und verleiht Eurem Zauber 'Hand der Freiheit' eine Chance von 100%, einen Betäubungseffekt vom Ziel zu entfernen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2176,
                        "name": "Die Kunst des Krieges",
                        "icon": "ability_paladin_artofwar",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht sowohl den Schaden Eurer Richturteilszauber, als auch den Eurer Zauber 'Kreuzfahrerstoß' und 'Göttlicher Sturm' um 5%. Zudem wird, wenn Eure Nahkampfangriffe kritisch treffen, die Zauberzeit Eures nächsten Wirkens von 'Lichtblitz' oder 'Exorzismus' um 0.75 Sek. verringert."
                            },
                            {
                                "description": "Erhöht sowohl den Schaden Eurer Richturteilszauber, als auch den Eurer Zauber 'Kreuzfahrerstoß' und 'Göttlicher Sturm' um 10%. Zudem wird, wenn Eure Nahkampfangriffe kritisch treffen, Euer nächstes Wirken von 'Lichtblitz' oder 'Exorzismus' zu einem Spontanzauber."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1441,
                        "name": "Buße",
                        "icon": "spell_holy_prayerofhealing",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Versetzt das feindliche Ziel in einen meditativen Zustand, macht es so bis zu 1 Min. lang handlungsunfähig und entfernt den Effekt von 'Rechtschaffene Vergeltung'. Jeglicher erlittene Schaden wird das Ziel wecken. Wirkt auf Dämonen, Drachkin, Riesen, Humanoide und Untote.",
                                "cost": "9% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "range": "20 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1758,
                        "name": "Richturteil des Weisen",
                        "icon": "ability_paladin_judgementofthewise",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Eure Schaden verursachenden Richturteilszauber haben eine Chance von 33%, bis zu 10 Gruppen- oder Schlachtzugsmitgliedern 15 Sek. lang den Effekt 'Erfrischung' zu gewähren, der alle 5 Sek. 1% ihres maximalen Manas wiederherstellt und Euch sofort 25% Eures Grundmanas gewährt."
                            },
                            {
                                "description": "Eure Schaden verursachenden Richturteilszauber haben eine Chance von 66%, bis zu 10 Gruppen- oder Schlachtzugsmitgliedern 15 Sek. lang den Effekt 'Erfrischung' zu gewähren, der alle 5 Sek. 1% ihres maximalen Manas wiederherstellt und Euch sofort 25% Eures Grundmanas gewährt."
                            },
                            {
                                "description": "Eure Schaden verursachenden Richturteilszauber haben eine Chance von 100%, bis zu 10 Gruppen- oder Schlachtzugsmitgliedern 15 Sek. lang den Effekt 'Erfrischung' zu gewähren, der alle 5 Sek. 1% ihres maximalen Manas wiederherstellt und Euch sofort 25% Eures Grundmanas gewährt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1759,
                        "name": "Fanatismus",
                        "icon": "spell_holy_fanaticism",
                        "x": 1,
                        "y": 7,
                        "req": 1441,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance aller Richturteile, bei denen kritische Treffer möglich sind, um 6% und verringert die durch alle Aktionen verursachte Bedrohung um 10%, außer wenn 'Zorn der Gerechtigkeit' aktiv ist."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Richturteile, bei denen kritische Treffer möglich sind, um 12% und verringert die durch alle Aktionen verursachte Bedrohung um 20%, außer wenn 'Zorn der Gerechtigkeit' aktiv ist."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Richturteile, bei denen kritische Treffer möglich sind, um 18% und verringert die durch alle Aktionen verursachte Bedrohung um 30%, außer wenn 'Zorn der Gerechtigkeit' aktiv ist."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2147,
                        "name": "Geweihte Wut",
                        "icon": "ability_paladin_sanctifiedwrath",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance von 'Hammer des Zorns' um 25% und verringert die Abklingzeit von 'Zornige Vergeltung' um 30 Sek. Zudem ignorieren 25% des Schadens, der unter dem Einfluss von 'Zornige Vergeltung' verursacht wird, schadensverringernde Effekte."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance von 'Hammer des Zorns' um 50% und verringert die Abklingzeit von 'Zornige Vergeltung' um 60 Sek. Zudem ignorieren 50% des Schadens, der unter dem Einfluss von 'Zornige Vergeltung' verursacht wird, schadensverringernde Effekte."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2148,
                        "name": "Schnelle Vergeltung",
                        "icon": "ability_paladin_swiftretribution",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Eure Auren erhöhen zusätzlich das Zauber-, Distanzangriffs- und Nahkampfangriffstempo um 1%."
                            },
                            {
                                "description": "Eure Auren erhöhen zusätzlich das Zauber-, Distanzangriffs- und Nahkampfangriffstempo um 2%."
                            },
                            {
                                "description": "Eure Auren erhöhen zusätzlich das Zauber-, Distanzangriffs- und Nahkampfangriffstempo um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1823,
                        "name": "Kreuzfahrerstoß",
                        "icon": "spell_holy_crusaderstrike",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Ein sofortiger Stoß, der 75% Waffenschaden verursacht.",
                                "cost": "5% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "4 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2179,
                        "name": "Ummantelung des Lichts",
                        "icon": "ability_paladin_sheathoflight",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubermacht um einen Wert, der 10% Eurer Angriffskraft entspricht, und Eure kritischen Heilungen heilen das Ziel im Verlauf von 12 Sek. um 20% des geheilten Wertes."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um einen Wert, der 20% Eurer Angriffskraft entspricht, und Eure kritischen Heilungen heilen das Ziel im Verlauf von 12 Sek. um 40% des geheilten Wertes."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um einen Wert, der 30% Eurer Angriffskraft entspricht, und Eure kritischen Heilungen heilen das Ziel im Verlauf von 12 Sek. um 60% des geheilten Wertes."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2149,
                        "name": "Rechtschaffene Vergeltung",
                        "icon": "ability_paladin_righteousvengeance",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erzielen Eure Richturteilszauber, 'Kreuzfahrerstoß' oder 'Göttlicher Sturm' einen kritischen Treffer, erleidet Euer Ziel 8 Sek. lang 10% zusätzlichen Schaden."
                            },
                            {
                                "description": "Erzielen Eure Richturteilszauber, 'Kreuzfahrerstoß' oder 'Göttlicher Sturm' einen kritischen Treffer, erleidet Euer Ziel 8 Sek. lang 20% zusätzlichen Schaden."
                            },
                            {
                                "description": "Erzielen Eure Richturteilszauber, 'Kreuzfahrerstoß' oder 'Göttlicher Sturm' einen kritischen Treffer, erleidet Euer Ziel 8 Sek. lang 30% zusätzlichen Schaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2150,
                        "name": "Göttlicher Sturm",
                        "icon": "ability_paladin_divinestorm",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "10 Sek. Abklingzeit",
                                "cost": "12% des Grundmanas",
                                "description": "Ein sofortiger Waffenangriff, der bei bis zu 4 Feinden innerhalb von 8 Metern 110% Waffenschaden verursacht. 'Göttlicher Sturm' heilt bis zu 3 Gruppen- oder Schlachtzugsmitglieder in Höhe von 25% des verursachten Schadens."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [
        {
    "183": {
        "name": "Glyphe 'Richturteil'",
        "id": "183",
        "type": 0,
        "description": "Euer Zauber 'Richturteil' verursacht 10% mehr Schaden.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41092",
        "spellKey": "54922",
        "spellId": "54922",
        "prettyName": "",
        "typeOrder": 2
    },
    "184": {
        "name": "Glyphe 'Siegel des Befehls'",
        "id": "184",
        "type": 0,
        "description": "Verringert die Manakosten Eures Zaubers 'Zurechtweisung' um 100%.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41094",
        "spellKey": "54925",
        "spellId": "54925",
        "prettyName": "",
        "typeOrder": 2
    },
    "185": {
        "name": "Glyphe 'Hammer der Gerechtigkeit'",
        "id": "185",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41095",
        "spellKey": "54923",
        "spellId": "54923",
        "prettyName": "",
        "typeOrder": 2
    },
    "186": {
        "name": "Glyphe 'Einklang des Geistes'",
        "id": "186",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41096",
        "spellKey": "54924",
        "spellId": "54924",
        "prettyName": "",
        "typeOrder": 2
    },
    "187": {
        "name": "Glyphe 'Hammer des Zorns'",
        "id": "187",
        "type": 0,
        "description": "Verringert die Manakosten Eures Zaubers 'Hammer des Zorns' um 100%.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41097",
        "spellKey": "54926",
        "spellId": "54926",
        "prettyName": "",
        "typeOrder": 2
    },
    "188": {
        "name": "Glyphe 'Kreuzfahrerstoß'",
        "id": "188",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41098",
        "spellKey": "54927",
        "spellId": "54927",
        "prettyName": "",
        "typeOrder": 2
    },
    "189": {
        "name": "Glyphe 'Weihe'",
        "id": "189",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41099",
        "spellKey": "54928",
        "spellId": "54928",
        "prettyName": "",
        "typeOrder": 2
    },
    "190": {
        "name": "Glyphe 'Rechtschaffene Verteidigung'",
        "id": "190",
        "type": 0,
        "description": "Increases the chance for your Righteous Defense ability to work successfully by 8% on each target.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41100",
        "spellKey": "54929",
        "spellId": "54929",
        "prettyName": "",
        "typeOrder": 2
    },
    "191": {
        "name": "Glyphe 'Schild des Rächers'",
        "id": "191",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41101",
        "spellKey": "54930",
        "spellId": "54930",
        "prettyName": "",
        "typeOrder": 2
    },
    "192": {
        "name": "Glyphe 'Böses vertreiben'",
        "id": "192",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41102",
        "spellKey": "54931",
        "spellId": "54931",
        "prettyName": "",
        "typeOrder": 2
    },
    "193": {
        "name": "Glyphe 'Exorzismus'",
        "id": "193",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41103",
        "spellKey": "54934",
        "spellId": "54934",
        "prettyName": "",
        "typeOrder": 2
    },
    "194": {
        "name": "Glyphe 'Läuterung'",
        "id": "194",
        "type": 0,
        "description": "Verringert die Manakosten Eures Zaubers 'Reinigung des Glaubens' um 20%.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41104",
        "spellKey": "54935",
        "spellId": "54935",
        "prettyName": "",
        "typeOrder": 2
    },
    "195": {
        "name": "Glyphe 'Lichtblitz'",
        "id": "195",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41105",
        "spellKey": "54936",
        "spellId": "54936",
        "prettyName": "",
        "typeOrder": 2
    },
    "196": {
        "name": "Glyphe 'Heiliges Licht'",
        "id": "196",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41106",
        "spellKey": "54937",
        "spellId": "54937",
        "prettyName": "",
        "typeOrder": 2
    },
    "197": {
        "name": "Glyphe 'Zornige Vergeltung'",
        "id": "197",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41107",
        "spellKey": "54938",
        "spellId": "54938",
        "prettyName": "",
        "typeOrder": 2
    },
    "198": {
        "name": "Glyphe 'Göttlichkeit'",
        "id": "198",
        "type": 0,
        "description": "Wenn Ihr Euren Zauber 'Handauflegung' nutzt, werden sofort 10% Eures maximalen Manas wiederhergestellt.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41108",
        "spellKey": "54939",
        "spellId": "54939",
        "prettyName": "",
        "typeOrder": 2
    },
    "199": {
        "name": "Glyphe 'Siegel der Weisheit'",
        "id": "199",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41109",
        "spellKey": "54940",
        "spellId": "54940",
        "prettyName": "",
        "typeOrder": 2
    },
    "200": {
        "name": "Glyphe 'Siegel des Lichts'",
        "id": "200",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "41110",
        "spellKey": "54943",
        "spellId": "54943",
        "prettyName": "",
        "typeOrder": 2
    },
    "452": {
        "name": "Glyphe 'Segen der Könige'",
        "id": "452",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorpaladin",
        "itemId": "43365",
        "spellKey": "57937",
        "spellId": "57937",
        "prettyName": "",
        "typeOrder": 2
    },
    "453": {
        "name": "Glyphe 'Segen der Macht'",
        "id": "453",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers 'Segen der Macht' um 50%.",
        "icon": "inv_glyph_minorpaladin",
        "itemId": "43340",
        "spellKey": "57958",
        "spellId": "57958",
        "prettyName": "",
        "typeOrder": 2
    },
    "454": {
        "name": "Glyphe 'Segen der Weisheit'",
        "id": "454",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers 'Siegel der Einsicht' um 50%.",
        "icon": "inv_glyph_minorpaladin",
        "itemId": "43366",
        "spellKey": "57979",
        "spellId": "57979",
        "prettyName": "",
        "typeOrder": 2
    },
    "455": {
        "name": "Glyphe 'Handauflegung'",
        "id": "455",
        "type": 1,
        "description": "Verringert die Abklingzeit Eures Zaubers 'Handauflegung' um 3 Min.",
        "icon": "inv_glyph_minorpaladin",
        "itemId": "43367",
        "spellKey": "57955",
        "spellId": "57955",
        "prettyName": "",
        "typeOrder": 2
    },
    "456": {
        "name": "Glyphe 'Untote spüren'",
        "id": "456",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers 'Siegel der Wahrheit' um 50%.",
        "icon": "inv_glyph_minorpaladin",
        "itemId": "43368",
        "spellKey": "57947",
        "spellId": "57947",
        "prettyName": "",
        "typeOrder": 2
    },
    "457": {
        "name": "Glyphe 'Der Weise'",
        "id": "457",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers 'Siegel der Gerechtigkeit' um 50%.",
        "icon": "inv_glyph_minorpaladin",
        "itemId": "43369",
        "spellKey": "57954",
        "spellId": "57954",
        "prettyName": "",
        "typeOrder": 2
    },
    "559": {
        "name": "Glyphe 'Siegel des Blutes'",
        "id": "559",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "43867",
        "spellKey": "56420",
        "spellId": "56420",
        "prettyName": "",
        "typeOrder": 2
    },
    "560": {
        "name": "Glyphe 'Siegel der Rechtschaffenheit'",
        "id": "560",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "43868",
        "spellKey": "56414",
        "spellId": "56414",
        "prettyName": "",
        "typeOrder": 2
    },
    "561": {
        "name": "Glyphe 'Siegel der Vergeltung'",
        "id": "561",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "43869",
        "spellKey": "56416",
        "spellId": "56416",
        "prettyName": "",
        "typeOrder": 2
    },
    "701": {
        "name": "Glyphe 'Flamme des Glaubens'",
        "id": "701",
        "type": 0,
        "description": "Euer Zauber 'Flamme des Glaubens' kostet kein Mana.",
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45741",
        "spellKey": "63218",
        "spellId": "63218",
        "prettyName": "",
        "typeOrder": 2
    },
    "702": {
        "name": "Glyphe 'Hammer der Rechtschaffenen'",
        "id": "702",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45742",
        "spellKey": "63219",
        "spellId": "63219",
        "prettyName": "",
        "typeOrder": 2
    },
    "703": {
        "name": "Glyphe 'Göttlicher Sturm'",
        "id": "703",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45743",
        "spellKey": "63220",
        "spellId": "63220",
        "prettyName": "",
        "typeOrder": 2
    },
    "704": {
        "name": "Glyphe 'Schild der Rechtschaffenheit'",
        "id": "704",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45744",
        "spellKey": "63222",
        "spellId": "63222",
        "prettyName": "",
        "typeOrder": 2
    },
    "705": {
        "name": "Glyphe 'Göttliche Bitte'",
        "id": "705",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45745",
        "spellKey": "63223",
        "spellId": "63223",
        "prettyName": "",
        "typeOrder": 2
    },
    "706": {
        "name": "Glyphe 'Heiliger Schock'",
        "id": "706",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45746",
        "spellKey": "63224",
        "spellId": "63224",
        "prettyName": "",
        "typeOrder": 2
    },
    "707": {
        "name": "Glyphe 'Erlösung'",
        "id": "707",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorpaladin",
        "itemId": "45747",
        "spellKey": "63225",
        "spellId": "63225",
        "prettyName": "",
        "typeOrder": 2
    }
}
    ]
}