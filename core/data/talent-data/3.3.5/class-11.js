{
    "talentData": {
        "characterClass": {
            "classId": 11,
            "name": "Druide",
            "powerType": "MANA",
            "powerTypeId": 0,
            "powerTypeSlug": "mana"
        },
        "talentTrees": [
            {
                "name": "Gleichgewicht",
                "icon": "spell_nature_starfall",
                "backgroundFile": "DruidBalance",
                "overlayColor": "#cc4ccc",
                "description": "Kann die Gestalt eines machtvollen Mondkin annehmen. Seine Arkan- und Naturmagie zerstört Feinde aus der Entfernung.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 762,
                        "name": "Zorniges Sternenlicht",
                        "icon": "spell_nature_abolishmagic",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Zorn' und 'Sternenfeuer' um 0.1 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Zorn' und 'Sternenfeuer' um 0.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Zorn' und 'Sternenfeuer' um 0.3 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Zorn' und 'Sternenfeuer' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Zorn' und 'Sternenfeuer' um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2238,
                        "name": "Genesis",
                        "icon": "spell_arcane_arcane03",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer regelmäßigen Zauberschadens- und Heilungseffekte um 1%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer regelmäßigen Zauberschadens- und Heilungseffekte um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer regelmäßigen Zauberschadens- und Heilungseffekte um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer regelmäßigen Zauberschadens- und Heilungseffekte um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden und die Heilung Eurer regelmäßigen Zauberschadens- und Heilungseffekte um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 783,
                        "name": "Mondschein",
                        "icon": "spell_nature_sentinal",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Mondfeuer', 'Sternenfeuer', 'Sternenregen', 'Zorn', 'Heilende Berührung', 'Pflege', 'Nachwachsen' und 'Verjüngung' um 3%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Mondfeuer', 'Sternenfeuer', 'Sternenregen', 'Zorn', 'Heilende Berührung', 'Pflege', 'Nachwachsen' und 'Verjüngung' um 6%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Mondfeuer', 'Sternenfeuer', 'Sternenregen', 'Zorn', 'Heilende Berührung', 'Pflege', 'Nachwachsen' und 'Verjüngung' um 9%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1822,
                        "name": "Erhabenheit der Natur",
                        "icon": "inv_staff_01",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Zorn', 'Sternenfeuer', 'Sternenregen', 'Pflege' und 'Heilende Berührung' um 2%."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer Zauber 'Zorn', 'Sternenfeuer', 'Sternenregen', 'Pflege' und 'Heilende Berührung' um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 763,
                        "name": "Verbessertes Mondfeuer",
                        "icon": "spell_nature_starfall",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden sowie die Chance für einen kritischen Treffer Eures Zaubers 'Mondfeuer' um 5%."
                            },
                            {
                                "description": "Erhöht den Schaden sowie die Chance für einen kritischen Treffer Eures Zaubers 'Mondfeuer' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 782,
                        "name": "Dornenranken",
                        "icon": "spell_nature_thorns",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Eure Zauber 'Dornen' und 'Wucherwurzeln' verursachten Schaden um 25% und den durch Eure Treants verursachten Schaden um 5%. Zusätzlich hat sowohl der von Euren Treants verursachte als auch der von Euch erlittene Schaden, während 'Baumrinde' aktiv ist, eine Chance von 5%, das Ziel 3 Sek. lang benommen zu machen."
                            },
                            {
                                "description": "Erhöht den durch Eure Zauber 'Dornen' und 'Wucherwurzeln' verursachten Schaden um 50% und den durch Eure Treants verursachten Schaden um 10%. Zusätzlich hat sowohl der von Euren Treants verursachte als auch der von Euch erlittene Schaden, während 'Baumrinde' aktiv ist, eine Chance von 10%, das Ziel 3 Sek. lang benommen zu machen."
                            },
                            {
                                "description": "Erhöht den durch Eure Zauber 'Dornen' und 'Wucherwurzeln' verursachten Schaden um 75% und den durch Eure Treants verursachten Schaden um 15%. Zusätzlich hat sowohl der von Euren Treants verursachte als auch der von Euch erlittene Schaden, während 'Baumrinde' aktiv ist, eine Chance von 15%, das Ziel 3 Sek. lang benommen zu machen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 789,
                        "name": "Anmut der Natur",
                        "icon": "spell_nature_naturesblessing",
                        "x": 1,
                        "y": 2,
                        "req": 1822,
                        "ranks": [
                            {
                                "description": "Kritische Zaubertreffer von unregelmäßigen Zaubern haben eine Chance von 33%, Euch den Segen der Natur zu gewähren, der Euer Zaubertempo 3 Sek. lang um 20% erhöht."
                            },
                            {
                                "description": "Kritische Zaubertreffer von unregelmäßigen Zaubern haben eine Chance von 66%, Euch den Segen der Natur zu gewähren, der Euer Zaubertempo 3 Sek. lang um 20% erhöht."
                            },
                            {
                                "description": "Kritische Zaubertreffer von unregelmäßigen Zaubern haben eine Chance von 100%, Euch den Segen der Natur zu gewähren, der Euer Zaubertempo 3 Sek. lang um 20% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2240,
                        "name": "Pracht der Natur",
                        "icon": "spell_nature_natureguardian",
                        "x": 2,
                        "y": 2,
                        "req": 1822,
                        "ranks": [
                            {
                                "description": "Erhöht die Dauer von 'Mondfeuer' und 'Verjüngung' um 3 Sek., von 'Nachwachsen' um 6 Sek. und von 'Insektenschwarm' und 'Blühendes Leben' um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 764,
                        "name": "Reichweite der Natur",
                        "icon": "spell_nature_naturetouchgrow",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Gleichgewichtszauber sowie der Fähigkeit 'Feenfeuer (Tiergestalt)' um 10% und verringert die von Euren Gleichgewichtszaubern erzeugte Bedrohung um 15%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Gleichgewichtszauber sowie der Fähigkeit 'Feenfeuer (Tiergestalt)' um 20% und verringert die von Euren Gleichgewichtszaubern erzeugte Bedrohung um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 792,
                        "name": "Rache",
                        "icon": "spell_nature_purge",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Schadensbonus für kritische Treffer Eurer Zauber 'Sternenfeuer', 'Sternenregen', 'Mondfeuer' und 'Zorn' um 20%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus für kritische Treffer Eurer Zauber 'Sternenfeuer', 'Sternenregen', 'Mondfeuer' und 'Zorn' um 40%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus für kritische Treffer Eurer Zauber 'Sternenfeuer', 'Sternenregen', 'Mondfeuer' und 'Zorn' um 60%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus für kritische Treffer Eurer Zauber 'Sternenfeuer', 'Sternenregen', 'Mondfeuer' und 'Zorn' um 80%."
                            },
                            {
                                "description": "Erhöht den Schadensbonus für kritische Treffer Eurer Zauber 'Sternenfeuer', 'Sternenregen', 'Mondfeuer' und 'Zorn' um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 784,
                        "name": "Himmlischer Fokus",
                        "icon": "spell_arcane_starfire",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Verzögerung der Zauberzeit beim Wirken von 'Sternenfeuer', 'Winterschlaf' und 'Hurrikan' um 23% und erhöht Euer gesamtes Zaubertempo um 1%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Verzögerung der Zauberzeit beim Wirken von 'Sternenfeuer', 'Winterschlaf' und 'Hurrikan' um 46% und erhöht Euer gesamtes Zaubertempo um 2%."
                            },
                            {
                                "description": "Verringert die durch erlittenen Schaden verursachte Verzögerung der Zauberzeit beim Wirken von 'Sternenfeuer', 'Winterschlaf' und 'Hurrikan' um 70% und erhöht Euer gesamtes Zaubertempo um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1782,
                        "name": "Weisheit des Mondes",
                        "icon": "ability_druid_lunarguidance",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubermacht um 4% Eurer gesamten Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 8% Eurer gesamten Intelligenz."
                            },
                            {
                                "description": "Erhöht Eure Zaubermacht um 12% Eurer gesamten Intelligenz."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 788,
                        "name": "Insektenschwarm",
                        "icon": "spell_nature_insectswarm",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "cost": "8% des Grundmanas",
                                "description": "Das feindliche Ziel wird von Insekten umschwärmt, die dessen Trefferchance um 3% verringern und beim Betroffenen über 12 Sek. 144 Naturschaden verursachen."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2239,
                        "name": "Verbesserter Insektenschwarm",
                        "icon": "spell_nature_insectswarm",
                        "x": 2,
                        "y": 4,
                        "req": 788,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Zorn' um 1%, wenn das Ziel von Eurem Zauber 'Insektenschwarm' betroffen ist. Erhöht die kritische Trefferchance Eures Zaubers 'Sternenfeuer' um 1%, wenn das Ziel von Eurem Zauber 'Mondfeuer' betroffen ist."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Zorn' um 2%, wenn das Ziel von Eurem Zauber 'Insektenschwarm' betroffen ist. Erhöht die kritische Trefferchance Eures Zaubers 'Sternenfeuer' um 2%, wenn das Ziel von Eurem Zauber 'Mondfeuer' betroffen ist."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Zorn' um 3%, wenn das Ziel von Eurem Zauber 'Insektenschwarm' betroffen ist. Erhöht die kritische Trefferchance Eures Zaubers 'Sternenfeuer' um 3%, wenn das Ziel von Eurem Zauber 'Mondfeuer' betroffen ist."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1784,
                        "name": "Traumzustand",
                        "icon": "ability_druid_dreamstate",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Stellt alle 5 Sek. Mana entsprechend 4% Eurer Intelligenz wieder her, sogar beim Wirken von Zaubern."
                            },
                            {
                                "description": "Stellt alle 5 Sek. Mana entsprechend 7% Eurer Intelligenz wieder her, sogar beim Wirken von Zaubern."
                            },
                            {
                                "description": "Stellt alle 5 Sek. Mana entsprechend 10% Eurer Intelligenz wieder her, sogar beim Wirken von Zaubern."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 790,
                        "name": "Mondfuror",
                        "icon": "spell_nature_moonglow",
                        "x": 1,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Sternenfeuer', 'Mondfeuer' und 'Zorn' um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Sternenfeuer', 'Mondfeuer' und 'Zorn' um 6%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Sternenfeuer', 'Mondfeuer' und 'Zorn' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1783,
                        "name": "Gleichgewicht der Kräfte",
                        "icon": "ability_druid_balanceofpower",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Zaubertrefferchance um 2% und verringert den erlittenen Zauberschaden um 3%."
                            },
                            {
                                "description": "Erhöht Eure Zaubertrefferchance um 4% und verringert den erlittenen Zauberschaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 793,
                        "name": "Mondkingestalt",
                        "icon": "spell_nature_forceofnature",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cost": "13% des Grundmanas",
                                "description": "Verwandelt Euch in eine Mondkingestalt. In dieser Gestalt wird der Rüstungsbonus durch Gegenstände um 370% erhöht und jeglicher erlittene Schaden, während Ihr von Betäubungseffekten betroffen seid, um 15% verringert. Zusätzlich wird die kritische Zaubertrefferchance aller Gruppen- und Schlachtzugsmitglieder in einem Umkreis von 100 Metern um 5% erhöht. In dieser Gestalt erzielte kritische Zaubertreffer auf ein Einzelziel haben eine Chance, sofort 2% Eures gesamten Manas wiederherzustellen. In Mondkingestalt können keine Heil- oder Wiederbelebungszauber gewirkt werden.<br/><br/>Der Akt des Gestaltwandelns befreit den Zaubernden von Verwandlungs- und bewegungseinschränkenden Effekten."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1912,
                        "name": "Verbesserte Mondkingestalt",
                        "icon": "ability_druid_improvedmoonkinform",
                        "x": 2,
                        "y": 6,
                        "req": 793,
                        "ranks": [
                            {
                                "description": "Eure von 'Aura des Mondkin' beeinflussten Verbündeten gewinnen auch 1% Tempo und Ihr gewinnt 10% Eurer Willenskraft als zusätzlichen Zauberschaden."
                            },
                            {
                                "description": "Eure von 'Aura des Mondkin' beeinflussten Verbündeten gewinnen auch 2% Tempo und Ihr gewinnt 20% Eurer Willenskraft als zusätzlichen Zauberschaden."
                            },
                            {
                                "description": "Eure von 'Aura des Mondkin' beeinflussten Verbündeten gewinnen auch 3% Tempo und Ihr gewinnt 30% Eurer Willenskraft als zusätzlichen Zauberschaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1785,
                        "name": "Verbessertes Feenfeuer",
                        "icon": "spell_nature_faeriefire",
                        "x": 3,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Feenfeuer' erhöht zusätzlich die Chance, dass das Ziel von Zaubern getroffen wird, um 1% und erhöht die kritische Trefferchance Eurer Schadenszauber gegen das von 'Feenfeuer' betroffene Ziel um 1%."
                            },
                            {
                                "description": "Euer Zauber 'Feenfeuer' erhöht zusätzlich die Chance, dass das Ziel von Zaubern getroffen wird, um 2% und erhöht die kritische Trefferchance Eurer Schadenszauber gegen das von 'Feenfeuer' betroffene Ziel um 2%."
                            },
                            {
                                "description": "Euer Zauber 'Feenfeuer' erhöht zusätzlich die Chance, dass das Ziel von Zaubern getroffen wird, um 3% und erhöht die kritische Trefferchance Eurer Schadenszauber gegen das von 'Feenfeuer' betroffene Ziel um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1913,
                        "name": "Eulkinraserei",
                        "icon": "ability_druid_owlkinfrenzy",
                        "x": 0,
                        "y": 7,
                        "req": 793,
                        "ranks": [
                            {
                                "description": "In Mondkingestalt haben gegen Euch geführte Angriffe eine Chance von 5%, Euch in Raserei zu versetzen, wodurch Euer verursachter Schaden um 10% erhöht wird, Ihr beim Wirken von Gleichgewichtszaubern durch erlittenen Schaden keine Verzögerung der Zauberzeit erleidet und alle 2 Sek. 2% Eures Grundmanas wiederhergestellt werden. Hält 10 Sek. lang an."
                            },
                            {
                                "description": "In Mondkingestalt haben gegen Euch geführte Angriffe eine Chance von 10%, Euch in Raserei zu versetzen, wodurch Euer verursachter Schaden um 10% erhöht wird, Ihr beim Wirken von Gleichgewichtszaubern durch erlittenen Schaden keine Verzögerung der Zauberzeit erleidet und alle 2 Sek. 2% Eures Grundmanas wiederhergestellt werden. Hält 10 Sek. lang an."
                            },
                            {
                                "description": "In Mondkingestalt haben gegen Euch geführte Angriffe eine Chance von 15%, Euch in Raserei zu versetzen, wodurch Euer verursachter Schaden um 10% erhöht wird, Ihr beim Wirken von Gleichgewichtszaubern durch erlittenen Schaden keine Verzögerung der Zauberzeit erleidet und alle 2 Sek. 2% Eures Grundmanas wiederhergestellt werden. Hält 10 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1786,
                        "name": "Zorn des Cenarius",
                        "icon": "ability_druid_twilightswrath",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Sternenfeuer' erhält zusätzliche 4% und Euer Zauber 'Zorn' zusätzliche 2% Bonus durch Zauberschaden beeinflussende Effekte."
                            },
                            {
                                "description": "Euer Zauber 'Sternenfeuer' erhält zusätzliche 8% und Euer Zauber 'Zorn' zusätzliche 4% Bonus durch Zauberschaden beeinflussende Effekte."
                            },
                            {
                                "description": "Euer Zauber 'Sternenfeuer' erhält zusätzliche 12% und Euer Zauber 'Zorn' zusätzliche 6% Bonus durch Zauberschaden beeinflussende Effekte."
                            },
                            {
                                "description": "Euer Zauber 'Sternenfeuer' erhält zusätzliche 16% und Euer Zauber 'Zorn' zusätzliche 8% Bonus durch Zauberschaden beeinflussende Effekte."
                            },
                            {
                                "description": "Euer Zauber 'Sternenfeuer' erhält zusätzliche 20% und Euer Zauber 'Zorn' zusätzliche 10% Bonus durch Zauberschaden beeinflussende Effekte."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1924,
                        "name": "Finsternis",
                        "icon": "ability_druid_eclipse",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erzielt Ihr mit 'Sternenfeuer' einen kritischen Treffer, habt Ihr eine Chance von 33%, den verursachten Schaden Eures Zaubers 'Zorn' um 40% zu erhöhen. Erzielt Ihr mit 'Zorn' einen kritischen Treffer, habt Ihr eine Chance von 20%, die kritische Trefferchance Eures Zaubers 'Sternenfeuer' um 40% zu erhöhen. Jeder Effekt hält 15 Sek. lang an und hat eine Abklingzeit von 30 Sek. Es kann jeweils nur ein Effekt gleichzeitig auftreten."
                            },
                            {
                                "description": "Erzielt Ihr mit 'Sternenfeuer' einen kritischen Treffer, habt Ihr eine Chance von 66%, den verursachten Schaden Eures Zaubers 'Zorn' um 40% zu erhöhen. Erzielt Ihr mit 'Zorn' einen kritischen Treffer, habt Ihr eine Chance von 40%, die kritische Trefferchance Eures Zaubers 'Sternenfeuer' um 40% zu erhöhen. Jeder Effekt hält 15 Sek. lang an und hat eine Abklingzeit von 30 Sek. Es kann jeweils nur ein Effekt gleichzeitig auftreten."
                            },
                            {
                                "description": "Erzielt Ihr mit 'Sternenfeuer' einen kritischen Treffer, habt Ihr eine Chance von 100%, den verursachten Schaden Eures Zaubers 'Zorn' um 40% zu erhöhen. Erzielt Ihr mit 'Zorn' einen kritischen Treffer, habt Ihr eine Chance von 60%, die kritische Trefferchance Eures Zaubers 'Sternenfeuer' um 40% zu erhöhen. Jeder Effekt hält 15 Sek. lang an und hat eine Abklingzeit von 30 Sek. Es kann jeweils nur ein Effekt gleichzeitig auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1923,
                        "name": "Taifun",
                        "icon": "ability_druid_typhoon",
                        "x": 1,
                        "y": 8,
                        "req": 793,
                        "ranks": [
                            {
                                "description": "Ihr beschwört einen wilden Taifun, der an feindlichen Zielen bei Kontakt 400 Naturschaden verursacht, sie zurückstößt und 6 Sek. lang benommen macht.",
                                "cost": "25% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1787,
                        "name": "Naturgewalt",
                        "icon": "ability_druid_forceofnature",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Beschwört 3 Treants, die das Ziel 30 Sek. lang angreifen.",
                                "cost": "12% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1925,
                        "name": "Windböen",
                        "icon": "ability_druid_galewinds",
                        "x": 3,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Hurrikan' und 'Taifun' um 15% und die Reichweite Eures Zaubers 'Wirbelsturm' um 2 Meter."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Hurrikan' und 'Taifun' um 30% und die Reichweite Eures Zaubers 'Wirbelsturm' um 4 Meter."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1928,
                        "name": "Erde und Mond",
                        "icon": "ability_druid_earthandsky",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Zorn' und 'Sternenfeuer' haben eine Chance von 100%, beim Ziel den Effekt 'Erde und Mond' auszulösen. Dadurch wird der erlittene Zauberschaden des Ziels 12 Sek. lang um 4% erhöht. Erhöht auch Euren Zauberschaden um 2%."
                            },
                            {
                                "description": "Eure Zauber 'Zorn' und 'Sternenfeuer' haben eine Chance von 100%, beim Ziel den Effekt 'Erde und Mond' auszulösen. Dadurch wird der erlittene Zauberschaden des Ziels 12 Sek. lang um 9% erhöht. Erhöht auch Euren verursachten Zauberschaden um 4%."
                            },
                            {
                                "description": "Eure Zauber 'Zorn' und 'Sternenfeuer' haben eine Chance von 100%, beim Ziel den Effekt 'Erde und Mond' auszulösen. Dadurch wird der erlittene Zauberschaden des Ziels 12 Sek. lang um 13% erhöht. Erhöht auch Euren verursachten Zauberschaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1926,
                        "name": "Sternenregen",
                        "icon": "ability_druid_starfall",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1,5 Min. Abklingzeit",
                                "cost": "35% des Grundmanas",
                                "description": "Ihr beschwört einen Sternenschauer vom Himmel, der auf alle Ziele innerhalb von 30 Metern um den Zaubernden herniedergeht. Jeder Stern verursacht 145 bis 167 Arkanschaden sowie an Gegnern innerhalb von 5 Metern um das feindliche Ziel zusätzliche 26 Arkanschaden. Es werden maximal 20 Sterne beschworen. Hält 10 Sek. lang an. Gestaltwandel in eine Tiergestalt oder Aufsitzen brechen den Effekt ab. Jedweder Effekt, der Euch die Kontrolle über Euren Charakter verlieren lässt, führt zum Abbruch von 'Sternenregen'."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0

            },
            {
                "name": "Wilder Kampf",
                "icon": "ability_racial_bearform",
                "backgroundFile": "DruidFeralCombat",
                "overlayColor": "#ff00ff",
                "description": "Nimmt die Gestalt einer Großkatze an, um zu beißen und blutende Wunden zu schlagen oder eine mächtige Bärengestalt, um Schaden zu absorbieren und Verbündete zu schützen.",
                "treeNo": 1,
                "roles": {
                    "tank": true,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 796,
                        "name": "Wildheit",
                        "icon": "ability_hunter_pet_hyena",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeiten 'Zermalmen', 'Prankenhieb', 'Klaue', 'Krallenhieb' und 'Zerfleischen' um 1 Wut oder Energie."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeiten 'Zermalmen', 'Prankenhieb', 'Klaue', 'Krallenhieb' und 'Zerfleischen' um 2 Wut oder Energie."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeiten 'Zermalmen', 'Prankenhieb', 'Klaue', 'Krallenhieb' und 'Zerfleischen' um 3 Wut oder Energie."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeiten 'Zermalmen', 'Prankenhieb', 'Klaue', 'Krallenhieb' und 'Zerfleischen' um 4 Wut oder Energie."
                            },
                            {
                                "description": "Verringert die Kosten Eurer Fähigkeiten 'Zermalmen', 'Prankenhieb', 'Klaue', 'Krallenhieb' und 'Zerfleischen' um 5 Wut oder Energie."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 795,
                        "name": "Wilde Aggression",
                        "icon": "ability_druid_demoralizingroar",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Angriffskraft-Reduzierung Eures demoralisierenden Gebrülls um 8% und den durch Eure Fähigkeit 'Wilder Biss' verursachten Schaden um 3%."
                            },
                            {
                                "description": "Erhöht die Angriffskraft-Reduzierung Eures demoralisierenden Gebrülls um 16% und den durch Eure Fähigkeit 'Wilder Biss' verursachten Schaden um 6%."
                            },
                            {
                                "description": "Erhöht die Angriffskraft-Reduzierung Eures demoralisierenden Gebrülls um 24% und den durch Eure Fähigkeit 'Wilder Biss' verursachten Schaden um 9%."
                            },
                            {
                                "description": "Erhöht die Angriffskraft-Reduzierung Eures demoralisierenden Gebrülls um 32% und den durch Eure Fähigkeit 'Wilder Biss' verursachten Schaden um 12%."
                            },
                            {
                                "description": "Erhöht die Angriffskraft-Reduzierung Eures demoralisierenden Gebrülls um 40% und den durch Eure Fähigkeit 'Wilder Biss' verursachten Schaden um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 799,
                        "name": "Instinkt der Wildnis",
                        "icon": "ability_ambush",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Prankenhieb' verursachten Schaden um 10% und verringert die Chance, dass Gegner Euch beim Schleichen entdecken."
                            },
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Prankenhieb' verursachten Schaden um 20% und verringert die Chance, dass Gegner Euch beim Schleichen entdecken."
                            },
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Prankenhieb' verursachten Schaden um 30% und verringert die Chance, dass Gegner Euch beim Schleichen entdecken."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 805,
                        "name": "Ungezähmte Wut",
                        "icon": "ability_druid_ravage",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Eure Fähigkeiten 'Klaue', 'Krallenhieb', 'Zerfleischen (Katze)', 'Zerfleischen (Bär)' und 'Zermalmen' verursachten Schaden um 10%."
                            },
                            {
                                "description": "Erhöht den durch Eure Fähigkeiten 'Klaue', 'Krallenhieb', 'Zerfleischen (Katze)', 'Zerfleischen (Bär)' und 'Zermalmen' verursachten Schaden um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 794,
                        "name": "Dickes Fell",
                        "icon": "inv_misc_pelt_bear_03",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Euren durch Gegenstände aus Stoff und Leder erzielten Rüstungswert um 4%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände aus Stoff und Leder erzielten Rüstungswert um 7%."
                            },
                            {
                                "description": "Erhöht Euren durch Gegenstände aus Stoff und Leder erzielten Rüstungswert um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 807,
                        "name": "Schnelligkeit der Wildnis",
                        "icon": "spell_nature_spiritwolf",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Bewegungstempo in Katzengestalt um 15% und Eure Ausweichchance um 2%, während Ihr in Katzen-, Bären- und Terrorbärengestalt seid."
                            },
                            {
                                "description": "Erhöht Euer Bewegungstempo in Katzengestalt um 30% und Eure Ausweichchance um 4%, während Ihr in Katzen-, Bären- und Terrorbärengestalt seid."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1162,
                        "name": "Überlebensinstinkte",
                        "icon": "ability_druid_tigersroar",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Wenn aktiv, gewährt Euch diese Fähigkeit in Bären-, Terrorbären- oder Katzengestalt 20 Sek. lang 30% Eurer gesamten Gesundheit. Wenn der Effekt endet, geht die zusätzliche Gesundheit verloren."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 798,
                        "name": "Geschärfte Klauen",
                        "icon": "inv_misc_monsterclaw_04",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer, während Ihr in Bären-, Terrorbären- oder Katzengestalt seid, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer, während Ihr in Bären-, Terrorbären- oder Katzengestalt seid, um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer, während Ihr in Bären-, Terrorbären- oder Katzengestalt seid, um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 802,
                        "name": "Schreddernde Angriffe",
                        "icon": "spell_shadow_vampiricaura",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeit 'Schreddern' um 9 und die Wutkosten Eurer Fähigkeit 'Aufschlitzen' um 1."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeit 'Schreddern' um 18 und die Wutkosten Eurer Fähigkeit 'Aufschlitzen' um 2."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 803,
                        "name": "Raubtierschläge",
                        "icon": "ability_hunter_pet_cat",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Nahkampfangriffskraft in Katzen-, Bären- und Terrorbärengestalt um 50% Eurer aktuellen Stufe sowie um 7% der Angriffskraft Eurer ausgerüsteten Waffe. Zudem haben Eure Finishing-Moves eine Chance von 7% pro Combopunkt, Euren nächsten Naturzauber mit einer Zauberzeit von unter 10 Sek. zu einem Spontanzauber werden zu lassen."
                            },
                            {
                                "description": "Erhöht Eure Nahkampfangriffskraft in Katzen-, Bären- und Terrorbärengestalt um 100% Eurer aktuellen Stufe sowie um 14% der Angriffskraft Eurer ausgerüsteten Waffe. Zudem haben Eure Finishing-Moves eine Chance von 13% pro Combopunkt, Euren nächsten Naturzauber mit einer Zauberzeit von unter 10 Sek. zu einem Spontanzauber werden zu lassen."
                            },
                            {
                                "description": "Erhöht Eure Nahkampfangriffskraft in Katzen-, Bären- und Terrorbärengestalt um 150% Eurer aktuellen Stufe sowie um 20% der Angriffskraft Eurer ausgerüsteten Waffe. Zudem haben Eure Finishing-Moves eine Chance von 20% pro Combopunkt, Euren nächsten Naturzauber mit einer Zauberzeit von unter 10 Sek. zu einem Spontanzauber werden zu lassen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 801,
                        "name": "Urfuror",
                        "icon": "ability_racial_cannibalize",
                        "x": 2,
                        "y": 3,
                        "req": 798,
                        "ranks": [
                            {
                                "description": "Verleiht Euch eine Chance von 50%, zusätzliche 5 Wut zu gewinnen, wenn Ihr in Bären- oder Terrorbärengestalt einen kritischen Treffer erzielt. Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte erzeugen, eine Chance von 50%, dem Ziel einen zusätzlichen Combopunkt hinzuzufügen, wenn Ihr Euch in Katzengestalt befindet."
                            },
                            {
                                "description": "Verleiht Euch eine Chance von 100%, zusätzliche 5 Wut zu gewinnen, wenn Ihr in Bären- oder Terrorbärengestalt einen kritischen Treffer erzielt. Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte erzeugen, eine Chance von 100%, dem Ziel einen zusätzlichen Combopunkt hinzuzufügen, wenn Ihr Euch in Katzengestalt befindet."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1914,
                        "name": "Wilde Präzision",
                        "icon": "ability_druid_primalprecision",
                        "x": 3,
                        "y": 3,
                        "req": 798,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Waffenkunde um 5 und 40% der Energiekosten eines Finishing-Moves werden Euch zurückerstattet, wenn der Move verfehlt."
                            },
                            {
                                "description": "Erhöht Eure Waffenkunde um 10 und 80% der Energiekosten eines Finishing-Moves werden Euch zurückerstattet, wenn der Move verfehlt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 797,
                        "name": "Brutaler Hieb",
                        "icon": "ability_druid_bash",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Betäubungsdauer Eurer Fähigkeiten 'Hieb' und 'Anspringen' um 0.5 Sek. und verringert die Abklingzeit von 'Hieb' um 15 Sek."
                            },
                            {
                                "description": "Erhöht die Betäubungsdauer Eurer Fähigkeiten 'Hieb' und 'Anspringen' um 1 Sek. und verringert die Abklingzeit von 'Hieb' um 30 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 804,
                        "name": "Wilde Attacke",
                        "icon": "ability_hunter_pet_bear",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Lehrt 'Wilde Attacke (Bär)' und 'Wilde Attacke (Katze)'.<br/><br/>Wilde Attacke (Bär) - Stürmt auf einen Gegner zu, macht ihn unbeweglich und unterbricht gewirkte Zauber. Hält 4 Sek. lang an. Die Fähigkeit kann in Bären- und Terrorbärengestalt genutzt werden. 15 Sekunden Abklingzeit.<br/><br/>Wilde Attacke (Katze) - Springt hinter einen Gegner und macht ihn 3 Sek. lang benommen. 30 Sekunden Abklingzeit."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1792,
                        "name": "Beschützerinstinkt",
                        "icon": "ability_druid_healinginstincts",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Heilzauber um bis zu 35% Eurer Beweglichkeit und Eure in Katzengestalt erhaltene Heilung um 10%."
                            },
                            {
                                "description": "Erhöht Eure Heilzauber um bis zu 70% Eurer Beweglichkeit und Eure in Katzengestalt erhaltene Heilung um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2242,
                        "name": "Natürliche Reaktion",
                        "icon": "ability_bullrush",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance in Bärengestalt oder Terrorbärengestalt um 2%. Zusätzlich gewinnt Ihr bei jedem Ausweichen in Bärengestalt oder Terrorbärengestalt 1 Wut."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance in Bärengestalt oder Terrorbärengestalt um 4%. Zusätzlich gewinnt Ihr bei jedem Ausweichen in Bärengestalt oder Terrorbärengestalt 2 Wut."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance in Bärengestalt oder Terrorbärengestalt um 6%. Zusätzlich gewinnt Ihr bei jedem Ausweichen in Bärengestalt oder Terrorbärengestalt 3 Wut."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 808,
                        "name": "Herz der Wildnis",
                        "icon": "spell_holy_blessingofagility",
                        "x": 1,
                        "y": 5,
                        "req": 803,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Intelligenz um 4%. Zusätzlich wird in Bären- oder Terrorbärengestalt Eure Ausdauer um 2% erhöht, in Katzengestalt Eure Angriffskraft um 2%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 8%. Zusätzlich wird in Bären- oder Terrorbärengestalt Eure Ausdauer um 4% erhöht, in Katzengestalt Eure Angriffskraft um 4%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 12%. Zusätzlich wird in Bären- oder Terrorbärengestalt Eure Ausdauer um 6% erhöht, in Katzengestalt Eure Angriffskraft um 6%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 16%. Zusätzlich wird in Bären- oder Terrorbärengestalt Eure Ausdauer um 8% erhöht, in Katzengestalt Eure Angriffskraft um 8%."
                            },
                            {
                                "description": "Erhöht Eure Intelligenz um 20%. Zusätzlich wird in Bären- oder Terrorbärengestalt Eure Ausdauer um 10% erhöht, in Katzengestalt Eure Angriffskraft um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1794,
                        "name": "Überleben der Stärksten",
                        "icon": "ability_druid_enrage",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht alle Attribute um 2%, verringert die Chance, dass Ihr von Nahkampfangriffen kritisch getroffen werdet, um 2% und erhöht Euren durch Gegenstände aus Stoff und Leder erzielten Rüstungswert in der Bären- und Terrorbärengestalt um 11%."
                            },
                            {
                                "description": "Erhöht alle Attribute um 4%, verringert die Chance, dass Ihr von Nahkampfangriffen kritisch getroffen werdet, um 4% und erhöht Euren durch Gegenstände aus Stoff und Leder erzielten Rüstungswert in der Bären- und Terrorbärengestalt um 22%."
                            },
                            {
                                "description": "Erhöht alle Attribute um 6%, verringert die Chance, dass Ihr von Nahkampfangriffen kritisch getroffen werdet, um 6% und erhöht Euren durch Gegenstände aus Stoff und Leder erzielten Rüstungswert in der Bären- und Terrorbärengestalt um 33%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 809,
                        "name": "Rudelführer",
                        "icon": "spell_nature_unyeildingstamina",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Befindet Ihr Euch in Bären-, Terrorbären- oder Katzengestalt, erhöht das Talent 'Rudelführer' die kritische Distanz- und Nahkampftrefferchance aller Gruppen- und Schlachtzugsmitglieder in einem Umkreis von 100 Metern um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1798,
                        "name": "Verbesserter Rudelführer",
                        "icon": "spell_nature_unyeildingstamina",
                        "x": 2,
                        "y": 6,
                        "req": 809,
                        "ranks": [
                            {
                                "description": "Durch Eure Fähigkeit 'Rudelführer' heilen sich betroffene Ziele um 2% ihrer gesamten Gesundheit, wenn sie mit einem Nahkampf- oder Distanzangriff kritisch treffen. Dieser Heileffekt kann innerhalb von 6 Sek. nur einmal auftreten. Zusätzlich gewinnt Ihr 4% Eures gesamten Manas, wenn Ihr selbst von dieser Heilung profitiert."
                            },
                            {
                                "description": "Durch Eure Fähigkeit 'Rudelführer' heilen sich betroffene Ziele um 4% ihrer gesamten Gesundheit, wenn sie mit einem Nahkampf- oder Distanzangriff kritisch treffen. Dieser Heileffekt kann innerhalb von 6 Sek. nur einmal auftreten. Zusätzlich gewinnt Ihr 8% Eures gesamten Manas, wenn Ihr selbst von dieser Heilung profitiert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1793,
                        "name": "Wilde Zähigkeit",
                        "icon": "ability_druid_primaltenacity",
                        "x": 3,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Wirkungsdauer von Furchteffekten um 10% und verringert in Katzengestalt jeglichen erlittenen Schaden, während Ihr von Betäubungseffekten betroffen seid, um 10%."
                            },
                            {
                                "description": "Verringert die Wirkungsdauer von Furchteffekten um 20% und verringert in Katzengestalt jeglichen erlittenen Schaden, während Ihr von Betäubungseffekten betroffen seid, um 20%."
                            },
                            {
                                "description": "Verringert die Wirkungsdauer von Furchteffekten um 30% und verringert in Katzengestalt jeglichen erlittenen Schaden, während Ihr von Betäubungseffekten betroffen seid, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2241,
                        "name": "Alphatier",
                        "icon": "ability_druid_challangingroar",
                        "x": 0,
                        "y": 7,
                        "req": 809,
                        "ranks": [
                            {
                                "description": "Erhöht in Bären- oder Terrorbärengestalt Eure Angriffskraft um 2% und verringert Euren erlittenen Schaden um 4%."
                            },
                            {
                                "description": "Erhöht in Bären- oder Terrorbärengestalt Eure Angriffskraft um 4% und verringert Euren erlittenen Schaden um 8%."
                            },
                            {
                                "description": "Erhöht in Bären- oder Terrorbärengestalt Eure Angriffskraft um 6% und verringert Euren erlittenen Schaden um 12%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1795,
                        "name": "Instinkt des Raubtiers",
                        "icon": "ability_druid_predatoryinstincts",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "In Katzengestalt wird der Schaden Eurer kritischen Nahkampftreffer um 3% erhöht und der erlittene Schaden durch Effekte, die auf ein Gebiet wirken, um 10% verringert."
                            },
                            {
                                "description": "In Katzengestalt wird der Schaden Eurer kritischen Nahkampftreffer um 7% erhöht und der erlittene Schaden durch Effekte, die auf ein Gebiet wirken, um 20% verringert."
                            },
                            {
                                "description": "In Katzengestalt wird der Schaden Eurer kritischen Nahkampftreffer um 10% erhöht und der erlittene Schaden durch Effekte, die auf ein Gebiet wirken, um 30% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1919,
                        "name": "Infizierte Wunden",
                        "icon": "ability_druid_infectedwound",
                        "x": 3,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Angriffe 'Schreddern', 'Zermalmen' und 'Zerfleischen' schlagen dem Ziel eine infizierte Wunde. Die infizierte Wunde verringert das Bewegungstempo des Ziels um 16% und sein Angriffstempo um 6%. Hält 12 Sek. lang an."
                            },
                            {
                                "description": "Eure Angriffe 'Schreddern', 'Zermalmen' und 'Zerfleischen' schlagen dem Ziel eine infizierte Wunde. Die infizierte Wunde verringert das Bewegungstempo des Ziels um 34% und sein Angriffstempo um 14%. Hält 12 Sek. lang an."
                            },
                            {
                                "description": "Eure Angriffe 'Schreddern', 'Zermalmen' und 'Zerfleischen' schlagen dem Ziel eine infizierte Wunde. Die infizierte Wunde verringert das Bewegungstempo des Ziels um 50% und sein Angriffstempo um 20%. Hält 12 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1921,
                        "name": "König des Dschungels",
                        "icon": "ability_druid_kingofthejungle",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Für die Dauer Eurer Fähigkeit 'Wutanfall' wird Euer verursachter Schaden in Bären- oder Terrorbärengestalt um 5% erhöht und Eure Fähigkeit 'Tigerfuror' stellt sofort 20 Energie wieder her. Zudem werden die Manakosten der Bären- Katzen- und Terrorbärengestalt um 20% verringert."
                            },
                            {
                                "description": "Für die Dauer Eurer Fähigkeit 'Wutanfall' wird Euer verursachter Schaden in Bären- oder Terrorbärengestalt um 10% erhöht und Eure Fähigkeit 'Tigerfuror' stellt sofort 40 Energie wieder her. Zudem werden die Manakosten der Bären- Katzen- und Terrorbärengestalt um 40% verringert."
                            },
                            {
                                "description": "Für die Dauer Eurer Fähigkeit 'Wutanfall' wird Euer verursachter Schaden in Bären- oder Terrorbärengestalt um 15% erhöht und Eure Fähigkeit 'Tigerfuror' stellt sofort 60 Energie wieder her. Zudem werden die Manakosten der Bären- Katzen- und Terrorbärengestalt um 60% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1796,
                        "name": "Zerfleischen",
                        "icon": "ability_druid_mangle2",
                        "x": 1,
                        "y": 8,
                        "req": 809,
                        "ranks": [
                            {
                                "description": "Zerfleischt das Ziel, verursacht Schaden und lässt es 1 Min. lang zusätzlichen Schaden durch Blutungseffekte erleiden. Diese Fähigkeit kann in Katzen- und Terrorbärengestalt angewendet werden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1920,
                        "name": "Verbessertes Zerfleischen",
                        "icon": "ability_druid_mangle2",
                        "x": 2,
                        "y": 8,
                        "req": 1796,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Zerfleischen' (Bärengestalt) um 0.5 Sek. und verringert die Energiekosten Eurer Fähigkeit 'Zerfleischen' (Katzengestalt) um 2."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Zerfleischen' (Bärengestalt) um 1.0 Sek. und verringert die Energiekosten Eurer Fähigkeit 'Zerfleischen' (Katzengestalt) um 4."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeit 'Zerfleischen' (Bärengestalt) um 1.5 Sek. und verringert die Energiekosten Eurer Fähigkeit 'Zerfleischen' (Katzengestalt) um 6."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1918,
                        "name": "Beißen und Reißen",
                        "icon": "ability_druid_primalagression",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Zermalmen' und 'Schreddern' an blutenden Zielen verursachten Schaden um 4% und erhöht die kritische Trefferchance Eurer Fähigkeit 'Wilder Biss' auf blutenden Zielen um 5%."
                            },
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Zermalmen' und 'Schreddern' an blutenden Zielen verursachten Schaden um 8% und erhöht die kritische Trefferchance Eurer Fähigkeit 'Wilder Biss' auf blutenden Zielen um 10%."
                            },
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Zermalmen' und 'Schreddern' an blutenden Zielen verursachten Schaden um 12% und erhöht die kritische Trefferchance Eurer Fähigkeit 'Wilder Biss' auf blutenden Zielen um 15%."
                            },
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Zermalmen' und 'Schreddern' an blutenden Zielen verursachten Schaden um 16% und erhöht die kritische Trefferchance Eurer Fähigkeit 'Wilder Biss' auf blutenden Zielen um 20%."
                            },
                            {
                                "description": "Erhöht den von Euren Fähigkeiten 'Zermalmen' und 'Schreddern' an blutenden Zielen verursachten Schaden um 20% und erhöht die kritische Trefferchance Eurer Fähigkeit 'Wilder Biss' auf blutenden Zielen um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2266,
                        "name": "Urtümliches Blutvergießen",
                        "icon": "ability_druid_rake",
                        "x": 2,
                        "y": 9,
                        "req": 1918,
                        "ranks": [
                            {
                                "description": "Gewährt dem regelmäßigen Schaden Eurer Fähigkeiten 'Aufschlitzen' und 'Zerfetzen' die Fähigkeit, kritisch zu treffen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1927,
                        "name": "Berserker",
                        "icon": "ability_druid_berserk",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Wenn aktiviert, gewährt Euch diese Fähigkeit, dass 'Zerfleischen (Bär)' bis zu 3 Ziele trifft und keiner Abklingzeit unterliegt. In Katzengestalt werden die Energiekosten aller Eurer Fähigkeiten um 50% verringert. Hält 15 Sek. lang an. Während 'Berserker' aktiv ist, kann 'Tigerfuror' nicht genutzt werden.<br/><br/>'Berserker' entfernt sofort Furchteffekte und macht Euch während seiner Dauer gegen sie immun."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Wiederherstellung",
                "icon": "spell_nature_healingtouch",
                "backgroundFile": "DruidRestoration",
                "overlayColor": "#66cc33",
                "description": "Nutzt Zauber, die Heilung über Zeit hervorrufen, um seine Verbündeten am Leben zu halten und nimmt, wenn es nötig ist, die Gestalt eines Baumes an.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": true,
                    "dps": false
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 821,
                        "name": "Verbessertes Mal der Wildnis",
                        "icon": "spell_nature_regeneration",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Effekte Eurer Zauber 'Mal der Wildnis' und 'Gabe der Wildnis' um 20% und erhöht Eure gesamten Attribute um 1%."
                            },
                            {
                                "description": "Erhöht die Effekte Eurer Zauber 'Mal der Wildnis' und 'Gabe der Wildnis' um 40% und erhöht Eure gesamten Attribute um 2%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 823,
                        "name": "Naturfokus",
                        "icon": "spell_nature_healingwavegreater",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Heilende Berührung', 'Zorn', 'Wucherwurzeln', 'Wirbelsturm', 'Pflege', 'Nachwachsen' und 'Gelassenheit' um 23%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Heilende Berührung', 'Zorn', 'Wucherwurzeln', 'Wirbelsturm', 'Pflege', 'Nachwachsen' und 'Gelassenheit' um 46%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Heilende Berührung', 'Zorn', 'Wucherwurzeln', 'Wirbelsturm', 'Pflege', 'Nachwachsen' und 'Gelassenheit' um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 822,
                        "name": "Ingrimm",
                        "icon": "spell_holy_blessingofstamina",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 20%, 10 Wut zu gewinnen, wenn Ihr Bären- oder Terrorbärengestalt annehmt. Ihr behaltet bis zu 20 Eurer Energie, wenn Ihr Katzengestalt annehmt. Erhöht Eure gesamte Intelligenz in Mondkingestalt um 2%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 40%, 10 Wut zu gewinnen, wenn Ihr Bären- oder Terrorbärengestalt annehmt. Ihr behaltet bis zu 40 Eurer Energie, wenn Ihr Katzengestalt annehmt. Erhöht Eure gesamte Intelligenz in Mondkingestalt um 4%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 60%, 10 Wut zu gewinnen, wenn Ihr Bären- oder Terrorbärengestalt annehmt. Ihr behaltet bis zu 60 Eurer Energie, wenn Ihr Katzengestalt annehmt. Erhöht Eure gesamte Intelligenz in Mondkingestalt um 6%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 80%, 10 Wut zu gewinnen, wenn Ihr Bären- oder Terrorbärengestalt annehmt. Ihr behaltet bis zu 80 Eurer Energie, wenn Ihr Katzengestalt annehmt. Erhöht Eure gesamte Intelligenz in Mondkingestalt um 8%."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 100%, 10 Wut zu gewinnen, wenn Ihr Bären- oder Terrorbärengestalt annehmt. Ihr behaltet bis zu 100 Eurer Energie, wenn Ihr Katzengestalt annehmt. Erhöht Eure gesamte Intelligenz in Mondkingestalt um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 824,
                        "name": "Naturalist",
                        "icon": "spell_nature_healingtouch",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Heilende Berührung' um 0.1 Sek. und erhöht den verursachten Schaden Eurer körperlichen Angriffe in allen Gestalten um 2%."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Heilende Berührung' um 0.2 Sek. und erhöht den verursachten Schaden Eurer körperlichen Angriffe in allen Gestalten um 4%."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Heilende Berührung' um 0.3 Sek. und erhöht den verursachten Schaden Eurer körperlichen Angriffe in allen Gestalten um 6%."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Heilende Berührung' um 0.4 Sek. und erhöht den verursachten Schaden Eurer körperlichen Angriffe in allen Gestalten um 8%."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Heilende Berührung' um 0.5 Sek. und erhöht den verursachten Schaden Eurer körperlichen Angriffe in allen Gestalten um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 841,
                        "name": "Feingefühl",
                        "icon": "ability_eyeoftheowl",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die von Euren Wiederherstellungszaubern verursachte Bedrohung um 10% und verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch 'Mondfeuer' und 'Insektenschwarm' gebannt werden, um 10%."
                            },
                            {
                                "description": "Verringert die von Euren Wiederherstellungszaubern verursachte Bedrohung um 20% und verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch 'Mondfeuer' und 'Insektenschwarm' gebannt werden, um 20%."
                            },
                            {
                                "description": "Verringert die von Euren Wiederherstellungszaubern verursachte Bedrohung um 30% und verringert die Chance, dass sowohl Eure hilfreichen Zauber als auch 'Mondfeuer' und 'Insektenschwarm' gebannt werden, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 826,
                        "name": "Schnellwandlung",
                        "icon": "spell_nature_wispsplode",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten aller Gestaltwandelzauber um 10%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Gestaltwandelzauber um 20%."
                            },
                            {
                                "description": "Verringert die Manakosten aller Gestaltwandelzauber um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 829,
                        "name": "Intensität",
                        "icon": "spell_frost_windwalkon",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Gewährt Euch 17% Eurer Manaregeneration während des Zauberwirkens. Eure Fähigkeit 'Wutanfall' erzeugt sofort 4 Wut."
                            },
                            {
                                "description": "Gewährt Euch 33% Eurer Manaregeneration während des Zauberwirkens. Eure Fähigkeit 'Wutanfall' erzeugt sofort 7 Wut."
                            },
                            {
                                "description": "Gewährt Euch 50% Eurer Manaregeneration während des Zauberwirkens. Eure Fähigkeit 'Wutanfall' erzeugt sofort 10 Wut."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 827,
                        "name": "Omen der Klarsicht",
                        "icon": "spell_nature_crystalball",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Jeder automatische Angriff, jeder Schadens- und Heilzauber des Druiden hat die Chance, ihn in einen Freizauberzustand zu versetzen. Der Freizauberzustand verringert die Manakosten, Wutkosten oder Energiekosten Eures nächsten Schadens- oder Heilzaubers oder Eurer nächsten Offensivfähigkeit um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1915,
                        "name": "Meisterlicher Gestaltwandler",
                        "icon": "ability_druid_mastershapeshifter",
                        "x": 2,
                        "y": 2,
                        "req": 826,
                        "ranks": [
                            {
                                "description": "Verleiht einen Effekt, der so lang aktiv ist, wie sich der Druide in der jeweiligen gestaltveränderten Form befindet.<br/><br/>Bärengestalt - Erhöht körperlichen Schaden um 2%.<br/><br/>Katzengestalt - Erhöht kritische Trefferchance um 2%.<br/><br/>Mondkingestalt - Erhöht Zauberschaden um 2%.<br/><br/>Baumgestalt - Erhöht Heilung um 2%."
                            },
                            {
                                "description": "Verleiht einen Effekt, der so lang aktiv ist, wie sich der Druide in der jeweiligen gestaltveränderten Form befindet.<br/><br/>Bärengestalt - Erhöht körperlichen Schaden um 4%.<br/><br/>Katzengestalt - Erhöht kritische Trefferchance um 4%.<br/><br/>Mondkingestalt - Erhöht Zauberschaden um 4%.<br/><br/>Baumgestalt - Erhöht Heilung um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 843,
                        "name": "Gelassener Geist",
                        "icon": "spell_holy_elunesgrace",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Heilende Berührung', 'Pflege' und 'Gelassenheit' um 2%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Heilende Berührung', 'Pflege' und 'Gelassenheit' um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Heilende Berührung', 'Pflege' und 'Gelassenheit' um 6%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Heilende Berührung', 'Pflege' und 'Gelassenheit' um 8%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zauber 'Heilende Berührung', 'Pflege' und 'Gelassenheit' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 830,
                        "name": "Verbesserte Verjüngung",
                        "icon": "spell_nature_rejuvenation",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt Eures Zaubers 'Verjüngung' um 5%."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Zaubers 'Verjüngung' um 10%."
                            },
                            {
                                "description": "Erhöht den Effekt Eures Zaubers 'Verjüngung' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 831,
                        "name": "Schnelligkeit der Natur",
                        "icon": "spell_nature_ravenform",
                        "x": 0,
                        "y": 4,
                        "req": 829,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Bei Aktivierung wird Euer nächster Naturzauber mit einer Basiszauberzeit von weniger als 10 Sek. zu einem Spontanzauber."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 828,
                        "name": "Geschenk der Natur",
                        "icon": "spell_nature_protectionformnature",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die Wirkung aller Heilzauber um 2%."
                            },
                            {
                                "description": "Erhöht die Wirkung aller Heilzauber um 4%."
                            },
                            {
                                "description": "Erhöht die Wirkung aller Heilzauber um 6%."
                            },
                            {
                                "description": "Erhöht die Wirkung aller Heilzauber um 8%."
                            },
                            {
                                "description": "Erhöht die Wirkung aller Heilzauber um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 842,
                        "name": "Verbesserte Gelassenheit",
                        "icon": "spell_nature_tranquility",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die von Eurem Zauber 'Gelassenheit' verursachte Bedrohung um 50% und verringert die Abklingzeit um 30%."
                            },
                            {
                                "description": "Verringert die von Eurem Zauber 'Gelassenheit' verursachte Bedrohung um 100% und verringert die Abklingzeit um 60%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1788,
                        "name": "Machtvolle Berührung",
                        "icon": "ability_druid_empoweredtouch",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Heilende Berührung' erhält einen um 20% erhöhten Bonus durch Heilung beeinflussende Effekte, der Bonus von 'Pflege' wird um 10% erhöht."
                            },
                            {
                                "description": "Euer Zauber 'Heilende Berührung' erhält einen um 40% erhöhten Bonus durch Heilung beeinflussende Effekte, der Bonus von 'Pflege' wird um 20% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 825,
                        "name": "Überfluss der Natur",
                        "icon": "spell_nature_resistnature",
                        "x": 2,
                        "y": 5,
                        "req": 830,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Nachwachsen' und 'Pflege' um 5%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Nachwachsen' und 'Pflege' um 10%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Nachwachsen' und 'Pflege' um 15%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Nachwachsen' und 'Pflege' um 20%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Effekt Eurer Zauber 'Nachwachsen' und 'Pflege' um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1797,
                        "name": "Geist des Lebens",
                        "icon": "spell_nature_giftofthewaterspirit",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 5%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 10%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Willenskraft um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 844,
                        "name": "Rasche Heilung",
                        "icon": "inv_relics_idolofrejuvenation",
                        "x": 1,
                        "y": 6,
                        "req": 828,
                        "ranks": [
                            {
                                "description": "Verbraucht bei einem befreundeten Ziel einen Effekt von 'Verjüngung' oder 'Nachwachsen', und heilt dieses Ziel sofort um einen Betrag, der 12 Sek. 'Verjüngung' beziehungsweise 18 Sek. 'Nachwachsen' entspricht.",
                                "cost": "16% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "15 Sek. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1790,
                        "name": "Vollkommenheit der Natur",
                        "icon": "ability_druid_naturalperfection",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Eure kritische Zaubertrefferchance ist um 1% erhöht und kritische Treffer gegen Euch verleihen Euch den Effekt 'Vollkommenheit der Natur', wodurch jeglicher erlittene Schaden um 2% verringert wird. Bis zu 3-mal stapelbar. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Eure kritische Zaubertrefferchance ist um 2% erhöht und kritische Treffer gegen Euch verleihen Euch den Effekt 'Vollkommenheit der Natur', wodurch jeglicher erlittene Schaden um 3% verringert wird. Bis zu 3-mal stapelbar. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Eure kritische Zaubertrefferchance ist um 3% erhöht und kritische Treffer gegen Euch verleihen Euch den Effekt 'Vollkommenheit der Natur', wodurch jeglicher erlittene Schaden um 4% verringert wird. Bis zu 3-mal stapelbar. Hält 8 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1789,
                        "name": "Machtvolle Verjüngung",
                        "icon": "ability_druid_empoweredrejuvination",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Der Bonus für Heileffekte Eurer Zauber, die über einen bestimmten Zeitraum wirken, ist um 4% erhöht."
                            },
                            {
                                "description": "Der Bonus für Heileffekte Eurer Zauber, die über einen bestimmten Zeitraum wirken, ist um 8% erhöht."
                            },
                            {
                                "description": "Der Bonus für Heileffekte Eurer Zauber, die über einen bestimmten Zeitraum wirken, ist um 12% erhöht."
                            },
                            {
                                "description": "Der Bonus für Heileffekte Eurer Zauber, die über einen bestimmten Zeitraum wirken, ist um 16% erhöht."
                            },
                            {
                                "description": "Der Bonus für Heileffekte Eurer Zauber, die über einen bestimmten Zeitraum wirken, ist um 20% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1922,
                        "name": "Samenkorn des Lebens",
                        "icon": "ability_druid_giftoftheearthmother",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Wenn Ihr mit Euren Zaubern 'Rasche Heilung', 'Nachwachsen', 'Pflege' oder 'Heilende Berührung' einen kritischen Treffer erzielt, habt Ihr eine Chance von 33%, dem Ziel für 30% des geheilten Wertes ein 'Samenkorn des Lebens' einzupflanzen. Das Samenkorn wird erblühen, wenn das Ziel das nächste Mal angegriffen wird. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr mit Euren Zaubern 'Rasche Heilung', 'Nachwachsen', 'Pflege' oder 'Heilende Berührung' einen kritischen Treffer erzielt, habt Ihr eine Chance von 66%, dem Ziel für 30% des geheilten Wertes ein 'Samenkorn des Lebens' einzupflanzen. Das Samenkorn wird erblühen, wenn das Ziel das nächste Mal angegriffen wird. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Wenn Ihr mit Euren Zaubern 'Rasche Heilung', 'Nachwachsen', 'Pflege' oder 'Heilende Berührung' einen kritischen Treffer erzielt, habt Ihr eine Chance von 100%, dem Ziel für 30% des geheilten Wertes ein 'Samenkorn des Lebens' einzupflanzen. Das Samenkorn wird erblühen, wenn das Ziel das nächste Mal angegriffen wird. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1929,
                        "name": "Revitalisieren",
                        "icon": "ability_druid_replenish",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Verjüngung' und 'Wildwuchs' haben eine Chance von 5%, pro Tick 8 Energie, 4 Wut, 1% Mana oder 16 Runenmacht wiederherzustellen."
                            },
                            {
                                "description": "Eure Zauber 'Verjüngung' und 'Wildwuchs' haben eine Chance von 10%, pro Tick 8 Energie, 4 Wut, 1% Mana oder 16 Runenmacht wiederherzustellen."
                            },
                            {
                                "description": "Eure Zauber 'Verjüngung' und 'Wildwuchs' haben eine Chance von 15%, pro Tick 8 Energie, 4 Wut, 1% Mana oder 16 Runenmacht wiederherzustellen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1791,
                        "name": "Baum des Lebens",
                        "icon": "ability_druid_treeoflife",
                        "x": 1,
                        "y": 8,
                        "req": 1789,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zauber, die Heilung über Zeit hervorrufen, um 20% und verleiht Euch die Fähigkeit, Euch in den Baum des Lebens zu verwandeln. In dieser Gestalt erhöht Ihr die erhaltene Heilung aller Gruppen- und Schlachtzugsmitglieder im Umkreis von 100 Metern um 6%. Ihr könnt ausschließlich Wiederherstellungszauber, 'Anregen', 'Baumrinde', 'Griff der Natur' und 'Dornen' wirken.<br/><br/>Der Akt des Gestaltwandelns befreit den Zaubernden von Verwandlungs- und bewegungseinschränkenden Effekten."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1930,
                        "name": "Verbesserter Baum des Lebens",
                        "icon": "ability_druid_improvedtreeform",
                        "x": 2,
                        "y": 8,
                        "req": 1791,
                        "ranks": [
                            {
                                "description": "Erhöht in Baumgestalt sowohl Euren durch Gegenstände erzielten Rüstungswert um 67% als auch Eure Heilzaubermacht um einen Wert, der 5% Eurer Willenskraft entspricht."
                            },
                            {
                                "description": "Erhöht in Baumgestalt sowohl Euren durch Gegenstände erzielten Rüstungswert um 133% als auch Eure Heilzaubermacht um einen Wert, der 10% Eurer Willenskraft entspricht."
                            },
                            {
                                "description": "Erhöht in Baumgestalt sowohl Euren durch Gegenstände erzielten Rüstungswert um 200% als auch Eure Heilzaubermacht um einen Wert, der 15% Eurer Willenskraft entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2264,
                        "name": "Verbesserte Baumrinde",
                        "icon": "spell_nature_stoneclawtotem",
                        "x": 0,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Gewährt in Reisegestalt und in unverwandeltem Zustand zusätzlich 80% Rüstung von Stoff- und Lederrüstungen. Zudem wird die von Eurem Zauber 'Baumrinde' gewährte Schadensverringerung um 5% erhöht und die Chance, dass 'Baumrinde' gebannt wird, um 35% verringert."
                            },
                            {
                                "description": "Gewährt in Reisegestalt und in unverwandeltem Zustand zusätzlich 160% Rüstung von Stoff- und Lederrüstungen. Zudem wird die von Eurem Zauber 'Baumrinde' gewährte Schadensverringerung um 10% erhöht und die Chance, dass 'Baumrinde' gebannt wird, um 70% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1916,
                        "name": "Gabe der Erdmutter",
                        "icon": "ability_druid_manatree",
                        "x": 2,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht Euer gesamtes Zaubertempo um 2% und verringert die Basisabklingzeit Eures Zaubers 'Blühendes Leben' um 2%."
                            },
                            {
                                "description": "Erhöht Euer gesamtes Zaubertempo um 4% und verringert die Basisabklingzeit Eures Zaubers 'Blühendes Leben' um 4%."
                            },
                            {
                                "description": "Erhöht Euer gesamtes Zaubertempo um 6% und verringert die Basisabklingzeit Eures Zaubers 'Blühendes Leben' um 6%."
                            },
                            {
                                "description": "Erhöht Euer gesamtes Zaubertempo um 8% und verringert die Basisabklingzeit Eures Zaubers 'Blühendes Leben' um 8%."
                            },
                            {
                                "description": "Erhöht Euer gesamtes Zaubertempo um 10% und verringert die Basisabklingzeit Eures Zaubers 'Blühendes Leben' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1917,
                        "name": "Wildwuchs",
                        "icon": "ability_druid_flourish",
                        "x": 1,
                        "y": 10,
                        "req": 1791,
                        "ranks": [
                            {
                                "description": "Heilt bis zu 5 freundliche Gruppen- oder Schlachtzugsmitglieder in einem Umkreis von 15 Metern um das Ziel über 7 Sek. für insgesamt 686. Die Heilung erfolgt anfangs schnell und wird mit zunehmender Dauer von 'Wildwuchs' langsamer.",
                                "cost": "23% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "40 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "161": {
        "name": "Glyphe 'Rasende Regeneration'",
        "id": "161",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40896",
        "spellKey": "54810",
        "spellId": "54810",
        "prettyName": "",
        "typeOrder": 2
    },
    "162": {
        "name": "Glyphe 'Zermalmen'",
        "id": "162",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40897",
        "spellKey": "54811",
        "spellId": "54811",
        "prettyName": "",
        "typeOrder": 2
    },
    "163": {
        "name": "Glyphe 'Knurren'",
        "id": "163",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40899",
        "spellKey": "54812",
        "spellId": "54812",
        "prettyName": "",
        "typeOrder": 2
    },
    "164": {
        "name": "Glyphe 'Zerfleischen'",
        "id": "164",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40900",
        "spellKey": "54813",
        "spellId": "54813",
        "prettyName": "",
        "typeOrder": 2
    },
    "165": {
        "name": "Glyphe 'Schreddern'",
        "id": "165",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40901",
        "spellKey": "54815",
        "spellId": "54815",
        "prettyName": "",
        "typeOrder": 2
    },
    "166": {
        "name": "Glyphe 'Zerfetzen'",
        "id": "166",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40902",
        "spellKey": "54818",
        "spellId": "54818",
        "prettyName": "",
        "typeOrder": 2
    },
    "167": {
        "name": "Glyphe 'Krallenhieb'",
        "id": "167",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40903",
        "spellKey": "54821",
        "spellId": "54821",
        "prettyName": "",
        "typeOrder": 2
    },
    "168": {
        "name": "Glyphe 'Rasche Heilung'",
        "id": "168",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40906",
        "spellKey": "54824",
        "spellId": "54824",
        "prettyName": "",
        "typeOrder": 2
    },
    "169": {
        "name": "Glyphe 'Anregen'",
        "id": "169",
        "type": 0,
        "description": "Wird Euer Zauber \\'Anregen\\' auf ein anderes befreundetes Ziel gewirkt, als auf Euch selbst, regeneriert Ihr im Verlauf von 10 Sek. 10% Eures maximalen Manas.",
        "icon": "inv_glyph_majordruid",
        "itemId": "40908",
        "spellKey": "54832",
        "spellId": "54832",
        "prettyName": "",
        "typeOrder": 2
    },
    "170": {
        "name": "Glyphe 'Wiedergeburt'",
        "id": "170",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40909",
        "spellKey": "54733",
        "spellId": "54733",
        "prettyName": "",
        "typeOrder": 2
    },
    "171": {
        "name": "Glyphe 'Nachwachsen'",
        "id": "171",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40912",
        "spellKey": "54743",
        "spellId": "54743",
        "prettyName": "",
        "typeOrder": 2
    },
    "172": {
        "name": "Glyphe 'Verjüngung'",
        "id": "172",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40913",
        "spellKey": "54754",
        "spellId": "54754",
        "prettyName": "",
        "typeOrder": 2
    },
    "173": {
        "name": "Glyphe 'Heilende Berührung'",
        "id": "173",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40914",
        "spellKey": "54825",
        "spellId": "54825",
        "prettyName": "",
        "typeOrder": 2
    },
    "174": {
        "name": "Glyphe 'Blühendes Leben'",
        "id": "174",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40915",
        "spellKey": "54826",
        "spellId": "54826",
        "prettyName": "",
        "typeOrder": 2
    },
    "175": {
        "name": "Glyphe 'Sternenfeuer'",
        "id": "175",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40916",
        "spellKey": "54845",
        "spellId": "54845",
        "prettyName": "",
        "typeOrder": 2
    },
    "176": {
        "name": "Glyphe 'Insektenschwarm'",
        "id": "176",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40919",
        "spellKey": "54830",
        "spellId": "54830",
        "prettyName": "",
        "typeOrder": 2
    },
    "177": {
        "name": "Glyphe 'Hurrikan'",
        "id": "177",
        "type": 0,
        "description": "Euer Zauber \\'Hurrikan\\' verringert nun auch das Bewegungstempo seiner Ziele um 50%.",
        "icon": "inv_glyph_majordruid",
        "itemId": "40920",
        "spellKey": "54831",
        "spellId": "54831",
        "prettyName": "",
        "typeOrder": 2
    },
    "178": {
        "name": "Glyphe 'Sternenregen'",
        "id": "178",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Sternenregen\\' um 30 Sek.",
        "icon": "inv_glyph_majordruid",
        "itemId": "40921",
        "spellKey": "54828",
        "spellId": "54828",
        "prettyName": "",
        "typeOrder": 2
    },
    "179": {
        "name": "Glyphe 'Zorn'",
        "id": "179",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40922",
        "spellKey": "54756",
        "spellId": "54756",
        "prettyName": "",
        "typeOrder": 2
    },
    "180": {
        "name": "Glyphe 'Mondfeuer'",
        "id": "180",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "40923",
        "spellKey": "54829",
        "spellId": "54829",
        "prettyName": "",
        "typeOrder": 2
    },
    "181": {
        "name": "Glyphe 'Wucherwurzeln'",
        "id": "181",
        "type": 0,
        "description": "Verringert die Zauberzeit Eures Zaubers \\'Wucherwurzeln\\' um 0,2 Sek.",
        "icon": "inv_glyph_majordruid",
        "itemId": "40924",
        "spellKey": "54760",
        "spellId": "54760",
        "prettyName": "",
        "typeOrder": 2
    },
    "431": {
        "name": "Glyphe 'Wassergestalt'",
        "id": "431",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minordruid",
        "itemId": "43316",
        "spellKey": "57856",
        "spellId": "57856",
        "prettyName": "",
        "typeOrder": 2
    },
    "432": {
        "name": "Glyphe 'Herausforderndes Gebrüll'",
        "id": "432",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minordruid",
        "itemId": "43334",
        "spellKey": "57858",
        "spellId": "57858",
        "prettyName": "",
        "typeOrder": 2
    },
    "433": {
        "name": "Glyphe 'Gabe der Wildnis'",
        "id": "433",
        "type": 1,
        "description": "Die Manakosten Eures Zaubers \\'Mal der Wildnis\\' sind um 50% verringert.",
        "icon": "inv_glyph_minordruid",
        "itemId": "43335",
        "spellKey": "57855",
        "spellId": "57855",
        "prettyName": "",
        "typeOrder": 2
    },
    "434": {
        "name": "Glyphe 'Sorglose Wiedergeburt'",
        "id": "434",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minordruid",
        "itemId": "43331",
        "spellKey": "57857",
        "spellId": "57857",
        "prettyName": "",
        "typeOrder": 2
    },
    "435": {
        "name": "Glyphe 'Dornen'",
        "id": "435",
        "type": 1,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Dornen\\' um 20 Sek.",
        "icon": "inv_glyph_minordruid",
        "itemId": "43332",
        "spellKey": "57862",
        "spellId": "57862",
        "prettyName": "",
        "typeOrder": 2
    },
    "551": {
        "name": "Glyphe 'Spurt'",
        "id": "551",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minordruid",
        "itemId": "43674",
        "spellKey": "59219",
        "spellId": "59219",
        "prettyName": "",
        "typeOrder": 2
    },
    "613": {
        "name": "Glyphe 'Taifun'",
        "id": "613",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minordruid",
        "itemId": "44922",
        "spellKey": "62135",
        "spellId": "62135",
        "prettyName": "",
        "typeOrder": 2
    },
    "631": {
        "name": "Glyphe 'Fokus'",
        "id": "631",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "44928",
        "spellKey": "62080",
        "spellId": "62080",
        "prettyName": "",
        "typeOrder": 2
    },
    "671": {
        "name": "Glyphe 'Berserker'",
        "id": "671",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "45601",
        "spellKey": "62969",
        "spellId": "62969",
        "prettyName": "",
        "typeOrder": 2
    },
    "672": {
        "name": "Glyphe 'Wildwuchs'",
        "id": "672",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "45602",
        "spellKey": "62970",
        "spellId": "62970",
        "prettyName": "",
        "typeOrder": 2
    },
    "673": {
        "name": "Glyphe 'Pflege'",
        "id": "673",
        "type": 0,
        "description": "Wenn Euer Zauber \\'Sternensog\\' Schaden verursacht, wird die verbleibende Abklingzeit Eures Zaubers \\'Sternenregen\\' um 5 Sek. verringert.",
        "icon": "inv_glyph_majordruid",
        "itemId": "45603",
        "spellKey": "62971",
        "spellId": "62971",
        "prettyName": "",
        "typeOrder": 2
    },
    "674": {
        "name": "Glyphe 'Wildes Brüllen'",
        "id": "674",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "45604",
        "spellKey": "63055",
        "spellId": "63055",
        "prettyName": "",
        "typeOrder": 2
    },
    "675": {
        "name": "Glyphe 'Monsun'",
        "id": "675",
        "type": 0,
        "description": "Verringert die Abklingzeit des Zaubers \\'Taifun\\' um 3 Sek.",
        "icon": "inv_glyph_majordruid",
        "itemId": "45622",
        "spellKey": "63056",
        "spellId": "63056",
        "prettyName": "",
        "typeOrder": 2
    },
    "676": {
        "name": "Glyphe 'Baumrinde'",
        "id": "676",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "45623",
        "spellKey": "63057",
        "spellId": "63057",
        "prettyName": "",
        "typeOrder": 2
    },
    "811": {
        "name": "Glyphe der Überlebensinstinkte",
        "id": "811",
        "type": 0,
        "description": "You Survival Instincts ability grants an additional 15% of your maximum health.",
        "icon": "inv_glyph_majordruid",
        "itemId": "46372",
        "spellKey": "65243",
        "spellId": "65243",
        "prettyName": "",
        "typeOrder": 2
    },
    "831": {
        "name": "Glyph of Claw",
        "id": "831",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majordruid",
        "itemId": "48720",
        "spellKey": "67598",
        "spellId": "67598",
        "prettyName": "",
        "typeOrder": 2
    },
    "891": {
        "name": "Glyph of Rapid Rejuvenation",
        "id": "891",
        "type": 0,
        "description": "",
        "icon": "inv_glyph_majordruid",
        "itemId": "50125",
        "spellKey": "71013",
        "spellId": "71013",
        "prettyName": "",
        "typeOrder": 2
    },
    "": {
        "name": "Deprecated Glyph of the Black Bear",
        "id": null,
        "type": 1,
        "description": "",
        "icon": "inv_misc_rune_14",
        "itemId": "43336",
        "spellKey": null,
        "spellId": null,
        "prettyName": "",
        "typeOrder": 2
    }
}]
}