{
    "talentData": {
        "characterClass": {
            "classId": 4,
            "name": "Schurke",
            "powerType": "ENERGY",
            "powerTypeId": 3,
            "powerTypeSlug": "energy"
        },
        "talentTrees": [
            {
                "name": "Meucheln",
                "icon": "ability_rogue_eviscerate",
                "backgroundFile": "RogueAssassination",
                "overlayColor": "#7fcc7f",
                "description": "Ein Meister der tödlichen Gifte, der seine Feinde mit hinterhältigen Dolchstößen beseitigt.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 276,
                        "name": "Verbessertes Ausweiden",
                        "icon": "ability_rogue_eviscerate",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Ausweiden' verursachten Schaden um 7%."
                            },
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Ausweiden' verursachten Schaden um 14%."
                            },
                            {
                                "description": "Erhöht den von Eurer Fähigkeit 'Ausweiden' verursachten Schaden um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 272,
                        "name": "Gnadenlose Angriffe",
                        "icon": "ability_fiegndead",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Nachdem ein Gegner getötet wurde, der Erfahrung oder Ehre gewährt, erhaltet Ihr eine 20% höhere Chance auf einen kritischen Treffer bei Eurem nächsten 'Finsteren Stoß', 'Blutsturz', 'Meucheln', 'Verstümmeln', 'Hinterhalt' oder 'Geisterhaften Stoß'. Hält 20 Sek. lang an."
                            },
                            {
                                "description": "Nachdem ein Gegner getötet wurde, der Erfahrung oder Ehre gewährt, erhaltet Ihr eine 40% höhere Chance auf einen kritischen Treffer bei Eurem nächsten 'Finsteren Stoß', 'Blutsturz', 'Meucheln', 'Verstümmeln', 'Hinterhalt' oder 'Geisterhaften Stoß'. Hält 20 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 270,
                        "name": "Tücke",
                        "icon": "ability_racial_bloodrage",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance auf einen kritischen Treffer um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 273,
                        "name": "Skrupellosigkeit",
                        "icon": "ability_druid_disembowel",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Gewährt Euren Nahkampf-Finishing-Moves eine Chance von 20%, Eurem Ziel einen Combopunkt hinzuzufügen."
                            },
                            {
                                "description": "Gewährt Euren Nahkampf-Finishing-Moves eine Chance von 40%, Eurem Ziel einen Combopunkt hinzuzufügen."
                            },
                            {
                                "description": "Gewährt Euren Nahkampf-Finishing-Moves eine Chance von 60%, Eurem Ziel einen Combopunkt hinzuzufügen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2068,
                        "name": "Blut vergießen",
                        "icon": "ability_rogue_bloodsplatter",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Erdrosseln' und 'Blutung' um 15%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Erdrosseln' und 'Blutung' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 277,
                        "name": "Stichwunden",
                        "icon": "ability_backstab",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Meucheln' um 10% und die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Verstümmeln' um 5%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Meucheln' um 20% und die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Verstümmeln' um 10%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Meucheln' um 30% und die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Verstümmeln' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 382,
                        "name": "Lebenskraft",
                        "icon": "spell_nature_earthbindtotem",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht Eure maximale Energie um 10."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 278,
                        "name": "Verbessertes Rüstung schwächen",
                        "icon": "ability_warrior_riposte",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeit 'Rüstung schwächen' um 5."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeit 'Rüstung schwächen' um 10."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 269,
                        "name": "Tödlichkeit",
                        "icon": "ability_criticalstrike",
                        "x": 2,
                        "y": 2,
                        "req": 270,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Fähigkeiten, die Combopunkte erzeugen, jedoch keine Verstohlenheit benötigen, um 6%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Fähigkeiten, die Combopunkte erzeugen und keine Verstohlenheit benötigen, um 12%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Angriffe, die Combopunkte erzeugen und keine Verstohlenheit benötigen, um 18%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Angriffe, die Combopunkte erzeugen und keine Verstohlenheit benötigen, um 24%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus aller Eurer Angriffe, die Combopunkte erzeugen und keine Verstohlenheit benötigen, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 682,
                        "name": "Üble Gifte",
                        "icon": "ability_rogue_feigndeath",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den von Euren Giften und der Fähigkeit 'Vergiften' angerichteten Schaden um 7% und verleiht Euren Giften, die Schaden im Laufe der Zeit verursachen, eine Chance von zusätzlichen 10%, Banneffekten zu widerstehen."
                            },
                            {
                                "description": "Erhöht den von Euren Giften und der Fähigkeit 'Vergiften' angerichteten Schaden um 14% und verleiht Euren Giften, die Schaden im Laufe der Zeit verursachen, eine Chance von zusätzlichen 20%, Banneffekten zu widerstehen."
                            },
                            {
                                "description": "Erhöht den von Euren Giften und der Fähigkeit 'Vergiften' angerichteten Schaden um 20% und verleiht Euren Giften, die Schaden im Laufe der Zeit verursachen, eine Chance von zusätzlichen 30%, Banneffekten zu widerstehen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 268,

                        "name": "Verbesserte Gifte",
                        "icon": "ability_poisons",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance, Euer Ziel mit tödlichem Gift zu erreichen, um 4% und die Frequenz, mit der das Ziel mit sofort wirkendem Gift erreicht werden kann, um 10%."
                            },
                            {
                                "description": "Erhöht die Chance, Euer Ziel mit tödlichem Gift zu erreichen, um 8% und die Frequenz, mit der das Ziel mit sofort wirkendem Gift erreicht werden kann, um 20%."
                            },
                            {
                                "description": "Erhöht die Chance, Euer Ziel mit tödlichem Gift zu erreichen, um 12% und die Frequenz, mit der das Ziel mit sofort wirkendem Gift erreicht werden kann, um 30%."
                            },
                            {
                                "description": "Erhöht die Chance, Euer Ziel mit tödlichem Gift zu erreichen, um 16% und die Frequenz, mit der das Ziel mit sofort wirkendem Gift erreicht werden kann, um 40%."
                            },
                            {
                                "description": "Erhöht die Chance, Euer Ziel mit tödlichem Gift zu erreichen, um 20% und die Frequenz, mit der das Ziel mit sofort wirkendem Gift erreicht werden kann, um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1721,
                        "name": "Leichtfüßig",
                        "icon": "ability_rogue_fleetfooted",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer aller auf Euch wirkenden bewegungseinschränkenden Effekte um 15% und erhöht Euer Bewegungstempo um 8%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            },
                            {
                                "description": "Verringert die Dauer aller auf Euch wirkenden bewegungseinschränkenden Effekte um 30% und erhöht Euer Bewegungstempo um 15%. Nicht stapelbar mit anderen Effekten, die das Bewegungstempo erhöhen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 280,
                        "name": "Kaltblütigkeit",
                        "icon": "spell_ice_lament",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Wenn aktiv, wird die kritische Trefferchance Eurer nächsten Offensivfähigkeit um 100% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 279,
                        "name": "Verbesserter Nierenhieb",
                        "icon": "ability_rogue_kidneyshot",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Während Euer Ziel von 'Nierenhieb' betroffen ist, erleidet es zusätzliche 3% Schaden von allen Schadensquellen."
                            },
                            {
                                "description": "Während Euer Ziel von 'Nierenhieb' betroffen ist, erleidet es zusätzliche 6% Schaden von allen Schadensquellen."
                            },
                            {
                                "description": "Während Euer Ziel von 'Nierenhieb' betroffen ist, erleidet es zusätzliche 9% Schaden von allen Schadensquellen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1762,
                        "name": "Schnelle Erholung",
                        "icon": "ability_rogue_quickrecovery",
                        "x": 3,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Alle auf Euch gewirkten Heileffekte werden um 10% erhöht. Außerdem stellen Eure Finishing-Moves 40% ihrer Energiekosten wieder her, wenn sie das Ziel verfehlen."
                            },
                            {
                                "description": "Alle auf Euch gewirkten Heileffekte werden um 20% erhöht. Außerdem stellen Eure Finishing-Moves 80% ihrer Energiekosten wieder her, wenn sie das Ziel verfehlen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 283,
                        "name": "Schicksal besiegeln",
                        "icon": "spell_shadow_chilltouch",
                        "x": 1,
                        "y": 5,
                        "req": 280,
                        "ranks": [
                            {
                                "description": "Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte hinzufügen, eine Chance von 20%, einen zusätzlichen Combopunkt hinzuzufügen."
                            },
                            {
                                "description": "Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte hinzufügen, eine Chance von 40%, einen zusätzlichen Combopunkt hinzuzufügen."
                            },
                            {
                                "description": "Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte hinzufügen, eine Chance von 60%, einen zusätzlichen Combopunkt hinzuzufügen."
                            },
                            {
                                "description": "Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte hinzufügen, eine Chance von 80%, einen zusätzlichen Combopunkt hinzuzufügen."
                            },
                            {
                                "description": "Gewährt Euren kritischen Treffern aus Fähigkeiten, die Combopunkte hinzufügen, eine Chance von 100%, einen zusätzlichen Combopunkt hinzuzufügen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 274,
                        "name": "Mord",
                        "icon": "spell_shadow_deathscream",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht jeglichen Schaden um 2%."
                            },
                            {
                                "description": "Erhöht jeglichen Schaden um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2065,
                        "name": "Tödliche Mischung",
                        "icon": "ability_rogue_deadlybrew",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Wenn Ihr ein Ziel mit sofort wirkendem Gift, Wundgift oder gedankenbenebelndem Gift vergiftet, besteht eine Chance von 50%, das Ziel auch mit verkrüppelndem Gift zu vergiften."
                            },
                            {
                                "description": "Wenn Ihr ein Ziel mit sofort wirkendem Gift, Wundgift oder gedankenbenebelndem Gift vergiftet, besteht eine Chance von 100%, das Ziel auch mit verkrüppelndem Gift zu vergiften."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 281,
                        "name": "Amok",
                        "icon": "ability_hunter_rapidkilling",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "In der Verstohlenheit und bis zu 20 Sekunden nach Verlassen der Verstohlenheit regeneriert Ihr zusätzlich 30% Energie."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1723,
                        "name": "Abgestumpfte Nerven",
                        "icon": "ability_rogue_deadenednerves",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 2%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 4%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2069,
                        "name": "Fokussierte Angriffe",
                        "icon": "ability_rogue_focusedattacks",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure kritischen Nahkampftreffer haben eine Chance von 33%, Euch 2 Energie zu gewähren."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer haben eine Chance von 66%, Euch 2 Energie zu gewähren."
                            },
                            {
                                "description": "Eure kritischen Nahkampftreffer haben eine Chance von 100%, Euch 2 Energie zu gewähren."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1718,
                        "name": "Schwächen aufspüren",
                        "icon": "ability_rogue_findweakness",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden von Offensivfähigkeiten um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden von Offensivfähigkeiten um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden von Offensivfähigkeiten um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1715,
                        "name": "Meister der Gifte",
                        "icon": "ability_creature_poison_06",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance aller Angriffe gegen jegliche Ziele, die von Euch vergiftet wurden, um 1%, verringert die Dauer aller auf Euch wirkenden Gifteffekte um 17% und gewährt 'Vergiften' eine Chance von 33%, tödliches Gift nicht zu verbrauchen."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Angriffe gegen jegliche Ziele, die von Euch vergiftet wurden, um 2%, verringert die Dauer aller auf Euch wirkenden Gifteffekte um 34% und gewährt 'Vergiften' eine Chance von 66%, tödliches Gift nicht zu verbrauchen."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance aller Angriffe gegen jegliche Ziele, die von Euch vergiftet wurden, um 3%, verringert die Dauer aller auf Euch wirkenden Gifteffekte um 50% und gewährt 'Vergiften' eine Chance von 100%, tödliches Gift nicht zu verbrauchen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1719,
                        "name": "Verstümmeln",
                        "icon": "ability_rogue_shadowstrikes",
                        "x": 1,
                        "y": 8,
                        "req": 281,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "5 Meter Reichweite",
                                "cost": "60 Energie",
								"requires": "Benötigt Dolche",
                                "description": "Greift sofort mit beiden Waffen an, verursacht 100% Waffenschaden sowie mit jeder Waffe zusätzlich 44 Schaden. Der Schaden gegen vergiftete Ziele ist um 20% erhöht. Gewährt 2 Combopunkte."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2066,
                        "name": "Den Spieß umdrehen",
                        "icon": "ability_rogue_turnthetables",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Wenn ein Gruppen- oder Schlachtzugsmitglied blockt, pariert oder einem Angriff ausweicht, wird Eure Chance, mit allen Comboangriffen kritisch zu treffen 8 Sek. lang um 2% erhöht."
                            },
                            {
                                "description": "Wenn ein Gruppen- oder Schlachtzugsmitglied blockt, pariert oder einem Angriff ausweicht, wird Eure Chance, mit allen Comboangriffen kritisch zu treffen 8 Sek. lang um 4% erhöht."
                            },
                            {
                                "description": "Wenn ein Gruppen- oder Schlachtzugsmitglied blockt, pariert oder einem Angriff ausweicht, wird Eure Chance, mit allen Comboangriffen kritisch zu treffen 8 Sek. lang um 6% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2070,
                        "name": "In Stücke schneiden",
                        "icon": "ability_rogue_cuttothechase",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Eure Fähigkeiten 'Ausweiden' und 'Vergiften' haben eine Chance von 20%, 'Zerhäckseln' zu seiner maximalen Dauer von 5 Combopunkten wieder aufzufrischen."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Ausweiden' und 'Vergiften' haben eine Chance von 40%, 'Zerhäckseln' zu seiner maximalen Dauer von 5 Combopunkten wieder aufzufrischen."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Ausweiden' und 'Vergiften' haben eine Chance von 60%, 'Zerhäckseln' zu seiner maximalen Dauer von 5 Combopunkten wieder aufzufrischen."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Ausweiden' und 'Vergiften' haben eine Chance von 80%, 'Zerhäckseln' zu seiner maximalen Dauer von 5 Combopunkten wieder aufzufrischen."
                            },
                            {
                                "description": "Eure Fähigkeiten 'Ausweiden' und 'Vergiften' haben eine Chance von 100%, 'Zerhäckseln' zu seiner maximalen Dauer von 5 Combopunkten wieder aufzufrischen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2071,
                        "name": "Blutgier",
                        "icon": "ability_rogue_hungerforblood",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "cost": "15 Energie",
                                "description": "Löst einen Wutanfall aus, der jeglichen verursachten Schaden um 5% erhöht. Es muss ein Blutungseffekt auf das Ziel wirken. Hält 1 Min. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Kampf",
                "icon": "ability_backstab",
                "backgroundFile": "RogueCombat",
                "overlayColor": "#ff7f00",
                "description": "Ein geschickter Fechter, der seine Beweglichkeit und Verschlagenheit nutzt, um seinen Feinden gegenüberzutreten.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 203,
                        "name": "Verbesserter Solarplexus",
                        "icon": "ability_gouge",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Effektdauer Eurer Fähigkeit 'Solarplexus' um 0.5 Sek."
                            },
                            {
                                "description": "Erhöht die Effektdauer Eurer Fähigkeit 'Solarplexus' um 1 Sek."
                            },
                            {
                                "description": "Erhöht die Effektdauer Eurer Fähigkeit 'Solarplexus' um 1.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 201,
                        "name": "Verbesserter finsterer Stoß",
                        "icon": "spell_shadow_ritualofsacrifice",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeit 'Finsterer Stoß' um 3."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeit 'Finsterer Stoß' um 5."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 221,
                        "name": "Beidhändigkeits-Spezialisierung",
                        "icon": "ability_dualwield",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 10%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 20%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 30%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 40%."
                            },
                            {
                                "description": "Erhöht den von Eurer Schildhandwaffe verursachten Schaden um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1827,
                        "name": "Verbessertes Zerhäckseln",
                        "icon": "ability_rogue_slicedice",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Dauer Eurer Fähigkeit 'Zerhäckseln' um 25%."
                            },
                            {
                                "description": "Erhöht die Dauer Eurer Fähigkeit 'Zerhäckseln' um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 187,
                        "name": "Abwehr",
                        "icon": "ability_parry",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Parierchance um 2%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 4%."
                            },
                            {
                                "description": "Erhöht Eure Parierchance um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 181,
                        "name": "Präzision",
                        "icon": "ability_marksmanship",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Waffen und Giftangriffen zu treffen, um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen und Giftangriffen zu treffen, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen und Giftangriffen zu treffen, um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen und Giftangriffen zu treffen, um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Waffen und Giftangriffen zu treffen, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 204,
                        "name": "Durchhaltevermögen",
                        "icon": "spell_shadow_shadowward",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Sprinten' und 'Entrinnen' um 30 Sek., und erhöht Eure gesamte Ausdauer um 2%."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Sprinten' und 'Entrinnen' um 60 Sek., und erhöht Eure gesamte Ausdauer um 4%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 301,
                        "name": "Riposte",
                        "icon": "ability_warrior_challange",
                        "x": 1,
                        "y": 2,
                        "req": 187,
                        "ranks": [
                            {
                                "description": "Ein Schlag, der aktiv wird, nachdem ein gegnerischer Angriff pariert wurde. Dieser Angriff verursacht 150% Waffenschaden und verlangsamt das Nahkampfangriffstempo des Gegners 30 Sek. lang um 20%. Gewährt 1 Combopunkt.",
                                "cost": "10 Energie",
                                "castTime": "Spontanzauber",
                                "cooldown": "6 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 182,
                        "name": "Nahbereichsgefecht",
                        "icon": "inv_weapon_shortblade_05",
                        "x": 2,
                        "y": 2,
                        "req": 221,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Chance, mit Dolchen und Faustwaffen einen kritischen Treffer zu erzielen, um 1%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Dolchen und Faustwaffen einen kritischen Treffer zu erzielen, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Dolchen und Faustwaffen einen kritischen Treffer zu erzielen, um 3%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Dolchen und Faustwaffen einen kritischen Treffer zu erzielen, um 4%."
                            },
                            {
                                "description": "Erhöht Eure Chance, mit Dolchen und Faustwaffen einen kritischen Treffer zu erzielen, um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 206,
                        "name": "Verbesserter Tritt",
                        "icon": "ability_kick",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Gewährt Eurer Fähigkeit 'Tritt' eine Chance von 50%, das Ziel 2 Sek. lang zum Schweigen zu bringen."
                            },
                            {
                                "description": "Gewährt Eurer Fähigkeit 'Tritt' eine Chance von 100%, das Ziel 2 Sek. lang zum Schweigen zu bringen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 222,
                        "name": "Verbessertes Sprinten",
                        "icon": "ability_rogue_sprint",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verleiht eine Chance von 50%, alle bewegungseinschränkenden Effekte zu entfernen, wenn Ihr die Fähigkeit 'Sprinten' aktiviert."
                            },
                            {
                                "description": "Verleiht eine Chance von 100%, alle bewegungseinschränkenden Effekte zu entfernen, wenn Ihr die Fähigkeit 'Sprinten' aktiviert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 186,
                        "name": "Blitzartige Reflexe",
                        "icon": "spell_nature_invisibilty",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Ausweichchance um 2% und gewährt Euch 4% Nahkampfangriffstempo."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 4% und gewährt Euch 7% Nahkampfangriffstempo."
                            },
                            {
                                "description": "Erhöht Eure Ausweichchance um 6% und gewährt Euch 10% Nahkampfangriffstempo."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1122,
                        "name": "Aggression",
                        "icon": "ability_racial_avatar",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Finsterer Stoß', 'Meucheln' und 'Ausweiden' um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Finsterer Stoß', 'Meucheln' und 'Ausweiden' um 6%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Finsterer Stoß', 'Meucheln' und 'Ausweiden' um 9%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Finsterer Stoß', 'Meucheln' und 'Ausweiden' um 12%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Finsterer Stoß', 'Meucheln' und 'Ausweiden' um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 184,
                        "name": "Streitkolben-Spezialisierung",
                        "icon": "inv_mace_01",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 3% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 6% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 9% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 12% der Rüstung Eures Feindes."
                            },
                            {
                                "description": "Eure Angriffe mit Streitkolben ignorieren bis zu 15% der Rüstung Eures Feindes."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 223,
                        "name": "Klingenwirbel",
                        "icon": "ability_warrior_punishingblow",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "cost": "25 Energie",
                                "description": "Erhöht Euer Angriffstempo um 20%. Zusätzlich treffen Angriffe einen weiteren in der Nähe befindlichen Gegner. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 242,
                        "name": "Niedermetzeln",
                        "icon": "inv_sword_27",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 1%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert oder Eurer Axt einen Treffer erzielt habt."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 2%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert oder Eurer Axt einen Treffer erzielt habt."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 3%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert oder Eurer Axt einen Treffer erzielt habt."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 4%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert oder Eurer Axt einen Treffer erzielt habt."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 5%, einen zusätzlichen Angriff auf das gleiche Ziel zu erhalten, nachdem Ihr mit Eurem Schwert oder Eurer Axt einen Treffer erzielt habt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1703,
                        "name": "Waffenexperte",
                        "icon": "spell_holy_blessingofstrength",
                        "x": 1,
                        "y": 5,
                        "req": 223,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Waffenkunde um 5."
                            },
                            {
                                "description": "Erhöht Eure Waffenkunde um 10."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1706,
                        "name": "Klingenwendung",
                        "icon": "ability_rogue_bladetwisting",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden von 'Finsterer Stoß' und 'Meucheln' um 5% und Eure Schaden verursachenden Nahkampfangriffe haben eine Chance von 10%, das Ziel 4 Sek. lang benommen zu machen."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden von 'Finsterer Stoß' und 'Meucheln' um 10% und Eure Schaden verursachenden Nahkampfangriffe haben eine Chance von 10%, das Ziel 8 Sek. lang benommen zu machen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1705,
                        "name": "Vitalität",
                        "icon": "ability_warrior_revenge",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Energieregenerationsrate um 8%."
                            },
                            {
                                "description": "Erhöht Eure Energieregenerationsrate um 16%."
                            },
                            {
                                "description": "Erhöht Eure Energieregenerationsrate um 25%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 205,
                        "name": "Adrenalinrausch",
                        "icon": "spell_shadow_shadowworddominate",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Erhöht Eure Energieregenerationsrate 15 Sek. lang um 100%."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1707,
                        "name": "Nerven aus Stahl",
                        "icon": "ability_rogue_nervesofsteel",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert Euren erlittenen Schaden, während Ihr unter dem Effekt von Betäubungs- und Furchteffekten steht um 15%."
                            },
                            {
                                "description": "Verringert Euren erlittenen Schaden, während Ihr unter dem Effekt von Betäubungs- und Furchteffekten steht um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2072,
                        "name": "Wurfspezialisierung",
                        "icon": "ability_rogue_throwingspecialization",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite von 'Werfen' und 'Tödlicher Wurf' um 2 Meter und verleiht 'Tödlicher Wurf' eine Chance von 50%, das Ziel 3 Sek. lang zu unterbrechen."
                            },
                            {
                                "description": "Erhöht die Reichweite von 'Werfen' und 'Tödlicher Wurf' um 4 Meter und verleiht 'Tödlicher Wurf' eine Chance von 100%, das Ziel 3 Sek. lang zu unterbrechen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1825,
                        "name": "Kampfkraft",
                        "icon": "inv_weapon_shortblade_38",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Gewährt Euren erfolgreichen Nahkampfangriffen mit der Schildhand eine Chance von 20%, 3 Energie zu erzeugen."
                            },
                            {
                                "description": "Gewährt Euren erfolgreichen Nahkampfangriffen mit der Schildhand eine Chance von 20%, 6 Energie zu erzeugen."
                            },
                            {
                                "description": "Gewährt Euren erfolgreichen Nahkampfangriffen mit der Schildhand eine Chance von 20%, 9 Energie zu erzeugen."
                            },
                            {
                                "description": "Gewährt Euren erfolgreichen Nahkampfangriffen mit der Schildhand eine Chance von 20%, 12 Energie zu erzeugen."
                            },
                            {
                                "description": "Gewährt Euren erfolgreichen Nahkampfangriffen mit der Schildhand eine Chance von 20%, 15 Energie zu erzeugen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2073,
                        "name": "Unfairer Vorteil",
                        "icon": "ability_rogue_unfairadvantage",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Weicht Ihr einem Angriff aus, gewinnt Ihr einen unfairen Vorteil und schlagt umgehend zurück. Verursacht 50% des Schadens Eurer Waffenhandwaffe. Dieser Effekt kann nur einmal pro Sekunde auftreten."
                            },
                            {
                                "description": "Weicht Ihr einem Angriff aus, gewinnt Ihr einen unfairen Vorteil und schlagt umgehend zurück. Verursacht 100% des Schadens Eurer Waffenhandwaffe. Dieser Effekt kann nur einmal pro Sekunde auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1709,
                        "name": "Überraschungsangriffe",
                        "icon": "ability_rogue_surpriseattack",
                        "x": 1,
                        "y": 8,
                        "req": 205,
                        "ranks": [
                            {
                                "description": "Euren Finishing-Moves kann nicht mehr ausgewichen werden. Der durch Eure Fähigkeiten 'Finsterer Stoß', 'Meucheln', 'Tückische Klinge', 'Blutsturz' und 'Solarplexus' verursachte Schaden wird um 10% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2074,
                        "name": "Grausamer Kampf",
                        "icon": "ability_creature_disease_03",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Angriffskraft um 2% und jeglicher körperliche Schaden an Gegnern, die von Euch vergiftet wurden, wird um 2% erhöht."
                            },
                            {
                                "description": "Erhöht Eure gesamte Angriffskraft um 4% und jeglicher körperliche Schaden an Gegnern, die von Euch vergiftet wurden, wird um 4% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2075,
                        "name": "Die Schwachen ausbeuten",
                        "icon": "ability_rogue_preyontheweak",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Euer kritischer Schaden wird um 4% erhöht, wenn das Ziel über weniger Gesundheit verfügt, als Ihr (Prozentsatz der gesamten Gesundheit)."
                            },
                            {
                                "description": "Euer kritischer Schaden wird um 8% erhöht, wenn das Ziel über weniger Gesundheit verfügt, als Ihr (Prozentsatz der gesamten Gesundheit)."
                            },
                            {
                                "description": "Euer kritischer Schaden wird um 12% erhöht, wenn das Ziel über weniger Gesundheit verfügt, als Ihr (Prozentsatz der gesamten Gesundheit)."
                            },
                            {
                                "description": "Euer kritischer Schaden wird um 16% erhöht, wenn das Ziel über weniger Gesundheit verfügt, als Ihr (Prozentsatz der gesamten Gesundheit)."
                            },
                            {
                                "description": "Euer kritischer Schaden wird um 20% erhöht, wenn das Ziel über weniger Gesundheit verfügt, als Ihr (Prozentsatz der gesamten Gesundheit)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2076,
                        "name": "Mordlust",
                        "icon": "ability_rogue_murderspree",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "2 Min. Abklingzeit",
                                "range": "10 Meter Reichweite",
                                "description": "Taucht in die Schatten und springt von Gegner zu Gegner innerhalb von 10 Metern und greift einen Feind mit beiden Waffen alle 0,5 Sek. an, bis 5 Angriffe vollführt wurden. Während der Effekt anhält, ist der verursachte Schaden um 20% erhöht. Das gleiche Ziel kann mehrmals getroffen werden. Unsichtbare oder verstohlene Ziele können nicht getroffen werden."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Täuschung",
                "icon": "ability_stealth",
                "backgroundFile": "RogueSubtlety",
                "overlayColor": "#4c7fff",
                "description": "Ein tödlicher Schatten, der seiner ahnungslosen Beute auflauert und hinterrücks zuschlägt.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 2244,
                        "name": "Unerbittliche Stöße",
                        "icon": "ability_warrior_decisivestrike",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Gewährt Euren Finishing-Moves eine Chance von 4% je Combopunkt, 25 Energie wiederherzustellen."
                            },
                            {
                                "description": "Gewährt Euren Finishing-Moves eine Chance von 8% je Combopunkt, 25 Energie wiederherzustellen."
                            },
                            {
                                "description": "Gewährt Euren Finishing-Moves eine Chance von 12% je Combopunkt, 25 Energie wiederherzustellen."
                            },
                            {
                                "description": "Gewährt Euren Finishing-Moves eine Chance von 16% je Combopunkt, 25 Energie wiederherzustellen."
                            },
                            {
                                "description": "Gewährt Euren Finishing-Moves eine Chance von 20% je Combopunkt, 25 Energie wiederherzustellen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 241,
                        "name": "Meister der Täuschung",
                        "icon": "spell_shadow_charm",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Chance der Feinde, Euch zu entdecken, während Ihr Euch im Verstohlenheitsmodus befindet."
                            },
                            {
                                "description": "Verringert die Chance der Feinde, Euch zu entdecken, während Ihr Euch im Verstohlenheitsmodus befindet. Wirkungsvoller als 'Meister der Täuschung' (Rang 1)."
                            },
                            {
                                "description": "Verringert die Chance der Feinde, Euch zu entdecken, während Ihr Euch im Verstohlenheitsmodus befindet. Wirkungsvoller als 'Meister der Täuschung' (Rang 2)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 261,
                        "name": "Günstige Gelegenheit",
                        "icon": "ability_warrior_warcry",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Meucheln', 'Verstümmeln, 'Erdrosseln' und 'Hinterhalt' um 10%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Fähigkeiten 'Meucheln', 'Verstümmeln, 'Erdrosseln' und 'Hinterhalt' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1700,
                        "name": "Kunstgriff",
                        "icon": "ability_rogue_feint",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Die Bedrohungsverringerung Eurer Fähigkeit 'Finte' wird um 10% verbessert und die Chance, dass Ihr einen kritischen Treffer durch Nahkampf- oder Distanzangriffe erleidet, um 1% verringert."
                            },
                            {
                                "description": "Die Bedrohungsverringerung Eurer Fähigkeit 'Finte' wird um 20% verbessert und die Chance, dass Ihr einen kritischen Treffer durch Nahkampf- oder Distanzangriffe erleidet, um 2% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 262,
                        "name": "Faule Tricks",
                        "icon": "ability_sap",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Fähigkeiten 'Kopfnuss' und 'Blenden' um 2 Meter und verringert die Energiekosten von 'Kopfnuss' und 'Blenden' um 25%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Fähigkeiten 'Kopfnuss' und 'Blenden' um 5 Meter und verringert die Energiekosten von 'Kopfnuss' und 'Blenden' um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 244,
                        "name": "Tarnung",
                        "icon": "ability_stealth",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht Euer Tempo im Verstohlenheitszustand um 5% und verringert die Abklingzeit Eurer Fähigkeit 'Verstohlenheit' um 2 Sek."
                            },
                            {
                                "description": "Erhöht Euer Tempo im Verstohlenheitszustand um 10% und verringert die Abklingzeit Eurer Fähigkeit 'Verstohlenheit' um 4 Sek."
                            },
                            {
                                "description": "Erhöht Euer Tempo im Verstohlenheitszustand um 15% und verringert die Abklingzeit Eurer Fähigkeit 'Verstohlenheit' um 6 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 247,
                        "name": "Flüchtigkeit",
                        "icon": "spell_magic_lesserinvisibilty",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Verschwinden' und 'Blenden' um 30 Sek. und die Abklingzeit von 'Mantel der Schatten' um 15 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Verschwinden' und 'Blenden' um 60 Sek. und die Abklingzeit von 'Mantel der Schatten' um 30 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 303,
                        "name": "Geisterhafter Stoß",
                        "icon": "spell_shadow_curse",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Ein Stoß, der 125% Waffenschaden (oder bei Ausrüstung mit einem Dolch 180% Waffenschaden) verursacht und Eure Ausweichchance 7 Sek. lang um 15% erhöht. Gewährt 1 Combopunkt.",
                                "cost": "40 Energie",
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "range": "5 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1123,
                        "name": "Gezahnte Klingen",
                        "icon": "inv_sword_17",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Eure Angriffe ignorieren bis zu 3% Rüstung Eures Ziels. Zudem wird der von Eurer Fähigkeit 'Blutung' verursachte Schaden um 10% erhöht."
                            },
                            {
                                "description": "Eure Angriffe ignorieren bis zu 6% Rüstung Eures Ziels. Zudem wird der von Eurer Fähigkeit 'Blutung' verursachte Schaden um 20% erhöht."
                            },
                            {
                                "description": "Eure Angriffe ignorieren bis zu 9% Rüstung Eures Ziels. Zudem wird der von Eurer Fähigkeit 'Blutung' verursachte Schaden um 30% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 246,
                        "name": "Reinlegen",
                        "icon": "spell_nature_mirrorimage",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Gewährt Euch eine Chance von 33%, Eurem Ziel einen Combopunkt hinzuzufügen, nachdem Ihr dessen Angriff ausgewichen seid oder einem seiner Zauber vollständig widerstanden habt. Dieser Effekt kann nicht öfter als einmal pro Sekunde auftreten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 66%, Eurem Ziel einen Combopunkt hinzuzufügen, nachdem Ihr dessen Angriff ausgewichen seid oder einem seiner Zauber vollständig widerstanden habt. Dieser Effekt kann nicht öfter als einmal pro Sekunde auftreten."
                            },
                            {
                                "description": "Gewährt Euch eine Chance von 100%, Eurem Ziel einen Combopunkt hinzuzufügen, nachdem Ihr dessen Angriff ausgewichen seid oder einem seiner Zauber vollständig widerstanden habt. Dieser Effekt kann nicht öfter als einmal pro Sekunde auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 245,
                        "name": "Initiative",
                        "icon": "spell_shadow_fumble",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Gibt Euch eine Chance von 33%, Eurem Ziel einen weiteren Combopunkt hinzuzufügen, wenn Ihr Eure Fähigkeiten 'Hinterhalt', 'Erdrosseln' oder 'Fieser Trick' benutzt."
                            },
                            {
                                "description": "Gibt Euch eine Chance von 66%, Eurem Ziel einen weiteren Combopunkt hinzuzufügen, wenn Ihr Eure Fähigkeiten 'Hinterhalt', 'Erdrosseln' oder 'Fieser Trick' benutzt."
                            },
                            {
                                "description": "Gibt Euch eine Chance von 100%, Eurem Ziel einen weiteren Combopunkt hinzuzufügen, wenn Ihr Eure Fähigkeiten 'Hinterhalt', 'Erdrosseln' oder 'Fieser Trick' benutzt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 263,
                        "name": "Verbesserter Hinterhalt",
                        "icon": "ability_rogue_ambush",
                        "x": 2,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Hinterhalt' um 25%."
                            },
                            {
                                "description": "Erhöht die Chance auf einen kritischen Treffer Eurer Fähigkeit 'Hinterhalt' um 50%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1701,
                        "name": "Geschärfte Sinne",
                        "icon": "ability_ambush",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Verstohlenheitsentdeckung und verringert die Chance, dass Ihr von Zaubern und Distanzangriffen getroffen werdet, um 2%."
                            },
                            {
                                "description": "Erhöht Eure Verstohlenheitsentdeckung und verringert die Chance, dass Ihr von Zaubern und Distanzangriffen getroffen werdet, um 4%. Effektiver als 'Geschärfte Sinne' (Rang 1)."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 284,
                        "name": "Vorbereitung",
                        "icon": "spell_shadow_antishadow",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "8 Min. Abklingzeit",
                                "description": "Bei Aktivierung schließt diese Fähigkeit die Abklingzeit Eurer Fähigkeiten 'Entrinnen', 'Sprinten', 'Verschwinden', 'Kaltblütigkeit' und 'Schattenschritt' sofort ab."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 265,
                        "name": "Gemeinheiten",
                        "icon": "spell_shadow_summonsuccubus",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Fieser Trick' und 'Erdrosseln' um 10. Zusätzlich verursachen Eure Spezialfähigkeiten 10% mehr Schaden gegen Ziele mit weniger als 35% Gesundheit."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Fieser Trick' und 'Erdrosseln' um 20. Zusätzlich verursachen Eure Spezialfähigkeiten 20% mehr Schaden gegen Ziele mit weniger als 35% Gesundheit."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 681,
                        "name": "Blutsturz",
                        "icon": "spell_shadow_lifedrain",
                        "x": 3,
                        "y": 4,
                        "req": 1123,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "5 Meter Reichweite",
                                "cost": "35 Energie",
                                "description": "Ein sofortiger Stoß, der 110% Waffenschaden (oder bei Ausrüstung mit einem Dolch 160% Waffenschaden) sowie einen Blutsturz beim Ziel verursacht und so den dem Ziel zugefügten körperlichen Schaden um bis zu 13 erhöht. Hält 10 Aufladungen oder 15 Sek. lang an. Gewährt 1 Combopunkt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1713,
                        "name": "Meister des hinterhältigen Angriffs",
                        "icon": "ability_rogue_masterofsubtlety",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Angriffe, die aus dem Verstohlenheitsmodus und bis zu 6 Sekunden nach Verlassen des Verstohlenheitsmodus ausgeführt werden, verursachen 4% zusätzlichen Schaden."
                            },
                            {
                                "description": "Angriffe, die aus dem Verstohlenheitsmodus und bis zu 6 Sekunden nach Verlassen des Verstohlenheitsmodus ausgeführt werden, verursachen 7% zusätzlichen Schaden."
                            },
                            {
                                "description": "Angriffe, die aus dem Verstohlenheitsmodus und bis zu 6 Sekunden nach Verlassen des Verstohlenheitsmodus ausgeführt werden, verursachen 10% zusätzlichen Schaden."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1702,
                        "name": "Todbringer",
                        "icon": "inv_weapon_crossbow_11",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht Eure Angriffskraft um 2%."
                            },
                            {
                                "description": "Erhöht Eure Angriffskraft um 4%."
                            },
                            {
                                "description": "Erhöht Eure Angriffskraft um 6%."
                            },
                            {
                                "description": "Erhöht Eure Angriffskraft um 8%."
                            },
                            {
                                "description": "Erhöht Eure Angriffskraft um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1711,
                        "name": "Verhüllende Schatten",
                        "icon": "ability_rogue_envelopingshadows",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert den erlittenen Schaden durch Effekte, die auf ein Gebiet wirken, um 10%."
                            },
                            {
                                "description": "Verringert den erlittenen Schaden durch Effekte, die auf ein Gebiet wirken, um 20%."
                            },
                            {
                                "description": "Verringert den erlittenen Schaden durch Effekte, die auf ein Gebiet wirken, um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 381,
                        "name": "Konzentration",
                        "icon": "spell_shadow_possession",
                        "x": 1,
                        "y": 6,
                        "req": 284,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite",
                                "description": "Bei Benutzung werden Eurem Ziel 2 Combopunkte hinzugefügt. Wenn auf diese Weise erhaltene Combopunkte nicht innerhalb von 20 Sek. verbraucht oder weitere hinzugefügt werden, gehen die Combopunkte verloren."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1722,
                        "name": "Von der Schippe springen",
                        "icon": "ability_rogue_cheatdeath",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Ihr verfügt über eine Chance von 33%, dass ein Angriff, der Euch töten würde, Eure Gesundheit lediglich auf 10% verringert und Euer erlittener Schaden 3 Sek. lang um 90% verringert wird (modifiziert durch Abhärtung). Dieser Effekt kann nicht öfter als einmal pro Minute auftreten."
                            },
                            {
                                "description": "Ihr verfügt über eine Chance von 66%, dass ein Angriff, der Euch töten würde, Eure Gesundheit lediglich auf 10% verringert und Euer erlittener Schaden 3 Sek. lang um 90% verringert wird (modifiziert durch Abhärtung). Dieser Effekt kann nicht öfter als einmal pro Minute auftreten."
                            },
                            {
                                "description": "Ihr verfügt über eine Chance von 100%, dass ein Angriff, der Euch töten würde, Eure Gesundheit lediglich auf 10% verringert und Euer erlittener Schaden 3 Sek. lang um 90% verringert wird (modifiziert durch Abhärtung). Dieser Effekt kann nicht öfter als einmal pro Minute auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1712,
                        "name": "Finstere Berufung",
                        "icon": "ability_rogue_sinistercalling",
                        "x": 1,
                        "y": 7,
                        "req": 381,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um 3% und den prozentualen Schadensbonus von 'Meucheln' und 'Blutsturz' um zusätzliche 2%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um 6% und den prozentualen Schadensbonus von 'Meucheln' und 'Blutsturz' um zusätzliche 4%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um 9% und den prozentualen Schadensbonus von 'Meucheln' und 'Blutsturz' um zusätzliche 6%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um 12% und den prozentualen Schadensbonus von 'Meucheln' und 'Blutsturz' um zusätzliche 8%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Beweglichkeit um 15% und den prozentualen Schadensbonus von 'Meucheln' und 'Blutsturz' um zusätzliche 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2077,
                        "name": "Wegelagerei",
                        "icon": "ability_rogue_waylay",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Die Treffer Eurer Fähigkeiten 'Hinterhalt' und 'Meucheln' haben eine Chance von 50%, das Ziel aus dem Gleichgewicht zu bringen und die Zeit zwischen den Nah- und Distanzangriffen um 20% zu erhöhen sowie sein Bewegungstempo um 50% zu verringern. Hält 8 Sek. lang an."
                            },
                            {
                                "description": "Die Treffer Eurer Fähigkeiten 'Hinterhalt' und 'Meucheln' haben eine Chance von 100%, das Ziel aus dem Gleichgewicht zu bringen und die Zeit zwischen den Nah- und Distanzangriffen um 20% zu erhöhen sowie sein Bewegungstempo um 50% zu verringern. Hält 8 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2078,
                        "name": "Ehre unter Dieben",
                        "icon": "ability_rogue_honoramongstthieves",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erzielt eines Eurer Gruppenmitglieder mit einer Fähigkeit, einem Schadens- oder Heilzauber einen kritischen Treffer, habt Ihr eine Chance von 33%, einen zusätzlichen Combopunkt auf Eurem aktuellen Ziel dazuzugewinnen. Dieser Effekt kann nur einmal pro Sekunde auftreten."
                            },
                            {
                                "description": "Erzielt eines Eurer Gruppenmitglieder mit einer Fähigkeit, einem Schadens- oder Heilzauber einen kritischen Treffer, habt Ihr eine Chance von 66%, einen zusätzlichen Combopunkt auf Eurem aktuellen Ziel dazuzugewinnen. Dieser Effekt kann nur einmal pro Sekunde auftreten."
                            },
                            {
                                "description": "Erzielt eines Eurer Gruppenmitglieder mit einer Fähigkeit, einem Schadens- oder Heilzauber einen kritischen Treffer, habt Ihr eine Chance von 100%, einen zusätzlichen Combopunkt auf Eurem aktuellen Ziel dazuzugewinnen. Dieser Effekt kann nur einmal pro Sekunde auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1714,
                        "name": "Schattenschritt",
                        "icon": "ability_rogue_shadowstep",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Versucht, in die Schatten zu tauchen, um hinter dem Gegner zu erscheinen, und erhöht das Bewegungstempo 3 Sek. lang um 70%. Der Schaden Eurer nächsten Fähigkeit wird um 20% erhöht und die dabei erzeugte Bedrohung um 50% verringert. Hält 10 Sek. lang an.",
                                "cost": "10 Energie",
                                "castTime": "Spontanzauber",
                                "cooldown": "30 Sek. Abklingzeit",
                                "range": "25 Meter Reichweite"
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2079,
                        "name": "Schmutzige Tricks",
                        "icon": "ability_rogue_wrongfullyaccused",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Schurkenhandel', 'Ablenken' und 'Schattenschritt' um 5 Sek. sowie ihre Energiekosten um 5. Zudem wird die Abklingzeit von 'Vorbereitung' um 1.5 Min. verringert."
                            },
                            {
                                "description": "Verringert die Abklingzeit Eurer Fähigkeiten 'Schurkenhandel', 'Ablenken' und 'Schattenschritt' um 10 Sek. sowie ihre Energiekosten um 10. Zudem wird die Abklingzeit von 'Vorbereitung' um 3 Min. verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2080,
                        "name": "Hinterhältiger Mord",
                        "icon": "ability_rogue_slaughterfromtheshadows",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Meucheln' und 'Hinterhalt' um 4 und die Energiekosten von 'Blutsturz' um 1. Erhöht zudem jeglichen verursachten Schaden um 1."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Meucheln' und 'Hinterhalt' um 8 und die Energiekosten von 'Blutsturz' um 2. Erhöht zudem jeglichen verursachten Schaden um 2."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Meucheln' und 'Hinterhalt' um 12 und die Energiekosten von 'Blutsturz' um 3. Erhöht zudem jeglichen verursachten Schaden um 3."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Meucheln' und 'Hinterhalt' um 16 und die Energiekosten von 'Blutsturz' um 4. Erhöht zudem jeglichen verursachten Schaden um 4."
                            },
                            {
                                "description": "Verringert die Energiekosten Eurer Fähigkeiten 'Meucheln' und 'Hinterhalt' um 20 und die Energiekosten von 'Blutsturz' um 5. Erhöht zudem jeglichen verursachten Schaden um 5."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2081,
                        "name": "Schattentanz",
                        "icon": "ability_rogue_shadowdance",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "description": "Beginnt den Schattentanz, durch den 6 Sek. lang 'Kopfnuss', 'Erdrosseln', 'Hinterhalt', 'Fieser Trick', 'Konzentration', 'Taschendiebstahl' und 'Falle entschärfen' unabhängig von Verstohlenheit angewendet werden können."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [
{
    "391": {
        "name": "Glyphe 'Adrenalinrausch'",
        "id": "391",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42954",
        "spellKey": "56808",
        "spellId": "56808",
        "prettyName": "",
        "typeOrder": 2
    },
    "392": {
        "name": "Glyphe 'Hinterhalt'",
        "id": "392",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42955",
        "spellKey": "56813",
        "spellId": "56813",
        "prettyName": "",
        "typeOrder": 2
    },
    "393": {
        "name": "Glyphe 'Meucheln'",
        "id": "393",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42956",
        "spellKey": "56800",
        "spellId": "56800",
        "prettyName": "",
        "typeOrder": 2
    },
    "394": {
        "name": "Glyphe 'Klingenwirbel'",
        "id": "394",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42957",
        "spellKey": "56818",
        "spellId": "56818",
        "prettyName": "",
        "typeOrder": 2
    },
    "395": {
        "name": "Glyphe 'Verkrüppelndes Gift'",
        "id": "395",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42958",
        "spellKey": "56820",
        "spellId": "56820",
        "prettyName": "",
        "typeOrder": 2
    },
    "396": {
        "name": "Glyphe 'Tödlicher Wurf'",
        "id": "396",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42959",
        "spellKey": "56806",
        "spellId": "56806",
        "prettyName": "",
        "typeOrder": 2
    },
    "397": {
        "name": "Glyphe 'Entrinnen'",
        "id": "397",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42960",
        "spellKey": "56799",
        "spellId": "56799",
        "prettyName": "",
        "typeOrder": 2
    },
    "398": {
        "name": "Glyphe 'Ausweiden'",
        "id": "398",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42961",
        "spellKey": "56802",
        "spellId": "56802",
        "prettyName": "",
        "typeOrder": 2
    },
    "399": {
        "name": "Glyphe 'Rüstung schwächen'",
        "id": "399",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42962",
        "spellKey": "56803",
        "spellId": "56803",
        "prettyName": "",
        "typeOrder": 2
    },
    "400": {
        "name": "Glyphe 'Finte'",
        "id": "400",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42963",
        "spellKey": "56804",
        "spellId": "56804",
        "prettyName": "",
        "typeOrder": 2
    },
    "401": {
        "name": "Glyphe 'Erdrosseln'",
        "id": "401",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42964",
        "spellKey": "56812",
        "spellId": "56812",
        "prettyName": "",
        "typeOrder": 2
    },
    "402": {
        "name": "Glyphe 'Geisterhafter Stoß'",
        "id": "402",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42965",
        "spellKey": "56814",
        "spellId": "56814",
        "prettyName": "",
        "typeOrder": 2
    },
    "403": {
        "name": "Glyphe 'Solarplexus'",
        "id": "403",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42966",
        "spellKey": "56809",
        "spellId": "56809",
        "prettyName": "",
        "typeOrder": 2
    },
    "404": {
        "name": "Glyphe 'Blutsturz'",
        "id": "404",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42967",
        "spellKey": "56807",
        "spellId": "56807",
        "prettyName": "",
        "typeOrder": 2
    },
    "405": {
        "name": "Glyphe 'Vorbereitung'",
        "id": "405",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42968",
        "spellKey": "56819",
        "spellId": "56819",
        "prettyName": "",
        "typeOrder": 2
    },
    "406": {
        "name": "Glyphe 'Blutung'",
        "id": "406",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42969",
        "spellKey": "56801",
        "spellId": "56801",
        "prettyName": "",
        "typeOrder": 2
    },
    "407": {
        "name": "Glyphe 'Kopfnuss'",
        "id": "407",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42970",
        "spellKey": "56798",
        "spellId": "56798",
        "prettyName": "",
        "typeOrder": 2
    },
    "408": {
        "name": "Glyphe 'Lebenskraft'",
        "id": "408",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42971",
        "spellKey": "56805",
        "spellId": "56805",
        "prettyName": "",
        "typeOrder": 2
    },
    "409": {
        "name": "Glyphe 'Finsterer Stoß'",
        "id": "409",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42972",
        "spellKey": "56821",
        "spellId": "56821",
        "prettyName": "",
        "typeOrder": 2
    },
    "410": {
        "name": "Glyphe 'Zerhäckseln'",
        "id": "410",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42973",
        "spellKey": "56810",
        "spellId": "56810",
        "prettyName": "",
        "typeOrder": 2
    },
    "411": {
        "name": "Glyphe 'Sprinten'",
        "id": "411",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "42974",
        "spellKey": "56811",
        "spellId": "56811",
        "prettyName": "",
        "typeOrder": 2
    },
    "464": {
        "name": "Glyphe 'Ablenken'",
        "id": "464",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorrogue",
        "itemId": "43376",
        "spellKey": "58032",
        "spellId": "58032",
        "prettyName": "",
        "typeOrder": 2
    },
    "465": {
        "name": "Glyphe 'Schloss knacken'",
        "id": "465",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorrogue",
        "itemId": "43377",
        "spellKey": "58027",
        "spellId": "58027",
        "prettyName": "",
        "typeOrder": 2
    },
    "466": {
        "name": "Glyphe 'Taschendiebstahl'",
        "id": "466",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorrogue",
        "itemId": "43343",
        "spellKey": "58017",
        "spellId": "58017",
        "prettyName": "",
        "typeOrder": 2
    },
    "467": {
        "name": "Glyphe 'Sicheres Fallen'",
        "id": "467",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorrogue",
        "itemId": "43378",
        "spellKey": "58033",
        "spellId": "58033",
        "prettyName": "",
        "typeOrder": 2
    },
    "468": {
        "name": "Glyphe 'Verschwimmen'",
        "id": "468",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorrogue",
        "itemId": "43379",
        "spellKey": "58039",
        "spellId": "58039",
        "prettyName": "",
        "typeOrder": 2
    },
    "469": {
        "name": "Glyphe 'Verschwinden'",
        "id": "469",
        "type": 1,
        "description": "Ihr streicht Eure Waffen 50% schneller mit Giften ein.",
        "icon": "inv_glyph_minorrogue",
        "itemId": "43380",
        "spellKey": "58038",
        "spellId": "58038",
        "prettyName": "",
        "typeOrder": 2
    },
    "714": {
        "name": "Glyphe 'Blutgier'",
        "id": "714",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45761",
        "spellKey": "63249",
        "spellId": "63249",
        "prettyName": "",
        "typeOrder": 2
    },
    "715": {
        "name": "Glyphe 'Mordlust'",
        "id": "715",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45762",
        "spellKey": "63252",
        "spellId": "63252",
        "prettyName": "",
        "typeOrder": 2
    },
    "716": {
        "name": "Glyphe 'Schattentanz'",
        "id": "716",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45764",
        "spellKey": "63253",
        "spellId": "63253",
        "prettyName": "",
        "typeOrder": 2
    },
    "731": {
        "name": "Glyphe 'Dolchfächer'",
        "id": "731",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45766",
        "spellKey": "63254",
        "spellId": "63254",
        "prettyName": "",
        "typeOrder": 2
    },
    "732": {
        "name": "Glyphe 'Schurkenhandel'",
        "id": "732",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45767",
        "spellKey": "63256",
        "spellId": "63256",
        "prettyName": "",
        "typeOrder": 2
    },
    "733": {
        "name": "Glyphe 'Verstümmeln'",
        "id": "733",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45768",
        "spellKey": "63268",
        "spellId": "63268",
        "prettyName": "",
        "typeOrder": 2
    },
    "734": {
        "name": "Glyphe 'Mantel der Schatten'",
        "id": "734",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorrogue",
        "itemId": "45769",
        "spellKey": "63269",
        "spellId": "63269",
        "prettyName": "",
        "typeOrder": 2
    },
    "791": {
        "name": "Glyph of Envenom",
        "id": "791",
        "type": 0,
        "description": "Your Envenom ability no longer consumes Deadly Poison from your target.",
        "icon": "inv_glyph_majorrogue",
        "itemId": "45908",
        "spellKey": "64199",
        "spellId": "64199",
        "prettyName": "",
        "typeOrder": 2
    }
}
    ]
}