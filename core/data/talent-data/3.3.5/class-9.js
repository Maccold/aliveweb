{
    "talentData": {
        "characterClass": {
            "classId": 9,
            "name": "Hexenmeister",
            "powerType": "MANA",
            "powerTypeId": 0,
            "powerTypeSlug": "mana"
        },
        "talentTrees": [
            {
                "name": "Gebrechen",
                "icon": "spell_shadow_deathcoil",
                "backgroundFile": "WarlockCurses",
                "overlayColor": "#00ff99",
                "description": "Ein Meister der Schattenmagie, dessen Spezialität es sowohl ist, im Lauf der Zeit Schaden zuzufügen, als auch Furcht- und Schwächungszauber zu wirken.",
                "treeNo": 0,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1284,
                        "name": "Verbesserter Fluch der Pein",
                        "icon": "spell_shadow_curseofsargeras",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurem 'Fluch der Pein' angerichteten Schaden um 5%."
                            },
                            {
                                "description": "Erhöht den von Eurem 'Fluch der Pein' angerichteten Schaden um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1005,
                        "name": "Unterdrückung",
                        "icon": "spell_shadow_unsummonbuilding",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die Trefferchance Eurer Zauber um 1% und verringert die Manakosten Eurer Gebrechenszauber um 2%."
                            },
                            {
                                "description": "Erhöht die Trefferchance Eurer Zauber um 2% und verringert die Manakosten Eurer Gebrechenszauber um 4%."
                            },
                            {
                                "description": "Erhöht die Trefferchance Eurer Zauber um 3% und verringert die Manakosten Eurer Gebrechenszauber um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1003,
                        "name": "Verbesserte Verderbnis",
                        "icon": "spell_shadow_abominationexplosion",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 2% und erhöht die kritische Trefferchance von 'Saat der Verderbnis' um 1%."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 4% und erhöht die kritische Trefferchance von 'Saat der Verderbnis' um 2%."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 6% und erhöht die kritische Trefferchance von 'Saat der Verderbnis' um 3%."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 8% und erhöht die kritische Trefferchance von 'Saat der Verderbnis' um 4%."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um 10% und erhöht die kritische Trefferchance von 'Saat der Verderbnis' um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1006,
                        "name": "Verbesserter Fluch der Schwäche",
                        "icon": "spell_shadow_curseofmannoroth",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Angriffskraftverringerung Eures Zaubers 'Fluch der Schwäche' um 10%."
                            },
                            {
                                "description": "Erhöht die Angriffskraftverringerung Eures Zaubers 'Fluch der Schwäche' um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1101,
                        "name": "Verbesserter Seelendieb",
                        "icon": "spell_shadow_haunting",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Stellt 7% Eures maximalen Manas wieder her, wenn Euer Ziel von Euch getötet wird, während es unter dem Einfluss von 'Seelendieb' steht. Zusätzlich verursachen Eure Gebrechen 10% weniger Bedrohung."
                            },
                            {
                                "description": "Stellt 15% Eures maximalen Manas wieder her, wenn Euer Ziel von Euch getötet wird, während es unter dem Einfluss von 'Seelendieb' steht. Zusätzlich verursachen Eure Gebrechen 20% weniger Bedrohung."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1007,
                        "name": "Verbesserter Aderlass",
                        "icon": "spell_shadow_burningspirit",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die durch Euren Zauber 'Aderlass' gewährte Manamenge um 10%."
                            },
                            {
                                "description": "Erhöht die durch Euren Zauber 'Aderlass' gewährte Manamenge um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1004,
                        "name": "Seelenentzug",
                        "icon": "spell_shadow_lifedrain02",
                        "x": 3,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die durch Eure Zauber 'Blutsauger' und 'Seelendieb' abgezogene Menge pro auf das Ziel wirkenden Gebrechenseffekt um zusätzliche 3%, bis zu einem Maximum von 9% zusätzlichem Effekt."
                            },
                            {
                                "description": "Erhöht die durch Eure Zauber 'Blutsauger' und 'Seelendieb' abgezogene Menge pro auf das Ziel wirkenden Gebrechenseffekt um zusätzliche 6%, bis zu einem Maximum von 18% zusätzlichem Effekt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2205,
                        "name": "Verbesserte Furcht",
                        "icon": "spell_shadow_possession",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Furcht' ruft beim Ende seiner Dauer den Effekt 'Alptraum' hervor. Dieser Effekt verringert 5 Sek. lang das Bewegungstempo des Ziels um 15%."
                            },
                            {
                                "description": "Euer Zauber 'Furcht' ruft beim Ende seiner Dauer den Effekt 'Alptraum' hervor. Dieser Effekt verringert 5 Sek. lang das Bewegungstempo des Ziels um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1001,
                        "name": "Teufelskonzentration",
                        "icon": "spell_shadow_fingerofdeath",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Blutsauger', 'Mana entziehen', 'Seelendieb', 'Instabiles Gebrechen' und 'Heimsuchung' um 23%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Blutsauger', 'Mana entziehen', 'Seelendieb', 'Instabiles Gebrechen' und 'Heimsuchung' um 46%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken von 'Blutsauger', 'Mana entziehen', 'Seelendieb', 'Instabiles Gebrechen' und 'Heimsuchung' um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1061,
                        "name": "Fluch verstärken",
                        "icon": "spell_shadow_contagion",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die globale Abklingzeit Eurer Flüche um 0.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1021,
                        "name": "Grimmige Reichweite",
                        "icon": "spell_shadow_callofbone",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Gebrechenzauber um 10%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Gebrechenzauber um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1002,
                        "name": "Einbruch der Nacht",
                        "icon": "spell_shadow_twilight",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Gewährt Euren Zaubern 'Verderbnis' und 'Blutsauger' eine Chance von 2%, Euch in einen Schattentrancezustand zu versetzen, nachdem Ihr dem Feind Schaden zugefügt habt. Der Schattentrancezustand verringert die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' um 100%."
                            },
                            {
                                "description": "Gewährt Euren Zaubern 'Verderbnis' und 'Blutsauger' eine Chance von 4%, Euch in einen Schattentrancezustand zu versetzen, nachdem Ihr dem Feind Schaden zugefügt habt. Der Schattentrancezustand verringert die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1764,
                        "name": "Machtvolle Verderbnis",
                        "icon": "spell_shadow_abominationexplosion",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um einen Wert, der 12% Eurer Zaubermacht entspricht."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um einen Wert, der 24% Eurer Zaubermacht entspricht."
                            },
                            {
                                "description": "Erhöht den Schaden Eures Zaubers 'Verderbnis' um einen Wert, der 36% Eurer Zaubermacht entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1763,
                        "name": "Umschlingende Schatten",
                        "icon": "spell_shadow_shadowembrace",
                        "x": 0,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 1% erhöht und seine erhaltene regelmäßige Heilung um 2% verringert. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 2% erhöht und seine erhaltene regelmäßige Heilung um 4% verringert. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 3% erhöht und seine erhaltene regelmäßige Heilung um 6% verringert. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 4% erhöht und seine erhaltene regelmäßige Heilung um 8% verringert. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz' und 'Heimsuchung' verursachen den Effekt 'Umschlingende Schatten', der jeglichen von Euch verursachten regelmäßigen Schattenschaden gegen das Ziel um 5% erhöht und seine erhaltene regelmäßige Heilung um 10% verringert. Hält 12 Sek. lang an. Bis zu 3-mal stapelbar."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1041,
                        "name": "Lebensentzug",
                        "icon": "spell_shadow_requiem",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Wenn Ihr durch Euren Zauber 'Verderbnis' Schaden verursacht, werdet Ihr sofort um 40% des zugefügten Schadens geheilt. Zusätzlich wird der über Zeit verursachte Schaden Eurer Zauber 'Verderbnis', 'Saat der Verderbnis' und 'Instabiles Gebrechen' um 5% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1081,
                        "name": "Fluch der Erschöpfung",
                        "icon": "spell_shadow_grimward",
                        "x": 2,
                        "y": 4,
                        "req": 1061,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "30 Meter Reichweite",
                                "cost": "6% des Grundmanas",
                                "description": "Verringert das Bewegungstempo des Ziels 12 Sek. lang um 30%. Es kann immer nur jeweils ein Fluch pro Hexenmeister auf einem beliebigen Ziel aktiv sein."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1873,
                        "name": "Verbesserter Teufelsjäger",
                        "icon": "spell_shadow_summonfelhunter",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Jedes Mal, wenn Euer Teufelsjäger mit seiner Fähigkeit 'Schattenbiss' trifft, erhält er 4% seines maximalen Manas zurück und die Abklingzeit dieser Fähigkeit wird um 2 Sek. verringert. Zudem wird der Effekt des Zaubers 'Teufelsintelligenz' Eures Teufelsjägers um 5% erhöht."
                            },
                            {
                                "description": "Jedes Mal, wenn Euer Teufelsjäger mit seiner Fähigkeit 'Schattenbiss' trifft, erhält er 8% seines maximalen Manas zurück und die Abklingzeit dieser Fähigkeit wird um 4 Sek. verringert. Der Effekt des Zaubers 'Teufelsintelligenz' Eures Teufelsjägers wird um 10% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1042,
                        "name": "Schattenbeherrschung",
                        "icon": "spell_shadow_shadetruesight",
                        "x": 1,
                        "y": 5,
                        "req": 1041,
                        "ranks": [
                            {
                                "description": "Erhöht den durch Eure Schattenzauber zugefügten Schaden oder das abgesaugte Leben sowie den durch den Zauber 'Schattenbiss' Eures Teufelsjägers zugefügten Schaden um 3%."
                            },
                            {
                                "description": "Erhöht den durch Eure Schattenzauber zugefügten Schaden oder das abgesaugte Leben sowie den durch den Zauber 'Schattenbiss' Eures Teufelsjägers zugefügten Schaden um 6%."
                            },
                            {
                                "description": "Erhöht den durch Eure Schattenzauber zugefügten Schaden oder das abgesaugte Leben sowie den durch den Zauber 'Schattenbiss' Eures Teufelsjägers zugefügten Schaden um 9%."
                            },
                            {
                                "description": "Erhöht den durch Eure Schattenzauber zugefügten Schaden oder das abgesaugte Leben sowie den durch den Zauber 'Schattenbiss' Eures Teufelsjägers zugefügten Schaden um 12%."
                            },
                            {
                                "description": "Erhöht den durch Eure Schattenzauber zugefügten Schaden oder das abgesaugte Leben sowie den durch den Zauber 'Schattenbiss' Eures Teufelsjägers zugefügten Schaden um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1878,
                        "name": "Ausrottung",
                        "icon": "ability_warlock_eradication",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Wenn Ihr durch den Zauber 'Verderbnis' Schaden verursacht, besteht eine Chance von 6%, dass Euer Zaubertempo 10 Sek. lang um 6% erhöht wird."
                            },
                            {
                                "description": "Wenn Ihr durch den Zauber 'Verderbnis' Schaden verursacht, besteht eine Chance von 6%, dass Euer Zaubertempo 10 Sek. lang um 12% erhöht wird."
                            },
                            {
                                "description": "Wenn Ihr durch den Zauber 'Verderbnis' Schaden verursacht, besteht eine Chance von 6%, dass Euer Zaubertempo 10 Sek. lang um 20% erhöht wird."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1669,
                        "name": "Ansteckung",
                        "icon": "spell_shadow_painfulafflictions",
                        "x": 1,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Fluch der Pein', 'Verderbnis' und 'Saat der Verderbnis' um 1% und verringert die Chance, dass sowohl Eure hilfreichen Zauber der Kategorie 'Gebrechen' als auch ihre Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um zusätzliche 6%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Fluch der Pein', 'Verderbnis' und 'Saat der Verderbnis' um 2% und verringert die Chance, dass sowohl Eure hilfreichen Zauber der Kategorie 'Gebrechen' als auch ihre Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um zusätzliche 12%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Fluch der Pein', 'Verderbnis' und 'Saat der Verderbnis' um 3% und verringert die Chance, dass sowohl Eure hilfreichen Zauber der Kategorie 'Gebrechen' als auch ihre Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um zusätzliche 18%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Fluch der Pein', 'Verderbnis' und 'Saat der Verderbnis' um 4% und verringert die Chance, dass sowohl Eure hilfreichen Zauber der Kategorie 'Gebrechen' als auch ihre Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um zusätzliche 24%."
                            },
                            {
                                "description": "Erhöht den Schaden Eurer Zauber 'Fluch der Pein', 'Verderbnis' und 'Saat der Verderbnis' um 5% und verringert die Chance, dass sowohl Eure hilfreichen Zauber der Kategorie 'Gebrechen' als auch ihre Effekte, die Schaden im Laufe der Zeit verursachen, gebannt werden, um zusätzliche 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1022,
                        "name": "Dunkler Pakt",
                        "icon": "spell_shadow_darkritual",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Zieht 305 Mana Eures beschworenen Dämons ab und gibt 100% an Euch zurück."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1668,
                        "name": "Verbessertes Schreckensgeheul",
                        "icon": "spell_shadow_deathscream",
                        "x": 0,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Schreckensgeheul' um 0.8 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Zaubers 'Schreckensgeheul' um 1.5 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1667,
                        "name": "Verhängnis",
                        "icon": "spell_shadow_curseofachimonde",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Zauberschaden um 1% und erhöht die regelmäßige kritische Trefferchance Eurer Zauber 'Verderbnis' und 'Instabiles Gebrechen' um 3%."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um 2% und erhöht die regelmäßige kritische Trefferchance Eurer Zauber 'Verderbnis' und 'Instabiles Gebrechen' um 6%."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um 3% und erhöht die regelmäßige kritische Trefferchance Eurer Zauber 'Verderbnis' und 'Instabiles Gebrechen' um 9%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1875,
                        "name": "Umarmung des Todes",
                        "icon": "spell_shadow_deathsembrace",
                        "x": 0,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht, wenn Eure Gesundheit 20% oder weniger beträgt, die von Eurem Zauber 'Blutsauger' entzogene Menge um 10%. Erhöht den verursachten Schaden Eurer Schattenzauber um 4%, wenn die Gesundheit Eures Ziels 35% oder weniger beträgt."
                            },
                            {
                                "description": "Erhöht, wenn Eure Gesundheit 20% oder weniger beträgt, die von Eurem Zauber 'Blutsauger' entzogene Menge um 20%. Erhöht den verursachten Schaden Eurer Schattenzauber um 8%, wenn die Gesundheit Eures Ziels 35% oder weniger beträgt."
                            },
                            {
                                "description": "Erhöht, wenn Eure Gesundheit 20% oder weniger beträgt, die von Eurem Zauber 'Blutsauger' entzogene Menge um 30%. Erhöht den verursachten Schaden Eurer Schattenzauber um 12%, wenn die Gesundheit Eures Ziels 35% oder weniger beträgt."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1670,
                        "name": "Instabiles Gebrechen",
                        "icon": "spell_shadow_unstableaffliction_3",
                        "x": 1,
                        "y": 8,
                        "req": 1669,
                        "ranks": [
                            {
                                "castTime": "1,5 Sek. Zauberzeit",
                                "range": "30 Meter Reichweite",
                                "cost": "15% des Grundmanas",
                                "description": "Schattenenergie zerstört langsam das Ziel, im Verlauf von 15 Sek. wird insgesamt 550 Schaden verursacht. Sollte 'Instabiles Gebrechen' gebannt werden, erleidet der Bannende 990 Schaden und wird 5 Sek. lang zum Schweigen gebracht. Es kann pro Hexenmeister jeweils nur ein Effekt von 'Feuerbrand' oder 'Instabiles Gebrechen' gleichzeitig auf einem beliebigen Ziel aktiv sein."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2245,
                        "name": "Pandemie",
                        "icon": "spell_shadow_unstableaffliction_2",
                        "x": 2,
                        "y": 8,
                        "req": 1670,
                        "ranks": [
                            {
                                "description": "Gewährt dem regelmäßigen Schaden Eurer Zauber 'Verderbnis' und 'Instabiles Gebrechen' die Fähigkeit, für um 100% erhöhten Schaden kritisch zu treffen. Zudem wird der kritische Schadensbonus Eures Zaubers 'Heimsuchung' um 100% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1876,
                        "name": "Immerwährende Gebrechen",
                        "icon": "ability_warlock_everlastingaffliction",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Verderbnis' und 'Instabiles Gebrechen' erhalten zusätzlich 1% Eurer Boni auf Zauberschadenseffekte, und Eure Zauber 'Blutsauger', 'Seelendieb', 'Schattenblitz' und 'Heimsuchung' haben eine Chance von 20%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
                            },
                            {
                                "description": "Eure Zauber 'Verderbnis' und 'Instabiles Gebrechen' erhalten zusätzlich 2% Eurer Boni auf Zauberschadenseffekte, und Eure Zauber 'Blutsauger', 'Seelendieb', 'Schattenblitz' und 'Heimsuchung' haben eine Chance von 40%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
                            },
                            {
                                "description": "Eure Zauber 'Verderbnis' und 'Instabiles Gebrechen' erhalten zusätzlich 3% Eurer Boni auf Zauberschadenseffekte, und Eure Zauber 'Blutsauger', 'Seelendieb', 'Schattenblitz' und 'Heimsuchung' haben eine Chance von 60%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
                            },
                            {
                                "description": "Eure Zauber 'Verderbnis' und 'Instabiles Gebrechen' erhalten zusätzlich 4% Eurer Boni auf Zauberschadenseffekte, und Eure Zauber 'Blutsauger', 'Seelendieb', 'Schattenblitz' und 'Heimsuchung' haben eine Chance von 80%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
                            },
                            {
                                "description": "Eure Zauber 'Verderbnis' und 'Instabiles Gebrechen' erhalten zusätzlich 5% Eurer Boni auf Zauberschadenseffekte, und Eure Zauber 'Blutsauger', 'Seelendieb', 'Schattenblitz' und 'Heimsuchung' haben eine Chance von 100%, die Dauer Eures Zaubers 'Verderbnis' auf dem Ziel zurückzusetzen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2041,
                        "name": "Heimsuchung",
                        "icon": "ability_warlock_haunt",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Ihr entsendet eine verderbte Seele in das Ziel, die 405 bis 473 Schattenschaden verursacht und 12 Sek. lang den Schaden aller Eurer Schaden über Zeit verursachenden Effekte auf dem Ziel um 20% erhöht. Endet die Heimsuchung oder wird sie gebannt, kehrt die Seele zu Euch zurück, um Euch um 100% des von ihr verursachten Schadens zu heilen.",
                                "cost": "12% des Grundmanas",
                                "castTime": "1,5 Sek. Zauberzeit",
                                "cooldown": "8 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 0
            },
            {
                "name": "Dämonologie",
                "icon": "spell_shadow_metamorphosis",
                "backgroundFile": "WarlockSummoning",
                "overlayColor": "#ff0000",
                "description": "Ein Hexenmeister, der Schatten- und Feuermagie zusammen mit mächtigen Dämonen nutzt.",
                "treeNo": 1,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 1221,
                        "name": "Verbesserter Gesundheitsstein",
                        "icon": "inv_stone_04",
                        "x": 0,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht die von Eurem 'Gesundheitsstein' wiederhergestellte Gesundheit um 10%."
                            },
                            {
                                "description": "Erhöht die von Eurem 'Gesundheitsstein' wiederhergestellte Gesundheit um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1222,
                        "name": "Verbesserter Wichtel",
                        "icon": "spell_shadow_summonimp",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den Effekt der Zauber 'Feuerblitz', 'Feuerschild' und 'Blutpakt' um 10%."
                            },
                            {
                                "description": "Erhöht den Effekt der Zauber 'Feuerblitz', 'Feuerschild' und 'Blutpakt' um 20%."
                            },
                            {
                                "description": "Erhöht den Effekt der Zauber 'Feuerblitz', 'Feuerschild' und 'Blutpakt' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1223,
                        "name": "Dämonische Umarmung",
                        "icon": "spell_shadow_metamorphosis",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht Eure gesamte Ausdauer um 4%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Ausdauer um 7%."
                            },
                            {
                                "description": "Erhöht Eure gesamte Ausdauer um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1883,
                        "name": "Teufelssynergie",
                        "icon": "spell_shadow_felmending",
                        "x": 3,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Ihr habt eine Chance von 50%, Euren Begleiter um 15% des von Euch verursachten Zauberschadens zu heilen."
                            },
                            {
                                "description": "Ihr habt eine Chance von 100%, Euren Begleiter um 15% des von Euch verursachten Zauberschadens zu heilen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1224,
                        "name": "Verbesserte Lebenslinie",
                        "icon": "spell_shadow_lifedrain",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die durch Euren Zauber 'Lebenslinie' übertragene Gesundheit um 10% und verringert die Gesundheitskosten um 10%. Zusätzlich wird der erlittene Schaden Eures Dämons, wenn er unter dem Effekt von 'Lebenslinie' steht, um 15% verringert."
                            },
                            {
                                "description": "Erhöht die durch Euren Zauber 'Lebenslinie' übertragene Gesundheit um 20% und verringert die Gesundheitskosten um 20%. Zusätzlich wird der erlittene Schaden Eures Dämons, wenn er unter dem Effekt von 'Lebenslinie' steht, um 30% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1225,
                        "name": "Dämonische Brutalität",
                        "icon": "spell_shadow_summonvoidwalker",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Wirksamkeit der Zauber 'Qual', 'Schatten verzehren', 'Opferung' und 'Leiden' Eures Leerwandlers um 10% und erhöht den Angriffskraftbonus des Effekts 'Dämonische Raserei' Eurer Teufelswache um 1%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit der Zauber 'Qual', 'Schatten verzehren', 'Opferung' und 'Leiden' Eures Leerwandlers um 20% und erhöht den Angriffskraftbonus des Effekts 'Dämonische Raserei' Eurer Teufelswache um 2%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit der Zauber 'Qual', 'Schatten verzehren', 'Opferung' und 'Leiden' Eures Leerwandlers um 30% und erhöht den Angriffskraftbonus des Effekts 'Dämonische Raserei' Eurer Teufelswache um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1242,
                        "name": "Teuflische Vitalität",
                        "icon": "spell_holy_magicalsentry",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht die Ausdauer und Intelligenz Eures Wichtels, Leerwandlers, Sukkubus, Teufelsjägers und Eurer Teufelswache um 5% und erhöht sowohl Eure maximale Gesundheit als auch Euer maximales Mana um 1%."
                            },
                            {
                                "description": "Erhöht die Ausdauer und Intelligenz Eures Wichtels, Leerwandlers, Sukkubus, Teufelsjägers und Eurer Teufelswache um 10% und erhöht sowohl Eure maximale Gesundheit als auch Euer maximales Mana um 2%."
                            },
                            {
                                "description": "Erhöht die Ausdauer und Intelligenz Eures Wichtels, Leerwandlers, Sukkubus, Teufelsjägers und Eurer Teufelswache um 15% und erhöht sowohl Eure maximale Gesundheit als auch Euer maximales Mana um 3%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1243,
                        "name": "Verbesserter Sukkubus",
                        "icon": "spell_shadow_summonsuccubus",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit des Zaubers 'Verführung' Eures Sukkubus um 22% und erhöht die Dauer seiner Zauber 'Verführung' und 'Geringe Unsichtbarkeit' um 10%."
                            },
                            {
                                "description": "Verringert die Zauberzeit des Zaubers 'Verführung' Eures Sukkubus um 44% und erhöht die Dauer seiner Zauber 'Verführung' und 'Geringe Unsichtbarkeit' um 20%."
                            },
                            {
                                "description": "Verringert die Zauberzeit des Zaubers 'Verführung' Eures Sukkubus um 66% und erhöht die Dauer seiner Zauber 'Verführung' und 'Geringe Unsichtbarkeit' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1282,
                        "name": "Seelenverbindung",
                        "icon": "spell_shadow_gathershadows",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "range": "100 Meter Reichweite",
                                "cost": "16% des Grundmanas",
                                "description": "Bei Aktivierung werden 20% des von Euch erlittenen Schadens auf Euren Wichtel, Leerwandler, Sukkubus, Teufelsjäger, Teufelswache oder versklavten Dämon umgelenkt. Dieser Schaden kann nicht vermieden werden. Hält an, solange der Dämon aktiv ist und kontrolliert wird."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1226,
                        "name": "Teufelsbeherrschung",
                        "icon": "spell_nature_removecurse",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "3 Min. Abklingzeit",
                                "description": "Euer nächster Beschwörungszauber für Wichtel, Leerwandler, Sukkubus, Teufelsjäger oder Teufelswache hat eine um 5.5 Sek. verringerte Zauberzeit und um 50% verringerte Manakosten."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1671,
                        "name": "Dämonische Ägide",
                        "icon": "spell_shadow_ragingscream",
                        "x": 3,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Dämonenrüstung und Teufelsrüstung um 10%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Dämonenrüstung und Teufelsrüstung um 20%."
                            },
                            {
                                "description": "Erhöht die Wirksamkeit Eurer Dämonenrüstung und Teufelsrüstung um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1262,
                        "name": "Unheilige Macht",
                        "icon": "spell_shadow_shadowworddominate",
                        "x": 1,
                        "y": 3,
                        "req": 1282,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eures Leerwandlers, Sukkubus, Teufelsjägers sowie den Nahkampfschaden Eurer Teufelswache und den Feuerblitzschaden Eures Wichtels um 4%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Leerwandlers, Sukkubus, Teufelsjägers sowie den Nahkampfschaden Eurer Teufelswache und den Feuerblitzschaden Eures Wichtels um 8%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Leerwandlers, Sukkubus, Teufelsjägers sowie den Nahkampfschaden Eurer Teufelswache und den Feuerblitzschaden Eures Wichtels um 12%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Leerwandlers, Sukkubus, Teufelsjägers sowie den Nahkampfschaden Eurer Teufelswache und den Feuerblitzschaden Eures Wichtels um 16%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Leerwandlers, Sukkubus, Teufelsjägers sowie den Nahkampfschaden Eurer Teufelswache und den Feuerblitzschaden Eures Wichtels um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1227,
                        "name": "Meister der Beschwörung",
                        "icon": "spell_shadow_impphaseshift",
                        "x": 2,
                        "y": 3,
                        "req": 1226,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eures Beschwörungszaubers für Wichtel, Leerwandler, Sukkubus, Teufelsjäger und Teufelswache um 2 Sek. und die Manakosten um 20%."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eures Beschwörungszaubers für Wichtel, Leerwandler, Sukkubus, Teufelsjäger und Teufelswache um 4 Sek. und die Manakosten um 40%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1281,
                        "name": "Mananachschub",
                        "icon": "spell_shadow_manafeed",
                        "x": 0,
                        "y": 4,
                        "req": 1262,
                        "ranks": [
                            {
                                "description": "Wenn Ihr durch 'Mana entziehen' oder 'Aderlass' Mana bekommt, erhält Euer beschworener Dämon 100% des von Euch gewonnenen Manas."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1261,
                        "name": "Meister der Herbeizauberung",
                        "icon": "inv_ammo_firetar",
                        "x": 2,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht die durch Eure Feuersteine und Zaubersteine gewonnenen Kampfwerte um 150%."
                            },
                            {
                                "description": "Erhöht die durch Eure Feuersteine und Zaubersteine gewonnenen Kampfwerte um 300%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1244,
                        "name": "Meister der Dämonologie",
                        "icon": "spell_shadow_shadowpact",
                        "x": 1,
                        "y": 5,
                        "req": 1262,
                        "ranks": [
                            {
                                "description": "Gewährt Hexenmeister und beschworenem Dämon einen Effekt, solange dieser Dämon aktiv ist.<br/><br/>Wichtel - Erhöht Euren Feuerschaden um 1% und erhöht Eure kritische Trefferchance mit Feuerzaubern um 1%.<br/><br/>Leerwandler - Verringert erlittenen körperlichen Schaden um 2%.<br/><br/>Sukkubus - Erhöht Euren Schattenschaden um 1% und erhöht Eure kritische Trefferchance mit Schattenzaubern um 1%.<br/><br/>Teufelsjäger - Verringert jeglichen erlittenen Zauberschaden um 2%.<br/><br/>Teufelswache - Erhöht jeglichen verursachten Schaden um 1% und verringert jeglichen erlittenen Schaden um 1%."
                            },
                            {
                                "description": "Gewährt Hexenmeister und beschworenem Dämon einen Effekt, solange dieser Dämon aktiv ist.<br/><br/>Wichtel - Erhöht Euren Feuerschaden um 2% und erhöht Eure kritische Trefferchance mit Feuerzaubern um 2%.<br/><br/>Leerwandler - Verringert erlittenen körperlichen Schaden um 4%.<br/><br/>Sukkubus - Erhöht Euren Schattenschaden um 2% und erhöht Eure kritische Trefferchance mit Schattenzaubern um 2%.<br/><br/>Teufelsjäger - Verringert jeglichen erlittenen Zauberschaden um 4%.<br/><br/>Teufelswache - Erhöht jeglichen verursachten Schaden um 2% und verringert jeglichen erlittenen Schaden um 2%."
                            },
                            {
                                "description": "Gewährt Hexenmeister und beschworenem Dämon einen Effekt, solange dieser Dämon aktiv ist.<br/><br/>Wichtel - Erhöht Euren Feuerschaden um 3% und erhöht Eure kritische Trefferchance mit Feuerzaubern um 3%.<br/><br/>Leerwandler - Verringert erlittenen körperlichen Schaden um 6%.<br/><br/>Sukkubus - Erhöht Euren Schattenschaden um 3% und erhöht Eure kritische Trefferchance mit Schattenzaubern um 3%.<br/><br/>Teufelsjäger - Verringert jeglichen erlittenen Zauberschaden um 6%.<br/><br/>Teufelswache - Erhöht jeglichen verursachten Schaden um 3% und verringert jeglichen erlittenen Schaden um 3%."
                            },
                            {
                                "description": "Gewährt Hexenmeister und beschworenem Dämon einen Effekt, solange dieser Dämon aktiv ist.<br/><br/>Wichtel - Erhöht Euren Feuerschaden um 4% und erhöht Eure kritische Trefferchance mit Feuerzaubern um 4%.<br/><br/>Leerwandler - Verringert erlittenen körperlichen Schaden um 8%.<br/><br/>Sukkubus - Erhöht Euren Schattenschaden um 4% und erhöht Eure kritische Trefferchance mit Schattenzaubern um 4%.<br/><br/>Teufelsjäger - Verringert jeglichen erlittenen Zauberschaden um 8%.<br/><br/>Teufelswache - Erhöht jeglichen verursachten Schaden um 4% und verringert jeglichen erlittenen Schaden um 4%."
                            },
                            {
                                "description": "Gewährt Hexenmeister und beschworenem Dämon einen Effekt, solange dieser Dämon aktiv ist.<br/><br/>Wichtel - Erhöht Euren Feuerschaden um 5% und erhöht Eure kritische Trefferchance mit Feuerzaubern um 5%.<br/><br/>Leerwandler - Verringert erlittenen körperlichen Schaden um 10%.<br/><br/>Sukkubus - Erhöht Euren Schattenschaden um 5% und erhöht Eure kritische Trefferchance mit Schattenzaubern um 5%.<br/><br/>Teufelsjäger - Verringert jeglichen erlittenen Zauberschaden um 10%.<br/><br/>Teufelswache - Erhöht jeglichen verursachten Schaden um 5% und verringert jeglichen erlittenen Schaden um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1283,
                        "name": "Geschmolzener Kern",
                        "icon": "ability_warlock_moltencore",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht die Effektdauer Eures Zaubers 'Feuerbrand' um 3 Sek., zudem habt Ihr eine Chance von 4%, den Effekt 'Geschmolzener Kern' zu gewinnen, wenn Eurer Zauber 'Verderbnis' Schaden verursacht. Der Effekt 'Geschmolzener Kern' verstärkt die Wirkung der 3 innerhalb der nächsten 15 Sek. gewirkten Zauber 'Verbrennen' oder 'Seelenfeuer'.<br/><br/>Verbrennen - Erhöht den verursachten Schaden um 6% und verringert die Zauberzeit um 10%.<br/><br/>Seelenfeuer - Erhöht den verursachten Schaden um 6% und erhöht die kritische Trefferchance um 5%."
                            },
                            {
                                "description": "Erhöht die Effektdauer Eures Zaubers 'Feuerbrand' um 6 Sek., zudem habt Ihr eine Chance von 8%, den Effekt 'Geschmolzener Kern' zu gewinnen, wenn Eurer Zauber 'Verderbnis' Schaden verursacht. Der Effekt 'Geschmolzener Kern' verstärkt die Wirkung der 3 innerhalb der nächsten 15 Sek. gewirkten Zauber 'Verbrennen' oder 'Seelenfeuer'.<br/><br/>Verbrennen - Erhöht den verursachten Schaden um 12% und verringert die Zauberzeit um 20%.<br/><br/>Seelenfeuer - Erhöht den verursachten Schaden um 12% und erhöht die kritische Trefferchance um 10%."
                            },
                            {
                                "description": "Erhöht die Effektdauer Eures Zaubers 'Feuerbrand' um 9 Sek., zudem habt Ihr eine Chance von 12%, den Effekt 'Geschmolzener Kern' zu gewinnen, wenn Eurer Zauber 'Verderbnis' Schaden verursacht. Der Effekt 'Geschmolzener Kern' verstärkt die Wirkung der 3 innerhalb der nächsten 15 Sek. gewirkten Zauber 'Verbrennen' oder 'Seelenfeuer'.<br/><br/>Verbrennen - Erhöht den verursachten Schaden um 18% und verringert die Zauberzeit um 30%.<br/><br/>Seelenfeuer - Erhöht den verursachten Schaden um 18% und erhöht die kritische Trefferchance um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1680,
                        "name": "Dämonische Abhärtung",
                        "icon": "spell_shadow_demonicfortitude",
                        "x": 0,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verringert die Chance, dass Ihr einen kritischen Treffer durch Nahkampfangriffe oder Zauber erleidet, um 1%. Verringert jeglichen von Eurem beschworenen Dämon erlittenen Schaden um 5%."
                            },
                            {
                                "description": "Verringert die Chance, dass Ihr einen kritischen Treffer durch Nahkampfangriffe oder Zauber erleidet, um 2%. Verringert jeglichen von Eurem beschworenen Dämon erlittenen Schaden um 10%."
                            },
                            {
                                "description": "Verringert die Chance, dass Ihr einen kritischen Treffer durch Nahkampfangriffe oder Zauber erleidet, um 3%. Verringert jeglichen von Eurem beschworenen Dämon erlittenen Schaden um 15%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1880,
                        "name": "Dämonische Energie",
                        "icon": "ability_warlock_demonicempowerment",
                        "x": 1,
                        "y": 6,
                        "req": 1244,
                        "ranks": [
                            {
                                "castTime": "Spontanzauber",
                                "cooldown": "1 Min. Abklingzeit",
                                "range": "100 Meter Reichweite",
                                "description": "Erfüllt den beschworenen Dämon des Hexenmeisters mit Macht.<br/><br/>Wichtel - Erhöht die kritische Zaubertrefferchance des Wichtels 30 Sek. lang um 20%.<br/><br/>Leerwandler - Erhöht die Gesundheit des Leerwandlers um 20% und 20 Sek. lang die von seinen Zaubern und Angriffen verursachte Bedrohung um 20%.<br/><br/>Sukkubus - Verschwindet sofort in eine verbesserte Unsichtbarkeit. Das Verschwinden entfernt alle Betäubungen, Verlangsamungen und bewegungseinschränkenden Effekte von dem Sukkubus.<br/><br/>Teufelsjäger - Bannt alle auf den Teufelsjäger wirkenden magischen Effekte.<br/><br/>Teufelswache - Erhöht das Angriffstempo Eurer Teufelswache um 20%, entfernt alle Betäubungen, Verlangsamungen und bewegungseinschränkenden Effekte und macht die Teufelswache gegen sie immun. Hält 15 Sek. lang an.",
                                "cost": "6% des Grundmanas"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1263,
                        "name": "Dämonisches Wissen",
                        "icon": "spell_shadow_improvedvampiricembrace",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Zauberschaden um einen Betrag, der 4% der Summe von Ausdauer und Intelligenz Eures aktiven Dämons entspricht."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um einen Betrag, der 8% der Summe von Ausdauer und Intelligenz Eures aktiven Dämons entspricht."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um einen Betrag, der 12% der Summe von Ausdauer und Intelligenz Eures aktiven Dämons entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1673,
                        "name": "Dämonische Taktiken",
                        "icon": "spell_shadow_demonictactics",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Nahkampf- und Zaubertrefferchance von Euch und Eurem beschworenen Dämon um 2%."
                            },
                            {
                                "description": "Erhöht die kritische Nahkampf- und Zaubertrefferchance von Euch und Eurem beschworenen Dämon um 4%."
                            },
                            {
                                "description": "Erhöht die kritische Nahkampf- und Zaubertrefferchance von Euch und Eurem beschworenen Dämon um 6%."
                            },
                            {
                                "description": "Erhöht die kritische Nahkampf- und Zaubertrefferchance von Euch und Eurem beschworenen Dämon um 8%."
                            },
                            {
                                "description": "Erhöht die kritische Nahkampf- und Zaubertrefferchance von Euch und Eurem beschworenen Dämon um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2261,
                        "name": "Dezimierung",
                        "icon": "spell_fire_fireball02",
                        "x": 2,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Wenn Ihr die Zauber 'Schattenblitz', 'Verbrennen' oder 'Seelenfeuer' auf ein Ziel wirkt, das weniger als 35% Gesundheit besitzt, wird die Zauberzeit Eurer nächsten Wirkens von 'Seelenfeuer' 10 Sek. lang um 20% verringert. Wird 'Seelenfeuer' gewirkt', während Ihr unter dem Effekt von 'Dezimierung' steht, kostet der Zauber keinen Seelensplitter."
                            },
                            {
                                "description": "Wenn Ihr die Zauber 'Schattenblitz', 'Verbrennen' oder 'Seelenfeuer' auf ein Ziel wirkt, das weniger als 35% Gesundheit besitzt, wird die Zauberzeit Eurer nächsten Wirkens von 'Seelenfeuer' 10 Sek. lang um 40% verringert. Wird 'Seelenfeuer' gewirkt', während Ihr unter dem Effekt von 'Dezimierung' steht, kostet der Zauber keinen Seelensplitter."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1882,
                        "name": "Verbesserte dämonische Taktiken",
                        "icon": "ability_warlock_improveddemonictactics",
                        "x": 0,
                        "y": 8,
                        "req": 1673,
                        "ranks": [
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer beschworenen Dämonen um einen Wert, der 10% Eurer kritischen Trefferchance entspricht."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer beschworenen Dämonen um einen Wert, der 20% Eurer kritischen Trefferchance entspricht."
                            },
                            {
                                "description": "Erhöht die kritische Trefferchance Eurer beschworenen Dämonen um einen Wert, der 30% Eurer kritischen Trefferchance entspricht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1672,
                        "name": "Teufelswache beschwören",
                        "icon": "spell_shadow_summonfelguard",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "castTime": "10 Sek. Zauberzeit",
                                "cost": "80% des Grundmanas",
                                "description": "Beschwört eine Teufelswache, die unter dem Befehl des Hexenmeisters steht."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1884,
                        "name": "Nemesis",
                        "icon": "spell_shadow_demonicempathy",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit der Zauber 'Dämonische Energie', 'Metamorphose' und 'Teufelsbeherrschung' um 10%."
                            },
                            {
                                "description": "Verringert die Abklingzeit der Zauber 'Dämonische Energie', 'Metamorphose' und 'Teufelsbeherrschung' um 20%."
                            },
                            {
                                "description": "Verringert die Abklingzeit der Zauber 'Dämonische Energie', 'Metamorphose' und 'Teufelsbeherrschung' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1885,
                        "name": "Dämonischer Pakt",
                        "icon": "spell_shadow_demonicpact",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht Euren Zauberschaden um 2% und die kritischen Treffer Eures Begleiters belegen alle Gruppen- oder Schlachtzugsmitglieder mit dem Effekt 'Dämonischer Pakt'. Der dämonische Pakt erhöht 45 Sek. lang die Zaubermacht um 2% Eures Zauberschadens. Dieser Effekt hat eine Abklingzeit von 20 Sek. Versklavte Dämonen können diesen Effekt nicht auslösen."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um 4% und die kritischen Treffer Eures Begleiters belegen alle Gruppen- oder Schlachtzugsmitglieder mit dem Effekt 'Dämonischer Pakt'. Der dämonische Pakt erhöht 45 Sek. lang die Zaubermacht um 4% Eures Zauberschadens. Dieser Effekt hat eine Abklingzeit von 20 Sek. Versklavte Dämonen können diesen Effekt nicht auslösen."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um 6% und die kritischen Treffer Eures Begleiters belegen alle Gruppen- oder Schlachtzugsmitglieder mit dem Effekt 'Dämonischer Pakt'. Der dämonische Pakt erhöht 45 Sek. lang die Zaubermacht um 6% Eures Zauberschadens. Dieser Effekt hat eine Abklingzeit von 20 Sek. Versklavte Dämonen können diesen Effekt nicht auslösen."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um 8% und die kritischen Treffer Eures Begleiters belegen alle Gruppen- oder Schlachtzugsmitglieder mit dem Effekt 'Dämonischer Pakt'. Der dämonische Pakt erhöht 45 Sek. lang die Zaubermacht um 8% Eures Zauberschadens. Dieser Effekt hat eine Abklingzeit von 20 Sek. Versklavte Dämonen können diesen Effekt nicht auslösen."
                            },
                            {
                                "description": "Erhöht Euren Zauberschaden um 10% und die kritischen Treffer Eures Begleiters belegen alle Gruppen- oder Schlachtzugsmitglieder mit dem Effekt 'Dämonischer Pakt'. Der dämonische Pakt erhöht 45 Sek. lang die Zaubermacht um 10% Eures Zauberschadens. Dieser Effekt hat eine Abklingzeit von 20 Sek. Versklavte Dämonen können diesen Effekt nicht auslösen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1886,
                        "name": "Metamorphose",
                        "icon": "spell_shadow_demonform",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Ihr verwandelt Euch 30 Sek. lang in einen Dämon. Diese Gestalt erhöht Eure Rüstung um 600%, Euren Schaden um 20% und die Chance, einen kritischen Nahkampftreffer zu erleiden, wird um 6% verringert. Die Dauer auf Euch wirkender Betäubungs- und Verlangsamungseffekte wird um 50% verringert. Zusätzlich zu Euren Fähigkeiten gewinnt Ihr einige einzigartige Dämonenfähigkeiten. 3 Minuten Abklingzeit."
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 1
            },
            {
                "name": "Zerstörung",
                "icon": "spell_shadow_rainoffire",
                "backgroundFile": "WarlockDestruction",
                "overlayColor": "#ff7f00",
                "description": "Ruft dämonisches Feuer hernieder, um Feinde zu verbrennen und zu zerstören.",
                "treeNo": 2,
                "roles": {
                    "tank": false,
                    "healer": false,
                    "dps": true
                },
                "primarySpells": [],
                "masteries": [],
                "talents": [
                    {
                        "id": 944,
                        "name": "Verbesserter Schattenblitz",
                        "icon": "spell_shadow_shadowbolt",
                        "x": 1,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Schattenblitz' um 2%. Zudem hat Euer Zauber 'Schattenblitz' eine Chance von 20%, das Ziel verwundbar gegen Zauberschaden werden zu lassen, wodurch die kritische Zaubertrefferchance gegen dieses Ziel um 5% erhöht wird. Der Effekt hält bis zu 30 Sek. lang an."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Schattenblitz' um 4%. Zudem hat Euer Zauber 'Schattenblitz' eine Chance von 40%, das Ziel verwundbar gegen Zauberschaden werden zu lassen, wodurch die kritische Zaubertrefferchance gegen dieses Ziel um 5% erhöht wird. Der Effekt hält bis zu 30 Sek. lang an."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Schattenblitz' um 6%. Zudem hat Euer Zauber 'Schattenblitz' eine Chance von 60%, das Ziel verwundbar gegen Zauberschaden werden zu lassen, wodurch die kritische Zaubertrefferchance gegen dieses Ziel um 5% erhöht wird. Der Effekt hält bis zu 30 Sek. lang an."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Schattenblitz' um 8%. Zudem hat Euer Zauber 'Schattenblitz' eine Chance von 80%, das Ziel verwundbar gegen Zauberschaden werden zu lassen, wodurch die kritische Zaubertrefferchance gegen dieses Ziel um 5% erhöht wird. Der Effekt hält bis zu 30 Sek. lang an."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Schattenblitz' um 10%. Zudem hat Euer Zauber 'Schattenblitz' eine Chance von 100%, das Ziel verwundbar gegen Zauberschaden werden zu lassen, wodurch die kritische Zaubertrefferchance gegen dieses Ziel um 5% erhöht wird. Der Effekt hält bis zu 30 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 943,
                        "name": "Dunkle Macht",
                        "icon": "spell_shadow_deathpact",
                        "x": 2,
                        "y": 0,
                        "ranks": [
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um 0.1 Sek. und die Zauberzeit Eures Zaubers 'Seelenfeuer' um 0.4 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um 0.2 Sek. und die Zauberzeit Eures Zaubers 'Seelenfeuer' um 0.8 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um 0.3 Sek. und die Zauberzeit Eures Zaubers 'Seelenfeuer' um 1.2 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um 0.4 Sek. und die Zauberzeit Eures Zaubers 'Seelenfeuer' um 1.6 Sek."
                            },
                            {
                                "description": "Verringert die Zauberzeit Eurer Zauber 'Schattenblitz', 'Chaosblitz' und 'Feuerbrand' um 0.5 Sek. und die Zauberzeit Eures Zaubers 'Seelenfeuer' um 2 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 982,
                        "name": "Nachwirkung",
                        "icon": "spell_fire_fire",
                        "x": 0,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Erhöht den regelmäßig verursachten Schaden Eures Zaubers 'Feuerbrand' um 3% und gewährt Eurem Zauber 'Feuersbrunst' eine Chance von 50%, das Ziel 5 Sek. lang benommen zu machen."
                            },
                            {
                                "description": "Erhöht den regelmäßig verursachten Schaden Eures Zaubers 'Feuerbrand' um 6% und gewährt Eurem Zauber 'Feuersbrunst' eine Chance von 100%, das Ziel 5 Sek. lang benommen zu machen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1887,
                        "name": "Geschmolzene Haut",
                        "icon": "ability_mage_moltenarmor",
                        "x": 1,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 2%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 4%."
                            },
                            {
                                "description": "Verringert jeglichen erlittenen Schaden um 6%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 941,
                        "name": "Katastrophe",
                        "icon": "spell_fire_windsofwoe",
                        "x": 2,
                        "y": 1,
                        "ranks": [
                            {
                                "description": "Verringert die Manakosten Eurer Zerstörungszauber um 4%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zerstörungszauber um 7%."
                            },
                            {
                                "description": "Verringert die Manakosten Eurer Zerstörungszauber um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 983,
                        "name": "Dämonische Macht",
                        "icon": "spell_fire_firebolt",
                        "x": 0,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Verringert die Abklingzeit des Zaubers 'Schmerzenspeitsche' Eures Sukkubus um 3 Sek. und verringert die Zauberzeit des Zaubers 'Feuerblitz' Eures Wichtels um 0.25 Sek."
                            },
                            {
                                "description": "Verringert die Abklingzeit des Zaubers 'Schmerzenspeitsche' Eures Sukkubus um 6 Sek. und verringert die Zauberzeit des Zaubers 'Feuerblitz' Eures Wichtels um 0.50 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 963,
                        "name": "Schattenbrand",
                        "icon": "spell_shadow_scourgebuild",
                        "x": 1,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Überzieht das Ziel sofort mit 87 bis 99 Schattenschaden. Wenn das Ziel innerhalb von 5 Sek. nach 'Schattenbrand' stirbt und Erfahrung oder Ehrenpunkte abgibt, erhält der Zaubernde einen Seelensplitter.",
                                "cost": "20% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "15 Sek. Abklingzeit",
                                "range": "20 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 967,
                        "name": "Verderben",
                        "icon": "spell_shadow_shadowwordpain",
                        "x": 2,
                        "y": 2,
                        "ranks": [
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zerstörungszauber und den des Zaubers 'Feuerblitz' Eures Wichtels um 20%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zerstörungszauber und den des Zaubers 'Feuerblitz' Eures Wichtels um 40%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zerstörungszauber und den des Zaubers 'Feuerblitz' Eures Wichtels um 60%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zerstörungszauber und den des Zaubers 'Feuerblitz' Eures Wichtels um 80%."
                            },
                            {
                                "description": "Erhöht den kritischen Schadensbonus Eurer Zerstörungszauber und den des Zaubers 'Feuerblitz' Eures Wichtels um 100%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 985,
                        "name": "Intensität",
                        "icon": "spell_fire_lavaspawn",
                        "x": 0,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken oder Zauberzeitverringerung beim Kanalisieren von Zerstörungszaubern um 35%."
                            },
                            {
                                "description": "Verringert die Dauer der durch erlittenen Schaden verursachten Zauberzeiterhöhung beim Wirken oder Zauberzeitverringerung beim Kanalisieren von Zerstörungszaubern um 70%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 964,
                        "name": "Zerstörerische Reichweite",
                        "icon": "spell_shadow_corpseexplode",
                        "x": 1,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Reichweite Eurer Zerstörungszauber um 10% und verringert die durch diese Zauber verursachte Bedrohung um 10%."
                            },
                            {
                                "description": "Erhöht die Reichweite Eurer Zerstörungszauber um 20% und verringert die durch diese Zauber verursachte Bedrohung um 20%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 965,
                        "name": "Verbesserter sengender Schmerz",
                        "icon": "spell_fire_soulburn",
                        "x": 3,
                        "y": 3,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eures Zaubers 'Sengender Schmerz' um 4%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eures Zaubers 'Sengender Schmerz' um 7%."
                            },
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eures Zaubers 'Sengender Schmerz' um 10%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1817,
                        "name": "Heimzahlen",
                        "icon": "spell_fire_playingwithfire",
                        "x": 0,
                        "y": 4,
                        "req": 985,
                        "ranks": [
                            {
                                "description": "Erhöht Eure kritische Zaubertrefferchance um zusätzliche 1% und gewährt Euch eine Chance von 8%, nach Erleiden eines körperlichen Treffers die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' oder 'Verbrennen' um 100% zu verringern. Dieser Effekt hält 8 Sek. lang an und kann innerhalb von 8 Sek. nur einmal auftreten."
                            },
                            {
                                "description": "Erhöht Eure kritische Zaubertrefferchance um zusätzliche 2% und gewährt Euch eine Chance von 16%, nach Erleiden eines körperlichen Treffers die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' oder 'Verbrennen' um 100% zu verringern. Dieser Effekt hält 8 Sek. lang an und kann innerhalb von 8 Sek. nur einmal auftreten."
                            },
                            {
                                "description": "Erhöht Eure kritische Zaubertrefferchance um zusätzliche 3% und gewährt Euch eine Chance von 25%, nach Erleiden eines körperlichen Treffers die Zauberzeit Eures nächsten Zaubers 'Schattenblitz' oder 'Verbrennen' um 100% zu verringern. Dieser Effekt hält 8 Sek. lang an und kann innerhalb von 8 Sek. nur einmal auftreten."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 961,
                        "name": "Verbesserter Feuerbrand",
                        "icon": "spell_fire_immolation",
                        "x": 1,
                        "y": 4,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Feuerbrand' um 10%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Feuerbrand' um 20%."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eures Zaubers 'Feuerbrand' um 30%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 981,
                        "name": "Verwüstung",
                        "icon": "spell_fire_flameshock",
                        "x": 2,
                        "y": 4,
                        "req": 967,
                        "ranks": [
                            {
                                "description": "Erhöht die Chance für einen kritischen Treffer Eurer Zerstörungszauber um 5%."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1679,
                        "name": "Netherschutz",
                        "icon": "spell_shadow_netherprotection",
                        "x": 0,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Nach einem erlittenen Zaubertreffer, besteht eine Chance von 10%, dass Ihr daraufhin durch 'Netherschutz' geschützt werdet, der 8 Sek. lang jeglichen durch diese Zauberart erlittenen Schaden um 30% verringert."
                            },
                            {
                                "description": "Nach einem erlittenen Zaubertreffer, besteht eine Chance von 20%, dass Ihr daraufhin durch 'Netherschutz' geschützt werdet, der 8 Sek. lang jeglichen durch diese Zauberart erlittenen Schaden um 30% verringert."
                            },
                            {
                                "description": "Nach einem erlittenen Zaubertreffer, besteht eine Chance von 30%, dass Ihr daraufhin durch 'Netherschutz' geschützt werdet, der 8 Sek. lang jeglichen durch diese Zauberart erlittenen Schaden um 30% verringert."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 966,
                        "name": "Glutsturm",
                        "icon": "spell_fire_selfdestruct",
                        "x": 2,
                        "y": 5,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Feuerzauber um 3% und verringert die Zauberzeit Eures Zaubers 'Verbrennen' um 0.05 Sek."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Feuerzauber um 6% und verringert die Zauberzeit Eures Zaubers 'Verbrennen' um 0.10 Sek."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Feuerzauber um 9% und verringert die Zauberzeit Eures Zaubers 'Verbrennen' um 0.15 Sek."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Feuerzauber um 12% und verringert die Zauberzeit Eures Zaubers 'Verbrennen' um 0.20 Sek."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Feuerzauber um 15% und verringert die Zauberzeit Eures Zaubers 'Verbrennen' um 0.25 Sek."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 968,
                        "name": "Feuersbrunst",
                        "icon": "spell_fire_fireball",
                        "x": 1,
                        "y": 6,
                        "req": 961,
                        "ranks": [
                            {
                                "description": "Zehrt einen auf das Ziel wirkenden Effekt der Zauber 'Feuerbrand' oder 'Schattenflamme' auf, um sofort Schaden in Höhe von 60% Eurer Zauber 'Feuerbrand' oder 'Schattenflamme' zuzufügen. Zudem wird im Verlauf von 6 Sek. zusätzlich 40% Schaden verursacht.",
                                "cost": "16% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "10 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1678,
                        "name": "Seele entziehen",
                        "icon": "spell_shadow_soulleech_3",
                        "x": 2,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Verleiht Euren Zaubern 'Schattenblitz', 'Schattenbrand', 'Chaosblitz', 'Seelenfeuer', 'Verbrennen', 'Sengender Schmerz' und 'Feuersbrunst' eine Chance von 10%, Eure Gesundheit in Höhe von 20% des verursachten Schadens wiederherzustellen."
                            },
                            {
                                "description": "Verleiht Euren Zaubern 'Schattenblitz', 'Schattenbrand', 'Chaosblitz', 'Seelenfeuer', 'Verbrennen', 'Sengender Schmerz' und 'Feuersbrunst' eine Chance von 20%, Eure Gesundheit in Höhe von 20% des verursachten Schadens wiederherzustellen."
                            },
                            {
                                "description": "Verleiht Euren Zaubern 'Schattenblitz', 'Schattenbrand', 'Chaosblitz', 'Seelenfeuer', 'Verbrennen', 'Sengender Schmerz' und 'Feuersbrunst' eine Chance von 30%, Eure Gesundheit in Höhe von 20% des verursachten Schadens wiederherzustellen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 986,
                        "name": "Feuerschwall",
                        "icon": "spell_fire_volcano",
                        "x": 3,
                        "y": 6,
                        "ranks": [
                            {
                                "description": "Erzielt Ihr mit Euren Zaubern 'Sengender Schmerz' oder 'Feuersbrunst' einen kritischen Treffer, wird Euer Feuer- und Schattenzauberschaden 10 Sek. lang um 2% erhöht."
                            },
                            {
                                "description": "Erzielt Ihr mit Euren Zaubern 'Sengender Schmerz' oder 'Feuersbrunst' einen kritischen Treffer, wird Euer Feuer- und Schattenzauberschaden 10 Sek. lang um 4% erhöht."
                            },
                            {
                                "description": "Erzielt Ihr mit Euren Zaubern 'Sengender Schmerz' oder 'Feuersbrunst' einen kritischen Treffer, wird Euer Feuer- und Schattenzauberschaden 10 Sek. lang um 6% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1677,
                        "name": "Schatten und Flamme",
                        "icon": "spell_shadow_shadowandflame",
                        "x": 1,
                        "y": 7,
                        "ranks": [
                            {
                                "description": "Eure Zauber 'Schattenblitz', 'Schattenbrand', 'Chaosblitz' und 'Verbrennen' erhalten zusätzliche 4% Bonus durch Effekte, die Zauberschaden beeinflussen."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz', 'Schattenbrand', 'Chaosblitz' und 'Verbrennen' erhalten zusätzliche 8% Bonus durch Effekte, die Zauberschaden beeinflussen."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz', 'Schattenbrand', 'Chaosblitz' und 'Verbrennen' erhalten zusätzliche 12% Bonus durch Effekte, die Zauberschaden beeinflussen."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz', 'Schattenbrand', 'Chaosblitz' und 'Verbrennen' erhalten zusätzliche 16% Bonus durch Effekte, die Zauberschaden beeinflussen."
                            },
                            {
                                "description": "Eure Zauber 'Schattenblitz', 'Schattenbrand', 'Chaosblitz' und 'Verbrennen' erhalten zusätzliche 20% Bonus durch Effekte, die Zauberschaden beeinflussen."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1889,
                        "name": "Verbessertes Seele entziehen",
                        "icon": "ability_warlock_improvedsoulleech",
                        "x": 2,
                        "y": 7,
                        "req": 1678,
                        "ranks": [
                            {
                                "description": "Euer Zauber 'Seele entziehen' stellt sowohl Euer Mana, als auch das Mana Eures Dämons in Höhe von 1% Eures maximalen Manas wieder her. Zudem besteht eine Chance von 50%, dass bis zu 10 Gruppen- oder Schlachtzugsmitglieder alle 5 Sek. 1% ihres maximalen Manas regenerieren. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Euer Zauber 'Seele entziehen' stellt sowohl Euer Mana, als auch das Mana Eures Dämons in Höhe von 2% Eures maximalen Manas wieder her. Zudem besteht eine Chance von 100%, dass bis zu 10 Gruppen- oder Schlachtzugsmitglieder alle 5 Sek. 1% ihres maximalen Manas regenerieren. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1888,
                        "name": "Pyrolyse",
                        "icon": "ability_warlock_backdraft",
                        "x": 0,
                        "y": 8,
                        "req": 968,
                        "ranks": [
                            {
                                "description": "Wirkt Ihr 'Feuersbrunst', werden die Zauberzeit und die globale Abklingzeit Eurer nächsten 3 Zerstörungszauber um 10% verringert. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Wirkt Ihr 'Feuersbrunst', werden die Zauberzeit und die globale Abklingzeit Eurer nächsten 3 Zerstörungszauber um 20% verringert. Hält 15 Sek. lang an."
                            },
                            {
                                "description": "Wirkt Ihr 'Feuersbrunst', werden die Zauberzeit und die globale Abklingzeit Eurer nächsten 3 Zerstörungszauber um 30% verringert. Hält 15 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1676,
                        "name": "Schattenfuror",
                        "icon": "spell_shadow_shadowfury",
                        "x": 1,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Der Schattenfuror wird entfesselt, verursacht 343 bis 407 Schattenschaden und betäubt 3 Sek. lang alle Gegner im Umkreis von 8 Metern.",
                                "cost": "27% des Grundmanas",
                                "castTime": "Spontanzauber",
                                "cooldown": "20 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 2045,
                        "name": "Machterfüllter Wichtel",
                        "icon": "ability_warlock_empoweredimp",
                        "x": 2,
                        "y": 8,
                        "ranks": [
                            {
                                "description": "Erhöht den von Eurem Wichtel verursachten Schaden um 10%, und von Eurem Wichtel erzielte kritische Treffer haben eine Chance von 33%, die kritische Trefferchance Eures nächsten Zaubers um 100% zu erhöhen. Der Effekt hält 8 Sek. lang an."
                            },
                            {
                                "description": "Erhöht den von Eurem Wichtel verursachten Schaden um 20%, und von Eurem Wichtel erzielte kritische Treffer haben eine Chance von 66%, die kritische Trefferchance Eures nächsten Zaubers um 100% zu erhöhen. Der Effekt hält 8 Sek. lang an."
                            },
                            {
                                "description": "Erhöht den von Eurem Wichtel verursachten Schaden um 30%, und von Eurem Wichtel erzielte kritische Treffer haben eine Chance von 100%, die kritische Trefferchance Eures nächsten Zaubers um 100% zu erhöhen. Der Effekt hält 8 Sek. lang an."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1890,
                        "name": "Feuer und Schwefel",
                        "icon": "ability_warlock_fireandbrimstone",
                        "x": 1,
                        "y": 9,
                        "ranks": [
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 2%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 5% erhöht."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 4%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 10% erhöht."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 6%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 15% erhöht."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 8%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 20% erhöht."
                            },
                            {
                                "description": "Erhöht den verursachten Schaden Eurer Zauber 'Verbrennen' und 'Chaosblitz' an Zielen, die vom Effekt Eures Zaubers 'Feuerbrand' betroffen sind, um 10%. Zudem wird die kritische Trefferchance Eures Zaubers 'Feuersbrunst' um 25% erhöht."
                            }
                        ],
                        "keyAbility": false,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    },
                    {
                        "id": 1891,
                        "name": "Chaosblitz",
                        "icon": "ability_warlock_chaosbolt",
                        "x": 1,
                        "y": 10,
                        "ranks": [
                            {
                                "description": "Schleudert einen Blitz chaotischen Feuers auf den Feind und verursacht 837 bis 1061 Feuerschaden. Dem Chaosblitz kann nicht widerstanden werden und er durchschlägt alle Absorptionseffekte.",
                                "cost": "7% des Grundmanas",
                                "castTime": "2,5 Sek. Zauberzeit",
                                "cooldown": "12 Sek. Abklingzeit",
                                "range": "30 Meter Reichweite"
                            }
                        ],
                        "keyAbility": true,
                        "categoryMask0": 0,
                        "categoryMask1": 0
                    }
                ],
                "index": 2
            }
        ]
    },
    "glyphs": [{
    "272": {
        "name": "Glyphe 'Verbrennen'",
        "id": "272",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42453",
        "spellKey": "56242",
        "spellId": "56242",
        "prettyName": "",
        "typeOrder": 2
    },
    "273": {
        "name": "Glyphe 'Feuersbrunst'",
        "id": "273",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Feuersbrunst\\' um 2 Sek.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42454",
        "spellKey": "56235",
        "spellId": "56235",
        "prettyName": "",
        "typeOrder": 2
    },
    "274": {
        "name": "Glyphe 'Verderbnis'",
        "id": "274",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42455",
        "spellKey": "56218",
        "spellId": "56218",
        "prettyName": "",
        "typeOrder": 2
    },
    "275": {
        "name": "Glyphe 'Fluch der Pein'",
        "id": "275",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42456",
        "spellKey": "56241",
        "spellId": "56241",
        "prettyName": "",
        "typeOrder": 2
    },
    "276": {
        "name": "Glyphe 'Todesmantel'",
        "id": "276",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42457",
        "spellKey": "56232",
        "spellId": "56232",
        "prettyName": "",
        "typeOrder": 2
    },
    "277": {
        "name": "Glyphe 'Furcht'",
        "id": "277",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42458",
        "spellKey": "56244",
        "spellId": "56244",
        "prettyName": "",
        "typeOrder": 2
    },
    "278": {
        "name": "Glyphe 'Teufelswache'",
        "id": "278",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42459",
        "spellKey": "56246",
        "spellId": "56246",
        "prettyName": "",
        "typeOrder": 2
    },
    "279": {
        "name": "Glyphe 'Teufelsjäger'",
        "id": "279",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42460",
        "spellKey": "56249",
        "spellId": "56249",
        "prettyName": "",
        "typeOrder": 2
    },
    "280": {
        "name": "Glyphe 'Lebenslinie'",
        "id": "280",
        "type": 0,
        "description": "Verringert die durch erlittenen Schaden verursachte Zauberzeitverringerung beim Kanalisieren Eures Zaubers \\'Lebenslinie\\' um 100%.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42461",
        "spellKey": "56238",
        "spellId": "56238",
        "prettyName": "",
        "typeOrder": 2
    },
    "281": {
        "name": "Glyphe 'Gesundheitsstein'",
        "id": "281",
        "type": 0,
        "description": "Ihr gewinnt durch das Nutzen von Gesundheitssteinen 30% mehr Gesundheit.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42462",
        "spellKey": "56224",
        "spellId": "56224",
        "prettyName": "",
        "typeOrder": 2
    },
    "282": {
        "name": "Glyphe 'Schreckensgeheul'",
        "id": "282",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Schreckensgeheul\\' um 8 Sek.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42463",
        "spellKey": "56217",
        "spellId": "56217",
        "prettyName": "",
        "typeOrder": 2
    },
    "283": {
        "name": "Glyphe 'Feuerbrand'",
        "id": "283",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42464",
        "spellKey": "56228",
        "spellId": "56228",
        "prettyName": "",
        "typeOrder": 2
    },
    "284": {
        "name": "Glyphe 'Wichtel'",
        "id": "284",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42465",
        "spellKey": "56248",
        "spellId": "56248",
        "prettyName": "",
        "typeOrder": 2
    },
    "285": {
        "name": "Glyphe 'Sengender Schmerz'",
        "id": "285",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42466",
        "spellKey": "56226",
        "spellId": "56226",
        "prettyName": "",
        "typeOrder": 2
    },
    "286": {
        "name": "Glyphe 'Schattenblitz'",
        "id": "286",
        "type": 0,
        "description": "Verringert die Manakosten Eures Zaubers \\'Schattenblitz\\' um 15%.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42467",
        "spellKey": "56240",
        "spellId": "56240",
        "prettyName": "",
        "typeOrder": 2
    },
    "287": {
        "name": "Glyphe 'Schattenbrand'",
        "id": "287",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42468",
        "spellKey": "56229",
        "spellId": "56229",
        "prettyName": "",
        "typeOrder": 2
    },
    "288": {
        "name": "Glyphe 'Lebensentzug'",
        "id": "288",
        "type": 0,
        "description": "Increases the healing you receive from your Siphon Life talent by 25%.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42469",
        "spellKey": "56216",
        "spellId": "56216",
        "prettyName": "",
        "typeOrder": 2
    },
    "289": {
        "name": "Glyphe 'Seelenstein'",
        "id": "289",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42470",
        "spellKey": "56231",
        "spellId": "56231",
        "prettyName": "",
        "typeOrder": 2
    },
    "290": {
        "name": "Glyphe 'Sukkubus'",
        "id": "290",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42471",
        "spellKey": "56250",
        "spellId": "56250",
        "prettyName": "",
        "typeOrder": 2
    },
    "291": {
        "name": "Glyphe 'Instabiles Gebrechen'",
        "id": "291",
        "type": 0,
        "description": "Verringert die Zauberzeit Eures Zaubers \\'Instabiles Gebrechen\\' um 0.2 Sek.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42472",
        "spellKey": "56233",
        "spellId": "56233",
        "prettyName": "",
        "typeOrder": 2
    },
    "292": {
        "name": "Glyphe 'Leerwandler'",
        "id": "292",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "42473",
        "spellKey": "56247",
        "spellId": "56247",
        "prettyName": "",
        "typeOrder": 2
    },
    "477": {
        "name": "Glyphe 'Unendlicher Atem'",
        "id": "477",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarlock",
        "itemId": "43389",
        "spellKey": "58079",
        "spellId": "58079",
        "prettyName": "",
        "typeOrder": 2
    },
    "478": {
        "name": "Glyphe 'Seelendieb'",
        "id": "478",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarlock",
        "itemId": "43390",
        "spellKey": "58070",
        "spellId": "58070",
        "prettyName": "",
        "typeOrder": 2
    },
    "479": {
        "name": "Glyphe 'Auge von Kilrogg'",
        "id": "479",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarlock",
        "itemId": "43391",
        "spellKey": "58081",
        "spellId": "58081",
        "prettyName": "",
        "typeOrder": 2
    },
    "480": {
        "name": "Glyphe 'Fluch der Erschöpfung'",
        "id": "480",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarlock",
        "itemId": "43392",
        "spellKey": "58080",
        "spellId": "58080",
        "prettyName": "",
        "typeOrder": 2
    },
    "481": {
        "name": "Glyphe 'Dämonensklave'",
        "id": "481",
        "type": 1,
        "description": null,
        "icon": "inv_glyph_minorwarlock",
        "itemId": "43393",
        "spellKey": "58107",
        "spellId": "58107",
        "prettyName": "",
        "typeOrder": 2
    },
    "482": {
        "name": "Glyphe 'Ritual der Seelen'",
        "id": "482",
        "type": 1,
        "description": "Verringert die Manakosten Eures Zaubers \\'Ritual der Seelen\\' um 70%.",
        "icon": "inv_glyph_minorwarlock",
        "itemId": "43394",
        "spellKey": "58094",
        "spellId": "58094",
        "prettyName": "",
        "typeOrder": 2
    },
    "755": {
        "name": "Glyphe 'Heimsuchung'",
        "id": "755",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45779",
        "spellKey": "63302",
        "spellId": "63302",
        "prettyName": "",
        "typeOrder": 2
    },
    "756": {
        "name": "Glyphe 'Metamorphose'",
        "id": "756",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45780",
        "spellKey": "63303",
        "spellId": "63303",
        "prettyName": "",
        "typeOrder": 2
    },
    "757": {
        "name": "Glyphe 'Chaosblitz'",
        "id": "757",
        "type": 0,
        "description": "Verringert die Abklingzeit Eures Zaubers \\'Chaosblitz\\' um 2 Sek.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45781",
        "spellKey": "63304",
        "spellId": "63304",
        "prettyName": "",
        "typeOrder": 2
    },
    "758": {
        "name": "Glyphe 'Dämonischer Zirkel'",
        "id": "758",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45782",
        "spellKey": "63309",
        "spellId": "63309",
        "prettyName": "",
        "typeOrder": 2
    },
    "759": {
        "name": "Glyphe 'Schattenflamme'",
        "id": "759",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45783",
        "spellKey": "63310",
        "spellId": "63310",
        "prettyName": "",
        "typeOrder": 2
    },
    "760": {
        "name": "Glyphe 'Aderlass'",
        "id": "760",
        "type": 0,
        "description": "Verringert die globale Abklingzeit Eures Zaubers \\'Aderlass\\' um 0,5 Sek.",
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45785",
        "spellKey": "63320",
        "spellId": "63320",
        "prettyName": "",
        "typeOrder": 2
    },
    "761": {
        "name": "Glyphe 'Seelenverbindung'",
        "id": "761",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "45789",
        "spellKey": "63312",
        "spellId": "63312",
        "prettyName": "",
        "typeOrder": 2
    },
    "911": {
        "name": "Glyph of Quick Decay",
        "id": "911",
        "type": 0,
        "description": null,
        "icon": "inv_glyph_majorwarlock",
        "itemId": "50077",
        "spellKey": "70947",
        "spellId": "70947",
        "prettyName": "",
        "typeOrder": 2
    }
}]
}