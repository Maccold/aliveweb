<?php

$data_races = array(
	RACE_HUMAN => array(
            "label" => array(
    				"de" => "Mensch",
                    "en" => "Human",
                ),

            "modeldata_1" => "human",
            "modeldata_2" => "hu",
        ),

    RACE_ORC => array(
            "label" => array(
                    "de" => "Orc",
                    "en" => "Orc",
                ),
            "modeldata_1" => "orc",
            "modeldata_2" => "or",
        ),

    RACE_DWARF => array(
            "label" => array(
                    "de" => "Zwerg",
                    "en" => "Dwarf",
                ),

            "modeldata_1" => "dwarf",
            "modeldata_2" => "dw",
        ),

    RACE_NIGHTELF => array(
            "label" => array(
                    "de" => "Nachtelf",
                    "en" => "Night Elf",
                ),

            "modeldata_1" => "nightelf",
            "modeldata_2" => "ni"
        ),

    RACE_UNDEAD => array(
            "label" => array(
                    "de" => "Untoter",
                    "en" => "Undead",
                ),

            "modeldata_1" => "scourge",
            "modeldata_2" => "sc"
        ),

    RACE_TAUREN => array(
            "label" => array(
                    "de" => "Tauren",
                    "en" => "Tauren",
                ),

            "modeldata_1" => "tauren",
            "modeldata_2" => "ta"
        ),

    RACE_GNOME => array(
            "label" => array(
                    "de" => "Gnom",
                    "en" => "Gnome",
                ),

            "modeldata_1" => "gnome",
            "modeldata_2" => "gn"
        ),

    RACE_TROLL => array(
            "label" => array(
                    "de" => "Troll",
                    "en" => "Troll",
                ),

            "modeldata_1" => "troll",
            "modeldata_2" => "tr"
        ),

    RACE_BLOODELF => array(
            "label" => array(
                    "de" => "Blutelf",
                    "en" => "Blood Elf",
                ),

            "modeldata_1" => "bloodelf",
            "modeldata_2" => "be",
        ),

    RACE_DRAENEI => array(
            "label" => array(
                    "de" => "Draenei",
                    "en" => "Draenei",
                ),

            "modeldata_1" => "draenei",
            "modeldata_2" => "dr",
        ),
);

