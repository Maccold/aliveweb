<?php


$realm_type_def = array(
    0 => 'Normal',
    1 => 'PVP',
    4 => 'Normal',
    6 => 'RP',
    8 => 'RPPVP',
    16 => 'FFA_PVP'
);

$realm_timezone_def = array(
     0 => 'Unknown',
     1 => 'Entwicklung-Deutsch',
     2 => 'United States',
     3 => 'Oceanic',
     4 => 'Latin America',
     5 => 'Tournament',
     6 => 'Korea',
     7 => 'Tournament',
     8 => 'English',
     9 => 'Deutsch',
    10 => 'French',
    11 => 'Spanish',
    12 => 'Russian',
    13 => 'Tournament',
    14 => 'Taiwan',
    15 => 'Tournament',
    16 => 'China',
    17 => 'CN1',
    18 => 'CN2',
    19 => 'CN3',
    20 => 'CN4',
    21 => 'CN5',
    22 => 'CN6',
    23 => 'CN7',
    24 => 'CN8',
    25 => 'Tournament',
    26 => 'Test Server',
    27 => 'Tournament',
    28 => 'QA Server',
    29 => 'CN9',
);

function escape_string($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }

    return mysql_real_escape_string($string);
}

function GetItemIcon($id, $size = 36){
	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/images/icons/".$size."/".$id.".jpg"))
		return;
	$url = "http://eu.media.blizzard.com/wow/icons/".$size."/".$id.".jpg";
	$contents = file_get_contents($url);
	
	if(strlen($contents) > 0)
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/images/icons/".$size."/".$id.".jpg", $contents);
}

function GetGreyscaleIcon($id, $size){
	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/images/icons/".$size."/".$id.".grey.jpg"))
		return;
	
	$im = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"]."/images/icons/".$size."/".$id.".jpg");

	if($im && imagefilter($im, IMG_FILTER_GRAYSCALE))
	{
		//debug('Image converted to grayscale.');
		imagepng($im, $_SERVER["DOCUMENT_ROOT"]."/images/icons/".$size."/".$id.".grey.jpg");
	}
	else
	{
		debug('Conversion to grayscale failed for id $id.');
	}

	imagedestroy($im);
}

function GetImage($path){
	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/images/".$path))
		return;
	debug("load image $path");
	$url = "http://eu.battle.net/wow/static/images/".$path;
	$contents = file_get_contents($url);
	
	if(strlen($contents) > 0){
		debug("image found $url");
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/images/".$path, $contents);
	}		
}

function LoadBlock($path){
	global $currtmp;
	// Load Block Content
	$block_content = "";
	debug("Load $path");
	$block_file = getLayout($path);
	
	ob_start("block");
		include ( $block_file ) ;
		$block_content = ob_get_contents();
	ob_end_clean();
	
	return $block_content;
}

function FormatMoney($cash){

	$copper = 0;
	$silver = 0;
	$gold = 0;
	$string = "";
	
	if($cash < 100){
		$copper = $cash;
	}
	else{
		$copper = $cash % 100;
		
		$cash = floor($cash / 100);
		
		if($cash < 100){
			$silver = $cash;
		}
		else {
			$silver = $cash % 100;
			$gold = floor($cash / 100);
		}
	}
	if($gold > 0)
		$string .= '<span class="icon-gold">'.$gold.'</span>';
	if($silver > 0)
		$string .= '<span class="icon-silver">'.$silver.'</span>';
	if($copper > 0)
		$string .= '<span class="icon-copper">'.$copper.'</span>';
	
	return $string;
}
function GetItemSlotTextByInvType($invType) {
 	global $lang_strings;
	
	switch($invType){
		case 0: return $lang_strings['armory.item-tooltip.non-equip']; break;
        case 1: return $lang_strings['armory.itemslot.slot.head']; break;
        case 2: return $lang_strings['armory.itemslot.slot.neck']; break;
        case 3: return $lang_strings['armory.itemslot.slot.shoulders']; break;
        case 4: return $lang_strings['armory.itemslot.slot.shirt']; break;
        case 5: return $lang_strings['armory.itemslot.slot.chest']; break;
        case 6: return $lang_strings['armory.itemslot.slot.waist']; break;
        case 7: return $lang_strings['armory.itemslot.slot.legs']; break;
        case 8: return $lang_strings['armory.itemslot.slot.feet']; break;
        case 9: return $lang_strings['armory.itemslot.slot.wrist']; break;
        case 10: return $lang_strings['armory.itemslot.slot.hand']; break;
        case 11: return $lang_strings['armory.itemslot.slot.finger']; break;
        case 12: return $lang_strings['armory.itemslot.slot.trinket']; break;
        case 13: return $lang_strings['armory.item-tooltip.one-hand']; break;
        case 14: return $lang_strings['armory.itemslot.slot.offHand']; break;
        case 15: return $lang_strings['armory.itemslot.slot.ranged']; break;
        case 16: return $lang_strings['armory.itemslot.slot.back']; break;
        case 17: return $lang_strings['armory.item-tooltip.two-hand']; break;
        case 18: return $lang_strings['armory.item-tooltip.bag-type']; break;
        case 19: return $lang_strings['armory.itemslot.slot.tabard']; break;
        case 20: return $lang_strings['armory.itemslot.slot.chest']; break;
        case 21: return $lang_strings['armory.itemslot.slot.mainHand']; break;
        case 22: return $lang_strings['armory.itemslot.slot.offHand']; break;
        case 23: return $lang_strings['armory.item-tooltip.held-off-hand']; break;
        case 24: return $lang_strings['armory.item-tooltip.projectile']; break;
        case 25: return $lang_strings['armory.item-tooltip.thrown']; break;
        case 26: return $lang_strings['armory.itemslot.slot.ranged']; break;
        case 27: return $lang_strings['armory.item-tooltip.quiver-type']; break;
        case 28: return $lang_strings['armory.itemslot.slot.relic']; break;
	}
}
/**
 * Returns max. array value index.
 * @category Utils class
 * @access   public
 * @param    array $arr
 * @return   array
 **/
function GetMaxArray($arr) {
	if(!is_array($arr)) {
		debug('GetMaxArray: arr must be in array');
		return false;
	}
	$keys = array_keys($arr);
	$cnt = count($arr);
	$min = $max = $arr[$keys[0]];
	$index_min=$index_max=0; 
	for($i = 1; $i < $cnt; $i++) {
		if($arr[$keys[$i]]>$max) {
			$index_max = $i;
			$max = $arr[$keys[$i]];
		}
	}
	return $index_max;
}
function getFactionStanding($standing){
	global $lang_strings;
	
	switch($standing){
		case 0: return $lang_strings['armory.item-tooltip.hated']; break;
		case 1: return $lang_strings['armory.item-tooltip.hostile']; break;
		case 2: return $lang_strings['armory.item-tooltip.unfriendly']; break;
		case 3: return $lang_strings['armory.item-tooltip.neutral']; break;
		case 4: return $lang_strings['armory.item-tooltip.friendly']; break;
		case 5: return $lang_strings['armory.item-tooltip.honored']; break;
		case 6: return $lang_strings['armory.item-tooltip.revered']; break;
		case 7: return $lang_strings['armory.item-tooltip.exalted']; break;
	}
}


/**
 * Returns item name according with defined locale (ru_ru, en_gb, etc.)
 * @category Items class
 * @access   public
 * @param    int $itemID
 * @return   string
 **/
function GetItemName($itemID) {
	global $WSDB;
	
	$locale = $WSDB->selectCell("SELECT `name_loc3` FROM `locales_item` WHERE `entry`=?d LIMIT 1", $itemID);
	
	if($locale)
		return $locale;
	
	$itemName = $WSDB->selectCell("SELECT `name` FROM `item_template` WHERE `entry`=?d LIMIT 1", $itemID);
	
	if($itemName) {
		return $itemName;
	}
	return false;
}

function GetItemInfo($itemID){
	global $WSDB, $aDB;
	
	$data = array(
		"name" => "",
		"icon" => "",
	);
	
	$locale = $WSDB->selectCell("SELECT `name_loc3` FROM `locales_item` WHERE `entry`=?d LIMIT 1", $itemID);
	if($locale)
		$data["name"] = $locale;
		
	$itemRow = $WSDB->selectRow("SELECT `name`,`displayid` FROM `item_template` WHERE `entry`=?d LIMIT 1", $itemID);
	if($itemRow){
		$data["icon"] = $aDB->selectCell("SELECT icon FROM armory_icons WHERE displayid = ?", $itemRow["displayid"]);
		if(empty($data["name"]))
			$data["name"] = $itemRow["name"];
	}
	
	return $data;
}


 /**
 * Returns NPC info (infoType)
 * @category Mangos class
 * @access   public
 * @param    int $npc
 * @param    string $infoType
 * @return   mixed
 **/
function GetNpcInfo($npc, $infoType) {
	global $WSDB;
	
	$info = null;
	switch($infoType) {
		case 'maxlevel':
			$info = $WSDB->selectCell("SELECT `maxlevel` FROM `creature_template` WHERE `entry`=?d", $npc);
			break;	
		case 'minlevel':
			$info = $WSDB->selectCell("SELECT `minlevel` FROM `creature_template` WHERE `entry`=?d", $npc);
			break;				
		case 'map':
			$mapID = $WSDB->selectCell("SELECT `map` FROM `creature` WHERE `id`=?d LIMIT 1", $npc);
			if(!$mapID) {
				$killCredit = $WSDB->selectRow("SELECT `KillCredit1`, `KillCredit2` FROM `creature_template` WHERE `entry`=?d", $npc);
				if($killCredit['KillCredit1'] > 0) {
					$kc_entry = $killCredit['KillCredit1'];
				}
				elseif($killCredit['KillCredit2'] > 0) {
					$kc_entry = $killCredit['KillCredit2'];
				}
				else {
					$kc_entry = false;
				}
				$mapID = $WSDB->selectCell("SELECT `map` FROM `creature` WHERE `id`=?d LIMIT 1", $kc_entry);
				if(!$mapID) {
					return false;
				}
			}
			if($info = $aDB->selectCell("SELECT `name_%s` FROM `ARMORYDBPREFIX_instance_template` WHERE `map`=?d", Armory::GetLocale(), $mapID)) {
				return $info;
			}
			else {
				$info = $aDB->selectCell("SELECT `name_%s` FROM `ARMORYDBPREFIX_maps` WHERE `id`=?d", Armory::GetLocale(), $mapID);
			}
			break;
		case 'areaUrl':
			$mapID = $WSDB->selectCell("SELECT `map` FROM `creature` WHERE `id`=?d LIMIT 1", $npc);
			if(!$mapID) {
				$killCredit = $WSDB->selectRow("SELECT `KillCredit1`, `KillCredit2` FROM `creature_template` WHERE `entry`=?d", $npc);
				if($killCredit['KillCredit1'] > 0) {
					$kc_entry = $killCredit['KillCredit1'];
				}
				elseif($killCredit['KillCredit2'] > 0) {
					$kc_entry = $killCredit['KillCredit2'];
				}
				else {
					$kc_entry = false;
				}
				$mapID = $WSDB->selectCell("SELECT `map` FROM `creature` WHERE `id`=?d LIMIT 1", $kc_entry);
				if(!$mapID) {
					return false;
				}
			}
			if($info = $aDB->selectCell("SELECT `key` FROM `ARMORYDBPREFIX_instance_template` WHERE `map`=?d", $mapID)) {
				$areaUrl = sprintf('source=dungeon&dungeon=%s&boss=all&difficulty=all', $info);
				return $areaUrl;
			}
			break;
		case 'mapID':
			$info = $WSDB->selectCell("SELECT `map` FROM `creature` WHERE `id`=?d LIMIT 1", $npc);
			break;
		case 'rank':
			return $WSDB->selectCell("SELECT `rank` FROM `creature_template` WHERE `entry`=?d", $npc);
			break;
		case 'subname':
			if(Armory::GetLocale() == 'en_gb' || Armory::GetLocale() == 'en_us') {
				return $WSDB->selectCell("SELECT `subname` FROM `creature_template` WHERE `entry`=?d LIMIT 1", $npc);
			}
			else {
				$info = $WSDB->selectCell("SELECT `subname_loc?d` FROM `locales_creature` WHERE `entry`=?d LIMIT 1", Armory::GetLoc(), $npc);
				if(!$info) {
					$killCredit = $WSDB->selectRow("SELECT `KillCredit1`, `KillCredit2` FROM `creature_template` WHERE `entry`=?d", $npc);
					$kc_entry = false;
					if($killCredit['KillCredit1'] > 0) {
						$kc_entry = $killCredit['KillCredit1'];
					}
					elseif($killCredit['KillCredit2'] > 0) {
						$kc_entry = $killCredit['KillCredit2'];
					}
					if($kc_entry) {
						$info = $WSDB->selectCell("SELECT `subname_loc?d` FROM `locales_creature` WHERE `entry`=?d LIMIT 1", Armory::GetLoc(), $kc_entry);
					}
					if(!$info) {
						$info = $WSDB->selectCell("SELECT `subname_loc?d` FROM `locales_creature` WHERE `entry`=?d LIMIT 1", Armory::GetLoc(), $npc);
					}
				}
			}
			break;				
		case 'dungeonlevel':
			$query = $WSDB->selectRow("
			SELECT `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`
				FROM `creature_template` 
					WHERE `entry`=?d AND `difficulty_entry_1` > 0 or `difficulty_entry_2` > 0 or `difficulty_entry_3` > 0", $npc);
			if(!$query) {
				// 10 Normal or 5 Normal
				return 0;
			}
			if($query['difficulty_entry_1'] > 0) {
				// 25 Normal or 5 Heroic
				return 1;
			}
			elseif($query['difficulty_entry_2'] > 0) {
				// 10 Heroic
				return 2;
			}
			elseif($query['difficulty_entry_3' > 0]) {
				// 25 Heroic
				return 3;
			}
			else {
				// 10 Normal or 5 Normal
				return 0;
			}
			break;            
		case 'instance_type':
			$mapID = $WSDB->selectCell("SELECT `map` FROM `creature` WHERE `id`=?d LIMIT 1", $npc);
			$instanceInfo = $aDB->selectCell("SELECT MAX(`max_players`) FROM `ARMORYDBPREFIX_instances_difficulty` WHERE `mapID`=?d", $mapID);
			if($instanceInfo == 5) {
				// Dungeon
				return 1;
			}
			elseif($instanceInfo > 5) {
				// Raid
				return 2;
			}
			break;				
		case 'isBoss': 
			$npc_data = $WSDB->selectRow("SELECT `rank`, `KillCredit1`, `KillCredit2` FROM `creature_template` WHERE `entry`=?d LIMIT 1", $npc);
			if($npc_data['rank'] == 3) {
				return true;
			}
			if($npc_data['KillCredit1'] > 0) {
				$kc_entry = $npc_data['KillCredit1'];
			}
			elseif($npc_data['KillCredit2'] > 0) {
				$kc_entry = $npc_data['KillCredit2'];
			}
			else {
				$kc_entry = 0;
			}
			$npc_id = $npc.', '.$kc_entry;
			$instance = $aDB->selectCell("SELECT `instance_id` FROM `ARMORYDBPREFIX_instance_data` WHERE `id` IN (%s) OR `name_id` IN (%s) OR `lootid_1` IN (%s) OR `lootid_2` IN (%s) OR `lootid_3` IN (%s) OR `lootid_4` IN (%s)", $npc_id, $npc_id, $npc_id, $npc_id, $npc_id, $npc_id);
			if($instance > 0) {
				return true;
			}
			else {
				return false;
			}
			break;
		case 'bossData':
			$data = $aDB->selectRow("
			SELECT `instance_id`, `key`, `lootid_1`, `lootid_2`, `lootid_3`, `lootid_4`
				FROM `ARMORYDBPREFIX_instance_data`
					WHERE `id`=?d OR `lootid_1`=?d OR `lootid_2`=?d OR `lootid_3`=?d OR `lootid_4`=?d",
					$npc, $npc, $npc, $npc, $npc);
			if(!$data) {
				return false;
			}
			$info = array(
				'difficulty' => 'all',
				'key' => $data['key'],
				'dungeon_key' => $aDB->selectCell("SELECT `key` FROM `ARMORYDBPREFIX_instance_template` WHERE `id`=?d", $data['instance_id'])
			);
			for($i=1;$i<5;$i++) {
				if($data['lootid_'.$i] == $npc) {
					if($i == 1 || $i == 2) {
						$info['difficulty'] = 'normal';
					}
					else {
						$info['difficulty'] = 'heroic';
					}
				}
			}
			break;
	}
	if($info) {
		return $info;
	}
	return false;
}
/**
 * Checks is item exists in DB
 * @category Items class
 * @access   public
 * @param    int $itemID
 * @return   bool
 **/
function IsItemExists($itemID) {
	global $WSDB;
	if($WSDB->selectCell("SELECT 1 FROM `item_template` WHERE `entry`= ?d LIMIT 1", $itemID)) {
		return true;
	}
	return false;
}

function checkCached($type, $id)
{
	global $MW, $refreshCacheItems;
	
	if($type == "character"){
		$DataDB = dbsimple_Generic::connect( "" . $MW->getDbInfo['db_type'] . "://" . $MW->getDbInfo['db_username'] .
			":" . $MW->getDbInfo['db_password'] . "@" . $MW->getDbInfo['db_host'] . ":" . $MW->getDbInfo['db_port'] .
			"/data" ) ;
		if ( $DataDB ){
			$DataDB->setErrorHandler( 'databaseErrorHandler' ) ;
		}

		$cacheRows = $DataDB->select('SELECT * FROM character_cache WHERE guid = '.$id.' AND type = '.CACHETYPE_TOOLTIP.';');
		$refreshCache = true;
		if(count($cacheRows))
		{

			$cache = $cacheRows[0];	
			
			$cachingPeriod = 60 * 60 * 24;
			
			if((time() - $cache["time"]) < $cachingPeriod)
				$refreshCache = false;
		}
		
		if($refreshCache)
		{
			$refreshCacheItems[] = "/tooltip/character/norgannon/".$id;
		}
	}
}

function make_token() {
	
	return md5(rand(4,8))."-".md5(rand(4,6))."-".md5(rand(8, 8));
	
}

function getLayout($filename){
	global $currtmp, $base_template;
	
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$currtmp.'/'.$filename))
		return $currtmp.'/'.$filename;
	else
		return $base_template.'/'.$filename;
}

function getFactionId($race)
{
	switch($race)
	{
		case RACE_HUMAN:
		case RACE_DWARF:
		case RACE_NIGHTELF:
		case RACE_GNOME:
		case RACE_DRAENEI:
			return FACTION_ALLIANCE; break;

		case RACE_ORC:
		case RACE_UNDEAD:
		case RACE_TAUREN:
		case RACE_TROLL:
		case RACE_BLOODELF:
			return FACTION_HORDE; break;
		
	}
}

function getAllianceFactions(){
	return array(RACE_HUMAN, RACE_DWARF, RACE_NIGHTELF, RACE_GNOME, RACE_DRAENEI);
}

function getHordeFactions(){
	return array(RACE_ORC, RACE_UNDEAD, RACE_TAUREN, RACE_TROLL, RACE_BLOODELF);
}

function getRaceLabel($race, $gender = 0)
{
	switch($race)
	{
		case RACE_HUMAN:		return "Mensch";
		case RACE_DWARF:		return ($gender == 0) ? "Zwerg" : "Zwergin";
		case RACE_NIGHTELF:		return ($gender == 0) ? "Nachtelf" : "Nachtelfin";
		case RACE_GNOME:		return ($gender == 0) ? "Gnom" : "Gnomin";
		case RACE_DRAENEI:		return "Draenei";


		case RACE_ORC:			return "Ork";
		case RACE_UNDEAD:		return ($gender == 0) ? "Untoter" : "Untote";
		case RACE_TAUREN:		return ($gender == 0) ? "Taure" : "Taurin";
		case RACE_TROLL:		return ($gender == 0) ? "Troll" : "Trollin";
		case RACE_BLOODELF:		return ($gender == 0) ? "Blutelf" : "Blutelfin";
		
	}
}

function getClassLabel($class, $gender = 0){
	switch($class){
		case CLASS_WARRIOR:		return ($gender == 0) ? "Krieger" : "Kriegerin";
		case CLASS_PALADIN:		return "Paladin";
		case CLASS_HUNTER:		return ($gender == 0) ? "J&auml;ger" : "J&auml;gerin";
		case CLASS_ROGUE:		return ($gender == 0) ? "Schurke" : "Schurkin";
		case CLASS_PRIEST:		return ($gender == 0) ? "Priester" : "Priesterin";
		case CLASS_DK:			return ($gender == 0) ? "Todesritter" : "Todesritterin";
		case CLASS_SHAMAN:		return ($gender == 0) ? "Schamane" : "Schamanin";
		case CLASS_MAGE:		return ($gender == 0) ? "Magier" : "Magierin";
		case CLASS_WARLOCK:		return ($gender == 0) ? "Hexenmeister" : "Hexenmeisterin";
		case CLASS_DRUID:		return ($gender == 0) ? "Druide" : "Druidin";
	}
}

function getProfessionLabel($professionId){
	switch($professionId){
		case 164:	return "Schmieden";
		case 165:	return "Lederverarbeitung";
		case 171:	return "Alchimie";
		case 182:	return "Kräuterkunde";
		case 186:	return "Bergbau";
		case 197:	return "Schneidern";
		case 202:	return "Ingenieurskunst";
		case 333:	return "Verzaubern";
		case 393:	return "Kürschnerei";
		case 755:	return "Juwelenschleifen";
		case 773:	return "Inschriftenkunde";
	}
	return "";
}

function time_diff_conv($start, $end) {
    $t = array( //suffixes
        'd' => 86400,
        'h' => 3600,
        'm' => 60,
    );
    $s = abs($end - $start);
    foreach($t as $key => &$val) {
        $$key = floor($s/$val);
        $s -= ($$key*$val);
        $string .= ($$key==0) ? '' : $$key . "$key ";
    }
    return $string . $s. 's';
}

// quote smart function to do MySQL Escaping properly //
function quote_smart($value)
{
    if( is_array($value) ) {
        return array_map("quote_smart", $value);
    } else {
        if( get_magic_quotes_gpc() ) {
            $value = stripslashes($value);
        }
        if( $value == '' ) {
            $value = 'NULL';
        } if( !is_numeric($value) || $value[0] == '0' ) {
            $value = "'".mysql_real_escape_string($value)."'";
        }
        return $value;
    }
}

function get_from_mangosconf($configvalue,$id){ // Get Config values FROM mangos conf file. $configvalue should be name of config. $id is ID of realm ( config array value )
    global $MW;
    $init = 'id_'.$id;
    $mangosconf = file((string)$MW->getConfig->mangos_conf_external->$init->mangos_world_conf);
    foreach ($mangosconf as $line_num){
        $line_num = (string)$line_num;
        if (strstr($line_num,$configvalue) == TRUE){
            $arr = explode(' = ', $line_num);
            return str_replace("\r","",str_replace("\n","",$arr['1']));
        }
    }
}

function sha_password($user,$pass){
    $user = strtoupper($user);
    $pass = strtoupper($pass);
    return SHA1($user.':'.$pass);
}

function check_for_symbols($string, $space_check = 0){
    //$space_check=1 means space is not allowed
    $len=strlen($string);
    $allowed_chars="abcdefghijklmnopqrstuvwxyz���ABCDEFGHIJKLMNOPQRSTUVWXYZ���0123456789";
    if(!$space_check) {
        $allowed_chars .= " ";
    }
    for($i=0;$i<$len;$i++)
        if(strstr($allowed_chars,$string[$i]) == FALSE)
            return TRUE;
    return FALSE;
}

function get_banned($account_id,$returncont){
    global $DB;
	
    // Account prüfen
    $banned = $DB->selectCell("SELECT id FROM account_banned WHERE id = '$account_id' AND active = 1");
    if($banned != FALSE && $returncont == 1){
    	return TRUE;
    }
    
    // IP noch prüfen.
    $get_last_ip = $DB->selectCell("SELECT last_ip FROM account WHERE id='".$account_id."'");
    $db_IP = $get_last_ip;
	
    $ip_check = $DB->selectCell("SELECT ip FROM `ip_banned` WHERE ip='".$db_IP."'");
    if ($ip_check == FALSE){
        if ($returncont == "1"){
            return FALSE;
        }
    }
    else{
        if ($returncont == "1"){
            return TRUE;
        }
        else{
            return $db_IP;
        }
    }
}

/**
 * Replaces the first character of a text by a left-aligned picture of this character (html img tag)
 *
 * Remark: this function is limited to alphabetical characters. If a known
 * character with accent is found, the image of this letter without accent
 * will be used. If the character is completely unknown, the text is returned
 * without changes.
 *
 * @param $text string Text for which the first character should be replaced by an image letter
 * @return string Text with image letter
 */
function add_pictureletter($text){
	global $userObject;
    
	if($userObject->theme == "Shattered-World")
		return '<h3 class="sub-title"><span>'.$text.'</span></h3>';
	
	$letter = substr($text, 0, 1);
    $imageletter = strtr(strtolower($letter),"���������������������������������������������������������������������",
                                             "sozsozyyuaaaaaaaceeeeiiiidnoooooouuuuysaaaaaaaceeeeiiiionoooooouuuuyy");
    if (strpos("abcdefghijklmnopqrstuvwxyz", $imageletter) === false)
        return $text;
    $img = '<img src="templates/WotLK/images/letters/'.$imageletter.'.gif" alt="'.$letter.'" align="left"/>';
    $output = $img . substr($text, 1);
    return $output;
}

function random_string($counts){
    $str = "abcdefghijklmnopqrstuvwxyz";//Count 0-25
    $o = 0;
    for($i=0;$i<$counts;$i++){
        if ($o == 1){
            $output .= rand(0,9);
            $o = 0;
        }else{
            $o++;
            $output .= $str[rand(0,25)];
        }
    }
    return $output;
}


function output_message($type,$text,$file='',$line=''){
    if($file)$text .= "\n<br>in file: $file";
    if($line)$text .= "\n<br>on line: $line";
    
    
    switch($type){
    	case "warning":
    		$class = "warning-orange"; break;
    	case "error":
    		$class = "warning-red"; break;
    	default:
    		$class = "warning-green";
    }
    
    	$GLOBALS['messages'] .= "\n<div class=\"".$type."_box warning ".$class." \"><div class=\"warning-inner2\">$text</div></div> \n";
}

function makeWowheadLinks($string){
	if(preg_match("@https?://(www|de|old)\.wowhead.com/\??([^=]+=-?(\d+))@i", $string, $matches)){
		$temp = preg_replace("@https?://(www|de|old)\.wowhead.com/\??([^=]+=-?(\d+))@i", "<a href=\"$0\" target=\"_blank\">$0</a>", $string);
		if(!empty($temp))
			$string = $temp;
	}
	if(preg_match("@https?://(www|portal)\.wow-alive.de/server/bugtracker/bug/(\d+)/?@i", $string, $matches)){
		$temp = preg_replace("@https?://(www|portal)\.wow-alive.de/server/bugtracker/bug/(\d+)/?@i", "<a href=\"$0\" target=\"_blank\">$0</a>", $string);
		if(!empty($temp))
			$string = $temp;
	}
	return $string;	
}

function redirect($linkto,$type=0,$wait_sec=0){
    if($linkto){
        if($type==0){
            $GLOBALS['redirect'] = '<meta http-equiv=refresh content="'.$wait_sec.';url='.$linkto.'">';
        }else{
            // Header not works for some(?) computers. Add hax to it.
            header("Location: ".$linkto);
        }
    }
}

function loadLanguages(){
    global $realmd;
    global $mangos;
    global $languages;
    global $lang;
    global $MW;
    $languages = array();
    $lang = array();
    if ($handle = opendir('lang/')) {
        $available_languages = explode(",", $MW->getConfig->generic->available_languages);
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".." && $file != "Thumbs.db" && $file != "index.html") {
                $tmp = explode('.',$file);
                if(isset($tmp[2]) && $tmp[2]=='lang' && in_array($tmp[0], $available_languages))$languages[$tmp[0]] = $tmp[1];
            }
        }
        closedir($handle);
        if (!in_array($MW->getConfig->generic->default_lang, $available_languages)) {
            array_unshift($available_languages, $MW->getConfig->generic->default_lang);
        }
        if (!in_array($GLOBALS['user_cur_lang'], $available_languages)) {
            $GLOBALS['user_cur_lang'] = $MW->getConfig->generic->default_lang;
        }
        if(file_exists('core/cache/lang/'.$GLOBALS['user_cur_lang'].'.'.$languages[$GLOBALS['user_cur_lang']].'.php')) {
            if((filemtime('core/cache/lang/'.$GLOBALS['user_cur_lang'].'.'.$languages[$GLOBALS['user_cur_lang']].'.php') >= filemtime('lang/'.$GLOBALS['user_cur_lang'].'.'.
            $languages[$GLOBALS['user_cur_lang']].'.lang')) && (filemtime('core/cache/lang/'.$GLOBALS['user_cur_lang'].'.'.$languages[$GLOBALS['user_cur_lang']].'.php') >=
            filemtime('lang/'.(string)$MW->getConfig->generic->default_lang.'.'.$languages[(string)$MW->getConfig->generic->default_lang].'.lang'))) {
                include_once('core/cache/lang/'.$GLOBALS['user_cur_lang'].'.'.$languages[$GLOBALS['user_cur_lang']].'.php');
                return;
            }
        }
        $langfile = @file_get_contents('lang/'.(string)$MW->getConfig->generic->default_lang.'.'.$languages[(string)$MW->getConfig->generic->default_lang].'.lang');
        $langfile = str_replace("\n",'',$langfile);
        $langfile = str_replace("\r",'',$langfile);
        $langfile = explode('|=|',$langfile);
        foreach($langfile as $langstr){
            $langstra = explode(' :=: ',$langstr);
            if(isset($langstra[1]))$lang[$langstra[0]] = $langstra[1];
        }
        if ($GLOBALS['user_cur_lang'] != (string)$MW->getConfig->generic->default_lang) {
            $langfile = @file_get_contents('lang/'.$GLOBALS['user_cur_lang'].'.'.$languages[$GLOBALS['user_cur_lang']].'.lang');
            $langfile = str_replace("\n",'',$langfile);
            $langfile = str_replace("\r",'',$langfile);
            $langfile = explode('|=|',$langfile);
            foreach($langfile as $langstr){
                $langstra = explode(' :=: ',$langstr);
                if(isset($langstra[1]) && trim($langstra[1])!='')$lang[$langstra[0]] = $langstra[1];
            }
        }

        $fhlang = fopen ('core/cache/lang/'.$GLOBALS['user_cur_lang'].'.'.$languages[$GLOBALS['user_cur_lang']].'.php', "w");
        fwrite($fhlang, '<?php'."\n".'$lang = '.var_export($lang,1).";\n".'?>');
        fclose($fhlang);
    }

}

/**
 * Translates a variable into the current user language
 *
 * This translation function uses the language file as cached by the
 * loadLanguages() function. If a requested translation is not found in the
 * default language file as well as the current users language's file, this
 * function will convert the given text by replacing underscores with spaces
 * and capitalizing the first character.
 *
 * @param $var string The variable/name to be translated. This should be available in the language file
 * @param $dontecho boolean Should this text be echoed (default) or not? This parameter can be valuable if you just want the translation to be returned, but not sent to the browser immediately.
 * @return string Translated text.
 */
function lang($var, $dontecho=false){
    global $lang;
    if (isset($lang[$var]))
        $result = $lang[$var];
    else
        $result = ucfirst(str_replace('_', ' ', $var));
    if (!$dontecho)
        echo $result;
    return $result;
}


/**
 * Returns a translated resource file
 *
 * This function can be called to retrieve a translated resource using simply a
 * filename like "howtoplay.html". It then expects the translated resource to
 * be available under /lang/howtoplay/en.html. It first looks for the
 * document in the current user's language and if not found it tries to use the
 * default_lang configured language.
 *
 * Note: To prevent backwards compatibility issues, this function also finds
 * your document when it's stored as /lang/en.howtoplay.html or
 * /lang/howtoplay/howtoplay_en.html. This format will be deprecated soon
 * however.
 *
 * @param $name string The resource name in "filename.ext" format
 * @return string Translated resource content.
 */
function lang_resource($name){
    global $MW;
    $resourceinfo = pathinfo($name);
    $resourcename = $resourceinfo["filename"];
    $resourceext = $resourceinfo["extension"];
    
    $languages[] = isset($GLOBALS['user_cur_lang']) ? $GLOBALS['user_cur_lang'] : '';
    $languages[] = (string)$MW->getConfig->generic->default_lang;
    $result = '';
    foreach($languages as $language)
    {
        $sourcefiles[] = "lang/$resourcename/$language.$resourceext";
        $sourcefiles[] = "lang/$resourcename/{$resourcename}_$language.$resourceext";
        $sourcefiles[] = "lang/$language.$resourcename.$resourceext";
        foreach($sourcefiles as $sourcefile)
        {
            if (file_exists($sourcefile))
                $result = file_get_contents($sourcefile);
            if (trim($result) != '')
                return $result;
        }
    }
    return '';
}


/**
 * Returns a list of available smilies
 *
 * @param $dir string Path on local filesystem of server to the smilies (defaults to images/smiles/)
 * @return array List of found files in the given directory (.svn, Thumbs.db and index.html are exluded)
 */
function load_smiles($dir='images/smiles/'){
    $allfiles = scandir($dir);
    $smiles = array_diff($allfiles, array(".", "..", ".svn", "Thumbs.db", "index.html"));
    return $smiles;
}

function sanitize($var, $type = "string"){
	
	if($type == "int"){
		$var = (int) $var;
		return $var;
	}
	elseif($type == "string"){
		return preg_replace("/[^A-Za-z0-9,-äöüÄÖÜß='\s]/","",$var);
	}
	
}

function save_session_start(){
	/*if(!defined("INCLUDED")){
		die("Fehlerhafter Dateiaufruf.");	
	}*/
	if(!session_id()) {
		// Start the session:
		session_start();
	}
}

function send_email($to_email,$to_name,$theme,$text_text,$text_html=''){
    global $MW;
    if(!(string)$MW->getConfig->generic->smtp_adress){
        output_message('alert','Set SMTP settings in config !');
        return false;
    }
    if(!$to_email){
        output_message('alert','Field "to" is empty.');
        return false;
    }
    set_time_limit(300);
    include('core/mail/smtp.php');
    $mail = new SMTP;
    $mail->Delivery('relay');
    $mail->Relay((string)$MW->getConfig->generic->smtp_adress,(string)$MW->getConfig->generic->smtp_username,(string)$MW->getConfig->generic->smtp_password);
    $mail->From((string)$MW->getConfig->generic->site_email, (string)$MW->getConfig->generic->site_title);
    $mail->AddTo($to_email, $to_name);
    $mail->Text($text_text);
    if($text_html)$mail->Html($text_html);
    $sent = $mail->Send($theme);
    return $sent;
}

function strip_if_magic_quotes($value){
    if (get_magic_quotes_gpc()) {
        $value = stripslashes($value);
    }
    return $value;
}

function my_preview($text,$userlevel=0){
    if($userlevel<1){$text = htmlspecialchars($text);if (get_magic_quotes_gpc()){$text = stripslashes($text);} }
    $text = nl2br($text);
    $text = preg_replace("/\\[b\\](.*?)\\[\\/b\\]/s","<b>$1</b>",$text);
    $text = preg_replace("/\\[i\\](.*?)\\[\\/i\\]/s","<i>$1</i>",$text);
    $text = preg_replace("/\\[u\\](.*?)\\[\\/u\\]/s","<u>$1</u>",$text);
    $text = preg_replace("/\\[s\\](.*?)\\[\\/s\\]/s","<s>$1</s>",$text);
    $text = preg_replace("/\\[hr\\]/s","<hr>",$text);
    $text = preg_replace("/\\[code\\](.*?)\\[\\/code\\]/s","<code>$1</code>",$text);
    //$text = preg_replace("/\[blockquote\](.*?)\[\/blockquote\]/s","<blockquote>$1</blockquote>",$text);
    if (strpos($text, 'blockquote') !== false)
    {
        if(substr_count($text, '[blockquote') == substr_count($text, '[/blockquote]')){
            $text = str_replace('[blockquote]', '<blockquote><div>', $text);
            $text = preg_replace('#\[blockquote=(&quot;|"|\'|)(.*)\\1\]#sU', '<blockquote><span class="bhead">Quote: $2</span><div>', $text);
            $text = preg_replace('#\[\/blockquote\]\s*#', '</div></blockquote>', $text);
        }
    }
    // Blizz quote <small><hr color="#9e9e9e" noshade="noshade" size="1"><small class="white">Q u o t e:</small><br>Text<hr color="#9e9e9e" noshade="noshade" size="1"></small>
    $text = preg_replace("/\\[img\\](.*?)\\[\\/img\\]/s","<img src=\"$1\" align=\"absmiddle\">",$text);
    $text = preg_replace("/\\[attach=(\\d+)\\]/se","check_attach('\\1')",$text);
    $text = preg_replace("/\\[url=(.*?)\\](.*?)\\[\\/url\\]/s","<a href=\"$1\" target=\"_blank\">$2</a>",$text);
    $text = preg_replace("/\\[size=(.*?)\\](.*?)\\[\\/size\\]/s","<font class='$1'>$2</font>",$text);
    $text = preg_replace("/\\[align=(.*?)\\](.*?)\\[\\/align\\]/s","<p align='$1'>$2</p>",$text);
    $text = preg_replace("/\\[color=(.*?)\\](.*?)\\[\\/color\\]/s","<font color=\"$1\">$2</font>",$text);
    $text = preg_replace("/[^\\'\"\\=\\]\\[<>\\w]([\\w]+:\\/\\/[^\n\r\t\\s\\[\\]\\>\\<\\'\"]+)/s"," <a href=\"$1\" target=\"_blank\">$1</a>",$text);
    return $text;
}

function my_previewreverse($text){
    $text = str_replace('<br />','',$text);
    $text = preg_replace("/<b>(.*?)<\\/b>/s","[b]$1[/b]",$text);
    $text = preg_replace("/<i>(.*?)<\\/i>/s","[i]$1[/i]",$text);
    $text = preg_replace("/<u>(.*?)<\\/u>/s","[u]$1[/u]",$text);
    $text = preg_replace("/<s>(.*?)<\\/s>/s","[s]$1[/s]",$text);
    $text = preg_replace("/<hr>/s","[hr]",$text);
    $text = preg_replace("/<code>(.*?)<\\/code>/s","[code]$1[/code]",$text);
    //$text = preg_replace("/<blockquote>(.*?)<\/blockquote>/s","[blockquote]$1[/blockquote]",$text);
    if (strpos($text, 'blockquote') !== false)
    {
        if(substr_count($text, '<blockquote>') == substr_count($text, '</blockquote>')){
            $text = str_replace('<blockquote><div>', '[blockquote]', $text);
            $text = preg_replace('#\<blockquote><span class="bhead">\w+: (&quot;|"|\'|)(.*)\\1\<\/span><div>#sU', '[blockquote="$2"]', $text);
            $text = preg_replace('#<\/div><\/blockquote>\s*#', '[/blockquote]', $text);
        }
    }
    $text = preg_replace("/<img src=.([^'\"<>]+). align=.absmiddle.>/s","[img]$1[/img]",$text);
    $text = preg_replace("/(<a href=.*?<\\/a>)/se","check_url_reverse('\\1')",$text);
    $text = preg_replace("/<font color=.([^'\"<>]+).>([^<>]*?)<\\/font>/s","[color=$1]$2[/color]",$text);
    $text = preg_replace("/<font class=.([^'\"<>]+).>([^<>]*?)<\\/font>/s","[size=$1]$2[/size]",$text);
    $text = preg_replace("/<p align=.([^'\"<>]+).>([^<>]*?)<\\/p>/s","[align=$1]$2[/align]",$text);
    return $text;
}

function check_url_reverse($url){
    $url = stripslashes($url);
    if(eregi('attach',$url) && eregi('attid',$url)){
        $result = preg_replace("/<a href=\"[^\\'\"]*attid=(\\d+)[^\\'\"]*\" target=\"_blank\">.*?<\\/a>/s","[attach=$1]",$url);
    }else{
        $result = preg_replace("/<a href=\"([^'\"<>]+)\" target=\"_blank\">(.*?)<\\/a>/s","[url=$1]$2[/url]",$url);
    }
    return $result;
}

function check_attach($attid){
    global $DB, $MW;
    $thisattach = $DB->selectRow("SELECT * FROM f_attachs WHERE attach_id=?d",$attid);
    $ext = strtolower(substr(strrchr($thisattach['attach_file'],'.'), 1));
    if($thisattach['attach_id']){
        $res  = '<a href="'.$MW->getConfig->temp->site_href.'index.php?n=forum&sub=attach&nobody=1&action=download&attid='.$thisattach['attach_id'].'">';
        $res .= '<img src="'.$MW->getConfig->temp->site_href.'images/mime/'.$ext.'.png" alt="" align="absmiddle">';
        $res .= ' Download: [ '.$thisattach['attach_file'].' ] '.return_good_size($thisattach['attach_filesize']).' </a>';
    }
    return $res;
}

function check_image($img_file){
    global $MW;
    $maximgsize = explode('x',(string)$MW->getConfig->generic->imageautoresize);
    $path_parts = pathinfo($img_file);
    $max_width = (int)$maximgsize[0];
    $max_height = (int)$maximgsize[1];
    $fil_scr_res = getimagesize(rawurldecode($img_file));
    if($fil_scr_res[0]>$max_width || $fil_scr_res[1]>$max_height){
        $n_img_file = $path_parts['dirname'].'/resized_'.$path_parts['basename'];
        if(!file_exists($n_img_file)){
            include('core/class.image.php');
            $img = new IMAGE;
            ob_start();
            $res = $img->send_thumbnail($img_file,$max_width,$max_height,true);
            $imgcontent = ob_get_contents();
            @ob_end_clean();
            if ($res && (@$fp = fopen($n_img_file,'w+')))
            {
                fwrite($fp,$imgcontent);
                fclose($fp);
            }else{
                output_message('alert','Could not create preview!');
            }
        }
        $image = $n_img_file;
    }else{
        $image = $img_file;
    }
    return $image;
}

function return_good_size($n){
    $kb_divide = 1024;
    $mb_divide = 1024*1024;
    $gb_divide = 1024*1024*1024;

    if($n < $mb_divide){$res = round(($n/$kb_divide),2).' Kb';}
    elseif($n < $gb_divide){$res = round(($n/$mb_divide),2).' Mb';}
    elseif($n >= $gb_divide){$res = round(($n/$gb_divide),2).' Gb';}

    return $res;
}

function default_paginate($num_pages, $cur_page, $link_to){
    $pages = array();
    $link_to_all = false;
    if ($cur_page == -1)
    {
        $cur_page = 1;
        $link_to_all = true;
    }
    if ($num_pages <= 1)
        $pages = array('');
    else
    {
        $tens = floor($num_pages/10);
        for ($i=1;$i<=$tens;$i++)
        {
            $tp = $i*10;
            $pages[$tp] = "<a href='$link_to&p=$tp'>$tp</a>";
        }
        if ($cur_page > 3)
        {
            $pages[1] = "<a href='$link_to&p=1'>1</a>";
        }
        for ($current = $cur_page - 2, $stop = $cur_page + 3; $current < $stop; ++$current)
        {
            if ($current < 1 || $current > $num_pages) {
                continue;
            } elseif ($current != $cur_page || $link_to_all) {
                $pages[$current] = "<a href='$link_to&p=$current'>$current</a>";
            } else {
                $pages[$current] = '['.$current.']';
            }
        }
        if ($cur_page <= ($num_pages-3))
        {
            $pages[$num_pages] = "<a href='$link_to&p=$num_pages'>$num_pages</a>";
        }
    }
    $pages = array_unique($pages);
    ksort($pages);
    $pp = implode(' ', $pages);
    return $pp;
}

// ACCOUNT KEY FUNCTIONS //
function matchAccountKey($id, $key) {
    clearOldAccountKeys();
    global $DB;
    $count = $DB->selectcell("SELECT count(*) FROM `account_keys` where id = ?", $id);
    if($count == 0) {
        return false;
    }
    $account_key = $DB->selectcell("SELECT `key` FROM `account_keys` where id = ?", $id);
    if($key == $account_key) {
        return true;
    }
    else {
        return false;
    }
}

function clearOldAccountKeys() {
    global $DB;
    global $MW;

    $cookie_expire_time = (int)$MW->getConfig->generic->account_key_retain_length;
    if(!$cookie_expire_time) {
        $cookie_expire_time = (60*60*24*365);   //default is 1 year
    }

    $expire_time = time() - $cookie_expire_time;

    $DB->query("DELETE FROM `account_keys` WHERE `assign_time` < ?", $expire_time);
}

function addOrUpdateAccountKeys($id, $key) {
    global $DB;

    $current_time = time();

    $count = $DB->selectcell("SELECT count(*) FROM account_keys where id = ?", $id);
    if($count == 0) {   //need to INSERT
        $DB->query("INSERT INTO `account_keys` SET `id` = ?, `key` = ?, `assign_time` = ?", $id, $key, $current_time);
    }
    else {              //need to UPDATE
        $DB->query("UPDATE `account_keys` SET `key` = ?, `assign_time` = ? WHERE `id` = ?", $key, $current_time, $id);
    }
}

function removeAccountKeyForUser($id) {
    global $DB;

    $count = $DB->selectcell("SELECT count(*) FROM account_keys where id = ?", $id);
    if($count == 0) {
        //do nothing
    }
    else {
        $DB->query("DELETE FROM `account_keys` WHERE `id` = ?", $id);
    }
}

function localizedImage($filename)
{
    $alang=$GLOBALS['user_cur_lang'];
    if (file_exists("images/localized/$alang/$filename"))
    {
        $path="images/localized/$alang/$filename";
    }
    else
    {
        $path="images/localized/en/$filename";
    }

    return $path;
}

// Function: Logs an message to log file.
function log_error($string, $choise=1){

  if($choise == 1){
    $fh = fopen("core/logs/core_error_log.log", 'a');
    fwrite($fh, "Error ".date('Y m d h: s: m')."  --  >  ".$string." \n");
    fclose($fh);
  }
}

function RemoveXSS($val) {
   // remove all non-printable characters. CR(0a) and LF(0b) and TAB(9) are allowed
   // this prevents some character re-spacing such as <java\0script>
   // note that you have to handle splits with \n, \r, and \t later since they *are* allowed in some inputs
   $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);

   // straight replacements, the user should never need these since they're normal characters
   // this prevents like <IMG SRC=&#X40&#X61&#X76&#X61&#X73&#X63&#X72&#X69&#X70&#X74&#X3A&#X61&#X6C&#X65&#X72&#X74&#X28&#X27&#X58&#X53&#X53&#X27&#X29>
   $search = 'abcdefghijklmnopqrstuvwxyz';
   $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $search .= '1234567890!@#$%^&*()';
   $search .= '~`";:?+/={}[]-_|\'\\';
   for ($i = 0; $i < strlen($search); $i++) {
      // ;? matches the ;, which is optional
      // 0{0,7} matches any padded zeros, which are optional and go up to 8 chars

      // &#x0040 @ search for the hex values
      $val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val); // with a ;
      // &#00064 @ 0{0,7} matches '0' zero to seven times
      $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
   }

   // now the only remaining whitespace attacks are \t, \n, and \r
   $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
   $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
   $ra = array_merge($ra1, $ra2);

   $found = true; // keep replacing as long as the previous round replaced something
   while ($found == true) {
      $val_before = $val;
      for ($i = 0; $i < sizeof($ra); $i++) {
         $pattern = '/';
         for ($j = 0; $j < strlen($ra[$i]); $j++) {
            if ($j > 0) {
               $pattern .= '(';
               $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
               $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
               $pattern .= ')?';
            }
            $pattern .= $ra[$i][$j];
         }
         $pattern .= '/i';
         $replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2); // add in <> to nerf the tag
         $val = preg_replace($pattern, $replacement, $val); // filter out the hex tags
         if ($val_before == $val) {
            // no replacements were made, so exit the loop
            $found = false;
         }
      }
   }
   return $val;
}

function get_realm_byid($id){
    global $DB, $MW;
    $search_q = $DB->selectRow("SELECT * FROM `realmlist` WHERE `id`=?d",$id);
    if((int)$MW->getConfig->generic->use_local_ip_port_test) {
        $search_q['address'] = "127.0.0.1";
    }

    return $search_q;
}

function check_port_status($ip, $port){
    $ERROR_NO = null;
    $ERROR_STR = null;
    if($fp1=fsockopen($ip, $port, $ERROR_NO, $ERROR_STR,(float)1.0)){
        fclose($fp1);return true;
    }else{
        return false;
    }
}

function databaseErrorHandler($message, $info)
{
    if (!error_reporting()) return;
    
    if(in_array($info["code"],array(1129)) ){
    	output_message('alert',"Wegen Angriffen auf den Server ist die Verbindung zur Datenbank nur eingeschränkt möglich, der Server ist aber ungeachtet der Anzeige unten online.");
    }
    else {
 	   output_message('alert',"SQL Error ".$info["code"].": $message <!--".$info["context"]."--></pre>");
    }
	echo "<!-- SQL Error: $message ".print_r($info,true)." -->";
}

function getMangosConfig($mangos_config_file) {
    $file_contents = file_get_contents($mangos_config_file);
    $explode_array = explode("\n",$file_contents);
    $config_details = array();
    foreach($explode_array as $explode_entry) {
        $new = explode('#', $explode_entry);
        $new[0] = trim($new[0]);
        if($new[0]) {
            $this_line_array = explode('=', $new[0]);
            $this_line_array[0] = trim($this_line_array[0]);
            $this_line_array[1] = trim($this_line_array[1]);
            $this_line_array[1] = trim($this_line_array[1],';');
            //$config_details[$this_line_array[0]] = $this_line_array[1];
            $config_details[$this_line_array[0]] = $this_line_array[1];
        }
    }
    unset($config_details['LoginDatabaseInfo']);
    unset($config_details['WorldDatabaseInfo']);
    return $config_details;
}

function cycle($var, $array){
	for($i = 0; $i < count($array); $i++){
		if($var == $array[$i])
			return (isset($array[$i+1]) ? $array[$i+1] : $array[0]);
	}
	return $array[0];	
}

/**
 * Generates money value
 **/
function GetMoney($money) {
	$getMoney['gold'] = floor($money/(100*100));
	$money = $money-$getMoney['gold']*100*100;
	$getMoney['silver'] = floor($money/100);
	$money = $money-$getMoney['silver']*100;
	$getMoney['copper'] = floor($money);
	return $getMoney;
}

/**
 * Composes a MangosWeb url which can be used in templates for example.
 *
 * Using this function instead of handcrafted index.php?n=..&sub=.. allows
 * for easier transparent url rewriting. It also implementes encoding the
 * entities in the url so you can echo the result of this result directly
 * in your html code without causing it fail W3C validation.
 *
 * @param $page string The page to be targeted by this url
 * @param $subpage string The subpage to be targeted by this url
 * @param $params array An optional array containing additional arguments to be passed when requesting the url (default empty)
 * @param $encodentities boolean Encode the entities (like replacing & by &amp;) so it can be used in html templates directly? (defaults to true)
 * @result string The url containing all the given parameters
 * @todo Make a config option for url rewriting and implement an if switch
 *       here to make urls like /account/manage instead of 
 *       index.php?n=account&sub=manage possible.
 */
function mw_url($page, $subpage, $params=null, $encodeentities=true) {
    $url = "index.php?n=$page&sub=$subpage";
    if (is_array($params)) {
        foreach($params as $key=>$value) {
            $url .= "&$key=$value";
        }
    }
    return $encodeentities ? htmlentities($url) : $url;
}

/*
	Routing version of mw_url
*/
function url_for($page, $subpage = "index", $params=array())
{
	$params["ext"] = $page;
	if(!empty($subpage))
		$params["sub"] = $subpage;
	$url = _url_for($params);	
	
	$url .= "/";
	$url = str_replace("//","/",$url);
	
	return $url;
	
}

function _url_for($params)
{
	global $oRouter, $oRequest;
	
	//convert parameters in string format to array format is necessary
	if (!is_array($params)) 
		$params = $oRouter->stringToRouteParams($params);
	
	if (!empty($params["sub"]) && $oRouter->isRouteExists($params['ext']."_".$params["sub"]))
	{
		return $oRequest->getRelativeUrlRoot().$oRouter->generate($params['ext']."_".$params["sub"], $params);
	}
	elseif ($oRouter->isRouteExists($params['ext']))
	{
		return $oRequest->getRelativeUrlRoot().$oRouter->generate($params['ext'], $params);
	}
	
	return $oRequest->getRelativeUrlRoot().$oRouter->generate('module_action', $params);
}
/**
 * Returns spell radius.
 * @category Utils class
 * @access   public
 * @param    int $index
 * @return   string
 **/
function GetRadius($index) {
	$gSpellRadiusIndex = array(
		 '7' => array(2,0,2),
		 '8' => array(5,0,5),
		 '9' => array(20,0,20),
		'10' => array(30,0,30),
		'11' => array(45,0,45),
		'12' => array(100,0,100),
		'13' => array(10,0,10),
		'14' => array(8,0,8),
		'15' => array(3,0,3),
		'16' => array(1,0,1),
		'17' => array(13,0,13),
		'18' => array(15,0,15),
		'19' => array(18,0,18),
		'20' => array(25,0,25),
		'21' => array(35,0,35),
		'22' => array(200,0,200),
		'23' => array(40,0,40),
		'24' => array(65,0,65),
		'25' => array(70,0,70),
		'26' => array(4,0,4),
		'27' => array(50,0,50),
		'28' => array(50000,0,50000),
		'29' => array(6,0,6),
		'30' => array(500,0,500),
		'31' => array(80,0,80),
		'32' => array(12,0,12),
		'33' => array(99,0,99),
		'35' => array(55,0,55),
		'36' => array(0,0,0),
		'37' => array(7,0,7),
		'38' => array(21,0,21),
		'39' => array(34,0,34),
		'40' => array(9,0,9),
		'41' => array(150,0,150),
		'42' => array(11,0,11),
		'43' => array(16,0,16),
		'44' => array(0.5,0,0.5),
		'45' => array(10,0,10),
		'46' => array(5,0,10),
		'47' => array(15,0,15),
		'48' => array(60,0,60),
		'49' => array(90,0,90)
	);
	if(!isset($gSpellRadiusIndex[$index])) {
		return false;
	}
	$radius = @$gSpellRadiusIndex[$index];
	if($radius == 0) {
		return false;
	}
	if($radius[0] == 0 || $radius[0] == $radius[2]) {
		return $radius[2];
	}
	return $radius[0] . ' - ' . $radius[2];
}

function dump( $var ) {
    $result = var_export( $var, true );
    $loc = whereCalled();
    return "\n<pre>Dump: $loc\n$result</pre>";
}


	
function debug($label, $data = ""){
	$string = "";
	
	if(is_array($label) || is_object($label))
		$string .= " ".print_r($label,true)." ";
	else
		$string .= " $label ";
	
	if($data === ""){
	}
	elseif(is_array($data) || is_object($data))
		$string .= " ".print_r($data,true)." ";
	else
		$string .= " $data ";
	
	echo "\n<!-- ".$string." -->";
}

function sec_to_dhms($sec, $show_days = false, $label = "")
{
	global $lang;
	$days = intval($sec / 86400);
	$hours = intval(($sec / 3600) % 24);
	$minutes = intval(($sec / 60) % 60);
	$seconds = intval($sec % 60);
	$string = array();
	if($days > 0)
		$string[] = $days." ".$lang["rs_days"];
	if($hours > 0)
		$string[] = $hours." ".$lang["rs_hours"];
	if($minutes > 0)
		$string[] = $minutes." ".$lang["rs_minutes"];
	if($seconds > 0)
		$string[] = $seconds." ".$lang["rs_seconds"];
	
	$string = implode(", ", $string);	
		
	if(!empty($label))	
		$string = $label.$string;
	return $string;
}



function whereCalled( $level = 1 ) {
    $trace = debug_backtrace();
    $file   = $trace[$level]['file'];
    $line   = $trace[$level]['line'];
    $object = $trace[$level]['object'];
    if (is_object($object)) { $object = get_class($object); }

    return "Where called: line $line of $object \n(in $file)";
}