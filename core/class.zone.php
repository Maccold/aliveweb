<?php

class Zone{
	
	var $debug = false;
	var $locale = "de_de";
	
	var $id = 0;
	var $name = "";
	var $label = "";
	var $location = "";
	var $expansion = 0;
	var $boss_num = 0;
	var $levelMin = 0;
	var $levelMax = 0;
	var $is_heroic = false;
	var $heroic_closed = false;
	var $raid = false;
	var $patch = "";
	var $intro = "";
	var $lore = "";
	
	var $partySizes = array();
	var $difficulties = array();
	var $bosses = array();
	var $floors = array();
	var $wings = array();
	var $data = array();
	
	function Zone($zoneID, $data, $debug = false){
		global $aDB, $data_npcs;
		
		$this->debug = $debug;
		
		$instanceRows = $aDB->select("SELECT * FROM armory_instance_template WHERE id = ?d", $zoneID);
		
		if(!$instanceRows){
			self::debug("no instance found");
			return false;
		}
		
		$this->name = $data["name"];
		$this->data = $data;
		
		foreach($instanceRows as $row){
			$this->id = (int) $row["id"];
			$this->expansion = (int) $row["expansion"];
			$this->boss_num = (int) $row["boss_num"];
			$this->is_heroic = (bool) $row["is_heroic"];
			$this->levelMin = (int) $row["levelMin"];
			$this->levelMax = (int) $row["levelMax"];
			$this->raid = (bool) $row["raid"];		
			$this->label = $row["label"];			
		
			$this->partySizes[] = $row["partySize"];
			$this->difficulties[] = $row["difficulty"];
		}
		unset($instanceRows);
		
		if(isset($data["location"]))
			$this->location = $data["location"];
		if(isset($data["patch"]))
			$this->patch = $data["patch"];
		if(isset($data["intro"]))
			$this->intro = $data["intro"];
		if(isset($data["lore"]))
			$this->lore = $data["lore"];
		if(isset($data["heroic"]))
			$this->is_heroic = (bool) $data["heroic"];
		if($data["heroic"] == "closed"){
			$this->heroic_closed = true;
		} 
	}
	
	function getZoneDetails(){
		global $aDB, $WSDB, $data_npcs;
		
		for($i = 1; $i <= 10; $i++){
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/images/zone/maps-large/".$this->label.$i."-large.jpg")){
				$this->floors[$i] = "Level ".$i;
			}
		}
		
		$bossRows = $aDB->select("SELECT * FROM armory_instance_data WHERE instance_id = ?d  ORDER BY `id` ASC", $this->id);
		
		$bosses = array();
		$boss_keys = array();
		
		if(!$bossRows)
			self::debug($aDB);
		
		foreach($bossRows as $row){
			$key = $row["key"];
			// ignore double entries
			if(!in_array($key, $boss_keys)){
				$boss_keys[] = $key;
				
				$bossData = array(
					"id" => ($row["type"] == "object") ? $row["name_id"] : $row["id"],
					"name" => $row["name_de_de"],
					"key" => $row["key"],
					"type" => $row["type"],
					"label" => $row["label"],
					"lootid_1" => $row["lootid_1"],
					"lootid_2" => $row["lootid_2"],
					"lootid_3" => $row["lootid_3"],
					"lootid_4" => $row["lootid_4"],
					"closed" => false,
				);
				
				if(isset($data_npcs[$bossData["id"]]) && $data_npcs[$bossData["id"]]["closed"] == true)
					$bossData["closed"] = true;
								
				//self::debug($bossData);
				
				if($row["type"] == "object"){
					// The data in armory has to be correct. There is no real way to check correct values for gameobjects.
					$bosses[$bossData["id"]] = $bossData;
					continue;
				}
				$bossRow = $WSDB->selectRow("SELECT entry, lootid, difficulty_entry_1,difficulty_entry_2,difficulty_entry_3 
					FROM creature_template 
					WHERE entry = ?d LIMIT 1", $bossData["id"]);
				
				if($bossRow){
					//self::debug("Boss found: ".$bossData["id"]);
					//self::debug($bossRow);
					$updateRequired = false;
					
					// lootid in armory table correct?
					if($bossRow["lootid"] != $bossData["lootid_1"]){
						$updateRequired = true;
						$bossData["lootid_1"] = $bossRow["lootid"];
					}
					
					
					$diffRows = $WSDB->query("SELECT entry, lootid FROM creature_template WHERE entry IN(?a);", array($bossRow["difficulty_entry_1"],$bossRow["difficulty_entry_2"],$bossRow["difficulty_entry_3"]));
					
					foreach($diffRows as $diffRow){
						// lootid in armory table correct?
						if($diffRow["entry"] == $bossRow["difficulty_entry_1"] && $diffRow["lootid"] != $bossData["lootid_2"]){
							$updateRequired = true;
							$bossData["lootid_2"] = $diffRow["lootid"];
						}
						if($diffRow["entry"] == $bossRow["difficulty_entry_2"] && $diffRow["lootid"] != $bossData["lootid_3"]){
							$updateRequired = true;
							$bossData["lootid_3"] = $diffRow["lootid"];
						}
						if($diffRow["entry"] == $bossRow["difficulty_entry_3"] && $diffRow["lootid"] != $bossData["lootid_4"]){
							$updateRequired = true;
							$bossData["lootid_4"] = $diffRow["lootid"];
						}
					}
					
					if($updateRequired){
						self::debug("Update armory_instance_data");
						//self::debug($bossData);
						$aDB->query("UPDATE armory_instance_data SET lootid_1 = ?d, lootid_2 = ?d, lootid_3 = ?d, lootid_4 = ?d WHERE `key` LIKE ?;", 
							$bossData["lootid_1"], $bossData["lootid_2"], $bossData["lootid_3"], $bossData["lootid_4"],
							$bossData["key"]);
					}
					else{
						self::debug("No update neccessary");
					}
					$bosses[$bossData["id"]] = $bossData;
				}
				else{
					self::debug("Creature Template ".$row["id"]." not found.");
				}
			}
			
		}
		self::debug("Bosses:");
		self::debug($bosses);			
		if(isset($this->data["wings"])){
			$wings = array();
			
			foreach($this->data["wings"] as $wing_name => $data){
				$wing = array("name" => $wing_name, "bosses" => array());
				
				foreach($data as $bossId){
					if(isset($bosses[$bossId]))
						$wing["bosses"][] = $bosses[$bossId];
				}
				
				$wings[] = $wing;
			}
			$this->wings = $wings;
		}
		elseif(isset($this->data["boss_order"])){
			foreach($this->data["boss_order"] as $bossId){
				//self::debug("Boss: $bossId");
				if(isset($bosses[$bossId]))
					$this->bosses[] = $bosses[$bossId];
			}
		}
		else{
			foreach($bosses as $boss){
				$this->bosses[] = $boss;
			}
		}
		
		
		self::debug($this);
		
	}
	
	function isHeroic(){
		return $this->is_heroic;
	}
	
	function getLevel(){
		if($this->levelMin < $this->levelMax)
			return $this->levelMin."-".$this->levelMax;
		else
			return $this->levelMax;	
	}
	
	function getHeroicLevel(){
		if($this->expansion == 0)
			return 60;
		if($this->expansion == 1)
			return 70;
		if($this->expansion == 2)
			return 80;
	}
	
	function getType(){
		global $lang_strings;
		if($this->raid)
			return $lang_strings["armory.dungeons.raid"];
		else
			return $lang_strings["armory.dungeons.dungeon"];
			
	}
	
	function getSize(){
		
		if(isset($this->data["sizes"])){
			return $this->data["sizes"];
		}
		
		if(count($this->partySizes) > 0){
			sort($this->partySizes);
			return implode("/", $this->partySizes);
		}
		else implode("",$this->partySizes);
	}
	
	function getLoot(){
		global $WSDB;
		
		$npcLootIds = array();
		$objectLootIds = array();
		$referenceIds = array();
		
		foreach($this->bosses as $boss){
			for($i = 1; $i < 5; $i++)
				if($boss["lootid_".$i] > 0){
					if($boss["type"] == "npc")
						$npcLootIds[] = $boss["lootid_".$i];
					if($boss["type"] == "object")
						$objectLootIds[] = $boss["lootid_".$i];
				}
		}
		
		// Find direct loot
		
	}
	
	function debug($string){
		if($this->debug == false)
			return;
		if(is_array($string) || is_object($string))
			echo "\n<!-- ".print_r($string,true)." -->";
		else
			echo "\n<!-- ".$string." -->";
	}
	

}