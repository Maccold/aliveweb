<?php

class GameObject{
	
	var $entry = 0;
	var $npc_id = 0;
	var $name;
	var $label = "";
	var $rank;
	var $type;
	var $levelMin;
	var $levelMax;
	var $closed = false;
	var $hp = 0;
	var $hp_hero = 0;
	var $locations = array();
	var $instances = array();
	var $locationName = "";
	var $locationLabel = "";
	var $locationType = "";
	var $description = "";
	var $difficulty_entry_1 = "";
	var $difficulty_entry_2 = "";
	var $difficulty_entry_3 = "";

	function GameObject($entry){
		global $WSDB;
		
		$goTemplate = $WSDB->selectRow("
			SELECT entry, `type`, name, data1  
			FROM gameobject_template WHERE entry = ?d", $entry);
		if(!$goTemplate)
			return false;
		
		$this->entry = $goTemplate["entry"];
		$this->name = $goTemplate["name"];
		$this->type = "Kiste";
		
		if($this->entry != $this->originalId && $this->difficulty_entry_1 > 0){
			if($this->originalId == $this->difficulty_entry_1){
				$this->locationType = "25";
			}
			elseif($this->originalId == $this->difficulty_entry_2){
				$this->locationType = "10 (Heroisch)";
			}
			elseif($this->originalId == $this->difficulty_entry_3){
				$this->locationType = "25 (Heroisch)";
			}
		}
		elseif($this->difficulty_entry_1 > 0){
			$this->locationType = "10";
		}
		
	}

	function getName(){
		
		if(!empty($this->locationLabel) && !empty($this->label)){
			if($this->npc_id > 0)
				return '<a href="/game/zone/'.$this->locationLabel.'/'.$this->label.'/" data-npc="'.$this->npc_id.'"> <strong>'.$this->name.'</strong> </a>';
			else
				return '<a href="/game/zone/'.$this->locationLabel.'/'.$this->label.'/"> <strong>'.$this->name.'</strong> </a>';
		} 
		else {	
			if($this->npc_id > 0)
				return '<span data-npc="'.$this->npc_id.'"> <strong>'.$this->name.'</strong> </span>';
			else
				return ' <strong>'.$this->name.'</strong> ';
		}
	}
	function getType(){
		return $this->type;
	}
	
	function getRank(){
		global $lang_strings;
		
		if(isset($lang_strings["armory.creature.rank.".$this->rank]))
			return $lang_strings["armory.creature.rank.".$this->rank];
		return "";
	}
	
	function getFansiteLink(){
		if($this->npc_id > 0)
			return '<a href="javascript:;" data-fansite="npc|'.$this->npc_id.'|'.$this->name.'" class="fansite-link float-right"> </a>';
		else
			return '<a href="javascript:;" data-fansite="object|'.$this->entry.'|'.$this->name.'" class="fansite-link float-right"> </a>';			
	}
	
	function getInstanceData(){
		global $aDB;
		
		$data = $aDB->selectRow("
			SELECT instance_id, name_id, at.map, ad.label as `npc_label`, at.label as `instance_label`, at.name_de_de 
			FROM armory_instance_data ad JOIN armory_instance_template at ON( ad.instance_id = at.id ) 
			WHERE ad.id = ?d OR heroic_id = ?d", $this->entry, $this->entry);
		
		if($data){
			$this->label = $data["npc_label"];
			
			$data["name_de_de"] = str_replace("(10)","",$data["name_de_de"]);
			$data["name_de_de"] = str_replace("(25)","",$data["name_de_de"]);
			$this->label = $data["npc_label"];
			$this->npc_id = $data["name_id"];
			
			$this->locationLabel = $data["instance_label"];
		
			$instance = array(
				"name" => $data["name_de_de"],
				"id" => $data["instance_id"],
				"label" => $data["instance_label"],
			);
			
			$this->locations[$data["map"]] = $instance;
		}
		else{
			debug("No data for object found", $aDB);
		}
	}
	
	function GetLocations(){
		global $WSDB, $aDB;
		
		$map_names = array();
		$raw_names = array();
		$map_spawns = $WSDB->select("SELECT map, spawnMask FROM gameobject WHERE id = ?d GROUP by map;", $this->entry);
		
		if(!$map_spawns){
			debug("No Map Data");
			return "";
		}
		
		foreach($map_spawns as $spawn){
			$map_id = $spawn["map"];
			$spawnMask = $spawn["spawnMask"];
			
			$map_name = $aDB->selectCell("SELECT name_de_de FROM armory_maps WHERE id = ?d", $map_id);
			$instance_data = $aDB->selectRow("SELECT id,label FROM armory_instance_template WHERE map = ?d", $map_id);
			
			if($map_name){
				$map = array(
					"name" => $map_name,
				);
				if($instance_data){
					$map["id"] = $instance_data["id"];
					$map["label"] = $instance_data["label"];
				}
				
				$spawnTypes = array();
				if($spawnMask & 1)
					$spawnTypes[] = "10";
				if($spawnMask & 2)
					$spawnTypes[] = "25";
				if($spawnMask & 4)
					$spawnTypes[] = "10 (Heroisch)";
				if($spawnMask & 8)
					$spawnTypes[] = "25 (Heroisch)";
				
				foreach($spawnTypes as $type){
					debug("Spawn Type $type");
					if(isset($map["id"])){
						$map_names[] = 	'<a href="/game/zone/'.$map["label"].'/" data-zone="'.$map["id"].'"> '.$map_name.' </a> '.$type;
					}
					else{
						$map_names[] = 	$map_name;
					}
					$raw_names[] = 	$map_name;
				}
				
			}
		}
		
		$this->locationName = implode(",",$raw_names);
		
		return implode(", ",$map_names);
	}
	
}