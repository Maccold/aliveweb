<?php

if(false){ 
	include("defines.php"); 
	include("class.item.prototype.php"); 
}

class Item{
		
	var $db       = false;			// Char DB
	var $debug = false;
	var $isCached = false;
	
	var $entry    = -1;
	var $equipped = false;
	var $loaded   = false;
	var $m_bag    = 0;
	var $m_guid   = false;			// GUID of the Item
	var $m_ench   = array();
	var $m_owner  = 0;
	var $m_proto  = false;
	var $m_server = 0;
	var $m_slot   = 0;
	var $m_socketInfo = array();
	var $m_values = false;
	var $tc_data  = false;
	var $tc_ench  = false;
	var $locale = "de_de";
	
	var $name = "";
	var $icon = "";
	var $heroic = 0;
	var $stackable = 1;
	var $overallQualityId = 0;
	var $instanceBound = "";
	var $conjured = "";
	var $bonding = "";
	var $counterpartId = 0;
	var $counterpartName = "";
	var $counterpartFaction = 0;
	var $counterpartIcon = 0;
	var $sources = array();	
	var $sourceTypeCount = array(
		"creature" => 0, 
		"object" => 0, 
		"vendor" => 0, 
		"quest" => 0, 
		"crafting" => 0, 
		"achievement" => 0, 
	);	
	var $socketCount = 0;
	
	
	function Item($entry = -1, $debug = false){
		$this->entry = $entry;
		$this->debug = $debug;
	}
	
	function debug($label, $data = ""){
		if($this->debug == false)
			return;
		
		$string = "";
		
		
		
		if(is_array($label) || is_object($label))
			$string .= " ".print_r($label,true)." ";
		else
			$string .= " $label ";
		
		if($data === ""){
		}
		elseif(is_array($data) || is_object($data))
			$string .= " ".print_r($data,true)." ";
		else
			$string .= " $data ";
		
		echo "\n<!-- ".$string." -->";
	}
	
	function getCache(){
		global $DataDB;
		
		$cacheRow = $DataDB->selectRow("SELECT * FROM item_cache WHERE entry = ?d AND type = ?;", $this->entry, CACHETYPE_TOOLTIP);
			
		if(!$cacheRow){
			return false;
		}
		else{
			if(empty($cacheRow["html"]))
				return false;
			$this->isCached = true;
			return $cacheRow["html"];
		}
	}
	
	function getData($key){
		global $lang_strings;
		
		if(empty($this->$key))
			return;
			
		switch($key){
			case "bonusStrength":
			case "bonusAgility":
			case "bonusStamina":
			case "bonusIntellect":
			case "bonusSpirit":
				echo '<li>+'.$this->$key." ".$lang_strings["armory.item-tooltip.".$key].'</li>';
				break;
			case "bonusDefenseSkillRating":
			case "bonusDodgeRating":
			case "bonusParryRating":
			case "bonusBlockRating":
			case "bonusHitMeleeRating":
			case "bonusHitRangedRating":
			case "bonusHitSpellRating":
			case "bonusCritMeleeRating":
			case "bonusCritRangedRating":
			case "bonusCritSpellRating":
			case "bonusHitTakenMeleeRating":
			case "bonusHitTakenRangedRating":
			case "bonusHitTakenSpellRating":
			case "bonusCritTakenMeleeRating":
			case "bonusCritTakenRangedRating":
			case "bonusCritTakenSpellRating":
			case "bonusHasteMeleeRating":
			case "bonusHasteRangedRating":
			case "bonusHasteSpellRating":
			case "bonusHitRating":
			case "bonusCritRating":
			case "bonusHitTakenRating":
			case "bonusCritTakenRating":
			case "bonusResilienceRating":
			case "bonusHasteRating":
			case "bonusSpellPower":
			case "bonusAttackPower":
			case "bonusRangedAttackPower":
			case "bonusFeralAttackPower":
			case "bonusManaRegen":
			case "bonusArmorPenetration":
			case "bonusExpertiseRating":
			case "bonusBlockValue":
			case "bonusHealthRegen":
			case "bonusSpellPenetration":
			case "bonusDodgeRating":
			case "bonusDodgeRating":
			case "bonusDodgeRating":
			case "bonusDodgeRating":
				echo '<li class="color-tooltip-green">'.$lang_strings["armory.item-tooltip.".$key].' '.$this->$key.'</li>'; 
				break;
			case "durabilityCurrent":
				echo '<li>'.$lang_strings["armory.item-tooltip.durability"]." ".$this->durabilityCurrent." / ".$this->durabilityMax.'</li>';
				break;
			case "socketData":
				$allSocketsMatch = true;
				//debug("Sockets",$this->sockets);
				foreach($this->sockets as $socket){
					if(!isset($socket["color"]))
						continue;
					if(empty($socket["enchant"])){
						$allSocketsMatch = false;
						echo '
					<li class="color-d4">
						<span class="icon-socket socket-'.$socket["color"].'">
							<span class="empty"></span>
							<span class="frame"></span>
						</span>
						'.$socket["label"].'
						<span class="clear"><!-- --></span>
					</li>';
					}
					else{
						GetItemIcon($socket["icon"], 18);
						
						if($socket["match"] == false)
							$allSocketsMatch = false;
						echo '
					<li>
						<span class="icon-socket socket-'.$socket["color"].'">
							<a href="http://portal.wow-alive.de/item/'.$socket["gem"].'" class="gem">
								<img src="/images/icons/18/'.$socket["icon"].'.jpg" alt="" />
								<span class="frame"></span>
							</a>
						</span>
						'.$socket["enchant"].'
						<span class="clear"><!-- --></span>
					</li>';
					}
				}
				if(!empty($this->socketMatchEnchant)){
					if($allSocketsMatch)
						echo '<li class="color-tooltip-green">'.$lang_strings["armory.item-tooltip.socket-bonus"].": ".$this->socketMatchEnchant.'</li>';
					else
						echo '<li class="color-d4">'.$lang_strings["armory.item-tooltip.socket-bonus"].": ".$this->socketMatchEnchant.'</li>';
				}
				break;
			case "enchant":
				echo '<li class="color-tooltip-green">'.$this->$key.'</li>';
				break;
			case "desc":
				echo '<li class="color-tooltip-yellow">'.$this->$key.'</li>';
				break;
			case "startQuestId":
				if($this->startQuestId > 0)
					echo "<li>Dieser Gegenstand beginnt eine Quest.</li>";
				break;
			case "armor":
			{
				if($this->armorBonus)
					echo '<li class="color-tooltip-green">'.$this->$key." ".$lang_strings["armory.item-tooltip.armor"]."</li>";
				else
					echo "<li>".$this->$key." ".$lang_strings["armory.item-tooltip.armor"]."</li>";
				
			} break;
			case "inventoryType":
			{
				if($this->inventoryType == 18)
					echo "<li>".$this->containerSlots." Platz ".$this->subclassName."</li>";
				else if($this->inventoryType != 0){
					echo "<li>";
					if($this->classId == 6){
						echo "Projektil";
					}
					else{
						if(!empty($this->subclassName))
							echo '<span class="float-right">'.$this->subclassName.'</span>';
						echo GetItemSlotTextByInvType($this->inventoryType);
					}
					echo "</li>";
				}
			} break;
			case "damageData":
			{
				if($this->classId == 6){
					echo "<li>".$lang_strings["armory.item-tooltip.adds"]." ".(($this->damageMin + $this->damageMax) * 0.5)." ".$lang_strings["armory.item-tooltip.dps"]."</li>";
				}
				else {
					echo "<li>";
					echo $this->damageMin." - ".$this->damageMax." ";
					switch($this->damageType){
						case 0: echo $lang_strings['armory.item-tooltip.damage']; break;
						case 1: echo $lang_strings['armory.item-tooltip.holy-damage']; break;
						case 2: echo $lang_strings['armory.item-tooltip.fire-damage']; break;
						case 3: echo $lang_strings['armory.item-tooltip.nature-damage']; break;
						case 4: echo $lang_strings['armory.item-tooltip.frost-damage']; break;
						case 5: echo $lang_strings['armory.item-tooltip.shadow-damage']; break;
						case 6: echo $lang_strings['armory.item-tooltip.arcane-damage']; break;						
					}
					
					if(isset($this->speed))
						echo '<span class="float-right">'.$lang_strings['armory.item-tooltip.speed']." ".round(($this->speed * 100 / 100),2).'</span>';
					echo "</li>";
					
					if(isset($this->dps)){
						echo '<li>('.round($this->dps,1)." ".$lang_strings["armory.item-tooltip.dps"].')</li>';
					}
				}
			} break;
			case "maxCount":
				if(!empty($this->uniqueEquippable))
					echo '<li>'.$this->uniqueEquippable.')</li>';
				if($this->maxCount > 1)
					echo '<li>Einzigartig ('.$this->{$key}.')</li>';
				else
					echo '<li>Einzigartig</li>';
				break;
			case "glyphType":
				echo '<li class="color-tooltip-blue">'.$this->{$key}.'</li>';
				break;
			case "heroic":
				echo '<li class="color-tooltip-green">'.$this->{$key}.'</li>';
				break;
			case "spellData":
			{
				foreach($this->spellData as $spell){
					switch($spell["trigger"]){
						case 0: $trigger = $lang_strings['armory.item-tooltip.use']; break;
						case 1: $trigger = $lang_strings['armory.item-tooltip.equip']; break;
						case 2: $trigger = $lang_strings['armory.item-tooltip.chance-on-hit']; break;
						case 6: $trigger = $lang_strings['armory.item-tooltip.use']; break;
					}
					echo '<li class="color-tooltip-green">'.$trigger.': '.$spell["desc"].'</li>';	
					if($spell["charges"] > 0)
						echo '<li class="color-tooltip-green">'.$spell["charges"].' '.$lang_strings["armory.item-tooltip.charges"].'</li>';	
					if(count($spell["reagent"])){
						echo '<li>'.$lang_strings['armory.item-tooltip.requires']." ";
						$first = true;
						foreach($spell["reagent"] as $reagent){
							
							if(!$first)
								echo ", ";
							echo $reagent["name"];
							if($reagent["reagent"] > 0)
								echo "(".$reagent["reagent"].")";	
							
							$first = false;
						}
						echo '</li>';
					}
				}
				
			} break;
			case "setData":
			{
				
			} break;
			default:
			{
				echo '<li>'.$this->{$key}.'</li>';
				break;
			}
		}
	}
		
	 /**
     * Create item tooltip with provided options
     *  - $parent: used for items that created by spells (displays under patterns/recipes, etc.)
     *  - $comparison: used for dual tooltips (if user logged in and primary character is selected)
     * @category Item class
     * @access   public
     * @param    Characters $characters
     * @param    bool $parent = false
     * @param    bool $comparison = false
     **/
    public function CreateTooltip($item_data = array(), $parent = false, $comparison = false){
		global $WSDB, $aDB, $lang_strings;
		
		$characters = false;
		$isCharacter = false;
		
		$item_data_default = array(
			"enchant" => 0,
			"g0" => 0,
			"g1" => 0,
			"g2" => 0,
			"d" => -1,
			"set" => "",
		);
		
		foreach($item_data_default as $key => $value){
			if(isset($item_data[$key]))
				$isCharacter = true;
			else
				$item_data[$key] = $value;
		}
		
		$proto = new ItemPrototype();
		$proto->LoadItem($this->entry);
		if(!$proto) {
			output_message( "debug", '?s : unable to find item with entry #?d', __METHOD__, $this->entry);
			self::debug("item not found");
			return false;
		}
		
		if($name = $WSDB->selectCell("SELECT name_loc3 FROM locales_item WHERE entry = ?d", $this->entry))
			$this->name = $name;
		else	
			$this->name = utf8_decode($proto->name);
		$this->icon = self::getItemIcon($this->entry, $proto->displayid);
		
		if($proto->Flags & ITEM_FLAGS_HEROIC) {
			$this->heroic = $lang_strings["armory.item-info.heroic"];
		}
		
		$this->overallQualityId = $proto->Quality;

		if($proto->stackable > 1)
			$this->stackable = $proto->stackable;
	
		// Map
        if($proto->Map > 0) {
			$mapName = $aDB->selectCell("SELECT `name_{$this->locale}` FROM `armory_maps` WHERE `id` = {$proto->Map}");
        	$this->instanceBound = $mapName;
        }
		if($proto->Flags & ITEM_FLAGS_CONJURED) {
            $this->conjured = "Herbeigezaubert";
        }
		
		switch($proto->bonding){
			case 0:	$this->bonding = ""; break;
			case 1:	$this->bonding = "Wird beim Aufheben gebunden"; break;
			case 2:	$this->bonding = "Wird beim Anlegen gebunden"; break;
			case 3:	$this->bonding = "Wird bei Benutzung gebunden"; break;
			case 4:
			case 5:	$this->bonding = "Questgegenstand";
		}
		
		$this->maxCount = $proto->maxcount;
    	if($proto->Flags & ITEM_FLAGS_UNIQUE_EQUIPPED){
			$this->maxCount = "Einzigartig anlegbar";
		}
		
		if($proto->SellPrice > 0){
			$this->sellPrice = $lang_strings["armory.item-info.label.sellPrice"].": ".FormatMoney($proto->SellPrice);
		}
		
		if($proto->startquest > 0) {
            $this->startQuestId = $proto->startquest;
        }
     	$this->classId = $proto->class;
        $this->inventoryType = $proto->InventoryType;
        $this->subclassName = self::GetItemSubTypeInfo(true, array('class' => $proto->class, 'subclass' => $proto->subclass));
        
		if($proto->class== ITEM_CLASS_CONTAINER) {
			$this->containerSlots = $proto->ContainerSlots;
        }
		
		if($proto->class== ITEM_CLASS_GLYPH) {
			$this->glyphType = "Glyphe";
        	
			if(substr_count($this->icon, "major") > 0)
				$this->glyphType = "Erhebliche Glyphe";
        	else
				$this->glyphType = "Geringe Glyphe";
        }
		
		if($proto->class == ITEM_CLASS_WEAPON) {
        	$this->damageData = true;
			$this->damageType = 0;
            
			// Damage
            $minDmg = $proto->Damage[0]['min'];
            $maxDmg = $proto->Damage[0]['max'];
            $dps = null;
            
			$this->damageMin = $minDmg;
            $this->damageMax = $maxDmg;
            
			$this->speed = round($proto->delay / 1000, 2);
            
			for($jj = 0; $jj <= 1; $jj++) {
                $d_type = $proto->Damage[$jj]['type'];// $data['dmg_type' . $jj];
                $d_min  = $proto->Damage[$jj]['min']; // $data['dmg_min' . $jj];
                $d_max  = $proto->Damage[$jj]['max']; // $data['dmg_max' . $jj];
                if(($d_max > 0) && ($proto->class != ITEM_CLASS_PROJECTILE)) {
                    $delay = $proto->delay / 1000;
                    if($delay > 0) {
                        $dps = $dps + round(($d_max + $d_min) / (2 * $delay), 1);
                    }
                    if($jj > 1) {
                        $delay = 0;
                    }
               	}
            }
            if($dps != null) {
    	        $this->dps = $dps;
            }
        }
		
		// Projectile DPS
        if($proto->class == ITEM_CLASS_PROJECTILE) {
        	$this->damageData = true;
            if($proto->Damage[0]['min'] > 0 && $proto->Damage[0]['max'] > 0) {
            
				$this->damageType = $proto->Damage[0]['type'];
		
				$this->damageMin = $proto->Damage[0]['min'];
	            $this->damageMax = $proto->Damage[0]['max'];
     		}
        }
		
		// Gem properties
        if($proto->class == ITEM_CLASS_GEM && $proto->GemProperties > 0) {
            
			$GemSpellItemEcnhID = $aDB->selectCell("SELECT `spellitemenchantement` FROM `armory_gemproperties` WHERE `id`=?d", $proto->GemProperties);
            $GemText = $aDB->selectCell("SELECT ?# FROM `armory_enchantment` WHERE `id`=?d", "text_".$this->locale, $GemSpellItemEcnhID);
            if($GemText) {
        		$this->gemProperties = $GemText;
            }
        }
		
		if($proto->block > 0) {
			$this->blockValue = $proto->block." ".$lang_strings["armory.item-tooltip.block"];
        }
		
		// Resistance
		$resists = array("fire", "nature", "frost", "shadow", "arcane");
		foreach($resists as $res){
			if($proto->{$res."_res"} > 0)
				$this->{$res."Resist"} = "+".$proto->{$res."_res"}." ".$lang_strings["armory.item-tooltip.".$res."-resistance"];
		}
		
		for($i = 0; $i < MAX_ITEM_PROTO_STATS; $i++) {
            $key = $i + 1;
            if($proto->ItemStat[$i]['type'] > 0 && $proto->ItemStat[$i]['value'] > 0) {
				$bonus_template = self::GetItemBonusTemplate($proto->ItemStat[$i]['type']);
				
				$this->$bonus_template = $proto->ItemStat[$i]['value'];
	 		}
        }
		
		$this->armor = $proto->armor;
		if($proto->ArmorDamageModifier > 0) {
        	$this->armorBonus = 1;
        }
		
		$itemSlotName = GetItemSlotTextByInvType($proto->InventoryType);
        if(!$parent && $isCharacter && $itemSlotName != null) {
            //$enchantment = $characters->GetCharacterEnchant($itemSlotName);
            $enchantment = $item_data["enchant"];
            if($enchantment > 0) {
                $this->enchant = $aDB->selectCell("SELECT ?# FROM `armory_enchantment` WHERE `id`=?d LIMIT 1", "text_".$this->locale, $enchantment);
            }
        }
		
		// Random property
        if($proto->RandomProperty > 0 || $proto->RandomSuffix > 0) {
            if(!$isCharacter) {
				$this->randomEnchantData = $lang_strings["armory.item-tooltip.random-enchant"];
            }
			/*
            else {
                if($itemSlotName) {
                    $rPropInfo = $this->GetRandomPropertiesData($characters->GetGUID(), $characters->GetEquippedItemGuidBySlot($itemSlotName), false, array('RandomProperty' => $proto->RandomProperty, 'RandomSuffix' => $proto->RandomSuffix));
                }
                else {
                    $rPropInfo = $this->GetRandomPropertiesData($this->entry, $characters->GetGUID(), 0, false);
                }
                if($isCharacter && !$parent && is_array($rPropInfo)) {
                    
					$this->randomEnchantSuffix = $rPropInfo['suffix'];
					
                    if(is_array($rPropInfo['data'])) {
                        if(Utils::IsWriteRaw()) {
                            foreach($rPropInfo['data'] as $randProp) {
                                $xml->XMLWriter()->writeRaw('<enchant>');
                                $xml->XMLWriter()->writeRaw($randProp);
                                $xml->XMLWriter()->writeRaw('</enchant>'); //enchant
                            }
                        }
                        else {
                            foreach($rPropInfo['data'] as $randProp) {
                                $xml->XMLWriter()->startElement('enchant');
                                $xml->XMLWriter()->text($randProp);
                                $xml->XMLWriter()->endElement(); //enchant
                            }
                        }
                    }
                    $xml->XMLWriter()->endElement(); //randomEnchantData
                }
            }*/
       	}
		
		// Socket data
        // If there's no character, check $proto->Socket[X]
        if(!$isCharacter) {
			$this->sockets = array();
            for($i = 0; $i < 3; $i++) {
                if(isset($proto->Socket[$i]['color']) && $proto->Socket[$i]['color'] > 0) {
                	$this->socketData = true;
					
					$socket_data = array(
						"color" => $proto->Socket[$i]['color'], 
						"label" => self::GetSocketColorString($proto->Socket[$i]['color']),
					);
					if(is_array($socket_data)) {
						$this->sockets[$i] = $socket_data;
                    }
                }
            }
        }
       	elseif($isCharacter) {
        	$gems = array(
                'g0' => self::GetSocketInfoByGem($item_data["g0"]),
                'g1' => self::GetSocketInfoByGem($item_data["g1"]),
                'g2' => self::GetSocketInfoByGem($item_data["g2"])
            );
                
            for($i = 0; $i < 3; $i++) {
                $index = $i+1;
                $socket_data = array();
                if(isset($gems['g' . $i]["item"]) && $gems['g' . $i]["item"] > 0) {
                	$this->socketData = true;
                    $socket_data = array(
                        'gem'    => $gems['g' . $i]['item'],
                    	'color'   => ($proto->Socket[$i]['color'] > 0) ? $proto->Socket[$i]['color'] : 14,
                    	'label'   => self::GetSocketColorString($proto->Socket[$i]['color']),
                    	'enchant' => $gems['g' . $i]['enchant'],
                        'icon'    => $gems['g' . $i]['icon'],
                        'match' => false,
                    );
                    if($socket_data["color"] == 14){
                        $socket_data['match'] = true;
                    }
                    elseif(self::IsGemMatchesSocketColor($gems['g' . $i]['color'], ($proto->Socket[$i]['color']) ? $proto->Socket[$i]['color'] : -1)) {
                        $socket_data['match'] = true;
                    }
                }
                else {
                    if(isset($proto->Socket[$i]['color']) && $proto->Socket[$i]['color'] > 0) {
                    	$this->socketData = true;
					
						$socket_data = array(
							"color" => $proto->Socket[$i]['color'], 
							"label" => self::GetSocketColorString($proto->Socket[$i]['color']),
						);
						if(is_array($socket_data)) {
							$this->sockets[$i] = $socket_data;
                    	}
                    }
                }
            
            	$this->sockets[$i] = $socket_data;
            }
       	}
       	//self::debug("Sockets",$this->sockets);
       	
		if($proto->socketBonus > 0) {
            $bonus_text = $aDB->selectCell("SELECT ?# FROM `armory_enchantment` WHERE `id`=?d", "text_".$this->locale, $proto->socketBonus);
			$this->socketMatchEnchant = $bonus_text;
        }
		
        // Durability
        if($isCharacter && $item_data["d"] >= 0) {
            $item_durability = array('current' => $item_data["d"], 'max' => $proto->MaxDurability);
        }
        else {
            $item_durability = array('current' => $proto->MaxDurability, 'max' => $proto->MaxDurability);
        }
		if(is_array($item_durability) && $item_durability['current'] > 0) {
			$this->durabilityCurrent = (int) $item_durability['current'];
        	$this->durabilityMax = (int) $item_durability['max'];
        }
		
        $allowable_classes = $this->AllowableClasses($proto->AllowableClass);
        if($allowable_classes) {
        	$this->allowableClasses = $lang_strings["armory.item-tooltip.classes"].": ".implode(", ", $allowable_classes);
        }
        
        $allowable_races = $this->AllowableRaces($proto->AllowableRace);
        if($allowable_races) {
        	$this->allowableRaces = $lang_strings["armory.item-tooltip.races"].": ".implode(", ", $allowable_races);
        }
		
		// Requirements		
        if($proto->RequiredSkill > 0 && $reqName = $aDB->selectCell("SELECT ?# FROM `armory_skills` WHERE `id`=?d", "name_".$this->locale, $proto->RequiredSkill)) {
        	$this->requiredSkillId = $proto->RequiredSkill;
			$this->requiredSkill = $lang_strings["armory.item-tooltip.requires"]." ".$reqName;
			if(!empty($proto->RequiredSkillRank) && $proto->RequiredSkillRank > 0)
				$this->requiredSkill .= " (".$proto->RequiredSkillRank.")";
        }
		
        if($proto->requiredspell > 0 && $spellName = $aDB->selectCell("SELECT ?# FROM `armory_spell` WHERE `id`=?d", 
			"SpellName_en_gb", $proto->requiredspell)) {
			$this->requiredAbility = $lang_strings["armory.item-tooltip.requires"]." ".$spellName;
        }
		
        if($proto->RequiredReputationFaction > 0 && $factionName = $aDB->selectCell("SELECT ?# FROM `armory_faction` WHERE `id`=?d", 
			"name_".$this->locale, $proto->RequiredReputationFaction)) {    
			$this->requiredFaction = $lang_strings["armory.item-tooltip.requires"]." ".htmlentities($factionName)." - ".getFactionStanding($proto->RequiredReputationRank);
        }
		
		if($this->requiredLevel > 0)
			$this->requiredLevel = $lang_strings["armory.item-tooltip.requires-level"]." ".$proto->RequiredLevel;
		if($proto->ItemLevel > 0) {
        	$this->itemLevel = $lang_strings["armory.item-info.itemlevel"]." ".$proto->ItemLevel;
		}
		
   		// Item set
   		if($proto->itemset > 0) {
            debug("proto-itemset: ",$proto->itemset);
			$this->setData = 1;
			$itemsetName = $aDB->selectCell("SELECT ?# FROM `armory_itemsetinfo` WHERE `id`=?d", "name_".$this->locale, $proto->itemset);
            $this->itemsetName = $itemsetName;
			$this->itemsetItems = array();
			$this->numSetPieces = 0;
			
			$equippedSetItems = explode(",", $item_data["set"]);
			debug("equippedSetItems",$equippedSetItems);
			
            $setdata = $aDB->selectRow("SELECT * FROM `armory_itemsetinfo` WHERE `id`=?d", $proto->itemset);
            
			if($this->IsMultiplyItemSet($proto->itemset)) {
				debug("IsMultiplyItemSet");
				// Get itemset info from other table (armory_itemsetdata)
                $currentSetData = $aDB->select("SELECT * FROM `armory_itemsetdata` WHERE `original`=?d", $proto->itemset);
                if(is_array($currentSetData)) {
                	debug("CurrentsetData", $currentSetData);
                    $activeSetInfo = array();
                    $basicSetData = $currentSetData[0];
                    foreach($currentSetData as $iSet) {
                        for($i = 1; $i < 6; $i++) {
                            if($isCharacter && in_array($iSet['item' . $i],$equippedSetItems)) {
                                $activeSetInfo['item' . $i] = $iSet['item' . $i];
                            }
                        }
                    }
			    	for($i = 1; $i < 6; $i++) {
                    	
						if(isset($activeSetInfo['item' . $i])) {
							
							if($isCharacter && in_array($activeSetInfo['item' . $i], $equippedSetItems)){
								$equipped = true;
								$this->numSetPieces++;
							}
							$this->itemsetItems[] = array(
								"name" => (GetItemName($activeSetInfo['item' . $i])),
								"equipped" => ($equipped) ? 1 : 0,
							);
						}
						else{
							$this->itemsetItems[] = array(
								"name" => (GetItemName($basicSetData['item' . $i])),
							);
						}
                    }
                }
            }
            else {
				for($i = 1; $i < 10; $i++) {
					if(isset($setdata['item' . $i]) && IsItemExists($setdata['item' . $i])) {
						if($isCharacter && in_array($setdata['item' . $i], $equippedSetItems)){
							$equipped = true;
							$this->numSetPieces++;
						}
						$this->itemsetItems[] = array(
							"name" => GetItemName($setdata['item' . $i]),
							"equipped" => ($equipped) ? 1 : 0,
						);
					}
				}
            }
			
            $itemsetbonus = self::GetItemSetBonusInfo($setdata);
			self::debug($itemsetbonus);
            $this->itemsetBonus = array();
			if(is_array($itemsetbonus)) {
				$this->itemsetBonus = $itemsetbonus;
			}
			
			$this->itemsetCount = count($this->itemsetItems);
			$this->meetsSkillReq = empty($this->requiredSkill) ? true : false;
			
        }
        
        $spellData = 0;
        $spellInfo = false;
        $this->spellData = array();
		
		for($i = 0; $i < 5; $i++) {
            if($proto->Spells[$i]['spellid'] > 0) {
                $spellData = 1;
				$spellId = $proto->Spells[$i]['spellid'];
                $spell_tmp = $aDB->selectRow("SELECT * FROM `armory_spell` WHERE `id`=?d", $spellId);
                $tmp_locale = 'en_gb';
                
                //debug("Spell $i", $spell_tmp);
                
				$wowhead = file_get_contents("http://de.wowhead.com/spell=".$spellId."&power");
					
				if(preg_match("@<span class=\"q\">([^<]+)</span>@", $wowhead, $matches)){
					$spell_tmp['Description_en_gb'] = $matches[1];
				}
				
				$spell_tmp['Description_en_gb'] = str_replace("\'","'",$spell_tmp['Description_en_gb']);
				
				
				$spellInfo = $this->SpellReplace($spell_tmp, self::ValidateSpellText($spell_tmp['Description_en_gb']));
                if($spellInfo) {
                    $spellData = 2;
                    $spellInfo = str_replace('&quot;', '"', $spellInfo);
                    
					$this->spellData[] = array(
						"trigger" => $proto->Spells[$i]['trigger'],
						"desc" => $spellInfo,
					);
                }
            }
        }
		
        if($spellData == 1 && $proto->description != null) {
         
			$this->spellData[] = array(
				"trigger" => 6,
				"desc" => utf8_decode($proto->description),
			);

            if(!$parent) {
                for($k = 1; $k < 4; $k++) {
                    if($spell_tmp['EffectItemType_' . $k] > 0 && IsItemExists($spell_tmp['EffectItemType_' . $k])) {
                        /*
						$xml->XMLWriter()->startElement('itemTooltip');
                        self::ItemTooltip($spell_tmp['EffectItemType_' . $k], $xml, $characters, true);
                        $xml->XMLWriter()->endElement(); //itemTooltip
                        */
						$spellreagents = $this->GetSpellItemCreateReagentsInfo($spell_tmp['EffectItemType_' . $k]);
                        $this->spellReagents = array();
						if(is_array($spellreagents)) {
                            foreach($spellreagents as $reagent) {
                        		$this->spellReagents[] = array(
									"reagent" => $reagent["count"],
									"name" => $reagent["name"],
								);
                            }
                        }
                    }
                }
            }
        }

        if($proto->description != null && $proto->description != $spellInfo && $spellData != 1) {
            $this->desc = (utf8_decode($proto->description));
        }
		
		$this->source = "sourceType.none";
		/*
		if(!$parent) {
			$itemSource = $this->GetItemSource($this->entry);
			//echo "Source:".print_r($itemSource,true);
            if(is_array($itemSource)) {
            	$this->source = $itemSource;
			
            	if($itemSource['value'] == 'sourceType.vendor' && $reqArenaRating = self::IsRequiredArenaRating($this->entry)) {
            		$this->requiredPersonalRating = $lang_strings["armory.item-tooltip.requires.personalarena"]. " ". $reqArenaRating;
            	}
			}
        }*/
        
	}
	
	function GetFactionCounterpart(){
		global $WSDB, $aDB;
		
		$counterRow = $WSDB->selectRow("SELECT * FROM player_factionchange_items WHERE alliance_id = ?d OR horde_id = ?d", $this->entry, $this->entry);
		
		if($counterRow){
			
			if($counterRow["alliance_id"] == $this->entry){
				$this->faction = FACTION_ALLIANCE;
				$this->counterpartId = $counterRow["horde_id"];
				$this->counterpartFaction = FACTION_HORDE;
			}
			else{
				$this->faction = FACTION_HORDE;
				$this->counterpartFaction = FACTION_ALLIANCE;
				$this->counterpartId = $counterRow["alliance_id"];
			}
			
			$this->counterpartName = GetItemName($this->counterpartId);
			
			$displayId = $WSDB->selectCell("SELECT `displayid` FROM `item_template` WHERE `entry`=?d LIMIT 1", $this->counterpartId);
			$this->counterpartIcon = $aDB->selectCell("SELECT `icon` FROM `armory_icons` WHERE `displayid`=?d LIMIT 1", $displayId);
        
		}
		else{
			return false;
		}	
	
	}
	
	/**
     * Returns item source string (vendor, drop, chest loot, etc.)
     * @category Items class
     * @access   public
     * @param    int $item
     * @param    bool $areaDataOnly = false
     * @return   string
     **/
	public function GetAllItemSources() {
		global $WSDB, $aDB;
		
		$item_id = $this->entry;
		
		self::debug("Item: $item_id");
		
		$sources = array();
		
		
		// Loot
		$referenceIds = array(0);
		$referenceLoot = $WSDB->select("SELECT `entry` FROM `reference_loot_template` WHERE `item`= ?d;", $item_id);
		foreach($referenceLoot as $row){
			$refId = $row["entry"];
			$referenceIds[] = "-".abs($refId);
		}
		self::debug("Ref", $referenceIds);
		
		$creatureLoot = $WSDB->select("SELECT ct.`entry` FROM `creature_loot_template` cl JOIN creature_template ct ON (cl.entry = ct.lootid) 
			WHERE (`item`= ?d AND mincountOrRef > 0) OR (mincountOrRef IN(?a)) ;", $item_id, $referenceIds);
		foreach($creatureLoot as $row){
			$sources[] = array(
				"type" => "creature",
				"entry" => $row["entry"],
			);
			$this->sourceTypeCount["creature"]++;
		}
			
		$objectLoot = $WSDB->select("SELECT gt.`entry` FROM `gameobject_loot_template` as gl JOIN gameobject_template gt ON (gl.entry = gt.data1)
			WHERE (`item`= ?d AND mincountOrRef > 0) OR (mincountOrRef IN(?a)) ;", $item_id, $referenceIds);
		foreach($objectLoot as $row){
			$sources[] = array(
				"type" => "object",
				"entry" => $row["entry"],
			);
			$this->sourceTypeCount["creature"]++;	// Objekte und NPCs zählen zusammen
		}
		
		// Vendor
		$vendorLoot = $WSDB->select("SELECT `entry`, `ExtendedCost` FROM `npc_vendor` WHERE `item`=?d;", $item_id);
		//if(!$vendorLoot)
		//	self::debug("Vendor Loot", $WSDB);
		foreach($vendorLoot as $row){
			$sources[] = array(
				"type" => "vendor",
				"entry" => $row["entry"],
				"cost" => $row["ExtendedCost"],
			);
			$this->sourceTypeCount["vendor"]++;
		}
		
		// Quest Loot
		$questLoot = $WSDB->select("
			SELECT `entry` FROM `quest_template`
			WHERE 	`RewChoiceItemId1` = ?d OR `RewChoiceItemId2` = ?d OR `RewChoiceItemId3` = ?d OR 
					`RewChoiceItemId4` = ?d OR `RewChoiceItemId5` = ?d OR `RewChoiceItemId6` = ?d;", $item_id, $item_id, $item_id, $item_id, $item_id, $item_id);
		//if(!$questLoot)
		//	self::debug("Quest Loot", $WSDB);
		foreach($questLoot as $row){
			$sources[] = array(
				"type" => "quest",
				"entry" => $row["entry"],
			);
			$this->sourceTypeCount["quest"]++;
		}
		
		// Crafting
		$craftLoot = $aDB->select("SELECT `id` FROM `armory_spell` WHERE `EffectItemType_1`=?d OR `EffectItemType_2`=?d OR `EffectItemType_3`=?d;", $item_id, $item_id, $item_id);
		//if(!$craftLoot)
		//	self::debug("Craft Loot", $aDB);
		foreach($craftLoot as $row){
			$sources[] = array(
				"type" => "crafting",
				"entry" => $row["id"],
			);
			$this->sourceTypeCount["crafting"]++;
		}
		
		// Achievement
		$achLoot = $WSDB->select("SELECT `id` FROM `achievement_reward` WHERE `item`=?d;", $item_id);
		//if(!$achLoot)
		//	self::debug("Achievement Loot", $WSDB);
		foreach($achLoot as $row){
			$sources[] = array(
				"type" => "achievement",
				"entry" => $row["id"],
			);
			$this->sourceTypeCount["achievement"]++;
		}
		
		self::debug("Loot Data:",$sources);
		
		$this->sources = $sources;
		
		
	}
	
	function GetSources($type = "creature"){
		
		$n = 0;
		$sources = array();
		foreach($this->sources as $source_key => $source){
			if( $type == "creature" && $source["type"] == "creature"){
				$n++;
				$npc = new Npc($source["entry"]);
				$npc->getInstanceData();
				
				$source["n"] = $n;
				$source["entry"] = $npc->entry;
				$source["name"] = $npc->getName();
				$source["raw_name"] = $npc->name;
				$source["level"] = $npc->getLevel();
				$source["rank"] = $npc->getRank();
				$source["type"] = $npc->getType();
				//self::debug("NPC after InstanceData", $npc);
				$source["label"] = $npc->label;
				$source["locations"] = $npc->GetLocations();
				//self::debug("NPC after GetLocations", $npc);
				$source["locationName"] = $npc->locationName;
				$source["locationLabel"] = $npc->locationLabel;
				$source["fansite"] = $npc->getFansiteLink();
				
				$sources[] = $source;
			}
			elseif( $type == "creature" && $source["type"] == "object"){
				$n++;
				$object = new GameObject($source["entry"]);
				$object->getInstanceData();
				
				$source["n"] = $n;
				$source["name"] = $object->getName();
				$source["raw_name"] = $object->name;
				$source["entry"] = $object->entry;
				$source["type"] = $object->getType();
				$source["label"] = $object->label;
				$source["locations"] = $object->GetLocations();
				$source["locationName"] = $object->locationName;
				$source["fansite"] = $object->getFansiteLink();
				
				$sources[] = $source;
				//self::debug("Source", $source);
			} 
			elseif( $type == "vendor" ){
				$n++;
				$npc = new Npc($source["entry"]);
			
				$source["n"] = $n;
				$source["entry"] = $npc->entry;
				$source["name"] = $npc->getNpcName();
				$source["raw_name"] = $npc->name;
				$source["level"] = $npc->getLevel();
				//self::debug("NPC after InstanceData", $npc);
				$source["locations"] = $npc->GetLocations();
				//self::debug("NPC after GetLocations", $npc);
				$source["locationName"] = $npc->locationName;
				$source["locationLabel"] = $npc->locationLabel;
				$source["fansite"] = $npc->getFansiteLink();
				
				$source["cost"] = $this->GetExtendedCostString($source["cost"]);
				
				$sources[] = $source;
			}
		}
		
		//self::debug("Sources", $sources);
		
		return $sources;
	}
	
	function GetExtendedCostString($costId){
		global $aDB;
		
		$costData = $aDB->selectRow("SELECT * FROM armory_extended_cost WHERE id = ?d", $costId);
		
		$item_info = array();
		
		$emblems = array();
		if($costData){
			$string = '<span data-currency="'.$costId.'" class="item-link-small-right color-q1">';
			
			if($costData["arenaPoints"] > 0){
				$emblems[] = '
				<div id="honor-tooltip" style="display: none"> Arenapunkte </div>
				<span class="count tip" data-tooltip="#honor-tooltip">'.$costData["arenaPoints"].'</span>
				<span class="icon-frame frame-14 "><img src="http://portal.wow-alive.de/images/icons/pvp-arenapoints-icon.png" alt="" width="14" height="14" /></span>';
			}
			if($costData["honorPoints"] > 0){
				$emblems[] = '
				<div id="arena-tooltip" style="display: none"> Ehrenpunkte </div>
				<span class="count tip" data-tooltip="#arena-tooltip">'.$costData["honorPoints"].'</span>
				<span class="icon-frame frame-14 "><img src="http://portal.wow-alive.de/images/icons/18/pvpcurrency-honor-alliance.jpg" alt="" width="14" height="14" /></span>
				<span class="icon-frame frame-14 "><img src="http://portal.wow-alive.de/images/icons/18/pvpcurrency-honor-horde.jpg" alt="" width="14" height="14" /></span>';
			}
			
			for($i = 1; $i <= 5; $i++){
				$item_id = $costData["item".$i];
				if($item_id > 0){
					if(!isset($item_info[$item_id])){
						$item_info[$item_id] = GetItemInfo($item_id);
					}
					$emblems[] = '
					<div id="item'.$i.'-tooltip" style="display: none"> '.$item_info[$item_id]["name"].' </div>
					<span class="count tip" data-tooltip="#item'.$i.'-tooltip">'.$costData["item".$i."count"].'</span>
					<span class="icon-frame frame-14 "><img src="http://portal.wow-alive.de/images/icons/18/'.$item_info[$item_id]["icon"].'.jpg" alt="" width="14" height="14" /></span>';
				}
			
			}
			
			$string .= implode(" ", $emblems);
			$string .= '</span>';
			
			return $string;
		}
		else{
			self::debug("extended cost $costId not found");
			return "";
		}
		// inv_misc_frostemblem_01
	}
	
	/**
     * Returns item source string (vendor, drop, chest loot, etc.)
     * @category Items class
     * @access   public
     * @param    int $item
     * @param    bool $areaDataOnly = false
     * @return   string
     **/
	public function GetItemSource($item, $areaDataOnly = false) {
		global $WSDB, $aDB;
		
		self::debug("Item: $item");
		
		$referenceLoot = $WSDB->selectCell("SELECT `entry` FROM `reference_loot_template` WHERE `item`= ?d LIMIT 1", $item);
		if($referenceLoot){
			$bossLoot = $WSDB->selectCell("SELECT `entry` FROM `creature_loot_template` WHERE `mincountOrRef`= -".$referenceLoot." LIMIT 1");
			if($bossLoot){
				$isBoss = GetNpcInfo($bossLoot, 'isBoss');
				if($isBoss && self::IsUniqueLoot($item)) {
					self::debug("Referenced Boss Loot ID:$bossLoot");
					// We have boss loot, generate improved tooltip.
					return self::GetImprovedItemSource($item, $bossLoot, $areaDataOnly);
				}
				elseif($isBoss) {
					return array('value' => 'sourceType.creatureDrop');
				}
				else {
					return array('value' => 'sourceType.worldDrop');
				}
			}
		}
		
		$bossLoot = $WSDB->selectCell("SELECT `entry` FROM `creature_loot_template` WHERE `item`= ?d LIMIT 1", $item);
		if($bossLoot) {
			$isBoss = GetNpcInfo($bossLoot, 'isBoss');
			if($isBoss && self::IsUniqueLoot($item)) {
				self::debug("Boss Loot ID:$bossLoot");
				// We have boss loot, generate improved tooltip.
				return self::GetImprovedItemSource($item, $bossLoot, $areaDataOnly);
			}
			elseif($isBoss) {
				return array('value' => 'sourceType.creatureDrop');
			}
			else {
				return array('value' => 'sourceType.worldDrop');
			}
		}
		
		
		
		$chestLoot = $WSDB->selectCell("SELECT `entry` FROM `gameobject_loot_template` WHERE `item`= ?d LIMIT 1", $item);
		if($chestLoot) {
			if($chest_data = self::GetImprovedItemSource($item, $chestLoot, $areaDataOnly)) {
				return $chest_data;
			}
			else {
				return array('value' => 'sourceType.gameObjectDrop');
			}
		}
		
		$vendorLoot = $WSDB->selectCell("SELECT `entry` FROM `npc_vendor` WHERE `item`=?d LIMIT 1", $item);
		$reputationReward = $WSDB->selectCell("SELECT `RequiredReputationFaction` FROM `item_template` WHERE `entry`=?d", $item);
		if($vendorLoot && $reputationReward > 0) {
			return array('value' => 'sourceType.factionReward');
		}
		elseif($vendorLoot && (!$reputationReward || $reputationReward == 0)) {
			return array('value' => 'sourceType.vendor');
		}
		
		$questLoot = $WSDB->selectCell("
			SELECT `entry` FROM `quest_template`
			WHERE 	`RewChoiceItemId1` = ?d OR `RewChoiceItemId2` = ?d OR `RewChoiceItemId3` = ?d OR 
					`RewChoiceItemId4` = ?d OR `RewChoiceItemId5` = ?d OR `RewChoiceItemId6` = ?d LIMIT 1", $item, $item, $item, $item, $item, $item);
		if($questLoot) {
			return array('value' => 'sourceType.questReward');
		}
		
		$craftLoot = $aDB->selectCell("SELECT `id` FROM `armory_spell` WHERE `EffectItemType_1`=?d OR `EffectItemType_2`=?d OR `EffectItemType_3`=?d LIMIT 1", $item, $item, $item);
		if($craftLoot) {
			return array('value' => 'sourceType.createdByPlans');
		}
	
		self::debug("No Loot Data found");
		return array('value' => 'sourceType.none');
	}

	/**
	 * Returns item source with area name, boss name, drop rate and area URL
	 * @category Items class
	 * @access   public
	 * @param    int $itemID
	 * @param    int $bossID
	 * @param    bool $areaDataOnly = false
	 * @return   array
 	 **/
	public function GetImprovedItemSource($itemID, $bossID, $areaDataOnly = false) {
		global $aDB, $WSDB, $lang_strings;
		
		$creatures = array();
		$objects = array();
		$achievements = array();
		$quests = array();
		
		$lootID = $bossID;
		self::debug("GetImprovedItemSource($itemID, $bossID, $areaDataOnly);");
		
		$dungeonData = $aDB->selectRow("
			SELECT `instance_id`, ?# AS `name`, `lootid_1`, `lootid_2`, `lootid_3`, `lootid_4` 
			FROM `armory_instance_data` 
			WHERE `id`=?d OR `lootid_1`=?d OR `lootid_2`=?d OR `lootid_3`=?d OR `lootid_4`=?d OR `name_id`=?d LIMIT 1", "name_".$this->locale, $bossID, $bossID, $bossID, $bossID, $bossID, $bossID);
		if(!$dungeonData){
			self::debug("No data in armory_instance_data found");
			$bossRow = $WSDB->selectRow("
				SELECT `entry`, `lootid` 
				FROM `creature_template` 
				WHERE `entry`= ?d LIMIT 1", $bossID);
				
			if($bossRow) {
				$difficultyRow = $WSDB->selectRow("
					SELECT `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3` 
					FROM `creature_template` 
					WHERE `difficulty_entry_1`=?d OR `difficulty_entry_2`=?d OR `difficulty_entry_3` = ?d LIMIT 1", $bossID, $bossID, $bossID);
				if($difficultyRow){
					if($difficultyRow["difficulty_entry_1"] == $bossID){
						$item_difficulty = "25n";
					}
					elseif($difficultyRow["difficulty_entry_2"] == $bossID){
						$item_difficulty = "10h";
					}
					elseif($difficultyRow["difficulty_entry_3"] == $bossID){
						$item_difficulty = "25h";
					}
					$bossID = $difficultyRow["entry"];
				}
				else{
					$item_difficulty = "10n";
				}
				// now get dungeon data again
				$dungeonData = $aDB->selectRow("
					SELECT `instance_id`, ?# AS `name`, `lootid_1`, `lootid_2`, `lootid_3`, `lootid_4` 
					FROM `armory_instance_data` 
					WHERE `id`=?d OR `name_id`=?d LIMIT 1", "name_".$this->locale, $bossID, $bossID);
				if(!$dungeonData){
					return false;
				}
			}
			else{
				return false;
			}
		}
		else{
			self::debug("Data in armory_instance_data found");
			$difficulty_enum = array(1 => '10n', 2 => '25n', 3 => '10h', 4 => '25h');
			$item_difficulty = null;
		
			for($i = 1; $i < 5; $i++) {
				if(isset($dungeonData['lootid_' . $i]) && $dungeonData['lootid_' . $i] == $bossID && isset($difficulty_enum[$i])) {
					$item_difficulty = $difficulty_enum[$i];
				}
			}
		}
		$heroic_string = $lang_strings["armory.item-info.heroic"];
		$instance_heroic = array(4812, 4722, 4987);
		
		//print_r($dungeonData);
		//echo "D: $item_difficulty";	
		switch($item_difficulty) {
			case '10n':
				if(in_array($dungeonData['instance_id'], $instance_heroic)) {
					$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d AND `partySize`=10 AND `is_heroic`=1", "name_".$this->locale, $dungeonData['instance_id']);
				}
				else {
					$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` WHERE `id`=?d", 
							"name_".$this->locale, $dungeonData['instance_id']);
				}
				if(!$instance_data) {
					return false;
				}
				break;
			case '10h':
				if(in_array($dungeonData['instance_id'], $instance_heroic)) {
					$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d AND `partySize`=10 AND `is_heroic`=1", "name_".$this->locale, $dungeonData['instance_id']);
				}
				else {
					$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d", "name_".$this->locale, $dungeonData['instance_id']);
				}
				if(!$instance_data) {
					return false;
				}
				if($heroic_string) {
					//$instance_data['areaName'] .= ' ' . $heroic_string;
				}
				break;
			case '25n':
				if(in_array($dungeonData['instance_id'], $instance_heroic)) {
				$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d AND `partySize`=25 AND `is_heroic`=1", "name_".$this->locale, $dungeonData['instance_id']);
				}
				else {
				$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d", "name_".$this->locale, $dungeonData['instance_id']);
				}
				if(!$instance_data) {
					return false;
				}
				if($instance_data['is_heroic'] == 0) {
					if($heroic_string) {
						//$instance_data['areaName'] .= ' ' . $heroic_string;
					}
					else {
						$instance_data['areaName'] .= ' (25)';
					}
				}
				break;
			case '25h':
				if(in_array($dungeonData['instance_id'], $instance_heroic)) {
					$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d AND `partySize`=25 AND `is_heroic`=1", "name_".$this->locale, $dungeonData['instance_id']);
				}
				else {
					$instance_data = $aDB->selectRow("
						SELECT `id` AS `areaId`, ?# AS `areaName`, `is_heroic`, `key` 
						FROM `armory_instance_template` 
						WHERE `id`=?d", "name_".$this->locale, $dungeonData['instance_id']);
				}
				if(!$instance_data) {
					return false;
				}
				if($heroic_string) {
					//$instance_data['areaName'] .= ' ' . $heroic_string;
				}
				break;
		}
		if(!isset($instance_data) || !is_array($instance_data)) {
			//echo "-noini-";
			//print_r($instance_data);
			return false;
		}
		if($areaDataOnly == true) {
			return array(
				'areaName' => $instance_data['areaName'], 
				'areaUrl' => '/game/zone/'.$instance_data['key'],
			);
		}
		$instance_data['creatureId'] = $aDB->selectCell("
			SELECT `id` 
			FROM `armory_instance_data` 
			WHERE `id`=?d OR `lootid_1`=?d OR `lootid_2`=?d OR `lootid_3`=?d OR `lootid_4`=?d OR `name_id`=?d LIMIT 1", $bossID, $bossID, $bossID, $bossID, $bossID, $bossID);
		$instance_data['creatureName'] = $dungeonData['name'];
		if($bossID > 100000) {
			// GameObject
			$drop_percent = self::GenerateLootPercent($lootID, 'gameobject_loot_template', $itemID);
		}
		else {
			// Creature
			$drop_percent = self::GenerateLootPercent($lootID, 'creature_loot_template', $itemID);
			if($drop_percent == 0)
				$drop_percent = self::GenerateLootPercent($lootID, 'reference_loot_template', $itemID);
			
		}
		$instance_data['dropRate'] = self::GetDropRate($drop_percent);
		$instance_data['value'] = 'sourceType.creatureDrop';
		return $instance_data;
	}
	
	/**
	 * Generates drop percent for $boss_id boss and $item_id item.
	 * @author   DiSlord
	 * @category Mangos class
	 * @access   public
	 * @param    int $boss_id
	 * @param    string $db_table
	 * @param    int $item_id
	 * @return   int
	 **/
	public function GenerateLootPercent($boss_id, $db_table, $item_id) {
		global $WSDB;
		
		self::debug("GenerateLootPercent($boss_id, $db_table, $item_id);");
		
		$allowed_tables = array(
			'creature_loot_template'   => true,
			'disenchant_loot_template' => true,
			'fishing_loot_template'    => true,
			'gameobject_loot_template' => true,
			'item_loot_template'       => true,
			'reference_loot_template'  => true
		);
		if(!isset($allowed_tables[$db_table])) {
			return 0;
		}
		$lootTable = $WSDB->select("SELECT `ChanceOrQuestChance`, `groupid`, `mincountOrRef`, `item` FROM ?# WHERE `entry`=?d", $db_table, $boss_id);
		if(!$lootTable) {
			return 0;
		}
		
		$percent = 0;
		$refIDs = array();
		$lootEntries = array();
		foreach($lootTable as $loot) {
			if($loot['mincountOrRef'] < 0) {
				$refIDs[] = abs($loot["mincountOrRef"]);
			}
			else{
				$lootEntries[] = $loot;
			}
		}
		
		if(count($refIDs)){
			$lootTable = $WSDB->select("SELECT `ChanceOrQuestChance`, `groupid`, `mincountOrRef`, `item` FROM reference_loot_template WHERE `entry` IN(?);", implode(",",$refIDs));
			if($lootTable){
				foreach($lootTable as $loot) {
					$lootEntries[] = $loot;
				}
			}
		}
		
		foreach($lootEntries as $loot) {
			if($loot['ChanceOrQuestChance'] > 0 && $loot['item'] == $item_id) {
				$percent = $loot['ChanceOrQuestChance'];
			}
			elseif($loot['ChanceOrQuestChance'] == 0 && $loot['item'] == $item_id) {
				$current_group = $loot['groupid'];
				$percent = 0;
				$i = 0;
				foreach($lootTable as $tLoot) {
					if($tLoot['groupid'] == $current_group) {
						if($tLoot['ChanceOrQuestChance'] > 0) {
							$percent += $tLoot['ChanceOrQuestChance'];
						}
						else {
							$i++;
						}
					}
				}
				$percent = round((100 - $percent) / $i, 3);
			}
		}
		
		
		return $percent;
	}
	/**
	 * Assign text value to int drop percent (drop > 51 = High, etc.)
	 * @category Mangos class
	 * @access   public
	 * @param    float $percent
	 * @return   string
	 **/
	public function GetDropRate($percent) {
		if($percent == 100) {
			return 6;
		}
		elseif($percent > 51) {
			return 5;
		}
		elseif($percent > 25) {
			return 4;
		}
		elseif($percent > 15) {
			return 3;
		}
		elseif($percent > 3) {
			return 2;
		}
		elseif($percent > 0 && $percent < 1) {
			return 1;
		}
		elseif($percent < 0 || $percent == 0) {
			return 0;
		}
	}
	
	function getRotateImage(){
		$url = "http://eu.media.blizzard.com/wow/renders/items/item".$this->entry.".jpg";
		$contents = file_get_contents($url);
		if(strlen($contents) > 0)
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/templates/Shattered-World/images/items/item".$this->entry.".jpg", $contents);
	}
	
	/**
     * Return itemset bonus spell(s) info
     * @category Items class
     * @access   public
     * @param    int $itemsetdata
     * @return   array
     **/
    public function GetItemSetBonusInfo($itemsetdata) {
    	global $aDB;
		
	    $tmp_locale = 'de_de';
        
		$threshold_count = array();
		
		$itemSetBonuses = array();
        for($i = 1; $i < 9; $i++) {
            if($itemsetdata['bonus' . $i] > 0) {
                $threshold = $itemsetdata['threshold' . $i];
                $spell_tmp = array();

                $spell_tmp = $aDB->selectRow("SELECT * FROM `armory_spell` WHERE `id`=?d", $itemsetdata['bonus' . $i]);
                $wowhead = file_get_contents("http://de.wowhead.com/spell=".$itemsetdata['bonus' . $i]."&power");
		
				if(preg_match("@<span class=\"q\">([^<]+)</span>@", $wowhead, $matches)){
					$spell_tmp['Description_de_de'] = utf8_decode($matches[1]);
					$spell_tmp['Description_de_de'] = str_replace("\'", "'", $spell_tmp['Description_de_de']);
					
				}
				/*if(!isset($spell_tmp['Description_' . $tmp_locale]) || empty($spell_tmp['Description_' . $tmp_locale])) {
                    // try to find en_gb locale
                    if(isset($spell_tmp['Description_en_gb']) && !empty($spell_tmp['Description_en_gb'])) {
                        $tmp_locale = 'en_gb';
                    }
                    else {
                        continue;
                    }
                }*/
				
				$desc = self::SpellReplace($spell_tmp, self::ValidateSpellText($spell_tmp['Description_de_de']));
				$desc = str_replace('&quot;', '"', $desc);
				$bonus = array(
					"threshold" => $threshold,
					"desc" => htmlentities($desc),
				);
				
				if(!isset($threshold_count[$threshold]))
					$threshold_count[$threshold] = 1;
				else
                	$threshold_count[$threshold]++;
				$order_id = $threshold."-".$threshold_count[$threshold];
				$itemSetBonuses[$order_id] = $bonus;
			    
            }
	   }
       ksort($itemSetBonuses); // Correct display itemset bonuses
	   return $itemSetBonuses;
    }	
	
	 /**
     * Spell Description handler
     * @author   DiSlord aka Chestr
     * @category Items class
     * @access   public
     * @param    array $spell
     * @param    string $text
     * @return   array
     **/
    public function SpellReplace($spell, $text) {
		global $aDB;
		
        $letter = array('${','}');
        $values = array( '[',']');
        $text = str_replace($letter, $values, $text);
        $signs = array('+', '-', '/', '*', '%', '^');
        $data = $text;
        $pos = 0;
        $npos = 0;
        $str = null;
        $cacheSpellData=array(); // Spell data for spell
        $lastCount = 1;
        while(false !== ($npos = strpos($data, '$', $pos))) {
            if($npos != $pos) {
                $str .= substr($data, $pos, $npos-$pos);
            }
            $pos = $npos + 1;
            if('$' == substr($data, $pos, 1)) {
                $str .= '$';
    			$pos++;
                continue;
    		}
            if(!preg_match('/^((([+\-\/*])(\d+);)?(\d*)(?:([lg].*?:.*?);|(\w\d*)))/', substr($data, $pos), $result)) {
                continue;
            }
            $pos += strlen($result[0]);
            $op = $result[3];
            $oparg = $result[4];
            $lookup = $result[5]? $result[5]:$spell['id'];
            $var = $result[6] ? $result[6]:$result[7];
            if(!$var) {
                continue;
            }
            if($var[0] == 'l') {
                $select = explode(':', substr($var, 1));
                $str .= @$select[$lastCount == 1 ? 0 : 1];
            }
            elseif($var[0] == 'g') {
                $select = explode(':', substr($var, 1));
                $str .= $select[0];
            }
            else {
                $spellData = @$cacheSpellData[$lookup];
                if($spellData == 0) {
                    if($lookup == $spell['id']) {
                        $cacheSpellData[$lookup] = $this->GetSpellData($spell);
                    }
                    else {
                        $cacheSpellData[$lookup] = $this->GetSpellData($aDB->selectRow("SELECT * FROM `armory_spell` WHERE `id`=?d", $lookup));
                    }
                    $spellData = @$cacheSpellData[$lookup];
                }
                if($spellData && $base = @$spellData[strtolower($var)]) {
                    if($op && is_numeric($oparg) && is_numeric($base)) {
                         $equation = $base.$op.$oparg;
                         @eval("\$base = $equation;");
    		        }
                    if(is_numeric($base)) {
                        $lastCount = $base;
                    }
                }
                else {
                    $base = $var;
                }
                $str .= $base;
            }
        }
        $str .= substr($data, $pos);
        $str = preg_replace_callback("/\[.+[+\-\/*\d]\]/", array($this, 'MyReplace'), $str);
        return $str;
    }
	
	/**
     * Spell Description handler
     * @author   DiSlord aka Chestr
     * @category Items class
     * @access   public
     * @param    array $spell
     * @return   array
     **/
    public function GetSpellData($spell) {
		global $aDB;
        // Basepoints
        $s1 = abs($spell['EffectBasePoints_1'] + $spell['EffectBaseDice_1']);
        $s2 = abs($spell['EffectBasePoints_2'] + $spell['EffectBaseDice_2']);
        $s3 = abs($spell['EffectBasePoints_3'] + $spell['EffectBaseDice_3']);
        if($spell['EffectDieSides_1']>$spell['EffectBaseDice_1'] && ($spell['EffectDieSides_1']-$spell['EffectBaseDice_1'] != 1)) {
            $s1 .= ' - ' . abs($spell['EffectBasePoints_1'] + $spell['EffectDieSides_1']);
        }
        if($spell['EffectDieSides_2']>$spell['EffectBaseDice_2'] && ($spell['EffectDieSides_2']-$spell['EffectBaseDice_2'] != 1)) {
            $s2 .= ' - ' . abs($spell['EffectBasePoints_2'] + $spell['EffectDieSides_2']);
        }
        if($spell['EffectDieSides_3']>$spell['EffectBaseDice_3'] && ($spell['EffectDieSides_3']-$spell['EffectBaseDice_3'] != 1)) {
            $s3 .= ' - ' . abs($spell['EffectBasePoints_3'] + $spell['EffectDieSides_3']);
        }
        $d = 0;
        if($spell['DurationIndex']) {
            if($spell_duration = $aDB->selectRow("SELECT * FROM `armory_spell_duration` WHERE `id`=?d", $spell['DurationIndex'])) {
                $d = $spell_duration['duration_1']/1000;
            }
        }
        // Tick duration
        $t1 = $spell['EffectAmplitude_1'] ? $spell['EffectAmplitude_1'] / 1000 : 5;
        $t2 = $spell['EffectAmplitude_1'] ? $spell['EffectAmplitude_2'] / 1000 : 5;
        $t3 = $spell['EffectAmplitude_1'] ? $spell['EffectAmplitude_3'] / 1000 : 5;
        
        // Points per tick
        $o1 = @intval($s1 * $d / $t1);
        $o2 = @intval($s2 * $d / $t2);
        $o3 = @intval($s3 * $d / $t3);
        $spellData['t1'] = $t1;
        $spellData['t2'] = $t2;
        $spellData['t3'] = $t3;
        $spellData['o1'] = $o1;
        $spellData['o2'] = $o2;
        $spellData['o3'] = $o3;
        $spellData['s1'] = $s1;
        $spellData['s2'] = $s2;
        $spellData['s3'] = $s3;
        $spellData['m1'] = $s1;
        $spellData['m2'] = $s2;
        $spellData['m3'] = $s3;
        $spellData['x1'] = $spell['EffectChainTarget_1'];
        $spellData['x2'] = $spell['EffectChainTarget_2'];
        $spellData['x3'] = $spell['EffectChainTarget_3'];
        $spellData['i']  = $spell['MaxAffectedTargets'];
        $spellData['d']  = sec_to_dhms($d);
        $spellData['d1'] = sec_to_dhms($d);
        $spellData['d2'] = sec_to_dhms($d);
        $spellData['d3'] = sec_to_dhms($d);
        $spellData['v']  = $spell['MaxTargetLevel'];
        $spellData['u']  = $spell['StackAmount'];
        $spellData['a1'] = GetRadius($spell['EffectRadiusIndex_1']);
        $spellData['a2'] = GetRadius($spell['EffectRadiusIndex_2']);
        $spellData['a3'] = GetRadius($spell['EffectRadiusIndex_3']);
        $spellData['b1'] = $spell['EffectPointsPerComboPoint_1'];
        $spellData['b2'] = $spell['EffectPointsPerComboPoint_2'];
        $spellData['b3'] = $spell['EffectPointsPerComboPoint_3'];
        $spellData['e']  = $spell['EffectMultipleValue_1'];
        $spellData['e1'] = $spell['EffectMultipleValue_1'];
        $spellData['e2'] = $spell['EffectMultipleValue_2'];
        $spellData['e3'] = $spell['EffectMultipleValue_3'];
        $spellData['f1'] = $spell['DmgMultiplier_1'];
        $spellData['f2'] = $spell['DmgMultiplier_2'];
        $spellData['f3'] = $spell['DmgMultiplier_3'];
        $spellData['q1'] = $spell['EffectMiscValue_1'];
        $spellData['q2'] = $spell['EffectMiscValue_2'];
        $spellData['q3'] = $spell['EffectMiscValue_3'];
        $spellData['h']  = $spell['procChance'];
        $spellData['n']  = $spell['procCharges'];
        $spellData['z']  = "<home>";
        return $spellData;
    }
	
	/**
     * Returns available races string (if mask > 0)
     * @category Items class
     * @access   public
     * @param    int $mask
     * @return   string
     **/
    public function AllowableRaces($mask) {
		global $aDB;
        
		$mask &= 0x7FF;
        // Return zero if for all class (or for none)
		if($mask == 0x7FF || $mask == 0) {
            return 0;
		}
        $races = $aDB->select("SELECT `id`, ?# AS `name` FROM `armory_races`", "name_".$this->locale);
        if(!is_array($races)) {
            output_message("debug", '?s : unable to find races names for locale ?s', __METHOD__, $this->locale);
            return false;
        }
        $races_data = array();
        foreach($races as $race_tmp) {
            $races_data[$race_tmp['id']] = $race_tmp['name'];
        }
        $i = 1;
        $rMask = array();
        while($mask) {
			if($mask & 1) {
                $rMask[$i] = $races_data[$i];
		   	}
			$mask>>=1;
			$i++;
		}
		return $rMask;
    }
	
	/**
     * Returns available classes string (if mask > 0)
     * @category Items class
     * @access   public
     * @param    int $mask
     * @return   string
     **/
    public function AllowableClasses($mask) {
		global $aDB;

		$mask &= 0x5FF;
		// Return zero if for all class (or for none)
		if($mask == 0x5FF || $mask == 0) {
			return 0;
		}
        
		$classes = $aDB->select("SELECT `id`, ?# AS `name` FROM `armory_classes`", "name_".$this->locale);
        if(!is_array($classes)) {
           	output_message("debug",'?s : unable to find classes names for locale ?s', __METHOD__, $this->locale);
            return false;
        }
        $classes_data = array();
        foreach($classes as $class_tmp) {
            $classes_data[$class_tmp['id']] = $class_tmp['name'];
        }
        $i = 1;
        $rMask = array();
        while($mask) {
			if($mask & 1) {
                $rMask[$i] = '<span class="color-c'.$i.'">'.$classes_data[$i].'</span>';
	    	}
			$mask>>=1;
			$i++;
		}
		return $rMask;
	}	
   /**
     * Generates random enchantments for $item_entry and $item_guid (if provided)
     * @category Items class
     * @access   public
     * @param    int $item_entry
     * @param    int $owner_guid
     * @param    int $item_guid
     * @return   array
     **/
    public function GetRandomPropertiesData($item_entry, $owner_guid, $item_guid = 0, $rIdOnly = false, $item_data = null) {
		global $CHDB, $WSDB, $aDB;
		
        // I have no idea how it works but it works :D
        // Do not touch anything in this method (at least until somebody will explain me what the fuck am I did here).
        $enchId = 0;
        $use = 'property';
		if($item_guid > 0) {
			if($this->IsCorrect()) {
				$enchId = $this->GetItemRandomPropertyId();
				if($enchId < 0) {
					$use = 'suffix';
					$enchId = abs($enchId);
				}
			}
			else {
				$enchId = $CHDB->selectCell("SELECT `randomPropertyId` FROM `item_instance` WHERE `guid`=?d", $item_guid);
			}
		}
		else {
			$item_guid = $this->GetItemGUIDByEntry($item_entry, $owner_guid);
			$enchId = $CHDB->selectCell("SELECT `randomPropertyId` FROM `item_instance` WHERE `guid`=?d", $item_guid);
		}
        if($rIdOnly == true) {
            return $enchId;
        }
		
        $return_data = array();
        $table = 'randomproperties';
        if($use == 'property') {
            $rand_data = $aDB->selectRow("SELECT `name_?s` AS `name`, `ench_1`, `ench_2`, `ench_3` FROM `armory_randomproperties` WHERE `id`=?d", $this->locale, $enchId);
        }
        elseif($use == 'suffix') {
            $table = 'randomsuffix';
        }
        if($table == 'randomproperties') {
            if(!$rand_data) {
                output_message("debug", '?s : unable to get rand_data FROM `?s_?s` for id ?d (itemGuid: ?d, ownerGuid: ?d)', __METHOD__, $armoryconfig['db_prefix'], $table, $enchId, $item_guid, $owner_guid);
                return false;
            }
            $return_data['suffix'] = $rand_data['name'];
            $return_data['data'] = array();
            for($i = 1; $i < 4; $i++) {
                if($rand_data['ench_' . $i] > 0) {
                    $return_data['data'][$i] = $aDB->selectCell("SELECT `text_?s` FROM `armory_enchantment` WHERE `id`=?d", $this->locale, $rand_data['ench_' . $i]);
                }
            }
        }
        elseif($table == 'randomsuffix') {
            $enchant = $aDB->selectRow("SELECT `id`, `name_?s` AS `name`, `ench_1`, `ench_2`, `ench_3`, `pref_1`, `pref_2`, `pref_3` FROM `armory_randomsuffix` WHERE `id`=?d", $this->locale, $enchId);
            if(!$enchant) {
                return false;
            }
            $return_data['suffix'] = $enchant['name'];
            $return_data['data'] = array();
            $item_data = $WSDB->selectRow("SELECT `InventoryType`, `ItemLevel`, `Quality` FROM `item_template` WHERE `entry`=?d", $item_entry);
            $points = $this->GetRandomPropertiesPoints($item_data['ItemLevel'], $item_data['InventoryType'], $item_data['Quality']);
            $return_data = array(
                'suffix' => $enchant['name'],
                'data' => array()
            );
            $k = 1;
            for($i = 1; $i < 4; $i++) {
                if(isset($enchant['ench_' . $i]) && $enchant['ench_' . $i] > 0) {
                    $cur = $aDB->selectCell("SELECT `text_?s` FROM `armory_enchantment` WHERE `id` = ?d", $this->locale, $enchant['ench_' . $i]);
                    $return_data['data'][$k] = str_replace('$i', round(floor($points * $enchant['pref_' . $i] / 10000), 0), $cur);
                }
                $k++;
            }
        }
        return $return_data;
    }
	
	public function GetItemSubTypeInfo($tooltip = false, $data = false) {
        global $WSDB, $aDB;
		
		if($data) {
            $itemclassInfo = array('class' => $data['class'], 'subclass' => $data['subclass']);
        }
        else {
            $itemclassInfo = $WSDB->selectRow("SELECT `class`, `subclass` FROM `item_template` WHERE `entry`={$this->entry}");
        }
		
        if($tooltip == true) {
            if($itemclassInfo['class'] == ITEM_CLASS_ARMOR && $itemclassInfo['subclass'] == 0) {
                return;
            }
            return $aDB->selectCell("SELECT ?# FROM `armory_itemsubclass` WHERE `class`= ?d AND `subclass` = ?d;", 
				"subclass_name_".$this->locale, $itemclassInfo['class'], $itemclassInfo['subclass']);
        }
        return $aDB->selectRow("SELECT ?# AS `subclass_name`, `key` FROM `armory_itemsubclass` WHERE `class` = ?d AND `subclass`= ?d", 
			"subclass_name_".$this->locale, $itemclassInfo['class'], $itemclassInfo['subclass']);
    }
	
	public function LoadFromDB($data, $owner_guid) {
    	global $WSDB, $CHDB;
		
        $this->m_owner = $owner_guid;
        $this->m_guid = $data['item'];
        
		$this->tc_data = $CHDB->selectRow("SELECT * FROM `item_instance` WHERE `guid` = ?d AND `owner_guid` = ?d", $this->m_guid, $this->m_owner);
		if(!$this->tc_data) {
			output_message("debug", '?s : item (GUID: ?d) was not found in `item_instance` table!', __METHOD__, $this->m_guid);
			return false;
		}
		
		if(isset($this->tc_data['enchantments'])) {
			$this->tc_ench = explode(' ', $this->tc_data['enchantments']);
		}
		$this->entry = $this->tc_data['itemEntry'];
		$this->tc_data['maxdurability'] = $WSDB->selectCell("SELECT `MaxDurability` FROM `item_template` WHERE `entry`=?d", $this->entry);
        
     
		if($data['bag'] == 0 && $data['slot'] < INV_MAX) {
            $this->equipped = true;
        }
        $this->m_slot = $data['slot'];
        $this->m_bag = $data['bag'];
        $this->m_ench = $data['enchants'];
        $this->m_socketInfo = array();
        $this->loaded = true;
        if($this->entry == 0) {
            $this->loaded = false;
            return false;
        }
        return true;
    }
	
	function gatherItemInfo(){
		$list = array();
	}
	
	function GetParamData(){
		return "e=4209&amp;g0=68778&amp;g1=52211&amp;re=140&amp;set=71047,71045&amp;d=87";
	}
	
	function getItemIcon($entry = -1, $displayId = NULL) {
        global $WSDB, $aDB;
		
		if($entry == -1 && $this->entry > 0)
			$entry = $this->entry;
		
		if($displayId == NULL){
            $displayId = $WSDB->selectCell("SELECT `displayid` FROM `item_template` WHERE `entry`=?d LIMIT 1", $entry);
        }
		$itemIcon = $aDB->selectCell("SELECT `icon` FROM `armory_icons` WHERE `displayid`=?d LIMIT 1", $displayId);
        return strtolower($itemIcon);
    }
	
	public function IsEquipped() {
        return $this->equipped;
    }
    
    public function IsEnchantable($charIsEnchanter){
    	
    	
    }
    
	/**
	* Checks is item is unique in boss's loot table
	* @category Items class
	* @access   public
	* @param    int $itemID
	* @return   bool
	**/
	public function IsUniqueLoot($itemID) {
		global $WSDB;
		$item_count = $WSDB->selectCell("SELECT COUNT(`entry`) FROM `creature_loot_template` WHERE `item`=?d", $itemID);
		if($item_count > 1) {
			return false;
		}
		return true;
	}
	
	/**
     * Checks if personal arena rating required for sale current item (and returns required rating if exists)
     * @category Items class
     * @access   public
     * @param    int $itemID
     * @return   int
     **/
    public function IsRequiredArenaRating($itemID) {
		global $WSDB, $aDB;
		
        $extended_cost_id = $WSDB->selectCell("SELECT `ExtendedCost` FROM `npc_vendor` WHERE `item`= ?d", $itemID);
        if($extended_cost_id === false) {
            return false;
        }
        if($extended_cost_id < 0) {
            $extended_cost_id = abs($extended_cost_id);
        }
        $arenaTeamRating = $aDB->selectCell("SELECT `personalRating` FROM `armory_extended_cost` WHERE `id`=?d", $extended_cost_id);
        if($arenaTeamRating > 0) {
            return $arenaTeamRating;
        }
        return false;
    }
	
    /**
     * Returns item GUID
     * @category Item class
     * @access   public
     * @return   int
     **/
    public function GetGUID() {
        return $this->m_guid;
    }
    
    /**
     * @return object
     **/
    public function GetProto() {
        if(!$this->m_proto) {
            $this->m_proto = new ItemPrototype();
            $this->m_proto->LoadItem($this->GetEntry(), $this->GetGUID(), $this->GetOwnerGUID());
            if(!$this->m_proto || !$this->m_proto->IsCorrect()) {
                output_message("debug", '?s : unable to find item with entry ?d in `item_template` table.', __METHOD__, $this->GetEntry());
                return false;
            }
        }
        return $this->m_proto;
    }
    
    /**
     * @return int
     **/
    public function GetEntry() {
        return $this->entry;
    }
    
    /**
     * @return int
     **/
    public function GetOwnerGUID() {
        return $this->m_owner;
    }
    
    /**
     * @return object
     **/
    public function GetOwner() {
        return $this->GetOwnerGUID(); //PH
    }
    
    /**
     * @return bool
     **/
    public function IsBroken() {
    	return $this->tc_data['maxdurability'] > 0 && $this->tc_data['durability'] == 0;
    }
    
	private function IsMultiplyItemSet($itemSetID) {
        global $aDB;
		if($itemSetID >= 843 && $itemSetID != 881 && $itemSetID != 882) {
        	self::debug("IsMultiplyItemSet");
		    return true;
        }
        $setID = $aDB->selectCell("SELECT `id` FROM `armory_itemsetdata` WHERE `original`=?d LIMIT 1", $itemSetID);
        self::debug("Search for $itemSetID");
		if($setID > 160) {
        	self::debug("IsMultiplyItemSet");
		    return true;
        }
        self::debug("IsMultiplyItemSet");
		return false;
    }
	
    /**
     * @return int
     **/
    public function GetItemRandomPropertyId() {
    	return $this->tc_data['randomPropertyId'];
    }
    
    /**
     * @return int
     **/
    public function GetItemSuffixFactor() {
        return 0;
    }
    
    /**
     * @return int
     **/
    public function GetEnchantmentId() {
    	global $CHDB;
    	if($this->m_ench > 10000 && $this->m_guid > 0){
    		$row = $CHDB->selectCell("SELECT enchantments FROM item_instance WHERE guid = ?d;", $this->m_guid);
    		$array = explode(" ",$row);
    		return $array[0];
    	}
    	
        return $this->m_ench;
    }
    
    /**
     * @return array
     **/
    public function GetSocketInfo($num) {
		global $aDB;
		
        if($num <= 0 || $num > 4) {
            return 0;
        }
        if(isset($this->m_socketInfo[$num])) {
            return $this->m_socketInfo[$num];
        }
        $data = array();
		$socketfield = array(
			1 => 6,
			2 => 9,
			3 => 12
		);
		$socketInfo = $this->tc_ench[$socketfield[$num]];
		
		if($socketInfo > 0) {
            $gemData = $aDB->selectRow("SELECT ?# AS `text`, `gem` FROM `armory_enchantment` WHERE `id`=?d", "text_".$this->locale, $socketInfo);
            $data['enchant_id'] = $socketInfo;
            $data['item'] = $gemData['gem'];
            $data['icon'] = self::getItemIcon($data['item']);
            $data['enchant'] = $gemData['text'];
			$data['color'] = $aDB->selectCell("SELECT `color` FROM `armory_gemproperties` WHERE `spellitemenchantement`=?d", $socketInfo);
            $this->m_socketInfo[$num] = $data; // Is it neccessary?
            
            return $data;
        }
        return false;
    }
    
    /**
     * @return array
     **/
    public function GetSocketInfoByGem($gem_id = 0) {
		global $aDB;
		//self::debug("GetSocketInfoByGem($gem_id)");
		
		if(!is_numeric($gem_id) || $gem_id <= 0)
			return false;
		
        if(isset($this->m_socketInfo[$gem_id])) {
            return $this->m_socketInfo[$gem_id];
        }
        $data = array();
		$socketfield = array(
			1 => 6,
			2 => 9,
			3 => 12
		);
		
		$gemData = $aDB->selectRow("SELECT `id`, ?# AS `text`, `gem` FROM `armory_enchantment` WHERE `gem`=?d", "text_".$this->locale, $gem_id);
        $data['enchant_id'] = $gemData["id"];
        $data['item'] = $gemData['gem'];
        $data['icon'] = self::getItemIcon($gemData['gem']);
        $data['enchant'] = $gemData['text'];
		$data['color'] = $aDB->selectCell("SELECT `color` FROM `armory_gemproperties` WHERE `spellitemenchantement`=?d", $gemData["id"]);
        $this->m_socketInfo[$gem_id] = $data; // Is it neccessary?
        return $data;
    }
    
    /**
     * @return int
     **/
    public function GetUInt32Value($index) {
        return (isset($this->m_values[$index])) ? $this->m_values[$index] : 0;
    }
    
    /**
     * @return int
     **/
    public function GetSlot() {
        return $this->m_slot;
    }
    
    /**
     * @return int
     **/
    public function GetBag() {
        return $this->m_bag;
    }
    
    /**
     * @return int
     **/
    public function GetSkill() {
		global $WSDB;
        $item_weapon_skills = array(
            SKILL_AXES,     SKILL_2H_AXES,  SKILL_BOWS,          SKILL_GUNS,      SKILL_MACES,
            SKILL_2H_MACES, SKILL_POLEARMS, SKILL_SWORDS,        SKILL_2H_SWORDS, 0,
            SKILL_STAVES,   0,              0,                   SKILL_UNARMED,   0,
            SKILL_DAGGERS,  SKILL_THROWN,   SKILL_ASSASSINATION, SKILL_CROSSBOWS, SKILL_WANDS,
            SKILL_FISHING
        );
        $item_armor_skills = array(
            0, SKILL_CLOTH, SKILL_LEATHER, SKILL_MAIL, SKILL_PLATE_MAIL, 0, SKILL_SHIELD, 0, 0, 0, 0
        );
        $item_info = $WSDB->selectRow("SELECT `class`, `subclass` FROM `item_template` WHERE `entry`=?d", $this->GetEntry());
        if(!$item_info) {
            return 0;
        }
        switch($item_info['class']) {
            case ITEM_CLASS_WEAPON:
                if($item_info['subclass'] > MAX_ITEM_SUBCLASS_WEAPON) {
                    return 0;
                }
                return (isset($item_weapon_skills[$item_info['subclass']])) ? $item_weapon_skills[$item_info['subclass']] : 0;
                break;
            case ITEM_CLASS_ARMOR:
                if($item_info['subclass'] > MAX_ITEM_SUBCLASS_ARMOR) {
                    return 0;
                }
                return (isset($item_armor_skills[$item_info['subclass']])) ? $item_armor_skills[$item_info['subclass']] : 0;
                break;
            default:
                return 0;
                break;
        }
    }
    
	/**
     * Returns reagents info for crafted item (itemID)
     * @category Items class
     * @access   private
     * @param    int $itemID
     * @return   array
     **/
    private function GetSpellItemCreateReagentsInfo($itemID) {
    	global $aDB;
	    $spell = $aDB->selectRow("
        SELECT
        `Reagent_1`, `Reagent_2`, `Reagent_3`, `Reagent_4`, `Reagent_5`, `Reagent_6`,
        `ReagentCount_1`, `ReagentCount_2`, `ReagentCount_3`,
        `ReagentCount_4`, `ReagentCount_5`, `ReagentCount_6`
        FROM `armory_spell`
        WHERE `EffectItemType_1`=?d OR `EffectItemType_2`=?d OR `EffectItemType_3`=?d", $itemID, $itemID, $itemID);
        if(!$spell) {
            return false;
        }
        $data = array();
        for($i = 1; $i < 6; $i++) {
            if(isset($spell['Reagent_' . $i]) && $spell['Reagent_' . $i] > 0) {
                $data[$i] = array(
                    'count' => $spell['ReagentCount_' . $i],
                    'name'  => self::GetItemName($spell['Reagent_' . $i])
                );
            }
        }
        return $data;
    }
	
    /**
     * @return int
     **/
    public function GetCurrentDurability() {
        return $this->tc_data['durability'];
    }
    
    /**
     * @return int
     **/
    public function GetMaxDurability() {
		return $this->tc_data['maxdurability']; // assigned in Item::LoadFromDB()
    }
	
    public function GetSocketColorString($color) {
    	global $lang_strings;
    	
    	$string = 0;
        switch($color) {
    		case 1:
    			$string = $lang_strings["armory.item-tooltip.meta-socket"];
    			break;
    		case 2:
    			$string = $lang_strings["armory.item-tooltip.red-socket"];
    			break;
    		case 4:
    			$string = $lang_strings["armory.item-tooltip.yellow-socket"];
    			break;
    		case 8:
    			$string = $lang_strings["armory.item-tooltip.blue-socket"];
    			break;
        }
        return $string;
    }
    
    public function IsGemMatchesSocketColor($gem_color, $socket_color) {
    	if($socket_color == $gem_color) {
    		return true;
    	}
    	elseif($socket_color == 2 && in_array($gem_color, array(6, 10, 14))) {
    		return true;
    	}
    	elseif($socket_color == 4 && in_array($gem_color, array(6, 12, 14))) {
    		return true;
    	}
    	elseif($socket_color == 8 && in_array($gem_color, array(10, 12, 14))) {
    		return true;
    	}
    	elseif($socket_color == 0) {
    		// Extra socket
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    /**
     * @return array
     **/
    public function GetItemDurability() {
        return array('current' => $this->GetCurrentDurability(), 'max' => $this->GetMaxDurability());
    }
    
    /**
     * @return array
     **/
    public function GetRandomSuffixData() {
        return false;
    }
	
	 public function GetItemBonusTemplate($statType) {
        switch($statType) {
            case 3:
                $bonus_template = 'bonusAgility';
                break;
            case 4:
                $bonus_template = 'bonusStrength';
                break;
            case 5:
                $bonus_template = 'bonusIntellect';
                break;
            case 6:
                $bonus_template = 'bonusSpirit';
                break;
            case 7:
                $bonus_template = 'bonusStamina';
                break;
            case 12:
                $bonus_template = 'bonusDefenseSkillRating';
                break;
            case 13:
                $bonus_template = 'bonusDodgeRating';
                break;
            case 14:
                $bonus_template = 'bonusParryRating';
                break;
            case 15:
            case 48:
                $bonus_template = 'bonusBlockRating';
                break;
            case 16:
                $bonus_template = 'bonusHitMeleeRating';
                break;
            case 17:
                $bonus_template = 'bonusHitRangedRating';
                break;
            case 18:
                $bonus_template = 'bonusHitSpellRating';
                break;
            case 19:
                $bonus_template = 'bonusCritMeleeRating';
                break;
            case 20:
                $bonus_template = 'bonusCritRangedRating';
                break;
            case 21:
                $bonus_template = 'bonusCritSpellRating';
                break;
            case 22:
                $bonus_template = 'bonusHitTakenMeleeRating';
                break;
            case 23:
                $bonus_template = 'bonusHitTakenRangedRating';
                break;
            case 24:
                $bonus_template = 'bonusHitTakenSpellRating';
                break;
            case 25:
                $bonus_template = 'bonusCritTakenMeleeRating';
                break;                
            case 26:
                $bonus_template = 'bonusCritTakenRangedRating';
                break;
            case 27:
                $bonus_template = 'bonusCritTakenSpellRating';
                break;
            case 28:
                $bonus_template = 'bonusHasteMeleeRating';
                break;
            case 29:
                $bonus_template = 'bonusHasteRangedRating';
                break;
            case 30:
                $bonus_template = 'bonusHasteSpellRating';
                break;
            case 31:
                $bonus_template = 'bonusHitRating';
                break;
            case 32:
                $bonus_template = 'bonusCritRating';
                break;
            case 33:
                $bonus_template = 'bonusHitTakenRating';
                break;
            case 34:
                $bonus_template = 'bonusCritTakenRating';
                break;
            case 35:
                $bonus_template = 'bonusResilienceRating';
                break;
            case 36:
                $bonus_template = 'bonusHasteRating';
                break;
            case 37:
                $bonus_template = 'bonusExpertiseRating';
                break;
            case 38:
            case 39:
                $bonus_template = 'bonusAttackPower';
                break;
            case 40:
                $bonus_template = 'bonusFeralAttackPower';
                break;
            case 41:
            case 42:
            case 45:
                $bonus_template = 'bonusSpellPower';
                break;
            case 43:
                $bonus_template = 'bonusManaRegen';
                break;
            case 44:
                $bonus_template = 'bonusArmorPenetration';
                break;
            case 46:
                $bonus_template = 'bonusHealthRegen';
                break;
            case 47:
                $bonus_template = 'bonusSpellPenetration';
                break;
            default:
                $bonus_template = null;
        }
        return $bonus_template;
    }
	
	/**
     * Replace special symbols in $text.
     * @category Utils class
     * @access   public
     * @param    string $text.
     * @return   string
     **/
    public function ValidateSpellText($text) {
        $letter = array("'",'"'     ,"<"   ,">"   ,">"   ,"\r","\n"  , "\n"    , "\n"   );
        $values = array("`",'&quot;',"&lt;","&gt;","&gt;",""  ,"<br>", "<br />", "<br/>");
        return str_replace($letter, $values, $text);
    }
	
	public function GenerateEnchantmentSpellData($spellID) {
        global $aDB;
		
		$spell_info = $aDB->selectRow("SELECT `SpellName_en_gb`, `Description_en_gb` FROM `armory_spell` WHERE `id`=?d LIMIT 1", $spellID);
        $data = array(
            'name' => $spell_info['SpellName_en_gb'],
            'desc' => str_replace(array('&quot;', '&lt;br&gt;', '<br>'), array('"', '', ''), $spell_info['Description_en_gb'])
        );
        return $data;
    }
}

