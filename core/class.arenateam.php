<?php

require_once("defines.php"); 


class ArenaTeam{
	
	private $name = "";
	private $teamId = -1;
	public $members = array();
	public $type = 0;
	public $size = 0;
	public $personalRating = 0;
	public $selected = false;
	
	public function ArenaTeam($name){
		global $CHDB, $DataDB;
		
		if(is_numeric){
			$team = $CHDB->selectRow('SELECT * FROM arena_team WHERE arenaTeamId = ?d;', $name);
		}
		else {
			$team = $CHDB->selectRow('SELECT * FROM arena_team WHERE name LIKE ?;', $name);
		}
		
		if(!$team)
			return false;

		$this->name = $team["name"];
		$this->teamId = $team["arenaTeamId"];
		$this->size = $team["type"];
		$this->type = $team["type"]."v".$team["type"];
		$this->captainGuid = $team["captainGuid"];
		$this->rank = $team["rank"];
		$this->rating = $team["rating"];
		
		// Load cached ranking
		$cache = $DataDB->selectRow('SELECT * FROM ranking_cache WHERE id = "'.$this->teamId.'" AND type = '.CACHETYPE_TEAMRANKING.';');
		if($cache){
			$this->lastweek_rank = 0;
			$this->rankPage = 1;
		}
		else{
			$this->rank = ($this->rank == 0) ? $cache["rank"] : $this->rank;
			$this->lastweek_rank = $cache["lastweek_rank"];
			$this->rankPage = ceil($team["rank"]/50);
		}

		// Emblem
		$this->backgroundStyle = $team["type"];
		$this->backgroundColor = dechex($team["backgroundColor"]);
		$this->borderColor = dechex($team["borderColor"]);
		$this->borderStyle = $team["type"].$team["borderStyle"];;
		$this->emblemColor = dechex($team["emblemColor"]);
		$this->emblemStyle = $team["emblemStyle"];
		
		$this->weekGames = $team["weekGames"];
		$this->weekWins = $team["weekWins"];
		$this->weekLosses = $team["weekGames"] - $team["weekWins"];
		$this->weekPercentage = round(($team["weekWins"] / $team["weekGames"]) * 100, 2);

		$this->seasonGames = $team["seasonGames"];
		$this->seasonGames = $team["seasonGames"];
		$this->seasonLosses = $team["seasonGames"] - $team["seasonWins"];
		$this->seasonPercentage = round(($team["seasonWins"] / $team["seasonGames"]) * 100, 2);

	}
	
	public function LoadMembers(){
		global $CHDB;
		
		// Find the Members
		$rows = $CHDB->select("
		SELECT characters.guid, 
			arena_team_member.weekGames, arena_team_member.weekWins, 
			arena_team_member.seasonGames, arena_team_member.seasonWins, arena_team_member.personalRating 
			FROM arena_team_member JOIN characters ON(characters.guid = arena_team_member.guid) 
			WHERE arenaTeamId = ?d;", $this->teamId);
		
		$i = 0;
		foreach($rows as $row){
			
			$member = new Character($row["guid"],"small",true);
			
			$member->weekGames = $row["weekGames"];
			$member->weekWins = $row["weekWins"];
			$member->seasonGames = $row["seasonGames"];
			$member->seasonWins = $row["seasonWins"];
			$member->personalRating = $row["personalRating"];
			
			$member->weekLosses = $row["weekGames"] - $row["weekWins"];
			$member->weekPercentage = round(($row["weekWins"] / $row["weekGames"]) * 100, 2);
			$member->weekAttendance = round(($row["weekGames"] / $this->weekGames) * 100, 2);
			
			$member->seasonLosses = $row["seasonGames"] - $row["seasonWins"];
			$member->seasonPercentage = round(($row["seasonWins"] / $row["seasonGames"]) * 100, 2);
			$member->seasonAttendance = round(($row["seasonGames"] / $this->seasonGames) * 100, 2);
			
			$this->members[$i] = $member;
			$i++;
		}
	
	}
	
	public function GetName(){
		return $this->name;
	}
	
	public function GetMembers(){
		return $this->members;
	}
	
	public function GetFactionText(){
		return ($this->faction == FACTION_ALLIANCE) ? "Allianz" : "Horde";
	}
	
	public function GetFactionClass(){
		return ($this->faction == FACTION_ALLIANCE) ? "alliance" : "horde";
	}
	
	public function GetUrl(){
		return "/server/arena/".$this->type."/".urlencode($this->name);
	}
	
	public function GetCaptainGUID(){
		return $this->captainGuid;
	}
}