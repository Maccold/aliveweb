<?php
require ($_SERVER['DOCUMENT_ROOT'].'/core/router/icException.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/core/router/icRouter.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/core/router/icRouteParameter.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/core/router/icRoute.class.php');

require ($_SERVER['DOCUMENT_ROOT'].'/core/router/icSimpleRequest.class.php'); 

$oRequest = new icSimpleRequest();

$oRouter = new icRouter();

//icRouter::$DEBUG=1;
//define some route parameter types here
	$oRPWord = new icRouteParameter(icRouteParameter::REG_MATCH, '\w+' );
   	$oRPBool = new icRouteParameter(icRouteParameter::REG_MATCH, '[01]' );
	$oRPInt = new icRouteParameter(icRouteParameter::INT);
   	$pRPSlug = new icRouteParameter(icRouteParameter::REG_REPLACE, 'A-Za-z\-_0-9' );
   	$pRPNotIn = new icRouteParameter(icRouteParameter::NOT_IN, array('debug') );

//add some routes
//note. 4-th argument of icRoute constructor - you must define all parameters in url with exactly same order!!
//for example if url pattern looks like  '/photo/:action/:id-:name' 4-th argument
//will be something like array( 'action' => NULL, 'id' => NULL, 'name' => NULL)

/*
	Schema:
		new icRoute( 'NAME', // Eindeutiger Bezeichner 
			// URL Muster 
			'/photo/:action/:id-:name/:delete',
			// Default werte für die einzelnen Variablen 
			array( 'module' => 'photo', 'action' => 'index', 'id' => 0, 'delete' => 0, 'name' => 'noname' ),
			// Wenn ein Parameter nur bestimmte Werte entgegennehmen darf dann wird das hier festgelegt
			array( 'action' => $oRPWord, 'id' => $oRPInt, 'name' => $pRPSlug, 'delete' => $oRPBool) ),
	
		Ergebnisse dieser Route:
		/photo/add/1-blub 		=> module=photo, action=add, 	id=1, name=blub, 	delete=0
		/photo/view/1-blub/1 	=> module=photo, action=view, 	id=1, name=blub, 	delete=1
		/photo/list/ 			=> module=photo, action=list, 	id=0, name=noname, 	delete=0
		/photo				 	=> module=photo, action=index, 	id=0, name=noname, 	delete=0

		Bei Fragen einfach Mac fragen ;)
*/

	$oRouter->addRoutes( array(
		
		new icRoute( 'account_logout', 
			'/account/logout/*', 
			array( "ext" => "account", 'sub' => 'login', "action" => "logout" ) 
		),	
		new icRoute( 'server_ta', 
			'/account/transferanleitung/*', 
			array( "ext" => "server", 'sub' => 'ta' ) 
		),	
		new icRoute( 'account_ct', 
			'/account/transferantrag/*', 
			array( "ext" => "account", 'sub' => 'ct' ) 
		),	
		new icRoute( 'change-character', 
			'/change-character/*', 
			array( "ext" => "frontpage", 'sub' => 'index', "action" => "change-character" ) 
		),	
		
		new icRoute( 'ajax_search',	'/ajax/search/:type/*', 				array( "ext" => "ajax", 'sub' => 'search', "type" => "") ),	
		
		
		new icRoute( 'community_bugtracker',	'/bugtracker/*', 				array( "ext" => "community", 'sub' => 'bugtracker') ),	
		new icRoute( 'community_tc001',			'/server/media/videos/*', 		array( "ext" => "community", 'sub' => 'tc001') ),	
		new icRoute( 'community_player',		'/server/spielerbilder/*', 		array( "ext" => "community", 'sub' => 'player') ),
		new icRoute( 'community_changelog',		'/server/changelog', 			array( "ext" => "server", 'sub' => 'roadmap') ),
		new icRoute( 'community_clcore',		'/server/changelog/core/*', 	array( "ext" => "server", 'sub' => 'roadmap') ),	
		new icRoute( 'community_clpage',		'/server/changelog/page/*', 	array( "ext" => "community", 'sub' => 'clpage') ),
		new icRoute( 'server_ta',				'/server/transferanleitung/*', 	array( "ext" => "server", 'sub' => 'ta') ),	
		
		new icRoute( 'server_arena',			'/server/arena/:mode/:teamname/*', array( "ext" => "server", 'sub' => 'arena', "mode" => "", "teamname" => "") ),	
		new icRoute( 'server_pvp-list',			'/server/pvp-list/:mode/*', 	array( "ext" => "server", 'sub' => 'pvp-list', "mode" => "") ),	
		new icRoute( 'server_ranking',			'/server/ranking/:mode/*', 		array( "ext" => "server", 'sub' => 'ranking', "mode" => "achievement") ),
		new icRoute( 'server_talents',			'/talents/*', 					array( "ext" => "server", 'sub' => 'talents') ),
		new icRoute( 'server_bugtracker',		'/server/bugtracker/*', 		array( "ext" => "server", 'sub' => 'bugtracker') ),
		
				
		new icRoute( 'tooltip_item',			'/tooltip/item/:item_id/*', 	array( "ext" => "tooltip", 'sub' => 'item', "item_id" => "") ),	
		
		new icRoute( 'tooltip_character',		'/tooltip/character/:realm/:name/*', 	
						array( "ext" => "tooltip", 'sub' => 'character', "realm" => "norgannon", "name" => "") ),	
		new icRoute( 'tooltip_zone',			'/tooltip/zone/:zone/*', 		array( "ext" => "tooltip", 'sub' => 'zone', "zone" => "") ),	
		new icRoute( 'tooltip_npc',				'/tooltip/npc/:npc_id/*', 		array( "ext" => "tooltip", 'sub' => 'npc', "npc_id" => "") ),	
		
		new icRoute( 'search',					'/search/*', 					array( "ext" => "game", 'sub' => 'search') ),	
		new icRoute( 'game_guild',				'/guild/:guild_realm/:guild_name/*',	array( 
			"ext" => "game", 'sub' => 'guild', "guild_realm" => "Norgannon", "guild_name" => "") ),	
		new icRoute( 'game_zone_detail',		'/game/zone/:zone/:zone_detail', array( "ext" => "game", 'sub' => 'zone', "zone_detail" => "") ),	
		new icRoute( 'game_item_detail',		'/item/:item_id/:detail',		array( "ext" => "game", 'sub' => 'item', "detail" => "base") ),	
		new icRoute( 'game_character',			'/character/:search_realm/:search_for/:char_mode/:char_detail*',	
			array( 
				"ext" => "game", 
				'sub' => 'character', 
				"search_realm" => "norgannon", 
				"search_for" => "", 
				"char_mode" => "simple", 
				"char_detail" => "") ),	
		new icRoute( 'ajax_character',			'/ajax/character/:search_realm/:search_for/*',	
			array( 
				"ext" => "ajax", 
				'sub' => 'character', 
				"search_realm" => "norgannon", 
				"search_for" => "", 
				) ),	
		
		
		new icRoute( 'admin_transferlist_archiv', '/admin/transferlist/archive/*', 	array( "ext" => "admin", 'sub' => 'transferlist', "archive" => true) ),
		new icRoute( 'admin_transferlist_archiv', '/admin/transferlist/*', 	array( "ext" => "admin", 'sub' => 'transferlist', "archive" => false) ),	
		
		
		 
		new icRoute( 'home', 
			'/', 							
			array( 'module' => 'home', 'action' => 'index' ), 
			//define parameters on your own - order is important
			array(),
			//match pattern defined manually 
			'#^/(?:page=(?P<page>[0-9]+))?$#'
		),	
		
		new icRoute( 'module_action_page', 
			'/:ext/:sub/page-:pid/*', 
			array( "ext" => $MW->getConfig->generic->default_component, 'sub' => 'index', "pid" => 1 ) 
		),	
		
		new icRoute( 'module_action', 
			'/:ext/:sub/*', 
			array( "ext" => $MW->getConfig->generic->default_component, 'sub' => 'index' ) 
		)
	));
	
	